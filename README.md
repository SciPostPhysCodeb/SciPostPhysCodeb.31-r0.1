# Codebase release r0.1 for PanScales

by Melissa van Beekveld, Mrinal Dasgupta, Basem Kamal El-Menoufi, Silvia Ferrario Ravasio, Keith Hamilton, Jack Helliwell, Alexander Karlberg, Rok Medves, Pier Francesco Monni, Gavin P. Salam, Ludovic Scyboz, Alba Soto-Ontoso, Gregory Soyez, Rob Verheyen

SciPost Phys. Codebases 31-r0.1 (2024) - published 2024-06-25

[DOI:10.21468/SciPostPhysCodeb.31-r0.1](https://doi.org/10.21468/SciPostPhysCodeb.31-r0.1)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.31-r0.1) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Melissa van Beekveld, Mrinal Dasgupta, Basem Kamal El-Menoufi, Silvia Ferrario Ravasio, Keith Hamilton, Jack Helliwell, Alexander Karlberg, Rok Medves, Pier Francesco Monni, Gavin P. Salam, Ludovic Scyboz, Alba Soto-Ontoso, Gregory Soyez, Rob Verheyen.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.31-r0.1](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.31-r0.1)
* Live (external) repository at [https://gitlab.com/panscales/panscales-0.X/-/tree/release-0.1.2?ref_type=tags](https://gitlab.com/panscales/panscales-0.X/-/tree/release-0.1.2?ref_type=tags)
