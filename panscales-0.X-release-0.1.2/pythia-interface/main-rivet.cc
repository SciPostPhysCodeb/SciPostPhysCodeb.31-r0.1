//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include <iostream>
#include "PanScalesPythiaAnalysisFramework.hh"
#include "fjcore_local.hh"
#include "Pythia8Plugins/Pythia8Rivet.h"

////////////////////////////////////////////
/// Pythia interfaced to PanScales
/// rivet analysis on Pythia-Panscales data:
///   * LEP analysis (ALEPH_1996_S3486095, L3_2004_I652683, ALEPH_2004_S5765862)
///   * HERA analysis  (H1_2006_I699835)
///   * DY @ LHC with 7, 8 or 13 TeV data
///
/// example of a command line is 
/**
    ./build-double/main-rivet -shower panglobal -physical-coupling -no-spin-corr \
              -match-process  -hadron-level -analysis 0  -nev 1e4 -out main-rivet-lep.dat 
*/
///
/// which will run the LEP analysis on e+e- -> Z -> qqbar events
/// or
/**
    ./build-double/main-rivet -shower panglobal -physical-coupling -no-spin-corr \
                -lhapdf-set CT14lo -hadron-level -analysis 2 -nev 1e4 -out main-rivet-lhc.dat
*/
/// 
/// which will run the LHC analysis on pp->Z event
///
/// Make sure that Rivet is compiled in and the relevant analyses are downloaded
class PythiaAnalysisFrameworkRivet : public PythiaAnalysisFramework {
public:
  // declare the rivet analysis
  Pythia8Rivet * rivet;

  PythiaAnalysisFrameworkRivet(CmdLine * cmdline_in) : PythiaAnalysisFramework(cmdline_in) {
    // set the usage of rivet
    cmdline_in->start_section("Rivet-analysis commands");
    std::string output_file = cmdline->value<std::string>("-out");
    rivet = new Pythia8Rivet(_pythia, output_file+".yoda");
    cmdline_in->end_section("Rivet-analysis commands");

    // initialise the process and rivet analysis
    setup_analyses_and_process(cmdline_in);
    // initialise showerrunner, matching, process etc
    init_analysis(cmdline_in);
  }

  // function to set up the Rivet analysis and with it the process
  void setup_analyses_and_process(CmdLine * cmdline){
    cmdline->start_section("Rivet-analysis commands");
    _rivet_analysis = cmdline->value<int>("-analysis", 2).help("Specify which analysis to run: \n" 
      "0 = LEP (ALEPH_1996_S3486095, L3_2004_I652683, ALEPH_2004_S5765862) \n"
      "1 = HERA (H1_2006_I699835) \n"
      "2 = LHC: DY@7, 8 or 13 TeV \n");
    // add info to header
    switch (_rivet_analysis){
      case 0:  
        init_LEP();
        break;
      case 1:  
        init_HERA();
        break;
      case 2:
        init_LHC_DY();
        break;
      default: 
        throw invalid_argument("This analysis is not implemented");
    }
    cmdline->end_section("Rivet-analysis commands");
  }

  // sets up the analysis for LEP configuration
  void init_LEP(){
    cmdline->start_subsection("LEP specific settings");
    // set up the process (e+e- -> Z -> qqbar)
    _pythia.readString("Beams:idA = 11");
    _pythia.readString("Beams:idB = -11");
    double rts = cmdline->value<double>("-rts", 91.2)
                            .help("collider centre-of-mass energy")
                            .argname("rts");
    _pythia.readString("Beams:eCM = "+to_string(rts));
    _pythia.readString("WeakSingleBoson:ffbar2ffbar(s:gmZ) = on");
    // set the decays of the Z
    _pythia.readString("WeakZ0:gmZmode = 0");
    _pythia.readString("23:onMode = off");
    std::string Zdecays;
    bool only_light_decays = cmdline->value_bool("-Z2uds", false)
                                .help("Set the Z decays to only light partons (default includes decays to c and b)");
    if (only_light_decays) {
      Zdecays = "23:onIfAny = 1 2 3";
    } else {
      Zdecays = "23:onIfAny = 1 2 3 4 5";
    }

    header << "# Pythia hard event with settings: \n# Process e+e-->Z->qqbar\n# WeakSingleBoson:ffbar2ffbar(s:gmZ) = on\n # Zdecays" << Zdecays << std::endl;
    _pythia.readString(Zdecays);
    _pythia.readString("PDF:lepton = off");
    cmdline->end_subsection("LEP specific settings");
    
    // set the LEP analysis
    rivet->addAnalysis("ALEPH_2004_S5765862");
    rivet->addAnalysis("L3_2004_I652683");
    rivet->addAnalysis("ALEPH_1996_S3486095");
    // add info to header
    header << "# Running RIVET analysis LEP (ALEPH_1996_S3486095, L3_2004_I652683, ALEPH_2004_S5765862) \n";
  }

  // sets up the analysis for H1 configuration at HERA
  void init_HERA(){

    cmdline->start_subsection("HERA specific settings");
    ///////////// Process information /////////////////
    // read in the energies of the beam
    double eProton   = cmdline->value<double>("-eProton", 820.).help("Proton beam energy");
    double eElectron = cmdline->value<double>("-eElectron", 27.6).help("Electron beam energy");
    double Q2min     = cmdline->value<double>("-Q2min", 25.).help("minimal Q2 for the hard event");
    // set up the incoming beams
    // we have a frame with unequal beam energies, we need to tell this to pythia
    _pythia.readString("Beams:frameType = 2");
    // BeamA = proton.
    _pythia.readString("Beams:idB = 2212");
    // set the energy
    _pythia.settings.parm("Beams:eB", eProton);
    // BeamB = electron.
    _pythia.readString("Beams:idA = -11");
    // set the energy
    _pythia.settings.parm("Beams:eA", eElectron);
    // make sure the lepton does not radiate a photon
    _pythia.readString("PDF:lepton = off");
    // set up the DIS process
    // neutral current (with gamma/Z interference) -> see 't' in process declaration
    _pythia.readString("WeakBosonExchange:ff2ff(t:gmZ) = on");
    // phase-space cut: minimal Q2 of the process
    _pythia.readString("PhaseSpace:Q2Min = "+to_string(Q2min));
    
    // initialise the PDFs when needed
    string lhapdf_set = cmdline->value<string>("-lhapdf-set", "");
    if (lhapdf_set != ""){
      _pythia.readString("PDF:pSet = LHAPDF6:" + lhapdf_set);
    }

    cmdline->end_subsection("HERA specific settings");

    // set the H1 analysis
    rivet->addAnalysis("H1_2006_I699835");
    header << "# Running RIVET analysis HERA (H1_2006_I699835) \n";
  }

  // LHC DY analysis
  void init_LHC_DY(){

    cmdline->start_subsection("DY at LHC specific settings");
    ///////////// Process information /////////////////
    // set up the incoming beams and energy
    double rts = cmdline->value<double>("-rts", 13000.).help("Collider centre-of-mass energy");
    _pythia.readString("Beams:eCM = "+to_string(rts));
    // set up the pp->Z/gamma->ll process, with l \in [e, mu]
    _pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
    _pythia.readString("23:onMode = off");    // turn off decay into general particles
    _pythia.readString("23:onIfAny = 11 13"); // allow decay into electrons & muons

    // initialise the PDFs when needed
    string lhapdf_set = cmdline->value<string>("-lhapdf-set", "");
    if (lhapdf_set != ""){
      _pythia.readString("PDF:pSet = LHAPDF6:" + lhapdf_set);
    }
    
    cmdline->end_subsection("DY at LHC specific settings");

    // set the analysis for rivet
    if(rts == 13000){
      // Z plus jets at 13 TeV
      rivet->addAnalysis("ATLAS_2017_I1514251");
      // Z pT and Z phi* at 13 TeV
      rivet->addAnalysis("ATLAS_2019_I1768911");
      // Dilepton mass spectrum in 13 TeV pp collisions with 139/fb Run 2 dataset
      // rivet->addAnalysis("ATLAS_2019_I1725190");
      // Measurement of the underlying event using inclusive Z-boson production in proton-proton collisions at sqrt(s) = 13 TeV
      // rivet->addAnalysis("CMS_2017_I1635889");
      // Differential cross section of Z boson production in association with jets in proton-proton collisions at sqrt(s) = 13 TeV
      rivet->addAnalysis("CMS_2018_I1667854");
      // Measurement of the differential Drell-Yan cross section in proton-proton collisions at sqrt(s) = 13 TeV
      // rivet->addAnalysis("CMS_2018_I1711625");
      // Measurements of differential Z boson production cross sections in proton-proton collisions at 13 TeV
      // rivet->addAnalysis("CMS_2019_I1753680");
      // Study of Z boson plus jets events using variables sensitive to double-parton scattering in pp collisions at 13 TeV
      // rivet->addAnalysis("CMS_2021_I1866118");
      // Measurement of the mass dependence of the transverse momentum of lepton pairs in Drell--Yan production in proton-proton collisions at 13 TeV 
      rivet->addAnalysis("CMS_2022_I2079374");
      // adding info to header
      header << "# Running RIVET analysis LHC DY @ 13 TeV (ATLAS_2017_I1514251, ATLAS_2019_I1725190, ATLAS_2019_I1768911, CMS_2017_I1635889, CMS_2018_I1667854, CMS_2018_I1711625, CMS_2019_I1753680, CMS_2021_I1866118, CMS_2022_I2079374) \n";
    } else if (rts == 8000){
      // Zpt and Zphi*
      rivet->addAnalysis("ATLAS_2015_I1408516");
      // High-mass Drell-Yan at 8 TeV
      rivet->addAnalysis("ATLAS_2016_I1467454");
      // Z+jet at 8 TeV
      rivet->addAnalysis("ATLAS_2019_I1744201");
      // Measurement of the transverse momentum spectra of weak vector bosons produced in proton-proton collisions at 8 TeV
      // rivet->addAnalysis("CMS_2016_I1471281");
      // MvB is not sure how to get only the Z here, see https://rivet.hepforge.org/analyses/CMS_2016_I1471281.html
      // VMode has to be passed somehow... 

      // adding info to header
      header << "# Running RIVET analysis LHC DY @ 8 TeV (ATLAS_2015_I1408516, ATLAS_2016_I1467454, ATLAS_2019_I1744201) \n";
    } else if (rts == 7000){
      // Z inclusive cross sections at 7 TeV
      // rivet->addAnalysis("ATLAS_2011_I928289_Z");
      // Z+jets in pp at 7 TeV
      // rivet->addAnalysis("ATLAS_2011_I945498");
      // Measurement of the Z pT with electrons and muons at 7 TeV
      // rivet->addAnalysis("ATLAS_2011_S9131140");
      // Measurement of angular correlations in Drell-Yan lepton pairs to probe Z/gamma boson transverse momentum
      rivet->addAnalysis("ATLAS_2012_I1204784");
      // Z+jets in pp at 7 TeV
      rivet->addAnalysis("ATLAS_2013_I1230812");
      // High-mass Drell-Yan at 7 TeV
      // rivet->addAnalysis("ATLAS_2013_I1234228");
      // Measurement of the low-mass Drell-Yan differential cross section at 7 TeV
      // rivet->addAnalysis("ATLAS_2014_I1288706");
      // Measurement of Z/gamma boson pT at 7 TeV
      rivet->addAnalysis("ATLAS_2014_I1300647");
      // Distributions sensitive to the underlying event in inclusive Z-boson production at 7 TeV
      // rivet->addAnalysis("ATLAS_2014_I1315949");
      // Z forward-backward asymmetry
      // rivet->addAnalysis("ATLAS_2015_I1351916");
      // Event shapes in leptonic Z-events
      // rivet->addAnalysis("ATLAS_2016_I1424838");
      // Measurement of differential Z/gamma* pT and y
      rivet->addAnalysis("CMS_2012_I941555");
      // Azimuthal correlations and event shapes in Z + jets in pp collisions at 7 TeV
      // rivet->addAnalysis("CMS_2013_I1209721");
      // adding info to header
      header << "# Running RIVET analysis LHC DY @ 7 TeV (ATLAS_2011_I928289_Z, ATLAS_2011_I945498, ATLAS_2011_S9131140, ATLAS_2012_I1204784, ATLAS_2013_I1230812, ATLAS_2013_I1234228, ATLAS_2014_I1288706, ATLAS_2014_I1300647, ATLAS_2014_I1315949, ATLAS_2015_I1351916, ATLAS_2016_I1424838, CMS_2012_I941555, CMS_2013_I1209721) \n";
    } else{
      throw invalid_argument("There is no LHC DY analysis for this value of rts");
    }
  }



  // initialises the matching pointer
  // this should arguably be part of the process
  // however, for now, since matching is not handled uniformly, it is done here seperately
  void init_matching(){
    if(!cmdline->present("-match-process")) return;
    if(_rivet_analysis == 0){ // LEP
      // matching is only available if we use one of the panscales showers
      if (!_use_native_pythia){ 
        f_pythia_panscales_process->init_hard_me(f_shower_runner.get());
        f_shower_runner->set_matching(new panscales::MatchedProcess(
                          f_pythia_panscales_process->hard_me(),
                          f_shower_runner->shower(),
                          f_shower_runner->shower_for_matching(), f_shower_runner->strategy(),
                          f_matching_scheme, true, false));
      
      }
    } else{
      assert(false && "Matching not available for this process");
    }
  }
  
  // we just need to run rivet on top of the generate_event
  void user_analyse_event() override {
    // run rivet
    if(event_weight() != 0) (*rivet)();
  }

  private:

  // store the input analysis as an int
  int _rivet_analysis;

};

//----------------------------------------------------------------------
// the main loop
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  PythiaAnalysisFrameworkRivet driver(&cmdline);
  driver.run();
  driver.rivet->done();
  return 0;
}
