//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include "PanScalesPythiaAnalysisFramework.hh"
#include "Collider.hh"
#include "ShowerFromCmdLine.hh"

#include <chrono>

using namespace std;
using namespace std::chrono;

// initialise the pythia-panscales analysisframework
PythiaAnalysisFramework::PythiaAnalysisFramework(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in, "", true){
  // timing information
  f_timingInfo = cmdline->value_bool("-timing-info", true).help("determines whether timing information is requested");
  // seed setup
  auto seed = cmdline->any_optional_value<unsigned>({"-rseq","-seed"});
  if (seed.present()) {
    panscales::gsl.set(seed.value());
    if(seed.value() == 0) 
      std::cout << "Warning - seed corresponds to a random number that is time-based in pythia. Make sure that this is what is needed!" << std::endl;
  } else {
    // the default seed is set to 1
    // avoids setting pythia to 0 which means time-based random
    panscales::gsl.set(1); 
  }
  // MPI settings
  cmdline->section("Pythia hadronisation and MPI settings");
  _include_mpi = cmdline->value_bool("-mpi", false)
      .help("Turn on interleaved MPI in a pp event. Warning - this is not tested / functioning properly.");
  if(_include_mpi){
    _pythia.readString("PartonLevel:MPI = on");
    header << "# PartonLevel:MPI = on" << std::endl;
  } else{ // no MPI
    _pythia.readString("PartonLevel:MPI = off");
    header << "# PartonLevel:MPI = off" << std::endl;
  }
  // hadron level output
  _hadron_level =  cmdline->value_bool("-hadron-level", false).help("Hadronise the partons after showering");
  if(_hadron_level){
    _pythia.readString("HadronLevel:Hadronize = on");
    header << "# HadronLevel:Hadronize = on" << std::endl;
  } else{ // no hadronisation
    _pythia.readString("HadronLevel:Hadronize = off");
    _pythia.readString("HadronLevel:all = off");
    header << "# HadronLevel:Hadronize = off" << std::endl;
  }
  // beam remnants
  _include_beam_remnants = cmdline->value_bool("-remnants", true).help("Determine whether beam remnants must be added");
  if(_include_beam_remnants){
    header << "# PartonLevel:Remnants = on " << std::endl;
  } else{ // no beam remnants
    _pythia.readString("PartonLevel:Remnants = off");
    _pythia.readString("ColourReconnection:reconnect = off");
    _pythia.readString("Check:event = off");
    header << "# PartonLevel:Remnants = off" << std::endl;
  }
  cmdline->end_section("Pythia hadronisation and MPI settings");
  // initialise the tune for pythia's hadronisation model
  if(_hadron_level) 
    init_tune();
}

// initialise the various options
void PythiaAnalysisFramework::init_analysis(CmdLine * cmdline_in){
  // get the collider
  f_collider.reset(panscales::get_collider_from_settings(&_pythia.settings));
  // get the process
  f_pythia_panscales_process.reset(panscales::get_process_from_settings(cmdline_in, &_pythia.particleData, &_pythia.settings));
  // setup the showerrunner etc
  init_shower_qcd_pdf_runner(cmdline_in);
  // matching is only available if we use one of the panscales showers
  if (!_use_native_pythia && _do_match_shower){ 
    f_pythia_panscales_process->init_hard_me(f_shower_runner.get());
    f_shower_runner->set_matching(new panscales::MatchedProcess(
                      f_pythia_panscales_process->hard_me(),
                      f_shower_runner->shower(),
                      f_shower_runner->shower_for_matching(), f_shower_runner->strategy(),
                      f_matching_scheme, true, false));
  }
}

void PythiaAnalysisFramework::init_tune(){
  // there are two choices of tunes: ee and pp
  // according to pythia, Tune:ee is "mainly for the hadronization and timelike-showering aspects of PYTHIA"
  // Tune:pp is "mainly for the initial-state-radiation, multiparton-interactions and beam-remnants aspects of PYTHIA"
  cmdline->start_section("Tuning options for Pythia");
  int tune_option_ee = cmdline->value<int>("-tune-had", 7).help("Tune:ee option for Pythia (default = 7 = Monash 2013 tune)");
  _pythia.readString("Tune:ee = "+to_string(tune_option_ee));
  // tune for pythia
  _pythia.readString("Tune:preferLHAPDF = 2"); //< tell pythia we want to use LHAPDF6 library
  // "ATLAS Tune AZ", is tuned to the pT spectrum of the Z^/gamma^*0 boson in a set of rapidity bins
  int tune_option_pp = cmdline->value<int>("-tune-mpi", 14).help("Tune:pp option for Pythia (default = 14 = Monash 2013 tune, other option is 17 = ATLAS AZ tune, or 20/21 = ATLAS A14 central tune with MSTW2008lo68cl/NNPDF2.3LO)");
  _pythia.readString("Tune:pp = "+to_string(tune_option_pp));
  auto use_vincia_tune = cmdline->value_bool("-vincia-tune", false).help("Overwrite the tuning with the Vincia settings");
  if(use_vincia_tune.present()){
    set_vincia_tune();
    header << "# Using native Vincia tune" << std::endl;
  } else{
    header << "# Tune:ee = " << tune_option_ee << std::endl;
    header << "# Tune:pp = " << tune_option_pp << std::endl;
  }
  // optionally set aLund and aavLund (not clear it is used)
  // if(cmdline->present("-aLund")){
  //   std::string aLund = cmdline->value<string>("-aLund", "0.68");
  //   _pythia.readString("StringZ:aLund = "+aLund);
  //   std::string avZLund = cmdline->value<string>("-avgZLund","0.55" );
  //   _pythia.readString("StringZ:avgZLund = "+avZLund);
  // }
  cmdline->end_section("Tuning options for Pythia");
}

// performs the operations needed after the startup
void PythiaAnalysisFramework::post_startup() {
  // set the seed information for pythia
  int seed = panscales::gsl.seed();
  _pythia.readString("Random:setSeed = on");
  string seed_info = "Random:seed = ";
  seed_info += std::to_string(seed); 
  _pythia.readString(seed_info);
  _pythia.readString("Next:numberCount = 0"); //< to avoid having a printout from both PanScales and Pythia
  // initialise pythia
  _pythia.init();
  // _pythia.particleData.list(vector<int>{0,1,2,3,4,5,6,21,23,24,25});
  // _pythia.settings.writeFile(std::cout, true); //< if you need a complete output of the pythia settings
  // initialise possible hadron multiplicity
  if(_hadron_level) hists_err["ln_hadronlevel_multiplicity"].declare(0.0,20.0,0.5);
}

// initialise showerrunner, the shower, hoppetrunner
void PythiaAnalysisFramework::init_shower_qcd_pdf_runner(CmdLine * cmdline){
  // check if we want a shower
  bool turn_off_shower = cmdline->present("-no-shower")
                          .help("Option to run unshowered events")
                          .argname("noshower");
  //
  if(turn_off_shower){
    _use_native_pythia = true; //< use pythia for the hard process generation
    // turn off pythia's timeshower
    _pythia.readString("PartonLevel:FSR = off"); //< no final-state radiation
    _pythia.readString("TimeShower:QCDshower = off"); //< q->gamma q
    _pythia.readString("TimeShower:QEDshowerByQ = off"); //< q->gamma q
    _pythia.readString("TimeShower:QEDshowerByL = off"); //< l->gamma l
    _pythia.readString("TimeShower:QEDshowerByOther = off"); //< resonance shower (i.e. gamma off W)
    _pythia.readString("TimeShower:QEDshowerByGamma = off"); //< gamma -> l+l- or qqbar
    // turn off pythia's space shower
    _pythia.readString("PartonLevel:ISR = off"); //< no initial-state radiation
    _pythia.readString("SpaceShower:QCDshower = off"); //< q->gamma q
    _pythia.readString("SpaceShower:QEDshowerByQ = off"); //< q->gamma q
    _pythia.readString("SpaceShower:QEDshowerByL = off"); //< l->gamma l
    header << "# shower = no shower" << std::endl;
    // nothing left to be done
    return;
  }

  // check if we are using native pythia
  string shower_opt = cmdline->value<string>("-shower","panglobal")
    .help("name of shower (incl. dire, dipole-kt, pythia8, panlocal, panglobal, panlocal-vincia, panlocal-antenna, pythia8-native, vincia)")
    .argname("showername");
  _use_native_pythia = (shower_opt == "pythia8-native") || (shower_opt == "vincia");
  
  // cache the matching pointer for now, might be relevant for pythia as well
  string matching_section = "Matching options (most only enabled if -match-process is present)";
  cmdline->start_section(matching_section);
  _do_match_shower = cmdline->any_present({"-match-process","-3-jet-matching"})
                            .help("Turn on matching to the Born process + 1 emission");
  cmdline->end_section(matching_section);

  // if we are using native pythia:
  // 1. set up the header info
  // 2. turn off pythia's QED shower
  // 3. initialise ME corrections if needed
  if(_use_native_pythia){
    bool use_vincia = (shower_opt == "vincia");
    cmdline->start_section("Settings for Pythia's/Vincia's native shower (only active with -shower pythia8-native or vincia)");
    bool with_qed   = cmdline->value_bool("-with-QED", false)
                         .help("Determines whether the QED shower is turned on (default in Pythia)");
    // add information to header to specify we use the native pythia shower here
    if(use_vincia){
      _pythia.readString("PartonShowers:model = 2");
      header << "# shower = vincia" << std::endl;
      // make sure the QED shower is turned off by default
      if(!with_qed){
        _pythia.readString("Vincia:EWmode = 0"); //< no QED shower
        _pythia.readString("Vincia:EWmodeMPI = 0"); //< no QED shower in MPI
        header << "# QED shower = off" << std::endl;
      } else{
        header << "# QED shower = on " << std::endl;
      }
    } else {
      // pythia native
      bool use_local_recoil = cmdline->value_bool("-local",false)
                                        .help("Use the local recoil shower of Pythia");
      if (use_local_recoil){
        _pythia.readString("SpaceShower:dipoleRecoil = on");
        header << "# shower = pythia8-native with dipole-local recoil" << std::endl;
      } else{
        header << "# shower = pythia8-native" << std::endl;
      }
      // turn off the QED shower by default
      if(!with_qed){
        // turn off the time QED shower
        _pythia.readString("TimeShower:QEDshowerByQ = off"); //< q->gamma q
        _pythia.readString("TimeShower:QEDshowerByL = off"); //< l->gamma l
        _pythia.readString("TimeShower:QEDshowerByOther = off"); //< resonance shower (i.e. gamma off W)
        _pythia.readString("TimeShower:QEDshowerByGamma = off"); //< gamma -> l+l- or qqbar
        // turn off the space QED shower
        _pythia.readString("SpaceShower:QEDshowerByQ = off"); //< q->gamma q
        _pythia.readString("SpaceShower:QEDshowerByL = off"); //< l->gamma l
        header << "# QED shower = off" << std::endl;
      } else{
        header << "# QED shower = on " << std::endl;
      }
      header << "# WARNING: Event weight not properly taken into account when filling the histograms! " << std::endl;
      // set up the matching
      if(_do_match_shower){
        _pythia.readString("SpaceShower:MEcorrections = on");
        _pythia.readString("TimeShower:MEcorrections = on");
        header << "# Pythia runs with ME corrections " << std::endl;
      } else{
        // turn off the ME corrections
        _pythia.readString("SpaceShower:MEcorrections = off");
        _pythia.readString("TimeShower:MEcorrections = off");
        header << "# Pythia runs without ME corrections " << std::endl;
      }
      cmdline->end_section("Settings for Pythia's/Vincia's native shower (only active with -shower pythia8-native or vincia)");
    } 
  } else{
    // if not using native pythia, we need to set up showerrunner inside panscales
    // first we need to require massless partons in this case
    for (int i=1; i<=5; i++) {
      _pythia.particleData.m0(i, 0);
    }
    // in addition, we cannot run with MPI turned on
    if(_include_mpi)
      throw runtime_error("MPI at this moment is not functional with a PanScales shower");
    header << "# Using a PanScales shower: all partons are massless now" << std::endl;
    // check that we already have a collider
    if(f_collider == NULL) 
      throw runtime_error("Collider should have been set - aborting");
    if(f_pythia_panscales_process == NULL) 
      throw runtime_error("PythiaProcess should have been set - aborting");
    // set up the shower runner instance
    f_shower_runner.reset(panscales::create_showerrunner_with_shower_and_qcd_instance(*cmdline, header, *f_collider));
    if(f_shower_runner == NULL) 
      if(!cmdline->help_requested()) throw runtime_error("Showerrunner not property set - aborting");
    
    cmdline->start_section("Options for the shower starting and cut-off scale");
    // set the lnvmax and lnvmin based on the collider if not present
    f_lnvmax = cmdline->value("-lnvmax", log(f_collider->rts()))
                         .help("set the maximum lnv value")
                         .argname("lnvmax");
    if (f_shower_runner->shower()->qcd().physical_coupling()) {
      double beta = f_shower_runner->shower()->beta();
      // lnkt_cutoff is really the renormalisation scale of the 
      // coupling below which it is set to zero
      double lnmuR_cutoff = f_shower_runner->shower()->qcd().lnkt_cutoff();
      // include the lnxmuR factor here, because the shower will need to go
      // further down in lnv if we are probing the coupling at lnmur = lnkt + lnxmuR
      // (keeping in mind that lnkt_cutoff, is really the smallest value of lnmur
      // for which the coupling is non-zero, so if lnxmuR > 0, we need to reach
      // physical lnkt values that are below the lnmur_cutoff)
      f_lnvrange = (1+beta) * (f_lnvmax - lnmuR_cutoff + std::max(0.0, f_shower_runner->shower()->qcd().lnxmuR()));
    } else {
      if(!cmdline->help_requested()) throw invalid_argument("Run with physical coupling (add -physical-coupling)");
    }
    f_lnvrange = cmdline->value("-lnvrange", f_lnvrange)
                            .help("set the extent (range) of lnv values (should be positive)")
                            .argname("lnvrange");
    f_lnvmin   = cmdline->value("-lnvmin", f_lnvmax - f_lnvrange)
                            .help("set the lower limit of lnv range")
                            .argname("lnvmin");
    f_lnvrange = f_lnvmax - f_lnvmin;
    header << "# f_lnvmax = " << f_lnvmax << std::endl;
    header << "# f_lnvmin = " << f_lnvmin << std::endl;
    cmdline->end_section("Options for the shower starting and cut-off scale");

    f_hoppet_runner.reset(panscales::create_hoppetrunner(*cmdline, header, f_collider->has_pdfs()));
    // pass the hoppetrunner instance to showerrunner
    init_hoppetrunner_in_showerrunner(*cmdline, f_shower_runner.get(), f_hoppet_runner.get(), f_lnvmax);
    
    // set up hoppet if needed
    if (f_collider->has_pdfs()) {
      if(f_hoppet_runner->pdf_needs_remapped_scales())
        if(!cmdline->help_requested()) throw invalid_argument("Use a physical PDF set, i.e. by setting -pdf-choice ToyNf5Physical or -lhapdf-set XYZ");
      if(f_hoppet_runner == NULL) 
        if(!cmdline->help_requested()) throw runtime_error("Hoppetrunner not property set - aborting");
      // initialise the hoppet runner (sets the scales and nloops correctly)
      if(!cmdline->help_requested()){
        f_hoppet_runner->initialise(f_shower_runner->shower()->qcd());
        // add output information
        header << "# Hoppet: " << f_hoppet_runner->description() << endl;
        // set up the hoppet runner in the process
        f_pythia_panscales_process->set_hoppet_ptr(f_hoppet_runner);
      }
    }
    // then setup the shower inside pythia
    ShowerModelPtr ShowerModelPtr = make_shared<PythiaPanScales>(f_shower_runner, f_hoppet_runner, f_pythia_panscales_process);  
    _pythia.setShowerModelPtr(ShowerModelPtr);
    _pythia.readString("PartonShowers:model = 2"); // hack to make sure the user hooks are properly set  

    // continue with the matching
    if (_do_match_shower || cmdline->help_requested()) {
      // set the shower for matching (it has its own section, so end and restart section)
      cmdline->start_section(matching_section);
      auto matching_shower_opt = cmdline->optional_value<string>("-matching-shower")
        .help("Shower to use for matching (default is the main shower)")
        .argname("matching-shower");
      cmdline->end_section(matching_section);
      set_shower_for_matching(*cmdline, f_shower_runner.get(), *f_collider);
      cmdline->start_section(matching_section);
      // set up the matching scheme
      panscales::MatchingScheme matching_scheme;
      if (cmdline->present("-mcatnlo-matching").help("Change the matching scheme to mc@nlo matching (where supported), "
                                                    "rather than the default multiplicative matching")) {
        matching_scheme = panscales::MCAtNLOStyle;
      } else {
        matching_scheme = panscales::PowhegStyle;
      }

      if (matching_scheme == panscales::MCAtNLOStyle) { // MC@NLO-type matching
        // We use the main shower as a matching shower (to later compute R - R_s)
        // this means we cannot use an explicit matching shower MC@NLO-like matching
        assert(!matching_shower_opt.present() &&
          "The MC@NLO scheme should be run with main_shower = matching_shower.");
        // add the info to the header
        header << "# MC@NLO matching" << endl;
      } else if (matching_scheme == panscales::PowhegStyle) { // Powheg-type matching
        header << "# Powheg-type matching with matching shower " << f_shower_runner->shower_for_matching()->name()
              << ", beta(matching) = " << f_shower_runner->shower_for_matching()->beta() 
              << " [internal description: " << f_shower_runner->shower_for_matching()->description() << "]"
              << endl;
      }
      else {
        std::cerr << "AnalysisFramework.cc: matching scheme not recognized." << std::endl;
        exit(-1);
      }
      cmdline->end_section(matching_section);
      // store the matching scheme
      f_matching_scheme = matching_scheme;
    }
  }
}

// set the matching given a hard matching process
void PythiaAnalysisFramework::set_matching(panscales::MatchingHardMatrixElement * hard_me){
  f_3jet_matching.reset(new panscales::MatchedProcess(
                        hard_me,
                        f_shower_runner->shower(),
                        f_shower_runner->shower_for_matching(), f_shower_runner->strategy(),
                        f_matching_scheme, true, false)); //< random axis = true, power shower = false
  f_shower_runner->set_matching(f_3jet_matching.get());
}

// set the tuning of the shower to that of vincia
void PythiaAnalysisFramework::set_vincia_tune() {
  // Z fractions in string breaks
  _pythia.settings.parm("StringZ:aLund            ", 0.45 );
  _pythia.settings.parm("StringZ:bLund            ", 0.80 );
  _pythia.settings.parm("StringZ:aExtraDiquark    ", 0.90 );
  // Z fractions for heavy quarks
  _pythia.settings.parm("StringZ:rFactC           ", 1.15 );
  _pythia.settings.parm("StringZ:rFactB           ", 0.85 );
  // pT in string breaks
  _pythia.settings.parm("StringPT:sigma",            0.305);
  _pythia.settings.parm("StringPT:enhancedFraction", 0.01);
  _pythia.settings.parm("StringPT:enhancedWidth",    2.0);
  // String breakup flavour parameters
  _pythia.settings.parm("StringFlav:probStoUD     ", 0.205);
  _pythia.settings.parm("StringFlav:mesonUDvector ", 0.42 );
  _pythia.settings.parm("StringFlav:mesonSvector  ", 0.53 );
  _pythia.settings.parm("StringFlav:mesonCvector  ", 1.3  );
  _pythia.settings.parm("StringFlav:mesonBvector  ", 2.2  );
  _pythia.settings.parm("StringFlav:probQQtoQ     ", 0.077);
  _pythia.settings.parm("StringFlav:probSQtoQQ    ", 1.0  );
  _pythia.settings.parm("StringFlav:probQQ1toQQ0  ", 0.025);
  _pythia.settings.parm("StringFlav:etaSup        ", 0.5  );
  _pythia.settings.parm("StringFlav:etaPrimeSup   ", 0.1  );
  _pythia.settings.parm("StringFlav:decupletSup   ", 1.0  );
  _pythia.settings.parm("StringFlav:popcornSpair  ", 0.75 );
  _pythia.settings.parm("StringFlav:popcornSmeson ", 0.75 );
  // Primordial kT
  _pythia.settings.parm("BeamRemnants:primordialKThard ", 0.4 );
  _pythia.settings.parm("BeamRemnants:primordialKTsoft ", 0.25);
  // MB/UE tuning parameters (MPI)
  // Use a "low" alphaS and 2-loop running everywhere, also for MPI
  _pythia.settings.parm("SigmaProcess:alphaSvalue ", 0.119);
  _pythia.settings.mode("SigmaProcess:alphaSorder ", 2);
  _pythia.settings.parm("MultiPartonInteractions:alphaSvalue", 0.119);
  _pythia.settings.mode("MultiPartonInteractions:alphaSorder", 2);
  _pythia.settings.parm("MultiPartonInteractions:pT0ref     ", 2.24);
  _pythia.settings.parm("MultiPartonInteractions:expPow     ", 1.75);
  _pythia.settings.parm("MultiPartonInteractions:ecmPow     ", 0.21);
  // Use PYTHIA 8's baseline CR model
  _pythia.settings.flag("ColourReconnection:reconnect", true);
  _pythia.settings.parm("ColourReconnection:range    ", 1.75);
  // Diffraction: switch off Pythia's perturbative MPI
  // (colours in diffractive systems not yet handled by Vincia)
  _pythia.settings.parm("Diffraction:mMinPert", 1000000.0);

  header << "# Using the Vincia tune for the showers " << std::endl;
}

// generate the event 
bool PythiaAnalysisFramework::generate_event() {
  if (f_timingInfo) f_evt_gen_start = high_resolution_clock::now();
  // generate with pythia.next
  if(_pythia.next()){
    if (!_use_native_pythia){
      // note that for final-state (e+e-->Z->qqbar), it is the dec shower that gets called
      // as the Z boson first needs to decay
      // therefore we check whether the collider has PDFs
      // this might need some rethinking when we start showering a top decay
      if(f_collider->has_pdfs()){
        f_event       = dynamic_cast<PythiaPanScalesTime &>(*_pythia.getShowerModelPtr()->getTimeShower()).ps_event();
        event_weight_ = dynamic_cast<PythiaPanScalesTime &>(*_pythia.getShowerModelPtr()->getTimeShower()).event_weight();
      } else{
        f_event       = dynamic_cast<PythiaPanScalesTime &>(*_pythia.getShowerModelPtr()->getTimeDecShower()).ps_event();
        event_weight_ = dynamic_cast<PythiaPanScalesTime &>(*_pythia.getShowerModelPtr()->getTimeDecShower()).event_weight();
      }
    } else{
      // warning: the event weight is not taken into account this way
      event_weight_ = 1.;
    }
  } else {
    event_weight_ = 0;
  }
  // if the event weight is 0, bail out
  if(event_weight_ == 0) return false;
  // fill cross sections
  if (weight_hist != nullptr)  weight_hist->add_entry(event_weight_, 1.0);
  // track cross section
  xsection_total += event_weight_;
  if (event_weight_ >= 0) xsection_positive += event_weight_;
  else                    xsection_negative += event_weight_;
  if (f_mcatnlo_hard_event) xsection_mcatnlo_hard += event_weight_;
  
  // get the multiplicity (note that this includes the beam remnants if that is turned on)
  if(!_hadron_level){
    multiplicity_unweighted.add_entry(_pythia.event.nFinal(), 1.0);
    multiplicity_weighted  .add_entry(_pythia.event.nFinal(), event_weight_);
    lnmult_hist            .add_entry(log(_pythia.event.nFinal()), event_weight_);
  } else{
    int nparticle_final_state = 0;
    for(int i = 0; i < _pythia.event.size();  i++){
      // particles that were produced at the showering stage
      if(_pythia.event[i].status() == -62) ++ nparticle_final_state;
      // particles that were produced by the beam remnants
      if(_pythia.event[i].status() == -63) ++ nparticle_final_state;
      // if the particles came out of the hard final state (i.e. as a decay)
      // they carry label 23, and should be counted
      if(_pythia.event[i].status() == 23) ++ nparticle_final_state;
    }
    multiplicity_unweighted.add_entry(nparticle_final_state, 1.0);
    multiplicity_weighted  .add_entry(nparticle_final_state, event_weight_);
    lnmult_hist            .add_entry(log(nparticle_final_state), event_weight_);
    
    _hadronlevel_multiplicity_unweighted.add_entry(_pythia.event.nFinal(), 1.0);
    _hadronlevel_multiplicity_weighted  .add_entry(_pythia.event.nFinal(), event_weight_);
    hists_err["ln_hadronlevel_multiplicity"].add_entry(log(_pythia.event.nFinal()), event_weight_);
  } 
  
  // record the timing info if relevant
  if (f_timingInfo) {
    f_evt_gen_end = high_resolution_clock::now();
    duration<double, std::milli> t_evt_gen = f_evt_gen_end - f_evt_gen_start;
    averages["t_evgen_ms"].add_entry(t_evt_gen.count(), 1.0);
    f_evt_ana_start = high_resolution_clock::now();
  }
  
  return true;
}
