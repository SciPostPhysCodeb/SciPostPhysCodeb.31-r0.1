//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

// [ADAPTED by PanScales] 
// main02.cc is a part of the PYTHIA event generator.
// Copyright (C) 2023 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// This is a simple test program. It fits on one slide in a talk.
// It studies the pT_Z spectrum at the LHC.
//
// After building (see README.md), run with:
//
//    build-double/main-pythia02 
//

#include <Pythia8/Pythia.h>
#include "PanScalesPythiaModule.hh"

using namespace Pythia8;
int main() {
  // Generator. Process selection. LHC initialization
  Pythia pythia;
  pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
  pythia.readString("Beams:eCM = 13000.");
  // Switch off all Z0 decays and then switch back on those to leptons.
  pythia.readString("23:onMode = off");
  pythia.readString("23:onIfAny = 11 13");

  // PanScales specific
  pythia.readString("PartonLevel:MPI = off"); // make sure MPI is seen as off 
  ShowerModelPtr shower_model_ptr = make_shared<PythiaPanScales>();  
  pythia.setShowerModelPtr(shower_model_ptr);
  // also set the settings
  std::dynamic_pointer_cast<PythiaPanScales>(shower_model_ptr)->init_settings_ptr(pythia);
  pythia.readString("PartonShowers:model = 2"); // hack to make sure the user hooks are properly set
  // add setting - use the panglobal shower with beta = 0
  pythia.readString("PanScales:shower = panglobal");
  pythia.readString("PanScales:beta = 0");

  pythia.init();
  Hist pTZ("dN/dpTZ", 100, 0., 100.);
  // Begin event loop. Generate event. Skip if error. List first one.
  for (int iEvent = 0; iEvent < 10000; ++iEvent) {
    if (!pythia.next()) continue;
    // Loop over particles in event. Find last Z0 copy. Fill its pT.
    int iZ = 0;
    for (int i = 0; i < pythia.event.size(); ++i)
      if (pythia.event[i].id() == 23) iZ = i;
    pTZ.fill( pythia.event[iZ].pT() );
  // End of event loop. Statistics. Histogram. Done.
  }
  pythia.stat();
  cout << pTZ;
  return 0;
}
