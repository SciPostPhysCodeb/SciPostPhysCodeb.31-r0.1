//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

////////////////////////////////////////////
/// Pythia interfaced to PanScales
/// for generating DIS events at HERA
/// stores the event shapes of the H1 analysis (https://arxiv.org/abs/hep-ex/0512014v1)
/// plus the Qt (https://arxiv.org/pdf/hep-ph/0606285.pdf)
/// 
///
/// An example of a command line is 
/** 
    ./build-double/main-dis -shower panglobal -physical-coupling -no-spin-corr  \
           -lhapdf-set CT14lo -nev 1e4 -out main-dis-pheno.dat
*/
///
/// Note that to run the above command line, one needs 
/// to compile the main PanScales (and pythia) code with lhapdf.
/// This can be done easily through adding '--with-lhapdf' 
/// to the build.py script arguments
///
/// For a test run without physical PDFs, replace
///   "-lhapdf-set CT14lo" 
/// by
///   "-pdf-choice ToyNf5Physical"
///
/// To hadronise the events, add -hadron-level


#include <iostream>
#include "DISCambridge.hh"
#include "PanScalesPythiaAnalysisFramework.hh"
#include "fjcore_local.hh"

using namespace std;
using namespace fjcore;

/// helper class for the declusterings
class declustering{
  public:
  declustering(precision_type kt, precision_type eta){
    _kt  = kt;
    _eta = eta;
  }

  void printout(){
    std::cout << " kt = " << _kt << ", eta = " << _eta << std::endl;
  }
  // returns lnkt
  precision_type lnkt() const {
    return log(_kt);
  }
  // returns kt
  precision_type kt() const {
    return _kt;
  }
  // returns eta
  precision_type eta() const{
    return _eta;
  }
  private:
    precision_type _kt;
    precision_type _eta;
};

class PythiaAnalysisDISPheno : public PythiaAnalysisFramework {
public:
  // ctor
  PythiaAnalysisDISPheno(CmdLine * cmdline_in) : PythiaAnalysisFramework(cmdline_in) {
    ///////////// Process information /////////////////
    // process information
    string cmdline_section = "Options specific to DIS production";
    cmdline_in->start_section(cmdline_section);
    // read in the energies of the beam
    double eProton   = cmdline->value<double>("-eProton", 920.)
                                          .help("Proton beam energy");
    double eElectron = cmdline->value<double>("-eElectron", 27.6)
                                          .help("Electron beam energy");
    // set up the incoming beams
    // we have a frame with unequal beam energies, we need to tell this to pythia
    _pythia.readString("Beams:frameType = 2");
    // BeamA = proton.
    _pythia.readString("Beams:idA = 2212");
    // set the energy
    _pythia.settings.parm("Beams:eA", eProton);
    // BeamB = electron.
    _pythia.readString("Beams:idB = 11");
    // set the energy
    _pythia.settings.parm("Beams:eB", eElectron);
    // make sure the lepton does not radiate a photon
    _pythia.readString("PDF:lepton = off");
    // turn of the showering of the decays of the final-state particles
    _pythia.readString("PartonLevel:FSRinResonances = off");
    // set up the DIS process
    // neutral current (with gamma/Z interference) -> see 't' in process declaration
    _pythia.readString("WeakBosonExchange:ff2ff(t:gmZ) = on");

    // set the electron and proton mass to 0
    _pythia.particleData.m0(11, 0);
    _pythia.particleData.m0(2212, 0);

    // set the scale choice
    int starting_scale_choice = cmdline->value<int>("-scale-choice", 6)
         .help("Scale choice in pythia for the hard event and starting scale of the shower. "\
               "Choose between 1, 4 or 6 where: \n"
               "1: mu=pT \n"
               "4: mu=sqrt(s) \n"
               "6: mu=sqrt(-t)");
    assert(((starting_scale_choice == 1) || (starting_scale_choice == 4) || (starting_scale_choice == 6)) && "Choose between 1 = pT, 4 = sqrt(s), 6 = sqrt(-t)" );
    _pythia.readString("SigmaProcess:renormScale2 = "+to_string(starting_scale_choice));
    _pythia.readString("SigmaProcess:factorScale2 = "+to_string(starting_scale_choice));
    header << "# Scale choice for pythia = " << starting_scale_choice << std::endl;

    // limit the phase-space generation
    _Q2min = cmdline->value<double>("-Q2min", 196.)
              .help("Minimum value of invariant mass photon (default = H1 analysis)");
    _pythia.settings.parm("PhaseSpace:Q2Min", _Q2min);

     // initialise the PDFs when needed
    string lhapdf_set = cmdline->value<string>("-lhapdf-set", "");
    if (lhapdf_set != ""){
      _pythia.readString("PDF:pSet = LHAPDF6:" + lhapdf_set);
    }

    cmdline->end_section(cmdline_section);

    
    // now we can initialise the analysis
    init_analysis(cmdline_in);
    // set the starting scale
    f_pythia_panscales_process->set_starting_scale_choice(starting_scale_choice);

    // further process-specific options
    cmdline->start_section(cmdline_section);
    // jet option
    _dcut = cmdline->value("-dcut", -1.);
    // min energy fraction to record the events
    _min_energy_fraction = cmdline->value<double>("-epsmin", 10.)
                .help("Minumum energy fraction in terms of Q^2 energy in the current hemisphere should be");
    _do_all_histograms = cmdline->value_bool("-do-all-histograms", false)
                          .help("Bin data in Q and y intervals (those from hep-ex/0512014v1)");

    // veto the hard event to speed up generation
    if((!_use_native_pythia && cmdline->present("-veto-hard")) || cmdline->help_requested()){
      double Q2_max = cmdline->value<double>("-Q2max", 40000.).help("Maximum value of invariant mass photon (default = H1 analysis)");
      double y_min  = cmdline->value<double>("-ymin", 0.1).help("Minimum value of proton inelasticity (default = H1 analysis)");
      double y_max  = cmdline->value<double>("-ymax", 0.7).help("Maximum value of proton inelasticity (default = H1 analysis)");
      f_pythia_panscales_process->set_veto_ptr(new VetoDIS_H1(_Q2min, Q2_max, y_min, y_max));
      header << "# Using a hard event veto with Q2min = " << _Q2min << ", Q2max = " << Q2_max <<  ", ymin = " << y_min  << ", ymax = " << y_max << std::endl;
    }
    cmdline->end_section(cmdline_section);
  }

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {
    // jet definition
    _jet_def = fjcore::JetDefinition(new DISCambridge(_dcut));       
    _jet_def.set_recombination_scheme(_recombScheme);

    // declare histograms
    int nbins = 10;
    cumul_hists_err["tau_z"].declare(0, 1, 1./nbins);
    cumul_hists_err["tau_wrt_axis"].declare(0, 0.5, 0.5/nbins); // max of thrust wrt thrust axis is 0.5
    cumul_hists_err["B_z"].declare(0, 0.5, 0.5/nbins); // max of broadening is 0.5
    cumul_hists_err["rho"].declare(0, 0.25, 0.25/nbins); // max of jet mass is 0.25
    cumul_hists_err["C_parameter"].declare(0, 1, 1./nbins);
    hists_err["Qt"].declare(0, 25, 0.25);
    hists_err["pTJ"].declare(0, 50, 0.25);   //pT of the hardest jet
    
    if(_do_all_histograms){
      // we here also do logarithmic bins
      cumul_hists_err["ln_tau_z"].declare(-10., 0, 0.1);
      cumul_hists_err["ln_tau_wrt_axis"].declare(-10., 0, 0.1); // max of thrust wrt thrust axis is 0.5
      cumul_hists_err["ln_B_z"].declare(-10., 0, 0.1); // max of broadening is 0.5
      cumul_hists_err["ln_rho"].declare(-10., 0, 0.1); // max of jet mass is 0.25
      cumul_hists_err["ln_C_parameter"].declare(-10., 0, 0.1);
      hists_err["ln_Qt-mZ"].declare(-10., 2, 0.1);
      hists_err["ln_Qt-Q"].declare(-10., 2, 0.1);
      hists_err["ln_pTJ-Q"].declare(-10., 2, 0.1);
      
      // in addition to these inclusive bins, we also take the bins of table 1
      for(auto range : Qintervals){
        if(pow2(range[1]) <= _Q2min) continue;
        string Qmin = to_string(int(range[0]));
        string Qmax = to_string(int(range[1]));
        string qrange = Qmin+"-"+Qmax;
        cumul_hists_err["tau_z_Q-"+qrange].declare(0, 1, 1./nbins);
        cumul_hists_err["tau_wrt_axis_Q"+qrange].declare(0, 0.5, 0.5/nbins); // max of thrust wrt thrust axis is 0.5
        cumul_hists_err["B_z_Q-"+qrange].declare(0, 0.5, 0.5/nbins); // max of broadening is 0.5
        cumul_hists_err["rho_Q-"+qrange].declare(0, 0.25, 0.25/nbins); // max of jet mass is 0.25
        cumul_hists_err["C_parameter_Q-"+qrange].declare(0, 1, 1./nbins);
        hists_err["Qt_Q-"+qrange].declare(0, 25, 0.25);
        hists_err["pTJ_Q-"+qrange].declare(0, 50, 0.25);
        // 
        cumul_hists_err["ln_tau_z_Q-"+qrange].declare(-10., 0, 0.1);
        cumul_hists_err["ln_tau_wrt_axis_Q-"+qrange].declare(-10., 0, 0.1); // max of thrust wrt thrust axis is 0.5
        cumul_hists_err["ln_B_z_Q-"+qrange].declare(-10., 0, 0.1); // max of broadening is 0.5
        cumul_hists_err["ln_rho_Q-"+qrange].declare(-10., 0, 0.1); // max of jet mass is 0.25
        cumul_hists_err["ln_C_parameter_Q-"+qrange].declare(-10., 0, 0.1);
        hists_err["ln_Qt-mZ_Q-"+qrange].declare(-10., 2, 0.1);
        hists_err["ln_Qt-Q_Q-"+qrange].declare(-10., 2, 0.1);
        hists_err["ln_pTJ-Q_Q-"+qrange].declare(-10., 2, 0.1);
        // we also bin in y 
        for(int i = 0; i < 10; i++){
          string yrange = interval(0.1*i,0.1*(i+1.),1);
          cumul_hists_err["tau_z_Q-"+Qmin+"-"+Qmax+"_y-"+yrange].declare(0, 1, 1./nbins);
          cumul_hists_err["tau_wrt_axis_Q"+Qmin+"-"+Qmax+"_y-"+yrange].declare(0, 0.5, 0.5/nbins); // max of thrust wrt thrust axis is 0.5
          cumul_hists_err["B_z_Q-"+Qmin+"-"+Qmax+"_y-"+yrange].declare(0, 0.5, 0.5/nbins); // max of broadening is 0.5
          cumul_hists_err["rho_Q-"+Qmin+"-"+Qmax+"_y-"+yrange].declare(0, 0.25, 0.25/nbins); // max of jet mass is 0.25
          cumul_hists_err["C_parameter_Q-"+Qmin+"-"+Qmax+"_y-"+yrange].declare(0, 1, 1./nbins);
          hists_err["Qt_Q-"+Qmin+"-"+Qmax+"_y-"+yrange].declare(0, 25, 0.25);  
          hists_err["pTJ_Q-"+Qmin+"-"+Qmax+"_y-"+yrange].declare(0, 50, 0.25);  
          //
          cumul_hists_err["ln_tau_z_Q-"+qrange+"_y-"+yrange].declare(-10., 0, 0.1);
          cumul_hists_err["ln_tau_wrt_axis_Q-"+qrange+"_y-"+yrange].declare(-10., 0, 0.1); // max of thrust wrt thrust axis is 0.5
          cumul_hists_err["ln_B_z_Q-"+qrange+"_y-"+yrange].declare(-10., 0, 0.1); // max of broadening is 0.5
          cumul_hists_err["ln_rho_Q-"+qrange+"_y-"+yrange].declare(-10., 0, 0.1); // max of jet mass is 0.25
          cumul_hists_err["ln_C_parameter_Q-"+qrange+"_y-"+yrange].declare(-10., 0, 0.1);
          hists_err["ln_Qt-mZ_Q-"+qrange+"_y-"+yrange].declare(-10., 2, 0.1);
          hists_err["ln_pTJ-Q_Q-"+qrange+"_y-"+yrange].declare(-10., 2, 0.1);
        }
      }

      // just take the y interval bins
      for(int i = 0; i < 10; i++){
        string yrange = interval(0.1*i,0.1*(i+1.),1);
        cumul_hists_err["tau_z_y-"+yrange].declare(0, 1, 1./nbins);
        cumul_hists_err["tau_wrt_axis_y-"+yrange].declare(0, 0.5, 0.5/nbins); // max of thrust wrt thrust axis is 0.5
        cumul_hists_err["B_z_y-"+yrange].declare(0, 0.5, 0.5/nbins); // max of broadening is 0.5
        cumul_hists_err["rho_y-"+yrange].declare(0, 0.25, 0.25/nbins); // max of jet mass is 0.25
        cumul_hists_err["C_parameter_y-"+yrange].declare(0, 1, 1./nbins);
        hists_err["Qt_y-"+yrange].declare(0, 25, 0.25);  
        hists_err["pTJ_y-"+yrange].declare(0, 50, 0.25);  
        //
        cumul_hists_err["ln_tau_z_y-"+yrange].declare(-10., 0, 0.1);
        cumul_hists_err["ln_tau_wrt_axis_y-"+yrange].declare(-10., 0, 0.1); // max of thrust wrt thrust axis is 0.5
        cumul_hists_err["ln_B_z_y-"+yrange].declare(-10., 0, 0.1); // max of broadening is 0.5
        cumul_hists_err["ln_rho_y-"+yrange].declare(-10., 0, 0.1); // max of jet mass is 0.25
        cumul_hists_err["ln_C_parameter_y-"+yrange].declare(-10., 0, 0.1);
        hists_err["ln_Qt-mZ_y-"+yrange].declare(-10., 2., 0.1);
        hists_err["ln_Qt-Q_y-"+yrange].declare(-10., 2., 0.1);
        hists_err["ln_pTJ-Q_y-"+yrange].declare(-10., 2., 0.1);
      }
    }
  }
  
  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    
    // we need to boost to the Breit frame
    Vec4 q_dis_breit;
    double Q2_bst; //< invariant mass of photon
    double y; // <inelasticity of the photon, defined as pgamma.proton/ plepton.proton
    double x_dis;
    RotBstMatrix Qbst = _get_breit_lorentz_transformation(q_dis_breit, Q2_bst, y, x_dis);
    // boost to breit and store the collection of final-state momenta in the current hemisphere
    std::vector<Vec4> final_state_momenta_in_remnant_hemisphere, final_state_momenta_in_current_hemisphere;
    _boost_to_breit_store_final(Qbst, q_dis_breit, final_state_momenta_in_remnant_hemisphere, final_state_momenta_in_current_hemisphere);
    // checks
    // _pythia.event.list();
    // _boost_from_breit(Qbst);
    // _pythia.event.list(); //< should be the same as the original event

    // then we construct the observables from 0512014v1
    // eqn1 : thrust
    double thrust = 0;
    // eqn2 : thrust_c -> see below, needs a definition of the thrust axis
    // eqn3 : B_z
    double B_z = 0;
    // eqn4 : rho
    double rho = 0;
    Vec4 p_sum_current_hemisphere; //< needed for rho and Qt
    // eqn5 : C -> comes later, needs a double sum
    // note there is a mistake in the paper, C needs to be defined with sin^2(theta)!
    // double C_parm = 0;
    // we need for all the same numerator, sum |p|
    double sum_p = 0;
    // then the events are only accepted if the energy in the current hemisphere is larger then some limit, which is Q/10 (eqn 6)
    double sum_e = 0;

    // build these observables
    for(auto p : final_state_momenta_in_current_hemisphere){
      sum_p  += p.pAbs();
      sum_e  += p.e();
      thrust += abs(p.pz());
      B_z    += p.pT();
      p_sum_current_hemisphere += p;
    }

    // we only accept if the energy is high enough
    double Q_bst   = sqrt(Q2_bst);
    double eps_lim = Q_bst/_min_energy_fraction;
    if(sum_e < eps_lim) return;

    // now normalise the observables
    thrust /= sum_p;
    B_z    /= (2*sum_p);
    double m2_in_hemisphere = p_sum_current_hemisphere.m2Calc();
    // build in a safe for when the mass is evaluated to -0 (otherwise underflow bin gets filled)
    if(m2_in_hemisphere < 0){
      if(m2_in_hemisphere > -1e-8) m2_in_hemisphere = 0;
      else{
        std::cout.precision(16);
        std::cout << "m2 = " << m2_in_hemisphere << std::endl;
        assert(false && "rho is negative");
      }
    }
    rho     = m2_in_hemisphere / pow2((2 * sum_p));

    // add this information to the histograms
    cumul_hists_err["tau_z"].add_entry(1 - thrust, event_weight());
    cumul_hists_err["B_z"].add_entry(B_z, event_weight());
    cumul_hists_err["rho"].add_entry(rho, event_weight());
    double Qt = p_sum_current_hemisphere.pT();
    hists_err["Qt"].add_entry(Qt, event_weight()); //< https://arxiv.org/pdf/hep-ph/0606285.pdf

    // now get thrust w.r.t. thrust axis
    Vec4 thrust_axis(0,0,0,0);
    double thrust_wrt_t = 0.;
    _get_thrust_axis(final_state_momenta_in_current_hemisphere, thrust_axis, thrust_wrt_t);
    thrust_wrt_t /= sum_p;
    cumul_hists_err["tau_wrt_axis"].add_entry(1 - thrust_wrt_t, event_weight());

    // then we calculate the C parameter
    // this sums over all partons in the current hemisphere only
    double C_param = 0;
    _calculate_C_parameter(final_state_momenta_in_current_hemisphere, C_param);
    // now normalise it (do not include a factor of 2 because the sum over particle pairs is only completed half in above)
    C_param *= 3./pow2(sum_p);
    // and bin it
    cumul_hists_err["C_parameter"].add_entry(C_param, event_weight());
    
    // now we need to get the max of the pT of the fat jet and beam jets
    // for this we need the beam direction in the breit frame
    Vec4 proton_in_breit;
    _get_proton_in_breit_frame(Qbst, proton_in_breit);
    // put this in the vector with all partons
    vector<panscales::Momentum> partons{panscales::Momentum(proton_in_breit.px(), proton_in_breit.py(), proton_in_breit.pz(), proton_in_breit.e())};

    // now we cluster the jets
    // first get the vector of all particles
    for(auto p : final_state_momenta_in_current_hemisphere){
      partons.push_back(panscales::Momentum(p.px(), p.py(), p.pz(), p.e()));
    }
    for(auto p : final_state_momenta_in_remnant_hemisphere){
      partons.push_back(panscales::Momentum(p.px(), p.py(), p.pz(), p.e()));
    }
    ClusterSequence cs(partons, _jet_def);
    auto jets = cs.inclusive_jets();

    double betai     =-10;
    int    imacrojet = 0;

    for (unsigned int j=0; j<jets.size()-1; j++){
      double betaj = jets[j].e() * proton_in_breit.e()  -  jets[j].px() * proton_in_breit.px()  -  jets[j].py() * proton_in_breit.py() -  jets[j].pz() * proton_in_breit.pz();
      if(betaj>betai){
        betai = betaj;
        imacrojet = j;
      } 
    }

    vector<declustering> declusts;
    _follow_energetic_branch(jets[imacrojet], declusts);
    double pTJ=0.;
    _max_pT(declusts, pTJ);
    hists_err["pTJ"].add_entry(pTJ, event_weight());

    if(_do_all_histograms){
      // the analysis in 0512014v1 bins it in terms of Q intervals, where Q = sqrt(Q2_bst)
      // determine in which range this event lies
      string Q_interval;
      for(auto range : Qintervals){
        if(Q_bst > range[1]) continue;
        else{
          Q_interval = to_string(int(range[0]))+"-"+to_string(int(range[1]));
          break;
        }
      }
      // find also the y interval range
      string y_interval;
      for(int i = 0; i < 10; i++){
          if(y > (0.1*(i+1.))) continue;
          else{
            y_interval = interval(0.1*i,0.1*(i+1.),1);
            break;
          }
      }
      // bin it
      cumul_hists_err["tau_z_Q-"+Q_interval].add_entry(1 - thrust, event_weight());
      cumul_hists_err["B_z_Q-"+Q_interval].add_entry(B_z, event_weight());
      cumul_hists_err["rho_Q-"+Q_interval].add_entry(rho, event_weight());
      hists_err["Qt_Q-"+Q_interval].add_entry(Qt, event_weight()); 
      hists_err["pTJ_Q-"+Q_interval].add_entry(pTJ, event_weight()); 
      cumul_hists_err["tau_wrt_axis_Q-"+Q_interval].add_entry(1 - thrust_wrt_t, event_weight());
      cumul_hists_err["C_parameter_Q-"+Q_interval].add_entry(C_param, event_weight());
      cumul_hists_err["tau_z_y-"+y_interval].add_entry(1 - thrust, event_weight());
      cumul_hists_err["B_z_y-"+y_interval].add_entry(B_z, event_weight());
      cumul_hists_err["rho_y-"+y_interval].add_entry(rho, event_weight());
      hists_err["Qt_y-"+y_interval].add_entry(Qt, event_weight()); 
      cumul_hists_err["tau_wrt_axis_y-"+y_interval].add_entry(1 - thrust_wrt_t, event_weight());
      cumul_hists_err["C_parameter_y-"+y_interval].add_entry(C_param, event_weight());
      cumul_hists_err["tau_z_Q-"+Q_interval+"_y-"+y_interval].add_entry(1 - thrust, event_weight());
      cumul_hists_err["B_z_Q-"+Q_interval+"_y-"+y_interval].add_entry(B_z, event_weight());
      cumul_hists_err["rho_Q-"+Q_interval+"_y-"+y_interval].add_entry(rho, event_weight());
      hists_err["Qt_Q-"+Q_interval+"_y-"+y_interval].add_entry(Qt, event_weight()); 
      hists_err["pTJ_Q-"+Q_interval+"_y-"+y_interval].add_entry(pTJ, event_weight()); 
      cumul_hists_err["tau_wrt_axis_Q-"+Q_interval+"_y-"+y_interval].add_entry(1 - thrust_wrt_t, event_weight());
      cumul_hists_err["C_parameter_Q-"+Q_interval+"_y-"+y_interval].add_entry(C_param, event_weight());

      // and finally we bin the logarithms
      double ln_tau_z   = safe_log(1-thrust);
      double ln_B_z     = safe_log(B_z);
      double ln_rho     = safe_log(rho);
      double ln_tau_nt  = safe_log(1-thrust_wrt_t);
      double ln_C       = safe_log(C_param);
      double ln_Qt_Qbst = safe_log(Qt/Q_bst);
      double ln_pTJ_Qbst = safe_log(pTJ/Q_bst);
      double ln_Qt_mZ   = safe_log(Qt/91.1876);

      cumul_hists_err["ln_tau_z"].add_entry(ln_tau_z, event_weight());
      cumul_hists_err["ln_tau_wrt_axis"].add_entry(ln_tau_nt, event_weight());
      cumul_hists_err["ln_B_z"].add_entry(ln_B_z, event_weight());
      cumul_hists_err["ln_rho"].add_entry(ln_rho, event_weight());
      cumul_hists_err["ln_C_parameter"].add_entry(ln_C, event_weight());
      hists_err["ln_Qt-mZ"].add_entry(ln_Qt_mZ, event_weight());
      hists_err["ln_Qt-Q"].add_entry(ln_Qt_Qbst, event_weight());
      hists_err["ln_pTJ-Q"].add_entry(ln_pTJ_Qbst, event_weight());

      cumul_hists_err["ln_tau_z_Q-"+Q_interval].add_entry(ln_tau_z, event_weight());
      cumul_hists_err["ln_tau_wrt_axis_Q-"+Q_interval].add_entry(ln_tau_nt, event_weight());
      cumul_hists_err["ln_B_z_Q-"+Q_interval].add_entry(ln_B_z, event_weight());
      cumul_hists_err["ln_rho_Q-"+Q_interval].add_entry(ln_rho, event_weight());
      cumul_hists_err["ln_C_parameter_Q-"+Q_interval].add_entry(ln_C, event_weight());
      hists_err["ln_Qt-mZ_Q-"+Q_interval].add_entry(ln_Qt_mZ, event_weight());
      hists_err["ln_Qt-Q_Q-"+Q_interval].add_entry(ln_Qt_Qbst, event_weight());
      hists_err["ln_pTJ-Q_Q-"+Q_interval].add_entry(ln_pTJ_Qbst, event_weight());

      cumul_hists_err["ln_tau_z_y-"+y_interval].add_entry(ln_tau_z, event_weight());
      cumul_hists_err["ln_tau_wrt_axis_y-"+y_interval].add_entry(ln_tau_nt, event_weight());
      cumul_hists_err["ln_B_z_y-"+y_interval].add_entry(ln_B_z, event_weight());
      cumul_hists_err["ln_rho_y-"+y_interval].add_entry(ln_rho, event_weight());
      cumul_hists_err["ln_C_parameter_y-"+y_interval].add_entry(ln_C, event_weight());
      hists_err["ln_Qt-mZ_y-"+y_interval].add_entry(ln_Qt_mZ, event_weight());
      hists_err["ln_Qt-Q_y-"+y_interval].add_entry(ln_Qt_Qbst, event_weight());
      hists_err["ln_pTJ-Q_y-"+y_interval].add_entry(ln_pTJ_Qbst, event_weight());

      cumul_hists_err["ln_tau_z_Q-"+Q_interval+"_y-"+y_interval].add_entry(ln_tau_z, event_weight());
      cumul_hists_err["ln_tau_wrt_axis_Q-"+Q_interval+"_y-"+y_interval].add_entry(ln_tau_nt, event_weight());
      cumul_hists_err["ln_B_z_Q-"+Q_interval+"_y-"+y_interval].add_entry(ln_B_z, event_weight());
      cumul_hists_err["ln_rho_Q-"+Q_interval+"_y-"+y_interval].add_entry(ln_rho, event_weight());
      cumul_hists_err["ln_C_parameter_Q-"+Q_interval+"_y-"+y_interval].add_entry(ln_C, event_weight());
      hists_err["ln_Qt-mZ_Q-"+Q_interval+"_y-"+y_interval].add_entry(ln_Qt_mZ, event_weight());
      hists_err["ln_Qt-Q_Q-"+Q_interval+"_y-"+y_interval].add_entry(ln_Qt_Qbst, event_weight());
      hists_err["ln_pTJ-Q_Q-"+Q_interval+"_y-"+y_interval].add_entry(ln_pTJ_Qbst, event_weight());
    }
  }

  // helper functions
  private:
  // precision for the thrust-finding algorithm to converge on
  double _thrust_axis_precision = 1e-5;
  double _min_energy_fraction;
  double _Q2min;
  // jet definition
  JetDefinition _jet_def;
  // fjcore::Strategy             _strategy = fjcore::Best; //< default
  fjcore::RecombinationScheme  _recombScheme = fjcore::E_scheme;
  double _dcut;
  // clustersequence
  std::unique_ptr<fjcore::ClusterSequence> _cs;
  // the vector containing the Q intervals
  std::vector<std::vector<double>> Qintervals{{14, 16}, //+2
                                              {16, 20}, //+4
                                              {20, 30}, //+10
                                              {30, 50}, //+20
                                              {50, 70}, //+20
                                              {70, 100}, //+30 //<< up to here it are the H1 bins
                                              {100, 140}, //+40
                                              {140, 200}, //+60
                                              {200, 320} }; //< last bin extends all the way to the CM energy

  // constructs the boost matrix necessary to transform to the Breit frame
  RotBstMatrix _get_breit_lorentz_transformation(Vec4 & q_dis_breit, double & Q2, double & y, double & x_dis){
    // get the incoming beams
    int p_index = abs(_pythia.event[1].id()) == 11 ? 2 : 1;
    int e_index = abs(_pythia.event[1].id()) == 11 ? 1 : 2;
    assert((_pythia.event[2].status() == -12 && _pythia.event[1].status() == -12) && "Code assumes that particles 1 and 2 are the beam particles");
    Vec4 p_proton    = _pythia.event[p_index].p();
    Vec4 p_lepton_in = _pythia.event[e_index].p();
    // and the outgoing lepton
    Vec4 p_lepton_out;
    for (int i = 0; i < _pythia.event.size(); ++i) {
      // final state only
      if (!_pythia.event[i].isFinal()) continue;
      // if it is a lepton store the finals state momentum
      if (_pythia.event[i].idAbs() == 11){
        p_lepton_out = _pythia.event[i].p();
        break;
      }
    }
    // check on the final state momentum
    // Vec4 total_final_momentum;
    // for (int i = 0; i < _pythia.event.size(); ++i) {
    //   // final state only
    //   if (!_pythia.event[i].isFinal()) continue;
    //   // if it is a lepton store the finals state momentum
    //   if (_pythia.event[i].idAbs() == 11) continue;
    //   total_final_momentum += _pythia.event[i].p();
    // }
    // from this construct the dis photon
    Vec4 q_dis = p_lepton_in - p_lepton_out;
    
    // 1. get the invariant mass of the photon
    Q2 = -q_dis.m2Calc();
    // 2. get the x_dis fraction, which is a boost invariant quantity
    double dot_proton_photon = (p_proton * q_dis);
    x_dis = Q2 / (2. * dot_proton_photon);
    // 3. get the inelasticity y
    y = dot_proton_photon / (p_lepton_in * p_proton);
    // from this we construct the boost vector Qbst = n_in + n_out = 2*n_in + q_dis = 2*x_dis*p_proton + q_dis
    Vec4 Qbst = 2*x_dis*p_proton + q_dis;
    // now we know the boost vector, we need to rotate q_dis in the boosted frame to the z-axis
    // first boost it to the Breit CM frame
    q_dis.bstback(Qbst);
    // then get phi
    double phi_rot   = q_dis.phi();
    // rotate q_dis
    q_dis.rot(0,-phi_rot);
    // and finally get theta
    double theta_rot = q_dis.theta();
    q_dis.rot(-theta_rot,0);
    // return the photon in the breit frame to check the sign of the rapidity
    q_dis_breit = q_dis;
    
    // now we set up the boost matrix
    RotBstMatrix rot_bst;
    rot_bst.bstback(Qbst);
    rot_bst.rot(0,-phi_rot);
    rot_bst.rot(-theta_rot,0);

    // // check: these should be back-to-back
    // std::cout << "Proton momentum after boost and rotation " << rot_bst * p_proton << std::endl;
    // std::cout << "n1 momentum after boost and rotation "  << x_dis * (rot_bst * p_proton) << std::endl;
    // std::cout << "Photon momentum after boost and rotation " << rot_bst * copy_q << std::endl;
    // std::cout << "Rotation and boost on final state: " << rot_bst * total_final_momentum << std::endl;
    // std::cout << "Expand: " << sqrt(Q2)/2 * (1 + total_final_momentum.m2Calc() / Q2) << ", " << sqrt(Q2)/2 * (1 - total_final_momentum.m2Calc() / Q2) <<  std::endl;
    return rot_bst;
  } 

  // this function boosts from the lab frame to the breit frame
  // it also stores the set of final-state-hemisphere momenta and constructs Bz, tau and rho
  void _boost_to_breit_store_final(const RotBstMatrix & Qbst, const Vec4 & q_dis_breit,
     std::vector<Vec4> & final_state_momenta_in_remnant_hemisphere, std::vector<Vec4> & final_state_momenta_in_current_hemisphere){
    // get the sign of q_dis.pz() to know what the current hemisphere is
    int sign_q_dis_pz = q_dis_breit.pz() > 0 ? 1 : -1;
    // for sign_q_dis_pz == 1  -> p.eta() > 0 for current hemisphere
    // for sign_q_dis_pz == -1 -> p.eta() < 0 for current hemisphere
    for (int i = 0; i < _pythia.event.size(); ++i) {
      // final state only
      if (!_pythia.event[i].isFinal()) continue;
      // if it is a lepton continue
      if (_pythia.event[i].idAbs() == 11) continue;
      // _pythia.event[i].bstback(Qbst);
      _pythia.event[i].rotbst(Qbst);
      // store the final state momenta as well 
      const auto & p = _pythia.event[i];
      if(sign_q_dis_pz == 1){
        if (p.eta() < 0) final_state_momenta_in_remnant_hemisphere.push_back(p.p());
        else             final_state_momenta_in_current_hemisphere.push_back(p.p());
      } else{ // otherwise if pz < 0; current hemisphere is defined by eta < 0
        if (p.eta() > 0) final_state_momenta_in_remnant_hemisphere.push_back(p.p());
        else             final_state_momenta_in_current_hemisphere.push_back(p.p());
      }
    }
  }

  void _get_proton_in_breit_frame(const RotBstMatrix & Qbst, Vec4 & proton_in_breit){
    int p_index     = abs(_pythia.event[1].id()) == 11 ? 2 : 1;
    proton_in_breit = Qbst * _pythia.event[p_index].p();
    // std::cout << "PROTON " << proton_in_breit << std::endl;
  }

  // this function boosts from the breit frame to the lab
  void _boost_from_breit(const RotBstMatrix & Qbst){
    const auto Qbst_inverse = Qbst.inverse();
    for (int i = 0; i < _pythia.event.size(); ++i) {
      // final state only
      if (!_pythia.event[i].isFinal()) continue;
      // if it is a lepton continue
      if (_pythia.event[i].idAbs() == 11) continue;
      // _pythia.event[i].bst(Qbst);
      _pythia.event[i].rotbst(Qbst_inverse);
    }
  }

  // get the thrust axis over the sum of all final-state partons that are in the current hemisphere
  void _get_thrust_axis(std::vector<Vec4> & final_state_momenta, Vec4 & thrust_axis, double & unnormalised_thrust){
    
    // get the number of final-state partons
    unsigned int n_fs = final_state_momenta.size();
    // if there is only one parton - the trust axis is equal to the three momentum of that parton
    // and the trust is equal to 1
    if (n_fs == 1){
      thrust_axis = final_state_momenta[0]/final_state_momenta[0].pAbs(); 
      //if (thrust_axis.pz() < 0) thrust_axis = -thrust_axis;
      unnormalised_thrust = final_state_momenta[0].pAbs();
      return;
    }
    // otherwise first sort the vectors in terms of their pAbs()
    std::sort(final_state_momenta.begin(), final_state_momenta.end(), [](const Vec4& v1, const Vec4& v2) {
    return v1.pAbs2() > v2.pAbs2();
    });

    // handle special case of thrust if there are only 2 particles
    if (n_fs == 2) {
      // the maximum will just be alligned with the particle with the highest pAbs()
      thrust_axis = final_state_momenta[0]/final_state_momenta[0].pAbs(); 
      // if (thrust_axis.pz() < 0) thrust_axis = -thrust_axis;
      // compute the trust
      for (unsigned int k = 0 ; k < n_fs ; k++) unnormalised_thrust += abs(dot3(thrust_axis, final_state_momenta[k]));
      return;
    }

    // otherwise we need to do some work
    // we use the algorithm written schematically in the pythia manual around eqn 224
    vector<double> thrust_values;
    vector<Vec4>   thrust_axises;
    // we either take the first four or three leading particles
    // resulting in eight or four different starting vectors for thrust
    // for three partons we have (one of these needs to maximize the thrust)
    // n0 = - p0 - p1 - p2
    // n1 =   p0 - p1 - p2
    // n2 = - p0 + p1 - p2
    // n3 =   p0 + p1 - p2  >> sign takes care of this
    // for four or more partons we have:
    // n0 = - p0 - p1 - p2 - p3
    // n1 =   p0 - p1 - p2 - p3 
    // n2 = - p0 + p1 - p2 - p3 
    // n3 =   p0 + p1 - p2 - p3 
    // n4 = - p0 - p1 + p2 - p3
    // n5 =   p0 - p1 + p2 - p3 
    // n6 = - p0 + p1 + p2 - p3 
    // n7 =   p0 + p1 + p2 - p3 
    // if we have exactly four partons, one of these again maximises the thrust
    unsigned int max_n = 4;
    for(int i = 0; i < pow(2, std::min(max_n,n_fs) - 1); i++){
      int sign = i;
      // first build a vector for the initial four jets
      thrust_axis.p(0,0,0,0); // < reset the axis
      // take the four highest energetic partons
      // std::cout << "Trail i = " << i << std::endl;
      for (unsigned int k = 0; k < std::min(max_n,n_fs); k++){
        (sign % 2) == 1 ? thrust_axis += final_state_momenta[k] : thrust_axis -=final_state_momenta[k];
        sign /= 2;
      }
      thrust_axis /= thrust_axis.pAbs(); //< make it a unit vector
      // std::cout << "n" << i << " = " << thrust_axis << std::endl;
      // with this as a starting point we start the convergence algorithm
      // we calculate the projection of each of the momenta on the thrust axis
      // depending on the sign of the projection we add it or subtract it
      // the algorithm converges if the difference between the newly calculated vector and the current
      // thrust axis is smaller than some number
      double diff = numeric_limits<double>::max();
      // int l = 0;
      while (diff > _thrust_axis_precision) {
        Vec4 test_all_partons;
        // loop over all partons
        for (unsigned int k = 0; k < n_fs; k++){
          dot3(test_all_partons, final_state_momenta[k]) > 0 ? test_all_partons+=final_state_momenta[k] : test_all_partons-=final_state_momenta[k];
        }
        // calculate the difference
        diff = (thrust_axis - test_all_partons/test_all_partons.pAbs()).pAbs();
        // reset the benchmark
        thrust_axis = test_all_partons/test_all_partons.pAbs();
        // l++;
      }
      // and calculate the thrust value for this axis
      unnormalised_thrust = 0.;
      for (unsigned int k = 0 ; k < n_fs ; k++) unnormalised_thrust += abs(dot3(thrust_axis, final_state_momenta[k]));
      // now store the quantities
      thrust_axises.push_back(thrust_axis); 
      thrust_values.push_back(unnormalised_thrust);
      // std::cout << "Have just calculated " << thrust_axis << ", with T = " << unnormalised_thrust << ", in " << l << " tries" << std::endl;
    }

    // pick the solution with the largest thrust
    unnormalised_thrust = 0.;
    for (unsigned int i=0 ; i < thrust_axises.size(); i++){
      if (thrust_values[i] > unnormalised_thrust){
        unnormalised_thrust = thrust_values[i];
        thrust_axis         = thrust_axises[i];
      }
    }
  }
  
  // get the C parameter = sum_{i,j} |p_i| |p_j| sin^2 theta_ij= sum_{i,j} pow2(p_i X p_j)/(|p_i|*|p_j|)
  void _calculate_C_parameter(const std::vector<Vec4> & final_state_momenta, double & unnormalised_C){
    unsigned int n_fs = final_state_momenta.size();
    for(unsigned int i = 0; i < n_fs; i++){
      for(unsigned int j = i + 1; j < n_fs; j++){
        // don't need a protection for pAbs == 0, particles are massless
        unnormalised_C += cross3(final_state_momenta[i], final_state_momenta[j]).pAbs2() / (final_state_momenta[i].pAbs() * final_state_momenta[j].pAbs());
        // check for the above calculation
        // double cos_thetaij  = dot3(final_state_momenta[i], final_state_momenta[j])/(final_state_momenta[i].pAbs() * final_state_momenta[j].pAbs());
        // double sin_thetaij2 = 1. - pow2(cos_thetaij);
        // double numerator   = final_state_momenta[i].pAbs() * final_state_momenta[j].pAbs() * sin_thetaij2;
        // std::cout << numerator << " vs  " <<  cross3(final_state_momenta[i], final_state_momenta[j]).pAbs2() / (final_state_momenta[i].pAbs() * final_state_momenta[j].pAbs()) << std::endl;
        // these agree
      }
    }
  }


  // helpers for declustering
  // follows the most energetic particle in a jet recursively
  // and fills the kT and eta
  void _follow_energetic_branch(const PseudoJet & jet, vector<declustering> & declusterings){
    PseudoJet parent_1, parent_2;
    
    if(jet.has_parents(parent_1, parent_2)){
      // make sure parent_1 is the most energetic
      if(parent_2.e() > parent_1.e()) std::swap(parent_1, parent_2);
      // build eta and lnkt

      // 2.p1.p2 = 2E1 E2 - 2|p1|.|p2| cos(theta)
      //   cos(theta) = (E1 E2 - p1.p2)/|p1|.|p2|
      double omc = panscales::one_minus_costheta(panscales::Particle::fromP3M2(panscales::Momentum3<precision_type>(parent_1.px(), parent_1.py(), parent_1.pz()), parent_1.m2()),
                                                 panscales::Particle::fromP3M2(panscales::Momentum3<precision_type>(parent_2.px(), parent_2.py(), parent_2.pz()), parent_2.m2()));
      double eta = std::abs(-0.5 * log((omc)/(2 - omc)));
      double kt  = parent_2.e() * sqrt(omc*(2 - omc)); //< E*sin(theta)
      declustering d(kt, eta);
      declusterings.push_back(d);
      _follow_energetic_branch(parent_1, declusterings);
    
    }
  }
  // get the max and sum of the declusterings, return max_v_obs and sum_v_obs
  void _max_pT(const vector<declustering> dec, double & max_v_obs) {
    // Loop over declusterings
    for( auto d : dec) {
      // Compute contribution to observable from declustering d.
      double pt_obs = d.kt();
      // Update max observable.
      if( pt_obs > max_v_obs ) max_v_obs = pt_obs;
    }
  }

  // helper for the interval output
  string interval(double a, double b, int decimals){
    ostringstream ss;
    ss << setprecision(decimals) << a << "-" << b;
    return ss.str();
  }

  // log avoiding 0
  double safe_log(precision_type x) const{
    return x<0 ? -std::numeric_limits<double>::max() : to_double(log_T(x));
  }

  // optional bool to consider double binned historgrams
  bool _do_all_histograms;
};
//----------------------------------------------------------------------
// the main loop
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  PythiaAnalysisDISPheno driver(&cmdline);
  driver.run();
  return 0;
}
