//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef PANSCALES_PYTHIA_VETO_HARD_H
#define PANSCALES_PYTHIA_VETO_HARD_H

#include "ShowerBase.hh"
#include "Pythia8/Event.h"
#include "Pythia8/SpaceShower.h"
#include "Pythia8/UserHooks.h"


using namespace Pythia8;
using namespace std;

// simple class for vetoing events in the hard generation of pythia
class VetoHardEvent{
    public:
    // default constructor
    VetoHardEvent() {};
    // default destructor
    virtual ~VetoHardEvent() {}
    // the actual veto
    virtual bool veto_event(const Event & event) = 0;
};

// veto for VBF higgs events
class VetoPtHiggs : public VetoHardEvent{
    public:
    //ctor
    VetoPtHiggs(double pT_higgs_min) {
        _pT_higgs_min = pT_higgs_min;
    }
    //dtor
    virtual ~VetoPtHiggs(){} 
    // implementation of the veto
    bool veto_event(const Event & event){
        for(int i = 0; i < event.size(); i++){
            if(event[i].isFinal() && event[i].idAbs() == 25){
                // found a final-state higgs boson, now check its pT
                if(event[i].pT() < _pT_higgs_min) return true;  // pT is too low -> veto
                else                              return false; // we may bail out at this point
            }
        }
        return false;
    }

    private:

    double _pT_higgs_min;
};


// veto for DIS events in hep-ex/0512014v1
// kinematic region covered by analysis is defined by
// 196 < Q^2 < 40,0000 GeV^2
// 0.1 < y   < 0.7
class VetoDIS_H1 : public VetoHardEvent{
    public:

    // ctor sets the analysis
    VetoDIS_H1(double Q2_min, double Q2_max, double y_min, double y_max) {
        _Q2_min = Q2_min;
        _Q2_max = Q2_max;
        _y_min  = y_min;
        _y_max  = y_max;
    }
    // dtor default
    virtual ~VetoDIS_H1(){}
    // implementation of the veto
    bool veto_event(const Event & event){
        double Q2, y;
        // get the Q2 and y variables
        _construct_photon_momentum(event, Q2, y);
        // check the hera cuts
        if((Q2 < _Q2_min) || (Q2 > _Q2_max) || (y < _y_min) || (y > _y_max)) return true;
        return false;
    }

    private:

    double _Q2_min, _Q2_max, _y_min, _y_max;

    // this returns Q2 (the photon invariant mass)
    void _construct_photon_momentum(const Event & event, double & Q2, double & y){
        int p_index = abs(event[1].id()) == 11 ? 2 : 1;
        int e_index = abs(event[1].id()) == 11 ? 1 : 2;
        assert((event[2].status() == -12 && event[1].status() == -12) && "Code assumes that particles 1 and 2 are the beam particles");
        Vec4 p_proton    = event[p_index].p();
        Vec4 p_lepton_in = event[e_index].p();
        // and the outgoing lepton
        Vec4 p_lepton_out;
        for (int i = 0; i < event.size(); ++i) {
            // final state only
            if (!event[i].isFinal()) continue;
            // if it is a lepton store the finals state momentum
            if (event[i].idAbs() == 11){
                p_lepton_out = event[i].p();
                break;
            }
        }
        // from this construct the dis photon
        Vec4 q_dis = p_lepton_in - p_lepton_out;
        // 1. get the invariant mass of the photon
        Q2 = -q_dis.m2Calc();
        // 2. get the inelasticity y
        y = (p_proton * q_dis) / (p_lepton_in * p_proton);
    }
};

#endif // PANSCALES_PYTHIA_VETO_HARD_H