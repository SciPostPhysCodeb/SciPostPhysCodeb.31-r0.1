//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include <iostream>
#include "PanScalesPythiaAnalysisFramework.hh"
#include "fjcore_local.hh"

using namespace std;
using namespace fjcore;




////////////////////////////////////////////
/// Pythia interfaced to PanScales
/// for generating pp -> Hjj events (via VBF)
/// The analysis computes:
///  * rapidity of the Higgs and first, second and third jet
///  * rapidity separation between the first and second jet
///  * pT of the Higgs and first, second and third jet
///  * pT of the Hj1j2 system
///  * invariant mass of the dijet system
///  * azimuthal separation between the first and second jet
///  * azimuthal separation between the higgs, and the first and second jet
///  * HT
///  * number of jets
///
/// example of a command line is 
/** 
    ./build-double/main-vbf -shower panglobal -physical-coupling -no-spin-corr \
              -lhapdf-set CT14lo -ZZ -nev 1e4 -out main-vbf.dat
*/
///
/// This produces VBF events with ZZ fusion (use -WW for WW fusion)
/// Note that to run the above command line, one needs 
/// to compile the main PanScales (and pythia) code with lhapdf.
/// This can be done easily through adding '--with-lhapdf' 
/// to the build.py script arguments
///
/// For a test run without physical PDFs, replace
///   "-lhapdf-set CT14lo" 
/// by
///   "-pdf-choice ToyNf5Physical"
///
/// To hadronise the events, add -hadron-level
///
/// Jets are defined with the anti-kt algorithm and
/// the energy recombination scheme (4-momenta are added)
/// The jet radius parameter, maximal rapidity 
/// and the minimal pTs of the jets
/// can be adjusted via the flags '-R', '-etaj-max', and '-pTjet'
///
/// VBF cuts can be set through '-min-mjj' and '-min-delta-etajj'
/// to determine the minimal dijet invariant mass and rapidity separation
///
/// To run with different scale choices (-scale-choice X), not that one
/// needs to add in pythia; SigmaProcess.cc //
/// replace in store3Kin //, // Vector boson fusion 2 -> 3 processes; recoils in positions 4 and 5.
/// // Different options for renormalization scale.
/// if      (renormScale3VV == 1) Q2RenSave = m3/2. * sqrt(pow2(m3/2.) + p3cm.pT2()); // < powheg scale
/// else if (renormScale3VV == 2) Q2RenSave = sqrt( mTV4S * mTV5S ); //< same as before
/// else if (renormScale3VV == 3) Q2RenSave = pow( mT3S * mTV4S * mTV5S,
///                                           1./3. ); //< same as before
/// else if (renormScale3VV == 4) Q2RenSave = pow2(p4cm.pT() + p5cm.pT() + sqrt(mT3S))/4.; // HT2/4
/// else if (renormScale3VV == 5) Q2RenSave = sH; // < Q2
/// else if (renormScale3VV == 6){
///   assert(false && "Cannot use this scale choice - error with p4cm.pz()");
///   double ECM_had =  sqrt(sH/x1in/x2in) ;
///   double Q2_dip1 = p4cm.pT2() + pow2(p4cm.pz() - sqrt(sH/x1in/x2in)/2. * x1in)  - pow2(p4cm.e() - sqrt(sH/x1in/x2in)/2. * x1in);
///   double Q2_dip2 = p5cm.pT2() + pow2(p5cm.pz() + sqrt(sH/x1in/x2in)/2. * x2in)  - pow2(p5cm.e() - sqrt(sH/x1in/x2in)/2. * x2in);
///   Q2RenSave = sqrt(Q2_dip1*Q2_dip2);
/// }
/// // pick same for Q2FacSave
/// Q2FacSave = Q2RenSave;
class PythiaAnalysisFrameworkVBF : public PythiaAnalysisFramework {
public:
  // ctor

  // Fastjet analysis - select algorithm and parameters
  double R;
  double pTjets;
  Strategy             strategy = Best;
  RecombinationScheme  recombScheme = E_scheme;
  // jet definition
  JetDefinition jet_def;
  double jet_rapidity_max;

  // VBF cuts
  double mjj_min;
  double delta_etajj_min;
  // optional Higgs pT cut
  double pT_higgs_min;
  //
  bool include_soft_pj3 = false; //< if true then the 3rd jet can be very soft

  PythiaAnalysisFrameworkVBF(CmdLine * cmdline_in) : PythiaAnalysisFramework(cmdline_in) {
    string cmdline_section = "Options specific to Higgs production via vector-boson fusion";
    cmdline_in->start_section(cmdline_section);
    // set up the process
    double rts = cmdline->value<double>("-rts", 13600.).help("Collider centre-of-mass energy");
    _pythia.readString("Beams:eCM = "+to_string(rts));
    header << "# f_rts = " << rts << endl;
    // Let the Higgs remain on-shell, simplifies the analysis
    _pythia.readString("25:onMode = on");
    _pythia.readString("25:mayDecay = off");
    // set the widths of Z and W
    _pythia.readString("23:mWidth = 2.4952");
    _pythia.readString("24:mWidth = 10.085");
    // StandardModel:alphaEMmZ   (default = 0.00781751; minimum = 0.00780; maximum = 0.00783)
    // StandardModel:sin2thetaW   (default = 0.2312; minimum = 0.225; maximum = 0.240)
    // _pythia.readString("StandardModel:alphaEMmZ = 0.00775927621");
    // _pythia.readString("StandardModel:sin2thetaW = 0.2312");

    // Higgs production via ZZ VBF or WW VBF
    if(cmdline->present("-ZZ").help("VBF with ZZ"))     {
      _pythia.readString("HiggsSM:ff2Hff(t:ZZ) = on");
      header << "# Pythia hard event with settings: \n# HiggsSM:ff2Hff(t:ZZ) = on\n# 25:onMode = on" << std::endl;
    }
    else if(cmdline->present("-WW").help("VBF with WW")) {
      _pythia.readString("HiggsSM:ff2Hff(t:WW) = on");
      header << "# Pythia hard event with settings: \n# HiggsSM:ff2Hff(t:WW) = on\n# 25:onMode = on" << std::endl;
    }
    else{
      if(!cmdline->help_requested()) throw runtime_error("Choose VBF channel via -ZZ or -WW");
    }
    // set the scale choice
    int scale_choice = cmdline->value<int>("-scale-choice", 5)
         .help("Scale choice in pythia for the hard event and starting scale of the shower. "\
               "Choose between [1..7] where mu^2 = \n"\
               "1: mh/2 * sqrt((mh/2)^2 + pTh^2) \n"
               "2: sqrt((mV1^2 + pTj1^2) * (mV2^2 + pTj2^2)) \n"\
               "3: ((mH^2 + pTH^2) *(mV1^2 + pTj1^2) * (mV2^2 + pTj2^2))^(1/3) \n"\
               "4: HT^2/4 \n"\
               "5: Q^2 \n"\
               "6: sqrt(Q_1^2 * Q_2^2), Q_1 and Q_2 are masses of the two DIS dipoles.\n"\
               "7: Q_1 for chain 1, Q_2 for chain 2 \n"\
               "Note this needs modification of the pythia src file, see instructions in the comments at the top of this .cc file.");  
    if((scale_choice > 0 ) && (scale_choice < 8)){
      _pythia.readString("SigmaProcess:renormScale3VV = "+to_string(min(5, scale_choice))); //< pythia can only run with scale 1-5
      _pythia.readString("SigmaProcess:factorScale3VV = "+to_string(min(5, scale_choice))); //< pythia can only run with scale 1-5
    } else{
      throw runtime_error("Unknown scale choice - choose and integer number between 1 and 7");
    }
    header << "# Scale choice for pythia = " << scale_choice << std::endl;
    // turn of the showering of the decays of the final-state particles
    _pythia.readString("PartonLevel:FSRinResonances = off"); // < the times_dec shower does not work due to the form of the assumed boost in PanGlobal
    
    // set usage of PDFs lhapdf6 in pythia (later also used in panscales)
    string lhapdf_set = cmdline->value<string>("-lhapdf-set", "");
    if (lhapdf_set != "") _pythia.readString("PDF:pSet = LHAPDF6:" + lhapdf_set);

    // set the jet definitions
    R                = cmdline->value<double>("-R", 0.4).help("set the jet radius parameter");
    pTjets           = cmdline->value<double>("-pTjet", 25).help("set the jet minimal pT in GeV");
    jet_rapidity_max = cmdline->value<double>("-etaj-max", 4.5).help("set the maximal rapidity of the jet");
    mjj_min          = cmdline->value<double>("-min-mjj", 600).help("set the minimum dijet invariant mass (VBF cut) in GeV");
    delta_etajj_min  = cmdline->value<double>("-min-delta-etajj", 4.5).help("set the minimum rapidity separation between the two leading jets (VBF cut) ");
    
    // declare our jets
    jet_def = JetDefinition(antikt_algorithm, R, recombScheme, strategy);
    // check how soft the jet can be
    include_soft_pj3 = cmdline->present("-soft-pj3").help("Place no cut on the 3rd jet pT (only set it higher than 1 GeV)");
    cmdline_in->end_section(cmdline_section);
    
    // set which histograms to produce
    _do_all_histograms = cmdline->value_bool("-do-all-histograms", false)
                          .help("If set to true: include histograms without any VBF cuts.");

    // initialise the analysis
    init_analysis(cmdline_in);
    // set the scale choice
    f_pythia_panscales_process->set_starting_scale_choice(scale_choice);
    
    // add additional information that is process specific
    cmdline_in->start_section(cmdline_section);
    if(!_use_native_pythia || cmdline_in->help_requested()){
      bool two_scale_VBF = cmdline->present("-two-scale-VBF").help("Evolve the two sides of the VBF scattering with a different starting scale");
      if (two_scale_VBF){
        assert(scale_choice == 7 && "Veto only makes sense when evolving the shower with two different starting scales");
        bool kt_veto   = cmdline->present("-use-kt-veto");
        bool kin_limit = cmdline->present("-use-kin-limit");
        //Use a kT veto. Notice that for PanLocal, we have a pT-ordering in the big lund Plane
        if(kt_veto){
          f_shower_runner->set_veto(new panscales::EmissionVetoKtVBF<panscales::ShowerBase>());
        } else{        
          f_shower_runner->set_veto(new panscales::EmissionVetoVBF<panscales::ShowerBase>(kin_limit));
        }
      }
      // check for vetos of the hard event
      double pT_higgs_min = cmdline->value<double>("-ptmin", -1)
            .help("Set a mimimum pT for the Higgs in the LO event");
      if(pT_higgs_min > 0){
        f_pythia_panscales_process->set_veto_ptr(new VetoPtHiggs(pT_higgs_min));
        header << "# Using a hard event veto with pT_higgs_min = " << pT_higgs_min << std::endl;
      }
    } else{
      if ((scale_choice == 6) || (scale_choice == 7)){
        throw runtime_error("Pythia's shower cannot run with scale choice "+to_string(scale_choice));
      }
    }
    cmdline_in->end_section(cmdline_section);
  }

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {
    // declare histograms (with and without cuts)
    /////////////////////////////
    /// histograms with no cuts
    /////////////////////////////
    if(_do_all_histograms){
      hists_err["nocut-etahiggs"].declare(-2*jet_rapidity_max, 2*jet_rapidity_max, 4*jet_rapidity_max/(2*36.));
      // histogram for the rapidity of the 1st jet
      hists_err["nocut-etajet1"].declare(-jet_rapidity_max, jet_rapidity_max, 2*jet_rapidity_max/36.);
      hists_err["nocut-absetajet1"].declare(0, jet_rapidity_max, 2*jet_rapidity_max/36.);
      // histogram for the rapidity of the 2nd jet
      hists_err["nocut-etajet2"].declare(-jet_rapidity_max, jet_rapidity_max, 2*jet_rapidity_max/36.);
      hists_err["nocut-absetajet2"].declare(0, jet_rapidity_max, 2*jet_rapidity_max/36.);
      // histogram for the rapidity of the 3rd jet
      hists_err["nocut-etajet3"].declare(-jet_rapidity_max, jet_rapidity_max, 2*jet_rapidity_max/36.);
      hists_err["nocut-absetajet3"].declare(0, jet_rapidity_max, 2*jet_rapidity_max/36.);
      // eta^* = eta_j - eta_0, eta_0 = 1/2(eta_j1 + eta_j2)
      hists_err["nocut-etajet3_star"].declare(0, 2*jet_rapidity_max, 2*jet_rapidity_max/36.);
      // histograms for the pTs of the jets
      hists_err["nocut-pTjet1"].declare(pTjets, 1000, 25.0);
      hists_err["nocut-pTjet2"].declare(pTjets, 500, 25.0);
      hists_err["nocut-pTjet3"].declare(pTjets, 500, 25.0);
      hists_err["nocut-pTjet3-zoom"].declare(pTjets, 50, 0.5);
      hists_err["nocut-pTjet3-small"].declare(0, pTjets, 0.5);
      // histogram for pT of the Higgs boson
      hists_err["nocut-pThiggs"].declare(0, 1000, 25.0);
      // histogram for the pT of the Higgs + j1 + j2 
      hists_err["nocut-pThj1j2"].declare(0, 500, 10.0);
      hists_err["nocut-pThj1j2-zoom"].declare(0, 50., 0.5);
      // histogram for the rapidity seperations between the two jets
      hists_err["nocut-delta_y12"].declare(0, 2*jet_rapidity_max, (2*jet_rapidity_max)/18.); //< min is determined by VBF cut, max by max jet rapidity
      // histogram for the dijet invariant mass
      hists_err["nocut-mjj"].declare(0, 13000, 100.0); //< min is determined by VBF cut
      // histogram for the azimuthal angle difference between the two jets
      hists_err["nocut-delta_phi12"].declare(-M_PI, M_PI, 2*M_PI/10.);
      // histogram for the azimuthal angle difference between the two jets
      hists_err["nocut-delta_phih_12"].declare(-M_PI, M_PI, 2*M_PI/10.);
      // histogram for number of jets
      hists_err["nocut-njets"].declare(1.5, 20.5, 1);
      // HT
      hists_err["nocut-HT"].declare(50, 2000, 25.0);
      // histogram for the HT in abs(eta) < 1/2 (central region)
      hists_err["nocut-central_HT"].declare(0, 1000, 25.0);
    }

    ////////
    // histograms with only the VBF cuts
    ////////
    // histogram for the rapidity of the higgs boson
    hists_err["etahiggs"].declare(-2*jet_rapidity_max, 2*jet_rapidity_max, 4*jet_rapidity_max/(2*36.));
    // histogram for the rapidity of the 3rd jet
    hists_err["etajet3"].declare(-jet_rapidity_max, jet_rapidity_max, 2*jet_rapidity_max/36.);
    hists_err["absetajet3"].declare(0, jet_rapidity_max, 2*jet_rapidity_max/36.);
    // histogram for the rapidity of the 2nd jet
    hists_err["etajet2"].declare(-jet_rapidity_max, jet_rapidity_max, 2*jet_rapidity_max/36.);
    hists_err["absetajet2"].declare(0, jet_rapidity_max, 2*jet_rapidity_max/36.);
    // histogram for the rapidity of the 1st jet
    hists_err["etajet1"].declare(-jet_rapidity_max, jet_rapidity_max, 2*jet_rapidity_max/36.);
    hists_err["absetajet1"].declare(0, jet_rapidity_max, 2*jet_rapidity_max/36.);
    
    // eta^* = eta_j - eta_0, eta_0 = 1/2(eta_j1 + eta_j2)
    hists_err["etajet3_star"].declare(0, 2*jet_rapidity_max, 2*jet_rapidity_max/36.);
    // histograms for the pTs of the jets
    hists_err["pTjet1"].declare(pTjets, 1000, 25.0);
    hists_err["pTjet2"].declare(pTjets, 500, 25.0);
    hists_err["pTjet3"].declare(pTjets, 500, 25.0);
    hists_err["pTjet3-zoom"].declare(pTjets, 50., 0.5);
    hists_err["pTjet3-small"].declare(0, pTjets, 0.5);
    // histogram for pT of the Higgs boson
    hists_err["pThiggs"].declare(0, 1000, 25.0);
    // histogram for the pT of the Higgs + j1 + j2 
    hists_err["pThj1j2"].declare(0, 500, 10.0);
    hists_err["pThj1j2-zoom"].declare(0, 50., 0.5);
    // histogram for the rapidity seperations between the two jets
    hists_err["delta_y12"].declare(delta_etajj_min, 2*jet_rapidity_max, (2*jet_rapidity_max - delta_etajj_min)/18.); //< min is determined by VBF cut, max by max jet rapidity
    // histogram for the dijet invariant mass
    hists_err["mjj"].declare(mjj_min, 13000, 100.0); //< min is determined by VBF cut
    // histogram for the azimuthal angle difference between the two jets
    hists_err["delta_phi12"].declare(-M_PI, M_PI, 2*M_PI/10.);
    // histogram for the azimuthal angle difference between the two jets
    hists_err["delta_phih_12"].declare(-M_PI, M_PI, 2*M_PI/10.);
    // histogram for number of jets
    hists_err["njets"].declare(1.5, 20.5, 1);
    // HT
    hists_err["HT"].declare(50, 2000, 25.0);
    // histogram for the HT in abs(eta) < 1/2 (central region)
    hists_err["central_HT"].declare(0, 1000, 25.0);
  }
  
  // apply the VBF cuts to a list of jets
  bool check_VBF_cuts(const vector<PseudoJet> & sortedJets){
    // Employ the VBF cuts
    // m_j1j2 >= 600 GeV [default]
    if((sortedJets[0] + sortedJets[1]).m() < mjj_min) return true; // [2006.15458]
    // |delta_eta_j1j2| >= 4.5 [default]
    if(abs(sortedJets[0].eta() - sortedJets[1].eta()) < delta_etajj_min) return true; //[2006.15458]
    // eta_j1*eta_j2 < 0 [default]
    if(sortedJets[0].eta()*sortedJets[1].eta() > 0) return true;
    return false;
  }
  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    // get list of fastjet pseudojet inputs
    vector<PseudoJet> fjInputs;
    // Keep track of missing ET
    // Vec4 missingETvec;
    // keep track of the Higgs momentum
    Vec4 higgs;
    // Loop over event record to decide what to pass to FastJet
    // _pythia.event.list();
    for (int i = 0; i < _pythia.event.size(); ++i) {
      // Final state only
      if (!_pythia.event[i].isFinal()) continue;
      // Not the Higgs
      if (_pythia.event[i].idAbs() == 25){
        higgs = _pythia.event[i].p();
        continue;
      }
      // No neutrinos
      if (_pythia.event[i].idAbs() == 12 || _pythia.event[i].idAbs() == 14 ||
          _pythia.event[i].idAbs() == 16)     continue;
      // Missing ET
      // missingETvec += _pythia.event[i].p();
      // Store as input to Fastjet
      fjInputs.push_back(PseudoJet(_pythia.event[i].px(), _pythia.event[i].py(), _pythia.event[i].pz(), _pythia.event[i].e()));
    }

    if (fjInputs.size() == 0) {
      cout << "Error: event with no final state particles" << endl;
      return;
    }
    
    // run Fastjet algorithm
    ClusterSequence cs(fjInputs, jet_def);
    auto inclusiveJets = cs.inclusive_jets();

    // Extract inclusive jets sorted by pT (note minimum pT of 25.0 GeV)
    // only select the jets we want
    Selector select_rapidity = SelectorAbsEtaMax(jet_rapidity_max);
    double min_pT_non_leading_jets = pTjets;
    if(include_soft_pj3){
      min_pT_non_leading_jets = 1.;
    } 
    Selector select_pt       = SelectorPtMin(min_pT_non_leading_jets);
    Selector select_both     = select_pt && select_rapidity;
    auto selected_jets       = select_both(inclusiveJets);
    // sort the jets
    auto sortedJets    = sorted_by_pt(selected_jets);
    // check we have at least 2 jets after selection
    if(sortedJets.size() < 2){
      return;
    } else if(sortedJets[1].perp() < pTjets){ // additional check if include_soft_pj3 = true
      return;
    }


    // add the pTs of the higgs, jet1 and j2 in a vectorial sum
    Vec4 sum_h_j1_j2(higgs.px() + sortedJets[0].px() + sortedJets[1].px(), higgs.py() + sortedJets[0].py() + sortedJets[1].py(),higgs.pz() + sortedJets[0].pz() + sortedJets[1].pz(),higgs.e() + sortedJets[0].E() + sortedJets[1].E());
    
    // delta phi j1 and j2
    double delta_phi12; //2006.15458, 0609075 (gives info on the pseudoscalar component)
    if(sortedJets[0].eta() > sortedJets[1].eta()) delta_phi12 = sortedJets[0].phi() - sortedJets[1].phi();
    else                                          delta_phi12 = sortedJets[1].phi() - sortedJets[0].phi();
    // make sure the histogram is between -pi and pi
    if(delta_phi12 < -M_PI) delta_phi12 += 2.*M_PI;
    if(delta_phi12 >  M_PI) delta_phi12 -= 2.*M_PI;

     // delta phi higgs and j1j2
    double delta_phih_12; //2006.15458, 0609075 (gives info on the pseudoscalar component)
    auto j1j2 = sortedJets[0] + sortedJets[1];
    delta_phih_12 = higgs.phi() - j1j2.phi();
    // make sure the histogram is between -pi and pi
    if(delta_phih_12 < -M_PI) delta_phih_12 += 2.*M_PI;
    if(delta_phih_12 >  M_PI) delta_phih_12 -= 2.*M_PI;

    // obtain HT and HT in |eta| < 0.5
    double HT_central = 0;
    double HT         = 0;
    for(unsigned int nj = 0; nj < sortedJets.size(); nj++){
      auto & jet = sortedJets[nj];
      HT += jet.perp();
      if(abs(jet.eta()) < 0.5) HT_central += jet.perp();
    }

    //eta*
    double eta0 = 0, eta_star = 0;
    if(sortedJets.size() > 2){
      eta0     = 0.5*(sortedJets[0].eta() + sortedJets[1].eta());
      eta_star = std::abs(sortedJets[2].eta() - eta0);
    }

    // fill the histograms
    if(_do_all_histograms){
      // store the Higgs information
      hists_err["nocut-etahiggs"].add_entry(higgs.eta(), event_weight());
      hists_err["nocut-pThiggs"].add_entry(higgs.pT(), event_weight());
      // the two jet pTs
      hists_err["nocut-pTjet1"].add_entry(sortedJets[0].perp(), event_weight());
      hists_err["nocut-pTjet2"].add_entry(sortedJets[1].perp(), event_weight());
      // sum of h+j1+j2
      hists_err["nocut-pThj1j2"].add_entry(sum_h_j1_j2.pT(), event_weight());
      hists_err["nocut-pThj1j2-zoom"].add_entry(sum_h_j1_j2.pT(), event_weight());
      // rapidity separation
      hists_err["nocut-delta_y12"].add_entry(abs(sortedJets[0].eta() - sortedJets[1].eta()), event_weight());
      // histogram for the dijet invariant mass
      hists_err["nocut-mjj"].add_entry((sortedJets[0] + sortedJets[1]).m(), event_weight());
    
      // histogram for the azimuthal angle difference between the two jets
      hists_err["nocut-delta_phi12"].add_entry(delta_phi12, event_weight());
      hists_err["nocut-delta_phih_12"].add_entry(delta_phih_12, event_weight());
      // number of jets
      hists_err["nocut-njets"].add_entry(sortedJets.size(), event_weight());
      // HT
      hists_err["nocut-HT"].add_entry(HT, event_weight());
      hists_err["nocut-central_HT"].add_entry(HT_central, event_weight());

      if(sortedJets.size() > 1){
        hists_err["nocut-etajet1"].add_entry(sortedJets[0].eta(), event_weight());
        hists_err["nocut-absetajet1"].add_entry(std::abs(sortedJets[0].eta()), event_weight());
        hists_err["nocut-etajet2"].add_entry(sortedJets[1].eta(), event_weight());
        hists_err["nocut-absetajet2"].add_entry(std::abs(sortedJets[1].eta()), event_weight());
      }
    
      if(sortedJets.size() > 2){
        hists_err["nocut-pTjet3-small"] .add_entry(sortedJets[2].perp(), event_weight());
        hists_err["nocut-pTjet3"] .add_entry(sortedJets[2].perp(), event_weight());
        hists_err["nocut-pTjet3-zoom"] .add_entry(sortedJets[2].perp(), event_weight());
        hists_err["nocut-etajet3"].add_entry(sortedJets[2].eta(), event_weight());
        hists_err["nocut-absetajet3"].add_entry(std::abs(sortedJets[2].eta()), event_weight());
        // bin eta*
        hists_err["nocut-etajet3_star"].add_entry(eta_star, event_weight());
      }
    }

    // otherwise, check whether we pass the VBF veto
    bool vetoEvent = check_VBF_cuts(sortedJets);

    // if we pass the vetos, fill the histograms for the VBF analysis
    if(!vetoEvent){
      hists_err["etahiggs"].add_entry(higgs.eta(), event_weight());
      hists_err["pThiggs"].add_entry(higgs.pT(), event_weight());
      hists_err["pTjet1"].add_entry(sortedJets[0].perp(), event_weight());
      hists_err["etajet1"].add_entry(sortedJets[0].eta(), event_weight());
      hists_err["absetajet1"].add_entry(std::abs(sortedJets[0].eta()), event_weight());
      hists_err["etajet2"].add_entry(sortedJets[1].eta(), event_weight());
      hists_err["absetajet2"].add_entry(std::abs(sortedJets[1].eta()), event_weight());
      hists_err["pTjet2"].add_entry(sortedJets[1].perp(), event_weight());
      hists_err["pThj1j2"].add_entry(sum_h_j1_j2.pT(), event_weight());
      hists_err["pThj1j2-zoom"].add_entry(sum_h_j1_j2.pT(), event_weight());
      hists_err["delta_y12"].add_entry(abs(sortedJets[0].eta() - sortedJets[1].eta()), event_weight());
      hists_err["mjj"].add_entry((sortedJets[0] + sortedJets[1]).m(), event_weight());
      hists_err["delta_phi12"].add_entry(delta_phi12, event_weight());
      hists_err["delta_phih_12"].add_entry(delta_phih_12, event_weight());
      hists_err["njets"].add_entry(sortedJets.size(), event_weight());
      hists_err["HT"].add_entry(HT, event_weight());
      hists_err["central_HT"].add_entry(HT_central, event_weight());
      if(sortedJets.size() > 2){
        hists_err["pTjet3-small"] .add_entry(sortedJets[2].perp(), event_weight());
        hists_err["pTjet3"] .add_entry(sortedJets[2].perp(), event_weight());
        hists_err["pTjet3-zoom"] .add_entry(sortedJets[2].perp(), event_weight());
        hists_err["etajet3"].add_entry(sortedJets[2].eta(), event_weight());
        hists_err["absetajet3"].add_entry(std::abs(sortedJets[2].eta()), event_weight());
        hists_err["etajet3_star"].add_entry(eta_star, event_weight());
      }
    }
  }

  // private variables
  private:
  bool _do_all_histograms = false; //< only produce a small selection of histograms
};

//----------------------------------------------------------------------
// the main loop
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  PythiaAnalysisFrameworkVBF driver(&cmdline);
  driver.run();
  return 0;
}
