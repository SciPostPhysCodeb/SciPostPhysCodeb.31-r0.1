//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include "PanScalesPythiaProcess.hh"
#include "Pythia8/BeamParticle.h"
#include "Pythia8/PartonSystems.h"
// #define PSVERBOSE

namespace panscales{

// ctor for a PythiaProcess, does nothing
PythiaProcess::PythiaProcess(const CmdLine * cmdline, const Pythia8::ParticleData*  particle_data_ptr_pythia){}

// prepare the panscales hard event of the process given by pythia
void PythiaProcess::_get_scattered_event(int i_sys, Pythia8::BeamParticle* beamA_pythia, 
                                                    Pythia8::BeamParticle* beamB_pythia, Pythia8::PartonSystems* parton_systems_ptr, 
                                                    Pythia8::Event* pythia_hard_event){
  // new event: reset the mapping of the idxs
  _map_ps_idx_to_py_idx.clear();
  // reset the event weight
  _event_weight = 1.;
  //_pythia->info.weight()

  // now we set up the hard event
  // this consists of:
  // 1. beams
  // 2. initial- and final-state partons
  // 3. colour connections including auxiliary information and colour transitions
  // 4. remaining colour singlet particles
  // 5. handle the born weight when matching the process
  // 6. spin information -> not handled right now

  // 1. we set up the beams and map them to the panscales format
  Momentum beamA_mom(beamA_pythia->px(), beamA_pythia->py(), beamA_pythia->pz(), beamA_pythia->e());
  Momentum beamB_mom(beamB_pythia->px(), beamB_pythia->py(), beamB_pythia->pz(), beamB_pythia->e());
  Particle beamA(beamA_mom, beamA_pythia->id());
  Particle beamB(beamB_mom, beamB_pythia->id());

  // 2. get the partons
  // first we declare the vector of particles
  std::vector<Particle> particles;
  // keep track of whether we are initialising an MPI system
  bool event_is_MPI_scattering = false;
  // check whether we have initial-state beams
  if(parton_systems_ptr->hasInAB(i_sys)){
    // if there is an initial state, get particle A
    int idxA = parton_systems_ptr->getInA(i_sys);
    // get the reference to the initial-state parton
    const Pythia8::Particle &pa_pythia = (*pythia_hard_event)[idxA];
    // make a copy for panscales
    Momentum pa(pa_pythia.px(), pa_pythia.py(), pa_pythia.pz(), pa_pythia.e(), pa_pythia.m2());
    // push it into the vector of particles
    particles.push_back(Particle(pa, pa_pythia.id()));
    // add the map between panscales and pythia particles
    _map_ps_idx_to_py_idx.push_back(idxA);
    // set some properties of the initial state particle 
    particles[particles.size()-1].set_initial_state(1);     // this is beam 1
    particles[particles.size()-1].set_hard_system(false);   // not part of the hard system
    // also get the other initial-state, same steps as above
    int idxB = parton_systems_ptr->getInB(i_sys);// get the reference to the initial-state parton
    const Pythia8::Particle &pb_pythia = (*pythia_hard_event)[idxB];
    Momentum pb(pb_pythia.px(), pb_pythia.py(), pb_pythia.pz(), pb_pythia.e(), pb_pythia.m2());
    particles.push_back(Particle(pb, pb_pythia.id()));
    _map_ps_idx_to_py_idx.push_back(idxB);
    particles[particles.size()-1].set_initial_state(2);     // this is beam 2
    particles[particles.size()-1].set_hard_system(false);   // not part of the hard system
    // MPI is characterised by status ID = -31 in pythia
    if((pa_pythia.status() == -31) && (pb_pythia.status() == -31)) event_is_MPI_scattering = true;
  }

  // then also get the final-state particles
  for (unsigned int i = 0; i < (unsigned int) parton_systems_ptr->sizeOut(i_sys); ++i){
    // get the index of the final-state parton
    int idx = parton_systems_ptr->getOut(i_sys, i);
    // get the refence for the pythia particle
    const Pythia8::Particle &final_pythia = (*pythia_hard_event)[idx];
    // add it to the panscales list of particles
    Momentum p(final_pythia.px(), final_pythia.py(), final_pythia.pz(), final_pythia.e(), final_pythia.m2());
    particles.push_back(Particle(p, final_pythia.id()));
    _map_ps_idx_to_py_idx.push_back(idx);
    // set the properties of the particle
    particles[particles.size()-1].set_hard_system(true);   // part of the hard system
  }

  // 3. get the colour connections for the dipoles, non-splitting dipoles and the colour transitions
  // first get the LC connections
  std::vector<Dipole> dipoles               = _get_colour_dipoles(pythia_hard_event, &particles); 
  // pair up the dipoles such that the auxiliary information is set correctly
  // this includes the creation of auxiliary dipoles
  std::vector<Dipole> non_splitting_dipoles = _get_non_splitting_dipoles(&dipoles);   
  // set the colour transitions in the existing dipoles
  // note - this may need to be done in a process-specific way
  _set_colour_transitions(&dipoles, &non_splitting_dipoles, &particles);

  #ifdef PSVERBOSE
  std::cout << "Created the dipoles: " << std::endl;
  for (unsigned int dip_idx = 0; dip_idx < dipoles.size(); dip_idx++){
    std::cout << "[3bar,3] = [" << dipoles[dip_idx].index_qbar << ", " << dipoles[dip_idx].index_q << "], idx next: " <<dipoles[dip_idx].index_next <<", prev: " << dipoles[dip_idx].index_previous << std::endl;
    std::cout << dipoles[dip_idx].colour_transitions << std::endl;
  }
  for (unsigned int dip_idx = 0; dip_idx < non_splitting_dipoles.size(); dip_idx++){
    std::cout << "NS [3bar,3] = [" << non_splitting_dipoles[dip_idx].index_qbar << ", " << non_splitting_dipoles[dip_idx].index_q << "], idx next: " << non_splitting_dipoles[dip_idx].index_next <<", prev: " << non_splitting_dipoles[dip_idx].index_previous << std::endl;
  }
  
  #endif // PSVERBOSE

  // 4. create and store the event
  if(event_is_MPI_scattering){
    // set the MPI event
    _mpi_event  = Event(particles, dipoles, non_splitting_dipoles, beamA, beamB);
    // check the MPI event (may be removed when all has settled down)
    if(!_mpi_event.check_dipoles_pdgids()){
      throw std::runtime_error("Pairing of dipoles was not successful in the MPI event - abort");
    }
    // set the weight
    _mpi_weight = 1.;
  } else{
    _hard_event = Event(particles, dipoles, non_splitting_dipoles, beamA, beamB);
    // check the hard event (may be removed when all has settled down)
    if(!_hard_event.check_dipoles_pdgids()){
      throw std::runtime_error("Pairing of dipoles was not successful - abort");
    }
  }

  #ifdef PSVERBOSE
  std::cout << _hard_event << std::endl;
  _hard_event.print_following_dipoles();
  #endif // PSVERBOSE

  // 5. set the born ME (only if the event is not MPI)
  if(_hard_me_ptr != NULL && !event_is_MPI_scattering){
    _hard_event.set_born_me(_hard_me_ptr->get_tree_level_weight(_hard_event));
  }

  // 6. spin information... tbd
}


// get the colour dipoles using the information
// of the pythia hard event colour-connections
std::vector<Dipole> PythiaProcess::_get_colour_dipoles(Pythia8::Event* pythia_hard_event, std::vector<Particle> * particles){

  // get a vector of connected parton - colour pairs
  // keep track separately for colour and anti-colour
  std::map<int, int> col_pairs;
  std::map<int, int> acol_pairs;

  // for each particle in particles find the colour connections
  // in the pythia event
  // this fills up the maps col_pairs and acol_pairs
  for(unsigned int panscales_idx = 0; panscales_idx < particles->size(); panscales_idx++){
    std::pair <int, int> colour_and_particle; 
    // colour         = first 
    // particle index = second
    colour_and_particle.second = panscales_idx;
    
    // colType = 0 for colour singlet -> don't do anything
    //         = 1 for quark / anti-quark
    //         = 2 for gluon
    // colType = colour_type
    // particle.col(), acol() give the indices (2 for gluon, only 1 non-zero for quark/anti-quark)
    unsigned int pythia_idx = _map_ps_idx_to_py_idx[panscales_idx];
    // get the colour type
    int colour_type = (*pythia_hard_event)[pythia_idx].colType();
    if(colour_type == -1){ // == anti-quark
      assert((*pythia_hard_event)[pythia_idx].col() == 0 && "Found quark-type object with two indices, don't know what to do with this!");
      colour_and_particle.first = (*pythia_hard_event)[pythia_idx].acol();
      // check whether we are dealing with an initial-state quark
      // if so, we need to change the colour convention to ours (i.e. turn it around)
      if((*pythia_hard_event)[pythia_idx].status() < 0) // initial-state
        col_pairs. insert(colour_and_particle);
      else                                               
        acol_pairs.insert(colour_and_particle);
    } else if(colour_type == 1){ // == quark
      assert((*pythia_hard_event)[pythia_idx].acol() == 0 && "Found quark-type object with two indices, don't know what to do with this!");
      colour_and_particle.first = (*pythia_hard_event)[pythia_idx].col();
      if((*pythia_hard_event)[pythia_idx].status() < 0) // initial-state
        acol_pairs.insert(colour_and_particle);
      else                           
        col_pairs. insert(colour_and_particle);
    } else if (colour_type == 2){ // == gluon
      // first add the anti-colour
      colour_and_particle.first = (*pythia_hard_event)[pythia_idx].acol(); 
      if((*pythia_hard_event)[pythia_idx].status() < 0) // initial-state 
        col_pairs. insert(colour_and_particle);
      else                           
        acol_pairs.insert(colour_and_particle);
      // change and add the colour
      colour_and_particle.first = (*pythia_hard_event)[pythia_idx].col(); 
      if((*pythia_hard_event)[pythia_idx].status() < 0) // initial-state
        acol_pairs.insert(colour_and_particle);
      else                           
        col_pairs. insert(colour_and_particle);
    } else if (colour_type == 0){ // singlet - don't do anything
      continue;
    } else{ // other colour types are unknown
      assert(false && "Error - unknown colour type");
    }
  }
  assert(col_pairs.size() == acol_pairs.size() && "Error - number of colours is not equal to number of anti-colours");
  #ifdef PSVERBOSE
  pythia_hard_event->list();
  std::cout << "List of colour pairs: " << std::endl;
  for (std::map<int, int>::iterator it_col = col_pairs.begin(); it_col != col_pairs.end(); it_col++){
    std::cout << it_col->first << ", " << it_col->second << std::endl;
  }

  std::cout << "List of anti-colour pairs: " << std::endl;
  for (std::map<int, int>::iterator it_col = acol_pairs.begin(); it_col != acol_pairs.end(); it_col++){
    std::cout << it_col->first << ", " << it_col->second << std::endl;
  }
  #endif // PSVERBOSE


  // now we can make the dipoles
  // get a vector of fixed size (number of colour pairs)
  std::vector<Dipole> dipoles(col_pairs.size());
  // start with the first dipole, this increases in the loop below
  int dip_index = 0;
  // scan over all the colour pairs
  for (std::map<int, int>::iterator it_col = col_pairs.begin(); it_col != col_pairs.end(); it_col++) {
    panscales::Dipole dipole;
    dipole.index_q    = it_col->second;
    dipole.index_qbar = acol_pairs[it_col->first];
    // dipole connections will be handled later but we already set a
    // dummy value here
    dipole.index_next = dipole.index_previous = -1;
    dipoles[dip_index] = dipole;
    dip_index += 1;
  }
  // 
  return dipoles;
}

std::vector<Dipole> PythiaProcess::_get_non_splitting_dipoles(std::vector<Dipole> * dipoles){
  // initialise an empty vector
  std::vector<Dipole> non_splitting_dipoles;
  // to construct the auxiliary connections, we need to set
  // the index_next and index_previous for each dipole
  // for a dipole with a g 3-end, the index_next points
  // to the dipole that has that g as 3bar-end
  // for a dipole with a q(final)/qbar(initial) at a 3-end we need to add
  // an auxiliary dipole with that q at the 3bar-end
  // index_next will point to that aux dipole (=-2-non_splitting_dipole_idx)
  // where non_splitting_dipole_idx is place in non_splitting_dipoles array
  // for a dipole with a qbar(final)/q(initial) at the 3bar end we do
  // the same, but then index_prev needs to point to the aux dipole

  // the algorithm below will construct the above information
  // in O(N^2) operations
  // first we make a map of 3end and 3barend indices
  // this operation is O(N)
  std::map<int, unsigned int> dipole_3ends;     //< first index = index_q   , second is dipole_idx
  std::map<int, unsigned int> dipole_3barends;  //< first index = index_qbar, second is dipole_idx
  for(unsigned int dipole_idx = 0; dipole_idx < dipoles->size(); dipole_idx++){
    std::pair <int, unsigned int> index_and_dipole;
    index_and_dipole.second = dipole_idx;
    // add the 3end 
    index_and_dipole.first  = (*dipoles)[dipole_idx].index_q;
    dipole_3ends.insert(index_and_dipole);
    // add the 3bar end (overwrite the info in index_and_diple)
    index_and_dipole.first  = (*dipoles)[dipole_idx].index_qbar;
    dipole_3barends.insert(index_and_dipole);
  }

  // in the second step we pair up the dipoles 
  // (i.e. set the auxiliary information for the gluon ends)
  // to keep track of the indices that do not have
  // a partner, we will simply remove it from the
  // map once a pair is found
  // this operation is again O(N)
  // declare the iterator to go through the 3 and 3bar maps
  std::map<int,unsigned int>::iterator it;
  // loop over dipoles
  for(unsigned int dipole_idx = 0; dipole_idx < dipoles->size(); dipole_idx++){
    // for the index_next we need to see whether the
    // current dipole 3end has a 3bar partner
    it = dipole_3barends.find((*dipoles)[dipole_idx].index_q);
    if(it != dipole_3barends.end()){ 
      // this indicates the qbar index was found
      // we set the correct dipole index for the current dipole
      (*dipoles)[dipole_idx].index_previous = it->second;
      // we erase the value from the map, it has found a partner
      dipole_3barends.erase(it);
    } 
    // now we do the same for the 3bar-end
    it = dipole_3ends.find((*dipoles)[dipole_idx].index_qbar);
    if(it != dipole_3ends.end()){ 
      // this indicates the q-end index was found
      // we set the correct dipole index for the current dipole
      (*dipoles)[dipole_idx].index_next = it->second;
      // we erase the value from the map, it has found a partner
      dipole_3ends.erase(it);
    } 
  }

  // we now end up with two situations
  // 1. all dipoles are connected
  //    * size of dipole_3barends and dipole_3ends = 0
  //    * we don't have to find the non-splitting dipoles / colour transitions
  // 2. we are left with some unpaired indices
  //    * we should have an equal amount of 3 and 3bar ends that are unpaired
  //    * we need to deal with colour transitions in all dipoles
  //    * we need to add non-splitting dipoles to provide the auxiliary
  //      momenta information to the CF ends of the dipoles

  // check the remaining indices are equal in number
  if(dipole_3ends.size() != dipole_3barends.size())
    throw std::runtime_error("The number of unpaired 3-ends is not equal to the 3bar-ends");
  
  // return if no unpaired indices remain
  // this corresponds to case 1 above
  if(dipole_3ends.size() == 0) 
    return non_splitting_dipoles;
  
  // case 2: find the auxiliary dipoles
  // we first note that for any dipole with only
  // an open q OR qbar index, we definitely need a colour transition
  // for now we put this transition at eta = 0
  // later we will handle this more carefully
  //
  // if we have just 1 unpaired index, we pair it up (no choice)
  if(dipole_3ends.size() == 1){
    // create the non-splitting-dipole
    panscales::Dipole ns_dipole;
    // supply the information from the 3-end
    it = dipole_3ends.begin();
    ns_dipole.index_qbar = it->first;
    ns_dipole.index_next = it->second;
    // supply the information from the 3bar-end
    it = dipole_3barends.begin();
    ns_dipole.index_q        = it->first;
    ns_dipole.index_previous = it->second;
    // put it in the array
    non_splitting_dipoles.push_back(ns_dipole);
    // supply the auxiliary information to
    // the splitting dipoles
    int non_splitting_index = -1 - non_splitting_dipoles.size();
    (*dipoles)[ns_dipole.index_previous].index_next = non_splitting_index;
    (*dipoles)[ns_dipole.index_next].index_previous = non_splitting_index;
    // return the found non-splitting dipoles
    return non_splitting_dipoles;
  }

  // if we have 2 unpaired indices but the
  // number of total dipoles is 2 we can
  // still pair them up straightforwardly
  // this corresponds to the case of all-quark 2->2 event
  // some of the dipoles will have been connected
  // the other will be the auxiliary dipoles
  if((dipole_3ends.size() == 2) && (dipoles->size() == 2)){
    it = dipole_3barends.begin();
    panscales::Dipole ns_dipole1, ns_dipole2;
    ns_dipole1.index_q        = it->first;
    ns_dipole1.index_previous = it->second;
    // for the other ns-dipole we need
    // to set the qbar end to the q end of
    // the end that the current qbar index is connected to
    ns_dipole2.index_qbar = (*dipoles)[it->second].index_q;
    ns_dipole2.index_next = it->second;
    it++; //< points to the next unpaired index with a different dipole
    ns_dipole2.index_q        = it->first;
    ns_dipole2.index_previous = it->second;
    ns_dipole1.index_qbar     = (*dipoles)[it->second].index_q;
    ns_dipole1.index_next     = it->second;
    // add them to the non-spitting-dipole vector
    non_splitting_dipoles.push_back(ns_dipole1);
    non_splitting_dipoles.push_back(ns_dipole2);
    //
    // and now update the information in the pre-existing dipoles
    int ns_dipole_index = -2; 
    for(auto ns_dipole: non_splitting_dipoles){
      (*dipoles)[ns_dipole.index_next].index_previous = ns_dipole_index;
      (*dipoles)[ns_dipole.index_previous].index_next = ns_dipole_index;
      ns_dipole_index--;
    }
    // return the found non-splitting dipoles
    return non_splitting_dipoles;
  }  

  // other cases cannot be handled right now - need some prescription
  // to pair up dipoles in e.g. a gg->qqbar qqbar event
  std::cout << "Number of unpaired dipole indices = " << dipole_3barends.size() << std::endl; 
  throw std::runtime_error("Cannot handle this number of unpaired indices");

  // code should never arrive here,
  // this return is dummy
  return non_splitting_dipoles; 
}

// given a vector of dipoles this function sets up the colour transitions
// it does this in a simple way
// a more elaborate transition needs to be dealt with on a process-specific
// basis, as one needs to know the propagators of the process in that case
// in this simple case, the information in non_splitting_dipoles and particles is not used
void PythiaProcess::_set_colour_transitions(std::vector<Dipole> *dipoles, const vector<Dipole> * non_splitting_dipoles, const vector<Particle> *particles){
  // now we set up the colour transitions
  // in the list of dipoles there are three situations:
  // * index_next && index_prev => 0: gg dipole -> set up ColourTransitionVector(false), no transitions
  // * index_next && index_prev < 0:  qq dipole -> set up ColourTransitionVector(true), no transitions
  // * index_next * index_prev < 0:   qg or gq dipole -> set up ColourTransitionVector(3barend_is_quark), ColourTransition at eta = 0
  //    3barend = quark if the index_previous < 0
  // for(auto dipole : (*dipoles)){ //< creates a copy of the dipole, we don't want that
  for(unsigned int dipole_idx = 0; dipole_idx < dipoles->size(); dipole_idx++){
    if(((*dipoles)[dipole_idx].index_next < 0) && ((*dipoles)[dipole_idx].index_previous < 0)){
      // qqbar
      (*dipoles)[dipole_idx].colour_transitions = ColourTransitionVector(true);
    } else if (((*dipoles)[dipole_idx].index_next >= 0) && ((*dipoles)[dipole_idx].index_previous >= 0)){
      // gg
      (*dipoles)[dipole_idx].colour_transitions = ColourTransitionVector(false);
    } else{
      ColourTransitionVector colour_transition_vector(((*dipoles)[dipole_idx].index_previous >= 0));
      // we insert a transition point at eta = 0
      ColourTransitionPoint colour_transition_point(0.);
      // we set the colour transition point to the back
      // this means that the colour of the first is not changed 
      colour_transition_vector.push_back(colour_transition_point);
      // copy over the information to the dipole
      (*dipoles)[dipole_idx].colour_transitions = std::move(colour_transition_vector);
    }
  }
}

// checks whether the process has partons
bool PythiaProcess::_event_contains_partons(){
  // loop over the hard-event particles
  for(unsigned int i = 0; i < _hard_event.size(); i++){
    // we exit the loop once we found a parton
    if(_hard_event[i].is_parton()) return true;
  }
  // if no parton has been found we exit
  return false;
}

// updates the event with MPI
void PythiaProcess::update_current_event_with_mpi(Event & panscales_event, std::vector<unsigned int> & map_ps_to_py) {
  // check whether we need to add MPI
  if(_mpi_weight == 0){
    throw runtime_error("MPI weight is 0, this means that the MPI was not initialised - aborting");
  }
  if(_mpi_event.size() == 0){
    throw runtime_error("Empty MPI event found - aborting");
  }

  // otherwise we can continue with adding the MPI
  // this is done by simply adding the dipoles and particles to the panscales event
  
  // first we handle the particles
  // we store the new index, this information is used when adding the dipoles
  std::vector<int> new_particle_idx;
  for(auto p : _mpi_event.particles()){
    new_particle_idx.push_back(panscales_event.add_particle_return_index(p));
  }

  // add the dipoles, store the current number of dipoles
  const unsigned int n_current_dipoles = panscales_event.dipoles().size();
  // loop over all dipoles and add them
  for(auto d : _mpi_event.dipoles()){
    // get a copy of the dipole, as we need to overwrite some information
    Dipole new_dipole = d;
    // overwrite the q and qbar ends
    new_dipole.index_q    = new_particle_idx[d.index_q];
    new_dipole.index_qbar = new_particle_idx[d.index_qbar];
    // overwrite the previous and next dipoles
    // we need the index_next and prev to point to the
    // location in the array where the dipole will be stored
    // this means we just add n_current_dipoles to the
    // already stored number, if this number is not -1
    if(d.index_next != -1)
      new_dipole.index_next     += n_current_dipoles;
    if(d.index_previous != -1)
      new_dipole.index_previous += n_current_dipoles;
   panscales_event.add_dipole_return_index(new_dipole);
  }
  //reset the MPI weight and event, this event should not be used again
  _mpi_weight = 0.;
  _mpi_event  = Event();
}
////////////////////////////////////////////////
// process specific functions
////////////////////////////////////////////////

/////////////////////////////
// e+e- -> qqbar
/////////////////////////////

// ctor
PythiaProcessee2Z::PythiaProcessee2Z(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia) :
  PythiaProcess(cmdline, particle_data_ptr_pythia){}

/////////////////////////////
// DY
/////////////////////////////

// ctor
PythiaProcessDY::PythiaProcessDY(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia) :
  PythiaProcess(cmdline, particle_data_ptr_pythia){}

// gets the log of the starting scale of the event for showering
double PythiaProcessDY::get_log_starting_scale() const {
  assert(_hard_event.size() > 0 && "Hard event does not seem to be initialised - aborting");
  assert(_hard_event.size() ==3 && "Hard event seems not to be of the right size - aborting");
  // from the born process get the mandelstam invariants
  if(_starting_scale_choice == 0)       return log(_hard_event[2].m());
  else if (_starting_scale_choice == 1) return log(_hard_event.rts()); //< think whether this needs reweighting of the PDFs...
  else{
    assert(false && "This scale choice is not implemented");
  }
}

/////////////////////////////
// DIS
/////////////////////////////

// ctor
PythiaProcessDIS::PythiaProcessDIS(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia) :
  PythiaProcess(cmdline, particle_data_ptr_pythia){}

// prepare the hard event for a DIS process
// it sets the dipole chain information needed for the panscales showers
bool PythiaProcessDIS::prepare_event_for_showering(int i_sys, 
                                    Pythia8::BeamParticle* beamA_pythia, Pythia8::BeamParticle* beamB_pythia, 
                                    Pythia8::PartonSystems* parton_systems_ptr, Pythia8::Event* pythia_hard_event){
  // get the hard event
  _get_scattered_event(i_sys, beamA_pythia, beamB_pythia, parton_systems_ptr, pythia_hard_event);
  // get a reference to the dipoles and particles of the event
  // this is a VBF event: we should have only two dipoles
  const std::vector<Particle> & particles = _hard_event.particles();
  const std::vector<Dipole> & dipoles     = _hard_event.dipoles();
  assert(dipoles.size() == 1 && "The event does not have 1 dipole");
  // get the DIS chain information
  _hard_event.particles()[dipoles[0].index_q]   .set_DIS_chain(1);
  _hard_event.particles()[dipoles[0].index_qbar].set_DIS_chain(1);
  const panscales::Particle & p1_chain1 = (particles[dipoles[0].index_q].is_initial_state()) ? particles[dipoles[0].index_q]    : particles[dipoles[0].index_qbar];
  const panscales::Particle & p2_chain1 = (particles[dipoles[0].index_q].is_initial_state()) ? particles[dipoles[0].index_qbar] : particles[dipoles[0].index_q];
  _hard_event.set_incoming_ref_momenta(p1_chain1, p2_chain1, 1);
  
  // first check the presence of partons
  bool event_is_showerable = _event_contains_partons();
  // then check the veto if needed
  if(_veto_ptr != NULL){
    event_is_showerable = !_veto_ptr->veto_event(*pythia_hard_event);
  }
  return event_is_showerable;
}

// gets the log of the starting scale of the event for showering
double PythiaProcessDIS::get_log_starting_scale() const {
  assert(_hard_event.size() > 0 && "Hard event does not seem to be initialised - aborting");
  // from the born process get the mandelstam invariants
  double s = (_hard_event[0].momentum() + _hard_event[1].momentum()).m2();
  double t = (_hard_event[0].momentum() - _hard_event[2].momentum()).m2();
  double u = (_hard_event[0].momentum() - _hard_event[3].momentum()).m2();
  // get pT2
  double pT2 = u * t / (s); 
  switch (_starting_scale_choice)
    {
    case 1: // get the pT2 of the system
      return 0.5*log(pT2);
      break;
    case 4: // return the invariant mass of the system
      return 0.5*log(s);
      break;
    case 6: // return the t-channel invariant
      return 0.5*log(-t); //< this is the invariant mass of the photon
      break;
    default:
      return 0.5*log(-t);
      break;
    }
  }

/////////////////////////////
// VBF
/////////////////////////////

// ctor
PythiaProcessVBF::PythiaProcessVBF(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia) :
  PythiaProcess(cmdline, particle_data_ptr_pythia){
  // we also need the mass of the Z boson or W boson, depending on the interference
  bool use_ZZ = cmdline->present("-ZZ") ? true : false;
  _mV2 = use_ZZ ? pow2(particle_data_ptr_pythia->m0(23)) : pow2(particle_data_ptr_pythia->m0(24));
  // using a kinematic upper limit for the starting scale
  _use_kinematic_limit = cmdline->present("-use-kin-limit");
}

// prepare the hard event for a VBF process
// in addition to creating the hard event
// it sets the dipole chain information
// and it updates the event weight if a specific starting scale choice is used (6 or 7)
bool PythiaProcessVBF::prepare_event_for_showering(int i_sys, 
                                    Pythia8::BeamParticle* beamA_pythia, Pythia8::BeamParticle* beamB_pythia, 
                                    Pythia8::PartonSystems* parton_systems_ptr, Pythia8::Event* pythia_hard_event){
  // get the hard event
  _get_scattered_event(i_sys, beamA_pythia, beamB_pythia, parton_systems_ptr, pythia_hard_event);
  // get a reference to the dipoles and particles of the event
  // this is a VBF event: we should have only two dipoles
  const std::vector<Particle> & particles = _hard_event.particles();
  const std::vector<Dipole> & dipoles     = _hard_event.dipoles();
  assert(dipoles.size() == 2 && "The event does not have 2 dipoles");

  // now set the dipole chain information correctly, as needed for the shower
  // and also the reference vectors for the two DIS chains
  int chain_1_dipole_index = ((particles[dipoles[0].index_q].initial_state() == 1) || (particles[dipoles[0].index_qbar].initial_state() == 1)) ? 0 : 1;
  int chain_2_dipole_index = ((particles[dipoles[0].index_q].initial_state() == 2) || (particles[dipoles[0].index_qbar].initial_state() == 2)) ? 0 : 1;
  assert(chain_1_dipole_index != chain_2_dipole_index);
  _hard_event.particles()[dipoles[chain_1_dipole_index].index_q]   .set_DIS_chain(1);
  _hard_event.particles()[dipoles[chain_1_dipole_index].index_qbar].set_DIS_chain(1);
  const Particle & p1_chain1 = (particles[dipoles[chain_1_dipole_index].index_q].is_initial_state()) ? particles[dipoles[chain_1_dipole_index].index_q]    : particles[dipoles[chain_1_dipole_index].index_qbar];
  const Particle & p2_chain1 = (particles[dipoles[chain_1_dipole_index].index_q].is_initial_state()) ? particles[dipoles[chain_1_dipole_index].index_qbar] : particles[dipoles[chain_1_dipole_index].index_q];
  _hard_event.set_incoming_ref_momenta(p1_chain1, p2_chain1, 1);
  // same for chain 2
  _hard_event.particles()[dipoles[chain_2_dipole_index].index_q]   .set_DIS_chain(2);
  _hard_event.particles()[dipoles[chain_2_dipole_index].index_qbar].set_DIS_chain(2);
  const Particle & p1_chain2 = (particles[dipoles[chain_2_dipole_index].index_q].is_initial_state()) ? particles[dipoles[chain_2_dipole_index].index_q]    : particles[dipoles[chain_2_dipole_index].index_qbar];
  const Particle & p2_chain2 = (particles[dipoles[chain_2_dipole_index].index_q].is_initial_state()) ? particles[dipoles[chain_2_dipole_index].index_qbar] : particles[dipoles[chain_2_dipole_index].index_q];
  _hard_event.set_incoming_ref_momenta(p1_chain2, p2_chain2, 2);

  // also check the scale choice - this impacts the event weight
  // if it is set to 6 or 7, we need to modify the
  // event weight because the event was generated
  // with PDFs evaluated at a different scale (Q = total invariant mass)
  // we want to evaluate the PDF for chain 1 with Q2_dis_chain1, and that likewise that for two
  double lnQ        = 0.5*log(_hard_event.Q2());
  double x_chain1   = (p1_chain1.initial_state() == 1) ? _hard_event.pdf_x(p1_chain1) : _hard_event.pdf_x(p1_chain2);
  double x_chain2   = (p1_chain1.initial_state() == 1) ? _hard_event.pdf_x(p1_chain2) : _hard_event.pdf_x(p1_chain1); 
  assert(_hoppet_runner != NULL && "Hoppet is not correctly passed to the process"); 
  double old_pdf = 1., new_pdf = 1.;
  if(_starting_scale_choice == 6){
    double lnQ12 = 0.25*log(_hard_event.Q2_dis(1) * _hard_event.Q2_dis(2));
    old_pdf = (*_hoppet_runner)(x_chain1, lnQ).flav(p1_chain1.pdgid())   * (*_hoppet_runner)(x_chain2, lnQ).flav(p1_chain2.pdgid());
    new_pdf = (*_hoppet_runner)(x_chain1, lnQ12).flav(p1_chain1.pdgid()) * (*_hoppet_runner)(x_chain2, lnQ12).flav(p1_chain2.pdgid());
  } else if (_starting_scale_choice == 7){
    double lnQ_chain1 = 0.5*log(_hard_event.Q2_dis(1));
    double lnQ_chain2 = 0.5*log(_hard_event.Q2_dis(2));
    old_pdf = (*_hoppet_runner)(x_chain1, lnQ).flav(p1_chain1.pdgid())        * (*_hoppet_runner)(x_chain2, lnQ).flav(p1_chain2.pdgid());
    new_pdf = (*_hoppet_runner)(x_chain1, lnQ_chain1).flav(p1_chain1.pdgid()) * (*_hoppet_runner)(x_chain2, lnQ_chain2).flav(p1_chain2.pdgid());
  }  
  // update the event weight
  _event_weight *= new_pdf / old_pdf;
  
  // first check the presence of partons
  bool event_is_showerable = _event_contains_partons();
  // then check the veto if needed
  if(_veto_ptr != NULL){
    event_is_showerable = !_veto_ptr->veto_event(*pythia_hard_event);
  }
  return event_is_showerable;
}

double PythiaProcessVBF::get_log_starting_scale() const {

  assert(_hard_event.size() > 0 && "Hard event does not seem to be initialised - aborting");
  const Particle & p_higgs = _hard_event[2];
  if(p_higgs.pdgid() != 25) assert(false  && "Error - found an event without a Higgs! ");
  const Particle & p_j1    = _hard_event[3];
  const Particle & p_j2    = _hard_event[4];
  double mH       = p_higgs.m();
  double pT_higgs = p_higgs.pt();
  double pT_j1    = p_j1.pt();
  double pT_j2    = p_j2.pt();
  
  double x1 = _hard_event.ref_in(1).E()/_hard_event.beam1().E();
  double x2 = _hard_event.ref_in(2).E()/_hard_event.beam2().E();

  switch (_starting_scale_choice)
  {
  case 1:
    return log(mH/2. * sqrt(pow2(mH/2.) + pow2(pT_higgs)))/2.; // powheg scale
    break;
  case 2:
    return log((_mV2 + pow2(pT_j1)) * (_mV2 + pow2(pT_j2)))/4.; // pythia's choice
    break;
  case 3:
    return log((_mV2 + pow2(pT_j1)) * (_mV2 + pow2(pT_j2)) * (pow2(mH) + pow2(pT_higgs)))/6.; // pythia's choice
    break;
  case 4: 
    return log((pT_j1 + pT_j2 + sqrt(pow2(mH) + pow2(pT_higgs)))/2.); // HT/2
    break;
  case 5:
    return log(_hard_event.Q2())/2.; // Q = invariant mass
    break;
  case 6: 
    return log(_hard_event.Q2_dis(1) * _hard_event.Q2_dis(2))/4.; // sqrt(Q1 * Q2)
    break;
  case 7:
    // start from the maximum starting scale of either of the two dipole systems
    // note that in this case pythia's event weight needs to be modified
    {
      if(_use_kinematic_limit)
        return std::max(log(_hard_event.Q2_dis(1)*(1-x1)/x1)/2.,log(_hard_event.Q2_dis(2)*(1-x2)/x2)/2.);
      else
        return std::max(log(_hard_event.Q2_dis(1))/2.,log(_hard_event.Q2_dis(2))/2.); 	
      break;
    }
    break;
  default:
    return log(_hard_event.rts());
  break;
  }
}

/////////////////////////////
// z+jet
/////////////////////////////

// ctor
PythiaProcessZj::PythiaProcessZj(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia) :
  PythiaProcess(cmdline, particle_data_ptr_pythia){}

// gets the log of the starting scale of the event for showering
double PythiaProcessZj::get_log_starting_scale() const{
  assert(_hard_event.size() > 0);
  // we take the pT of the Zboson (or equivalently, the jet) as the starting scale
  return(log(_hard_event[3].pt()));
}

// based on the channel of the hard event we select
// the right colour transitions
void PythiaProcessZj::_set_colour_transitions(vector<Dipole> * dipoles, const vector<Dipole> * non_splitting_dipoles, const vector<Particle> *particles){
  // we will always have two qg dipoles
  // for the Zj process
  // the g will either be in the initial-state, or in the final-state
  bool gluon_in_initial_state = (((*particles)[0].pdgid() == 21) || ((*particles)[1].pdgid() == 21));
  for(unsigned int dipole_idx = 0; dipole_idx < dipoles->size(); dipole_idx++){
    // retrieve the information
    Dipole & dipole = (*dipoles)[dipole_idx];
    // we will not change index_next and index_previous
    const int & index_next     = dipole.index_next;
    const int & index_previous = dipole.index_previous;
    // we start of with either a CF or CA segment, depending on whether
    // the 3-bar end is a CF
    bool threebar_end_is_CF = (index_previous >= 0);
    ColourTransitionVector colour_transition_vector(threebar_end_is_CF);
    // we now measure the distance between the CF-end and the other
    // (anti-)quark in the event
    // the eta of the transition will correspond to this opening angle
    // i.e. with an opening angle larger than this we need to transition
    // to a CA colour (as then the overal colour charge is qqbar = g in the LC limit)
    // we insert a transition point at eta = 0
    precision_type eta_transition;
    if(gluon_in_initial_state){ // qg->qZ or qbarg->qbarZ
      precision_type omc_CF_q;
      if(threebar_end_is_CF){
        omc_CF_q = one_minus_costheta((*particles)[dipole.index_3bar()], (*particles)[(*non_splitting_dipoles)[-2-index_next].index_3bar()]);
      } else{
        omc_CF_q = one_minus_costheta((*particles)[dipole.index_3()], (*particles)[(*non_splitting_dipoles)[-2-index_previous].index_3()]);
      }
      eta_transition = 0.5*log(omc_CF_q/(2.-omc_CF_q));
      // we measure from the 3bar end above, so turn
      // around when instead we measured from the 3end
      // if(!threebar_end_is_CF) eta_transition *= -1; 
      // I believe same accuracy is achieved when we use
      if(threebar_end_is_CF){
        eta_transition = min(eta_transition, 0.);
      } else{
        eta_transition = max(eta_transition, 0.);
      }
      // we need to see the impact of this choice, but I believe it is beyond our accuracy
    } else{ //qqbar -> Zg
      // get the physical rapidity of the gluon
      int sign = 1;
      // check whether the 3bar is at positive or negative rapidity in the initial state
      // we interpret 3bar at negative rap, so if it is positive we need to flip the sign
      if(threebar_end_is_CF  && ((*particles)[dipole.index_3bar()].rap() > 0)) sign = -1;
      // otherwise we are in a [-inf,CA,eta_trans,CF,inf] segment
      // if the rapidity of the quark-end is negative we should therefore flip the sign
      if(!threebar_end_is_CF && ((*particles)[dipole.index_3()].rap()    < 0)) sign = -1;
      // I believe that the transition for the dipole with an opening angle < pi/2 can be anywhere between [etag, 0]
      // this should be checked though
      // one option would then be to set
      // eta_transition = threebar_end_is_CF ? sign*(*particles)[dipole.index_3()].rap() : sign*(*particles)[dipole.index_3bar()].rap();
      // another option would be to set
      if(threebar_end_is_CF){
        eta_transition = max(sign*(*particles)[dipole.index_3()].rap(), 0.);
      } else{
        eta_transition = min(sign*(*particles)[dipole.index_3bar()].rap(), 0.);
      }
    }
    // we set the colour transition point to the back
    // this means that the colour of the first is not changed 
    colour_transition_vector.push_back(ColourTransitionPoint(eta_transition));
    // copy over the information to the dipole
    dipole.colour_transitions = std::move(colour_transition_vector);
  }
}


/////////////////////////////
// dijet
/////////////////////////////

// ctor
PythiaProcessDijet::PythiaProcessDijet(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia, PythiaProcessDijet::DijetChannel dijet_channel) :
  PythiaProcess(cmdline, particle_data_ptr_pythia){
  _dijet_channel = dijet_channel;
}

// gets the log of the starting scale of the event for showering
double PythiaProcessDijet::get_log_starting_scale() const {
  assert(_hard_event.size() > 0);
  // we take the pT of the jet as starting scale
  return(log(_hard_event[3].pt()));
}

// based on the channel of the hard event we select
// the right colour transitions
void PythiaProcessDijet::_set_colour_transitions(vector<Dipole> * dipoles, const vector<Dipole> * non_splitting_dipoles, const vector<Particle> *particles){
  // first determine the channel
  DijetChannel dijet_channel;
  // this corresponds to the all-channel case
  // we should determine the channel based
  // on the event structure
  if(_dijet_channel == DijetChannel::all){
    // check below may be removed if things are settled down
    assert(particles->size() == 4 && "Number of particles not equal to 4 - aborting");
    // we have either gg, qg, or qq' (or variants of the latter) in the initial state
    if(((*particles)[0].pdgid() == 21) && ((*particles)[1].pdgid() == 21)){
      // gg in initial, either gg->gg or gg->qqbar
      if((*particles)[2].pdgid() == 21){
        // gg->gg
        dijet_channel = DijetChannel::gg_to_gg;
      } else{
        // gg->qqbar
        dijet_channel = DijetChannel::gg_to_qqb;
      }
    } else if (((*particles)[0].pdgid() == 21) || ((*particles)[1].pdgid() == 21)){
      // gq is only option
      dijet_channel = DijetChannel::qg_to_qg;
    } else{
      // qq in the initial state, check the final state
      if (((*particles)[2].pdgid() == 21) && ((*particles)[3].pdgid() == 21)){
        // qq->gg
        dijet_channel = DijetChannel::qqb_to_gg;
      } else{
        dijet_channel = DijetChannel::qq_to_qq;
      }
    }
  } else{
    // otherwise we know the channel already
    dijet_channel = _dijet_channel;
  }

  assert(dijet_channel != DijetChannel::all);

  // now switch between the cases
  // note there is some degree of similarity between the gg->qqbar, qqbar->gg and qg->qg cases
  // however, I've not been able to group them into a single form, so I've kept
  // the cases separate below
  // this leads to some code duplication, so maybe something to look at later
  switch(dijet_channel){
    case DijetChannel::gg_to_gg:
      // easiest case: just make sure all colour connections are CA
      for(unsigned int dipole_idx = 0; dipole_idx < dipoles->size(); dipole_idx++){
        (*dipoles)[dipole_idx].colour_transitions = ColourTransitionVector(false);
      }
      break;
    case DijetChannel::gg_to_qqb:
      // here we have an s-channel gluon-propagator (CA) diagram
      // or a t-channel quark-propagator (CF) channel
      // labeling the event as g0g1->q2qbar3
      // the dipole connections may be
      // a. [0,2] [3,1] [1,0]
      // b. [3,0] [1,2] [0,1]
      // There will always be two dipoles with qg transitions
      // note that code below does not assume the labeling
      // of the particles above!
      for(unsigned int dipole_idx = 0; dipole_idx < dipoles->size(); dipole_idx++){
        // retrieve the information
        Dipole & dipole = (*dipoles)[dipole_idx];
        // we will not change index_next and index_previous
        const int & index_next     = dipole.index_next;
        const int & index_previous = dipole.index_previous;
        // two cases: gg dipole or q(qbar)g dipole
        if ((index_next >= 0) && (index_previous >= 0)){
          // gg dipole
          // declare the colour transition vector with a CA end at -infty
          ColourTransitionVector colour_transition_vector(false);
          // the transition points should be at the opening angle between the 3bar-end and
          // its auxiliary quark, and similarly for the 3-end and its auxiliary anti-quark
          precision_type omc_3bar_qbar = one_minus_costheta((*particles)[dipole.index_3bar()], (*particles)[(*dipoles)[index_next].index_3bar()]);
          precision_type eta_3bar_qbar = 0.5*log(omc_3bar_qbar/(2.-omc_3bar_qbar));
          precision_type omc_3bar_q = one_minus_costheta((*particles)[dipole.index_3bar()], (*particles)[(*dipoles)[index_previous].index_3()]);
          precision_type eta_3bar_q = 0.5*log(omc_3bar_q/(2.-omc_3bar_q));
          // the transition only exists when eta_3bar_q < eta_3_qbar
          // otherwise we just have a CA segment
          if(eta_3bar_qbar < eta_3bar_q){
            colour_transition_vector.push_back(ColourTransitionPoint(eta_3bar_qbar));
            colour_transition_vector.push_back(ColourTransitionPoint(eta_3bar_q));
          }
          // set the colour transitions in the dipole
          dipole.colour_transitions = std::move(colour_transition_vector);
        } else{
          // these are qg dipoles
          // we start of with either a CF or CA segment, depending on whether
          // the 3-bar end is a CF
          bool threebar_end_is_CF = (index_previous >= 0);
          ColourTransitionVector colour_transition_vector(threebar_end_is_CF);
          // we now measure the distance between the CF-end and the other
          // (anti-)quark in the event
          // the eta of the transition will correspond to this opening angle
          // i.e. with an opening angle larger than this we need to transition
          // to a CA colour (as then the overall colour charge is qqbar = g in the LC limit)
          // we insert a transition point at eta = 0
          precision_type omc_CF_q;
          if(threebar_end_is_CF){
            omc_CF_q = one_minus_costheta((*particles)[dipole.index_3bar()], (*particles)[(*non_splitting_dipoles)[-2-index_next].index_3bar()]);
          } else{
            omc_CF_q = one_minus_costheta((*particles)[dipole.index_3()], (*particles)[(*non_splitting_dipoles)[-2-index_previous].index_3()]);
          }
          precision_type eta_CF_q = 0.5*log(omc_CF_q/(2.-omc_CF_q));
          // we measure from the 3bar end above, so turn
          // around when instead we measured from the 3end
          if(!threebar_end_is_CF) eta_CF_q *= -1; 
          // we set the colour transition point to the back
          // this means that the colour of the first is not changed 
          colour_transition_vector.push_back(ColourTransitionPoint(eta_CF_q));
          // copy over the information to the dipole
          dipole.colour_transitions = std::move(colour_transition_vector);
        }
      }
      break;
    case DijetChannel::qq_to_qq:
      // here we always have a qq dipole
      // however, this dipole can have a CA transition
      // we label the event as q0q1->q2q3
      // note that 'q' here can also mean anti-quark
      // we leave this implicit
      // based on the event structure we can either have
      // 1. IF connections: [0,2] [1,3] (or variants) (3bar.is_initial() != 3.is_initial())
      // 2. II/FF connection: [0,1] [2,3] (or variants)
      // they can be handled in a single instance, see below
      for(unsigned int dipole_idx = 0; dipole_idx < dipoles->size(); dipole_idx++){
        // retrieve the information
        Dipole & dipole = (*dipoles)[dipole_idx];
        // declare the colour transition vector with a CF end at -infty
        ColourTransitionVector colour_transition_vector(true);
        // get the angles between the in and outgoing particles
        precision_type omc_3bar_aux = one_minus_costheta((*particles)[dipole.index_3bar()], (*particles)[(*non_splitting_dipoles)[-2-dipole.index_next].index_3bar()]);
        precision_type eta_3bar_aux = 0.5*log(omc_3bar_aux/(2.-omc_3bar_aux));
        precision_type omc_3_aux = one_minus_costheta((*particles)[dipole.index_3()], (*particles)[(*non_splitting_dipoles)[-2-dipole.index_previous].index_3()]);
        precision_type eta_3_aux = -0.5*log(omc_3_aux/(2.-omc_3_aux));  //< measured from 3-end, so additional -sign
        // check whether the colour transition should exist
        if(eta_3bar_aux < eta_3_aux){
          colour_transition_vector.push_back(ColourTransitionPoint(eta_3bar_aux));
          colour_transition_vector.push_back(ColourTransitionPoint(eta_3_aux));
        }
        // set the colour transitions in the dipole
        dipole.colour_transitions = std::move(colour_transition_vector);
      }

      break;
    case DijetChannel::qg_to_qg:
      // here we have an s-channel quark-propagator (CF) diagram
      // or a t-channel gluon-propagator (CA) channel
      // There will always be two dipoles with qg transitions
      // note that code below does not assume the labeling
      // of the particles above!
      for(unsigned int dipole_idx = 0; dipole_idx < dipoles->size(); dipole_idx++){
        // retrieve the information
        Dipole & dipole = (*dipoles)[dipole_idx];
        // we will not change index_next and index_previous
        const int & index_next     = dipole.index_next;
        const int & index_previous = dipole.index_previous;
        // two cases: gg dipole or q(qbar)g dipole
        if ((index_next >= 0) && (index_previous >= 0)){
          // gg dipole
          // declare the colour transition vector with a CA end at -infty
          ColourTransitionVector colour_transition_vector(false);
          // the transition points should be at the opening angle between the 3bar-end and
          // its auxiliary quark, and similarly for the 3-end and its auxiliary anti-quark
          precision_type omc_3bar_qbar = one_minus_costheta((*particles)[dipole.index_3bar()], (*particles)[(*dipoles)[index_next].index_3bar()]);
          precision_type eta_3bar_qbar = 0.5*log(omc_3bar_qbar/(2.-omc_3bar_qbar));
          precision_type omc_3bar_q = one_minus_costheta((*particles)[dipole.index_3bar()], (*particles)[(*dipoles)[index_previous].index_3()]);
          precision_type eta_3bar_q = 0.5*log(omc_3bar_q/(2.-omc_3bar_q));
          // the transition only exists when eta_3bar_q < eta_3_qbar
          // otherwise we just have a CA segment
          // note that sometimes we may end up with eta_3bar_q or eta_3bar_qbar
          // being infinite
          // this in itself should not matter much, it corresponds
          // to the case where the emission from the gg line
          // just is sensitive the the q/qbar in the final state
          // so then the CF segment should indeed extend to infinity
          if(eta_3bar_qbar < eta_3bar_q){
            colour_transition_vector.push_back(ColourTransitionPoint(eta_3bar_qbar));
            colour_transition_vector.push_back(ColourTransitionPoint(eta_3bar_q));
          }
          // set the colour transitions in the dipole
          dipole.colour_transitions = std::move(colour_transition_vector);
        } else{
          // these are qg dipoles
          // we start of with either a CF or CA segment, depending on whether
          // the 3-bar end is a CF
          bool threebar_end_is_CF = (index_previous >= 0);
          ColourTransitionVector colour_transition_vector(threebar_end_is_CF);
          // we now measure the distance between the CF-end and the other
          // (anti-)quark in the event
          // the eta of the transition will correspond to this opening angle
          // i.e. with an opening angle larger than this we need to transition
          // to a CA colour (as then the overall colour charge is qqbar = g in the LC limit)
          // we insert a transition point at eta = 0
          precision_type omc_CF_q;
          if(threebar_end_is_CF){
            omc_CF_q = one_minus_costheta((*particles)[dipole.index_3bar()], (*particles)[(*non_splitting_dipoles)[-2-index_next].index_3bar()]);
          } else{
            omc_CF_q = one_minus_costheta((*particles)[dipole.index_3()], (*particles)[(*non_splitting_dipoles)[-2-index_previous].index_3()]);
          }
          precision_type eta_CF_q = 0.5*log(omc_CF_q/(2.-omc_CF_q));
          // we measure from the 3bar end above, so turn
          // around when instead we measured from the 3end
          if(!threebar_end_is_CF) eta_CF_q *= -1; 
          // we set the colour transition point to the back
          // this means that the colour of the first is not changed 
          colour_transition_vector.push_back(ColourTransitionPoint(eta_CF_q));
          // copy over the information to the dipole
          dipole.colour_transitions = std::move(colour_transition_vector);
        }
      }
      break;
    case DijetChannel::qqb_to_gg:
      // here we have an s-channel gluon-propagator (CA) diagram
      // or a t-channel quark-propagator (CF) channel
      // There will always be two dipoles with qg transitions, other gg
      // loop over the dipoles
      for(unsigned int dipole_idx = 0; dipole_idx < dipoles->size(); dipole_idx++){
        // retrieve the information
        Dipole & dipole = (*dipoles)[dipole_idx];
        // we will not change index_next and index_previous
        const int & index_next     = dipole.index_next;
        const int & index_previous = dipole.index_previous;
        // two cases: gg dipole or q(qbar)g dipole
        if ((index_next >= 0) && (index_previous >= 0)){
          // gg dipole
          // declare the colour transition vector with a CA end at -infty
          ColourTransitionVector colour_transition_vector(false);
          // the transition points should be at the opening angle between the 3bar-end and
          // its auxiliary quark, and similarly for the 3-end and its auxiliary anti-quark
          precision_type omc_3bar_qbar = one_minus_costheta((*particles)[dipole.index_3bar()], (*particles)[(*dipoles)[index_next].index_3bar()]);
          precision_type eta_3bar_qbar = 0.5*log(omc_3bar_qbar/(2.-omc_3bar_qbar));
          precision_type omc_3_q       = one_minus_costheta((*particles)[dipole.index_3()], (*particles)[(*dipoles)[index_previous].index_3()]);
          precision_type eta_3_q       = -0.5*log(omc_3_q/(2.-omc_3_q)); //< measured from 3 so additional - sign
          // the transition only exists when eta_3bar_q < eta_3_q
          if(eta_3bar_qbar < eta_3_q){
            colour_transition_vector.push_back(ColourTransitionPoint(eta_3bar_qbar));
            colour_transition_vector.push_back(ColourTransitionPoint(eta_3_q));
          }
          // set the colour transitions in the dipole
          dipole.colour_transitions = std::move(colour_transition_vector);
        } else{
          // these are qg dipoles
          // we start of with either a CF or CA segment, depending on whether
          // the 3-bar end is a CF
          bool threebar_end_is_CF = (index_previous >= 0);
          ColourTransitionVector colour_transition_vector(threebar_end_is_CF);
          // we now get the physical rapidity of the gluon
          int sign = 1;
          if(threebar_end_is_CF  && ((*particles)[dipole.index_3bar()].rap() > 0)) sign = -1;
          if(!threebar_end_is_CF && ((*particles)[dipole.index_3()].rap()    < 0)) sign = -1;
          precision_type eta_g = threebar_end_is_CF ? sign*(*particles)[dipole.index_3()].rap() : sign*(*particles)[dipole.index_3bar()].rap();
          // we set the colour transition point to the back
          // this means that the colour of the first is not changed 
          colour_transition_vector.push_back(ColourTransitionPoint(eta_g));
          // copy over the information to the dipole
          dipole.colour_transitions = std::move(colour_transition_vector);
        }
      }
      break;
    default:
      throw std::runtime_error("Unknown channel - aborting");
      break;
  }
}



/// function to get a Collider from
/// a settings ptr in pythia
/// note that the user has responsibility for proper
/// memory management of the created ptr
Collider * get_collider_from_settings(Pythia8::Settings * settings_ptr){
  // we need to get the beamIDs
  int beamIDa = settings_ptr->mode("Beams:idA");
  int beamIDb = settings_ptr->mode("Beams:idB");
  // initialise the collider ptr
  Collider * collider;
  if((abs(beamIDa) == 11) && (abs(beamIDb) == 11)){
    // e+e- collider
    collider = new Collider(Collider::ColliderType::epem, settings_ptr->parm("Beams:eCM"));
  } else if ((abs(beamIDa) == 2212) && (abs(beamIDb) == 2212)){
    // pp collider 
    if(settings_ptr->flag("HiggsSM:ff2Hff(t:ZZ)") || settings_ptr->flag("HiggsSM:ff2Hff(t:WW)")){
      // watch out - this is a DIS process
      collider = new Collider(Collider::ColliderType::dis, settings_ptr->parm("Beams:eCM"));
    } else{
      collider = new Collider(Collider::ColliderType::pp, settings_ptr->parm("Beams:eCM"));
    }
  } else if (((abs(beamIDa) == 2212) && (abs(beamIDb) == 11)) || ((abs(beamIDa) == 11) && (abs(beamIDb) == 2212))){
    double e_beamA = settings_ptr->parm("Beams:eA");
    double e_beamB = settings_ptr->parm("Beams:eB");
    // now we are ready to initialise the collider
    double rts = sqrt(4.*e_beamA*e_beamB);
    collider = new Collider(Collider::ColliderType::dis, rts);
  } else{
    throw runtime_error("Colliding beam types cannot be handled");
  }
  return collider;
}


/// function to get a PythiaProcess from
/// a settings ptr in pythia
/// note that the user has responsibility for proper
/// memory management of the created ptr
PythiaProcess * get_process_from_settings(const CmdLine * cmdline,  Pythia8::ParticleData*  particle_data_ptr, Pythia8::Settings * settings_ptr){
  // only a few types of processes that can be handled right now
  PythiaProcess * process;
  if(settings_ptr->flag("WeakSingleBoson:ffbar2ffbar(s:gmZ)") || settings_ptr->flag("WeakSingleBoson:ffbar2gmZ")){
    if(abs(settings_ptr->mode("Beams:idA")) == 11){
      // e+e-
      process = new PythiaProcessee2Z(cmdline, particle_data_ptr);
    } else{
      // Z DY
      process = new PythiaProcessDY(cmdline, particle_data_ptr);
    }
  } else if (settings_ptr->flag("HardQCD:all")){
    // pp -> jj
    process = new PythiaProcessDijet(cmdline, particle_data_ptr, PythiaProcessDijet::DijetChannel::all);
  } else if (settings_ptr->flag("HardQCD:gg2gg")){
    // gg -> gg
    process = new PythiaProcessDijet(cmdline, particle_data_ptr, PythiaProcessDijet::DijetChannel::gg_to_gg);
  } else if (settings_ptr->flag("HardQCD:gg2qqbar")){
    // gg -> qqbar
    process = new PythiaProcessDijet(cmdline, particle_data_ptr, PythiaProcessDijet::DijetChannel::gg_to_qqb);
  } else if (settings_ptr->flag("HardQCD:qq2qq")){
    // qq -> qq
    process = new PythiaProcessDijet(cmdline, particle_data_ptr, PythiaProcessDijet::DijetChannel::qq_to_qq);
  } else if (settings_ptr->flag("HardQCD:qg2qg")){
    // qg -> qg
    process = new PythiaProcessDijet(cmdline, particle_data_ptr, PythiaProcessDijet::DijetChannel::qg_to_qg);
  } else if (settings_ptr->flag("HardQCD:qqbar2gg")){
    // qqb -> gg
    process = new PythiaProcessDijet(cmdline, particle_data_ptr, PythiaProcessDijet::DijetChannel::qqb_to_gg);
  } else if (settings_ptr->flag("HiggsSM:ff2Hff(t:ZZ)")){
    // VBF with t-channel Z
    process = new PythiaProcessVBF(cmdline, particle_data_ptr);
    dynamic_cast<PythiaProcessVBF*>(process)->set_mV2(pow2(particle_data_ptr->m0(23)));
  } else if (settings_ptr->flag("HiggsSM:ff2Hff(t:WW)")){
    // VBF with t-channel W
    process = new PythiaProcessVBF(cmdline, particle_data_ptr);
    dynamic_cast<PythiaProcessVBF*>(process)->set_mV2(pow2(particle_data_ptr->m0(24)));
  } else if (settings_ptr->flag("WeakBosonAndParton:qqbar2gmZg") || settings_ptr->flag("WeakBosonAndParton:qg2gmZq")){
    process = new PythiaProcessZj(cmdline, particle_data_ptr);
  } else if (settings_ptr->flag("WeakBosonExchange:ff2ff(t:gmZ)")){
    process = new PythiaProcessDIS(cmdline, particle_data_ptr);
  } else{
    throw runtime_error("This process cannot be initialised");
  }
  return process;
}



}// namespace panscales
