//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include <iostream>
#include "PanScalesPythiaAnalysisFramework.hh"

using namespace std;


////////////////////////////////////////////
/// Pythia interfaced to PanScales
/// for generating pp -> Z  events
/// stores the Z rapidity, pT and invariant mass
/// separated in Z->e+e- decays and Z->mu+mu- decays
///
/// example of a command line is 
/** 
    ./build-double/main-dy -shower panglobal -physical-coupling \
              -lhapdf-set CT14lo -nev 1e4 -out main-dy.dat
*/
///
/// Note that to run the above command line, one needs 
/// to compile the main PanScales (and pythia) code with lhapdf.
/// This can be done easily through adding '--with-lhapdf' 
/// to the build.py script arguments
///
/// For a test run without real-life PDFs (and the associated need for LHAPDF), replace
///   "-lhapdf-set CT14lo" 
/// by
///   "-pdf-choice ToyNf5Physical"
///
/// To hadronise the events, add -hadron-level
class PythiaAnalysisFrameworkDY : public PythiaAnalysisFramework {
public:
  // we may either construct the Z momentum from the leptons
  // or we may cheat on the analysis and take the Z from the pythia event record
  bool cheat_on_analysis = false;
  double mll_min = 0.;
  // ctor
  PythiaAnalysisFrameworkDY(CmdLine * cmdline_in) : PythiaAnalysisFramework(cmdline_in) {
    string cmdline_section = "Options specific to pp->Z production";
    cmdline_in->start_section(cmdline_section);
    // process information
    double rts = cmdline->value<double>("-rts", 13600.).help("Collider centre-of-mass energy");
    _pythia.readString("Beams:eCM ="+to_string(rts));
    header << "# rts = " << rts << endl;
    // set the process
    _pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
    // interference is automatically taken into account
    // turn it off via
    // WeakZ0:gmZmode = 1 (only gamma*) or = 2 (only Z)
    _pythia.readString("23:onMode = off"); // turn off decay
    _pythia.readString("23:onIfAny = 11 13"); //< only decay into leptons
    // minimal mll 
    mll_min = cmdline->value<double>("-mll-min",70.).help("Set a minimum value of the invariant mass of the lepton pair (in GeV)");
    _pythia.readString("23:mMin ="+to_string(mll_min));
    header << "# Pythia hard event with settings: \n# WeakSingleBoson:ffbar2gmZ = on\n# 23:onMode = on\n# 23:mMin = " << (mll_min) << std::endl;
    cheat_on_analysis = cmdline->present("-cheat-on-analysis").help("Use the true Z from the event record instead of reconstructing it from leptons");
    
    // initialise the PDFs when needed
    string lhapdf_set = cmdline->value<string>("-lhapdf-set", "");
    if (lhapdf_set != ""){
      _pythia.readString("PDF:pSet = LHAPDF6:" + lhapdf_set);
    }

    // choice of starting scale
    int starting_scale_choice = cmdline->value<int>("-scale-choice", 0)
         .help("Scale choice in pythia for the hard event and starting scale of the shower. "
               " Choose 0 for starting the shower at the mass of the Z boson, or 1 to start the shower at the collider CM energy");

    cmdline_in->end_section(cmdline_section);

    // now we can initialise the analysis
    init_analysis(cmdline_in);
    // set up the different starting scale choice
    f_pythia_panscales_process->set_starting_scale_choice(starting_scale_choice);
  }

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {
    // get the pT and yrap of the Z boson
    hists_err["ZpT"].declare(0., 100., 2.); //< Zpt
    hists_err["lnZpT"].declare(0., log(1000.), log(1000.)/20.); //< log of Zpt
    
    hists_err["ZeepT"].declare(0., 100., 2.); //< Zpt for Z->ee
    hists_err["lnZeepT"].declare(0., log(1000.), log(1000.)/20.); //< log of Zpt for Z->ee
    
    hists_err["ZmupT"].declare(0., 100., 2.); //< Zpt for Z->mumu
    hists_err["lnZmupT"].declare(0., log(1000.), log(1000.)/20.); //< log of Zpt for Z->mumu

    hists_err["Zrap"].declare(-4.5, 4.5, 0.5); //< rapidity of the Z boson for Z->ll
    hists_err["Zeerap"].declare(-4.5, 4.5, 0.5); //< rapidity of the Z boson for Z->ee
    hists_err["Zmurap"].declare(-4.5, 4.5, 0.5); //< rapidity of the Z boson for Z->mumu
    hists_err["Zm"].declare(mll_min, 2*mll_min, 2.); //< Z (dilepton) mass for all channels
  }
  
  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    // loop over the pythia, find electrons or muons
    std::vector<Pythia8::Particle> final_state_leptons; 

    // get the Z boson directly -> this particle will be used if we cheat on the analysis
    Pythia8::Particle Z_boson_from_event_record;
    for (int i = 0; i < _pythia.event.size(); ++i) {
      // get the Z boson from the event record
      if(_pythia.event[i].status() == -62 && _pythia.event[i].id() == 23) {
        Z_boson_from_event_record = _pythia.event[i];
      } 
      // Final state only
      if (!_pythia.event[i].isFinal()) continue;
      // Only muons or electrons
      if (_pythia.event[i].idAbs() != 11 && _pythia.event[i].idAbs() != 13)     continue;
      final_state_leptons.push_back(_pythia.event[i]);
    }

    // construct the Z boson momentum
    Pythia8::Vec4 Z_boson;
    if(cheat_on_analysis){
      Z_boson = Z_boson_from_event_record.p();
    } else{
      if(final_state_leptons.size() == 2){
        // easy situation: the leptons pair up to make the Z boson
        Z_boson = final_state_leptons[0].p() + final_state_leptons[1].p();
      } else if(final_state_leptons.size() > 2){
        // we need to do some work here
        // we implement a simple Z-finder algorithm:
        // 1. we pair up compatible leptons (opposite sign, same ID)
        // 2. we get their invariant masses
        // 3. we select the lepton pair with an invariant mass closest to the Z boson on-shell mass
        // this algorithm may fail when the Z decayed off-shell or when the leptons
        // originated from a photon exchange
        // there are probably ways to improve upon it... 

        // first get the mass of the Z boson
        double mass_Z = _pythia.particleData.m0(23);
        // get a vector that has as index the index in final_state_leptons
        // it stores a pair of the index of the lepton it is paired with, and the invariant mass
        std::vector<std::pair<int, double>> invariant_mass_and_pairs;
        // loop over all leptons in the set
        for(unsigned int lepton_idx1 = 0; lepton_idx1 < final_state_leptons.size() - 1; lepton_idx1++){
          double diff_with_Z_mass = std::numeric_limits<double>::max();
          int lepton_idx2_paired = -1;
          for(unsigned int lepton_idx2 = lepton_idx1 + 1; lepton_idx2 < final_state_leptons.size(); lepton_idx2++){
            // two leptons can only originate from a Z decay if their ids are the same
            if(final_state_leptons[lepton_idx1].idAbs() != final_state_leptons[lepton_idx2].idAbs()) continue;
            // found two electrons / muons, check that they are not same sign
            if(final_state_leptons[lepton_idx1].id() == final_state_leptons[lepton_idx2].id()) continue;
            // found opposite signed leptons, pair them up
            Vec4 paired_momentum = final_state_leptons[lepton_idx1].p() + final_state_leptons[lepton_idx2].p();
            // get the difference with the Z boson mass
            double pair_diff_with_Z_mass = abs(mass_Z - paired_momentum.mCalc());
            // store this leptonpair if it has the mimimum up until now
            if(pair_diff_with_Z_mass < diff_with_Z_mass){
              diff_with_Z_mass   = pair_diff_with_Z_mass;
              lepton_idx2_paired = lepton_idx2;
            }
          }
          invariant_mass_and_pairs.push_back(std::make_pair(lepton_idx2_paired, diff_with_Z_mass));
        }
        // now loop over invariant_mass_and_pairs and find the mimimum
        double minimum_diff_with_Z_mass = std::numeric_limits<double>::max();
        int lepton_idx1_paired          = -1;
        int lepton_idx2_paired          = -1;
        for(unsigned int lepton_idx1 = 0; lepton_idx1 < invariant_mass_and_pairs.size(); lepton_idx1++){
          // protect against cases where no paired lepton was found
          if(invariant_mass_and_pairs[lepton_idx1].first < 0) continue;
          // otherwise store if smallest difference found until now
          if(minimum_diff_with_Z_mass > invariant_mass_and_pairs[lepton_idx1].second){
            minimum_diff_with_Z_mass = invariant_mass_and_pairs[lepton_idx1].second;
            lepton_idx2_paired       = invariant_mass_and_pairs[lepton_idx1].first;
            lepton_idx1_paired       = lepton_idx1;
          }
        }
        // construct the Z boson momentum
        Z_boson = final_state_leptons[lepton_idx1_paired].p() + final_state_leptons[lepton_idx2_paired].p();
        // std::cout << "Selected the (Z0) boson with " << Z_boson << std::endl;
        // std::cout << "Real Z0 boson was " << Z_boson_from_event_record << std::endl;
    
      } else{
        // number of leptons < 2, we cannot construct a Z boson momentum
        throw runtime_error("Number of leptons is less than 2 ");
      }
    }


    // get the rapidity and pT
    double Z_pT  = Z_boson.pT();
    double Z_rap = Z_boson.rap();
    double lnZ_pT = log(Z_pT);
    // store the output to the histograms
    hists_err["ZpT"].add_entry(Z_pT, event_weight()); 
    hists_err["Zm"].add_entry(Z_boson.mCalc(), event_weight()); 
    hists_err["lnZpT"].add_entry(lnZ_pT, event_weight());
    hists_err["Zrap"].add_entry(Z_rap, event_weight());
    // store specific decay information
    if(final_state_leptons[0].idAbs() == 11){
      // electon
      hists_err["ZeepT"].add_entry(Z_pT, event_weight()); 
      hists_err["lnZeepT"].add_entry(lnZ_pT, event_weight());
      hists_err["Zeerap"].add_entry(Z_rap, event_weight());
    }
    if(final_state_leptons[0].idAbs() == 13){
      // muon
      hists_err["ZmupT"].add_entry(Z_pT, event_weight()); 
      hists_err["lnZmupT"].add_entry(lnZ_pT, event_weight());
      hists_err["Zmurap"].add_entry(Z_rap, event_weight());
    }
  }
};

//----------------------------------------------------------------------
// the main loop
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  PythiaAnalysisFrameworkDY driver(&cmdline);
  driver.run();
}
