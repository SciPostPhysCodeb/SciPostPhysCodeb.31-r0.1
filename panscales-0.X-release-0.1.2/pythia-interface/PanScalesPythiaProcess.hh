//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef __PANSCALESPYTHIAPROCESS_HH__
#define __PANSCALESPYTHIAPROCESS_HH__
#include "ProcessHandler.hh"
#include "Pythia8/Event.h"
#include "Collider.hh"
#include "VetoPythiaHardEvent.hh"
#include "MatchingHardMatrixElements.hh"
#include "ShowerRunner.hh"


namespace panscales{

/// \class PythiaProcess
/// class inspired by from the PanScales process class
/// a PythiaProcess does three things:
/// * given a pythia event, it sets up a panscales event with the correct dipole structure
///   in this process it stores the panscales hard event as private variable
/// * it sets up the auxiliary dipole momenta and colour transitions
/// * it returns a starting scale to start showering  
class PythiaProcess {
public:
  /// default ctor that does nothing
  PythiaProcess();

  /// constructor to be called if we want to initialise a process
  /// it possibly reads commandline / particle_data settings
  PythiaProcess(const CmdLine * cmdline, const Pythia8::ParticleData * particle_data_ptr_pythia);  

  /// sets up a pointer to hoppet runner
  void set_hoppet_ptr(std::shared_ptr<HoppetRunner> & hoppet_runner){
    _hoppet_runner = hoppet_runner;
  }

  /// sets up a pointer to a hard veto, the process takes ownership
  void set_veto_ptr(VetoHardEvent * veto_ptr){
    _veto_ptr = std::unique_ptr<VetoHardEvent>(veto_ptr);
  }

  /// initialises the hard ME 
  virtual void init_hard_me(ShowerRunner * shower_runner){
    throw std::runtime_error("The hard-matching matrix element is not available for this process");
  }

  /// virtual destructor as we have virtual functions
  virtual ~PythiaProcess(){};

  /// description of the process
  std::string description() const {return "Pythia process";}
  
  /// this function can provide additional information that is process specific
  /// it returns true if event can be showered, false if it needs to be vetoed
  /// it reads the pythia event and sets up a panscales event from it
  /// this pythia event can be either a hard event obtained from a hard scattering,
  /// a decayed system (i.e. Z->qqbar), or an MPI scattering
  virtual bool prepare_event_for_showering(int i_sys, Pythia8::BeamParticle* beamA_pythia, Pythia8::BeamParticle* beamB_pythia, Pythia8::PartonSystems* parton_systems_ptr_pythia, Pythia8::Event* pythia_hard_event){
    // prepare the scattering event itself
    _get_scattered_event(i_sys, beamA_pythia, beamB_pythia, parton_systems_ptr_pythia, pythia_hard_event);
    // check whether we need to shower it
    return _event_contains_partons();
  }
  /// access to the hard event
  /// note it first has to be initialised 
  Event hard_event() const {
    assert(_hard_event.size() > 0 && "Hard event has not been initialised - aborting");
    return _hard_event;
  }
  /// access to the mpi event
  /// user is responsible for checking MPI is initialised
  Event mpi_event() const {
    assert(_mpi_event.size() > 0 && "Hard event has not been initialised - aborting");
    return _mpi_event;
  }


  /// access to the event weight at the start of the shower
  double hard_event_weight() const{
    return _event_weight;
  }

  /// access to the MPI weight if MPI system was found
  /// returns 0 if no MPI system exists
  double mpi_event_weight() const{
    return _mpi_weight;
  }

  /// this takes a pythia event where an MPI system has been added
  /// this MPI system is recognised by status -31 (initial-state) and +33 (final-state)
  /// then we create a new list of panscales particles, add the colour connections,
  /// and add these to the existing event
  /// at the end of the function, we will also reset _mpi_event and _mpi_weight to 0
  /// this is done to prevent setting the MPI again
  void update_current_event_with_mpi(Event & panscales_event, std::vector<unsigned int> & _map_ps_to_py);

  /// access to the hard matching ME pointer
  MatchingHardMatrixElement * hard_me(){ return _hard_me_ptr.get();}

  /// returns the starting scale for the shower
  virtual double get_log_starting_scale() const {
    assert(_hard_event.size() > 0 && "Hard event has not been initialised - aborting");
    assert(_starting_scale_choice == 0 && "The starting scale choice is not set properly");
    return log(_hard_event.rts());
  }

  /// set the starting scale choice
  void set_starting_scale_choice(int starting_scale_choice){
    _starting_scale_choice = starting_scale_choice;
  }

  /// returns the map from the pythia idx to panscales idx
  std::vector<unsigned int> initial_map_pythia_idx_from_panscales_idx() const{
    return _map_ps_idx_to_py_idx;
  }

  /// protected variables and functions
protected:
  std::vector<unsigned int> _map_ps_idx_to_py_idx;         //< map between the particles in the pythia event and the panscales event
  double                    _event_weight;                 //< weight of the panscales hard event
  Event                     _hard_event;                   //< the panscales hard event
  double                    _mpi_weight = 0;               //< weight of the panscales MPI event (initialised to 0 if no MPI event is there)
  Event                     _mpi_event;                    //< an MPI event created from the hard scattering
  std::shared_ptr<panscales::HoppetRunner> _hoppet_runner; //< pointer to the hoppet runner used in the event, as we need access to the PDFs in some cases
  std::unique_ptr<VetoHardEvent> _veto_ptr;                //< possible hard veto pointer, used to veto the hard events
  std::unique_ptr<MatchingHardMatrixElement> _hard_me_ptr; //< gives access to the hard matrix element
  int                       _starting_scale_choice = 0;    //< choice of starting scale for the showering
  
  /// given a pythia-generated hard event this function returns 
  /// the event in panscales format, with the dipole structure correctly set up
  /// it also fills the event weight and the map between panscales indices and pythia indices
  void _get_scattered_event(int i_sys, Pythia8::BeamParticle* beamA_pythia, Pythia8::BeamParticle* beamB_pythia, Pythia8::PartonSystems* parton_systems_ptr_pythia, Pythia8::Event* pythia_hard_event);
  
  /// given a pythia hard event and a panscales list of particles, this function loops over
  /// the panscales list of particles and assigns the correct colour connections
  /// it returns the dipole structure for the panscales event
  std::vector<Dipole> _get_colour_dipoles(Pythia8::Event* pythia_hard_event, std::vector<Particle> * particles);

  /// given a list of dipoles this function initialises the non-splitting dipoles
  /// and sets up the auxiliary dipole information
  std::vector<Dipole> _get_non_splitting_dipoles(std::vector<Dipole> * dipoles);

  /// function to set up the colour transitions, may be overloaded
  /// for specific processes
  virtual void _set_colour_transitions(std::vector<Dipole> *dipoles, const vector<Dipole> * non_splitting_dipoles, const vector<Particle> *particles);


  /// function to check whether the event is showerable
  /// it checks whether there are QCD particles in the event
  /// and whether the veto ptr vetoes the hard event
  bool _event_contains_partons();

};

///////////////////////////////////////
/// process-specific implementations
///////////////////////////////////////

/// @ingroup PanScalesPythiaProcess
/// \class PythiaProcessee2Z
/// for the e+e-->Z/gamma process
class PythiaProcessee2Z : public PythiaProcess{
  public:
  PythiaProcessee2Z(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia);
  /// overwrite the hard ME 
  void init_hard_me(ShowerRunner * shower_runner) override {
    _hard_me_ptr = std::unique_ptr<MatchingHardMatrixElement>(new ZqqHardME(shower_runner->shower()->qcd(), true));
  }
  
};

/// @ingroup PanScalesPythiaProcess
/// \class PythiaProcessDY
/// for the DY process
class PythiaProcessDY : public PythiaProcess{
  public:
  PythiaProcessDY(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia);  
           
  /// gets the starting scale for the event
  double get_log_starting_scale() const override;
};

/// @ingroup PanScalesPythiaProcess
/// \class PythiaProcessDIS
/// class for DIS processes
class PythiaProcessDIS : public PythiaProcess{
  public:
  PythiaProcessDIS(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia);  
  
  /// prepares the hard event for a DIS process
  bool prepare_event_for_showering(int i_sys, 
                            Pythia8::BeamParticle* beamA_pythia, Pythia8::BeamParticle* beamB_pythia, 
                            Pythia8::PartonSystems* parton_systems_ptr, Pythia8::Event* pythia_hard_event) override;
  
  /// gets the starting scale for the event
  double get_log_starting_scale() const override;
};

/// @ingroup PanScalesPythiaProcess
/// \class PythiaProcessVBF
/// class for Higgs production via VBF
class PythiaProcessVBF : public PythiaProcess{
  public:
  PythiaProcessVBF(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia);  
  
  /// prepares the hard event for a VBF process
  bool prepare_event_for_showering(int i_sys, 
                            Pythia8::BeamParticle* beamA_pythia, Pythia8::BeamParticle* beamB_pythia, 
                            Pythia8::PartonSystems* parton_systems_ptr, Pythia8::Event* pythia_hard_event) override;
  
  /// gets the starting scale for the event
  double get_log_starting_scale() const override;

  /// set mV2
  void set_mV2(double mV2){
    _mV2 = mV2;
  }

  private:
  bool   _use_kinematic_limit;
  double _mV2;                   //< mass of the vector boson in the t-channel exchange
};

/// @ingroup PanScalesPythiaProcess
/// \class PythiaProcessZj
/// for the DY process
class PythiaProcessZj : public PythiaProcess{
  public:
  PythiaProcessZj(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia);  
           
  /// gets the starting scale for the event
  double get_log_starting_scale() const override;
  
  private:
  /// set the process-specific colour transitions 
  void _set_colour_transitions(vector<Dipole> * dipoles, const vector<Dipole> * non_splitting_dipoles, const vector<Particle> *particles) override;
};

/// @ingroup PanScalesPythiaProcess
/// \class PythiaProcessDijet
/// class for dijet processes
class PythiaProcessDijet : public PythiaProcess{
  public:

  /// the enum stores the channel of the hard event
  /// it is used in the constructor so we declare it
  /// before the constructor is declared
  enum DijetChannel{
    all,           //< include all channels
    gg_to_gg,      //< gg -> gg
    gg_to_qqb,     //< gg -> qqbar
    qq_to_qq,      //< q q'-> q q', q qbar' -> q qbar', qbar qbar' -> qbar qbar' where the '-ed flavour may equal the other one, and outflavour = inflavour
    qg_to_qg,      //< q(qbar)g -> q(qbar)g
    qqb_to_gg      //< qqbar -> gg
  };

  /// constructor, sets the channel of the hard event
  PythiaProcessDijet(const CmdLine * cmdline, Pythia8::ParticleData*  particle_data_ptr_pythia, PythiaProcessDijet::DijetChannel dijet_channel);  
           
  /// gets the starting scale for the event
  double get_log_starting_scale() const override;

  private:
  DijetChannel _dijet_channel; //< stores the dijet channel
  /// set the process-specific colour transitions 
  void _set_colour_transitions(vector<Dipole> * dipoles, const vector<Dipole> * non_splitting_dipoles, const vector<Particle> *particles) override;

};


/// function to get a Collider from
/// a settings ptr in pythia
Collider * get_collider_from_settings(Pythia8::Settings * settings_ptr);

/// function to get a Process from
/// a settings ptr in pythia
PythiaProcess * get_process_from_settings(const CmdLine * cmdline,  Pythia8::ParticleData*  particle_data_ptr, Pythia8::Settings * settings_ptr);

}
#endif // __PANSCALESPYTHIAPROCESS_HH__
