//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef __PANSCALES_PYTHIA_ANALYSISFRAMEWORK_HH__
#define __PANSCALES_PYTHIA_ANALYSISFRAMEWORK_HH__

#include "AnalysisFramework.hh"
#include "PanScalesPythiaModule.hh"  //< needed to set pythia
#include "PanScalesPythiaProcess.hh" //< needed to set the process

/// class for setting up pythia events
/// derived from the AnalysisFramework class of PanScales
class PythiaAnalysisFramework : public panscales::AnalysisFramework {
public:
  /// ctor - only sets the git info
  /// initialisation of the shower etc gets done in init_shower_qcd_pdf_runner
  PythiaAnalysisFramework(CmdLine * cmdline_in);

  /// initialise everything in one go
  void init_analysis(CmdLine * cmdline_in);

  /// in post_startup we output
  /// some additional header information and
  /// declare the histograms
  virtual void post_startup() override;

  /// This function initialises:
  /// * showerrunner with a shower and QCD instance;
  /// * hoppetrunner - if PDFs are needed, also calls the initialise of hoppetrunner;
  /// * sets the Pythia shower to either a PythiaPanScalesTime shower, or the native pythia shower
  /// Note that the Panscales showers are initialised using information
  /// of the collider - this means that the collider needs to be set
  /// before calling this function. An exception will be thrown if this
  /// has not been done. 
  /// This function sets _use_native_pythia to false if a panscales shower is used
  void init_shower_qcd_pdf_runner(CmdLine * cmdline_in);

  /// initialises the tuning parameters in pythia
  void init_tune();
  
  /// sets the matching functionality in panscales
  void set_matching(panscales::MatchingHardMatrixElement * hard_me);

  /// access to whether pythia's shower is used
  bool use_native_pythia() {return _use_native_pythia;}

  /// tuning settings
  void set_vincia_tune();

  /// generates the next event with _pythia.next()
  /// this generates a new setup for the Born level event
  /// and subsequently showers the generated event
  /// it also fills the multiplicity at parton / hadron level
  /// and the cross section
  virtual bool generate_event() override;

  /// weight factor to get cross sections in nb (Pythia gives cross sections in mb)
  double weight_factor() const override {return 1e6*_pythia.info.sigmaGen()/(_pythia.info.weightSum());}
  std::string units_string() const override {return "nb";}

  /// pointer to the pythia panscales process
  std::shared_ptr<panscales::PythiaProcess> f_pythia_panscales_process; //< stores a pythia-panscales process instance

  protected:
  // runner for pythia
  Pythia8::Pythia _pythia;
  bool _use_native_pythia;     //< records whether native pythia is used as the shower
  bool _do_match_shower;       //< determines whether we need to match the shower
  // settings for hadron production, MPI, beam remnants
  bool _hadron_level;          //< sets whether we are hadronising the system
  bool _include_mpi;           //< sets whether we are including MPI
  bool _include_beam_remnants; //< sets inclusion of beam remnants
  // store the values in the command line
  AverageAndErrorWithRef & _hadronlevel_multiplicity_unweighted = averages["hadronlevel_multiplicity_unweighted"];
  AverageAndErrorWithRef & _hadronlevel_multiplicity_weighted   = averages["hadronlevel_multiplicity_weighted"];
};


#endif // __PANSCALES_PYTHIA_ANALYSISFRAMEWORK_HH__
