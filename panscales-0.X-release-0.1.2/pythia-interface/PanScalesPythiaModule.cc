//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include "PanScalesPythiaModule.hh"
#include <ShowerFromCmdLine.hh>
#include <sstream>
#include <string>
#include <regex>

using namespace std;
using namespace Pythia8;
// #define PSVERBOSE

PythiaPanScales::~PythiaPanScales() {
  cout << endl;
  cout << "========== BEWARE BEWARE BEWARE BEWARE =============" << endl;
  cout << " The PanScales-Pythia interface is preliminary and  " << endl;
  cout << " should be used with extreme caution.               " << endl;
  cout << " Report problems to the PanScales authors.          " << endl;
  cout << "========== BEWARE BEWARE BEWARE BEWARE =============" << endl;
  cout << endl;
}

// constructor
PythiaPanScalesTime::PythiaPanScalesTime(){
  _shower_runner = nullptr;
}

// constructor with information, typically from the analysis framework
PythiaPanScalesTime::PythiaPanScalesTime(std::shared_ptr<panscales::ShowerRunner>  & shower_runner, 
                                         std::shared_ptr<panscales::HoppetRunner>  & hoppet_runner,
                                         std::shared_ptr<panscales::PythiaProcess> & pythia_panscales_process){   
  _shower_runner = shower_runner;
  _hoppet_runner = hoppet_runner;
  _pythia_panscales_process = pythia_panscales_process;
}

// init function
void PythiaPanScalesTime::init(BeamParticle* beamA, BeamParticle* beamB){
  #ifdef PSVERBOSE
  std::cout << " In init" << std::endl;
  #endif //PSVERBOSE

  // initialise things if not done already
  if (! _shower_runner){
    // add a setting so that we can set which of the PanScales shower to run

    // add a setting to cache the matching pointer
  
    // make sure Pythia uses massless partons
    for (int i=1; i<=5; i++)
      particleDataPtr->m0(i, 0);

    // MPI is currently not supported. Complain if instructed otherwise
    if (flag("PartonLevel:MPI")){
      assert(false && "MPI currently not supported with the PanScales showers");
    }

    // get the settings
    CmdLine cmdline_for_shower = _cmdline_from_settings();

    // get an output
    ostringstream header;

    // get a collider, process and shower
    std::unique_ptr<panscales::Collider> collider(panscales::get_collider_from_settings(settingsPtr));
    _pythia_panscales_process.reset(panscales::get_process_from_settings(&cmdline_for_shower, particleDataPtr, settingsPtr));
    _shower_runner.reset(panscales::create_showerrunner_with_shower_and_qcd_instance(cmdline_for_shower, header, *collider));

    // handle PDFs if needed
    _hoppet_runner.reset(panscales::create_hoppetrunner(cmdline_for_shower, header, collider->has_pdfs()));
    // pass the hoppetrunner instance to showerrunner
    panscales::init_hoppetrunner_in_showerrunner(cmdline_for_shower, _shower_runner.get(), _hoppet_runner.get(), log(collider->rts()));
    
    if (collider->has_pdfs()) {
      // initialise the hoppet runner (sets the scales and nloops correctly)
      _hoppet_runner->initialise(_shower_runner->shower()->qcd());
      // add output information
      header << "# Hoppet: " << _hoppet_runner->description() << endl;
      // set up the hoppet runner in the process
      _pythia_panscales_process->set_hoppet_ptr(_hoppet_runner);
    }
    // initialise the matching if needed
    if(cmdline_for_shower.present("-match-process")){
      // initialise the shower
      panscales::set_shower_for_matching(cmdline_for_shower, _shower_runner.get(), *collider);
      // initialise the process
      _pythia_panscales_process->init_hard_me(_shower_runner.get());
      // initialise the matching
      _shower_runner->set_matching(new panscales::MatchedProcess(
                                      _pythia_panscales_process->hard_me(),
                                      _shower_runner->shower(),
                                      _shower_runner->shower_for_matching(), _shower_runner->strategy(),
                                      panscales::PowhegStyle, true, false));
      header << "# Match process (PowHegStyle)" << std::endl;      
    }
  }
  
  // reset emission info, used in the run
  _emission_info.reset(_shower_runner->shower()->create_emission_info());
}

void PythiaPanScalesTime::prepare(int iSys, Event& event, bool){
  // prepare gets called every time a subsystem of pythia gets showered
  #ifdef PSVERBOSE
  std::cout << "In prepare" << std::endl;
  std::cout << "Size of event out " << partonSystemsPtr->sizeOut(iSys) << std::endl;
  event.list();
  #endif // PSVERBOSE
  
  // find a hard/MPI scattering event 
  _veto_hard_event          = !_pythia_panscales_process->prepare_event_for_showering(iSys, beamAPtr, beamBPtr,  partonSystemsPtr, &event);
  // check whether an MPI was found
  // if not, we need to prepare the hard event for showering
  bool get_new_starting_scale = !_veto_hard_event;
  if((_pythia_panscales_process->mpi_event_weight() == 0) && !_veto_hard_event){
    prepare_hard_event_for_showering(iSys, event);
  } else if (!_veto_hard_event){
    // when MPI interactions are allowed, pythia will call prepare of all the showers
    // to update the dipoles
    // in this case, we therefore need to add the new MPI dipoles to the already existing event
    // and update the _map_ps_to_py accordingly
    _pythia_panscales_process->update_current_event_with_mpi(_event, _map_ps_to_py);
    // the number of dipoles now has increased outside of the knowledge
    // of showerrunner
    // we therefore need to update showerrunner with all the current elements
    // we have
    // note that we are NOT updating the spin correlation tree 
    // with these new partons
    // we should maybe think about what needs doing there...
    // if MPI is found we should not get a new starting scale for the shower
    get_new_starting_scale = false;
    // an issue that we face is that we now have more than 1 set of initial-state partons
    // this requires some thinking on how to handle the PDFs!
    // we need to update showerrunner, as now more dipoles are added to the event
    // this requires a new initialisation of showerrunner (maybe)...
  }
  #ifdef PSVERBOSE
  std::cout << "Got the following event from the pythia event:" << std::endl;
  _event.print_following_dipoles();
  std::cout << _event << std::endl;
  #endif //PSVERBOSE
  
  // only get a starting scale when we have a new hard system, otherwise we need to continue
  if (get_new_starting_scale){
    // set up the shower
    _lnv    = _pythia_panscales_process->get_log_starting_scale();
    _lnvmax = _lnv;
    _lnvmin = _lnv - (1 + _shower_runner->shower()->beta()) * (_lnv - _shower_runner->shower()->qcd().lnkt_cutoff());
    #ifdef PSVERBOSE
    cout << "Starting scale set at " << _lnv << ", lnvmin = " << _lnvmin << std::endl;
    #endif //PSVERBOSE
  }
  if(!_veto_hard_event){
    _shower_runner->initialise_run(_event, _lnv);
    #ifdef PSVERBOSE
    cout <<  "Initialised shower run " << std::endl;
    #endif //PSVERBOSE
    _nemsn = 0;
  } else{
    // make sure the histograms are not filled if we need to veto the event
    _event_weight = 0; 
  }
}

// prepares the hard event for showering
void PythiaPanScalesTime::prepare_hard_event_for_showering(int iSys, Event& event){
  _event                    = _pythia_panscales_process->hard_event();
  _map_ps_to_py             = _pythia_panscales_process->initial_map_pythia_idx_from_panscales_idx();
  _event_weight             = _pythia_panscales_process->hard_event_weight();
}

double PythiaPanScalesTime::pTnext(Event& event, double pTbegAll, double pTendAll, bool, bool) {
  #ifdef PSVERBOSE
  std::cout << "pTbegAll " << pTbegAll << " pTendAll " << pTendAll << std::endl;
  #endif // PSVERBOSE
  // check if this needs to run
  #ifdef PSVERBOSE
  std::cout << "In pTnext" << std::endl;
  #endif // PSVERBOSE

  // only relevant if we produce quarks for FSR
  // always for ISR
  if (_veto_hard_event) return 0.0;
  
  // Generate a candidate lnv + emitter from the event ensemble
  _element = _shower_runner->find_next_element_and_lnv(_event, _lnv);
  _emission_info->set_element(_element);
  _emission_info->lnv = _lnv;
  #ifdef PSVERBOSE
  std::cout << "next lnv is found at " << _lnv << std::endl;
  #endif // PSVERBOSE

  if (_lnv<_lnvmin){
    return 0.0; //< stops showering in Pythia8
  }

  return exp(_lnv);
  // 09-10-2023 query on MPI:
  // to interleave with Pythia's MPI system
  // we return the transverse momentum of the emission
  // pythia will then take this scale as the branching scale, and
  // see whether it needs to add MPI
  // for this we need a function that returns the max lnkt approx
  // we will obtain for the selected element
  // return exp(_element->max_lnkt_approx(_lnv));
  // this needs to be the factorisation scale of the PDFs
}

bool PythiaPanScalesTime::branch(Event& event, bool) {
  double lnv = _emission_info->lnv;
  #ifdef PSVERBOSE
  std::cout << "In branch" << std::endl;
  #endif // PSVERBOSE
  
  // get the next lnb
  double lnb;
  // the find_next_lnb function does two things:
  // * it updates the elements in showerrunner given the new event, lnv information
  // * it samples an lnb based on the algorithm the user wanted to use
  if (!_shower_runner->find_next_lnb(_event, _element, lnv, lnb)){
    return false;
    #ifdef PSVERBOSE
      std::cout << "no new lnb found" << std::endl;
    #endif // PSVERBOSE
  }
  _emission_info->lnb = lnb;
  #ifdef PSVERBOSE
    std::cout << "next lnb is found at " << lnb << std::endl;
  #endif // PSVERBOSE

  // first try to see if the emission is accepted
  double dummy_weight = 1.0;
  // when the action is not "accept", we will need to make sure that the
  // action is "veto_emission"; go via a lambda because we need to check
  // it in two places.
  auto validate_action = [](panscales::EmissionAction action){
    if (action != panscales::EmissionAction::veto_emission) {
      ostringstream oss; oss << action;
      throw std::runtime_error("Branching action is not accept or emission_veto, but instead " + oss.str());      
    }
  };

  panscales::EmissionAction action = _shower_runner->do_accept_emission(_event, _emission_info, false, dummy_weight);
  if (action != panscales::EmissionAction::accept) {
    validate_action(action);
    #ifdef PSVERBOSE
    std::cout << "Emission is not accepted" << std::endl;
    #endif // PSVERBOSE
    return false;
  }
  #ifdef PSVERBOSE
  std::cout << "Emission is accepted - moving on to processing the emission" << std::endl;
  #endif // PSVERBOSE
  
  // gather some info. The last line should be done once the
  // emission has been accepted in case matching changed things
  unsigned int i1_ps        = _element->emitter_index();
  unsigned int i2_ps        = _element->spectator_index();
  bool emitter_was_3bar_end = _element->emitter_is_3bar_end_of_dipole();
  bool emitter_splits       = _emission_info->do_split_emitter;
  if ((++_nemsn)==1)
    _shower_runner->set_lnvfirst(_emission_info->lnv);

  // do all the necessary updates in panscales
  action = _shower_runner->do_process_emission(_event, _emission_info, false);
  if (action !=panscales::EmissionAction::accept) {
    validate_action(action);
    return false;
  }
  
  // update the pythia event
  update_pythia_event(i1_ps, i2_ps, emitter_was_3bar_end, emitter_splits, event);

  #ifdef PSVERBOSE
    event.list();
    _event.print_following_dipoles();
    std::cout << "...done" << std::endl;
  #endif // PSVERBOSE
  return true;
}

// update the pythia event given an updated panscales event
void PythiaPanScalesTime::update_pythia_event(unsigned int emitter_index, unsigned int spectator_index, bool emitter_was_3bar_end, bool emitter_splits, Event & event){
  // determine whether we deal with an II, IF, FI or FF dipole
  // this information is used later when setting the new mother-daughter relations
  bool emitter_is_initial   = _event[emitter_index]  .is_initial_state();
  bool spectator_is_initial = _event[spectator_index].is_initial_state();
  #ifdef PSVERBOSE
    if((emitter_is_initial && spectator_is_initial)) std::cout << "II" << std::endl;
    else if((emitter_is_initial))                    std::cout << "IF" << std::endl;
    else if((spectator_is_initial))                  std::cout << "FI" << std::endl;
    else                                             std::cout << "FF" << std::endl;
  #endif // PSVERBOSE
  // the updates are written such that for an IF dipole, the emitter is defined as initial, and spectator as final
  // therefore if we find an FI dipole, just switch around the information
  if(spectator_is_initial && !emitter_is_initial){
    emitter_is_initial   = true;
    spectator_is_initial = false;
    emitter_was_3bar_end = !emitter_was_3bar_end;
    emitter_splits       = !emitter_splits;
    std::swap(emitter_index, spectator_index);
  }
  // get the three (updated) particles of the dipole in the panscales event
  const panscales::Particle &emitter   = _event[emitter_index];
  const panscales::Particle &spectator = _event[spectator_index];
  const panscales::Particle &radiated  = _event[_event.size()-1]; //< radiated particle is always the last

  ///////////////////////////////////////////////////
  // update the particle content for the pythia event
  
  // 1. retrieve the emitter and spectator in the Pythia event
  unsigned int emitter_idx_py   = _map_ps_to_py[emitter_index];
  unsigned int spectator_idx_py = _map_ps_to_py[spectator_index];
  // get the current system, store for later use
  int i_sys = partonSystemsPtr->getSystemOf(emitter_idx_py, true);
  
  // 2. set the status codes:
  // - 51 for either emitter and radiation, or any leg in an antenna shower
  // - 52 for the recoiler in a dipole shower
  // - 41 for incoming emitting particle (negative for initial state)
  // - 42 incoming copy of recoiler in dipole shower 
  // - 43 outgoing produced by initial-state branching (positive as final-state)
  int status_emitter   = emitter_is_initial   ? -41 : 51;
  int status_spectator = spectator_is_initial ? ((_shower_runner->shower()->only_emitter_splits()) ? -42 : -41) : (_shower_runner->shower()->only_emitter_splits()) ? 52 : 51;
  int status_radiated  = emitter_is_initial   ? 43  : 51;
  
  // 3. get the updated colour connections
  vector<pair<int, int>> new_colour_indices = get_new_colour_indices(emitter_index, spectator_index, emitter_was_3bar_end, emitter_splits, event);
  
  // 4. get the mother / daughter information for the emitter / spectator
  // this partially will be updated later
  // check whether the mother of the emitter is a beam
  bool sideA_emitter = (event[emitter_idx_py].mother1() == 1);
  // the mother of the emitter is the beam, or the old emitter
  int mother_emitter   = emitter_is_initial   ? (sideA_emitter ? 1 : 2) : emitter_idx_py;
  // the mother of the spectator is the other beam, or the old spectator
  int mother_spectator = spectator_is_initial ? (sideA_emitter ? 2 : 1) : spectator_idx_py;
  // the daughters are the emitted partons for initial-state splits, or 0 for final-state splits
  int daughter_emitter   = emitter_is_initial   ? emitter_idx_py   : 0;
  int daughter_spectator = spectator_is_initial ? spectator_idx_py : 0;
  
  // 5. add the emitter and spectator to the event
  int emitter_idx_py_new   = event.append(emitter.pdgid()  , status_emitter  , mother_emitter,     0, daughter_emitter,   daughter_emitter  , new_colour_indices[0].first, new_colour_indices[0].second, emitter.px(), emitter.py(), emitter.pz(), emitter.E(), 0.0); 
  int spectator_idx_py_new = event.append(spectator.pdgid(), status_spectator, mother_spectator,   0, daughter_spectator, daughter_spectator, new_colour_indices[1].first, new_colour_indices[1].second, spectator.px(), spectator.py(), spectator.pz(), spectator.E(), 0.0); 
  
  // 6. add the radiated particle to the event
  // this particle only has mothers (2 in the case of an antenna shower)
  int mother1_radiated, mother2_radiated;
  if(emitter_is_initial && emitter_splits){
    // II or IF dipoles
    mother1_radiated = emitter_idx_py_new;
    if(spectator_is_initial)
      mother2_radiated = (_shower_runner->shower()->only_emitter_splits()) ? mother1_radiated : spectator_idx_py_new;
    else
      mother2_radiated = (_shower_runner->shower()->only_emitter_splits()) ? 0 : spectator_idx_py;
  } else if (spectator_is_initial && !emitter_splits){
    // II dipoles
    mother1_radiated = spectator_idx_py_new;
    mother2_radiated = (_shower_runner->shower()->only_emitter_splits()) ? mother1_radiated : emitter_idx_py_new;
  } else if (!emitter_splits && emitter_is_initial){
    // IF dipoles
    mother1_radiated = spectator_idx_py;
    mother2_radiated = (_shower_runner->shower()->only_emitter_splits()) ? 0 : emitter_idx_py_new;
  } else{
    // FF dipoles
    mother1_radiated = emitter_idx_py;
    mother2_radiated = (_shower_runner->shower()->only_emitter_splits()) ? 0 : spectator_idx_py;
    // the line below is needed for one of pythia's checks
    // somehow it crashes when the mother1 mother2 are ordered differently
    // I believe this might be a bug / inconsistency in pythia
    if(!(_shower_runner->shower()->only_emitter_splits()) && (mother1_radiated>mother2_radiated)) std::swap(mother1_radiated, mother2_radiated);
  }
  int radiated_idx_py_new = event.append(radiated.pdgid(), status_radiated, mother1_radiated, mother2_radiated, 0, 0, new_colour_indices[2].first, new_colour_indices[2].second, radiated.px(), radiated.py(), radiated.pz(), radiated.E(), 0.0);
 
  // 7. update the emitter and spectator now that we have an index
  // update the daughter indices of the emitter and spectator
  // (in case these are initial-state partons)
  if (emitter_splits && emitter_is_initial){
    // initial-state emitter has new daughters
    event[emitter_idx_py_new].daughters(radiated_idx_py_new, daughter_emitter);
    // set the mothers of the old initial-state particles
    event[emitter_idx_py].mothers(emitter_idx_py_new, emitter_idx_py_new);
    // and update the spectators (either initial-state or final)
    if(spectator_is_initial){ 
      // case of II dipole: spectator has a new mother
      event[spectator_idx_py].mothers(spectator_idx_py_new, spectator_idx_py_new);
      if(!_shower_runner->shower()->only_emitter_splits()) // update daughters if we have an antenna shower
        event[spectator_idx_py_new].daughters(radiated_idx_py_new, daughter_spectator);
    }
    else{ 
      // case of IF dipole: spectator has a new daughter
      if(_shower_runner->shower()->only_emitter_splits())
        event[spectator_idx_py].daughters(spectator_idx_py_new, spectator_idx_py_new);
      else
        event[spectator_idx_py].daughters(spectator_idx_py_new, radiated_idx_py_new);
    }
  } else if (!emitter_splits && spectator_is_initial){
    // we should only reach here for II dipoles
    assert(emitter_is_initial);
    // initial-state spectator has new daughters
    event[spectator_idx_py_new].daughters(radiated_idx_py_new, daughter_spectator);
    // set the mothers of the old initial-state particles
    event[spectator_idx_py].mothers(spectator_idx_py_new, spectator_idx_py_new);
    event[emitter_idx_py].mothers(emitter_idx_py_new, emitter_idx_py_new);
    // update daughters of the emitter in case of antenna shower
    if(!_shower_runner->shower()->only_emitter_splits()) 
      event[emitter_idx_py_new].daughters(radiated_idx_py_new, daughter_emitter);
  } else if(!emitter_splits && emitter_is_initial){
    // should only reach here for IF dipoles
    assert(!spectator_is_initial);
    // initial-state particle has new daughters
    if(_shower_runner->shower()->only_emitter_splits())
      event[emitter_idx_py_new].daughters(emitter_idx_py, emitter_idx_py);
    else
      event[emitter_idx_py_new].daughters(radiated_idx_py_new, emitter_idx_py);
    // set the mothers of the old initial-state particles
    event[emitter_idx_py].mothers(emitter_idx_py_new, emitter_idx_py_new);
    // final-state spectator needs a new daughter
    event[spectator_idx_py].daughters(spectator_idx_py_new, radiated_idx_py_new);
   } else {
    // always a FF dipole
    assert(!emitter_is_initial && !spectator_is_initial);
    // final-state emitter has two daughters
    // dipole shower (py1 is the emitter and has 2 daughters)
    // look out for the ordering of the daughters here: first the radiated index, then the emitter
    // if this ordering is done incorrectly, the pythia check mother-daughter will fail
    event[emitter_idx_py]  .daughters(radiated_idx_py_new, emitter_idx_py_new);
    // for an antenna shower the spectator has two daughters
    // for a dipole shower only one
    if (_shower_runner->shower()->only_emitter_splits())
      event[spectator_idx_py].daughters(spectator_idx_py_new, spectator_idx_py_new); 
    else 
      event[spectator_idx_py].daughters(radiated_idx_py_new, spectator_idx_py_new);
  } 
  // set the status to negative for the old emitter and spectator
  event[emitter_idx_py].statusNeg();
  event[spectator_idx_py].statusNeg();

  // 8. update the parton system 
  partonSystemsPtr->replace(i_sys, emitter_idx_py, emitter_idx_py_new);
  partonSystemsPtr->replace(i_sys, spectator_idx_py, spectator_idx_py_new);
  partonSystemsPtr->addOut(i_sys, radiated_idx_py_new);
  
  // 9. update the other momenta of the pythia event
  update_pythia_momenta(i_sys, emitter_index, spectator_index, event);

  // 10. update the beams if needed
  // we have initial-state particles if there is a PDF for them
  if(_hoppet_runner != NULL){ 
    // update the beams
    assert((_event[0].is_initial_state() && _event[1].is_initial_state()) && "This code assumes that the particles 0 and 1 are initial-state particles");
    // check what type of splitting it is
    unsigned int i_sideA, i_sideB;
    if(emitter_is_initial && spectator_is_initial){
      i_sideA = sideA_emitter ? emitter_idx_py_new   : spectator_idx_py_new;
      i_sideB = sideA_emitter ? spectator_idx_py_new : emitter_idx_py_new;
    } else if (emitter_is_initial){
      i_sideA = sideA_emitter ? emitter_idx_py_new : _map_ps_to_py[0];
      i_sideB = sideA_emitter ? _map_ps_to_py[1]   : emitter_idx_py_new;
    } else{
      i_sideA = _map_ps_to_py[0];
      i_sideB = _map_ps_to_py[1];
    }
    // for pythia the initial-state particles are always #1 and #2
    event[1].daughters(i_sideA, 0);
    event[2].daughters(i_sideB, 0);
    // update the beam system
    update_beam_system(i_sys, i_sideA, i_sideB, event);
    // update partonic system and cm energy
    update_parton_system(i_sys, i_sideA, i_sideB, event);  
  } 

  // 11. update the mapping between pythia <-> panscales
  // this needs to be done at the end, cannot be done before
  _map_ps_to_py[emitter_index]   = emitter_idx_py_new;
  _map_ps_to_py[spectator_index] = spectator_idx_py_new;
  _map_ps_to_py.push_back(radiated_idx_py_new);
}

// get new colour indices
// returns a vector of colour, anticolour indices
// where first entry = emitter
//       second      = spectator
//       third       = radiated particle
vector<pair<int, int>> PythiaPanScalesTime::get_new_colour_indices(unsigned int emitter_index, unsigned int spectator_index, bool emitter_was_3bar_end, bool emitter_splits, Event & event){
  // determine whether we deal with an II, IF, FI or FF dipole
  bool emitter_is_initial   = _event[emitter_index]  .is_initial_state();
  bool spectator_is_initial = _event[spectator_index].is_initial_state();

  // get the three (updated) particles of the dipole in the panscales event
  const panscales::Particle &emitter   = _event[emitter_index];
  const panscales::Particle &spectator = _event[spectator_index];
  const panscales::Particle &radiated  = _event[_event.size()-1]; //< radiated particle is always the last

  // get the two original particles from pythia
  const Particle &emitter_old   = event[_map_ps_to_py[emitter_index]];
  const Particle &spectator_old = event[_map_ps_to_py[spectator_index]];
  
  // declare the vector that stores the output
  vector<pair<int, int>> col_acol_indices(3, std::make_pair(-1,-1));
  //                                       emit(0)  spec(1)  rad(2)


  // note that pythia follows the convention that an initial-state ubar caries
  // an anti-colour index. That means that the anti-colour of an initial-state
  // parton needs to be tied up with the anti-colour of a final-state particle
  // This is opposite in the PanShowers, so one needs to proceed carefully 

  // we distinguish the cases:
  // * radiated == gluon (always needs new colour index)
  //   - emission happened from 3 or 3bar
  // * radiated == quark
  //   - initial-state was quark, goes to inital-state gluon : needs a new colour index
  //   - initial-state was gluon, goes to inital-state quark : break the existing colour flow
  //   - final-state gluon emits: break existing colour flow
  // check the PDG ID of the radiated particle
  if(radiated.pdgid() == 21){ // we radiated a gluon
    // we need an new colour index
    int new_colour_index = event.nextColTag();
    // check whether the emitter or spectator splits
    if(emitter_splits){
      // spectator stays the same
      col_acol_indices[1].first  = spectator_old.col();
      col_acol_indices[1].second = spectator_old.acol();
      // check which end splits
      if(emitter_was_3bar_end){
        // the split happened at the anti-quark end for FS, quark end for IS
        col_acol_indices[0].first  = emitter_is_initial ? new_colour_index   : emitter_old.col();
        col_acol_indices[0].second = emitter_is_initial ? emitter_old.acol() : new_colour_index;
        col_acol_indices[2].first  = new_colour_index;
        col_acol_indices[2].second = emitter_is_initial ? emitter_old.col()  : emitter_old.acol(); 
      } else{
        // the split happened at the quark end for FS, anti-quark end for IS
        col_acol_indices[0].first  = emitter_is_initial ? emitter_old.col()  : new_colour_index;
        col_acol_indices[0].second = emitter_is_initial ? new_colour_index   : emitter_old.acol();
        col_acol_indices[2].first  = emitter_is_initial ? emitter_old.acol() : emitter_old.col();
        col_acol_indices[2].second = new_colour_index;
      }
    } else{ 
      // the emitter stays the same
      col_acol_indices[0].first  = emitter_old.col();
      col_acol_indices[0].second = emitter_old.acol();
      // check which end splits
      if(emitter_was_3bar_end){ // spectator was at 3end
        // the split happened at the quark end for FS, anti-quark end for IS
        col_acol_indices[1].first  = spectator_is_initial ? spectator_old.col()  : new_colour_index  ;
        col_acol_indices[1].second = spectator_is_initial ? new_colour_index     : spectator_old.acol();
        col_acol_indices[2].first  = spectator_is_initial ? spectator_old.acol() : spectator_old.col();  
        col_acol_indices[2].second = new_colour_index;
      } else{ // spectator was at 3bar end
        // the split happened at the anti-quark end for FS, quark end for IS
        col_acol_indices[1].first  = spectator_is_initial ? new_colour_index     : spectator_old.col();
        col_acol_indices[1].second = spectator_is_initial ? spectator_old.acol() : new_colour_index;
        col_acol_indices[2].first  = new_colour_index;
        col_acol_indices[2].second = spectator_is_initial ? spectator_old.col()  : spectator_old.acol(); 
      }
    }
  } else{ // we radiated a quark
    // if both the emitter and spectator were final state
    // we need to consider g->qqbar (i.e. we break the colour flow -> no new index)
    if ((emitter.pdgid() == 21) && emitter_splits){ 
      // this corresponds to an (anti-)quark emitter backwards evolving to gluon
      // we need a new colour index
      int new_colour_index = event.nextColTag();
      // spectator stays the same 
      col_acol_indices[1].first  = spectator_old.col();
      col_acol_indices[1].second = spectator_old.acol();
      // emitter_was_3bar_end  == initial-state quark backwards splitting      (final-state anti-quark) 
      // !emitter_was_3bar_end == initial-state anti-quark backwards splitting (final-state quark) 
      col_acol_indices[0].first  = emitter_was_3bar_end ? emitter_old.col() : new_colour_index;
      col_acol_indices[0].second = emitter_was_3bar_end ? new_colour_index  : emitter_old.acol();
      col_acol_indices[2].first  = emitter_was_3bar_end ? 0                 : new_colour_index;
      col_acol_indices[2].second = emitter_was_3bar_end ? new_colour_index  : 0;
    } else if ((spectator.pdgid() == 21) && !emitter_splits){ 
      // this corresponds to an (anti-)quark spectator backwards evolving to gluon
      // we need a new colour index
      int new_colour_index = event.nextColTag();
      // emitter stays the same 
      col_acol_indices[0].first  = emitter_old.col();
      col_acol_indices[0].second = emitter_old.acol();
      // emitter_was_3bar_end  == initial-state anti-quark backwards splitting (final-state quark) 
      // !emitter_was_3bar_end == initial-state quark backwards splitting      (final-state anti-quark) 
      col_acol_indices[1].first  = emitter_was_3bar_end ? new_colour_index     : spectator_old.col();
      col_acol_indices[1].second = emitter_was_3bar_end ? spectator_old.acol() : new_colour_index;
      col_acol_indices[2].first  = emitter_was_3bar_end ? new_colour_index     : 0;
      col_acol_indices[2].second = emitter_was_3bar_end ? 0                    : new_colour_index;
    } else{ 
      // case where initial-state gluon backwards evolves into qqbar
      // or final-state gluon emits a qqbar
      if (emitter_splits){
        // spectator stays the same
        col_acol_indices[1].first  = spectator_old.col();
        col_acol_indices[1].second = spectator_old.acol();
        if (emitter_was_3bar_end){
          col_acol_indices[0].first  = emitter_is_initial ? emitter_old.col()  : 0;
          col_acol_indices[0].second = emitter_is_initial ? 0                  : emitter_old.acol();
          col_acol_indices[2].first  = emitter_is_initial ? emitter_old.acol() : emitter_old.col();
          col_acol_indices[2].second = 0;
        } else { //< emitter at the 3 end
          col_acol_indices[0].first  = emitter_is_initial ? 0                  : emitter_old.col();
          col_acol_indices[0].second = emitter_is_initial ? emitter_old.acol() : 0;
          col_acol_indices[2].first  = 0;
          col_acol_indices[2].second = emitter_is_initial ? emitter_old.col()  : emitter_old.acol();
        }
      } else { // splitting the gluon at the spectator end
        // emitter stays the same
        col_acol_indices[0].first  = emitter_old.col();
        col_acol_indices[0].second = emitter_old.acol();
        if (emitter_was_3bar_end){
          col_acol_indices[1].first  = spectator_is_initial ? 0                    : spectator_old.col();
          col_acol_indices[1].second = spectator_is_initial ? spectator_old.acol() : 0                  ;
          col_acol_indices[2].first  = 0;
          col_acol_indices[2].second = spectator_is_initial ? spectator_old.col()  : spectator_old.acol();
        } else { //< emitter at the 3 end, spectator at 3bar
          col_acol_indices[1].first  = spectator_is_initial ? spectator_old.col()  : 0;
          col_acol_indices[1].second = spectator_is_initial ? 0                    : spectator_old.acol();
          col_acol_indices[2].first  = spectator_is_initial ? spectator_old.acol() : spectator_old.col();
          col_acol_indices[2].second = 0; 
        }
      }
    }
  }
  
  // some printout if asked for
  #ifdef PSVERBOSE
  std::cout << "Colour indices (colour, anti-colour) set in pythia:" << std::endl;
  std::cout << "emitter: " << col_acol_indices[0].first << ", " << col_acol_indices[0].second << std::endl;
  std::cout << "spec   : " << col_acol_indices[1].first << ", " << col_acol_indices[1].second << std::endl;
  std::cout << "rad    : " << col_acol_indices[2].first << ", " << col_acol_indices[2].second << std::endl;
  #endif //PSVERBOSE

  // finally return the vector of paired indices
  return col_acol_indices;
}

// update the beams given i_sideA and i_sideB
// these are the particles in the pythia event 
// that directly originate from the beams
// information stored here is needed to set up the beam remnants
void PythiaPanScalesTime::update_beam_system(int i_sys, int i_sideA, int i_sideB, Event & event){
    // get the sales of the PDFs
    double pdf_scale2 = exp(2*_lnv);
    // update of the resonance particle system
    (*beamAPtr)[i_sys].update(i_sideA, event[i_sideA].id(), _event.pdf_x_beam1());
    (*beamAPtr)[i_sys].p(event[i_sideA].p());
    // update of the beam system
    (*beamAPtr).xfISR(i_sys, event[i_sideA].id(), _event.pdf_x_beam1(), pdf_scale2);
    (*beamAPtr).pickValSeaComp();
    
    // update the other beam
    (*beamBPtr)[i_sys].update(i_sideB, event[i_sideB].id(), _event.pdf_x_beam2());
    (*beamBPtr)[i_sys].p(event[i_sideB].p());
    (*beamBPtr).xfISR(i_sys, event[i_sideB].id(),  _event.pdf_x_beam2(), pdf_scale2);
    (*beamBPtr).pickValSeaComp();
}

// update the parton system
// letting it know which particles are the new A and B beam particles
void PythiaPanScalesTime::update_parton_system(int i_sys, int i_sideA, int i_sideB, Event & event){
    partonSystemsPtr->setInA(i_sys, i_sideA);
    partonSystemsPtr->setInB(i_sys, i_sideB);
    double cm_energy = (event[partonSystemsPtr->getInA(i_sys)].p() + event[partonSystemsPtr->getInB(i_sys)].p()).m2Calc();
    partonSystemsPtr->setSHat(i_sys, cm_energy);
}

// update the pythia event
void PythiaPanScalesTime::update_pythia_momenta(int i_sys, unsigned int emitter_index_ps, unsigned int spectator_index_ps, Event & event){
  // for all showers we replace the entire pre-existing set of pythia particles
  // with a new one
  // this means we automatically handle boosts correctly
  // get the total number of particles
  unsigned int n = _map_ps_to_py.size();
  // loop over the panscales indices
  for (unsigned int i_ps = 0; i_ps < n; i_ps++){
    // don't do the emitter and spectator: these were replaced already
    if ((i_ps == emitter_index_ps) || (i_ps == spectator_index_ps)) continue;
    // get the pythia index
    int i_py = _map_ps_to_py[i_ps];
    // update the momentum (post-boost)
    const auto &q = _event[i_ps];
    // check whether the momenta are the same
    // don't copy over if they are
    if((q.px() == event[i_py].px()) && (q.py() == event[i_py].py()) &&
          (q.pz() == event[i_py].pz()) && (q.E() == event[i_py].e())) continue;
    // otherwise we need to update the momentum
    // copy over to a new particle
    int i_py_new = event.copy(i_py, event[i_py].status());
    // reset its momenta
    event[i_py_new].p(q.px(), q.py(), q.pz(), q.E());
    // adjust the parton system
    partonSystemsPtr->replace(i_sys, i_py, i_py_new);
    // store the new final-state pythia particle in the map
    _map_ps_to_py[i_ps] = i_py_new;
  }
}


CmdLine PythiaPanScalesTime::_cmdline_from_settings(){
  // initialise an empty vector
  std::vector<string> args;
  // set a dummy executable
  args.push_back("./cmdline");
  // we will look for the PanScales string in the maps
  string keyword = "PanScales:";
  // get the flags
  for(auto flag : settingsPtr->getFlagMap(keyword)){
    if(flag.second.valNow){
      args.push_back("-"+std::regex_replace(flag.second.name, std::regex(keyword), ""));
    } else{
      args.push_back("-no-"+std::regex_replace(flag.second.name, std::regex(keyword), ""));
    }
  }
  // get the modes
  for(auto mode : settingsPtr->getModeMap(keyword)){
    // add the name
    args.push_back("-"+std::regex_replace(mode.second.name, std::regex(keyword), ""));
    // and the value
    args.push_back(std::to_string(mode.second.valNow));
  }
  // get the parameters
  for(auto parm : settingsPtr->getParmMap(keyword)){
    // add the name
    args.push_back("-"+std::regex_replace(parm.second.name, std::regex(keyword), ""));
    // and the value
    args.push_back(std::to_string(parm.second.valNow));
  }
  // get the words
  for(auto word : settingsPtr->getWordMap(keyword)){
    // add the name
    args.push_back("-"+std::regex_replace(word.second.name, std::regex(keyword), ""));
    // and the value
    args.push_back(word.second.valNow);
  }
  
  // handle non-panscales-specific PDFs
  string lhapdf_set = std::regex_replace(settingsPtr->word("PDF:pSet"), std::regex("LHAPDF6:"), "");
  if(lhapdf_set != "13"){ //< pythia's internal set
    args.push_back("-lhapdf-set");
    args.push_back(lhapdf_set);
  }
  // create the command line
  return CmdLine(args);
}

// overview of the settings in panscales
void PythiaPanScales::init_settings_ptr(Pythia & pythia){
  // shower settings
  pythia.settings.addWord("PanScales:shower", "panglobal");
  // options that are allowed: panglobal, panlocal, panlocal-antenna
  pythia.settings.addParm("PanScales:beta", 0, true, true, 0, 1);
  // coupling
  pythia.settings.addFlag("PanScales:physical-coupling", true);
  // scale variations
  pythia.settings.addParm("PanScales:xmur", 1, true, true, 0.5, 2);
  pythia.settings.addParm("PanScales:xmuf", 1, true, true, 0.5, 2);
  pythia.settings.addParm("PanScales:xhard", 1, true, true, 0.5, 2);
  pythia.settings.addParm("PanScales:xsimkt", 1, true, true, 0.5, 2);
  // colour
  pythia.settings.addWord("PanScales:colour", "NODS");  
  // options that are allowed: CFHalfCA, CATwoCF, Segment, NODS, CFFE
  // double-soft corrections
  pythia.settings.addFlag("PanScales:double-soft", false);
  // spin correlations
  pythia.settings.addFlag("PanScales:spin-corr", false);
  // matching
  pythia.settings.addFlag("PanScales:match-process", false);
}
