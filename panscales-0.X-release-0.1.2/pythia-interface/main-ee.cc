//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include <iostream>
#include "PanScalesPythiaModule.hh"
#include "PanScalesPythiaAnalysisFramework.hh"
#include "CmdLine.hh"

using namespace std;

////////////////////////////////////////////
/// Pythia interfaced to PanScales
/// for generating e+e- -> Z -> qqbar events
///
/// example of a (NLO matched process) command line is 
/** 
    ./build-double/main-ee -shower panglobal -physical-coupling \
             -match-process -nev 1e4 -out main-ee.dat
*/
///
/// This code uses the PanScales analysis framework
/// to analyse the events
/// The analysis itself is based on main06 of pythia
////////////////////////////////////////////
class PythiaAnalysisFrameworkEE : public PythiaAnalysisFramework {
public:
  
  // ctor
  PythiaAnalysisFrameworkEE(CmdLine * cmdline_in) : PythiaAnalysisFramework(cmdline_in) {
    string cmdline_section = "Options specific to e+e-->Z production";
    cmdline_in->start_section(cmdline_section);
    // set up the process (e+e- -> Z -> qqbar)
    _pythia.readString("Beams:idA = 11");
    _pythia.readString("Beams:idB = -11");
    double rts = cmdline_in->value<double>("-rts", 91.1876)
                            .help("collider centre-of-mass energy")
                            .argname("rts");
    _pythia.readString("Beams:eCM = "+to_string(rts));
    _pythia.readString("WeakSingleBoson:ffbar2ffbar(s:gmZ) = on");
    header << "# Pythia hard event with settings: \n# Process e+e-->Z->qqbar\n# WeakSingleBoson:ffbar2ffbar(s:gmZ) = on" << std::endl;
    // set the decays of the Z
    _pythia.readString("WeakZ0:gmZmode = 0");
    _pythia.readString("23:onMode = off");
    std::string Zdecays;
    bool only_light_decays = cmdline->value_bool("-Z2uds", false)
                                .help("Set the Z decays to only light partons (default includes decays to c and b)");
    if (only_light_decays) {
      Zdecays = "23:onIfAny = 1 2 3";
    } else {
      Zdecays = "23:onIfAny = 1 2 3 4 5";
    }
    _pythia.readString(Zdecays);
    _pythia.readString("PDF:lepton = off");
    // add info to header
    header << "# Zdecays: " << Zdecays << std::endl;
    header << "# PDF:lepton = off" << std::endl;
    header << "# f_rts " << rts << std::endl;
    cmdline_in->end_section(cmdline_section);

    // printout some of the settings on the terminal
    _pythia.settings.flag("Init:showChangedParticleData", true);
    _pythia.settings.listChanged();

    // initialise the analysis
    init_analysis(cmdline_in);
  }

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() {
    // declare histograms and binning
    Binning sph_binning(0.0, 0.6, 0.01);  
    cumul_hists_err["evshp:Sph" ].declare(sph_binning);   
    Binning obl_binning(0.0, 0.8, 0.01);  
    cumul_hists_err["evshp:Obl" ].declare(obl_binning);   
    Binning tau_binning(0.0, 0.5, 0.01);  
    cumul_hists_err["evshp:tau" ].declare(tau_binning);

    // multiplicities
    Binning mult_binning(-0.5, 25.5, 1.);
    hists_err["multiplicity:charged"].declare(mult_binning); 
    hists_err["multiplicity:lund" ].declare(mult_binning);  
    hists_err["multiplicity:jade" ].declare(mult_binning);  
    hists_err["multiplicity:durham"].declare(mult_binning);  
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() {
    // get the event weight
    double evwgt = event_weight();

    // Find and histogram charged multiplicity (NB: if run
    // at parton level, as is the default, this just counts the quarks!)
    int nCh = 0;
    for (int i = 0; i < _pythia.event.size(); ++i)
      if (_pythia.event[i].isFinal() && _pythia.event[i].isCharged()) ++nCh;
    hists_err["multiplicity:charged"].add_entry(nCh, evwgt);

    // Find and histogram sphericity
    if (_sph.analyze(_pythia.event )) {
      cumul_hists_err["evshp:Sph"].add_entry(_sph.sphericity(), evwgt);
    }

    // Find and histogram thrust & oblateness
    if (_thr.analyze(_pythia.event )) {
      cumul_hists_err["evshp:Obl"].add_entry(_thr.oblateness(), evwgt);
      cumul_hists_err["evshp:tau"].add_entry(1-_thr.thrust(), evwgt);
    }

    // Find and histogram cluster jets: Lund, Jade and Durham
    if (_lund.analyze(_pythia.event, 0.01, 0.)) {
      hists_err["multiplicity:lund"].add_entry(_lund.size(), evwgt);
    }
    if (_jade.analyze(_pythia.event, 0.01, 0.)) {
      hists_err["multiplicity:jade"].add_entry(_jade.size(), evwgt);
    }
    if (_durham.analyze(_pythia.event, 0.01, 0.)) {
      hists_err["multiplicity:durham"].add_entry(_durham.size(), evwgt);
    }
  }

  private:

  // Set up Sphericity, Thrust and cluster jet analyses
  Pythia8::Sphericity _sph;
  Pythia8::Thrust _thr;
  Pythia8::ClusterJet _lund  = Pythia8::ClusterJet("Lund");
  Pythia8::ClusterJet _jade  = Pythia8::ClusterJet("Jade");
  Pythia8::ClusterJet _durham = Pythia8::ClusterJet("Durham");
};

//----------------------------------------------------------------------
// the main program, which creates the analysis framework class and runs it
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  PythiaAnalysisFrameworkEE driver(&cmdline);
  driver.run();
}
