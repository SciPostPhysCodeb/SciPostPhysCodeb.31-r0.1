//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

// [ADAPTED by PanScales] 
// main06.cc is a part of the PYTHIA event generator.
// Copyright (C) 2021 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// This is a simple test program.
// It studies event properties of LEP1 events.
//
// After building (see README.md), run with:
//
//    build-double/main-pythia06 

#include <Pythia8/Pythia.h>
#include "PanScalesPythiaModule.hh"

using namespace Pythia8;

int main() {

  // Generator
  Pythia pythia;

  // Allow no substructure in e+- beams: normal for corrected LEP data.
  pythia.readString("PDF:lepton = off");
  // Process selection.
  pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
  // Switch off all Z0 decays and then switch back on those to quarks.
  pythia.readString("23:onMode = off");
  pythia.readString("23:onIfAny = 1 2 3 4 5");

  // LEP1 initialization at Z0 mass.
  pythia.readString("Beams:idA =  11");
  pythia.readString("Beams:idB = -11");
  double mZ = pythia.particleData.m0(23);
  pythia.settings.parm("Beams:eCM", mZ);


  // PanScales specific
  pythia.readString("PartonLevel:MPI = off"); // make sure MPI is seen as off 
  ShowerModelPtr shower_model_ptr = make_shared<PythiaPanScales>();  
  pythia.setShowerModelPtr(shower_model_ptr);
  // also set the settings
  std::dynamic_pointer_cast<PythiaPanScales>(shower_model_ptr)->init_settings_ptr(pythia);
  pythia.readString("PartonShowers:model = 2"); // hack to make sure the user hooks are properly set
  // add settings
  pythia.readString("PanScales:shower = panglobal");
  pythia.readString("PanScales:beta = 0");
  pythia.readString("PanScales:match-process = on");

  // initialise
  pythia.init();


  // Histograms.
  Hist nCharge("charged multiplicity", 100, -0.5, 99.5);
  Hist spheri("Sphericity", 100, 0., 1.);
  Hist linea("Linearity", 100, 0., 1.);
  Hist thrust("thrust", 100, 0.5, 1.);
  Hist oblateness("oblateness", 100, 0., 1.);
  Hist sAxis("cos(theta_Sphericity)", 100, -1., 1.);
  Hist lAxis("cos(theta_Linearity)", 100, -1., 1.);
  Hist tAxis("cos(theta_Thrust)", 100, -1., 1.);
  Hist nLund("Lund jet multiplicity", 40, -0.5, 39.5);
  Hist nJade("Jade jet multiplicity", 40, -0.5, 39.5);
  Hist nDurham("Durham jet multiplicity", 40, -0.5, 39.5);
  Hist eDifLund("Lund e_i - e_{i+1}", 100, -5.,45.);
  Hist eDifJade("Jade e_i - e_{i+1}", 100, -5.,45.);
  Hist eDifDurham("Durham e_i - e_{i+1}", 100, -5.,45.);

  // Set up Sphericity, "Linearity", Thrust and cluster jet analyses.
  Sphericity sph;
  Sphericity lin(1.);
  Thrust thr;
  ClusterJet lund("Lund");
  ClusterJet jade("Jade");
  ClusterJet durham("Durham");

  // Begin event loop. Generate event. Skip if error. List first few.
  for (int iEvent = 0; iEvent < 10000; ++iEvent) {
    if (!pythia.next()) continue;

    // Find and histogram charged multiplicity.
    int nCh = 0;
    for (int i = 0; i < pythia.event.size(); ++i)
      if (pythia.event[i].isFinal() && pythia.event[i].isCharged()) ++nCh;
    nCharge.fill( nCh );

    // Find and histogram sphericity.
    if (sph.analyze( pythia.event )) {
      if (iEvent < 3) sph.list();
      spheri.fill( sph.sphericity() );
      sAxis.fill( sph.eventAxis(1).pz() );
      double e1 = sph.eigenValue(1);
      double e2 = sph.eigenValue(2);
      double e3 = sph.eigenValue(3);
      if (e2 > e1 || e3 > e2) cout << "eigenvalues out of order: "
      << e1 << "  " << e2 << "  " << e3 << endl;
    }

    // Find and histogram linearized sphericity.
    if (lin.analyze( pythia.event )) {
      if (iEvent < 3) lin.list();
      linea.fill( lin.sphericity() );
      lAxis.fill( lin.eventAxis(1).pz() );
      double e1 = lin.eigenValue(1);
      double e2 = lin.eigenValue(2);
      double e3 = lin.eigenValue(3);
      if (e2 > e1 || e3 > e2) cout << "eigenvalues out of order: "
      << e1 << "  " << e2 << "  " << e3 << endl;
    }

    // Find and histogram thrust.
    if (thr.analyze( pythia.event )) {
      if (iEvent < 3) thr.list();
      thrust.fill( thr.thrust() );
      oblateness.fill( thr.oblateness() );
      tAxis.fill( thr.eventAxis(1).pz() );
      if ( abs(thr.eventAxis(1).pAbs() - 1.) > 1e-8
        || abs(thr.eventAxis(2).pAbs() - 1.) > 1e-8
        || abs(thr.eventAxis(3).pAbs() - 1.) > 1e-8
        || abs(thr.eventAxis(1) * thr.eventAxis(2)) > 1e-8
        || abs(thr.eventAxis(1) * thr.eventAxis(3)) > 1e-8
        || abs(thr.eventAxis(2) * thr.eventAxis(3)) > 1e-8 ) {
        cout << " suspicious Thrust eigenvectors " << endl;
        thr.list();
      }
    }

    // Find and histogram cluster jets: Lund, Jade and Durham distance.
    if (lund.analyze( pythia.event, 0.01, 0.)) {
      if (iEvent < 3) lund.list();
      nLund.fill( lund.size() );
      for (int j = 0; j < lund.size() - 1; ++j)
        eDifLund.fill( lund.p(j).e() - lund.p(j+1).e() );
    }
    if (jade.analyze( pythia.event, 0.01, 0.)) {
      if (iEvent < 3) jade.list();
      nJade.fill( jade.size() );
      for (int j = 0; j < jade.size() - 1; ++j)
        eDifJade.fill( jade.p(j).e() - jade.p(j+1).e() );
    }
    if (durham.analyze( pythia.event, 0.01, 0.)) {
      if (iEvent < 3) durham.list();
      nDurham.fill( durham.size() );
      for (int j = 0; j < durham.size() - 1; ++j)
        eDifDurham.fill( durham.p(j).e() - durham.p(j+1).e() );
    }

  // End of event loop. Statistics. Output histograms.
  }
  pythia.stat();
  cout << nCharge << spheri << linea << thrust << oblateness
       << sAxis << lAxis << tAxis
       << nLund << nJade << nDurham
       << eDifLund << eDifJade << eDifDurham;
 
  HistPlot hpl( "plots-main06");
  
  // hpl.frame("", "charged multiplicity", 100, -0.5, 99.5);    , nCharge   
  // hpl.frame("", "Sphericity", 100, 0., 1.);                  , spheri    
  // hpl.frame("", "Linearity", 100, 0., 1.);                   , linea     
  // hpl.frame("", "thrust", 100, 0.5, 1.);                     , thrust    
  // hpl.frame("", "oblateness", 100, 0., 1.);                  , oblateness
  // hpl.frame("", "cos(theta_Sphericity)", 100, -1., 1.);      , sAxis     
  // hpl.frame("", "cos(theta_Linearity)", 100, -1., 1.);       , lAxis     
  // hpl.frame("", "cos(theta_Thrust)", 100, -1., 1.);          , tAxis     
  // hpl.frame("", "Lund jet multiplicity", 40, -0.5, 39.5);    , nLund     
  // hpl.frame("", "Jade jet multiplicity", 40, -0.5, 39.5);    , nJade     
  // hpl.frame("", "Durham jet multiplicity", 40, -0.5, 39.5);  , nDurham   
  // hpl.frame("", "Lund e_i - e_{i+1}", 100, -5.,45.);         , eDifLund  
  // hpl.frame("", "Jade e_i - e_{i+1}", 100, -5.,45.);         , eDifJade  
  // hpl.frame("", "Durham e_i - e_{i+1}", 100, -5.,45.);       , eDifDurham

  hpl.frame( "Thrust", "Thrust distributions", "T", "sigma");     hpl.add(thrust, "-,blue"); 
  hpl.plot(); 


  
  // Done.
  return 0;
}
