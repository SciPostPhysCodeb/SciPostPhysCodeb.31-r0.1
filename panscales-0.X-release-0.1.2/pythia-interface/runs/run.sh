ncores=7

function limit_procs {
    while [ `jobs -p | wc -w` -ge $ncores ]
    do
	sleep 1
    done
}

nev=1e5

for shower in panlocal-beta0.5 panglobal-beta0.0 pythia8-native
do
    sem -j9 ../build-double/main-rivet -out LOPS-${shower}  -shower $shower    -nohad -nomatching -nev $nev &
    sem -j9 ../build-double/main-rivet -out NLOPS-${shower}  -shower $shower -nohad -nev $nev &
    sem -j9 ../build-double/main-rivet -out NLOPS-had-${shower}  -shower $shower -nev $nev &
done



sem --wait
