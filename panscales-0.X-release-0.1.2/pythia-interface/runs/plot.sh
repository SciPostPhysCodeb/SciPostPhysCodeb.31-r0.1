#Rivet activate script
source activate


#comparison among several LOPS
rivet-mkhtml LOPS-py*yoda LOPS-pa*yoda --errs -o LO

#comparison among several NLOPS
rivet-mkhtml NLOPS-py*yoda NLOPS-pa*yoda --errs -o NLO

#comparison among several NLOPS
rivet-mkhtml NLOPS-had-py*yoda NLOPS-had-pa*yoda --errs -o NLOhad

#LO vs NLO vs NLO+had
for shower in pythia panglobal panlocal
do
    rivet-mkhtml NLOPS-had*$shower*yoda NLOPS-$shower*yoda LOPS-*$shower*yoda --errs -o shower
done

#LO+PS vs NLO+PS [Notice PanLocal LO = PanLocal NLO, bug?]
outdir=LOvsNLO
rivet-mkhtml NLOPS-py*.yoda NLOPS-pa*yoda LOPS-py*yoda LOPS-pa*yoda --errs -o $outdir

#make NLO as solid red   (pythia)
#                  blue  (panlocal)
#                  green (panglobal)
# and LO same color but dashed

for file in $outdir/*/*dat
do
    echo $file
    awk 'BEGIN{count=0}$1~/Line/{if($1~/LineColor/){count++;}if(count<=3){print $0; next;}else{if($1~/LineColor/){col="";if(count==4){col="red"}if(count==5){col="blue"}if(count==6){col="green"}printf("LineColor=%s\nLineStyle=dashed\n",col)}next}}{print $0}' $file > test
    mv -f test $file
done
for dir in $outdir/*
do
    cd $dir
    make-plots *dat
    cd -
done


#NLO+PS vs NLO+PS+had
outdir=HadOnOff
rivet-mkhtml NLOPS-had-py*.yoda NLOPS-had-pa*yoda NLOPS-py*yoda NLOPS-pa*yoda --errs -o $outdir
for file in $outdir/*/*dat
do
    echo $file
    awk 'BEGIN{count=0}$1~/Line/{if($1~/LineColor/){count++;}if(count<=3){print $0; next;}else{if($1~/LineColor/){col="";if(count==4){col="red"}if(count==5){col="blue"}if(count==6){col="green"}printf("LineColor=%s\nLineStyle=dashed\n",col)}next}}{print $0}' $file > test
    mv -f test $file
done
for dir in $outdir/*
do
    cd $dir
    make-plots *dat
    cd -
done

for dir in LO  LOvsNLO  NLO  NLOhad  shower
do

    pdfunite $dir/*/*pdf $dir.pdf
done
