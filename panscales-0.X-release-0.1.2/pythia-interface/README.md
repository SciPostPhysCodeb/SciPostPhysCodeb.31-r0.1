This directory contains an interface between PanScales and Pythia8

    ========== BEWARE BEWARE BEWARE BEWARE =============
     The PanScales-Pythia interface is preliminary and
     should be used with extreme caution.
     Report problems to the PanScales authors.
    ========== BEWARE BEWARE BEWARE BEWARE =============

Getting Pythia8
===============

If you have Pythia8 already installed on your system you can use the 

    -DPYTHIA_DIR=<PATH>         path to your installation of Pythia8
    
CMake option. Note that if you use the build script provided by
PanScales (`../scripts/build.py`), the path can be passed to CMake using
  
    --cmake-options="-DPYTHIA_DIR=<PATH>"
    
(and similar for other options you may want to pass to CMake). You can
also create a symbolic link to your existing Pythia installation
directory. For example, if the Pythia8 main code is in

    /usr/pythia8

the symlink must be created as

    ln -s /usr/pythia8 pythia

Alternatively, you can download, configure, and build a local
installation of Pythia8 using

    ./get-pythia.sh

Note that this assumes that LHAPDF 6 is installed on your system.
If this is not the case, you should change l.31 of `get-pythia.sh`

    ./configure --with-lhapdf6

to

    ./configure

Note that LHAPDF can then not be used in the examples below.


Building the code and running basic examples
============================================

If LHAPDF is installed: build the code using

    ../scripts/build.py --build-lib -j --with-lhapdf

If LHAPDF is not installed: build the code using

    ../scripts/build.py --build-lib -j

To run the $e^{+}e^{-}$ example

    ./build-double/main-ee -shower panglobal -physical-coupling -match-process -nev 1e4 -out main-ee.dat

This will produce $10^{4}$ $e^{+}e^{-} \to Z \to f\bar f$ events,
matched to NLO, showered with the PanGlobal shower, and hadronised by
Pythia.

To produce $pp\to Z$ events run

    ./build-double/main-dy -shower panglobal -physical-coupling -lhapdf-set CT14lo -nev 1e4 -out  main-dy-ct14lo.dat

Alternatively, if you do not have LHAPDF installed you can run

    ./build-double/main-dy -shower panglobal -physical-coupling -pdf-choice ToyNf5Physical -nev 1e4 -out main-dy-toypdf.dat

More information/examples can be found in each of the headers of the `main-*.cc` files

Using the rivet example
=======================

The [main-rivet.cc](main-rivet.cc) script enables a shower-to-data
comparison using Rivet analysis code. This file will only be compiled
after adding `-DWITH_RIVET=ON` in CMake for the build, and
requires installing Rivet (and its dependencies) first.

Note that paths to existing installations of several tools can be passed to CMake:

    -DRIVET_CONFIG=<PATH>     path to the rivet-config script
    -DYODA_DIR=<PATH>         path to your installation of YODA
    -DHEPMC_DIR=<PATH>        path to your installation of HepMC
    -DFASTJET_DIR=<PATH>      path to your installation of FastJet

Beware that the PanScales 0.x release is not intended to be used for
phenomenology.
