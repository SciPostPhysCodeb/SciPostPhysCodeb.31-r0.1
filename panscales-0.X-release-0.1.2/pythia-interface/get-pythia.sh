#!/bin/bash
#
# This script gets and builds Pythia8

version='8.3.10'

# get Pythia8
echo "--------------------------------------------------------------------------------"
echo " Download Pythia${version}"
echo "--------------------------------------------------------------------------------"

compactversion=${version//./}
if [[ ! -d "pythia${compactversion}" ]]; then
    wget https://pythia.org/download/pythia83/pythia${compactversion}.tgz -O - | tar zxf -
else
    echo "already downloaded"
fi

pydir="pythia"
if [[ -e "${pydir}" ]]; then
    echo "symlink already exist, removing old version"
    rm pythia
fi
ln -s pythia${compactversion} ${pydir}

# configuring
echo "--------------------------------------------------------------------------------"
echo " Configuring Pythia${version}"
echo "--------------------------------------------------------------------------------"
pushd pythia
./configure --with-lhapdf6

# building
echo "--------------------------------------------------------------------------------"
echo " Building Pythia${version}"
echo "--------------------------------------------------------------------------------"
make -j8
popd

