//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef PANSCALES_PYTHIA_MODULE_H
#define PANSCALES_PYTHIA_MODULE_H

#include "ShowerBase.hh"
#include "ShowerRunner.hh"
#include "QCD.hh"
#include "PanScalesPythiaProcess.hh"
#include "Pythia8/Event.h"
#include "Pythia8/UserHooks.h"
#include "Pythia8/TimeShower.h"
#include "Pythia8/Pythia.h"

// use a forward declaration for pythia stuff
class PythiaPanScalesTime : public TimeShower {
public:
  /// default ctor
  PythiaPanScalesTime();

  /// Constructor - sets the pointers to showerrunner, hoppetrunner and the process
  PythiaPanScalesTime(std::shared_ptr<panscales::ShowerRunner> & shower_runner, 
                      std::shared_ptr<panscales::HoppetRunner> & hoppet_runner, 
                      std::shared_ptr<panscales::PythiaProcess> & pythia_panscales_process);
          
  /// Destructor - does not need to do anything
  virtual ~PythiaPanScalesTime(){}
  
  /// init function called by pythia
  /// this will set some information on the colliding particles
  virtual void init(BeamParticle* beamA, BeamParticle* beamB);

  /// prepare function called by pythia 
  /// this generates the hard event and sets the starting scale
  virtual void prepare(int iSys, Event& event, bool);

  /// prepares the hard event for showering and sets the weights
  virtual void prepare_hard_event_for_showering(int iSys, Event& event);

  /// pTnext function called by pythia (generates the next lnv)
  virtual double pTnext(Event& event, double pTbegAll, double pTendAll, bool, bool);

  /// branch function called by pythia
  /// given a value of lnv, it tries lnb, calls acceptance probability
  /// and updates the event if the emission is accepted
  virtual bool branch(Event& event, bool);

  /// gets the new colour indices that are needed to update the pythia event 
  vector<pair<int, int>> get_new_colour_indices(unsigned int emitter_index, unsigned int spectator_index, bool emitter_was_3bar_end, bool emitter_splits, Event & event);
  
  /// updates the pythia event given a new panscales event
  /// it does four things:
  /// * set the new colour & status codes for the emitter, spectator and radiated particle
  /// * sets the correct mother-daughter information
  /// * updates the momenta not part of the emitting dipole
  /// * updates the parton and beam system, needed in pythia to construct beam remnants, MPI etc
  void update_pythia_event(unsigned int emitter_index, unsigned int spectator_index, bool emitter_was_3bar_end, bool emitter_splits, Event & event);

  /// update the beams given i_sideA and i_sideB
  /// these are the particles in the pythia event 
  /// that directly originate from the beams
  /// information stored here is needed to set up the beam remnants
  void update_beam_system(int i_sys, int i_sideA, int i_sideB, Event & event);

  /// update the parton system
  /// letting it know which particles are the new A and B beam particles
  void update_parton_system(int i_sys, int i_sideA, int i_sideB, Event & event); 

  /// updates the pythia event by replacing all pre-existing particles with a copy
  /// and possible boosted momenta (obtained from the panscales event)
  void update_pythia_momenta(int i_sys, unsigned int emitter_index_ps, unsigned int spectator_index_ps, Event & event);

  /// return the PanScales event (needed to cache the event weight and multiplicity)
  panscales::Event const & ps_event() { return _event;}

  /// access to the event weight 
  double event_weight(){
    return _event_weight;
  }


  /// private members
private:
  // various pointers
  std::shared_ptr<panscales::ShowerRunner>  _shower_runner;            //< pointer to showerrunner
  std::shared_ptr<panscales::HoppetRunner>  _hoppet_runner;            //< pointer to hoppetrunner (PDF handler)
  std::shared_ptr<panscales::PythiaProcess> _pythia_panscales_process; //< pointer to pythiaprocess (handles pythia-panscales event mapping)

  // event-specific information
  panscales::Event _event;               //< the panscales copy of the event
  double _event_weight;                  //< weight of the event
  unsigned int _nemsn;                   //< number of emissions generated  
  vector<unsigned int> _map_ps_to_py;    //< mapping from PanScales event indices to Pythia8 event indices
  bool _veto_hard_event = false;         //< option to veto the hard event if it does not pass selection cuts
  
  // showerrun information
  double _lnvmax, _lnv, _lnvmin;                                              //< starting, current and stopping scale for showering
  std::unique_ptr<panscales::ShowerBase::EmissionInfo> _emission_info = nullptr; //< generated lnv, lnb, phi, stores the emitting dipoles
  typename panscales::ShowerBase::Element *_element;                          //< information on the dipole pre-splitting

  // returns a commandline from the settings ptr
  CmdLine _cmdline_from_settings();
};


// small class to allow us to set our own starting scales for the shower
class PanScaleUserHooks : public UserHooks {

public:
  bool canSetResonanceScale() {
    return true;}

  // to obtain the invariant mass of the resonance scale 
  double scaleResonance(int iRes, const Event&  event) {
    return event[iRes].m();
  }

};

/// \class PythiaPanScales
/// shower model, initialises the shower pointers for pythia
class PythiaPanScales : public ShowerModel {
public:
  PythiaPanScales() {
    spacePtr     = make_shared<SpaceShower>();
    timesPtr     = make_shared<PythiaPanScalesTime>();
    timesDecPtr  = make_shared<PythiaPanScalesTime>();
  }

  PythiaPanScales(std::shared_ptr<panscales::ShowerRunner> & shower_runner, 
                  std::shared_ptr<panscales::HoppetRunner> & hoppet_runner, 
                  std::shared_ptr<panscales::PythiaProcess> & pythia_panscales_process
                    ) {
    spacePtr     = make_shared<SpaceShower>();
    timesPtr     = make_shared<PythiaPanScalesTime>(shower_runner, hoppet_runner, pythia_panscales_process);
    timesDecPtr  = make_shared<PythiaPanScalesTime>(shower_runner, hoppet_runner, pythia_panscales_process);
  }

  ~PythiaPanScales();

  bool init(MergingPtr, MergingHooksPtr, PartonVertexPtr, WeightContainer*) override {

    subObjects.clear();

    shared_ptr<PanScaleUserHooks> userHooks = make_shared<PanScaleUserHooks>();
    registerSubObject(*userHooks);
    // Update userHooksPtr.
    if ( !userHooksPtr ) {
      userHooksPtr = userHooks;
    }

    // Update infoPtr's pointer to userhooks.
    infoPtr->userHooksPtr = userHooksPtr;
    
    if (mergingHooksPtr) {
      registerSubObject(*mergingHooksPtr);
    }
    if (mergingPtr) {
      registerSubObject(*mergingPtr);
    }
    if (timesPtr)     registerSubObject(*timesPtr);
    if (timesDecPtr)  registerSubObject(*timesDecPtr);
    if (spacePtr)     registerSubObject(*spacePtr);

    // initialise settings pointer
    // init_settings_ptr();
    
    return true;
  }

  bool initAfterBeams() override {
    return true;
  }

  // initialises the settings we have for the panscales shower
  void init_settings_ptr(Pythia & pythia);

  SpaceShowerPtr    getSpaceShower() const override   { return spacePtr; }
  TimeShowerPtr     getTimeShower() const override    { return timesPtr; }
  TimeShowerPtr     getTimeDecShower() const override { return timesDecPtr; }
  //PanScaleUserHooks getTimeDecShower() const override { return userHooksPtr; }

  shared_ptr<SpaceShower>         spacePtr; 
  shared_ptr<PythiaPanScalesTime> timesPtr;
  shared_ptr<PythiaPanScalesTime> timesDecPtr;

};



// overloading << operator for Pythia8 particles
inline std::ostream & operator<<(std::ostream & ostr, const Pythia8::Particle & particle) {
  ostr << particle.px() << " "
        << particle.py() << " "
        << particle.pz() << " "
        << particle.e() << " "
        << particle.id();
  return ostr;
}



#endif
