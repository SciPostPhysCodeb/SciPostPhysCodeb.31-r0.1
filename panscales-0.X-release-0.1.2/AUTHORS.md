PanScales development team
===========================

Active developers
-----------------

* [Melissa van Beekveld   ](mailto:mbeekvel@nikhef.nl                "email")
* [Mrinal Dasgupta        ](mailto:mrinal.dasgupta@manchester.ac.uk  "email")
* [Basem El-Menoufi       ](mailto:basem.el-menoufi@monash.edu       "email")
* [Silvia Ferrario Ravasio](mailto:silvia.ferrario.ravasio@cern.ch   "email")
* [Keith Hamilton         ](mailto:keith.hamilton@ucl.ac.uk          "email")
* [Jack Helliwell         ](mailto:jack.helliwell@physics.ox.ac.uk   "email")
* [Alexander Karlberg     ](mailto:alexander.karlberg@cern.ch        "email")
* [Pier Monni             ](mailto:pier.monni@cern.ch                "email")
* [Gavin Salam            ](https://gsalam.web.cern.ch/gsalam/       "Homepage")
* [Ludovic Scyboz         ](mailto:ludovic.scyboz@monash.edu         "email")
* [Alba Soto-Ontoso       ](mailto:alba.soto.ontoso@cern.ch          "email")
* [Gregory Soyez          ](https://soyez.fastjet.fr/                "Homepage")


Earlier contributors
--------------------

* Frederic Dreyer
* Rok Medves
* Rob Verheyen
