#!/usr/bin/env python3
import augment_enums

files = [
    "QCD.hh",
    "Weight.hh",
    "ShowerRunner.hh",
    "HoppetRunner.hh",
    "ProcessHandler.hh",
    "ShowerPanScaleGlobal.hh",
    "ShowerPanScaleBase.hh",
    "ShowerPanScaleLocalpp.hh",
    "Collider.hh",
]

for f in files:
    augment_enums.process_header(f, insert_includes = True)
