#!/usr/bin/env bash
## 
## Install LHAPDF and one PDF set.
##
## This is exclusively intended for use in a docker image as part of CI.
##

version=6.5.4
echo Preparing to download and install LHAPDF version $version

pushd /tmp
wget https://lhapdf.hepforge.org/downloads/?f=LHAPDF-$version.tar.gz -O - | tar zxf -
cd LHAPDF-$version
# with python, it may fails...
./configure --disable-python
make -j install
wget http://lhapdfsets.web.cern.ch/lhapdfsets/current/CT14lo.tar.gz -O- | tar xz -C $(lhapdf-config --datadir)
echo output of lhapdf-config --libs is
lhapdf-config --libs
popd