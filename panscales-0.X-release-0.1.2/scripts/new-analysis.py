#!/usr/bin/env python3
#
# Script to start a new analysis.
#
# Usage:
#  new-analysis analysis_name
#
# This creates basic files: source code, CMake tools, git ...
#
import os
import sys
import re

# check arguments
if len(sys.argv) != 2:
    print("Usage: new-analysis analysis_name", file=sys.stderr)
    exit(1)
name = sys.argv[1]

# we'll need a name suitable for use in scripts. We'll replace -x and
# _x by X and capitalise the first letter
word_regex_pattern = re.compile("[^A-Za-z]+")
camel_name = ''.join(w.title() for w in word_regex_pattern.split(name))
print (f'Creating analysis {camel_name} in directory {name}')

# create dirs
os.mkdir(name)
scriptdir = os.path.dirname(os.path.realpath(__file__))

# create source file
print (f'  source file {name}.cc')
fin  = open(scriptdir + '/templates/analysis-main.cc.template', 'r')
fout = open(name+'/'+name + '.cc', 'w')

for line in fin:
    fout.write(line.replace('<Name>', camel_name))

# create CMakeLists
print (f'  CMakeLists.txt')
fin  = open(scriptdir + '/templates/analysis-cmake.template', 'r')
fout = open(name+'/CMakeLists.txt', 'w')

for line in fin:
    fout.write(line.replace('<Name>', camel_name).replace('<name>', name))

# create .gitignore
print (f'  .gitignore')
fin  = open(scriptdir + '/templates/analysis-gitignore.template', 'r')
fout = open(name+'/.gitignore', 'w')

for line in fin:
    fout.write(line.replace('<name>', name))

# create a README
print (f'  README.md')
fout = open(name+'/README.md', 'w')
fout.write(f'Documentation for {camel_name}\n')
fout.write('==================================================\n\n')
fout.write('<insert your description here\n')

# finish
