#!/bin/bash
#
# Usage:
#   adjust-case.sh <camelCase_expression> <path>

camelCase=$1
path=$2

# the sed expression is taken from
#   https://askubuntu.com/questions/1203087/using-sed-to-convert-camelcase-to-under-score
snake_case=`echo ${camelCase} | sed -E 's/\B([A-Z]+)/_\L\1/g'
# The following alternative does not treat leading underscores well
#   sed 's/\([A-Z]\{1,\}\)/_\L\1/g;s/^_//'`

echo "Replacing ${camelCase} by ${snake_case} in the following files:"

# replace in headers
for fn in `grep -rl "${camelCase}" --include '*.hh' .`; do
    echo ${fn}
    sed -i --follow-symlinks "s/${camelCase}/${snake_case}/g" ${fn}
done

# replace in source files
for fn in `grep -rl "${camelCase}" --include '*.cc' .`; do
    echo ${fn}
    sed -i --follow-symlinks "s/${camelCase}/${snake_case}/g" ${fn}
done
