#!/usr/bin/env python3
"""
Script that takes a vanilla fjcore and converts it for use with a generic "precision_type"
"""
import re
import sys

def main():
    convert("fjcore.hh")
    convert("fjcore.cc")


def convert(filename):
    text = f"// adapted automatically for general precision from {filename}, by {sys.argv[0]}\n"
    with open(filename,'r') as f:
        for line in f: text += line

    # cc file cmath include
    text = text.replace("#if defined PSFJCORECONVERT\n",
                        "#if defined PSFJCORECONVERT\n#include<cmath>\n")
    text = text.replace("#include \"fjcore.hh\"",
                        "\n#include \"Type.hh\"\n#include \"fjcore_convert.hh\"")

    # include 'Type.hh'
    # TODO: replace those and those below by a sub list
    text = text.replace("const double pi =",
                        "#include \"Type.hh\"\nconst precision_type pi =")
    
    #
    text = text.replace("#if !defined PSFJCORECONVERT", "#if defined PSFJCORECONVERT")
    text = text.replace("#endif // !defined(PSQUAD) && !defined(PSDDREAL) && !defined(PSQDREAL)",
                        "#endif // defined(PSQUAD) || defined(PSDDREAL) || defined(PSQDREAL)")


    # generic things, needed for all types
    text = text.replace("double","precision_type")
    text = re.sub(r'(max|min)\((-?[0-9]+\.[0-9]+)',r'\1(precision_type(\2)',text)

    # some specific workarounds with our types
    text = text.replace("std::numeric_limits<pre","numeric_limit_extras<pre")
    text = text.replace("numeric_limits<pre","numeric_limit_extras<pre")
    text = re.sub(r"(deta|dphi|max_NN_dist|_cumul2)( *= *)0;",
                  r"\1\2precision_type(0);",text)
    text = text.replace("std::sqrt", "sqrt")

    # this is ugly and a bit fragile if ever we change things in FJ
    oldtext = text
    text = text.replace(
     r"""    _rap = 0.5*log((_kt2 + effective_m2)/(E_plus_pz*E_plus_pz));
    if (_pz > 0) {_rap = - _rap;}""",
     r"""     /// 21.OCT.19 - KH
    /// In LundEEGenerator.hh append_to_vector, a line of code
    /// fjcore::PseudoJet u1 = j1/j1.modp(), u2 = j2/j2.modp();
    /// was indirectly calling this function. Sometimes this was
    /// happening for massless partons with exactly zero kT (which
    /// can happen with Dire/Py8/PanLocal recoil schemes (e.g.
    /// with the spectator in 3-parton events). In such cases the
    /// argument of the logarithm below would be zero and in
    /// dd_real / qd_real this would generate MANY terminal messages
    /// "ERROR (dd_real::log): Non-positive argument." Looking in
    /// the QD code you find that immediately after the latter
    /// message is spewed on-screen the function returns with _nan.
    /// Hence we are choosing to avoid the on-screen error mess by
    /// instead returning just _nan if the argument is <=0. Note
    /// that the whole fjcore_convert.cc is wrapped in an if defined
    /// such that it is only ever compiled for dd_real, qd_real, quad
    /// hence we included no further pre-processor if defineds here.
    precision_type arg = (_kt2 + effective_m2)/(E_plus_pz*E_plus_pz);
    if(arg>0.0) {
      _rap = 0.5*log(arg);
      if (_pz > 0) {_rap = - _rap;}
    } else {
      //_rap = precision_type::_nan;
      // GPS 2020-04-06: replaced precision_type::_nan with
      // a more standard quiet_NaN()
      _rap = numeric_limits<precision_type>::quiet_NaN();
    }     """
    )
    if len(oldtext) == len(text): print("Replacmement of big KH block failed")


    ## things specific to cc file that should be relatively safe
    text = re.sub(r'int\((floor.*);',r'int(to_double(\1);',text)
    text = text.replace(r"ieta = int(((eta - _tiles_eta_min) / _tile_size_eta));",
                        r"ieta = int(to_double((eta - _tiles_eta_min) / _tile_size_eta));")
    text = text.replace(r"iphi = int((phi+twopi)/_tile_size_phi) % _n_tiles_phi;",
                        r"iphi = int(to_double((phi+twopi)/_tile_size_phi)) % _n_tiles_phi;")
    text = text.replace(r"ibin = int(rap+nrap);",r"ibin = int(to_double(rap+nrap));")

    text = text.replace(r"max(_Rparam, 0.1)", r"max(_Rparam, precision_type(0.1))")
    text = text.replace(r"_CP2DChan_limited_cluster(min(_Rparam/2,0.3))", 
                        r"_CP2DChan_limited_cluster(min(_Rparam/2,precision_type(0.3)))")
    text = text.replace(r"max(_qmin.comparison_value(),0.0))", 
                        r"max(_qmin.comparison_value(),precision_type(0.0)))")
    text = text.replace(r"return jet==0;",r"return jet==precision_type(0);")

    text = text.replace(r"counts[ibin]++;",r"counts[ibin]+=1;")
    text = text.replace(r"_Qtot = 0;",r"_Qtot = 0.;")

    ## things specific for the cc file that should be revisited
    # in the shuffle, WE SHOULD REVISIT SAFETY OF THIS CHANGE
    text = re.sub(r"(twopow31 *\* *)(renorm_point.(x|y))",r"\1to_double(\2)", text)
    # the 1e-300 was to protect against division by zero, replace by min here
    # (but could do with more thought)
    text = text.replace(r"1e-300",r"numeric_limits<precision_type>::min()")
    text = text.replace(r"1e300" ,r"1.0/numeric_limits<precision_type>::min()")

    # replacement of constants:
    constant_replacements = [
      [r" pi *= *3.14159.*",       r" pi = 4*atan(precision_type(1.0));"],
      [r" twopi *= *6.28318530.*", r" twopi = 2*pi;"],
      [r" pisq *= *9.8696044.*", r" pisq = pi*pi;"],
      [r" zeta2 *= *1.644934.*", r" zeta2 = pisq/6;"],
      [r" ln2 *= *0.693147180559.*", r" ln2 = log(precision_type(2.0));"],
      ]
    for old, new in constant_replacements:
      text = re.sub(old,new,text)

    new_filename = re.sub("fjcore","fjcore_convert",filename)
    with open(new_filename,'w') as f:
        print(text,file=f,end="")

if __name__ == '__main__': main()





## old redundant things
#    text = text.replace("#define __FJCORE_SHARED_PTR_HH__\n",
#                        "#define __FJCORE_SHARED_PTR_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#include <tr1/memory>",
#                        "#include \"Type.hh\"\n#include <tr1/memory>")
#    text = text.replace("#define __FJCORE_LIMITEDWARNING_HH__\n",
#                        "#define __FJCORE_LIMITEDWARNING_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_ERROR_HH__\n",
#                        "#define __FJCORE_ERROR_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_PSEUDOJET_STRUCTURE_BASE_HH__\n",
#                        "#define __FJCORE_PSEUDOJET_STRUCTURE_BASE_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_PSEUDOJET_HH__\n",
#                        "#define __FJCORE_PSEUDOJET_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_SELECTOR_HH__\n",
#                        "#define __FJCORE_SELECTOR_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_JETDEFINITION_HH__\n",
#                        "#define __FJCORE_JETDEFINITION_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_CLUSTER_SEQUENCE_STRUCTURE_HH__\n",
#                        "#define __FJCORE_CLUSTER_SEQUENCE_STRUCTURE_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_CLUSTERSEQUENCE_HH__\n",
#                        "#define __FJCORE_CLUSTERSEQUENCE_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_DYNAMICNEARESTNEIGHBOURS_HH__\n",
#                        "#define __FJCORE_DYNAMICNEARESTNEIGHBOURS_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_SEARCHTREE_HH__\n",
#                        "#define __FJCORE_SEARCHTREE_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_MINHEAP__HH__\n",
#                        "#define __FJCORE_MINHEAP__HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_CLOSESTPAIR2DBASE__HH__\n",
#                        "#define __FJCORE_CLOSESTPAIR2DBASE__HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#define __FJCORE_CLOSESTPAIR2D__HH__\n",
#                        "#define __FJCORE_CLOSESTPAIR2D__HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#endif // __FJCORE_TILINGEXTENT_HH__\n",
#                        "#endif // __FJCORE_TILINGEXTENT_HH__\n#include \"Type.hh\"\n")
#    text = text.replace("#ifndef __FJCORE_DROP_CGAL // in case we do not have the code for CGAL\n",
#                        "#ifndef __FJCORE_DROP_CGAL // in case we do not have the code for CGAL\n#include \"Type.hh\"\n")
