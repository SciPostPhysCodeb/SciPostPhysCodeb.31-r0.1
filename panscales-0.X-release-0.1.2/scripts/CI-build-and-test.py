#!/usr/bin/env python3
import subprocess
import os
import re
import argparse

def main():
    # get the directory in which this script is located
    scriptdir = os.path.dirname(os.path.realpath(__file__))
    basedir = re.sub(r'/scripts$', '', scriptdir)
    header(f"basedir is {basedir}")

    # read the CI_VALIDATION_LENGTH environment variable
    # if it is set, then use it to set the length of the validation tests
    # otherwise, default to the "short" length
    validation_length = os.environ.get('CI_VALIDATION_LENGTH', 'short' )

    # if the CI_VALIDATION_SET environment variable is set to anything
    # other than "subset" then run the full set of validation tests
    # with normal runs (but still a subset for the dirdiff runs)
    validation_set    = os.environ.get('CI_VALIDATION_SET',    'subset')

    # if CI_CHECK_EXAMPLES is "yes", then run the checks of the examples
    check_examples    = os.environ.get('CI_CHECK_EXAMPLES',    'no')

    header("Configuration flags")
    print(f"validation_length = {validation_length}")
    print(f"validation_set    = {validation_set}"   )
    print(f"check examples    = {check_examples}"   )


    header("Checking C++ version")
    run("g++ --version")

    header("Building the code")
    os.chdir(f"{basedir}/shower-code")
    run("../scripts/build.py --builds double doubleexp -j")

    header("Doing the main unit tests")
    os.chdir("../unit-tests")
    run("../scripts/build.py --builds double")
    run("build-double/unit-tests")

    header("Doing the unit tests for the double_exp type")
    os.chdir(f"{basedir}/helpers/double_exp/unit-tests")
    run("make -j")
    run("./catch_amalgamated")

    header("Doing the validation tests")
    os.chdir(f"{basedir}/shower-code/validation")
    # NB: some of these will match more than one test
    tests = [
        "pp-panglobal-beta0.5-physical-pp2Z-ToyVFNPhysical-nods",
        "pp-panlocal-beta0.5-pp2H-alphas_0.1_lnvmin_-13_rts_5-alloff",
        # . at end removes wildcarding (which gets tree2 and wgt)
        "ee-panglobal-beta0-ee2Z-alphas_0.1_lnvmin_-14-allon.",
        "ee-panglobal-beta0-ee2H-alphas_0.1_lnvmin_-14-allon",
        "ee-panglobal-beta0-ee2H-alphas_0.1_lnvmin_-14-alloff",
        "ee-panglobal-beta0-sdf-ee2Z-alphas_0.1_lnvmin_-14-alloff",
        "ee-panglobal-beta0.5-ee2Z-alphas_0.1_lnvmin_-14-nods",
        "ee-panlocal-beta_0.5-ee2H-alphas_0.1_lnvmin_-14-allon",
        "dis-panglobal-beta0-alphas_0.1_lnvmin_-13_rts_5-spin-nods",
        "dis-panlocal-beta0.5-alphas_0.1_lnvmin_-13_rts_5-spin",
        "ee-panglobal-beta0.5-ee2H-alphas_0.1_lnvmin_-14-3j",
    ]

    if validation_set == "empty":        
        pass # do nothing, e.g. for to move on to examples
    elif validation_set == "subset":
        run(f"./validate-showers.py --no-build -l{validation_length} -j -s " + ",".join(tests))
    else: 
        run(f"./validate-showers.py --no-build -l{validation_length} -j")

    tests_dirdiff = [
        "pp-panglobal-beta0.5-physical-pp2Z-ToyVFNPhysical-nods",
        "pp-panlocal-beta0.5-pp2H-alphas_0.1_lnvmin_-13_rts_5-alloff",
        "ee-panglobal-beta0-ee2Z-alphas_0.1_lnvmin_-14-allon.",   
    ]
    run(f"./validate-showers.py --no-build -l{validation_length} -j -v double-diff -s " + ",".join(tests))

    if check_examples == "yes":
        header("Checking the examples")
        os.chdir(f"{basedir}/scripts")
        # without Pythia
        #run("./run-examples.py --no-pythia -j2")
        # with Pythia (needs more downloads, so may be more sensitive to outages)
        run("./run-examples.py --install-lhapdf --install-pythia --no-rivet -j2")

col_cyan = "\033[1;36m"
col_red  = "\033[1;91m"
col_end  = "\033[0m"


def run(args):
    print("running " + args)
    result = subprocess.run(args, shell=True)
    if (result.returncode != 0):
        print(result)
        print(col_red + "An error occurred while running " + args + " in directory" + os.getcwd() + col_end )
        exit(-1)

def header(name):
    print(col_cyan + f"\n************* {name} ***********************\n" + col_end)


if __name__ == '__main__': main()
