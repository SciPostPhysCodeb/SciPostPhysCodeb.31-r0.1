#!/usr/bin/env python3
#
# Helper script to handle CMake to build the same code in 3 precision
# types: double, dd_real and qd_real
#
# This codes handles both the build of the shower code and the build
# of the analyses

import os
import re
import sys
import shutil
import argparse
import getpass   #< for a hopefully-portable way of getting the user name

parser = argparse.ArgumentParser()

# what gets build (by default everything is built)
parser.add_argument('--builds', type=str, default=['double'], nargs='+', 
                    help='a space-separated list of the builds to be included, e.g. '
                    '"--builds double ddreal qdreal doubleexp mpfr4096" to get all builds')

# arguments for dependencies
parser.add_argument('--gsl-dir', type=str, default='', help='specifies a non-default GSL directory')
parser.add_argument('--qd-dir',  type=str, default='', help='specifies a non-default QD directory')
parser.add_argument('--with-lhapdf', action='store_true', help='when present, use LHAPDF')
parser.add_argument('--with-intel', action='store_true', help='when present, use Intel(R) compilers')
parser.add_argument('--lhapdf-dir',  type=str, default='', help='specifies a non-default LHAPDF directory')

# arguments for parallel build
parser.add_argument('-j', nargs="?", default='1', const='', 
    help='same as -j[N] option in make, using N processes in parallel (or unlimited number in parallel if N not specified)')

# argument for executable build (and make a guess as to the parent
# directory)
default_parent=re.sub(r'/scripts/.*','',os.path.realpath(__file__))
parser.add_argument("--parent-dir", type=str, default=default_parent, help='specify the parent code directory')

# option to disable the git watcher source code
parser.add_argument("--no-git-watcher", action="store_true", help='disable the git watcher support')

# option to enable spin declustering analyses
parser.add_argument("--do-spin-declustering", action='store_true', help='enables the declustering analysis with spin correlations')

# additional options passed straight to CMake
parser.add_argument("--cmake-options", type=str, default='', help='additional options passed unmodified to CMake')

parser.add_argument("--build-lib", action='store_true', help='when present first go into the library directory and see if it needs rebuilding')

#global args
args = parser.parse_args()


process_output_bytes = []
def process_read(fd):
    data = os.read(fd, 1024)
    process_output_bytes.append(data)
    return data

# optionally rebuild the library
cwd = os.getcwd()
if (not cwd.endswith('shower-code')) and args.build_lib:
    import pty
    print ("Building the PanScales library")
    all_args=sys.argv.copy()
    all_args.remove('--build-lib')
    print (all_args)
    
    # replace the 1st w the proper path (hardcoded)
    all_args[0] = '../scripts/build-multi-precisions.py'

    shower_dir = args.parent_dir+'/shower-code'
    print (f"  switching to directory {shower_dir}")
    os.chdir(shower_dir)
    print (f'  now in dir {os.getcwd()}')
    
    print (f"  building")
    process_output_bytes = []
    return_code = pty.spawn(all_args, process_read)
    #output = str(process_output_bytes)
    if return_code:
        print("Library build failed. Aborting.")
        sys.exit(-1)
    print("Library built. Returning to the original directory and carrying on...")
    os.chdir(cwd)


builds={'double' : {'lib-flag' : ''},
        'ddreal' : {'lib-flag' : '-DPSDDREAL=ON'},
        'qdreal' : {'lib-flag' : '-DPSQDREAL=ON'},
        'doubleexp' : {'lib-flag' : '-DPSDOUBLEEXP=ON'},
        'mpfr4096' : {'lib-flag' : '-DPSMPFR4096=ON'},
        }

#----------------------------------------------------------------------
# handle specific cases
hostname = os.uname()[1]
whoami = getpass.getuser()
extra_cmake_args = args.cmake_options

print ("host is",hostname[:6])

# on kanta, we get cmake via modules. This defines variables which do
# not percolate to scripts. We therefore fix these variables by hand
# (a slightly better approach would be to read them from a config file
# but we'll keep this for later as the setup only changes
# sporadically)

if hostname == 'kanta.ipht.fr':
    print ('Adding special flags for kanta')
    extra_cmake_args += ' -DCMAKE_CXX_COMPILER:FILEPATH=/usr/local/install/gcc-9.2.0/bin/g++ -DCMAKE_C_COMPILER:FILEPATH=/usr/local/install/gcc-9.2.0/bin/gcc -DGSL_ROOT_DIR=/usr/local/install/anaconda3'
if hostname == 'hydra' and whoami == 'karlberg':
    print ('Adding special flags for hydra')
    extra_cmake_args += ' -DCMAKE_CXX_COMPILER:FILEPATH=/usr/local/shared/gcc/gcc-13.2//bin/g++ -DCMAKE_C_COMPILER:FILEPATH=/usr/local/shared/gcc/gcc-13.2//bin/gcc'
if hostname[:6] == 'lxplus' and whoami == 'akarlber':
    print ('Adding special flags for lxplus')
    extra_cmake_args += ' -DCMAKE_PREFIX_PATH=/afs/cern.ch/user/a/akarlber/Local/'
# git watcher support
if args.no_git_watcher:
    extra_cmake_args += ' -DGIT_WATCHER=OFF '
    
    
# check if we are in the main shower code
if cwd.endswith('shower-code'):
    # clean existing build
    for f in ['libpanscales.so', 'config.hh', 'CMakeCache.txt', 'cmake_install.cmake']:
        if os.path.exists(f): os.remove(f)
    if os.path.exists('CMakeFiles'): shutil.rmtree('CMakeFiles')

    # loop over builds
    for build in args.builds:
        if not build in builds:
            print (f'******  {build} does not exist, skipping ******')
            
        print ('--------------------------------------------------')
        print ('  building precision ', build)
        print ('--------------------------------------------------')
        # create build directory if it does not exist
        build_dir = 'build-'+build
        if not os.path.exists(build_dir):
            os.mkdir(build_dir)
        os.chdir(build_dir)

        # run cmake
        cmake_args = builds[build]['lib-flag']+' '+extra_cmake_args
        if args.qd_dir:
            cmake_args += ' -DQD_ROOT_DIR='+args.qd_dir
        if args.gsl_dir:
            cmake_args += ' -DGSL_ROOT_DIR='+args.gsl_dir
        if args.with_intel:
            cmake_args += ' -DCMAKE_C_COMPILER=icx -DCMAKE_CXX_COMPILER=icx -DCMAKE_Fortran_COMPILER=ifx'
        if args.with_lhapdf:
            cmake_args += ' -DWITH_LHAPDF=ON'
            if args.lhapdf_dir:
                cmake_args += ' -DLHAPDF_ROOT_DIR='+args.lhapdf_dir
            
        cmake_command = 'cmake '+cmake_args+' ..'
        print(f"cmake command: {cmake_command}")
        return_code = os.system(cmake_command)
        if return_code != 0:
            print (f'\nCMake failed (with code {return_code}). Exiting', file=sys.stderr)
            exit(-1)

        # build
        return_code = os.system(f'make -j{args.j}')
        if return_code != 0:
            print (f'\nBuilding failed (with code {return_code}). Exiting', file=sys.stderr)
            exit(-1)

        print ('\nBuild successful')
        os.chdir('..')

else:
    # clean existing build
    for f in ['CMakeCache.txt', 'cmake_install.cmake']:
        if os.path.exists(f): os.remove(f)
    if os.path.exists('CMakeFiles'): shutil.rmtree('CMakeFiles')

    # loop over builds
    for build in args.builds:
        if not build in builds:
            print (f'******  {build} does not exist, skipping ******')
            
        print ('--------------------------------------------------')
        print ('  building precision ', build)
        print ('--------------------------------------------------')
        
        # create build directory if it does not exist
        pdir = os.path.abspath(args.parent_dir)
        build_dir = 'build-'+build
        if not os.path.exists(build_dir):
            os.mkdir(build_dir)
        os.chdir(build_dir)

        # run cmake
        #
        # We need o specify the parent dir and the subdir where to
        # fing the panscales library w the corresponding precision
        cmake_args  = builds[build]['lib-flag']+' '+extra_cmake_args
        cmake_args += ' -DPARENT_DIR='+pdir
        cmake_args += ' -DPANSCALES_BUILD_DIR='+pdir+'/shower-code/'+build_dir

        # We should not need LHAPDF arguments anywhere other than the in the shower
        # code directory
        #
        # if args.with_lhapdf:
        #     cmake_args += ' -DWITH_LHAPDF=ON'
        #     if args.lhapdf_dir:
        #         cmake_args += ' -DLHAPDF_ROOT_DIR='+args.lhapdf_dir
        print ('  Running cmake with arguments', cmake_args)
        
        return_code = os.system('cmake '+cmake_args+' ..')
        if return_code != 0:
            print (f'\nCMake failed (with code {return_code}). Exiting', file=sys.stderr)
            exit(-1)

        # build
        return_code = os.system('make -j')
        if return_code != 0:
            print (f'\nBuilding failed (with code {return_code}). Exiting', file=sys.stderr)
            exit(-1)

        print ('\nBuild successful')
        os.chdir('..')
    


