#!/usr/bin/perl -w

$mindiff = 1e-12;
$use_rel_diff = 0;
$ignore_line_regexp = "";
# an additional absolute tolerance that is can be used
# when using relative differences
$use_rel_diff_abs_tolerance = 0;

# very basic option handling (should be improved, so we can handle more than one option!)
if ($ARGV[0] eq "-I") {
  $ignore_line_regexp = $ARGV[1];
  shift @ARGV;
  shift @ARGV;
}

if ($#ARGV < 1 || $#ARGV > 4) {
  print STDERR "Usage: numdiff.pl [-I ignore_line_regexp] file1 file2 [threshold_difference] [r [abs_toleratnce]]\n";
  print STDERR "
Prints lines where text differs (other than extra spaces) or where 
numbers differ by more than threshold_difference (default=$mindiff).
If the last argument is 'r' then the threshold is relative to the average
of the sum of the absolute values of the numbers in the line.
\n";
  exit(2);
}

# poor mans numerical diff (does not do grouping).
open (A,"< $ARGV[0]") || die "Could not open arg 1";
open (B,"< $ARGV[1]") || die "Could not open arg 2";
if ($#ARGV >= 2) {$mindiff = $ARGV[2]}

# if the last argument is 'r' then we use relative differences.
if ($#ARGV >= 3) {
  if ($ARGV[3] ne 'r') {
    print STDERR "Unrecognised argument $ARGV[3] (it must be 'r', or nothing)";
    exit(2);
  } else {
    $use_rel_diff = 1;
    if ($#ARGV >= 4) {
      $use_rel_diff_abs_tolerance = $ARGV[4];
    }
  }
}
print STDERR "threshold_difference = $mindiff and use of rel difference is $use_rel_diff";
if ($use_rel_diff) {
  print STDERR ", with abs tolerance $use_rel_diff_abs_tolerance";
}
print STDERR "\n";


$linenum = 0;
$retcode = 0;
while (1) {
  while ($a = <A>) {
    if (!defined($a) || $ignore_line_regexp eq "") {last}
    if ($a =~ /$ignore_line_regexp/) {
      #print "Ignore $a";
      next;} else {last}
  }
  while ($b = <B>) {
    if (!defined($b) || $ignore_line_regexp eq "") {last}
    if ($b =~ /$ignore_line_regexp/) {
      #print "Ignore $b";
      next;
    } else {last}
  }
  #print $a,$b, $ignore_line_regexp,"\n";
  if (!defined($a) && !defined($b)) {last}
  if (!defined($a)) {
    print "File 1 is shorter than file 2 at line $linenum\n";
    exit(1);
  }
  if (!defined($b)) {
    print "File 2 is shorter than file 1 at line $linenum\n";
    exit(1);
  }

  $linenum += 1;
  if ($a eq $b) {next}
  @a = split(/\s+/,$a);
  @b = split(/\s+/,$b);
  if ($#a != $#b) {&write($a,$b,$linenum); next}
  while($#a > -1) {
    $aa = shift @a;
    $bb = shift @b;
    if ($aa eq $bb) {next}
    $aafloat = ($aa =~ /^([+-]?)(?=\d|\.\d)\d*(\.\d*)?([Ee]([+-]?\d+))?$/);
    $bbfloat = ($bb =~ /^([+-]?)(?=\d|\.\d)\d*(\.\d*)?([Ee]([+-]?\d+))?$/);
    if ($aafloat && $bbfloat) {
      # work out a normalisation for the difference according to whether
      # we are using relative differences or not.
      if ($use_rel_diff) {
        $norm = 0.5*(abs($aa) + abs($bb));
      } else {
        $norm = 1.0;
      }
      $absdiff = abs($aa - $bb);
      if ($absdiff > $mindiff*$norm && $absdiff > $use_rel_diff_abs_tolerance) {&write($a,$b,$linenum); last}
    } else {&write($a,$b,$linenum); last}
  }
}

exit($retcode);

sub write{  
  my ($a, $b, $linenum) = @_;
  $retcode = 1;
  print "$linenum< $a";
  print "$linenum> $b";
}
  
