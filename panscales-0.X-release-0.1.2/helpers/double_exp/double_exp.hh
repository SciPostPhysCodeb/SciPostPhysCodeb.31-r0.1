#ifndef __double_exp_hh__
#define __double_exp_hh__

//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include <iostream>
#include <ostream>
#include <cmath>
#include <limits>
#include <cstring>
#include <cassert>

/// \class double_exp
/// class that implements a new floating-point type that increases the
/// maximum size of the exponent of a regular double-precision type.
class double_exp {
public:

  typedef int64_t ExpType;
  //typedef int32_t ExpType;

  /// default constructor, with a value of zero
  double_exp()                      : d_(0)  , n_(0) {}
  /// constructor from a double
  double_exp(double val)            : d_(val), n_(0) {renorm();}
  /// contructor from an integer
  double_exp(int    val)            : d_(val), n_(0) {renorm();}
  /// constructor that that specifies a value separately as a mantissa val
  /// and an exponent (power of 2) n
  double_exp(double val, ExpType n) : d_(val), n_(n) {renorm();}

protected:
  /// constructor for when we don't want to renorm
  /// it is imperative that the values are sensible, in particular
  /// 0.5 <= |val| < 1.0
  constexpr double_exp(double val, ExpType n, bool do_renorm) : d_(val), n_(n) {}

public:

  /// constructs a constexpr value, with the (unchecked) requirement
  /// that 0.5 <= |val < 1
  static constexpr double_exp raw(double val, ExpType n) {return double_exp(val, n, false);}

  /// assuming that *this is not nan or inf, returns true if this number
  /// can be converted to an equivalent double without overflow or loss
  /// of precision
  ///
  /// Added on 2020-10-22: with the prescription that inf and nan are
  /// represented with n==max_n+1, the code below will return false for
  /// inf and nan. We therefore renamed it from is_valid_double to
  /// is_finite_double to make that explicit
  bool is_finite_double() const {
    // max_exponent is one more than the largest integer power of two
    // that can be represented; so we can get 0.5 * 2^max_exponent
    // and if n == max_exponent we should be OK.
    //
    // min_expondent is one more than the smallest integer power of two
    // that can be represented; we represent that as 0.5 * 2^n = 2^{n_-1},
    // so this should be n_-1 > std::numeric_limits<double>::min_exponent-1
    return n_ <= std::numeric_limits<double>::max_exponent
          && n_ >= std::numeric_limits<double>::min_exponent;
  }

  /// conversion to double
  //operator double() const {return to_double()}

  double to_double() const {return ldexp(d_,n_);}

  // put the explicit keyword here to avoid implicit
  // conversion, which would be dangerous
  explicit operator double() const {return to_double();}

  /// conversion to int, via a double, on the grounds that
  /// if it's representable in int it will also be representable
  /// in double
  explicit operator int() const {return int(to_double());}

  double d() const {return d_;}
  ExpType n() const {return n_;}

  //----------------------------------------------------------------------
  ///@{
  /// multiplication operators
  double_exp & operator *=(double_exp val) {
    d_ *= val.d_;
    n_ += val.n_;
    renorm();
    return *this;
  }
  double_exp & operator *=(double val) {
    d_ *= val;
    renorm();
    return *this;
  }
  double_exp operator*(double val) const {
    return double_exp(d_*val, n_);
  }
  double_exp operator*(double_exp val) const {
    return double_exp(d_ * val.d_, n_ + val.n_);
  }
  ///@}

  ///@{
  /// division operators
  double_exp & operator/=(double_exp val) {
    *this = *this / val;
    return *this;
  }
  double_exp & operator/=(double val) {
    *this = *this / val;
    return *this;
  }
  double_exp operator/(double_exp val) const {
    if (val.d_ == 0) {
      if (d_ > 0) return  double_exp::inf();
      else        return -double_exp::inf();
    }
    return double_exp(d_ / val.d_, n_ - val.n_);
  }  
  double_exp operator/(double val) const {
    if (val == 0) {
      if (d_ > 0) return  double_exp::inf();
      else        return -double_exp::inf();
    }
    return double_exp(d_ / val, n_);
  }
  ///@} 

  /// unary minus
  double_exp operator-() const {
    return double_exp(-d_, n_, false);
  }
  /// unary plus
  double_exp operator+() const {
    return *this;
  }

  double_exp abs() const {
    return double_exp(std::abs(d_), n_, false);
  }

  //----------------------------------------------------------------------
  ///@{
  /// addition & subtraction operators
  double_exp operator +=(double_exp val) {
    *this = *this + val;
    // if (val.n_ <= n_) {
    //   d_ += ldexp(val.d_,val.n_-n_);
    // } else {
    //   d_ = val.d_ + ldexp(d_, n_-val.n_);
    //   n_ = val.n_;
    // }
    // renorm();
    return *this;
  }
  double_exp operator -=(double_exp val) {
    *this = *this - val;
    return *this;
  }
  double_exp operator+(double_exp val) const {
    if (n_ >= val.n_) return sum_a_ge_b(val);
    else              return val.sum_a_ge_b(*this);
  }

  double_exp operator-(double_exp val) const {
    if (n_ >= val.n_) {return diff_a_ge_b(val);}
    else              {return -(val.diff_a_ge_b(*this));}
  }
  ///@}

  
  //----------------------------------------------------------------------
  ///@{
  /// equality operators
  bool operator==(double_exp val) const {
    return d_==val.d_ && n_==val.n_;
  }
  bool operator!=(double_exp val) const {
    return d_!=val.d_ || n_!=val.n_;
  }
  bool operator==(double val) const {
    return *this == double_exp(val);
  }
  bool operator!=(double val) const {
    return *this != double_exp(val);
  }
  bool operator==(int val) const {
    return *this == double_exp(val);
  }
  bool operator!=(int val) const {
    return *this != double_exp(val);
  }
  ///@}

  //----------------------------------------------------------------------
  ///@{
  /// greater and less than operators
  bool operator<(double_exp val) const {
    // subtraction is the simple approach but tests indicate that it's
    // about 2x slower than following the logic through to the end
    //return (*this-val).d_ < 0;

    // here comes the logic...
    if (d_ == 0.0) return val.d_ > 0.0;
    if (val.d_ == 0.0) return d_ < 0.0;
    // GPS 2020-10-22, for correct handling of nan ineqalities (always false)
    if (std::isnan(d_) || std::isnan(val.d_)) return false;
    bool this_neg = std::signbit(d_);
    bool val_neg  = std::signbit(val.d_);
    if (this_neg ^ val_neg) return this_neg;
    if (n_ == val.n_) return (d_<val.d_);
    return ((n_ < val.n_) ^ this_neg);
  }
  bool operator<=(double_exp val) const {
    // subtraction is the simple approach but tests indicate that it's
    // about 2x slower than following the logic through to the end
    //return (*this-val).d_ < 0;

    // here comes the logic...
    if (d_ == 0.0) return val.d_ >= 0.0;
    if (val.d_ == 0.0) return d_ <= 0.0;
    // GPS 2020-10-22, for correct handling of nan ineqalities (always false)
    if (std::isnan(d_) || std::isnan(val.d_)) return false;
    bool this_neg = std::signbit(d_);
    bool val_neg  = std::signbit(val.d_);
    if (this_neg ^ val_neg) return this_neg;
    if (n_ == val.n_) return (d_<=val.d_);
    return ((n_ < val.n_) ^ this_neg);
  }
  bool operator>(double_exp val) const {
    // subtraction is the simple approach but tests indicate that it's
    // about 2x slower than following the logic through to the end
    //return (*this-val).d_ < 0;

    // here comes the logic...
    if (d_ == 0.0) return val.d_ < 0.0;
    if (val.d_ == 0.0) return d_ > 0.0;
    // GPS 2020-10-22, for correct handling of nan ineqalities (always false)
    if (std::isnan(d_) || std::isnan(val.d_)) return false;
    bool this_neg = std::signbit(d_);
    bool val_neg  = std::signbit(val.d_);
    if (this_neg ^ val_neg) return !this_neg;
    if (n_ == val.n_) return (d_>val.d_);
    return ((n_ > val.n_) ^ this_neg);
  }
  bool operator>=(double_exp val) const {
    // subtraction is the simple approach but tests indicate that it's
    // about 2x slower than following the logic through to the end
    //return (*this-val).d_ < 0;

    // here comes the logic...
    if (d_ == 0.0) return val.d_ <= 0.0;
    if (val.d_ == 0.0) return d_ >= 0.0;
    // GPS 2020-10-22, for correct handling of nan ineqalities (always false)
    if (std::isnan(d_) || std::isnan(val.d_)) return false;
    bool this_neg = std::signbit(d_);
    bool val_neg  = std::signbit(val.d_);
    if (this_neg ^ val_neg) return !this_neg;
    if (n_ == val.n_) return (d_>=val.d_);
    return ((n_ > val.n_) ^ this_neg);
  }
  ///@}


  // when writing > & < operators, be careful of
  // issues when there are negative results.
  // Maybe first try with doubles?

  //----------------------------------------------------------------------
  ///@{
  /// special values

  /// the largest value that can be represented
  static constexpr double_exp max() {
    return double_exp(1.0 - std::numeric_limits<double>::epsilon() , max_n, false);
  }
  /// the smallest value that can be represented
  static constexpr double_exp min() {
    return double_exp(0.5, -max_n, false);
  }
  /// plus infinity
  static constexpr double_exp inf() {
    return double_exp(std::numeric_limits<double>::infinity(), max_n+1, false);
  }
  /// plus infinity
  static constexpr double_exp quiet_NaN() {
    return double_exp(std::numeric_limits<double>::quiet_NaN(), max_n+1, false);
  }
  static constexpr double_exp epsilon() {
    // building this manually to be able to provide a constexpr:
    // we observe that double gives 0.5,-51, that double::digits is -53
    // so build this in manually
    return double_exp(0.5, 2-std::numeric_limits<double>::digits, false);
  }
  ///@}

  //----------------------------------------------------------------------
  ///@{
  /// floor and ceil
  double_exp floor() const {
    // mostly use the double function
    if (internal_safe_double()) return std::floor(to_double());
    // if it's a very small number then be careful of the sign
    else if (n_ < 0) return d_ >= 0 ? 0.0 : -1.0;
    // and if it's very large, it's an integer by definition
    else             return *this;
  }
  double_exp ceil() const {
    // mostly use the double function
    if (internal_safe_double()) return std::ceil(to_double());
    // if it's a very small number then be careful of the sign
    else if (n_ < 0) return d_ > 0 ? 1.0 : 0.0;
    // and if it's very large, it's an integer by definition
    else             return *this;
  }
  ///@}

  //----------------------------------------------------------------------
  ///@{
  /// special functions
  double_exp sqrt() const {
    double d = d_;
    ExpType n = n_/2;
    ExpType ndiff = n_ - 2*n;
    if      (ndiff ==  1) d *= 2.0;
    else if (ndiff == -1) d *= 0.5;
    return double_exp(::sqrt(d),n);
  }

  double_exp log() const {
    return double_log();
  }

  // returns the logarithm of 1 + this
  double_exp log1p() const {
    // for very small numbers, the result is the just *this
    if (n_ <= -abs_max_double_exponent) return *this;
    // for very large numbers it's the same as the log
    if (n_ >=  abs_max_double_exponent) return this->log();
    // otherwise, the default log1p should do the job
    return std::log1p(to_double());
  }

  /// it's convenient to have a version that returns a double rather
  /// than a double_exp
  double double_log() const {
    return std::log(d_) + n_ * ln2;
  }

  /// it's convenient to have a version that returns a double rather
  /// than a double_exp and works on the absolute value
  double double_log_abs() const {
    return std::log(std::abs(d_)) + n_ * ln2;
  }

  double_exp exp() const {
    return exp_double(this->to_double());
  }

  /// a version that takes a double as an input argument, to avoid some
  /// pointless conversions to /from double_exp: if a value can't be
  /// represented in double then its exponential typically can't be
  /// represented in double_exp. 
  /// 
  /// Note that the relative accuracy of the result is of the order of
  /// val * numeric_limits<double>::epsilon(), which means that very
  /// large values of val will not give a particularly accurate result.
  static double_exp exp_double(double val) {
    if (fabs(val) > max_np1_ln2) {
      if (val > 0) return  double_exp::inf();
      else         return  double_exp();
    }
    ExpType npowers2 = ExpType(val * invln2);
    //double npowers2_double = val * invln2;
    //if (fabs(npowers2_double) > two_max_n) {
    //  if (npowers2_double > 0) return  double_exp::inf();
    //  else                     return  double_exp();
    //}
    //ExpType npowers2 = npowers2_double;
    double left_over = val - npowers2*ln2;
    return double_exp(std::exp(left_over), npowers2);
  }

  double_exp sin() const {
    if (internal_safe_double()) return std::sin(to_double());
    // otherwise handle case of very small number: sin(theta) = theta
    else if (n_ < 0) return *this;
    // for very large numbers, return sqrt(1/2), for both sin and cos
    else             return sqrthalf;
  }
  double_exp cos() const {
    if (internal_safe_double()) return std::cos(to_double());
    // otherwise handle case of very small number: cos(theta) = 1.0
    else if (n_ < 0) return 1.0;
    // for very large numbers, return sqrt(1/2), for both sin and cos
    else             return sqrthalf;
  }
  double_exp tan() const {
    if (internal_safe_double()) return std::tan(to_double());
    // otherwise handle case of very small number: tan(theta) = theta
    else if (n_ < 0) return *this;
    // for very large numbers, return 1, consistent with our choice for tan
    else             return 1.0;
  }

  double_exp asin() const {
    // handle case of small values on its own
    if (internal_underflow_double()) return *this;
    // large values convert to inf, which should then give nan
    else                             return std::asin(to_double());
  }

  double_exp acos() const {
    // no need for special handling of small values since they
    // just give 1
    return std::acos(to_double());
  }

  double_exp atan() const {
    // first handle small values 
    if (internal_underflow_double()) return *this;
    // normal values can go via std::atan, as can infinite
    // values, because sign of infinity tells us the correct answer
    return std::atan(to_double());
  }

  /// assuming *this is y, return atan2(y,x)
  double_exp atan2(double_exp x) const {
    // first handle case of zeros
    if (d_ == 0.0 || x.d_ == 0.0) return std::atan2(d_, x.d_);
    if (n_ > x.n_) {
      // except in cases with zeros (handled above), this means that |y| > |x| 
      return std::atan2(d_, std::ldexp(x.d_, x.n_ - n_));
    } else {
      // n_ <= x.n_
      ExpType n_minus_xn = n_-x.n_;
      // if (*this) is much smaller than x, then we 
      // handle two cases: 
      if (n_minus_xn < -abs_max_double_exponent) {
        // 1) x positive, in which case we use the small-angle
        //    formula for atan2
        if (x.d_ > 0.0) return double_exp(d_/x.d_, n_minus_xn);
        // 2) x negative, in which case we return +-pi, with 
        //    the sign determined by the sign of d_ (+pi is d_>0,
        //    -pi otherwise); we get pi from atan2 to
        else            return std::copysign(M_PI, d_);
        //else            return std::copysign(std::atan2(0.0,-1.0), d_);
      } else {
        return std::atan2(std::ldexp(d_, n_minus_xn), x.d_);
      }
    }
  }

  // get a template class that works for all integers
  // template<class T,
  //         typename std::enable_if<std::is_integral<T>::value> >
  //double_exp pow(T n) const {
  double_exp pow(int n) const {
    // special handling of case with n=2
    if (n == 2) return (*this)*(*this);

    // zero has a lot of cases, but since they either give
    // 0, 1 or +-infinity, let std::pow deal with them
    if (d_ == 0.0) return std::pow(d_,n);

    // otherwise, go via the logarithm,  with care 
    // as to sign handling (but is this safe)
    // (NB: we could have gone via pow operating on d_
    // and a multiplication operating on n_, but this 
    // could run into under/overflow)
    double_exp result = exp_double(double_log_abs() * n);
    if (std::signbit(d_) && (n & 1)) return -result;
    else                             return  result;
  }

  /// returns (*this) to the power p
  double_exp pow(double p) const {
    // handle the simple cases first
    // positive numbers are straightforward
    if (d_ > 0) return exp_double(double_log() * p);

    // zero has a lot of cases, but since they either give
    // 0, 1 or +-infinity, let std::pow deal with them
    if (d_ == 0.0) return std::pow(d_,p);

    // otherwise we have a negative number and need
    // to check if there is a non-integer part to the 
    // power.  
    double intpart;
    double fracpart = std::modf(p, &intpart);
    // If there is a non-integer part, we should return nan
    if (fracpart != 0.0) return double_exp::quiet_NaN();
    // otherwise we need to understand if the intpart is even or odd
    double intpart_halfp;
    double fracpart_halfp = std::modf(intpart*0.5, &intpart_halfp);
    double_exp result = exp_double(double_log_abs() * p);
    if (fracpart_halfp == 0) return  result;
    else                     return -result;
  }

  /// returns (*this) to the power p
  double_exp pow(double_exp p) const {
    // treat many complicated cases with the double routine
    if (std::abs(p.n_) < abs_max_double_exponent) return pow(p.to_double());
    // we are then left with situations where p is either very close
    // to zero, or p is very large.

    // first the case where p is very large; as with std::pow, we will assume
    // that p is whole and even, so that even if d is negative, the
    // result will be positive (keeping in mind that we'll probably end 
    // up with an infinite or zero answer...)
    if (p.n_ > 0) {
      return (p * double_log_abs()).exp();
    } else {
      return (p * double_log()).exp();
    }    
  }

  /// representing this number as 1.xxxx * exp(e), returns e
  /// (no benefit to having it as double_exp, so simply return a double)
  double  logb()  const { return n_-1.0; }
  /// same as logb() but returns integral type
  ExpType ilogb() const { return n_-1; }

  ///@}

  //------------------------------------------------
  ///@{
  /// Operattions with signs

  /// returns true if the sign of *this is negative
  bool signbit() const {return std::signbit(d_);}

  /// returns a number with the magnitude of *this and the sign of sign.
  double_exp copysign(double_exp sign) const {
    return double_exp(std::copysign(d_, sign.d_), n_, false);
  }
  //double_exp modp(double_exp * intpart) {
  //  if (abs(n_ )
  //}


  ///@}

  /// 
  /// https://en.cppreference.com/w/c/numeric/math/scalbn
  double_exp scalbn(int n) const {
    double_exp res = *this;
    res.n_ += n;
    res.renorm();
    return res;
  }

protected:



  /// sum of the two numbers when this->n_ is guaranteed
  /// larger than val.n_
  double_exp sum_a_ge_b(double_exp val) const {
    ExpType ndiff = val.n_ - n_;
    /// test
    if (d_ == 0) {
      // zero is a special case because n_ will be zero
      // This routine should only be called when val.n_ <= n_
      // so when val.d_==0 adding it will be safe.
      // But when d_==0 and ndiff is very large, we
      // may lose the ability to represent val.d_.
      // This forces is to test if d_ is zero and return
      // it.
      return val;
    } else if (ndiff >= -digits_plus_one) {
      // only carry out the addition if val is large enough
      // (2020-07-24: we also tested always carrying this
      // operation; it is perhaps a few percent faster in applications
      // where this "if" is always true, but appeared to add
      // a ~50% penalty in applications where the if condition
      // was often not satisfied)
      return double_exp(d_+ldexp(val.d_, ndiff),n_);
    } else {
      // if val appears to be small based on its exponent, watch out,
      // because exponents of NaNs (and sometimes infinities) so one
      // needs to ensure that one doesn't "lose" the non-finite number
      // by accident; this test has some cost
      //
      // As of 2020-10-22, we've improved the handling of inf in
      // renorm, so we no longer have to implement a special treatment
      // here
      return *this;
      //if (std::isfinite(val.d_)) return *this;
      //else return val;
    }
  }
  /// diff of the two numbers when this->n_ is guaranteed
  /// larger than val.n_ 
  // (see comments in sum_a_ge_b for rationale behind 
  // implementation choices)
  double_exp diff_a_ge_b(double_exp val) const {
    ExpType ndiff = val.n_ - n_;
    /// test
    if (d_ == 0) {
      // zero is a special case because n_ will be zero
      // This routine should only be called when val.n_ <= n_
      // so when val.d_==0 adding it will be safe.
      // But when d_==0 and ndiff is very large, we
      // may lose the ability to represent val.d_.
      // This forces is to test if d_ is zero and return
      // it.
      return -val;
    } else if (ndiff >= -digits_plus_one) {
      return double_exp(d_-ldexp(val.d_, ndiff),n_);
    } else {
      return *this;
      // special treatment of inf handled in renorm instead of here
      // (see comment in sum_a_ge_b)
      //if (std::isfinite(val.d_)) return *this;
      //else return -val;
    }
  }

  /// readjust sharing between d_ and n_ so that
  /// 0.5 <= d < 1, as for frexp
  void renorm() {
    if (d_ == 0) {
      n_ = 0;
    } else {
      // default action
      //
      // The initialisation of nn below is purely to prevent a (false
      // positive) gcc (-Wmaybe-uninitialized) warning. This extra
      // initialisation does not impact timings.
      int nn(0);
      d_ = frexp(d_,&nn);
      n_ += nn;
    }
    // check for overflow
    if (std::abs(n_) > max_n) {
      if (n_ < 0) {d_ = 0; n_ = 0;}
      else {
        // leave a nan as it is
        if (! std::isnan(d_)) d_ = std::copysign(std::numeric_limits<double>::infinity(),d_);
        n_ = max_n + 1;
      }
    }
    // 2020-07-24: currently non-finite numbers don't have a consistent
    // value for n_; we believe that the only place this could be
    // problematic is in addition and subtraction; we currently have
    // dedicated tests there, but alternatively one could simply require
    // non-finite numbers to have a consistent large exponent.
    // Experimentation suggests a ~10% speed penalty in real-life
    // applications if one chooses that route.
    //
    // 2020-10-22: we found other inf issues, in < and > , probably in
    // == as well and maybe in other functions. To simplify the
    // treatment of infinities and make the code more robust in the
    // longer run, we decided that it is safer to have a unique
    // representation of inf (inf, max_n+1)). We therefore re-activated
    // the line below (see 2020-10-21-speed.txt for more details on
    // timings). Note this also sets n=max_n for nan.
    if (!std::isfinite(d_)) n_= max_n + 1;
#ifdef DOUBLE_EXP_FORCE_DOUBLE_RANGE    
    if (n_ > std::numeric_limits<double>::max_exponent) throw std::runtime_error("double_exp::renorm: would give double overflow");
    if (n_ < std::numeric_limits<double>::min_exponent) throw std::runtime_error("double_exp::renorm: would give double underflow");
#endif // DOUBLE_EXP_FORCE_DOUBLE_RANGE
  }

  
  /// represents the mantissa; for good numbers, should satisfy 0.5 <= d < 1
  double d_;
  /// represents the exponent; for good numbers, should satisfy 
  /// abs(n) <= max_n
  ExpType n_;

  static constexpr double ln2  = 0.6931471805599453;
  static constexpr double invln2 = 1.4426950408889634;
  static constexpr double ln10 = 2.302585092994046;
  static constexpr double sqrttwo = 1.4142135623730951;
  static constexpr double sqrthalf = 0.7071067811865475;

  /// the choice of max_n is delicate: we basically want to make sure
  /// that for all simple arithmetic operations, we can detect afterwards
  /// if we under / over flowed by looking at n_, without worrying about
  /// integer overflow. The internal signal for overflow / underflow is
  /// if abs(n) > max_n and inf has max_n+1; so we want to be able to
  /// represent 2*(max_n+1) safely.
  static constexpr ExpType max_n = std::numeric_limits<ExpType>::max() / 4;
  static constexpr ExpType two_max_n = 2*max_n;
  /// this constant is used in the exp_double function, to ensure
  /// that the determination of the ExpType exponent is safe and within
  /// range. Note that we use (max_n+1) because the largest negative
  /// exponenent is -(max_n+1).
  static constexpr double max_np1_ln2 = (max_n+1)*ln2;
  //static constexpr ExpType max_n = 100000;


  /// the number of double digits plus one, which helps us
  /// in addition/subtraction -- if the numbers differ
  /// in their n_ by this or more, we can discard the one
  /// with smaller n_
  static constexpr ExpType digits_plus_one = 
                std::numeric_limits<double>::digits + 1;
  
  /// max_exponent is one more than the largest integer power of 2
  /// that can be represented; min_exponent is one more than the
  /// smallest integer power of 2 that can be represented; we define
  /// abs_max_double_exponent such that if |n_| < abs_max_double_exponent
  /// then it is safe to convert this number to double (e.g. for
  /// various complex operations)
  /// [NB: need to use explicit "?" because compiler rejects
  //  use of std::min or of home-made equivalent function]
  static constexpr ExpType abs_max_double_exponent = 
      (std::numeric_limits<double>::max_exponent < 
       -std::numeric_limits<double>::min_exponent) ?
       std::numeric_limits<double>::max_exponent : 
       -std::numeric_limits<double>::min_exponent;

  bool internal_safe_double() const {return std::abs(n_)<= abs_max_double_exponent;}
  /// returns true if *this will underflow as a double  
  bool internal_underflow_double() const {return n_ < -abs_max_double_exponent;}

  friend std::ostream & operator<<(std::ostream & ostr, const double_exp & dd);
  friend bool isinf(double_exp val);
  friend bool isnan(double_exp val);
  friend bool isfinite(double_exp val);
  friend double_exp char_to_double_exp(const char *);
};


inline double_exp char_to_double_exp(const char * literal) {
  const char * e = strchr(literal,'e');
  if (e == nullptr) return atof(literal);
  else {
    // we need to copy things into the buffer to get the 
    // part up to e in a null-terminated form
    const int n = 256;
    char buffer[n+1];
    assert(e - literal < n);
    std::copy(literal,e,buffer);
    buffer[e-literal] = 0;
    double prefix = atof(buffer);
    double exponent = atof(e+1);
    return double_exp::exp_double(double_exp::ln10 * exponent) * prefix;
  }
}

inline double_exp operator "" _de(const char * literal) {return char_to_double_exp(literal);}

double_exp inline operator*(double val1, double_exp val2) {return val2*val1;}
double_exp inline operator+(double val1, double_exp val2) {return val2+val1;}
double_exp inline operator-(double val1, double_exp val2) {return double_exp(val1)-val2;}
double_exp inline operator/(double val1, double_exp val2) {return double_exp(val1)/val2;}
bool inline operator< (double val1, double_exp val2) {return val2 >  val1;}
bool inline operator> (double val1, double_exp val2) {return val2 <  val1;}
bool inline operator<=(double val1, double_exp val2) {return val2 >= val1;}
bool inline operator>=(double val1, double_exp val2) {return val2 <= val1;}

inline bool isinf   (double_exp val) {return std::isinf   (val.d_);}
inline bool isnan   (double_exp val) {return std::isnan   (val.d_);}
inline bool isfinite(double_exp val) {return std::isfinite(val.d_);}

namespace std {

  inline bool signbit(double_exp val) {return val.signbit();}
  template<> inline constexpr double_exp numeric_limits<double_exp>::max() noexcept {
    return double_exp::max();
  }
  template<> inline constexpr double_exp numeric_limits<double_exp>::min() noexcept {
    return double_exp::min();
  }
  template<> inline constexpr double_exp numeric_limits<double_exp>::epsilon() noexcept {
    return double_exp::epsilon();
  }
  template<> inline constexpr double_exp numeric_limits<double_exp>::infinity() noexcept {
    return double_exp::inf();
  }
  template<> inline constexpr double_exp numeric_limits<double_exp>::quiet_NaN() noexcept {
    return double_exp::quiet_NaN();
  }


  // GPS 2020-04-01: could not get the following to work
  //template<> const int numeric_limits<double_exp>::digits = numeric_limits<double>::digits;
}

inline double_exp floor(double_exp val) {return val.floor();}
inline double_exp ceil (double_exp val) {return val.ceil ();}
inline double_exp abs  (double_exp val) {return val.abs();}
inline double_exp fabs (double_exp val) {return val.abs();}
inline double_exp sqrt (double_exp val) {return val.sqrt();}
inline double_exp log  (double_exp val) {return val.log(); }
inline double_exp log1p(double_exp val) {return val.log1p();}
inline double_exp exp  (double_exp val) {return val.exp(); }
inline double_exp sin  (double_exp val) {return val.sin(); }
inline double_exp cos  (double_exp val) {return val.cos(); }
inline double_exp tan  (double_exp val) {return val.tan(); }
inline double_exp asin  (double_exp val) {return val.asin(); }
inline double_exp acos  (double_exp val) {return val.acos(); }
inline double_exp atan  (double_exp val) {return val.atan(); }
inline double_exp atan2(double_exp y, double_exp x) {return y.atan2(x);}
template <class T> inline double_exp pow(double_exp val, T p) {return val.pow(p);}

inline double_exp fmax(double_exp x, double_exp y) {return std::max(x,y);}
inline double_exp fmin(double_exp x, double_exp y) {return std::min(x,y);}
inline double_exp scalbn(double_exp x, int n) {return x.scalbn(n);}
inline double_exp hypot(double_exp x, double_exp y) {return sqrt(x*x+y*y);}
inline double_exp copysign(double_exp x, double_exp y) {return x.copysign(y);}
inline double logb(double_exp x) {return x.logb();}
inline double_exp::ExpType ilogb(double_exp x) {return x.ilogb();}

namespace std {
  using ::floor;
  using ::ceil;
  using ::abs;
  using ::fabs;
  using ::sqrt;
  using ::log;
  using ::log1p;
  using ::exp;
  using ::sin;
  using ::cos;
  using ::tan;
  using ::asin;
  using ::acos;
  using ::atan;
  using ::atan2;
  using ::pow;

  using ::hypot;
  using ::fmax;
  using ::fmin;
  using ::copysign;
  using ::scalbn;
  using ::logb;

  using ::isinf;
  using ::isnan;
  using ::isfinite;
}

/// output
inline std::ostream & operator<<(std::ostream & ostr, const double_exp & dd) {
  // GS: one should handle the inf and nan first because
  // is_finite_double says it can't handle inf or nan.
  // 
  // Idea: if one properly handles "inf" by setting n-nmax+1, one
  // could simply replace the following line and function by
  // is_finite_double.
  // 
  // GPS: conceivably, as of 2020-10-22 we could replace this with 
  //         dd.is_finite_double() || !dd.isfinite() 
  // and then avoid having to worry about the isinf and isnan
  // checks below.
  if (dd.is_finite_double()) {
    ostr << double(dd);
    return ostr;
  } 
  // otherwise resort to our home-made handling
  if (dd.d_ == 0) {
    ostr << 0.0;
  } else if (std::isinf(dd.d_)) {
    if (std::signbit(dd.d_)) ostr << "-";
    ostr << "inf";
  } else if (std::isnan(dd.d_)) {
    ostr << "nan";
  } else {
    double lnval = std::log(std::abs(dd.d_)) + dd.n_ * double_exp::ln2;
    double log10val = lnval/double_exp::ln10;
    double pow10 = std::floor(log10val);
    double norm = std::pow(10.0,log10val-pow10);
    if (dd.d_ < 0) ostr << "-";
    ostr << norm << "e" << pow10;
    //ostr << "(=" << dd.to_double() << ")";
  }
  return ostr;
}

/// input.
inline std::istream & operator>>(std::istream & istr, double_exp & dd) {
  // NB: this is not quite final, in that it behaves a little differently
  // from a normal istr>>d in double, because the istr>>word reads things
  // including commas, which istr>>double would not normally do.
  // With the function as it stands currently
  std::string word;
  istr >> word;
  dd = char_to_double_exp(word.c_str());
  return istr;
}

#endif // __double_exp_hh__
