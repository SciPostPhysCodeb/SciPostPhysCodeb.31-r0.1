// file generated automatically by ./twotypes.py
// auto generated from ^template.+operator\+\(, replacing + -> -
template<class A, class B> TwoTypes<A,B> operator-(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  TwoTypes<A,B> result(x.a - y.a, x.b - y.b);
  if (!result.check_OK()) result.binary_error("operator-",x,y);
  return result;
 }
template<class A, class B> TwoTypes<A,B> operator-(TwoTypes<A,B> x, double y) {
  return x - TwoTypes<A,B>(y);
 }
template<class A, class B> TwoTypes<A,B> operator-(double x, TwoTypes<A,B> y) {
  return TwoTypes<A,B>(x) - y;
}
// auto generated from ^template.+operator\+\(, replacing + -> *
template<class A, class B> TwoTypes<A,B> operator*(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  TwoTypes<A,B> result(x.a * y.a, x.b * y.b);
  if (!result.check_OK()) result.binary_error("operator*",x,y);
  return result;
 }
template<class A, class B> TwoTypes<A,B> operator*(TwoTypes<A,B> x, double y) {
  return x * TwoTypes<A,B>(y);
 }
template<class A, class B> TwoTypes<A,B> operator*(double x, TwoTypes<A,B> y) {
  return TwoTypes<A,B>(x) * y;
}
// auto generated from ^template.+operator\+\(, replacing + -> /
template<class A, class B> TwoTypes<A,B> operator/(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  TwoTypes<A,B> result(x.a / y.a, x.b / y.b);
  if (!result.check_OK()) result.binary_error("operator/",x,y);
  return result;
 }
template<class A, class B> TwoTypes<A,B> operator/(TwoTypes<A,B> x, double y) {
  return x / TwoTypes<A,B>(y);
 }
template<class A, class B> TwoTypes<A,B> operator/(double x, TwoTypes<A,B> y) {
  return TwoTypes<A,B>(x) / y;
}
// auto generated from ^template.+operator\+\=\(, replacing + -> -
template<class A, class B> TwoTypes<A,B> & TwoTypes<A,B>::operator-=(TwoTypes<A,B> y) {
  a -= y.a;
  b -= y.b;
  if (!check_OK()) unary_error("operator-=",y);
  return *this;
}
// auto generated from ^template.+operator\+\=\(, replacing + -> *
template<class A, class B> TwoTypes<A,B> & TwoTypes<A,B>::operator*=(TwoTypes<A,B> y) {
  a *= y.a;
  b *= y.b;
  if (!check_OK()) unary_error("operator*=",y);
  return *this;
}
// auto generated from ^template.+operator\+\=\(, replacing + -> /
template<class A, class B> TwoTypes<A,B> & TwoTypes<A,B>::operator/=(TwoTypes<A,B> y) {
  a /= y.a;
  b /= y.b;
  if (!check_OK()) unary_error("operator/=",y);
  return *this;
}
// auto generated from ^template.+operator\<\(, replacing operator< -> operator>
template<class A, class B> bool operator>(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  bool result_a = x.a > y.a;
  bool result_b = x.b > y.b;
  if (result_a != result_b) TwoTypes<A,B>::binary_bool_error("operator>",x,y, result_a, result_b);
  return result_a;
 }
template<class A, class B> bool operator>(TwoTypes<A,B> x, double y) {
  return x > TwoTypes<A,B>(y);
 }
template<class A, class B> bool operator>(double x, TwoTypes<A,B> y) {
  return TwoTypes<A,B>(x) > y;
}
// auto generated from ^template.+operator\<\(, replacing operator< -> operator>=
template<class A, class B> bool operator>=(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  bool result_a = x.a >= y.a;
  bool result_b = x.b >= y.b;
  if (result_a != result_b) TwoTypes<A,B>::binary_bool_error("operator>=",x,y, result_a, result_b);
  return result_a;
 }
template<class A, class B> bool operator>=(TwoTypes<A,B> x, double y) {
  return x >= TwoTypes<A,B>(y);
 }
template<class A, class B> bool operator>=(double x, TwoTypes<A,B> y) {
  return TwoTypes<A,B>(x) >= y;
}
// auto generated from ^template.+operator\<\(, replacing operator< -> operator<=
template<class A, class B> bool operator<=(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  bool result_a = x.a <= y.a;
  bool result_b = x.b <= y.b;
  if (result_a != result_b) TwoTypes<A,B>::binary_bool_error("operator<=",x,y, result_a, result_b);
  return result_a;
 }
template<class A, class B> bool operator<=(TwoTypes<A,B> x, double y) {
  return x <= TwoTypes<A,B>(y);
 }
template<class A, class B> bool operator<=(double x, TwoTypes<A,B> y) {
  return TwoTypes<A,B>(x) <= y;
}
// auto generated from ^template.+operator\<\(, replacing operator< -> operator==
template<class A, class B> bool operator==(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  bool result_a = x.a == y.a;
  bool result_b = x.b == y.b;
  if (result_a != result_b) TwoTypes<A,B>::binary_bool_error("operator==",x,y, result_a, result_b);
  return result_a;
 }
template<class A, class B> bool operator==(TwoTypes<A,B> x, double y) {
  return x == TwoTypes<A,B>(y);
 }
template<class A, class B> bool operator==(double x, TwoTypes<A,B> y) {
  return TwoTypes<A,B>(x) == y;
}
// auto generated from ^template.+operator\<\(, replacing operator< -> operator!=
template<class A, class B> bool operator!=(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  bool result_a = x.a != y.a;
  bool result_b = x.b != y.b;
  if (result_a != result_b) TwoTypes<A,B>::binary_bool_error("operator!=",x,y, result_a, result_b);
  return result_a;
 }
template<class A, class B> bool operator!=(TwoTypes<A,B> x, double y) {
  return x != TwoTypes<A,B>(y);
 }
template<class A, class B> bool operator!=(double x, TwoTypes<A,B> y) {
  return TwoTypes<A,B>(x) != y;
}
// auto generated from ^template.+floor\(, replacing floor -> ceil 
template<class A, class B> TwoTypes<A,B> ceil(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::ceil(y.a), std::ceil(y.b));
  if (!result.check_OK()) result.unary_error("ceil",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> abs  
template<class A, class B> TwoTypes<A,B> abs(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::abs(y.a), std::abs(y.b));
  if (!result.check_OK()) result.unary_error("abs",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> fabs 
template<class A, class B> TwoTypes<A,B> fabs(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::fabs(y.a), std::fabs(y.b));
  if (!result.check_OK()) result.unary_error("fabs",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> sqrt 
template<class A, class B> TwoTypes<A,B> sqrt(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::sqrt(y.a), std::sqrt(y.b));
  if (!result.check_OK()) result.unary_error("sqrt",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> log  
template<class A, class B> TwoTypes<A,B> log(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::log(y.a), std::log(y.b));
  if (!result.check_OK()) result.unary_error("log",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> log1p
template<class A, class B> TwoTypes<A,B> log1p(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::log1p(y.a), std::log1p(y.b));
  if (!result.check_OK()) result.unary_error("log1p",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> exp  
template<class A, class B> TwoTypes<A,B> exp(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::exp(y.a), std::exp(y.b));
  if (!result.check_OK()) result.unary_error("exp",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> sin  
template<class A, class B> TwoTypes<A,B> sin(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::sin(y.a), std::sin(y.b));
  if (!result.check_OK()) result.unary_error("sin",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> cos  
template<class A, class B> TwoTypes<A,B> cos(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::cos(y.a), std::cos(y.b));
  if (!result.check_OK()) result.unary_error("cos",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> tan  
template<class A, class B> TwoTypes<A,B> tan(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::tan(y.a), std::tan(y.b));
  if (!result.check_OK()) result.unary_error("tan",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> asin 
template<class A, class B> TwoTypes<A,B> asin(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::asin(y.a), std::asin(y.b));
  if (!result.check_OK()) result.unary_error("asin",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> acos 
template<class A, class B> TwoTypes<A,B> acos(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::acos(y.a), std::acos(y.b));
  if (!result.check_OK()) result.unary_error("acos",y);
  return result;
}
// auto generated from ^template.+floor\(, replacing floor -> atan 
template<class A, class B> TwoTypes<A,B> atan(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::atan(y.a), std::atan(y.b));
  if (!result.check_OK()) result.unary_error("atan",y);
  return result;
}
// auto generated from ^template.+pow\(, replacing pow -> atan2
template<class A, class B> TwoTypes<A,B> atan2(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::atan2(x.a,y.a), std::atan2(x.b,y.b));
  if (!result.check_OK()) result.binary_error("atan2",x,y);
  return result;
 }
template<class A, class B> TwoTypes<A,B> atan2(TwoTypes<A,B> x, double y) {
  return atan2(x, TwoTypes<A,B>(y));
 }
template<class A, class B> TwoTypes<A,B> atan2(double x, TwoTypes<A,B> y) {
  return atan2(TwoTypes<A,B>(x), y);
}
namespace std {
  using ::floor;
  using ::ceil ;
  using ::abs  ;
  using ::fabs ;
  using ::sqrt ;
  using ::log  ;
  using ::log1p;
  using ::exp  ;
  using ::sin  ;
  using ::cos  ;
  using ::tan  ;
  using ::asin ;
  using ::acos ;
  using ::atan ;
  using ::pow;
  using ::atan2;
}

