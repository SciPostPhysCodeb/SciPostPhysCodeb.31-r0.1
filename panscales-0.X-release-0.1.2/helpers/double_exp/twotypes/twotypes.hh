#ifndef __TWOTYPES_HH__
#define __TWOTYPES_HH__

#include <iostream>
#include <cmath>

// // from https://gist.github.com/fmela/591333
// #include <execinfo.h> // for backtrace
// #include <dlfcn.h>    // for dladdr
// #include <cxxabi.h>   // for __cxa_demangle
// 
// #include <cstdio>
// #include <cstdlib>
// #include <string>
// #include <sstream>
// 
// // This function produces a stack backtrace with demangled function & method names.
// inline std::string Backtrace(int skip = 1)
// {
//     void *callstack[128];
//     const int nMaxFrames = sizeof(callstack) / sizeof(callstack[0]);
//     char buf[1024];
//     int nFrames = backtrace(callstack, nMaxFrames);
//     char **symbols = backtrace_symbols(callstack, nFrames);
// 
//     std::ostringstream trace_buf;
//     for (int i = skip; i < nFrames; i++) {
//         printf("%s\n", symbols[i]);
// 
//         Dl_info info;
//         if (dladdr(callstack[i], &info) && info.dli_sname) {
//             char *demangled = NULL;
//             int status = -1;
//             if (info.dli_sname[0] == '_')
//                 demangled = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);
//             snprintf(buf, sizeof(buf), "%-3d %*p %s + %zd\n",
//                      i, int(2 + sizeof(void*) * 2), callstack[i],
//                      status == 0 ? demangled :
//                      info.dli_sname == 0 ? symbols[i] : info.dli_sname,
//                      (char *)callstack[i] - (char *)info.dli_saddr);
//             free(demangled);
//         } else {
//             snprintf(buf, sizeof(buf), "%-3d %*p %s\n",
//                      i, int(2 + sizeof(void*) * 2), callstack[i], symbols[i]);
//         }
//         trace_buf << buf;
//     }
//     free(symbols);
//     if (nFrames == nMaxFrames)
//         trace_buf << "[truncated]\n";
//     return trace_buf.str();
// }


template <class A, class B> 
class TwoTypes {
public:     

  TwoTypes(double x) : a(x), b(x) {}
  TwoTypes() : a(0.0), b(0.0) {}
  constexpr TwoTypes(A a_in, B b_in): a(a_in), b(b_in) {}

  explicit operator double() const {return double(a);}

  A a;
  B b;

  static void set_threshold(double t) {threshold = t;}

  static double threshold;

  // bool check_OK() {return std::abs(to_double(a)-to_double(b)) 
  //                     < threshold * std::max(abs(to_double(a)), abs(to_double(b)));}
  bool check_OK() {return std::abs(a-b) 
                      <= threshold * std::max(abs(a), abs(b));}

  void binary_error(const std::string & op, TwoTypes x, TwoTypes y) {
    std::cerr << "TwoTypes check fails: " << op << "( " << x << " , " << y << " ) = " << *this << "\n";
    //Backtrace();
  }
  void unary_error(const std::string & op, TwoTypes x) {
    std::cerr << "TwoTypes check fails: " << op << "( " << x << " ) = " << *this << "\n";
    //Backtrace();
  }

  static void binary_bool_error(const std::string & op, TwoTypes x, TwoTypes y, 
                                bool result_a, bool result_b)  {
    std::streamsize prec = std::cerr.precision();
    std::cerr.precision(16);
    std::cerr << "TwoTypes check fails: " 
              << op << "( " << x.a << " , " << y.a << " ) = " << result_a << " BUT "
              << op << "( " << x.b << " , " << y.b << " ) = " << result_b << "\n";
    std::cerr.precision(prec);
    //Backtrace();
  }

  TwoTypes & operator+=(TwoTypes y);
  TwoTypes & operator-=(TwoTypes y);
  TwoTypes & operator/=(TwoTypes y);
  TwoTypes & operator*=(TwoTypes y);

};

template<class A, class B> 
double TwoTypes<A,B>::threshold = 1e-10;


template<class A, class B> 
std::ostream & operator<<(std::ostream & ostr, TwoTypes<A,B> x) {
  std::streamsize prec = ostr.precision();
  ostr.precision(16);
  ostr << x.a << "(" << x.b << ")";
  ostr.precision(prec);
  return ostr;
}


// this function acts as a basis for the twotypes.py script to create a
// further set of operators (binary built-ins); the template and
// operator+ strings must be on the same line and the closing "}" must
// not have any spaces preceding it
template<class A, class B> TwoTypes<A,B> operator+(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  TwoTypes<A,B> result(x.a + y.a, x.b + y.b);
  if (!result.check_OK()) result.binary_error("operator+",x,y);
  return result;
 }
template<class A, class B> TwoTypes<A,B> operator+(TwoTypes<A,B> x, double y) {
  return x + TwoTypes<A,B>(y);
 }
template<class A, class B> TwoTypes<A,B> operator+(double x, TwoTypes<A,B> y) {
  return TwoTypes<A,B>(x) + y;
}

// this function acts as a basis for the twotypes.py script to 
// create a further set of operators (binaries that give bool);
// The replacement function will look for " < " and replace
// that as well as replacing "operator<" (it needs to be
// careful not to replace ">" of the template)
template<class A, class B> bool operator<(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  bool result_a = x.a < y.a;
  bool result_b = x.b < y.b;
  if (result_a != result_b) TwoTypes<A,B>::binary_bool_error("operator<",x,y, result_a, result_b);
  return result_a;
 }
template<class A, class B> bool operator<(TwoTypes<A,B> x, double y) {
  return x < TwoTypes<A,B>(y);
 }
template<class A, class B> bool operator<(double x, TwoTypes<A,B> y) {
  return TwoTypes<A,B>(x) < y;
}


// this function acts as a basis for the twotypes.py script to 
// create a further set of operators (+= etc series)
template<class A, class B> TwoTypes<A,B> & TwoTypes<A,B>::operator+=(TwoTypes<A,B> y) {
  a += y.a;
  b += y.b;
  if (!check_OK()) unary_error("operator+=",y);
  return *this;
}
// template<class A, class B> TwoTypes<A,B> & TwoTypes<A,B>::operator+=(double y) {
//   *this += TwoTypes<A,B>(y);
// }

// this function acts as a basis for the twotypes.py script to 
// create a further set of operators (function unaries)
template<class A, class B> TwoTypes<A,B> floor(TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::floor(y.a), std::floor(y.b));
  if (!result.check_OK()) result.unary_error("floor",y);
  return result;
}

// this function acts as a basis for the twotypes.py script to 
// create a further set of operators (function binaries)
template<class A, class B> TwoTypes<A,B> pow(TwoTypes<A,B> x, TwoTypes<A,B> y) {
  TwoTypes<A,B> result(std::pow(x.a,y.a), std::pow(x.b,y.b));
  if (!result.check_OK()) result.binary_error("pow",x,y);
  return result;
 }
template<class A, class B> TwoTypes<A,B> pow(TwoTypes<A,B> x, double y) {
  return pow(x, TwoTypes<A,B>(y));
 }
template<class A, class B> TwoTypes<A,B> pow(double x, TwoTypes<A,B> y) {
  return pow(TwoTypes<A,B>(x), y);
}

// not a template (case with int)
template<class A, class B> TwoTypes<A,B> pow(TwoTypes<A,B> x, int n) {
  TwoTypes<A,B> result(std::pow(x.a,n), std::pow(x.b,n));
  if (!result.check_OK()) result.binary_error("pow",x,TwoTypes<A,B>(double(n)));
  return result;
}

template<class A, class B> TwoTypes<A,B> operator-(TwoTypes<A,B> x) {
  TwoTypes<A,B> result(-x.a, -x.b);
  if (!result.check_OK()) result.unary_error("operator-",x);
  return result;
}
template<class A, class B> TwoTypes<A,B> operator+(TwoTypes<A,B> x) {
  TwoTypes<A,B> result(+x.a, +x.b);
  if (!result.check_OK()) result.unary_error("operator+",x);
  return result;
}

#include "twotypes_inc.hh"


/*
 * Copyright (c) 2009-2017, Farooq Mela
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#endif // __TWOTYPES_HH__