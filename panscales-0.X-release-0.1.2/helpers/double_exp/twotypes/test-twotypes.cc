#include <iostream>
#include "twotypes.hh"
#include "../double_exp.hh"

using namespace std;

// inline double to_double(double x) {return x;}
// inline double to_double(float x) {return x;}

typedef TwoTypes<float,double> df;
namespace std {
  template<> constexpr df numeric_limits<df>::epsilon() noexcept {
    return df(numeric_limits<float>::epsilon(), numeric_limits<float>::epsilon());
  }
}
template<>
double TwoTypes<float,double>::threshold = 1e-10;
template<> bool df::check_OK() {
  return std::abs(a-b) < threshold*std::max(abs(double(a)),abs(double(b)));
} 
//typedef double_exp df;





int main() {
  df x(4.0);
  df y(5.1);
  df xx(4.00000000001);
  auto a1 = x+y;
  auto a2 = -y;

  cout << (x<xx) << endl;

  cout << (x > 0.0) << endl;

  y += x;
}