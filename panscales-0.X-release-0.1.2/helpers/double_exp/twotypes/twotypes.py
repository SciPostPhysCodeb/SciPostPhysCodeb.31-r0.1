#!/usr/bin/env python3
import re
import sys

unaries = (
    'floor',
    'ceil ',
    'abs  ',
    'fabs ',
    'sqrt ',
    'log  ',
    'log1p',
    'exp  ',
    'sin  ',
    'cos  ',
    'tan  ',
    'asin ',
    'acos ',
    'atan ',
)

binaries = (
    'pow',
    'atan2',
)

binaries_bool = (
    'operator>',
    'operator<',
    'operator>=',
    'operator<=',
    'operator==',
    'operator!=',
)



def main():
    newfns = f"// file generated automatically by {' '.join(sys.argv)}\n"
    newfns += get_newfunctions(r'^template.+operator\+\(', "+", "-*/")
    newfns += get_newfunctions(r'^template.+operator\+\=\(', "+", "-*/")
    newfns += get_newfunctions(r'^template.+operator\<\(', "operator<", binaries_bool)
    newfns += get_newfunctions(r'^template.+floor\(', "floor", unaries)
    newfns += get_newfunctions(r'^template.+pow\(', "pow", binaries)
    newfns += "namespace std {\n"

    for fn in unaries + binaries:
        newfns += f"  using ::{fn};\n"
    newfns += "}\n"
    
    out = open("twotypes_inc.hh","w")
    print (newfns, file=out)


def get_newfunctions(template_regexp, to_replace, with_list):
    newfns = ""
    fn = get_function(template_regexp)
    for op in with_list:
        #print(to_replace,op.strip())
        newfn = fn.replace(to_replace,op.strip())
        # with things like operator< we have to be careful to replace 
        m = re.match(r'operator(.*)',op)
        if m:
            opsymbol = m.group(1)
            torep = to_replace.replace('operator','')
            #print (torep, opsymbol)
            newfn = newfn.replace(f" {torep} ", f" {opsymbol} ")
        # don't duplicate the function if we get something identical
        # to what we have already
        if (newfn != fn):
            newfns += f"// auto generated from {template_regexp}, replacing {to_replace} -> {op}\n"
            newfns += newfn
    return newfns

def get_function(regexp):
    input = open('twotypes.hh','r')
    function = ''
    started=False
    for line in input:
        if not started and not function and re.search(regexp,line):
            started = True
        if started: function += line
        if line.rstrip() == "}": started = False
        #if re.match(r'\{', line): started = False

    return(function)

if __name__ == '__main__': main()