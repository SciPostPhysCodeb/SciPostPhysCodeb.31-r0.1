#!/usr/bin/env bash
#
# Script to run speed tests and register results in a file.

mainfile=log-speed-tests/results.dat
outfile=log-speed-tests/results.dat.tmp

# if outfile exists, recreate it from scratch
if [ -f $outfile ]; then
    rm $outfile
fi
touch $outfile

echo '----------------------------------------------------------' | tee -a $outfile
date | tee -a $outfile
# get git commit hash
git rev-parse HEAD | tee -a $outfile
echo $git_hash | tee -a $outfile
# register any uncommitted file-changes
git status -uno --porcelain | tee -a $outfile

# get info on system
uname -a | tee -a $outfile
(sysctl -n machdep.cpu.brand_string || grep -m 1 'model name' /proc/cpuinfo) | tee -a $outfile
gcc --version | tee -a $outfile
uptime | tee -a $outfile

# make sure things are built
make -j
./speed-tests | tee -a $outfile

# append results to the main file
mv $mainfile $mainfile.old
cat $outfile $mainfile.old > $mainfile