    0           TestTimesEqual<double, double> time/op =   1.11 ns, result =     22026.4
    1                 TestMult<double, double> time/op =   1.05 ns, result =     22026.4
    2              TestTwoMult<double, double> time/op =   1.97 ns, result =  4.8516e+08
    3                  TestDiv<double, double> time/op =   3.35 ns, result = 4.54002e-05
    4             TestDivEqual<double, double> time/op =   3.14 ns, result = 4.54002e-05
    5                 TestPlus<double, double> time/op =  0.927 ns, result =          11
    6            TestPlusEqual<double, double> time/op =  0.892 ns, result =       1e+07
    7                TestMinus<double, double> time/op =  0.888 ns, result =      -99999
    8               TestMultLT<double, double> time/op =   1.57 ns, result = 4.99998e+06
    9             TestSqrtMult<double, double> time/op =      5 ns, result =        1.01
   10              TestLogMult<double, double> time/op =     12 ns, result =      1.5219
   11            TestLog1pMult<double, double> time/op =   14.4 ns, result =     1.90893
   12              TestExpPlus<double, double> time/op =   4.94 ns, result =      1e+100
   13              TestPlusPow<double, double> time/op =   15.2 ns, result =  1.0027e+23
   14             TestPlusPow2<double, double> time/op =  0.875 ns, result = 3.33333e+10
   15             TestPlusPow3<double, double> time/op =   15.1 ns, result =     2.5e+12
   16              TestPlusSin<double, double> time/op =   7.85 ns, result =     13768.4
   17              TestPlusCos<double, double> time/op =    7.8 ns, result =    -50636.5
   18              TestPlusTan<double, double> time/op =   11.7 ns, result = 7.89929e+07
   19            TestPlusATan2<double, double> time/op =     19 ns, result =     5477.23

   21   TestTimesEqual<double_exp, double_exp> time/op =    3.1 ns, result =     22026.4   2.81x  ratio=1
   22         TestMult<double_exp, double_exp> time/op =   3.15 ns, result =     22026.4   3.01x  ratio=1
   23      TestTwoMult<double_exp, double_exp> time/op =   6.22 ns, result =  4.8516e+08   3.15x  ratio=1
   24          TestDiv<double_exp, double_exp> time/op =   5.54 ns, result = 4.54002e-05   1.65x  ratio=1
   25     TestDivEqual<double_exp, double_exp> time/op =   5.17 ns, result = 4.54002e-05   1.65x  ratio=1
   26         TestPlus<double_exp, double_exp> time/op =   7.04 ns, result =          11    7.6x  ratio=1
   27    TestPlusEqual<double_exp, double_exp> time/op =   7.87 ns, result =       1e+07   8.83x  ratio=1
   28        TestMinus<double_exp, double_exp> time/op =    7.4 ns, result =      -99999   8.33x  ratio=1
   29       TestMultLT<double_exp, double_exp> time/op =      4 ns, result = 4.99998e+06   2.55x  ratio=1
   30     TestSqrtMult<double_exp, double_exp> time/op =     10 ns, result =        1.01   2.01x  ratio=1
   31      TestLogMult<double_exp, double_exp> time/op =   17.7 ns, result =      1.5219   1.47x  ratio=1
   32    TestLog1pMult<double_exp, double_exp> time/op =   22.2 ns, result =     1.90893   1.55x  ratio=1
   33      TestExpPlus<double_exp, double_exp> time/op =   20.4 ns, result =      1e+100   4.12x  ratio=1
   34      TestPlusPow<double_exp, double_exp> time/op =   41.5 ns, result =  1.0027e+23   2.73x  ratio=1
   35     TestPlusPow2<double_exp, double_exp> time/op =   11.3 ns, result = 3.33333e+10   12.9x  ratio=1
   36     TestPlusPow3<double_exp, double_exp> time/op =     39 ns, result =     2.5e+12   2.59x  ratio=1
   37      TestPlusSin<double_exp, double_exp> time/op =   30.4 ns, result =     13768.4   3.87x  ratio=1
   38      TestPlusCos<double_exp, double_exp> time/op =   29.5 ns, result =    -50636.5   3.78x  ratio=1
   39      TestPlusTan<double_exp, double_exp> time/op =   33.4 ns, result = 7.89929e+07   2.86x  ratio=1
   40    TestPlusATan2<double_exp, double_exp> time/op =   31.2 ns, result =     5477.23   1.64x  ratio=1

   42       TestTimesEqual<double_exp, double> time/op =   3.17 ns, result =     22026.4   2.87x  ratio=1
   43             TestMult<double_exp, double> time/op =   3.17 ns, result =     22026.4   3.03x  ratio=1
   44          TestTwoMult<double_exp, double> time/op =   6.37 ns, result =  4.8516e+08   3.23x  ratio=1
   45              TestDiv<double_exp, double> time/op =   5.12 ns, result = 4.54002e-05   1.53x  ratio=1
   46         TestDivEqual<double_exp, double> time/op =   5.25 ns, result = 4.54002e-05   1.67x  ratio=1
   47             TestPlus<double_exp, double> time/op =   8.76 ns, result =          11   9.45x  ratio=1
   48        TestPlusEqual<double_exp, double> time/op =   8.46 ns, result =       1e+07   9.48x  ratio=1
   49            TestMinus<double_exp, double> time/op =   9.23 ns, result =      -99999   10.4x  ratio=1
   50           TestMultLT<double_exp, double> time/op =   6.16 ns, result = 4.99998e+06   3.93x  ratio=1
   51         TestSqrtMult<double_exp, double> time/op =   10.1 ns, result =        1.01   2.02x  ratio=1
   52          TestLogMult<double_exp, double> time/op =   18.3 ns, result =      1.5219   1.52x  ratio=1
   53        TestLog1pMult<double_exp, double> time/op =   22.4 ns, result =     1.90893   1.56x  ratio=1
   54          TestExpPlus<double_exp, double> time/op =   19.3 ns, result =      1e+100   3.91x  ratio=1
   55          TestPlusPow<double_exp, double> time/op =     36 ns, result =  1.0027e+23   2.37x  ratio=1
   56         TestPlusPow2<double_exp, double> time/op =   11.5 ns, result = 3.33333e+10   13.2x  ratio=1
   57         TestPlusPow3<double_exp, double> time/op =   39.9 ns, result =     2.5e+12   2.65x  ratio=1
   58          TestPlusSin<double_exp, double> time/op =   29.9 ns, result =     13768.4   3.81x  ratio=1
   59          TestPlusCos<double_exp, double> time/op =     30 ns, result =    -50636.5   3.85x  ratio=1
   60          TestPlusTan<double_exp, double> time/op =     34 ns, result = 7.89929e+07   2.92x  ratio=1
   61        TestPlusATan2<double_exp, double> time/op =   30.1 ns, result =     5477.23   1.58x  ratio=1

