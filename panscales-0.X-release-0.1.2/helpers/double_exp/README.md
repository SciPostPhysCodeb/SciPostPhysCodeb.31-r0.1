double_exp type
===============

A floating-point type that consists of a double for the mantissa and an
integer (int32_t or int64_t) for the exponent. This allows a large range
for the exponent. 

Usage
-----
It comes as a header-only implementation, get it with

```#include "double_exp.hh"```

Tests
-----
Testing code includes

- speed-tests.cc   - for checking speed
- unit-tests/*     - a whole set of unit tests

Points to be aware of
---------------------

For a number whose logarithm is L, there can be situations where
the relative accuracy of a calculation ends up being epsilon*L rather
than just epsilon.

Specifically, multiplications and additions should be full accuracy.
However any operation going via a logarithm will have the lower
accuracy.

- suppose we have a number x whose logarithm ln(x) is L=10^5 + y, where
  y is O(1)
- the number itself is stored with full 16 digits of precision, as some
  double * 2^n
- if some intermediate step of your calculation goes via the logarithm,
  L=log(x), L is also stored in 16 digits; but if the number is such
  that L=100000.1234567890123456 it gets represented in 16 digits, i.e.
  gets truncated to L=100000.12345678901; the part after the
  decimal point (which is what gets translated back to the double when
  one does exp(L)) is now represented only with 11 digits

Still to do
-----------
As of 2020-04-07, this type works within the shower code, but it is
still work in progress. Elements that need completing include

- think more carefully about handling of nan/inf, in particular
  consistency of representation and propagation beyond a single
  operation. This should be reflected in unit tests.
  (current 1e100_de + log(-1.0_de) incorrectly gives 1e100_de)
  [if correct handling imposes a big speed penalty, one could
  consider turning it on/off with a preprocessor or template control]

- complete implementation of special functions (C++98 or C++11)

- complete implementation of numeric_limits<...>

- think whether to make the int type templated and what to make as
  default (int32_t or int64_t)

 