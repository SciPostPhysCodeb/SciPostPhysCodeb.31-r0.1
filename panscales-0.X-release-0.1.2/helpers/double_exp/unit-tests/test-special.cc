#include "catch.hpp"
#include "double_exp.hh"
#include <iostream>

using namespace Catch::literals;
typedef Catch::Approx Approx;
using namespace std;

typedef double_exp D;

TEST_CASE("double_exp_isfinite", "[double_exp_isfinite]") {
  REQUIRE(isfinite(1e1000_de));
  REQUIRE(!isfinite(sqrt(-1.0_de)));
}

TEST_CASE("double_exp_hypot", "[double_exp_hypot]") {
  auto norm = GENERATE(1.0_de, 1e-1000_de, 1e1000_de);
  auto vals1 = GENERATE(3.1,-2.1);
  auto vals2 = GENERATE(4.1,-12.1);
  REQUIRE (hypot(D(vals1)*norm, D(vals2)*norm)/norm == Approx(hypot(vals1,vals2)));
}

TEST_CASE("double_exp_copysign", "[double_exp_copysign]") {
  auto norm = GENERATE(1.0_de, 1e-1000_de, 1e1000_de);
  auto vals1 = GENERATE(3.1,-2.1);
  auto vals2 = GENERATE(4.1,-12.1);
  REQUIRE (copysign(D(vals1)*norm, D(vals2)*norm)/norm == Approx(copysign(vals1,vals2)));
}

TEST_CASE("double_exp_scalbn", "[double_exp_scalbn]") {
  auto norm = GENERATE(1.0_de, 1e-1000_de, 1e1000_de);
  auto vals1 = GENERATE(3.1,-2.1);
  auto ns = GENERATE(-50,0,30);
  REQUIRE (scalbn(D(vals1)*norm, ns)/norm == Approx(ldexp(vals1,ns)));
}


TEST_CASE("double_exp_logb", "[double_exp_logb]") {
  auto norms = GENERATE(0,-10000,10000);
  auto vals = GENERATE(3.1,-2.1,0.3);  
  REQUIRE( logb(pow(2.0_de,norms)*vals) ==  logb(vals) + norms);
  REQUIRE(ilogb(pow(2.0_de,norms)*vals) == ilogb(vals) + norms);
}


TEST_CASE("double_exp_floorceil", "[double_exp_specials]") {
  // try a bunch of generic values
  vector<double> vals = {-2.3, -2.0, -0.03, 0.0, 0.03, 2.0, 2.3};
  for (double val: vals) {
    REQUIRE(floor(D(val)) == Approx(floor(val)));
    REQUIRE(ceil (D(val)) == Approx(ceil(val) ));
  }

  // and then try some extreme ones
  REQUIRE(floor( 1e-1000_de) ==  0.0_de);
  REQUIRE(floor(-1e-1000_de) == -1.0_de);
  REQUIRE(ceil ( 1e-1000_de) ==  1.0_de);
  REQUIRE(ceil (-1e-1000_de) ==  0.0_de);
  REQUIRE(floor( 1e+1000_de) ==  1e+1000_de);
  REQUIRE(ceil ( 1e+1000_de) ==  1e+1000_de);
  REQUIRE(floor(-1e+1000_de) == -1e+1000_de);
  REQUIRE(ceil (-1e+1000_de) == -1e+1000_de);
}

TEST_CASE("double_exp_sqrt","[double_exp_specials]") {
  for (int i = -10; i <= 10; i++) {
    double d = pow(1.2,i);
    REQUIRE(sqrt(D(d)).to_double() == Approx(sqrt(d)));
    // require sqrt to behave identically to normal doubles in this regime
    REQUIRE(sqrt(D(d)).to_double() == sqrt(d));
    // require it to work on huge numbers
    REQUIRE((sqrt(D(d) * 1e1000_de)/1e500_de).to_double() == Approx(sqrt(d)));
    // require it to work on tiny numbers
    REQUIRE((sqrt(D(d) * 1e-1000_de)/1e-500_de).to_double() == Approx(sqrt(d)));
  }
  D sqrtinf = sqrt(numeric_limits<D>::infinity());
  REQUIRE(isinf(sqrtinf));
  REQUIRE(isinf(sqrtinf+5.0));
  REQUIRE(isnan(sqrt(D(-100.0))));
}

TEST_CASE("double_exp_exp","[double_exp_specials]") {
  for (int i = -10; i <= 10; i++) {
    double d = i - 0.3;
    REQUIRE(exp(D(d)).to_double() == Approx(exp(d)));
  }

  double big_pos = 30000.0;
  REQUIRE(log(exp(D(big_pos))).to_double() == Approx(big_pos));
  REQUIRE(log(exp(D(-big_pos))).to_double() == Approx(-big_pos));

  double huge_pos = 1e100;
  REQUIRE(isinf(exp(D(huge_pos))));
  REQUIRE(exp(D(-huge_pos)) == 0.0);

  // now try things close to the edge both of the min and
  // max attainable values
  D lnmax = log(numeric_limits<D>::max());
  REQUIRE( isinf(exp(lnmax*1.0000001)));
  D lnmax_below_edge = lnmax*0.9999999999;
  D exp_lnmax_be = exp(lnmax_below_edge);
  REQUIRE(exp_lnmax_be.double_log() == Approx(double(lnmax_below_edge)));

  D lnmin = log(numeric_limits<D>::min());
  REQUIRE(exp(lnmin*1.0000001) == 0.0);
  D lnmin_above_edge = lnmin*0.9999999999;
  D exp_lnmin_ae = exp(lnmin_above_edge);
  REQUIRE(exp_lnmin_ae.double_log() == Approx(double(lnmin_above_edge)));
  
}


TEST_CASE("double_exp_log","[double_exp_specials]") {
  for (int i = -10; i <= 10; i++) {
    double d = pow(1.2,i);
    REQUIRE(log(D(d)).to_double() == Approx(log(d)));
  }
  D loginf = log(numeric_limits<D>::infinity());
  REQUIRE(isinf(loginf));
  REQUIRE(isinf(loginf+5.0));
  REQUIRE(isnan(log(D(-100.0))));
  
  D logzero = log(D(0.0));
  REQUIRE(isinf(logzero));
  REQUIRE(signbit(logzero));
}

TEST_CASE("double_exp_log1p","[double_exp_specials]") {
  for (int i = -10; i <= 10; i++) {
    double d = pow(1.2,i);
    REQUIRE(log1p(D(d)).to_double() == Approx(log1p(d)));
  }
  D loginf = log1p(numeric_limits<D>::infinity());
  REQUIRE(isinf(loginf));
  REQUIRE(isinf(loginf+5.0));
  loginf = log1p(D(-1.0));
  REQUIRE(isinf(loginf));
  REQUIRE(signbit(loginf));
  REQUIRE(isnan(log(D(-2.0))));
  
  vector<D> norms = {1e-1000_de, 1e-500_de, 1e-200_de};
  for (auto norm: norms) {
    REQUIRE(log1p( norm) ==  norm);
    REQUIRE(log1p(-norm) == -norm);
    REQUIRE(double(log1p(1.0/norm)) == Approx(double(log(1.0/norm))));
  }

}

TEST_CASE("double_exp_pow","[double_exp_specials]") {
  vector<double> values = {0.01, 0.1, 0.7, 0.0, 1.0, 1.4, 10.8};

  for (double value: values) {
    for (int i = -10; i <= 10; i++) {
      // cout << value << " " << i << " " << pow(D(value),i) << " " << pow(value,i) << endl;
      // cout << -value << " " << i << " " << pow(D(-value),i) << " " << pow(-value,i) << endl;
      REQUIRE(pow(D(value),i).to_double() == Approx(pow(value,i)));
      REQUIRE(pow(D(-value),i).to_double() == Approx(pow(-value,i)));
      REQUIRE(pow(D(value),double(i)).to_double() == Approx(pow(value,i)));
      REQUIRE(pow(D(-value),double(i)).to_double() == Approx(pow(-value,i)));
      REQUIRE(pow(D(value),i+0.7).to_double() == Approx(pow(value,i+0.7)));

      REQUIRE(pow(D( value),D(i)).to_double() == Approx(pow(value,i)));
      REQUIRE(pow(D(-value),D(i)).to_double() == Approx(pow(-value,i)));
      REQUIRE(pow(D( value),D(i+0.7)).to_double() == Approx(pow(value,i+0.7)));
      //REQUIRE(pow(D(value),int64_t(i)).to_double() == Approx(pow(value,i)));
    } 
  }

  REQUIRE(isnan(pow(D(-2.0),2.5)));
  double_exp res;
  res = pow(D(1 + 1e-10), pow(D(1e200),2));
  REQUIRE( isinf(res) );
  REQUIRE( res > 0);
  res = pow(-D(1 + 1e-10), pow(D(1e200),2));
  REQUIRE( isinf(res) );
  REQUIRE( res > 0);

  res = pow(1e1000_de, 1e-400_de);
  REQUIRE( res == 1.0);

}

TEST_CASE("double_exp_abs","[double_exp_specials]") {
  for (int i = -10; i <= 10; i++) {
    double d = i - 0.3;
    REQUIRE(abs(D(d)).to_double() == Approx(abs(d)));
  }
}

TEST_CASE("double_exp_atan2","[double_exp_specials]") {
  vector<D> norms = {1e-1000_de, 0.058333_de, 1.0_de, 10.345_de, 1e1000_de};
  const int nangles = 100;
  // first test a range of angles, with arguments
  // normalised in various ways
  for(const auto & norm: norms) {
    for (int i = 0; i <= nangles; i++) {
      double phi = 2*i*M_PI/nangles;
      double sinphi = sin(phi);
      double cosphi = cos(phi);
      REQUIRE(atan2(norm*sinphi,norm*cosphi) == Approx(atan2(sinphi,cosphi)));
    }
    // then test special cases
    REQUIRE( atan2( norm,    0).to_double() == Approx(atan2( 1.0, 0.0)));
    REQUIRE( atan2(-norm,    0).to_double() == Approx(atan2(-1.0, 0.0)));
    REQUIRE( atan2(    0, norm).to_double() == Approx(atan2( 0.0, 1.0)));
    REQUIRE( atan2(    0,-norm).to_double() == Approx(atan2( 0.0,-1.0)));
  } 

  // then test small-angle limits, both above and below the threshold 
  // numeric_limits<double>::min() and max()
  norms = {1e-1000_de, 1e-500_de, 1e-200_de};
  for (const auto & norm: norms) {
    // include cases with at least two normalisations of the "large"
    // component to test that we're not messing things up there
    REQUIRE(atan2( norm,1.0     ) ==  norm);
    REQUIRE(atan2( norm,2.0     ) ==  norm/2);
    //cout << "for norm = " << norm << ", atan2 test is " << (atan2(  1.0,1.0/norm) == norm) << ", diff = " << atan2(  1.0,1.0/norm) - norm << endl;
    // write this one with an Approx, because rounding errors in the 1/norm 
    // operation can introduce O(eps) issues
    REQUIRE((atan2(  1.0,1.0/norm)/norm).to_double() == Approx(1.0));

    REQUIRE(atan2(-norm,1.0     ) == -norm);
    REQUIRE(atan2(-norm,2.0     ) == -norm/2);
    REQUIRE((atan2(- 1.0,1.0/norm)/norm).to_double() == Approx(-1.0));

    REQUIRE(atan2( norm, -1.0     ) ==  M_PI);
    REQUIRE(atan2( norm, -2.0     ) ==  M_PI);
    REQUIRE(atan2(  1.0, -1.0/norm) ==  M_PI);

    REQUIRE(atan2(-norm, -1.0     ) == -M_PI);
    REQUIRE(atan2(-norm, -2.0     ) == -M_PI);
    REQUIRE(atan2(- 1.0, -1.0/norm) == -M_PI);

    REQUIRE(atan2(     1.0,  norm).to_double() == Approx(M_PI_2));
    REQUIRE(atan2(     2.0,  norm).to_double() == Approx(M_PI_2));
    REQUIRE(atan2(1.0/norm,   1.0).to_double() == Approx(M_PI_2));
    REQUIRE(atan2(     1.0, -norm).to_double() == Approx(M_PI_2));
    REQUIRE(atan2(     2.0, -norm).to_double() == Approx(M_PI_2));
    REQUIRE(atan2(1.0/norm, - 1.0).to_double() == Approx(M_PI_2));

    REQUIRE(atan2(-     1.0,  norm).to_double() == Approx(-M_PI_2));
    REQUIRE(atan2(-     2.0,  norm).to_double() == Approx(-M_PI_2));
    REQUIRE(atan2(-1.0/norm,   1.0).to_double() == Approx(-M_PI_2));
    REQUIRE(atan2(-     1.0, -norm).to_double() == Approx(-M_PI_2));
    REQUIRE(atan2(-     2.0, -norm).to_double() == Approx(-M_PI_2));
    REQUIRE(atan2(-1.0/norm, - 1.0).to_double() == Approx(-M_PI_2));
  }
}

TEST_CASE("double_exp_trig","[double_exp_specials]") {
  for (int i = -10; i <= 10; i++) {
    double d = i - 0.3;
    REQUIRE(sin(D(d)).to_double() == Approx(sin(d)));
    REQUIRE(cos(D(d)).to_double() == Approx(cos(d)));
    REQUIRE(tan(D(d)).to_double() == Approx(tan(d)));
  }

  D u = 1e-600_de;
  REQUIRE( sin(u) == u);
  REQUIRE( cos(u) == 1_de);
  REQUIRE( tan(u) == u);
  
  u = 1e600_de;
  REQUIRE( (pow(sin(u),2) + pow(cos(u),2)).to_double() == Approx(1.0));
  REQUIRE( (tan(u)*cos(u)/sin(u)).to_double() == Approx(1.0));
}

TEST_CASE("double_exp_atrig","[double_exp_specials]") {
  vector<double> values = {-1.0, -0.4, -0.2, 0.0, 0.2, 0.5, 0.8, 1.0};
  for (double d: values) {
    REQUIRE(asin(D(d)).to_double() == Approx(asin(d)));
    REQUIRE(acos(D(d)).to_double() == Approx(acos(d)));
  }

  values.push_back(-5.3);
  values.push_back( 5.3);
  for (double d: values) {
    REQUIRE(atan(D(d)).to_double() == Approx(atan(d)));
  }

  // then test very small values (beyond double reach)
  D u = 1e-600_de;
  REQUIRE( asin(u) == u);
  REQUIRE( acos(u) == M_PI_2);
  REQUIRE( atan(u) == u);

  // and very large ones
  u = 1e600_de;
  REQUIRE(isnan(asin(u)));
  REQUIRE(isnan(acos(u)));
  REQUIRE(     (atan( u)).to_double() == Approx( M_PI_2));
  REQUIRE(     (atan(-u)).to_double() == Approx(-M_PI_2));
}


TEST_CASE("double_exp_literals","[double_exp_specials]") {
  REQUIRE ((1.4e200_de).to_double() == Approx(1.4e200));
  REQUIRE ((1.4e-200_de).to_double() == Approx(1.4e-200));
  REQUIRE ((-1.4e-200_de).to_double() == Approx(-1.4e-200));
  REQUIRE ((5.3_de).to_double() == Approx(5.3));
}
