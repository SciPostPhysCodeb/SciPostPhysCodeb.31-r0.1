#include "catch.hpp"
#include "double_exp.hh"
#include <iostream>

using namespace Catch::literals;
typedef Catch::Approx Approx;
using namespace std;

typedef double_exp D;


void test_sum(double a, double b) {
  REQUIRE( (D(a) + D(b)).to_double() == Approx(a+b));
  REQUIRE( (D(b) + D(a)).to_double() == Approx(a+b));
  REQUIRE( (D(a) + b   ).to_double() == Approx(a+b));
  REQUIRE( (D(b) + a   ).to_double() == Approx(a+b));
  REQUIRE( (  a  + D(b)).to_double() == Approx(a+b));

  D aa = a;
  D bb = b;
  D cc = aa;
  cc += bb;
  REQUIRE(cc.to_double() == Approx(a+b));
  cc = bb;
  cc += aa;
  REQUIRE(cc.to_double() == Approx(a+b));
}

void test_product(double a, double b) {
  REQUIRE( (D(a) * D(b)).to_double() == Approx(a*b));
  REQUIRE( (D(b) * D(a)).to_double() == Approx(a*b));
  REQUIRE( (D(a) * b   ).to_double() == Approx(a*b));
  REQUIRE( (D(b) * a   ).to_double() == Approx(a*b));
  REQUIRE( (  a  * D(b)).to_double() == Approx(a*b));

  D aa = a;
  D bb = b;
  D cc = aa;
  cc *= bb;
  REQUIRE(cc.to_double() == Approx(a*b));
  cc = bb;
  cc *= aa;
  REQUIRE(cc.to_double() == Approx(a*b));
}


void test_diff(double a, double b) {
  REQUIRE( (D(a) - D(b)).to_double() == Approx(a-b));
  REQUIRE( (D(b) - D(a)).to_double() == Approx(b-a));
  REQUIRE( (D(a) - b   ).to_double() == Approx(a-b));
  REQUIRE( (D(b) - a   ).to_double() == Approx(b-a));
  REQUIRE( (  a  - D(b)).to_double() == Approx(a-b));

  D aa = a;
  D bb = b;
  D cc = aa;
  cc -= bb;
  REQUIRE(cc.to_double() == Approx(a-b));
  cc = bb;
  cc -= aa;
  REQUIRE(cc.to_double() == Approx(b-a));
}

void test_div(double a, double b) {
  REQUIRE( (D(a) / D(b)).to_double() == Approx(a/b));
  REQUIRE( (D(b) / D(a)).to_double() == Approx(b/a));
  REQUIRE( (D(a) / b   ).to_double() == Approx(a/b));
  REQUIRE( (D(b) / a   ).to_double() == Approx(b/a));
  REQUIRE( (  a  / D(b)).to_double() == Approx(a/b));

  D aa = a;
  D bb = b;
  D cc = aa;
  cc /= bb;
  REQUIRE(cc.to_double() == Approx(a/b));
  cc = bb;
  cc /= aa;
  REQUIRE(cc.to_double() == Approx(b/a));

  REQUIRE(isinf(D(a)/0.0));
  REQUIRE(isinf(D(a)/D(0.0)));
  aa /= 0.0;
  REQUIRE(isinf(aa));
}

void test_ltgt() {
  // compare refs to a set of values that scanned
  // in steps of du -- ensure that we test cases of both signs
  // and where we have underlying _n values that are both
  // equal and different
  vector<double> refs{-5.31, -0.13, 0.0, 0.13, 5.31};
  const double du = 1.0;

  for (auto ref: refs) {
    double u = -5.55;
    while (u < 5.6) {
      REQUIRE ((D(u) < D(ref))  == (u < ref ));
      REQUIRE ((D(ref) < D(u))  == (ref < u ));
      REQUIRE ((D(u) <= D(ref)) == (u <= ref));
      REQUIRE ((D(ref) <= D(u)) == (ref <= u));
      REQUIRE ((D(u) > D(ref))  == (u > ref ));
      REQUIRE ((D(ref) > D(u))  == (ref > u ));
      REQUIRE ((D(u) >= D(ref)) == (u >= ref));
      REQUIRE ((D(ref) >= D(u)) == (ref >= u));

      REQUIRE (( (u) < D(ref))  == (u < ref ));
      REQUIRE (( (ref) < D(u))  == (ref < u ));
      REQUIRE (( (u) <= D(ref)) == (u <= ref));
      REQUIRE (( (ref) <= D(u)) == (ref <= u));
      REQUIRE (( (u) > D(ref))  == (u > ref ));
      REQUIRE (( (ref) > D(u))  == (ref > u ));
      REQUIRE (( (u) >= D(ref)) == (u >= ref));
      REQUIRE (( (ref) >= D(u)) == (ref >= u));

      REQUIRE ((D(u) <  (ref))  == (u < ref ));
      REQUIRE ((D(ref) <  (u))  == (ref < u ));
      REQUIRE ((D(u) <=  (ref)) == (u <= ref));
      REQUIRE ((D(ref) <=  (u)) == (ref <= u));
      REQUIRE ((D(u) >  (ref))  == (u > ref ));
      REQUIRE ((D(ref) >  (u))  == (ref > u ));
      REQUIRE ((D(u) >=  (ref)) == (u >= ref));
      REQUIRE ((D(ref) >=  (u)) == (ref >= u));

      u += du;

    }
    // comparisons to +- infinity
    REQUIRE (D(ref) < 1.0_de/0.0_de);
    REQUIRE (D(ref) > log(0.0_de));

    // lt/gt comparisons to nan should always return false for doubles
    REQUIRE ((D(ref) <  sqrt(-1.0_de)) ==  (ref <  sqrt(-1.0)));
    REQUIRE ((D(ref) >  sqrt(-1.0_de)) ==  (ref >  sqrt(-1.0)));
    REQUIRE ((D(ref) <= sqrt(-1.0_de)) ==  (ref <= sqrt(-1.0)));
    REQUIRE ((D(ref) >= sqrt(-1.0_de)) ==  (ref >= sqrt(-1.0)));
  }

  // next we test edge cases, of both signs
  // (and we include tests of doubles and double_exp's)
  REQUIRE(! (D(0.0)  <  D(0.0))  );
  REQUIRE(! (D(5.0)  <  D(5.0))  );
  REQUIRE(! (D(-5.0) <  D(-5.0)) );
  REQUIRE(! (D(0.0)  >  D(0.0))  );
  REQUIRE(! (D(5.0)  >  D(5.0))  );
  REQUIRE(! (D(-5.0) >  D(-5.0)) );
  
  REQUIRE(  (D(0.0)  <=  D(0.0))  );
  REQUIRE(  (D(5.0)  <=  D(5.0))  );
  REQUIRE(  (D(-5.0) <=  D(-5.0)) );
  REQUIRE(  (D(0.0)  >=  D(0.0))  );
  REQUIRE(  (D(5.0)  >=  D(5.0))  );
  REQUIRE(  (D(-5.0) >=  D(-5.0)) );
  
  REQUIRE(! (D(0.0)  <   (0.0))  );
  REQUIRE(! (D(5.0)  <   (5.0))  );
  REQUIRE(! (D(-5.0) <   (-5.0)) );
  REQUIRE(! (D(0.0)  >   (0.0))  );
  REQUIRE(! (D(5.0)  >   (5.0))  );
  REQUIRE(! (D(-5.0) >   (-5.0)) );
  
  REQUIRE(  (D(0.0)  <=   (0.0))  );
  REQUIRE(  (D(5.0)  <=   (5.0))  );
  REQUIRE(  (D(-5.0) <=   (-5.0)) );
  REQUIRE(  (D(0.0)  >=   (0.0))  );
  REQUIRE(  (D(5.0)  >=   (5.0))  );
  REQUIRE(  (D(-5.0) >=   (-5.0)) );
  
  REQUIRE(! ( (0.0)  <   (0.0))  );
  REQUIRE(! ( (5.0)  <   (5.0))  );
  REQUIRE(! ( (-5.0) <   (-5.0)) );
  REQUIRE(! ( (0.0)  >   (0.0))  );
  REQUIRE(! ( (5.0)  >   (5.0))  );
  REQUIRE(! ( (-5.0) >   (-5.0)) );

  REQUIRE(  ( (0.0)  <=   (0.0))  );
  REQUIRE(  ( (5.0)  <=   (5.0))  );
  REQUIRE(  ( (-5.0) <=   (-5.0)) );
  REQUIRE(  ( (0.0)  >=   (0.0))  );
  REQUIRE(  ( (5.0)  >=   (5.0))  );
  REQUIRE(  ( (-5.0) >=   (-5.0)) );
  
}

TEST_CASE("double_exp_equality", "[double_exp_arithmetic]") {

  REQUIRE(numeric_limits<double_exp>::epsilon() == numeric_limits<double>::epsilon());

  REQUIRE(0.5*double_exp::min() == 0.0);

  //----------------------------------------------------------------------
  // equality and inequality
  REQUIRE( bool(D(0.4) == 0.4));
  REQUIRE( bool(D(0.0) == 0.0));
  REQUIRE( bool(D(0.4) != 0.8));
  REQUIRE(!bool(D(0.4) == 0.8));
  REQUIRE( bool(D(0.4)*D(0.0) == D(0.0)));

  // tests of infinity
  auto d = GENERATE(1.0_de, 1e100_de, 1e-100_de, 1e1000_de, 1e-1000_de);
  // infinite is equal to infinity in standard double, so aim for the
  // same here
  REQUIRE( d/0.0 ==  1.0_de/0.0);
  REQUIRE( d/0.0 != -1.0_de/0.0);
  REQUIRE( (-d)/0.0 == log(0.0_de));
  // nan is not equal to nan
  REQUIRE( sqrt(-d) != sqrt(-1.0_de) );
}

TEST_CASE("double_exp_int", "[double_exp_arithmetic]") {
  int i = static_cast<int>(105.0_de);
  REQUIRE(i == 105);
  i = static_cast<int>(-105.0_de);
  REQUIRE(i == -105);
}


TEST_CASE("double_exp_is_finite_double", "[double_exp_arithmetic]") {

  // start with maximum double value and check that
  // we return is_valid_double()
  D u = numeric_limits<double>::max();
  REQUIRE(u.is_finite_double());
  // then increase it by relative epsilon and
  // check we return that it is not valid
  u *= (1+numeric_limits<double>::epsilon());
  REQUIRE(!u.is_finite_double());


  // corresponding test at lower end
  u = numeric_limits<double>::min();
  REQUIRE(u.is_finite_double());
  u *= (1-numeric_limits<double>::epsilon());
  REQUIRE(!u.is_finite_double());

  u = sqrt(-1.0_de);
  REQUIRE(!u.is_finite_double());
  u = 1.0_de/0.0_de;
  REQUIRE(!u.is_finite_double());

}

TEST_CASE("double_exp_equalint", "[double_exp_arithmetic]") {
  REQUIRE (! (4.5_de == 4));
  REQUIRE (  (4.5_de != 4));
  REQUIRE (  (4.0_de == 4));
  REQUIRE (! (4.0_de != 4));
}

TEST_CASE("double_exp_arithmetic", "[double_exp_arithmetic]") {
  // test a range of values (with zero separate) for binary operators
  // including values that should see cancellations, and values
  // that are large
  vector<double> vals = {0.7,0.8,4.7,0.1,-0.3,-0.01,-5.6,-0.75,-0.8,
                         1e100,-1e100,1e-100,-1e100,-2e100};
  for (unsigned i = 0; i < vals.size(); i++) {
    for (unsigned j = 0; j < i; j++) {
      test_sum(vals[i],vals[j]);
      test_diff(vals[i],vals[j]);
      test_product(vals[i],vals[j]);
      test_div(vals[i],vals[j]);
    }
    test_sum    (vals[i],0.0);
    test_diff   (vals[i],0.0);
    test_product(vals[i],0.0);
  }
}

TEST_CASE("double_exp_ltgt", "[double_exp_arithmetic]") {
  test_ltgt();
}

TEST_CASE("double_exp_extremes", "[double_exp_arithmetic]") {
  cout << "max = " << double_exp::max() << endl;
  cout << "min = " << double_exp::min() << endl;

  REQUIRE(!signbit(D(+0.8)));
  REQUIRE( signbit(D(-0.8)));
  REQUIRE(isinf(2*double_exp::max()));
}


// ensure that when we add nan's and infinities we get a sensible answer
TEST_CASE("double_exp_pm_nan_inf","[double_exp_arithmetic]") {
  // first generate candidate inf and nan
  double_exp nan_de = sqrt(-1.0_de);
  double_exp inf_de = -log(0.0_de);
  REQUIRE(isnan(nan_de));
  REQUIRE(isinf(inf_de));

  // then use them in a variety of operations
  auto d = GENERATE(0.0_de, 1.0_de, 1e100_de, 1e-100_de, 1e1000_de, 1e-1000_de, -1e1000_de);
  //cout << inf_de.n() << " " << d << " " << d+inf_de << " " << isinf(d+inf_de) << endl;
  REQUIRE (isnan(d + nan_de));
  REQUIRE (isinf(d + inf_de));
  REQUIRE (isnan(d - nan_de));
  REQUIRE (isinf(d - inf_de));
  REQUIRE (isnan(nan_de + d));
  REQUIRE (isinf(inf_de + d));
  REQUIRE (isnan(nan_de - d));
  REQUIRE (isinf(inf_de - d));
  REQUIRE (d+inf_de > 0);
  REQUIRE (d-inf_de < 0);


  /// make sure the multiplications and divisions also return
  /// nan or inf as they should
  REQUIRE(isnan(nan_de*2     ));
  REQUIRE(isnan(nan_de*2.0   ));
  REQUIRE(isnan(nan_de*2.0_de));
  REQUIRE(isnan(nan_de/2     ));
  REQUIRE(isnan(nan_de/2.0   ));
  REQUIRE(isnan(nan_de/2.0_de));
  REQUIRE(isnan(nan_de*1e-1000_de));

  REQUIRE(isinf(inf_de*2     ));
  REQUIRE(isinf(inf_de*2.0   ));
  REQUIRE(isinf(inf_de*2.0_de));
  REQUIRE(isinf(inf_de/2     ));
  REQUIRE(isinf(inf_de/2.0   ));
  REQUIRE(isinf(inf_de/2.0_de));


}
