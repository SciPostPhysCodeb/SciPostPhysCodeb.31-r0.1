#include <array>
#include <cmath>
#include <iostream>
//#include <boost/multiprecision/cpp_bin_float.hpp>
#include <cmath>
#include <chrono>
#include <iomanip>
#include <vector>
#include "double_exp.hh"
//#include <gmpxx.h>

//#include <qd/dd_real.h>
//#include <qd/fpu.h>

#define MPFR_REAL_ENABLE_CONV_OPS
#include "mpfr_real.hh"
//#include <boost/multiprecision/gmp.hpp>
//#include <boost/multiprecision/mpfr.hpp> 
//#include "CmdLine.hh"
#include <cxxabi.h>
using namespace std;
using namespace chrono;
//using namespace boost::multiprecision;

//typedef number<backends::cpp_bin_float<113, backends::digit_base_2, void, boost::int32_t, -1000000000,1000000000>, et_off> cpp_double_exp;
//typedef number<backends::cpp_bin_float<53, backends::digit_base_2, void, boost::int32_t, -1000000000,1000000000>, et_off> cpp_double_exp;

typedef mpfr::real<53> mpreal53;
typedef mpfr::real<4096> mpreal4096;

template<class T>
inline double to_double(const T & x) {return double(x);}

class TimingBase {
public:
  virtual void operator()() = 0;
  void do_timing() {
    auto start = high_resolution_clock::now();
    this->operator()();
    auto end = high_resolution_clock::now();
    auto diff = end-start;
    time_ns = duration_cast<nanoseconds>(diff).count();
  }
  /// rescale the number of iterations by 1/r
  /// (but note that this will affect the answers)
  void rescale(double r) {
    n /= r;
    scale_factor_ *= r;
  }
  double scale_factor() const {
    return scale_factor_;
  }
  double time_ns;
  unsigned int n = pow(10,7);
  double scale_factor_ = 1.0;
  virtual string name() const {
    int     status;
    char   *realname;
    realname = abi::__cxa_demangle(typeid(*this).name(), 0, 0, &status);
    return realname;
  }
  virtual void write_result(ostream & ostr) const = 0;
  virtual double double_result() const = 0;
};

template<class T, class U = T>
class Timing : public TimingBase {
public:
  using TimingBase::time_ns;
  U increment;
  T result;
  void write_result(ostream & ostr) const override {
    ostr << "time/op = " << setprecision(3) << setw(6) << time_ns/n << " ns, result = "
         << setprecision(6) << setw(11) << result;
  }
  double double_result() const override {return to_double(result);}
};


ostream & operator<<(ostream & ostr, const TimingBase & timing) {
  timing.write_result(ostr);
  return ostr;
}
string name(const TimingBase * t)  {
  int     status;
  char   *realname;
  realname = abi::__cxa_demangle(typeid(*t).name(), 0, 0, &status);
  return realname;
}



template<class T, class U = T>
class TestTimesEqual : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestTimesEqual(double increment_in = 1e-6) {increment = increment_in;}
  void operator()() override {
    U u = 1.0 + increment;
    T res = 1.0;
    for (unsigned i = 0; i<n; i++ ) {
      res *= u;
    }
    result = res;
  }
};
template<class T, class U = T>
class TestMult : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestMult(double increment_in = 1e-6) {increment = increment_in;}
  void operator()() override {
    U u = 1.0 + increment;
    T res = 1.0;
    for (unsigned i = 0; i<n; i++ ) {
      res = res * u;
    }
    result = res;
  }
};

template<class T, class U = T>
class TestDivEqual : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestDivEqual(double increment_in = 1e-6) {increment = increment_in;}
  string name() const override {return "TestMult";}
  void operator()() override {
    U u = 1.0 + increment;
    T res = 1.0;
    for (unsigned i = 0; i<n; i++ ) {
      res /= u;
    }
    result = res;
  }
};

template<class T, class U = T>
class TestDiv : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestDiv(double increment_in = 1e-6) {increment = increment_in;}
  string name() const override {return "TestMult";}
  void operator()() override {
    U u = 1.0 + increment;
    T res = 1.0;
    for (unsigned i = 0; i<n; i++ ) {
      res = res / u;
    }
    result = res;
  }
};


template<class T, class U = T>
class TestTwoMult : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestTwoMult(double increment_in = 1e-6) {increment = increment_in;}
  string name() const override {return "TestMult";}
  void operator()() override {
    U u = 1.0 + increment;
    T res = 1.0;
    for (unsigned i = 0; i<n; i++ ) {
      res = res * u;
      res = res * u;
    }
    result = res;
  }
};

template<class T, class U = T>
class TestPlusEqual : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestPlusEqual(double increment_in = 1e-6) {increment = increment_in;}
  string name() const override {return "TestMult";}
  void operator()() override {
    U u = 1.0 + increment;
    T res = 1.0;
    for (unsigned i = 0; i<n; i++ ) {
      res += u;
    }
    result = res;
  }
};


template<class T, class U = T>
class TestPlus : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestPlus(double increment_in = 1e-6) {increment = increment_in;}
  string name() const override {return "TestMult";}
  void operator()() override {
    U u = increment;
    T res = 1.0;
    for (unsigned i = 0; i<n; i++ ) {
      res = res + u;
    }
    result = res;
  }
};

template<class T, class U = T>
class TestMinus : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestMinus(double increment_in = 1e-2) {increment = increment_in;}
  string name() const override {return "TestMult";}
  void operator()() override {
    U u = increment;
    T res = 1.0;
    for (unsigned i = 0; i<n; i++ ) {
      res = res - u;
    }
    result = res;
  }
};


template<class T, class U = T>
class TestSqrtMult : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestSqrtMult(double increment_in = 1e-2) {increment = increment_in; n = 1e7;}
  string name() const override {return "TestSqrt";}
  void operator()() override {
    T res = 1e308;
    U u = 1.0 + increment;
    for (unsigned i = 0; i<n; i++ ) {
      res = sqrt(res*u);
    }
    result = res;
  }
};

template<class T, class U = T>
class TestLogMult : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestLogMult(double increment_in = 1e-2) {increment = increment_in; n = 1e7;}
  void operator()() override {
    T res = 1e100;
    U u = 3.0 + increment;
    for (unsigned i = 0; i<n; i++ ) {
      res = log(res*u);
    }
    result = res;
  }
};
template<class T, class U = T>
class TestLog1pMult : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestLog1pMult(double increment_in = 1e-2) {increment = increment_in; n = 1e7;}
  void operator()() override {
    T res = 1e100;
    U u = 3.0 + increment;
    for (unsigned i = 0; i<n; i++ ) {
      res = log1p(res*u);
    }
    result = res;
  }
};

template<class T, class U = T>
class TestExpPlus : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestExpPlus(double increment_in = 1e-7) {increment = increment_in; n = 1e7;}
  void operator()() override {
    T res = 1e100;
    T u = increment;
    for (unsigned i = 0; i<n; i++ ) {
      res += exp(u*i);
    }
    result = res;
  }
};

template<class T, class U = T>
class TestMultLT : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestMultLT(double increment_in = 1e-5) {increment = increment_in; n = 1e7;}
  void operator()() override {
    T res = 1.0;
    U ref = exp(50.0);// exp(n*log(1.0*increment) / 2.0);
    U u = 1.0 + increment;
    double count = 0;
    for (unsigned i = 0; i<n; i++ ) {
      res *= u;
      if (res < ref) count += 1.0;
    }
    result = count;
  }
};

template<class T, class U = T>
class TestPlusPow : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestPlusPow(double increment_in = 1e-5) {increment = increment_in; n = 1e7;}
  void operator()() override {
    result = 0.0;
    T arg = 1.5;
    U u = increment;
    for (unsigned i = 0; i<n; i++ ) {
      result += pow(arg,u*i);
    }
  }
};

template<class T, class U = T>
class TestPlusPow2 : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestPlusPow2(double increment_in = 1e-5) {increment = increment_in; n = 1e7;}
  void operator()() override {
    result = 0.0;
    T u = increment;
    for (unsigned i = 0; i<n; i++ ) {
      result += pow(u*i,2);
    }
  }
};

template<class T, class U = T>
class TestPlusPow3 : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestPlusPow3(double increment_in = 1e-5) {increment = increment_in; n = 1e7;}
  void operator()() override {
    result = 0.0;
    T u = increment;
    for (unsigned i = 0; i<n; i++ ) {
      result += pow(u*i,3);
    }
  }
};

template<class T, class U = T>
class TestPlusSin : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestPlusSin(double increment_in = 1e-5) {increment = increment_in; n = 1e7;}
  void operator()() override {
    result = 0.0;
    T u = increment;
    for (unsigned i = 0; i<n; i++ ) {
      result += sin(u*i);
    }
  }
};
template<class T, class U = T>
class TestPlusCos : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestPlusCos(double increment_in = 1e-5) {increment = increment_in; n = 1e7;}
  void operator()() override {
    result = 0.0;
    T u = increment;
    for (unsigned i = 0; i<n; i++ ) {
      result += cos(u*i);
    }
  }
};
template<class T, class U = T>
class TestPlusTan : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestPlusTan(double increment_in = 1e-5) {increment = increment_in; n = 1e7;}
  void operator()() override {
    result = 0.0;
    T u = increment;
    for (unsigned i = 0; i<n; i++ ) {
      result += tan(u*i);
    }
  }
};

template<class T, class U = T>
class TestPlusATan2 : public Timing<T,U> {
public:
  using Timing<T,U>::n;
  using Timing<T,U>::increment;
  using Timing<T,U>::result;
  TestPlusATan2(double increment_in = 1e-5) {increment = increment_in; n = 1e7;}
  void operator()() override {
    result = 0.0;
    T u = 1.5;
    for (unsigned i = 0; i<n; i++ ) {
      result += atan2(u, result);
    }
  }
};


template<class T, class U = T> 
void prepare_tests(vector<TimingBase *> & tests, double rescale = 1.0) {

  vector<TimingBase *> tmp_tests;
  
  tmp_tests.push_back(new TestTimesEqual<T,U>());
  tmp_tests.push_back(new TestMult<T,U>());
  tmp_tests.push_back(new TestTwoMult<T,U>());
  tmp_tests.push_back(new TestDiv<T,U>());
  tmp_tests.push_back(new TestDivEqual<T,U>());
  tmp_tests.push_back(new TestPlus<T,U>());
  tmp_tests.push_back(new TestPlusEqual<T,U>());
  tmp_tests.push_back(new TestMinus<T,U>());

  tmp_tests.push_back(new TestMultLT<T,U>());

  tmp_tests.push_back(new TestSqrtMult<T,U>());
  tmp_tests.push_back(new TestLogMult<T,U>());
  tmp_tests.push_back(new TestLog1pMult<T,U>());
  tmp_tests.push_back(new TestExpPlus<T,U>());
  tmp_tests.push_back(new TestPlusPow<T,U>());
  tmp_tests.push_back(new TestPlusPow2<T,U>());
  tmp_tests.push_back(new TestPlusPow3<T,U>());

  tmp_tests.push_back(new TestPlusSin<T,U>());
  tmp_tests.push_back(new TestPlusCos<T,U>());
  tmp_tests.push_back(new TestPlusTan<T,U>());
  tmp_tests.push_back(new TestPlusATan2<T,U>());

  if (rescale != 1.0) {
    for (auto test: tmp_tests) {
      test->rescale(rescale);
    }
  }
  for (auto test: tmp_tests) {tests.push_back(test);}
}

int main(int argc, char ** argv) {

  bool do_mpfr = false;
  for (int iarg = 1; iarg < argc ; iarg++) {
    if (argv[iarg] == string("-mpfr")) {
      do_mpfr = true;
    } else {
      cout << "Usage: " << argv[0] << " [-mpfr]" << endl;
      return 1;
    }
  }


  vector<TimingBase *> tests;
  prepare_tests<double>(tests);
  // use a nullptr as a separator
  tests.push_back(nullptr);
  int ntests = tests.size();
  
  prepare_tests<double_exp>(tests);
  tests.push_back(nullptr);

  prepare_tests<double_exp, double>(tests);
  tests.push_back(nullptr);

  // double_exp a = 100;
  // while (true) {
  //   a *= 2e307;
  //   cout << a << endl;
  // }

  ////mpf_set_default_prec (53);
  //mpf_set_default_prec (128);
  //cout << "mpf precision (in bits) is " << mpf_get_default_prec() << endl;
  //prepare_tests<mpf_class>(tests);
  //tests.push_back(nullptr);

  // prepare_tests<cpp_double_exp>(tests);
  // tests.push_back(nullptr); 

  //prepare_tests<cpp_double_exp>(tests);
  //prepare_tests<dd_real>(tests);
  if (do_mpfr) {
    prepare_tests<mpreal53>(tests);
    tests.push_back(nullptr); 
    prepare_tests<mpreal4096>(tests,100.0);
    tests.push_back(nullptr); 
}
  
  for (auto & t: tests) {
    if (t == nullptr) {
      cout << endl;
      continue;
    }
    t->do_timing();
    int itest = &t - &tests[0];
    cout << setw(5) << itest << " ";
    cout <<  setw(40) ;
    string nm = name(t);
    if (nm.size() <= 40) {
      cout << nm ;
    } else {
      cout << nm.substr(0,40);
    }
    cout << " " << *t ;
    if (itest >= ntests) {
      cout << " " << setw(6) << setprecision(3) 
           << tests[itest]-> time_ns*tests[itest]->scale_factor() / tests[itest%ntests]->time_ns << "x";
      cout << "  ratio=" 
           << setprecision(6) << tests[itest%ntests]->double_result()
                                    / tests[itest]->double_result();
    }
    cout << endl;
  }
}
