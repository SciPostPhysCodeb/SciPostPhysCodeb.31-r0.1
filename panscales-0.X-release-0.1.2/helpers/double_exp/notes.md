Work on 2023-12-19
------------------

* issue discovered by Ludo in double_exp::exp_double(arg), which on some
  systems (Intel with recent compilers?) fails to return inf when the
  argument is very large (e.g. 1e100). This was because the operation
  `ExpType npowers2 = ExpType( val * invln2);` was overflowing the range
  of ExpType. 

  - solnA: c9dcc60cad, goes via an intermediate double, checking the log2 exponent that is
    computed. If it is too large, returns inf or zero. Then proceeds as before.

  - solnB: just checks the size of the argument first and if it is too large
    returns inf or zero. Then proceeds as before. This is the solution that we 
    will adopt, because it is marginally simpler. (But we may still want to reconsider)

  Timings on the TestExpPlus test
  ```
          | M2Pro clang 15.0.0  |  Intel Mac clang 15.0.0 | Intel gcv 7.5.0(hydra) | 
   -------|---------------------|-------------------------|------------------------|
   before | 10.6ns              | 31.7ns                  | 30.4ns                 |
   afterA | 11.3ns              | 30.6ns                  | 34.9ns                 |
   afterB | 10.8ns   4.93x      | 35.0ns  3.87x           | 32.3ns  2.41x          |
  ``` 
* Issue discovered by Ludo of failure of `REQUIRE(isnan(pow(D(-2.0),2.5)));`
  - this could be traced back to double_exp::quiet_NaN() * x giving
    infinity rather than NaN -- because renorm assumed that if n_==max_n+1
    then the result was infinity. This was fixed by only setting things to
    infinity if `!std::isnan(d_)` in `renorm()`. Various unit tests added
    to check this.

  - note also: pow(x,p) was going via exp(log) when x was negative and p
    was non-integer. Now instead it just returns quiet_NaN() straight
    off.

  - note that on M2Pro, the combination of changes here worsens the
    TextExpPlus results from 11ns to about 14ns, but improves TestPlus
    from 14ns to 10ns (which is probably more valuable). It's not clear
    why this is the case, perhaps some details of cache optimisation?

* also removed dependency on CmdLine in speed-tests.cc

* added `./register-speed-tests.sh` to be able to more easily track where 
  we stand over time.

* added unit-tests/Makefile, so that we can more easily integrate things
  into unit testing. 