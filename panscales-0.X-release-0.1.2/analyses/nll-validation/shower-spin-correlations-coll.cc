// Runs the collinear spin correlation analysis
///
/// Typical command line:
///
///  build-doubleexp/shower-spin-correlations-coll
///    -shower <shower>
///    -alphas <alphas> -lnkt-cutoff <lnkt_cutoff> -lnvmin <lnvmin> 
///    -use-diffs -nloops 1 -strategy CollinearRap  -lnzmin -11 -collinear-rap-window 11
///    -spin-store-extra
///    -nev <nev> -out <outfile>
///
/// Suggested values:
///   lambda      = -0.5
///   alphas      = 1e-7
///   lnkt_cutoff = lambda/alphas
///   lnvmin      = (1+beta)*lnkt_cutoff (-epsilon)

#include "AnalysisFramework.hh"
#include "ShowerRunner.hh"
#include "fjcore_local.hh"

using namespace std;
using namespace fjcore;
using namespace panscales;

class SpinCorrelationAllOrderFramework : public AnalysisFramework {

precision_type E_1, E_2;
Momentum3<precision_type> ref_direction_1, ref_direction_2;

public:
  SpinCorrelationAllOrderFramework(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in) {}

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() {
    assert(cmdline->present("-spin-do-decluster-analysis") && "Please run with -spin-do-decluster-analysis");
    hists_err["EEEC"].declare(-M_PI, M_PI, 1e-1);
    hists_err["EEEC_qq"].declare(-M_PI, M_PI, 1e-1);
    hists_err["EEEC_gg"].declare(-M_PI, M_PI, 1e-1);
    hists_err["EEEC_rest"].declare(-M_PI, M_PI, 1e-1);

    hists_err["dpsi_sameside"].declare(-M_PI, M_PI, 1e-1);
    hists_err["dpsi_sameside_qq"].declare(-M_PI, M_PI, 1e-1);
    hists_err["dpsi_sameside_gg"].declare(-M_PI, M_PI, 1e-1);
    hists_err["dpsi_sameside_rest"].declare(-M_PI, M_PI, 1e-1);

    hists_err["dpsi_opposite"].declare(-M_PI, M_PI, 1e-1);
    hists_err["dpsi_opposite_gggg"].declare(-M_PI, M_PI, 1e-1);
    hists_err["dpsi_opposite_ggqq"].declare(-M_PI, M_PI, 1e-1);
    hists_err["dpsi_opposite_qqqq"].declare(-M_PI, M_PI, 1e-1);
    hists_err["dpsi_opposite_rest"].declare(-M_PI, M_PI, 1e-1);
  }

  //----------------------------------------------------------------------
  /// Call this here to store the reference directions from the original event 
  void post_startup() {
    AnalysisFramework::post_startup();

    for (unsigned int i=0; i<f_event_base.size(); i++) {
      if (f_event_base[i].barcode() == 1) {
        E_1 = f_event_base[i].E();
        ref_direction_1 = f_event_base[i].direction();
      }
      if (f_event_base[i].barcode() == 2) {
        E_2 = f_event_base[i].E();
        ref_direction_2 = f_event_base[i].direction();
      }
    }
  }

  //----------------------------------------------------------------------
  /// Energy correlator tree searcher
  void search_tree(Parton *p, precision_type E_base, Momentum3<precision_type> direction_base) {

    // Check if this parton represents a vertex
    if (p->get_p_daughter_ptr() != nullptr && p->get_p_son_ptr() != nullptr) {
      // Get energies and angles from this vertex
      precision_type theta_S = p->get_theta();
      Momentum3<precision_type> azimuthal_plane_S = p->get_azimuthal_plane();
      precision_type zi = p->get_p_daughter_ptr()->get_energy()/E_base;
      precision_type zj = p->get_p_son_ptr()->get_energy()/E_base;
      bool is_gg = false;
      bool is_qq = false;
      if ((p->get_p_daughter_ptr()->get_pdg_id() == 21) && 
          (p->get_p_son_ptr()->get_pdg_id()) == 21) {
        is_gg = true;
      }
      else if (abs(p->get_p_daughter_ptr()->get_pdg_id()) == abs(p->get_p_son_ptr()->get_pdg_id())) {
        is_qq = true;
      }

      // Now loop over all ancestors
      Parton* p_ancestor = p;
      while(p_ancestor->get_p_parent_ptr() != nullptr) {
        // Store the current ancestor
        Parton* p_previous = p_ancestor;
        // Move back one ancestor
        p_ancestor = p_ancestor->get_p_parent_ptr();

        // Figure out the relevant angles
        precision_type theta_L = p_ancestor->get_theta();

        // Only bin when theta_S < theta_L
        if (theta_L < theta_S) continue;

        Momentum3<precision_type> azimuthal_plane_L = p_ancestor->get_azimuthal_plane();

        Momentum3<precision_type> twice_perp = cross(azimuthal_plane_S, azimuthal_plane_L);
        precision_type dpsi = atan2(cross(azimuthal_plane_S, azimuthal_plane_L).modp(), 
          dot_product_3(azimuthal_plane_S, azimuthal_plane_L));
        precision_type sign = dot_product_3(twice_perp, direction_base);
        if (sign < 0) dpsi = -dpsi;

        // Figure out the relevant energy
        precision_type zk;
        if (p_ancestor->get_p_daughter_ptr() == p_previous) {
          // If we followed the daughter, get the son energy
          zk = p_ancestor->get_p_son_ptr()->get_energy()/E_base;
        }
        else if (p_ancestor->get_p_son_ptr() == p_previous) {
          // If we followed the son, get the daughter energy
          zk = p_ancestor->get_p_daughter_ptr()->get_energy()/E_base;
        }
        else {
          std::cout << "Error in spincorr tree." << std::endl;
          zk = 0.0;
        }

        // Compute the weight
        double weight = to_double(zi*zj*zk);
        double two_cos = 2*cos(2*to_double(dpsi));
        
        // Add the entry to histograms
        hists_err["EEEC"].add_entry(to_double(dpsi), weight);
        xsections["EEEC_a0"] += weight;
        xsections["EEEC_a2"] += weight*two_cos;
        if (is_gg) {
          hists_err["EEEC_gg"].add_entry(to_double(dpsi), weight);
          xsections["EEEC_gg_a0"] += weight;
          xsections["EEEC_gg_a2"] += weight*two_cos;
        }
        else if (is_qq) {
          hists_err["EEEC_qq"].add_entry(to_double(dpsi), weight);
          xsections["EEEC_qq_a0"] += weight;
          xsections["EEEC_qq_a2"] += weight*two_cos;
        }
        else {
          hists_err["EEEC_rest"].add_entry(to_double(dpsi), weight);
          xsections["EEEC_rest_a0"] += weight;
          xsections["EEEC_rest_a2"] += weight*two_cos;
        }
      }

      // Keep going further
      search_tree(p->get_p_daughter_ptr(), E_base, direction_base);
      search_tree(p->get_p_son_ptr(), E_base, direction_base);
    }
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() {

    // --------------------------- Dpsi analysis ---------------------------
    // Dpsi sameside1
    if (f_shower_runner->get_spin_cor_tree_ptr()->found_primary_sameside_1_ && 
        f_shower_runner->get_spin_cor_tree_ptr()->found_secondary_sameside_1_) {

      double dpsi = f_shower_runner->get_spin_cor_tree_ptr()->dpsi_sameside_1_;
      hists_err["dpsi_sameside"].add_entry(dpsi, event_weight());
      xsections["dpsi_sameside_a0"] += event_weight();
      xsections["dpsi_sameside_a2"] += event_weight()*2*cos(2*dpsi);

      if (f_shower_runner->get_spin_cor_tree_ptr()->is_gg_sameside_1_) {
        hists_err["dpsi_sameside_gg"].add_entry(dpsi, event_weight());
        xsections["dpsi_sameside_gg_a0"] += event_weight();
        xsections["dpsi_sameside_gg_a2"] += event_weight()*2*cos(2*dpsi);
      }
      else if (f_shower_runner->get_spin_cor_tree_ptr()->is_qqbar_sameside_1_) {
        hists_err["dpsi_sameside_qq"].add_entry(dpsi, event_weight());
        xsections["dpsi_sameside_qq_a0"] += event_weight();
        xsections["dpsi_sameside_qq_a2"] += event_weight()*2*cos(2*dpsi);
      }
      else {
        hists_err["dpsi_sameside_rest"].add_entry(dpsi, event_weight());
        xsections["dpsi_sameside_rest_a0"] += event_weight();
        xsections["dpsi_sameside_rest_a2"] += event_weight()*2*cos(2*dpsi);
      }
    }

    // Dpsi sameside2
    if (f_shower_runner->get_spin_cor_tree_ptr()->found_primary_sameside_2_ && 
        f_shower_runner->get_spin_cor_tree_ptr()->found_secondary_sameside_2_) {

      double dpsi = f_shower_runner->get_spin_cor_tree_ptr()->dpsi_sameside_2_;
      hists_err["dpsi_sameside"].add_entry(dpsi, event_weight());
      xsections["dpsi_sameside_a0"] += event_weight();
      xsections["dpsi_sameside_a2"] += event_weight()*2*cos(2*dpsi);

      if (f_shower_runner->get_spin_cor_tree_ptr()->is_gg_sameside_2_) {
        hists_err["dpsi_sameside_gg"].add_entry(dpsi, event_weight());
        xsections["dpsi_sameside_gg_a0"] += event_weight();
        xsections["dpsi_sameside_gg_a2"] += event_weight()*2*cos(2*dpsi);
      }
      else if (f_shower_runner->get_spin_cor_tree_ptr()->is_qqbar_sameside_2_) {
        hists_err["dpsi_sameside_qq"].add_entry(dpsi, event_weight());
        xsections["dpsi_sameside_qq_a0"] += event_weight();
        xsections["dpsi_sameside_qq_a2"] += event_weight()*2*cos(2*dpsi);
      }
      else {
        hists_err["dpsi_sameside_rest"].add_entry(dpsi, event_weight());
        xsections["dpsi_sameside_rest_a0"] += event_weight();
        xsections["dpsi_sameside_rest_a2"] += event_weight()*2*cos(2*dpsi);
      }
    }

    // Dpsi opposite
    if (f_shower_runner->get_spin_cor_tree_ptr()->found_primary_opposite_1_ &&
        f_shower_runner->get_spin_cor_tree_ptr()->found_primary_opposite_2_) {
      
      double dpsi = f_shower_runner->get_spin_cor_tree_ptr()->dpsi_opposite_;
      hists_err["dpsi_opposite"].add_entry(dpsi, event_weight());
      xsections["dpsi_opposite_a0"] += event_weight();
      xsections["dpsi_opposite_a2"] += event_weight()*2*cos(2*dpsi);

      if (f_shower_runner->get_spin_cor_tree_ptr()->is_gg_opposite_1_ &&
          f_shower_runner->get_spin_cor_tree_ptr()->is_gg_opposite_2_) {
        hists_err["dpsi_opposite_gggg"].add_entry(dpsi, event_weight());
        xsections["dpsi_opposite_gggg_a0"] += event_weight();
        xsections["dpsi_opposite_gggg_a2"] += event_weight()*2*cos(2*dpsi);
      }

      else if (f_shower_runner->get_spin_cor_tree_ptr()->is_qqbar_opposite_1_ && 
               f_shower_runner->get_spin_cor_tree_ptr()->is_qqbar_opposite_2_) {
        hists_err["dpsi_opposite_qqqq"].add_entry(dpsi, event_weight());
        xsections["dpsi_opposite_qqqq_a0"] += event_weight();
        xsections["dpsi_opposite_qqqq_a2"] += event_weight()*2*cos(2*dpsi);
      }

      else if (f_shower_runner->get_spin_cor_tree_ptr()->is_gg_opposite_1_ && 
               f_shower_runner->get_spin_cor_tree_ptr()->is_qqbar_opposite_2_) {
        hists_err["dpsi_opposite_ggqq"].add_entry(dpsi, event_weight());
        xsections["dpsi_opposite_ggqq_a0"] += event_weight();
        xsections["dpsi_opposite_ggqq_a2"] += event_weight()*2*cos(2*dpsi);
      }

      else if (f_shower_runner->get_spin_cor_tree_ptr()->is_qqbar_opposite_1_ && 
               f_shower_runner->get_spin_cor_tree_ptr()->is_gg_opposite_2_) {
        hists_err["dpsi_opposite_ggqq"].add_entry(dpsi, event_weight());
        xsections["dpsi_opposite_ggqq_a0"] += event_weight();
        xsections["dpsi_opposite_ggqq_a2"] += event_weight()*2*cos(2*dpsi);
      }

      else {
        hists_err["dpsi_opposite_rest"].add_entry(dpsi, event_weight());
        xsections["dpsi_opposite_rest_a0"] += event_weight();
        xsections["dpsi_opposite_rest_a2"] += event_weight()*2*cos(2*dpsi);
      }
    }

    // --------------------------- EEEC analysis ---------------------------
    // Loop over the first branch
    search_tree(&f_shower_runner->get_spin_cor_tree_ptr()->parton_map_[1], E_1, ref_direction_1);

    // Loop over the second branch
    search_tree(&f_shower_runner->get_spin_cor_tree_ptr()->parton_map_[2], E_2, ref_direction_2);
  }
};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  SpinCorrelationAllOrderFramework driver(&cmdline);
  driver.run();
}
