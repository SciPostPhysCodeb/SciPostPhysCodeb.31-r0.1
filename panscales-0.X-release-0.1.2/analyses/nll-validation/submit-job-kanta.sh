#!/bin/bash

# get the current working dir
wd=`pwd`

# make sure the jobdir exists
jobdir=${wd}/kanta-jobs
jobid=1
if [ -d ${jobdir} ]; then
    # get the job ID
    jobid=`cat ${jobdir}/.qjobid`
    jobid=$(($jobid+1))
    echo $jobid > ${jobdir}/.qjobid
else
    # create the directory structure
    mkdir -p ${jobdir}
    mkdir -p ${jobdir}/scripts
    mkdir -p ${jobdir}/logs
    echo "1" > ${jobdir}/.qjobid
fi

# email address
email_command=""
if [[ "${USER}" == "gsoyez" ]]; then
    email_command="#PBS -M gregory.soyez@ipht.fr"
elif [[ "${USER}" == "asoto" ]]; then
    email_command="#PBS -M alba.soto@ipht.fr"
fi

# output will be saved to the following file
name="auto_${jobid}"

logfile="${jobdir}/logs/auto_${jobid}.log"
errfile="${jobdir}/logs/auto_${jobid}.err"

# process potential arguments
while (($#)); do
    if [ "$1" == "--name" ]; then
        shift
        name="$1"
        shift
    elif [ "$1" == "--log" ]; then
        shift
        logfile="$1"
        shift
    elif [ "$1" == "--err" ]; then
        shift
        errfile="$1"
        shift
    else
        break
    fi
done

# create the script with the appropriate command
cat > ${jobdir}/scripts/auto_${jobid} << EOF
#PBS -S /bin/bash
#PBS -N ${name}
#PBS -o ${logfile}
#PBS -e ${errfile}
#PBS -m abe
${email_command}
#PBS -l select=1:ncpus=1

cd ${wd}
$@

EOF

# submit the script
echo "will launch job ${jobdir}/scripts/auto_${jobid}"
qsub ${jobdir}/scripts/auto_${jobid}
