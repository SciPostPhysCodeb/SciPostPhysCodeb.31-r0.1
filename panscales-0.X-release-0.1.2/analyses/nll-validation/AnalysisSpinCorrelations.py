import sys
import os
import argparse
import glob
from math import sqrt
import numpy as np
sys.path.append("../../submodules/AnalysisTools/python")
from hfile import *
from poly_interp import *
import shutil

from utils import yes_no,print_log,stdout_from
import utils # for the variables
from AnalysisBase import *

#------------------------------------------------------------------------
# handles spin correlation observables
#
# 2 analyses:
#
# 1. Impose that the first emission is collinear and a second emission (secondary from the 1st one) to be collinear as well
#       build-doubleexp/shower-spin-correlations-coll -shower panglobal -beta 0.0 -alphas 1e-7 -lnkt-cutoff -5e6 \
#         -lnvmin -5.00005e6 -use-diffs -nloops 1 -strategy CollinearRap -lnzmin -11 -collinear-rap-window 11 -spin-store-extra -nev 1000 -out a
#
# 2. Impose that the first emission is soft (central) and a second emission (secondary from the 1st one) to be collinear 
#       build-doubleexp/shower-spin-correlations-soft -shower panglobal -beta 0.0 -alphas 1e-7 -lnkt-cutoff -5e6 \
#         -lnvmin -5.00005e6 -use-diffs -nloops 1 -strategy CentralAndCollinearRap -half-central-rap-window 9 -collinear-rap-window 9 \
#         -split-eta-max 10 -lnErel_min -10 -nev 1000 -out a


#------------------------------------------------------------------------
class SpinCorrelations(AnalysisBase):
    '''
    initialisation (no arguments)
    '''
    def __init__(self):
        # parameters adjustable from the command line
        #
        # Note: from the pp runs, we had run 80 1M event runs
        # each run last ~8k seconds (2.25 hours), alphas=1e-9, L=5e8 (+epsilon), buffer of 13)
        # This gives an accuracy around 2e-4.
        #
        # Trying to scale this:
        #  - do 1-day runs would do 10M events
        #  - we'd just need 8 jobs to get to an accuracy of 2e-4
        #  - 1 job swould get you ~6e-4
        self.default_alphas   = 1e-7
        self.default_lambda   = -0.5
        self.default_accuracy =  0.01   # gives a ~0.0004 absolute uncertainty on_sameside_gg(coll) or dpsi_gg(soft)



    '''
    define specific command-line arguments
    '''
    def add_parser_arguments(self, parser):
        self.parser = parser
        group = parser.add_argument_group(f'arguments for the spin-correlation studies')

        group.add_argument('--spincorr-modes', default=['coll', 'soft'],
                           nargs='+', choices=['coll', 'soft'], 
                           help='which spin-correlation tests to run')
        group.add_argument('--spincorr-lambda', type=float, default= self.default_lambda, 
                           help='target lambda=as L for multiplicity')
        group.add_argument('--spincorr-alphas', type=float, default=self.default_alphas, 
                           help='value of alphas used (no extrapolation)')
        group.add_argument('--spincorr-accuracy', type=float, default=self.default_accuracy, 
                           help='target statistical accuracy')
        group.add_argument('--spincorr-nev-warmup', type=int, default=40000, 
                           help='number of events for the warmup run')

    def args(self):
        if not hasattr(self, 'args_'):
            self.args_ = self.parser.parse_args()
        return self.args_
        
    '''
    initialisation/warmup
    '''
    def warmup(self, runner):
        # basic info
        self.lmbda    = self.args().spincorr_lambda
        self.alphas   = self.args().spincorr_alphas
        self.accuracy = self.args().spincorr_accuracy

        modes = self.args().spincorr_modes
        self.modes = []

        for mode in modes:
            # currently, the analytics is only available for lambda=-0.5. Make sure this is the case
            if self.lmbda<-0.50001 or self.lmbda>-0.49999:
                print_log(f"Analytic reference results only available for lambda=-0.5 and {self.lmbda} was requested. Skipping spin-correlation tests for mode {mode}.")
                continue

            # check that the analytic results are available
            fname=f"analytic-spincorr-refs/spincorr-{self.args().proc}-lambda0.5-{mode}.res"
            if not os.path.isfile(fname):
                print_log(f"Analytic reference results are not available for this process (file '{fname}' not found). Skipping spin-correlation tests for mode {mode}.")
            else:
                self.modes.append(mode)

        if not self.modes:
            return

        self.lnkt_cutoff = self.lmbda/self.alphas
        security = -4
        self.lnvmin = (1+utils.showers[self.args().shower]['beta'])*self.lnkt_cutoff+security
        
        proc = utils.procs[self.args().proc  ]['cmd_args']

        self.warmupdir= utils.outdir+'spin-correlations/'
        if not os.path.isdir(self.warmupdir): os.mkdir(self.warmupdir)
        self.warmupdir= utils.outdir+'spin-correlations/warmup/'
        queue_jobs = True
        if os.path.isdir(self.warmupdir): 
            if yes_no('warmup results for spin correlations seem to be already available. Do you want to generate new ones?'):
                shutil.rmtree(self.warmupdir)
                os.mkdir(self.warmupdir)
            else:
                queue_jobs = False
        else:
            os.mkdir(self.warmupdir)

        # build a basic command wo
        #  ../build-{float_precisions[ialphas]}/
        #  -rseq ARG_REPLACE -nev {nevs} 
        #  -out ...
        rts = self.args().rts if self.args().proc.startswith('pp') else 1.0
        self.command_base = {}
        if 'coll' in self.modes:
            self.command_base['coll'] = f""" build-doubleexp/shower-spin-correlations-coll \
                -shower {utils.showers[self.args().shower]['cmd_args']} {self.args().extra} \
                {utils.procs[self.args().proc]['cmd_args']} -Q 1.0 -rts {rts} \
                -alphas {self.alphas} -lnkt-cutoff {self.lnkt_cutoff} -lnvmin {self.lnvmin} \
                -spin-do-decluster-analysis \
                -use-diffs -nloops 1 -strategy CollinearRap -lnzmin -11 -collinear-rap-window 11 -spin-store-extra """
        if 'soft' in self.modes:
            self.command_base['soft'] = f""" build-doubleexp/shower-spin-correlations-soft \
                -shower {utils.showers[self.args().shower]['cmd_args']} {self.args().extra} \
                {utils.procs[self.args().proc]['cmd_args']} -Q 1.0 -rts {rts} \
                -alphas {self.alphas} -lnkt-cutoff {self.lnkt_cutoff} -lnvmin {self.lnvmin} \
                -use-diffs -nloops 1 -strategy CentralAndCollinearRap -half-central-rap-window 9 \
                -collinear-rap-window 9 -split-eta-max 10 -lnErel_min -10 """

        self.njobs_warmup = 2
        self.outbase = {}
        for mode in self.modes:
            self.queue_commands(runner, mode, self.args().spincorr_nev_warmup, self.njobs_warmup,
                                self.warmupdir, queue_jobs = queue_jobs, warmup=True)

    '''
    process the warmup output
    '''
    def process_warmup(self):
        if not self.modes:
            print_log("Skipping spin tests (analytic results not available).")
            return
        self.nev_per_job = {}
        self.unc_one_job = {}
        self.njobs       = {}

        nev_per_job = {}
        unc_one_job = {}

        for mode in self.modes:
            # we want to get (i) the number of events per job (ii) the number of jobs
            # based on (a) the timing (b) the accuracy for test runs
            nev_per_job[mode] = 0.0
            unc_one_job[mode] = 0.0
    
            a0_sum  = 0.0
            da0_sum = 0.0
            a2_sum  = 0.0
            da2_sum = 0.0
            t  = 0.0
            for iseq in range(1, 3):
                f = open(f'{self.outbase[mode]}{iseq}.res', 'r')
                
                # get the elapsed time
                elapsed_str = search(f, '^# time now = .*, elapsed = .*', return_line=True)
                t += float(elapsed_str[elapsed_str.rfind('=')+1:elapsed_str.rfind(' h')])
    
                # the delta psi for same-side gg emissions has the
                # smallest spin corelations and hence is the harder one to
                # get. We take this as our benchmark for speed calculation
                obs = 'dpsi_sameside_gg' if mode == 'coll' else 'dpsi_gg'
                a0, da0, a2, da2 = self.get_a0a2(f, obs)
                a0_sum  += a0
                da0_sum += da0**2
                a2_sum  += a2
                da2_sum += da2**2
                    
            # normalise s and e2 as if we were combining the 2 sequences
            a0_sum  /= self.njobs_warmup
            a2_sum  /= self.njobs_warmup
            da0_sum /= self.njobs_warmup**2
            da2_sum /= self.njobs_warmup**2
    
            rel_uncert2 = (da0_sum/a0_sum**2 + da2_sum/a2_sum**2)
    
            # number of events per job (of requested length)
            #   24 hours * 2 runs * nev_per_run / time_all_runs(hours)
            nev_per_job[mode] = 24*self.args().job_length*self.njobs_warmup*self.args().spincorr_nev_warmup/t

            # uncertainty scaled for one job  (of requested length)
            #
            # Note that here we take the absolute uncertainty on the
            # multiplicity (normalised by1/sqrt(alphas) for NDL tests)
            unc_one_job[mode] = sqrt(rel_uncert2/(nev_per_job[mode]/(self.njobs_warmup*self.args().spincorr_nev_warmup)))
    
    
        while True:
            print_log ("SpinCorr job summary (after warmup):")
            print_log (f'mode                  nevs        njobs')

            for mode in self.modes:
                # round the number of events per job
                nref = 1000
                self.nev_per_job[mode] = nref*max(int(nev_per_job[mode]/nref), 1)

                # update the uncertainty
                self.unc_one_job[mode] = unc_one_job[mode] * (nev_per_job[mode]/self.nev_per_job[mode])**2
            
                # for the number of jobns, the scaling is easy:
                self.njobs[mode] = int((self.unc_one_job[mode]/self.accuracy)**2+1)
            
                print_log (f'{mode}          {int(self.nev_per_job[mode]):12} {int(self.njobs[mode]):12}')
            print_log(f'target accuracy of {self.accuracy}')
            if yes_no('Accept these settings?'):
                break
            else:
                self.accuracy = float(input('Enter new target accuracy: '))
        
    '''
    queue the main jobs
    '''
    def main_jobs(self, runner):
        if not self.modes:
            print_log("Skipping spin tests (analytic results not available).")
            return
        
        # we build the jobs from
        # - the number of events per day in self.nevs_per_job[ialpha]
        # - the number of jobs in self.njobs[ialpha]
        self.jobdir= utils.outdir+'spin-correlations/parts/'
        if os.path.isdir(self.jobdir): 
            if yes_no('(main) results for the spin correlations seem to be already available. Do you want to generate new ones?'):
                shutil.rmtree(self.jobdir)
            else:
                return
        os.mkdir(self.jobdir)

        for mode in self.modes:
            self.queue_commands(runner, mode, self.nev_per_job[mode], self.njobs[mode], self.jobdir)

    '''
    process the main jobs
    '''
    def process_main(self):
        if not self.modes:
            print_log("Skipping spin tests (analytic results not available).")
            return
        
        observables = {'coll' : ['EEEC', 'EEEC_gg', 'EEEC_qq', 'EEEC_rest',
                                 'dpsi_opposite', 'dpsi_opposite_rest',
                                 'dpsi_sameside', 'dpsi_sameside_gg', 'dpsi_sameside_qq', 'dpsi_sameside_rest'],
                       'soft' : ['dpsi', 'dpsi_gg', 'dpsi_qq', 'dpsi_rest']}

        for mode in self.modes:
            # create the output file
            fout = open(f'{utils.outdir}{self.args().shower}-spincorr-lambda{self.lmbda}-{mode}', 'w')
            self.outbase[mode] = self.outbase[mode].replace('warmup', 'parts')

            # get the analytic result
            f = open(f"analytic-spincorr-refs/spincorr-{self.args().proc}-lambda0.5-{mode}.res", "r")
            analytic_ratio  = np.zeros(len(observables[mode]))
            analytic_uncert = np.zeros(len(observables[mode]))
            
            # the next core assumes that the entries in the result file
            # are ordered as in the above "observables" array
            for i, obs in enumerate(observables[mode]):
                while True:
                    l = f.readline()
                    if not l: break
                    if l.startswith(f'{obs} '):
                        tokens = l.split()
                        analytic_ratio [i] = float(tokens[1])
                        analytic_uncert[i] = float(tokens[2])
                        break
    
            a0_sum  = np.zeros(len(observables[mode]))
            da0_sum = np.zeros(len(observables[mode]))
            a2_sum  = np.zeros(len(observables[mode]))
            da2_sum = np.zeros(len(observables[mode]))
    
            for iseq in range(1, int(self.njobs[mode])+1):
                f = open(f'{self.outbase[mode]}{iseq}.res', 'r')
                
                for i, obs in enumerate(observables[mode]):
                    a0, da0, a2, da2 = self.get_a0a2(f, obs)
                    a0_sum[i]  += a0
                    da0_sum[i] += da0**2
                    a2_sum[i]  += a2
                    da2_sum[i] += da2**2
                    
            # normalise s and e2 as if we were combining the 2 sequences
            a0_sum  /= self.njobs[mode]
            a2_sum  /= self.njobs[mode]
            da0_sum /= self.njobs[mode]**2
            da2_sum /= self.njobs[mode]**2

            shower_ratio  = a2_sum/a0_sum
            shower_uncert = abs(shower_ratio) * np.sqrt(da0_sum/np.square(a0_sum) + da2_sum/np.square(a2_sum))
    
            # final quality measure
            diff     = shower_ratio - analytic_ratio
            diff_err = np.sqrt((shower_uncert)**2 + (analytic_uncert)**2)
    
            diff_err_a = analytic_uncert
            diff_err_s = shower_uncert
    
            # print the averages
            print(f'# spin correlations', file=fout)
            print (f'# columns: observable value uncertainty analytic uncertainty shower-analytics uncertainty', file=fout)
            for i, obs in enumerate(observables[mode]):
                print(f'{obs} {shower_ratio[i]} {shower_uncert[i]} {analytic_ratio[i]} {analytic_uncert[i]} {diff[i]} {diff_err[i]}', file=fout)
            print(f'', file=fout)
            print(f'', file=fout)            
    
            # produce the final summary
            print_log( '#--------------------------------------------------', extra_file=fout)
            print_log(f'# NLL spin correlations summary [{mode}]',            extra_file=fout)
            print_log( '#--------------------------------------------------', extra_file=fout)
            val  = diff
            for i, obs in enumerate(observables[mode]):
                if (abs(val[i]) < 2*diff_err[i]):
                    status = '\033[0;32mOK    \033[0m'
                elif (abs(val[i]) < 4*diff_err[i]):
                    status = '\033[0;33mRE-RUN\033[0m'
                else: 
                    status = '\033[0;31mNot OK\033[0m'
                sigmas = abs(val)/diff_err
                print_log(f'{obs:>20s}  {status} ({val[i]*100:+.6f} +- {diff_err_s[i]*100:.6f} +- {diff_err_a[i]*100:.6f})% [{sigmas[i]:5.2f} σ]', extra_file=fout)
            print_log('\n', extra_file=fout)

    '''
    add commands to the job runner's queue
    '''
    def queue_commands(self, runner, mode, nev, njobs, outdir, queue_jobs=True, warmup=False): 
        runtime=0.3 if warmup else 1.2*self.args().job_length

        out = f'{outdir}{self.args().shower}-lambda{self.lmbda}-{mode}-rseq'
        self.outbase[mode] = out
        if queue_jobs:
            command=f'{self.command_base[mode]} -rseq ARG_REPLACE -nev {nev} -out {out}ARG_REPLACE.res'
            runner.run(command, groupname=self.args().name+'-spin',
                       range=f'1-{njobs}',days=runtime)

    '''
    read a0, da0, a2, da2 from a result file for a given observable
    (optionally including channel)
    '''
    def get_a0a2(self, f_in, obs):
        f = open(f_in, 'r') if type(f_in) is str else f_in

        a0 = None
        a2 = None
        while True:
            l = f.readline()
            if not l: break
            if l.startswith(f'# {obs}_a0:'):
                tokens = l.split()
                a0  = float(tokens[4])
                da0 = float(tokens[6])
                if a2 is not None: break
            if l.startswith(f'# {obs}_a2:'):
                tokens = l.split()
                a2  = float(tokens[4])
                da2 = float(tokens[6])
                if a0 is not None: break

        if a0 is None or a2 is None:
            print_log(f'could not find a0 and a2 for {obs} in {f_in}')
            sys.exit(1)

        return a0, da0, a2, da2
