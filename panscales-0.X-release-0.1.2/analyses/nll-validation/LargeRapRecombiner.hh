// This code initially comes from Gavin's (et al) jet flavour studies.
// We've simplified it a little bit and renamed it so as to better
// highlight its purpose here.

#ifndef __LARGERAPRECOMBINER_HH__
#define __LARGERAPRECOMBINER_HH__
#include "fjcore_local.hh"
#include <limits>


FJCORE_BEGIN_NAMESPACE

class LargeRapMassInfo : public PseudoJet::UserInfoBase {
public:
  LargeRapMassInfo(precision_type m_in = 0.0) : _m(m_in) {}
  void set_mass(precision_type m_in) { _m = m_in; }
  precision_type mass() const { return _m; }
  
private:
  precision_type _m;
};

class LargeRapRecombiner : public JetDefinition::DefaultRecombiner {
public:
  /// Perform a recombination taking the cached mass into account,
  /// so as to be able to correctly reconstruct the rapidity of the
  /// newly formed particle, even when pa and pb are at quite
  /// extreme rapidities.
  void recombine(const PseudoJet &pa, const PseudoJet &pb,
                 PseudoJet &pab) const {
    precision_type ma = mass_of_particle(pa);
    precision_type mb = mass_of_particle(pb);
    
    precision_type avrap = (pa.rap() + pb.rap()) *0.5;
    
    PseudoJet shifted_pa = PtYPhiM(pa.pt(), pa.rap() - avrap, pa.phi(), ma);
    PseudoJet shifted_pb = PtYPhiM(pb.pt(), pb.rap() - avrap, pb.phi(), mb);
    
    //shifted_pa.set_user_info(new LargeRapMassInfo(ma));
    //shifted_pb.set_user_info(new LargeRapMassInfo(mb));
    
    // Recombine using the default recombiner
    PseudoJet shifted_pab;
    JetDefinition::DefaultRecombiner::recombine(shifted_pa, shifted_pb, shifted_pab);
    
    pab = PtYPhiM(shifted_pab.pt(), shifted_pab.rap() + avrap,
                  shifted_pab.phi(), shifted_pab.m());
    pab.set_user_info(new LargeRapMassInfo(shifted_pab.m()));
  }
  
  precision_type mass_of_particle(const PseudoJet &p) const {
    if (p.has_user_info<LargeRapMassInfo>())
      return p.user_info<LargeRapMassInfo>().mass();
    // otherwise we expect the mass to be zero to within rounding errors
    // so check this
    precision_type m2 = p.m2();
    const precision_type safety_factor = 10.0;
    if (std::abs(m2) > safety_factor *
        std::numeric_limits<precision_type>::epsilon() *
        pow(p.E(), 2)) {
      throw Error("LargeRapRecombiner found particle without LargeRapMassInfo,"
                  "whose mass is inconsistent with zero");
    }
    // if things are OK, then just return zero.
    return 0.0;
  }
};

FJCORE_END_NAMESPACE 

#endif // __LARGERAPRECOMBINER_HH__
