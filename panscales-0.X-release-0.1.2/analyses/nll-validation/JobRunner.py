#!/usr/bin/env python3

import argparse
import subprocess
import re
import os
import sys
from multiprocessing import Pool

if sys.version_info[0] != 3:
    print("This script requires Python 3")
    exit()


def job_runner_pool_launcher(command):
    print (f"Running {command}")
    subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


    
class Runner(object):
    '''
    Class to help run jobs, both local test ones and for submission to clusters
    '''
    def __init__(self, external_parser = None, default_queue=None):
        self.parser = argparse.ArgumentParser() if external_parser is None else external_parser
        # a shorthand
        parser = self.parser

        group = parser.add_mutually_exclusive_group()
        #group.add_argument("--cern", action="store_true")
        group.add_argument("--hydra", action="store_true", help='prepare for submission on hydra')
        group.add_argument("--kanta", action="store_true", help='prepare for submission on kanta')
        parser.add_argument("-1", dest='range_of_one', action="store_true", help='adapt run to first element of any range')

        parser.add_argument("-s", "--submit", action="store_true", dest='submit')
        parser.add_argument("-m", "--memory", type=str, default="", help="memory requirements (0.1 GB by default, works only on hydra)")
        parser.add_argument("-q", "--queue", type=str, default=default_queue, help="queue name (empty defaults to long on hydra)")
        
        parser.add_argument("--sem", action="store_true",help='run with sem')
        parser.add_argument("--pool", action="store_true",help="run with python's Pool multithreading")
        parser.add_argument("-j", type=int, const=0, default=1, nargs='?',
                            help='pass a -j argument to sem or pool (default 1); if -j is given on its own, use all available cores')


        self.commands = []
        self.jobsystem = None
        self.queue = None
        
    def args(self):
        if not hasattr(self, 'args_'):
            self.args_ = self.parser.parse_args()
        return self.args_
        

    def range_as_text(self, range_arg, separator=","):
        '''Return the range in a format suitable for hydra (or CERN) job
        systems'''
        if (type(range_arg) is tuple or type(range_arg) is list) and len(range_arg) == 2:
            return f"{range_arg[0]}{separator}{range_arg[1]}"
        else:
            return re.sub("[,-]", separator, range_arg)

    def range_as_expanded_list(self,range_arg):
        '''Return the range in a form suitable for other uses'''
        if (type(range_arg) is tuple or type(range_arg) is list) and len(range_arg) == 2:
            return range(range_arg[0], range_arg[1]+1)
        else:
            m = re.match(r'([0-9]+)[,-]([0-9]+)', range_arg)
            return range(int(m.group(1)), int(m.group(2))+1)

    def append_expanded_range(self, command, range):
        '''for each index in "range" append one command replacing ARG_REPLACE with the index'''
        do_range = (range is not None) and (not self.args().range_of_one)
        if do_range:
            range_vals = self.range_as_expanded_list(range)
            for arg_index in range_vals:
                self.commands.append(command.replace("%I", str(arg_index)).replace("ARG_REPLACE", str(arg_index)))
        else:
            self.commands.append(command)

    def run(self, command, hours=None, days=None, range = None, groupname= None):
        '''
        This doesn't actually run the command, but adds it to the list of things to be run.
        '''
        # don't do anything if the command is empty
        if command == "": return
        do_range = (range is not None) and (not self.args().range_of_one)
        ## specifically on hydra we can add a groupname

        if groupname is None:
            m = re.search(r'-ou?t? +([^ ]+)',command)
            if m:
                groupname = re.sub(r'rseq.*','',m.group(1))
                groupname = re.sub(r'.*/','',groupname)

        if (self.args().range_of_one and range is not None):
            #@MvB: why did you replace the above line with this:
            # if do_range:
            #It breaks things for me on some platforms
            rangeval = self.range_as_expanded_list(range)[0] #"{:04d}".format(int(re.sub('[,-].*','',range)))
            command = command.replace("%I", str(rangeval))
            command = command.replace("ARG_REPLACE", str(rangeval))
            
        command = re.sub(r' +',' ',command)
        if (self.args().hydra):
            jobs_output_dir = "{}/JOBS".format(os.getcwd())
            if not os.path.isdir(jobs_output_dir): os.mkdir(jobs_output_dir)
            full_command = "addqueue -o {}/%j.out ".format(jobs_output_dir)
            #if (do_range):              full_command += " --range {} ".format(range.replace("-",","))
            if (do_range):              full_command += " --range {} ".format(self.range_as_text(range))
            if (groupname is not None): full_command += f" -g {groupname} "
            #else                      : full_command += " -g random "

            if (self.queue) : full_command += f" --queue {self.queue} "
            if (self.args().queue) : full_command += f" --queue {self.args().queue} "
            if (self.args().memory) : full_command += f" --memory {self.args().memory} "

            duration_hours=0
            if (days  is not None): duration_hours += 24*days
            if (hours is not None): duration_hours += hours
            if   (duration_hours >= 24): full_command += f" -c '{duration_hours/24:.1f} day(s)' "
            elif (duration_hours >   0): full_command += f" -c '{duration_hours:.1f} hour(s)' "

            full_command += command
            if (do_range): full_command = full_command.replace("%I", "ARG_REPLACE")
            self.commands.append(full_command)
            self.jobsystem = "hydra"
        elif self.args().kanta:
            # run jobs on the IPhT cluster
            # 
            # if we're running a range, prepare one command per rseq
            # else, just add the one command
            script_path=os.path.dirname(os.path.realpath(__file__))
            command_base = f"{script_path}/submit-job-kanta.sh --name {groupname} {command}"
            self.append_expanded_range(command_base, range)
            self.jobsystem = "kanta"
        elif (self.args().sem):
            ncores='' if self.args().j==0 else self.args().j
            full_command = f"sem -j{ncores} "+command
            self.append_expanded_range(full_command, range)
            self.jobsystem = "sem"
        else:
            #command = command.replace("ARG_REPLACE","1")
            #self.commands.append(command)
            self.append_expanded_range(command, range)
            self.jobsystem = "pool" if self.args().pool else "local"
            
    def __del__(self):
        self.execute_commands()

    def execute_commands(self, assume_yes=False):
        '''
        All the action will take place at the end, once we are ready to destroy the class
        '''
        # protection: if we don't need to do anything we stop here
        # (this will avoid errors on deletion...)
        if (len(self.commands) == 0): return
        
        print("Command list")
        print("------------")
        for command in self.commands:
            print(command)
        print(f"[{len(self.commands)} commands]")
        print("-----------------------------------------------------")
        print (f'{assume_yes=}')
        if (self.jobsystem is None and self.args().submit):
            if assume_yes:
                print ("Are you sure you want to run these jobs locally in the background? (y/n) [assume yes]")
                answer = 'y'
            else:
                answer = input("Are you sure you want to run these jobs locally in the background? (y/n) ")
            if (answer != "y"): 
                self.commands = []
                return
            for command in self.commands:
                subprocess.Popen(command, shell=True)
        elif (self.jobsystem=="pool" and self.args().submit):
            if assume_yes:
                print (f"Are you sure you want to submit these jobs on {self.jobsystem}? (y/n) [assume yes]")
                answer = 'y'
            else:
                answer = input(f"Are you sure you want to submit these jobs on {self.jobsystem}? (y/n) ")
            if (answer != "y"): 
                self.commands = []
                return
            ncores=None if self.args().j==0 else self.args().j
            with Pool(ncores) as pool:        
                _ = pool.starmap(job_runner_pool_launcher, [[cmd] for cmd in self.commands])
                pool.close()
                print (f"Waiting for jobs to finish")                
                pool.join()
            print (f"All the jobs are done")                
        elif (self.args().submit and self.jobsystem is not None):
            if assume_yes:
                print (f"Are you sure you want to submit these jobs on {self.jobsystem}? (y/n) [assume yes]")
                answer = 'y'
            else:
                answer = input(f"Are you sure you want to submit these jobs on {self.jobsystem}? (y/n) ")
            if (answer != "y"): 
                self.commands = []
                return
            for command in self.commands:
                print(command)
                subprocess.run(command, shell=True)
        else:
            print ("Not submitting or running any of these jobs (add -s to submit)")

        print ("all commands have been executed")
        self.commands = []
            
