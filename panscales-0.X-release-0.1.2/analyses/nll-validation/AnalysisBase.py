
#------------------------------------------------------------------------
# Base analysis class
#------------------------------------------------------------------------
class AnalysisBase(object):
    '''
    initialisation (no arguments)
    '''
    def __init__(self):
        pass

    '''
    define specific command-line arguments
    '''
    def add_parser_arguments(self, parser):
        pass

    '''
    initialisation/warmup
    '''
    def warmup(self, runner):
        pass

    '''
    process the warmup output
    '''
    def process_warmup(self):
        pass

    '''
    construct the main series of jobs
    '''
    def main_jobs(self, runner):
        pass

    '''
    process the main jobs
    '''
    def process_main(self):
        pass

    '''
    add commands to the job runner's queue
    '''
    def queue_commands(self, runner, nevs, njobs):
        pass
