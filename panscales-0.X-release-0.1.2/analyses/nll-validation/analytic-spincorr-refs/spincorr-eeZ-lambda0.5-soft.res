# expected results for all-order soft spin correlations tests
#
# This is obtained by running the spin-correlation code for the PanGlobal shower
#
# columns: observable a2/a0_ratio uncertainty
dpsi      -0.0278137  0.0000292
dpsi_gg    0.0482333  0.0000314
dpsi_qq   -0.5864582  0.0000909
dpsi_rest  0.0        0.0
