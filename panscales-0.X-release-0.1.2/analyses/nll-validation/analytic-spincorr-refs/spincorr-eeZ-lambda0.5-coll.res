# expected results for all-order collinear spin correlations tests
#
# This is obtained by running the spin-correlation code for the PanGlobal shower
# and the logbook analyses/spin-correlation-toyshower/resummation-Moult-2011.02492.nb
# for the EEEC observables. These include a conservative uncertainty but are in principle "exact" 
#
# columns: observable value uncertainty
EEEC               -0.0077443   0.0000001   
EEEC_gg             0.0259769   0.0000001
EEEC_qq            -0.3636721   0.0000001
EEEC_rest           0.0         0.0
dpsi_opposite       0.0         0.0
dpsi_opposite_gggg  0.0         0.0
dpsi_opposite_ggqq  0.0         0.0
dpsi_opposite_qqqq  0.0         0.0
dpsi_opposite_rest  0.0         0.0
dpsi_sameside      -0.0251314   0.0000270
dpsi_sameside_gg    0.0502024   0.0000312
dpsi_sameside_qq   -0.6118409   0.0000905
dpsi_sameside_rest  0.0         0.0
