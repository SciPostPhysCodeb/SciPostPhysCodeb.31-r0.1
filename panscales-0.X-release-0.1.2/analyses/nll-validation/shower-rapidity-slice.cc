/// code to compute the scalar pt in a fixed-width rapidity slice
/// example of command line to run the code is
///
/* 
      ./build-doubleexp/shower-rapidity-slice  -process pp2H -mH 1.0 -pdf-lnQref 0.0 -Q 1.0 -rts 5.0 \ 
                    -use-diffs -shower panglobal -beta 0.0  -spin-corr off -nloops 1 \ 
                    -slice-maxrap 1.0 -lambda-obs-min -0.5 -lnkt-cutoff -5005000000 -alphas 1e-09 -colour CATwoCF \ 
                    -half-central-rap-window 13.0 -ln-obs-margin -15.0  -strategy CentralRap  \ 
                    -output-interval 1000 -rseq 1 -nev 25000 -out a
*/
/// computing the transverse pt in a slice of |eta| = 1
/// it vetoes emissions that are too collinear, and would therefore 
/// not contribute to the observable, set by the CentralRap strategy
/// As this is a single-logarithmic observable, we may then directly
/// run with an infinitesimal value of alphas to obtain the SL shower prediction

#include "AnalysisFramework.hh"
#include "ShowerRunner.hh"
#include "fjcore_local.hh"
#include "NonGlobalObsVeto.hh"
#include <sstream>
#include <cmath>

using namespace std;
using namespace fjcore;
using namespace panscales;


class RapiditySlice : public AnalysisFramework {
public:
  double _alphasQ, _beta_ps;
  double _lambda_obs_min, _lnQ;
  precision_type _Q;

  double _slice_maxrap;     ///< extent of the tested rapidity slide
  double _maxrap_margin;    ///< controls the rapidity-window allowed in the shower
  double _ln_obs_margin;    ///< additional seccurity in lnvmin at the observable boundary
  
  RapiditySlice(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in) {
    // get info from the command line
    // Note: _ln_obs_margin can be set to 0 is we run w a tiny alphas
    _lambda_obs_min = cmdline->value("-lambda-obs-min",std::numeric_limits<double>::max())
      .help("\n\t minimal value of ln(v_obs) that we want to reach");
    _slice_maxrap  = cmdline->value("-slice-maxrap",  1.0)
      .help("\n\t half-size of the rapidity slice we are probing");
    //  .help("\n\t additional margin in rapidity where we allow shower emissions");
    _maxrap_margin = cmdline->value("-half-central-rap-window",10.0) - _slice_maxrap;
    _ln_obs_margin = cmdline->value("-ln-obs-margin", log(numeric_limit_extras<double>::sqrt_epsilon()))
      .help("\n\t additional margin, in log(pt_slice), beyond lambda_lin/alphas");

    assert((_ln_obs_margin<=0.0) && "ln-obs-margin should be negative");
    
    _Q   = precision_type(cmdline->value<double>("-Q"));
    _lnQ = to_double(log(_Q));

  }

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {
    // get the value of alphas at the reference scale
    _alphasQ = f_shower_runner->shower()->qcd().alphasMSbar(_lnQ);
    _beta_ps = f_shower_runner->shower()->beta();

    //
    //   |\            : 
    //   | \           : 
    //   |  \          : 
    //   |  |\         : 
    //   |  | \        : 
    //   |  |  \       : 
    //   |  | / \      : 
    //   |  |/   \     : 
    //   |--|-----\    : this is lambda/aS + margin  [lnktcut-off should be below this
    //   | /|      \   : 
    //   |/ |       \  : 
    //   +             : this should be lnvmin
    //      +          : rapidity slice limit
    f_lnvmin = (_lambda_obs_min/_alphasQ+_ln_obs_margin) + (1+_beta_ps) * _slice_maxrap;
    
    if (f_shower_runner->shower()->qcd().lnkt_cutoff() > (_lambda_obs_min/_alphasQ+_ln_obs_margin)){
      header << "# WARNING: alphas is cutoff at a kt scale above the low end of the buffer region: "
             << f_shower_runner->shower()->qcd().lnkt_cutoff() << " is larger than "
             << (_lambda_obs_min/_alphasQ+_ln_obs_margin) << endl;
      cerr << "# WARNING: alphas is cutoff at a kt scale above the low end of the buffer region: "
           << f_shower_runner->shower()->qcd().lnkt_cutoff() << " is larger than "
           << (_lambda_obs_min/_alphasQ+_ln_obs_margin) << endl;
    }

    // unless explicitly specified, start the evolution at the Q scale
    // (may yiel a mismatch for beta_ps!=beta_obs observables
    if(!cmdline->present("-lnvmax")) f_lnvmax = _lnQ;

    // only generate emissions at central rapidity (rapidity slice + margin)
    f_shower_runner->half_central_rap_window(_slice_maxrap + _maxrap_margin);

    //--------------------------------------------------
    // observable histograms
    const double dasL  =   0.01;
    const double asLmin= _lambda_obs_min;
    const double asLmax= 0.0;
    
    cumul_hists_err["slice_scalar_pt"].declare( asLmin, asLmax, dasL);
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    // Use also the histogram because it is easier to read from python
    double evwgt = event_weight();

    //--------------------------------------------------------------------
    // compiute the scalar pt in the rapidity slice (excluding the Z/H boson
    precision_type sum_pt = 0.0;

    for (unsigned int i=0; i<f_event.particles().size(); ++i){
      const Particle & p = f_event.particles()[i];
      // skip initial-state particles
      if(p.is_initial_state()) continue;
      // skip non-partons
      if(!p.is_parton()) continue;

      if (fabs(to_double(p.rap())) > _slice_maxrap) continue;
      sum_pt += p.pt();
    }
    // normalise by Q
    sum_pt /= _Q;
    cumul_hists_err["slice_scalar_pt"].add_entry(_alphasQ * safe_log(sum_pt), evwgt);
  }

  //----------------------------------------------------------------------
  // helpers
  //
  // log avoiding 0
  double safe_log(precision_type x) const{
    return x<0 ? -std::numeric_limits<double>::max() : to_double(log_T(x));
  }
};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  RapiditySlice driver(&cmdline);
  driver.run();
}
