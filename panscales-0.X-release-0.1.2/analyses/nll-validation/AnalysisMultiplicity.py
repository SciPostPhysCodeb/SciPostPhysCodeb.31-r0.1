import sys
import os
import argparse
import glob
from math import sqrt
import numpy as np
sys.path.insert(0, os.path.dirname(__file__)+"../../submodules/AnalysisTools/python")
from hfile import *
from poly_interp import *
import shutil

from utils import yes_no,print_log,stdout_from
import utils # for the variables
from AnalysisBase import *

#------------------------------------------------------------------------
# handles global observables
#------------------------------------------------------------------------
class Multiplicity(AnalysisBase):
    '''
    initialisation (no arguments)
    '''
    def __init__(self):
        # parameters adjustable from the command line
        #
        # Note that we could think of auto-adjusting the alphas values
        self.default_xi       = 5.0
        # these are the values returned by the optimisation script
        #   self.default_alphass  = [0.0000634742, 0.0011530167,
        #                            0.0035933473, 0.0050000000]
        # we've checked that rounding them to the following has a negligible impact (O(2%))
        # however, L values are still notround numbers
        #   self.default_alphass  = [0.0000625, 0.001, 0.0032, 0.005]
        # these values of alphas also have nice L (time cost ~5-6%
        # compared to optimal, so acceptable)
        self.default_alphass  = [0.00008, 0.00128, 0.003125, 0.00512]
        self.default_accuracy = 0.0075

    '''
    define specific command-line arguments
    '''
    def add_parser_arguments(self, parser):
        self.parser = parser
        group = parser.add_argument_group(f'arguments for multiplicity')

        group.add_argument('--mult-xi', type=float,
                           default= self.default_xi, 
                           help='target xi for multiplicity')
        group.add_argument('--mult-alphass', default=self.default_alphass, 
                           nargs='+', type=float,
                           help='values of alphas used for the extrapolation to 0')
        group.add_argument('--mult-accuracy', type=float,
                           default=self.default_accuracy, 
                           help='target statistical accuracy')
        group.add_argument('--mult-nev-warmup', type=int,
                           default=100000, 
                           help='number of events for the warmup run')

    def args(self):
        if not hasattr(self, 'args_'):
            self.args_ = self.parser.parse_args()
        return self.args_
        
    '''
    initialisation/warmup
    '''
    def warmup(self, runner):
        # basic info
        self.xi       = self.args().mult_xi
        self.alphass  = self.args().mult_alphass
        self.rtalphass= [sqrt(a) for a in self.alphass]
        self.Ls       = [sqrt(self.xi/a) for a in self.alphass]
        self.accuracy = self.args().mult_accuracy
        self.length   = self.args().job_length
        proc   = utils.procs  [self.args().proc  ]['cmd_args']
        colour = utils.colours[self.args().colour]['cmd_args']

        self.warmupdir= utils.outdir+'multiplicity/'
        if not os.path.isdir(self.warmupdir): os.mkdir(self.warmupdir)
        self.warmupdir= utils.outdir+'multiplicity/warmup/'
        queue_jobs = True
        if os.path.isdir(self.warmupdir): 
            if yes_no('warmup results for the multiplicity seem to be already available. Do you want to generate new ones?'):
                shutil.rmtree(self.warmupdir)
                os.mkdir(self.warmupdir)
            else:
                queue_jobs = False
        else:
            os.mkdir(self.warmupdir)

        # build a basic command wo
        #  ../build-{float_precisions[ialphas]}/
        #  -alphas <...> -beta-obs <...>
        #  -lnkt-cutoff {lnktcuts[ialphas]} 
        #  -rseq ARG_REPLACE -nev {nevs} 
        #  -out {utils.outdir}/{showertag}-fapx{beta_obs}-lambda{-target_lambda}-as{alphas}-nods-rseqARG_REPLACE
        rts = self.args().rts if ("pdf" in utils.procs[self.args().proc]['cmd_args']) else 1.0
        self.command_base = f"""shower-multiplicity \
            {utils.procs[self.args().proc]['cmd_args']} -Q 1.0 -rts {rts} \
            -spin-corr off -nloops {self.args().nloops} \
            -use-diffs -shower {utils.showers[self.args().shower]['cmd_args']} \
            {colour} {self.args().extra} -output-interval 1000 """

        # get the various cuts and precision types
        self.lnkt_cutoffs = self.Ls
        security=4
        self.lnvmins = [(1+utils.showers[self.args().shower]['beta'])*L+security for L in self.Ls]
        self.precisions = ['doubleexp' for cut in self.lnkt_cutoffs ]

        self.queue_commands(runner, self.args().mult_nev_warmup, 2, self.warmupdir, queue_jobs = queue_jobs)

    '''
    process the warmup output
    '''
    def process_warmup(self):
        
        # we want to get (i) the number of events per job (ii) the number of jobs
        # based on (a) the timing (b) the accuracy for test runs
        nevs_per_job = np.zeros(len(self.alphass))
        uncs_one_job = np.zeros(len(self.alphass))
        for ialpha,alphas in enumerate(self.alphass):
            s  = 0.0
            e2 = 0.0
            t  = 0.0
            n_seq = 0
            for iseq in range(1, 3):
                filename = f'{self.outbases[ialpha]}{iseq}.res'
                if(os.path.isfile(filename)):
                    f = open(filename, 'r')
                    # get the elapsed time
                    elapsed_str = search(f, '^# time now = .*, elapsed = .*', return_line=True)
                    t += float(elapsed_str[elapsed_str.rfind('=')+1:elapsed_str.rfind(' h')])

                    # get the multiplicity
                    mult_info = search(f, '^# <multiplicity_weighted> =', return_line=True).split(' ')
                    s  += float(mult_info[3])
                    e2 += float(mult_info[5])**2
                    #print (s/iseq,sqrt(e2)/iseq)
                    n_seq += 1
                
            # normalise s and e2 as if we were combining the 2 sequences
            s  /= (n_seq*sqrt(alphas))
            e2 /= (n_seq**2*alphas)
                        
            # number of events per job (of requested length)
            #   24 hours * n_seq runs * nev_per_run / time_all_runs(hours)
            nevs_per_job[ialpha] = 24*self.length*n_seq*self.args().mult_nev_warmup/t

            # uncertainty scaled for one job  (of requested length)
            #
            # Note that here we take the absolute uncertainty on the
            # multiplicity (normalised by1/sqrt(alphas) for NDL tests)
            uncs_one_job[ialpha] = sqrt(e2/(nevs_per_job[ialpha]/(n_seq*self.args().mult_nev_warmup)))
            
        while True:
            self.nevs_per_job = np.zeros(len(self.alphass))
            self.njobs        = np.zeros(len(self.alphass))
            njobs_tot = 0

            # now try to get an optimal distribution of events so as
            # to get the desired stat uncertainty on the extrapolation
            # using all the values of alphas

            # extra coeffs
            coefs = []
            for i,si in enumerate(self.rtalphass):
                z = 1.0
                for j,sj in enumerate(self.rtalphass):
                    if i!=j:
                        z *= sj/(sj-si)
                coefs.append(abs(z))
       
            D = sum([c*u for c,u in zip(coefs, uncs_one_job)])
            for i in range(len(self.alphass)):
                self.njobs[i]=coefs[i]*uncs_one_job[i]*D/(self.accuracy**2)
            
            # now round things up. For simplicity, we take multiples
            # of 10k events
            for ialpha,alphas in enumerate(self.alphass):
                # we first round the number of jobs
                n = int(self.njobs[ialpha])+1
                # then the number of jobs
                nev = self.njobs[ialpha]*nevs_per_job[ialpha]/n
                nev_min = 10000
                self.nevs_per_job[ialpha] = nev_min * (int(nev/nev_min)+1)
                self.njobs[ialpha] = n
                njobs_tot += n
            
            print_log ("Multiplicity job summary (after warmup):")
            print_log (f'       alphas         nevs        njobs')
            for ialpha,alphas in enumerate(self.alphass):
                print_log (f'{alphas:13.10f} {int(self.nevs_per_job[ialpha]):12} {int(self.njobs[ialpha]):12}')
            print_log(f'Total number of jobs: {njobs_tot} (for a target accuracy of {self.accuracy})')
            if yes_no('Accept these settings?'):
                break
            else:
                self.accuracy = float(input('Enter new target accuracy: '))
        
    '''
    queue the main jobs
    '''
    def main_jobs(self, runner):
        # we build the jobs from
        # - the number of events per day in self.nevs_per_job[ialpha]
        # - the number of jobs in self.njobs[ialpha]
        self.jobdir= utils.outdir+'multiplicity/parts/'
        if os.path.isdir(self.jobdir): 
            if yes_no('(main) results for the multiplicity seem to be already available. Do you want to generate new ones?'):
                shutil.rmtree(self.jobdir)
            else:
                return
        os.mkdir(self.jobdir)

        self.queue_commands(runner, self.nevs_per_job, self.njobs, self.jobdir)

    '''
    process the main jobs
    '''
    def process_main(self):
        # create the output file
        fout = open(f'{utils.outdir}multiplicity-{self.args().shower}-xi{self.xi}-{self.args().colour}', 'w')

        # first combine the slices for individual values of alphas and beta
        for ialpha, alpha in enumerate(self.alphass):
            self.outbases[ialpha] = self.outbases[ialpha].replace('warmup', 'parts')
                                    
        # now process each value of alphas independently
        stots = np.zeros(len(self.alphass))
        etots = np.zeros(len(self.alphass))
        anas  = np.ones (len(self.alphass))

        # h1 and h2 for the analytics
        Nstr = stdout_from(f'./build-double/analytic-multiplicity -{self.args().proc} -{self.args().colour} -rts {self.args().rts} -Q 1.0 -yQ 0.0 -xi {self.xi} | tail -n1').split()
        h1 = float(Nstr[0])
        h2 = float(Nstr[1])
        for ialpha,alphas in enumerate(self.alphass):
            tot_jobs = 0
            for iseq in range(1, int(self.njobs[ialpha])+1):
                
                filename = f'{self.outbases[ialpha]}{iseq}.res'
                if(os.path.isfile(filename)):
                    f = open(filename, 'r')
                        
                    # get the multiplicity
                    mult_info = search(f, '^# <multiplicity_weighted> =', return_line=True).split(' ')
                    s  = float(mult_info[3])
                    e2 = float(mult_info[5])**2

                    stots[ialpha] += s
                    etots[ialpha] += e2
                    tot_jobs += 1
                else:
                    print(f'{filename} not found')

            # get the final averages
            stots[ialpha] = stots[ialpha]/tot_jobs
            etots[ialpha] = np.sqrt(etots[ialpha])/tot_jobs

            # get the analytic expectations for all observables
            # Note that this would only work for pp ar the moment
            anas[ialpha] = h1 + sqrt(alphas) * h2
                    
        # print the averages
        print(f'# mult-v-alphas', file=fout)
        print (f'# columns: alphas value uncertainty analytic shower-analytics uncertainty (shower-analytics)/rtalphas uncertainty/rtalphas', file=fout)
        for ialpha, alpha in enumerate(self.alphass):
            print(f'{alpha} {stots[ialpha]} {etots[ialpha]} {anas[ialpha]} {stots[ialpha]-anas[ialpha]} {etots[ialpha]} {(stots[ialpha]-anas[ialpha])/sqrt(alpha)} {etots[ialpha]/sqrt(alpha)}', file=fout)
        print(f'', file=fout)
        print(f'', file=fout)            


        # do the extrapolations and store them
        if len(self.alphass)==4:
            extraps=[[0,1,2],[0,1,3],[0,2,3],[1,2,3],[0,1,2,3]]
            extrap_iref = 1 + 2 + 4
            extrap_ialt = 1 + 4 + 8
        elif len(self.alphass)==3:
            extraps=[[0,1],[0,2],[1,2],[0,1,2]]
            extrap_iref = 1 + 2 + 4
            extrap_ialt = 1 + 2
        else:
            extraps=[[0,1]]
            extrap_iref = 1 + 2
            extrap_ialt = 1 + 2  # -> extrap_syst=0

        # v=value, e=uncertainty, a=analytics, rta=sqrt(alphas)
        val_dl  = lambda v, e, a, rta : v-a
        err_dl  = lambda v, e, a, rta : e
        desc_dl = "N_PS-N_NDL"
        val_ndl = lambda v, e, a, rta : (v-a)/rta
        err_ndl = lambda v, e, a, rta : e/rta
        desc_ndl= "(N_PS-N_NDL)/sqrt(alphas)"

        for order, fval, ferr, desc in zip(['DL',   'NDL'],
                                           [val_dl, val_ndl],
                                           [err_dl, err_ndl],
                                           [desc_dl, desc_ndl]):
            # do the extrapolations
            extrap_ref = None
            extrap_err = None
            extrap_alt = None

            # discard things if any of the errors are nan
            if np.isnan(etots).any():
                print (f'# mult-{order}-extrapolations', file=fout)
                print ( '# the barcode is the sum_{i\in indices} 2^i', file=fout) 
                print (f'# columns: barcode  extrapolation uncertainty deviation(sigma)', file=fout)
                print ( '# discarded because some uncertainties were found to be NaN', file=fout)
                extrap_ref = None
                extrap_err = None
                extrap_alt = None
                continue
            print (f'# mult-{order}-extrapolations', file=fout)
            print ( '# the barcode is the sum_{i\in indices} 2^i', file=fout) 
            print (f'# columns: barcode  extrapolation uncertainty deviation(sigma)', file=fout)
            ys  = fval(stots, etots, anas, np.asarray(self.rtalphass))
            dys = ferr(stots, etots, anas, np.asarray(self.rtalphass))

            for extrap_indices in extraps:
                extrap_barcode = 0
                x  = np.zeros(len(extrap_indices))
                y  = np.zeros(len(extrap_indices))
                dy = np.zeros(len(extrap_indices))
                for i, index in enumerate(extrap_indices):
                    extrap_barcode += 2**index
                    x[i]  = self.rtalphass[index]
                    y[i]  = ys[index]
                    dy[i] = dys[index]
                extrap,dextrap = get_polynomial_coefficients(x, y, dy)
                print (extrap_barcode, extrap[0], dextrap[0], (extrap[0]-1)/dextrap[0], file=fout)
                if extrap_barcode == extrap_iref:
                    extrap_ref = extrap[0]
                    extrap_err = dextrap[0]
                elif extrap_barcode == extrap_ialt:
                    extrap_alt = extrap[0]
            print ('\n', file=fout)
                    
            # produce the final summary
            print_log( '#-----------------------------------------------------------------', extra_file=fout)
            print_log(f'# {order} multiplicity summary [{desc}]', extra_file=fout)
            print_log( '#-----------------------------------------------------------------', extra_file=fout)
            if extrap_ref is None:
                print_log(f'multiplicity   unavailable', extra_file=fout)
            else:
                val  = extrap_ref
                stat = extrap_err
                syst = abs(extrap_alt-val)
                err  = np.sqrt(stat**2+syst**2)
                if (abs(val) < 2*err):
                    status = '\033[0;32mOK    \033[0m'
                elif (abs(val) < 4*err):
                    status = '\033[0;33mRE-RUN\033[0m'
                else: 
                    status = '\033[0;31mNot OK\033[0m'
#                        status = 'OK    ' if (abs(val) < 2*err) else 'Not OK'
                sigmas = abs(val)/err
                print_log(f'multiplicity                 {status} ({val*100:+.4f} +- {stat*100:.4f} +- {syst*100:.4f})% [{sigmas:5.2f} σ]', extra_file=fout)
            print_log('\n', extra_file=fout)
                    

            

    '''
    add commands to the job runner's queue
    '''
    def queue_commands(self, runner, nevs, njobs, outdir, queue_jobs=True):
        test = isinstance(nevs,  int)
        self.outbases = []
        for ialphas, alphas in enumerate(self.alphass):

            nev = nevs  if test else nevs[ialphas]
            nj  = njobs if test else int(njobs[ialphas])
            runtime=0.3 if test else 1.2*self.length

            out = f'{outdir}{self.args().shower}-xi{self.xi}-as{alphas}-{self.args().colour}-rseq'
            self.outbases.append(out)
            if queue_jobs:
                command=f'build-{self.precisions[ialphas]}/{self.command_base} -alphas {alphas} -lnvmax 0.0 -lnvmin -{self.lnvmins[ialphas]} -lnkt-cutoff -{self.lnkt_cutoffs[ialphas]} -rseq ARG_REPLACE -nev {nev} -out {out}ARG_REPLACE.res'
                runner.run(command, groupname=self.args().name+'-mult',
                           range=f'1-{nj}',days=runtime)
