#ifndef __PANSCALES_EMISSIONVETOSOFTSPIN_HH__
#define __PANSCALES_EMISSIONVETOSOFTSPIN_HH__

#include <string>
#include <vector>
#include <sstream>
#include "Event.hh"

namespace panscales{

/// Class to veto emissions so as to enable us to reliably
/// explore collinear spin correlations of soft emission
/// within a fixed slice (defined wrt the z axis)
///
/// It will work as follows, we want to achieve the following
///
/// - by default we do not accept a splitting if its angle
///   is smaller than some theta_min
///
/// - we make an exception if the emission is in the slice and 
///   its energy is above exp(lnErel_min)*Emax_in_slice. 
///
/// Put another way, we accept the emission if
///
///    (angle > theta_min) || (emission_in_slice && pt > exp(lnErel_min)*Emax_in_slice)
///
/// In practice the code uses split_eta_max rather than theta_min and we
/// actually check that the parent is in the slice rather than the emission
/// (because we can calculate the parent direction accurately and the
/// relevant region for this check is for collinear splitting, which 
/// means that parent or emission being in the slice should behave equivalently)
///
template<class Shower>
class EmissionVetoSoftSpin : public EmissionVeto<Shower> {

public:
  /// constructor that specifies the minimum log(z) that
  /// will be accepted for emissions, except in the region
  /// 
  EmissionVetoSoftSpin(double split_eta_max, double abs_rap_slice, double lnErel_min) : 
         _split_eta_max(split_eta_max), _abs_rap_slice(abs_rap_slice), _lnErel_min(lnErel_min) {
    assert(split_eta_max > 0);
    assert(abs_rap_slice > 0);
    assert(lnErel_min < 0);
  }

  virtual std::string description() const override {
    std::ostringstream ostr;
    ostr << "EmissionVetoSoftSpin with" 
         << " split_eta_max = " << _split_eta_max
         << ", abs_rap_slice = " << _abs_rap_slice
         << ", lnErel_min = " << _lnErel_min;
    return ostr.str();
  }


  virtual void initialise(Event & event, double lnv) override {
    _lnEmax_slice = -numeric_limits<double>::max();
  }

  virtual WeightedAction pre_accept_prob_veto(const Event & event,
                                              const typename Shower::EmissionInfo &emission_info,
                                              Weight& weight, 
                                              bool is_global = false) override {

    // the back of the event is the last emission that was created.
    // If it is in the slice, then we check its energy to see if we need
    // to update our _lnEmax_slice reference scale.
    const auto & particles = event.particles();
    //std::cout << particles.size()-1 << " " << particles.back() << std::endl;
    if (particles.size() > 2 && std::fabs(particles.back().rap()) < _abs_rap_slice){
      double lnEmax = to_double(log(particles.back().E()));
      if (lnEmax > _lnEmax_slice) _lnEmax_slice = lnEmax;
      //std::cout << lnEmax << " " << _lnEmax_slice << std::endl;
    }

    // if the angle is large enough, we always accept this
    const auto &element = *(emission_info.element());
    double lnv = emission_info.lnv;
    double lnb = emission_info.lnb;
    double eta_approx = element.eta_approx(lnv,lnb);
    double abs_eta_approx = std::fabs(eta_approx);
    //std::cout << "abs_eta_approx = " << abs_eta_approx << endl;
    if (abs_eta_approx < _split_eta_max) return EmissionAction::accept;

    // otherwise we have to figure out if the particle doing the
    // emitting is in the slice (only if it is in the slice
    // are we going to be more relaxed about vetoes than normal)
    bool in_slice = 
              (eta_approx > 0 && std::fabs(element.emitter  ().rap()) < _abs_rap_slice)
           || (eta_approx < 0 && std::fabs(element.spectator().rap()) < _abs_rap_slice);
    if (!in_slice) return EmissionAction::veto_emission;

    // next we compare the expected energy of the emission to the threshold
    // and veto if the energy is too low
    double lnE = element.lnkt_approx(lnv, lnb) + abs_eta_approx;
    return (lnE < _lnEmax_slice + _lnErel_min) ? EmissionAction::veto_emission : EmissionAction::accept;
  }


private:
  /// parameters that set the functionality
  double _split_eta_max;
  double _abs_rap_slice;
  double _lnErel_min;

  /// internal parameter to track the state of the event
  double _lnEmax_slice;
};

} // namespace panscales

#endif // __PANSCALES_EMISSIONVETOSOFTSPIN_HH__
