#ifndef __FLAVINFO_HH__
#define __FLAVINFO_HH__

#include "fjcore.hh"
//#include "fastjet/LimitedWarning.hh"
//#include "fastjet/ClusterSequence.hh"
//#include "fastjet/SharedPtr.hh"

using namespace std;

FJCORE_BEGIN_NAMESPACE      // defined in fastjet/internal/base.hh

//----------------------------------------------------------------------
/// class to allow representation of flavour, including concepts
/// such as the fact that a particle is an incoming "beam", or
/// should be a "spectator" during the clustering.
///
/// The class also provides a facility to interpret PDG codes
/// and deduce the corresponding flavour content of the particle.
class FlavInfo : public PseudoJet::UserInfoBase {

public:
  /// constructs a flavour info from a pdg code. This can handle most
  /// standard model codes without difficulty. A zero code produces a flavourless object.
  ///
  /// The flags argument is optional, and can be set either to
  /// FlavInfo::beam or FlavInfo::spectator
  ///
  FlavInfo (int pdg_code = 0, int flags = 0);
  /// constructs a flavour info object from the individual flavours
  FlavInfo (int n_d, int n_u, int n_s, int n_c, int n_b, int n_t, int flags = 0);

  /// returns the net number of quarks of flavour iflv
  /// (iflv runs from 1=d to 6=top).
  int operator[](int iflv) const {return _flav_content[iflv];}
  /// sets the number of objects of flavour iflv (1=d to 6=top) to be n
  void set_flav(int iflv, int n) {_flav_content[iflv] = n; update_flavourless_attribute();}

  /// allows comparison of flavours
  bool operator==(const FlavInfo &) const;
  bool operator!=(const FlavInfo &) const;

  /// resets all flavours to zero except iflv; this should be called,
  /// for example, when considering b-flavour at hadron level, so that
  /// the algorithm doesn't get confused by the many other quark
  /// flavours that are around (and also because, experimentally,
  /// those other flavours may not be well known).
  void reset_all_but_flav(int iflv);

  /// returns the pdg_code, or 0 if it's unknown (e.g. due to result
  /// of recombination)
  int pdg_code() const {return _pdg_code;}

  /// label this particle as being an incoming beam particle
  void label_as_beam() {_flav_content[0] |= beam;}
  /// returns true if this particle is a beam particle.
  bool is_beam() const {return (_flav_content[0] & beam);}

  /// label this object as being a "spectator", such as a W, which is
  /// relevant for calculating the beam distance in flavour clusterings
  /// but does itself take part in the clustering
  void label_as_spectator() {_flav_content[0] |= spectator;}
  /// returns true if this particle is a spectator.
  bool is_spectator() const {return (_flav_content[0] & spectator);}

  /// returns true if the object has no net flavour
  bool is_flavourless() const {return (_flav_content[0] & _is_flavourless);}
  bool is_flavorless() const {return is_flavourless();}

  /// returns true if the object has more than one unit of flavour
  bool is_multiflavoured() const;
  bool is_multiflavored() const {return is_multiflavoured();}

  bool has_opposite_flavour(const PseudoJet & particle) const;

  /// allows addition of flavour: note that beam, spectator and PDG status are lost
  FlavInfo operator+(const FlavInfo &) const;
  /// allows subtraction of flavour: note that beam, spectator and PDG status are lost
  FlavInfo operator-(const FlavInfo &) const;

  /// returns a string such as "u d", "cbar", etc.; "g" means gluon or anything
  /// else with no flavour
  std::string description() const;

  /// returns the flavour of a particle if that particle has flavour, otherwise
  /// just the default flavour
  static const FlavInfo & flavour_of(const PseudoJet & particle) {
    if (particle.has_user_info<FlavInfo>()) return particle.user_info<FlavInfo>();
    else                                    return _no_flav;
  }

  /// value of flag to indicate that the particle is an incoming beam particle
  static const int beam = 2;
  /// value of flag to indicate that the particle is a "spectator",
  /// such as a W, which is relevant for calculating the beam distance
  /// in flavour clusterings but does itself take part in the
  /// clustering
  static const int spectator = 4;
  int _flav_content[7];
  void update_flavourless_attribute();
  static const int _is_flavourless = 1;
private:
  int _pdg_code;
  static const FlavInfo _no_flav;

};

FJCORE_END_NAMESPACE        // defined in fastjet/internal/base.hh
#endif // __FLAVINFO_HH__
