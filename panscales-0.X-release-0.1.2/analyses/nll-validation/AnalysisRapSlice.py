import sys
import os
import argparse
import glob
from math import sqrt
import numpy as np
sys.path.append("../../submodules/AnalysisTools/python")
from hfile import *
from poly_interp import *
import shutil

from utils import yes_no,print_log,stdout_from
import utils # for the variables
from AnalysisBase import *

#------------------------------------------------------------------------
# handles global observables
#------------------------------------------------------------------------
class RapSlice(AnalysisBase):
    '''
    initialisation (no arguments)
    '''
    def __init__(self):
        # parameters adjustable from the command line
        #
        # Note: from the pp runs, we had run 80 1M event runs
        # each run last ~8k seconds (2.25 hours), alphas=1e-9, L=5e8 (+epsilon), buffer of 13)
        # This gives an accuracy around 2e-4.
        #
        # Trying to scale this:
        #  - do 1-day runs would do 10M events
        #  - we'd just need 8 jobs to get to an accuracy of 2e-4
        #  - 1 job swould get you ~6e-4
        self.default_rapmax   =  1.0
        self.default_lambda   = -0.5
        self.default_alphas   =  1.0e-9
        self.default_accuracy =  0.001
        
        self.L_buffer_abs_min   = -15.0   # these should be irrelevant at our accuracy!
        self.L_buffer_rel_min   =   0.001 # these should be irrelevant at our accuracy!
        self.rap_buffer         = 12.0    # not too high because that will let the multiplicity explode

    '''
    define specific command-line arguments
    '''
    def add_parser_arguments(self, parser):
        self.parser = parser
        group = parser.add_argument_group(f'arguments for the rapidity slice (Note:taking CA=2CF)')

        group.add_argument('--slice-rapmax', type=float, default= self.default_rapmax, 
                           help='(half-)size of the rapidity slice')
        
        group.add_argument('--slice-lambda', type=float, default= self.default_lambda, 
                           help='target lambda=as L for multiplicity')

        group.add_argument('--slice-alphas', type=float, default=self.default_alphas, 
                           help='value of alphas used (no extrapolation)')
        
        group.add_argument('--slice-accuracy', type=float, default=self.default_accuracy, 
                           help='target statistical accuracy')
        group.add_argument('--slice-nev-warmup', type=int, default=25000, 
                           help='number of events for the warmup run')

    def args(self):
        if not hasattr(self, 'args_'):
            self.args_ = self.parser.parse_args()
        return self.args_
        
    '''
    initialisation/warmup
    '''
    def warmup(self, runner):
        # basic info
        self.rapmax   = self.args().slice_rapmax
        self.lmbda    = self.args().slice_lambda
        self.alphas   = self.args().slice_alphas
        self.accuracy = self.args().slice_accuracy
        self.length   = self.args().job_length

        # currently, the analytics is only available for |y|<1 slices. Make sure this is the case
        if self.rapmax<0.999999 or self.rapmax>1.000001:
            print_log(f"Analytic reference results only available for |y|<1 and {self.rapmax} was requested. Giving up.")
            sys.exit(1)

        Lbase = self.lmbda/self.alphas
        self.L = min(Lbase * (1+self.L_buffer_rel_min),
                     Lbase + self.L_buffer_abs_min)
        
        proc   = utils.procs  [self.args().proc  ]['cmd_args']

        self.warmupdir= utils.outdir+'rapidity-slice/'
        if not os.path.isdir(self.warmupdir): os.mkdir(self.warmupdir)
        self.warmupdir= utils.outdir+'rapidity-slice/warmup/'
        queue_jobs = True
        if os.path.isdir(self.warmupdir): 
            if yes_no('warmup results for the rapidity slice seem to be already available. Do you want to generate new ones?'):
                shutil.rmtree(self.warmupdir)
                os.mkdir(self.warmupdir)
            else:
                queue_jobs = False
        else:
            os.mkdir(self.warmupdir)

        # build a basic command wo
        #  ../build-{float_precisions[ialphas]}/
        #  -rseq ARG_REPLACE -nev {nevs} 
        #  -out ...
        # rts = self.args().rts if self.args().proc.startswith('pp') else 1.0
        rts = self.args().rts if ("pdf" in utils.procs[self.args().proc]['cmd_args']) else 1.0
        gen_rapmax = self.rapmax + self.rap_buffer

        self.command_base = f"""build-doubleexp/shower-rapidity-slice \
            -use-diffs -shower {utils.showers[self.args().shower]['cmd_args']} \
            {utils.procs[self.args().proc]['cmd_args']} -Q 1.0 -rts {rts} \
            -spin-corr off -nloops 1 \
            -slice-maxrap {self.rapmax} -lambda-obs-min {self.lmbda} \
            -half-central-rap-window {gen_rapmax} -ln-obs-margin {self.L_buffer_abs_min} \
            -lnkt-cutoff {self.L} -alphas {self.alphas} -strategy CentralRap \
            -colour CATwoCF {self.args().extra} -output-interval 1000 """

        self.queue_commands(runner, self.args().slice_nev_warmup, 2, self.warmupdir, queue_jobs = queue_jobs, warmup=True)

    '''
    process the warmup output
    '''
    def process_warmup(self):
        
        # we want to get (i) the number of events per job (ii) the number of jobs
        # based on (a) the timing (b) the accuracy for test runs
        nev_per_job = 0.0
        unc_one_job = 0.0

        s  = 0.0
        e2 = 0.0
        t  = 0.0
        for iseq in range(1, 3):
            f = open(f'{self.outbase}{iseq}.res', 'r')
            
            # get the elapsed time
            elapsed_str = search(f, '^# time now = .*, elapsed = .*', return_line=True)
            t += float(elapsed_str[elapsed_str.rfind('=')+1:elapsed_str.rfind(' h')])

            # get the cumulative distrib for the rapidity slice
            a = get_array(f, f'cumul_hist:slice_scalar_pt')
            # we need the 2nd line
            s  += a[1,1]
            e2 += a[1,2]**2
                
        # normalise s and e2 as if we were combining the 2 sequences
        s  /= 2.0
        e2 /= 4.0
                        
        # number of events per job (of requested length)
        #   24 hours * 2 runs * nev_per_run / time_all_runs(hours)
        nev_per_job = 24*self.length*2*self.args().slice_nev_warmup/t

        # uncertainty scaled for one job  (of requested length)
        #
        # Note that here we take the absolute uncertainty on the
        # multiplicity (normalised by1/sqrt(alphas) for NDL tests)
        unc_one_job = sqrt(e2/(nev_per_job/(2*self.args().slice_nev_warmup)))

        while True:
            # round the number of events per job
            nref = 1000
            self.nev_per_job = nref*max(int(nev_per_job/nref), 1)

            # update the uncertainty
            self.unc_one_job = unc_one_job * (nev_per_job/self.nev_per_job)**2
            
            # for the number of jobns, the scaling is easy:
            self.njobs = int((self.unc_one_job/self.accuracy)**2+1)
            
            print_log ("RapSlice job summary (after warmup):")
            print_log (f'                      nevs        njobs')
            print_log (f'              {int(self.nev_per_job):12} {int(self.njobs):12}')
            print_log(f'target accuracy of {self.accuracy}')
            if yes_no('Accept these settings?'):
                break
            else:
                self.accuracy = float(input('Enter new target accuracy: '))
        
    '''
    queue the main jobs
    '''
    def main_jobs(self, runner):
        # we build the jobs from
        # - the number of events per day in self.nevs_per_job[ialpha]
        # - the number of jobs in self.njobs[ialpha]
        self.jobdir= utils.outdir+'rapidity-slice/parts/'
        if os.path.isdir(self.jobdir): 
            if yes_no('(main) results for the rapidity slice seem to be already available. Do you want to generate new ones?'):
                shutil.rmtree(self.jobdir)
            else:
                return
        os.mkdir(self.jobdir)

        self.queue_commands(runner, self.nev_per_job, self.njobs, self.jobdir)

    '''
    process the main jobs
    '''
    def process_main(self):
        # create the output file
        fout = open(f'{utils.outdir}rapidity_slice-{self.args().shower}-lambda{self.lmbda}-CA2CF', 'w')

        # first combine the slices for individual values of alphas and beta
        self.outbase = self.outbase.replace('warmup', 'parts')

        # get the analytic result
        a = get_array("analytic-slice-refs/slice-deta_2-CA2CF-tmax_0.12-amin_to_0.res", "S_lambda")
        iref = 0
        nref = a.shape[0]
        while iref<nref and a[iref,0]<self.lmbda-0.00001:
            iref += 1
        if a[iref,0]>self.lmbda+0.00001:
            print_log(f"Rapidity slice: the target lambda {self.lmbda} has not been found in the analytic reference file")
            sys.exit(2)
        ana  = a[iref,1]
        dana = a[iref,2]

        if 'H' in self.args().proc:
            dana = 2*dana*ana
            ana  = ana**2
            

        # get the shower results
        stot = 0.0
        etot = 0.0
        for iseq in range(1, int(self.njobs)+1):
            f = open(f'{self.outbase}{iseq}.res', 'r')

            # get the cumulative distrib for the rapidity slice
            a = get_array(f, f'cumul_hist:slice_scalar_pt')
            # we need the 2nd line
            s  = a[0,1]
            e2 = a[0,2]**2

            stot += s
            etot += e2

        # get the final averages
        stot = stot/self.njobs
        etot = np.sqrt(etot)/self.njobs

        # final quality measure
        ratio = stot/ana
        ratio_err_shw = ratio*(etot/stot)
        ratio_err_ana = ratio*(dana/ana)
        ratio_err = ratio*sqrt((dana/ana)**2 + (etot/stot)**2)

        # print the averages
        print(f'# rapidity-slice', file=fout)
        print (f'# columns: value uncertainty analytic uncertainty shower/analytics uncertainty', file=fout)
        print(f'{stot} {etot} {ana} {dana} {ratio} {ratio_err}', file=fout)
        print(f'', file=fout)
        print(f'', file=fout)            

        # produce the final summary
        print_log( '#-----------------------------------------------------------------', extra_file=fout)
        print_log(f'# NLL(SL) rapidity slice summary [Sigma_PS/Sigma_SL-1]', extra_file=fout)
        print_log( '#-----------------------------------------------------------------', extra_file=fout)
        val  = ratio-1
        if (abs(val) < 2*ratio_err):
            status = '\033[0;32mOK    \033[0m'
        elif (abs(val) < 4*ratio_err):
            status = '\033[0;33mRE-RUN\033[0m'
        else: 
            status = '\033[0;31mNot OK\033[0m'
#        status = 'OK    ' if (abs(val) < 2*ratio_err) else 'Not OK'
        sigmas = abs(val)/ratio_err
        print_log(f'rapidity-slice               {status} ({val*100:+.4f} +- {ratio_err_shw*100:.4f} +- {ratio_err_ana*100:.4f})% [{sigmas:5.2f} σ]', extra_file=fout)
        print_log('\n', extra_file=fout)

            

    '''
    add commands to the job runner's queue
    '''
    def queue_commands(self, runner, nev, njobs, outdir, queue_jobs=True, warmup=False): 
        runtime=0.3 if warmup else 1.2*self.length

        out = f'{outdir}{self.args().shower}-lambda{self.lmbda}-CA2CF-rseq'
        self.outbase = out
        if queue_jobs:
            command=f'{self.command_base} -rseq ARG_REPLACE -nev {nev} -out {out}ARG_REPLACE.res'
            runner.run(command, groupname=self.args().name+'-slice',
                       range=f'1-{njobs}',days=runtime)
