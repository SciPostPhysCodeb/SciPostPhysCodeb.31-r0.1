import subprocess
import time
import os

# collision details
procs = {'eeZ'  : {'cmd_args' : ''},
         'eeH'  : {'cmd_args' : '-process ee2gg'},
         'ppZ'  : {'cmd_args' : '-process pp2Z -mZ 1.0 -pdf-lnQref 0.0'},
         'ppH'  : {'cmd_args' : '-process pp2H -mH 1.0 -pdf-lnQref 0.0'},
         'disQ' : {'cmd_args' : '-process DIS  -Q2 1.0 -pdf-lnQref 0.0'}}
default_proc='eeZ'

# further defaults for pp
default_rts  = 5.0   # for pp and dis (energy of proton in that case)
default_yQ   = 0.0   # for pp

#default_common_args='-Q 1.0 -spin-corr off -nloops 2'

colours = {'CATwoCF'  : {'cmd_args'  : '-colour CATwoCF',
                         'mult_args' : ''},
           'CFHalfCA' : {'cmd_args'  : '-colour CFHalfCA',
                         'mult_args' : '-CF 1.5 -CA 3.0'},
           'Segment'  : {'cmd_args'  : '-colour Segment',
                         'mult_args' : '-CA 3.0' },
           'NODS'     : {'cmd_args'  : '-colour NODS',
                         'mult_args' : '-CA 3.0'}}
default_colour = 'NODS'

showers = {'panglobal00'        : {'beta'     : 0.0,
                                   'cmd_args' : 'panglobal -beta 0.0'},
           'panglobal05'        : {'beta'     : 0.5,
                                   'cmd_args' : 'panglobal -beta 0.5'},
           'panglobal10'        : {'beta'     : 1.0,
                                   'cmd_args' : 'panglobal -beta 1.0'},
           'panlocal-antenna00' : {'beta'     : 0.0,
                                   'cmd_args' : 'panlocal-antenna -beta 0.0'},
           'panlocal-antenna05' : {'beta'     : 0.5,
                                   'cmd_args' : 'panlocal-antenna -beta 0.5'},
           'panlocal-antenna10' : {'beta'     : 1.0,
                                   'cmd_args' : 'panlocal-antenna -beta 1.0'},
           'panlocal00'         : {'beta'     : 0.0,
                                   'cmd_args' : 'panlocal -beta 0.0'},
           'panlocal05'         : {'beta'     : 0.5,
                                   'cmd_args' : 'panlocal -beta 0.5'},
           'panlocal10'         : {'beta'     : 1.0,
                                   'cmd_args' : 'panlocal -beta 1.0'},
           'dipole-kt'          : {'beta'     : 0.0,
                                   'cmd_args' : 'dipole-kt -beta 0.0'},
                                   }

outdir='tmp/'
flog=None
yes_no_assume_yes=False

#------------------------------------------------------------------------
# helpers
#------------------------------------------------------------------------
def create_outdir():
    global outdir
    if os.path.isdir(outdir): 
        if not yes_no(f'A directory with name {outdir} already exists. Continue?', log=False):
            print ('Tests interrupted by user. Exiting...')
            exit()
    else: 
        print (f'Creating folder "{outdir}"')
        os.mkdir(outdir)


# def set_logfile(outf):
#     global flog
#     flog = open(outf, 'a')

def yes_no(text, log=True):
    while True:
        if log: print_log(text+" (y/n) ", stdout=False)
        if yes_no_assume_yes:
            print (text+" (y/n) [assuming y]")
            answer = 'y'
        else:
            answer = input(text+" (y/n) ").lower()
        if log: print_log("Answer: "+answer, stdout=False)
        
        if any(answer == f for f in ["yes", "y"]):
            return True
        elif any(answer == f for f in ["no", "n"]):
            return False
        else:
            print_log ('Please answer yes/y or no/n')

def build_all():
    print_log(f"Building everything (double doubleexp)")
    msg = stdout_from("../../scripts/build-multi-precisions.py --builds double doubleexp -j --build-lib")
    print_log(msg, stdout=False)
    print_log("Done\n")

# runner is a JobRunner instance
# message will be printed
# check_period is the interval (in seconds) between probes during auto checks
def execute_queued_jobs(runner, name, message, check_period=60, assume_yes=False):
    # execute things
    njobs = len(runner.commands)
    runner.execute_commands(assume_yes = assume_yes)

    # print the message
    print_log(message)

    # the behaviour at this point depends on the job system used. For kanta and hydra,
    # one has to handle submission to a queue. Otherwise, the jobs should only return
    # control once done
    if runner.args().sem:
        stdout_from("sem --wait")
        auto_checked = True
    elif runner.args().hydra or runner.args().kanta:
        # check if one wants to wait
        auto_checked = False
        if yes_no('Do you want me to check the job status and carry on once finished?'):
            while True:
                time.sleep(check_period)
                njobs_queued, njobs_running = get_running_jobs(runner, name)
                if njobs_queued==None:
                    print_log('automatic job checks not implemented for this cluster')
                    break
                print (f'  {njobs_queued} queued including {njobs_running} running')
                if njobs_queued==0:
                    print_log('All jobs done. Carrying on with NLL tests.')
                    auto_checked = True
                    break
    else:
        auto_checked = True
            
    if not auto_checked:
        print_log('Check your jobs and let me know when they are done.')
    else:
        print_log('Jobs done.')
        
    if not yes_no('Carry on with NLL tests?'):
        print_log ('Tests interrupted by user. Exiting...')
        exit()

def get_running_jobs(runner, name):
    njobs_queued = 0
    nrunning = 0
    if runner.args().hydra:
        out = stdout_from('q -t', log=False)
        for l in  out.splitlines():
            if name in l:
                njobs_queued += 1
                if 'RUNNING' in l:
                    nrunning += 1
    elif runner.args().kanta:
        out = stdout_from('qstat -u gsoyez', log=False)
        tag = name if len(name)<15 else name[:15]
        for l in  out.splitlines():
            if tag in l:
                njobs_queued += 1
                if ' R ' in l:
                    nrunning += 1
    else: # unsupported cluster
        return None, None
    return njobs_queued,nrunning
            

def print_log(data='', log=True, stdout=True, extra_file=None, end='\n'):
    if stdout: print(data, end=end)
    if log:
        print(data, end=end, file=flog)
        if flog is not None: flog.flush()
    if extra_file is not None:
        print(data, end=end, file=extra_file)

def stdout_from(args, log=True):
    print_log("Getting the output of : "+args, stdout=False)
    result = subprocess.run(args, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if (result.returncode != 0):
        print_log(result)
        print_log("An error occurred")
        exit(-1)
    return result.stdout.decode('utf-8')
