/// program to evaluate all-order spin correlations
/// in an emission that goes into the non-global region
///
/// Indicative command lines:
///
///   ./shower-spin-correlations-soft -shower panglobal -out b -lnvmin -1000000 -alphas 0.000001  -use-diffs  -nev 100 -strategy CentralAndCollinearRap -half-central-rap-window 10 -collinear-rap-window 10 -split-eta-max 10 -lnErel_min -10 -rseq 10

#include "AnalysisFramework.hh"
#include "ShowerRunner.hh"
#include "fjcore_local.hh"
#include "FastY3.hh"
#include "LundEEGenerator.hh"
#include "FlavInfo.hh"
#include "EmissionVetoSoftSpin.hh"
#include <cmath>

using namespace std;
using namespace fjcore;
using namespace panscales;

// Declustering is ran with WTA option enabled by default
class SoftSpinAllOrderFramework : public AnalysisFramework {
public:
  FastY3 fasty3;
  precision_type abs_rap_slice;
  precision_type z2_cut;

  SoftSpinAllOrderFramework(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in) {}

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() {
    abs_rap_slice = precision_type(cmdline->value<double>("-abs-rap-slice", 1)());
    z2_cut = precision_type(cmdline->value<double>("-z2-cut", 0.1)());

    // create the veto if needed
    double split_eta_max = cmdline->value("-split-eta-max", 0.0);
    double lnErel_min    = cmdline->value("-lnErel_min", 0.0);
    if (split_eta_max > 0) {
      assert(lnErel_min < 0);
      shower_runner().set_veto(new EmissionVetoSoftSpin<ShowerBase>(split_eta_max, to_double(abs_rap_slice), lnErel_min));
    }

    // Uses WTA by default
    bool use_WTA = true;
    bool use_diffs = cmdline->present("-use-diffs");
    fasty3 = FastY3(use_WTA, use_diffs);

    int n_bins_phi = 100;
    hists_err["dpsi"].     declare(-M_PI, M_PI, 2*M_PI/n_bins_phi);
    hists_err["dpsi_qq"].  declare(-M_PI, M_PI, 2*M_PI/n_bins_phi);
    hists_err["dpsi_gg"].  declare(-M_PI, M_PI, 2*M_PI/n_bins_phi);
    hists_err["dpsi_rest"].declare(-M_PI, M_PI, 2*M_PI/n_bins_phi);

    int n_bins_lnv = 20;
    hists_err["slice_max_lnpt"].declare(f_lnvmin, 0.0, f_lnvmin/n_bins_lnv);
  }


  bool in_slice(const PseudoJet & p) const {return fabs(p.rap()) < abs_rap_slice;}

  FlavInfo flav(const PseudoJet & j) const {
    assert(j.validated_cluster_sequence()->n_particles() == f_event.size());
    FlavInfo result;
    for (const auto & p: j.constituents()) {
      result = result + FlavInfo(f_event[p.cluster_hist_index()].pdgid());
    }
    return result;
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() {
    int depth = -1;
    bool dynamic_psi_reference = true;
    fjcore::contrib::RecursiveLundEEGenerator generator(depth, dynamic_psi_reference);
    generator.do_compute_psibar(true);
    fasty3.analyse(f_event);

    auto declusterings = generator.result(fasty3.cs());
    sort(declusterings.begin(), declusterings.end(),
      [](const fjcore::contrib::LundEEDeclustering & a, const fjcore::contrib::LundEEDeclustering & b) {return a.kt() > b.kt();});

    // find the declustering that throws something into our fixed slice
    // (from a parent that was not in the slice) and that throws the
    // largest pt (relative to the z axis) into that slice
    int index_of_max_pt_in_slice = -1;
    precision_type max_pt_in_slice = 0.0;
    for (unsigned i = 0; i < declusterings.size(); i++) {
      const auto & declust = declusterings[i];
      if (!in_slice(declust.harder()) && in_slice(declust.softer())) {
        if (declust.softer().pt() > max_pt_in_slice) {
          index_of_max_pt_in_slice = i;
          max_pt_in_slice = declust.softer().pt();
        }
      }
    }
    if (index_of_max_pt_in_slice < 0) return;

    // bin the lnpt (wrt z axis) for that emission
    hists_err["slice_max_lnpt"].add_entry(to_double(log(max_pt_in_slice)), event_weight());

    // establish what we need to follow and the reference psi_1
    int iplane_to_follow = declusterings[index_of_max_pt_in_slice].leaf_iplane();
    double psi_1 = declusterings[index_of_max_pt_in_slice].psibar();

    vector<const fjcore::contrib::LundEEDeclustering *> secondaries;
    for (const auto & declust: declusterings){
      if (declust.iplane() == iplane_to_follow) secondaries.push_back(&declust);
    }

    if (secondaries.size() == 0) return;

    for (unsigned int i_secondary=0; i_secondary<secondaries.size(); i_secondary++) {
      if (secondaries[i_secondary]->z() > z2_cut) {
        double psi_2 = secondaries[i_secondary]->psibar();
        precision_type dpsi = psi_2 - psi_1;
        if (dpsi > pi)  dpsi -= 2*pi;
        if (dpsi < -pi) dpsi += 2*pi;

        hists_err["dpsi"].add_entry(to_double(dpsi), event_weight());
        xsections["dpsi_a0"] += event_weight();
        xsections["dpsi_a2"] += event_weight()*2*cos(2*to_double(dpsi));

        FlavInfo flav1 = flav(secondaries[i_secondary]->harder());
        FlavInfo flav2 = flav(secondaries[i_secondary]->softer());

        if (flav1.is_flavourless() && flav2.is_flavourless()) {
          hists_err["dpsi_gg"].add_entry(to_double(dpsi), event_weight());
          xsections["dpsi_gg_a0"] += event_weight();
          xsections["dpsi_gg_a2"] += event_weight()*2*cos(2*to_double(dpsi));
        } else if (!flav1.is_multiflavoured() && (flav1+flav2).is_flavourless()) {
          hists_err["dpsi_qq"].add_entry(to_double(dpsi), event_weight());
          xsections["dpsi_qq_a0"] += event_weight();
          xsections["dpsi_qq_a2"] += event_weight()*2*cos(2*to_double(dpsi));
        } else {
          hists_err["dpsi_rest"].add_entry(to_double(dpsi), event_weight());
          xsections["dpsi_rest_a0"] += event_weight();
          xsections["dpsi_rest_a2"] += event_weight()*2*cos(2*to_double(dpsi));
        }

        return;
      }
    }    
  }
};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  SoftSpinAllOrderFramework driver(&cmdline);
  driver.run();
}
