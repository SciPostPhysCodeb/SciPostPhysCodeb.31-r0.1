/// Analytic NDL multiplicity code
///
/// This supports both quarks and gluon processes and both ee and
/// pp(->colour singlet) processes
///
/// The code return
///   h1 h2
/// (with N_NDL = h1 + sqrt(alphas) h2) for a given value of
/// xi. Otherwisem the arguments are similar to the ones used for the
/// global obs analytics
///
/// \warning
/// currently alpha_s has 1 loop running and
/// cannot be configured to 0-loop running 

#include "AnalysisFramework.hh" // For CmdLine
#include "QCD.hh"
#include <iostream>
#include <cmath>
#include <fstream>

#ifndef HAS_HOPPET
#include "HoppetRunner.hh"
#endif

using namespace std;
using namespace panscales;


/// Container class holding all the QCD constants
class Inputs{
public:
  Inputs(CmdLine * cmdline){
    // Find out which process and flavour we're dealing with
    // whether we have | ee         | dis       | pp
    //                 | 0 PDF      | 1 PDF     | 2 PDF
    if      (cmdline->present("-eeZ")) { quark=true;  npdf=0;}
    else if (cmdline->present("-eeH")) { quark=false; npdf=0;}
    else if (cmdline->present("-ppZ")) { quark=true;  npdf=2;}
    else if (cmdline->present("-ppH")) { quark=false; npdf=2;}
    else if (cmdline->present("-disQ")){ quark=true;  npdf=1;}
    else {
      cerr << "Process not found. Please specify one of eeZ, eeH, ppZ, ppH, disQ" << endl;
      exit(1);
    }

    xi     = cmdline->value<double>("-xi",5.0).help("Specific value of asL^2 to calculate");
    xL     = sqrt(xi);
    // for pp/dis
    rts    = cmdline->value<double>("-rts",5.0).help("Center-of-mass energy");
    Q      = cmdline->value<double>("-Q",1.0).help("reference vector (com)");
    // to compute PDF fractions - take yQ
    yQ     = cmdline->value<double>("-yQ",0.0).help("Q rapidity (used to compute x1,x2)");
    yDIS   = cmdline->value<double>("-y",0.2).help("DIS y");

    
#ifndef HAS_HOPPET
    if (npdf > 0){
      cerr << "pp collisions requested but no HOPPET support detected." << endl;
      cerr << "Note: rts (" << rts << "), Q (" << Q << ") and yQ (" << yQ << ") ignored." << endl;
      cerr << "Aborting...." << endl;
      exit(1);      
    }
#endif

    // Number of loops that enter in Hoppet
    nloops = cmdline->value<int>("-nloops", 1).help("number of loops for alphas running");

    // set up the CF and CA
    ca_is_2cf    = cmdline->present("-CATwoCF")   .help("set CA to 2 CF");
    cf_is_halfca = cmdline->present("-CFHalfCA").help("set CF to CA/2");
    cmdline->present("-Segment"); //< to avoid complains from all_options_used
    cmdline->present("-NODS");    //< to avoid complains from all_options_used
    
    // build the x value(s) from the boson mass and rapidity (pp) or just xDIS
    x1 = (npdf == 1) ? pow2(Q/rts)/yDIS : Q/rts*exp( yQ);
    x2 = (npdf == 1) ? -1             : Q/rts*exp(-yQ); 
  }

  // targetted xi = as L^2, xL=sqrt(xi) (i.e. sqrt(as) |L|)
  double xi, xL; 
  
  // Process info
  bool quark;   // if true: Z->qq or qq->Z, qgamma->q (DIS), if false: H->gg or gg->H
  int  npdf;    // =0 for ee, =2 for pp, =1 for DIS

  int nloops;
  bool ca_is_2cf;
  bool cf_is_halfca;
  
  // DY constants
  double Q, rts;
  double yQ, yDIS;
  // DIS constants
  double xDIS; // (=x1)

  // PDF fractions (x2 = -1 for DIS)
  double x1;
  double x2;
};

//================================================================================
/// base class for multiplicities
class MultiplicityBase{
public:
  /// ctor
  MultiplicityBase(QCDinstance& qcd, Inputs& inputs)
    : qcd_(qcd), inputs_(inputs) {
    // Parameters defined in Eq. 2.45 in logbook/2021-03-16-Drell-Yan-multiplicity/../ee-nndl.pdf
    beta0_ = (11.0*qcd_.CA()-4.0*qcd_.nf()*qcd_.TR())/(12.0 * M_PI);
    Bgg_ = -11.0/12.0;
    Bqq_ = -3.0/4.0;
    Bgq_ = qcd_.nf()*qcd_.TR()/(3.0*qcd_.CA());
  }

  /// dummy virtual dtor
  virtual ~MultiplicityBase() {}
  
  /// DL multiplicity
  virtual double h1(double xL) const = 0;

  /// NDL correction (divided by 1/sqrt(alphas)
  virtual double h2(double xL) const = 0;

  /// derivative of the multiplicity (divided by 1/sqrt(alphas)
  ///
  /// dh1_dxL = dN_DL/dxL = 1/sqrt(as) dN_DL/dL
  virtual double dh1_dxL(double xL) const{ return 0.0;}

protected:
  QCDinstance& qcd_;
  Inputs& inputs_;
  double beta0_;
  double Bgg_;
  double Bqq_;
  double Bgq_;
};
  

//================================================================================
/// Implementation of NDL quark multiplicity
class EEZMultiplicity : public MultiplicityBase{
public:
  EEZMultiplicity(QCDinstance& qcd, Inputs& inputs):
    MultiplicityBase(qcd, inputs) {}

  /// Eq. 2.16 in logbook/2021-03-16-Drell-Yan-multiplicity/../ee-nndl.pdf 
  virtual double h1(double xL) const override{
    double arg = sqrt(2.0 * qcd_.CA() * xL * xL/ M_PI);
    // include a factor 2 for 2 hemispheres
    return 2 * (qcd_.CF()*(cosh(arg) - 1.0) / qcd_.CA() + 1);
  }

  /// \brief the NDL function h2(xL)
  ///
  /// The source analytic expression can be found in
  /// Eq. 2.44 in logbook/2021-03-16-Drell-Yan-multiplicity/../ee-nndl.pdf
  /// To get the full NDL accurate quark multiplicity one needs
  /// N_q_fullNDL(xL) = quark_multiplicity_dl(xL) + sqrt(alphas) * quark_multiplicity_ndl_over_rootalphas(xL)
  virtual double h2(double xL) const override{
    double u = sqrt(2.0 * qcd_.CA()* xL * xL / M_PI);
    double chu = cosh(u);
    double shu = sinh(u);
    double prefac = sqrt( qcd_.CA()/ (2.0 * M_PI)) * qcd_.CF() / qcd_.CA();

    // we split the NDL correction in 3 terms: one from running
    // coupling, one from flavour-diagonal hard-collinear splittings
    // and one from flavour-changing hard-collinear splittings
    double running_coupling = M_PI * beta0_ * (u * chu + (u*u-1.0) * shu) /(2.0 * qcd_.CA());
    double hard_coll_flav_diagonal = Bgg_ * u * chu + (2.0 * Bqq_ - Bgg_)*shu;
    double hard_coll_flav_changing = Bgq_ * ((2.0*qcd_.CF()-qcd_.CA()) * u * chu /qcd_.CA()
                                             + (5.0*qcd_.CA()-6.0*qcd_.CF()) * shu/qcd_.CA()
                                             + 4.0 * u * (qcd_.CF()-qcd_.CA())/qcd_.CA());

    // include a factor 2 for 2 hemispheres
    return 2*prefac*(running_coupling + hard_coll_flav_diagonal + hard_coll_flav_changing);
  }
  
  /// Returns the derivative dh1/dxL (single hemisphere)
  double dh1_dxL(double xL) const override{
    double arg = sqrt(2.0 * qcd_.CA() * xL * xL/ M_PI);
    return qcd_.CF()/qcd_.CA() * sqrt(2.0 * qcd_.CA()/M_PI)  * sinh(arg);
  }
  
};

//================================================================================
/// Implementation of NDL gluon multiplicity
class EEHMultiplicity : public MultiplicityBase{
public:
  EEHMultiplicity(QCDinstance& qcd, Inputs& inputs)
    : MultiplicityBase(qcd, inputs) {}
  
  /// Eq. 2.14 in logbook/2021-03-16-Drell-Yan-multiplicity/../ee-nndl.pdf 
  virtual double h1(double xL) const override{
    double arg = sqrt(2.0 * qcd_.CA() * xL * xL/ M_PI);
    // include a factor 2 for 2 hemispheres
    return 2*cosh(arg);
  }
  
  /// Eq. 2.43 in logbook/2021-03-16-Drell-Yan-multiplicity/../ee-nndl.pdf
  virtual double h2(double xL) const override{
    double u = sqrt(2.0 * qcd_.CA() * xL * xL / M_PI);
    double chu = cosh(u);
    double shu = sinh(u);
    double prefac = sqrt(qcd_.CA() / (2.0 * M_PI));
    
    // we split the NDL correction in 3 terms: one from running
    // coupling, one from flavour-diagonal hard-collinear splittings
    // and one from flavour-changing hard-collinear splittings
    double running_coupling = M_PI * beta0_ * (u * chu + (u*u-1.0) * shu) /(2.0 * qcd_.CA());
    double hard_coll_flav_diagonal = Bgg_ * (u * chu + shu);
    double hard_coll_flav_changing = Bgq_ * ((2.0*qcd_.CF()-qcd_.CA()) * u * chu /qcd_.CA()
                                             + (3.0*qcd_.CA()-2.0*qcd_.CF()) * shu/qcd_.CA());
    
    // include a factor 2 for 2 hemispheres
    return 2*prefac*(running_coupling + hard_coll_flav_diagonal + hard_coll_flav_changing);
  }
  
  /// derivative of the DL multiplicity wrt xL (remember xL>0) (single hemisphere)
  virtual double dh1_dxL(double xL) const override{ 
    double arg = sqrt(2.0 * qcd_.CA() * xL * xL/ M_PI);
    return sqrt(2.0 * qcd_.CA()/M_PI)  * sinh(arg);
  }
};


#ifdef HAS_HOPPET

//================================================================================
/// a quick helper for the pp->ff->X or DIS multiplicities
class PPXMultiplicity : public MultiplicityBase{
public:
  /// ctor
  PPXMultiplicity(QCDinstance& qcd, Inputs& inputs, HoppetRunner & HR):
    MultiplicityBase(qcd, inputs), HR_(HR) {
    // set up the parameters with CmdLine
    //cout << "Reading inputs:" << inputs_.x1 << endl;
    x1_  = inputs_.x1;
    x2_  = inputs_.x2;
    M_   = inputs_.Q;
    nloops_ = inputs.nloops;
  }

protected:
  double x1_, x2_, M_;
  int nloops_;

  // pdf variables
  HoppetRunner& HR_;

  /// compute the DGLAP evolution (just the convolution part) of the PDF factor
  virtual double P_otimes_F_over_F() const = 0;
};


//================================================================================
/// implementation of the Drell-Yan multiplicity
class PPZMultiplicity : public PPXMultiplicity{
public:
  PPZMultiplicity(QCDinstance& qcd, Inputs& inputs, HoppetRunner & HR):
    PPXMultiplicity(qcd, inputs, HR), Nee_(qcd, inputs) {}

  /// returns the pure DL bit of the DY multiplicity
  double h1(double xL) const override{
    // this expression includes the 2 beams and the Z in the
    // multiplicity (easier for comparison to the shower)
    return Nee_.h1(xL) + 1.0;
  }

  /// NDL function h2(xL) [multiplied by sqrt(alphas) in N_NDL
  ///
  /// In this approach we split the Drell-Yan multiplicity as
  /// N_DY = N_DL_only + sqrt(alphas(Q)) * N_NDL_only,
  /// where N_DL_only and N_NDL_only are pure funcitons of
  /// xL = alphas(Q) * |L| (for definitions see top of file).
  ///
  /// N_DL_only is given by NDY_DL() and N_NDL_only
  /// is given by the following function
  double h2(double xL) const override {
    // ee contribution
    double h2_ee = Nee_.h2(xL);

    // PDF correction
    double h2_pp_correction = h2_pdf_correction(xL);
    return h2_ee + h2_pp_correction;
  }

  /// returns the PDF-related NDL bit of the DY multiplicity
  double h2_pdf_correction(double xL) const{
    // now the "pp" correction for qq->Z
    //
    // This term is written as
    //   2/abar ng(L) dlnF/dlnQ2
    //    = pi/(as CF) nq(L) dlnF/dlnQ2
    //    = pi/(as CF) (dNq/dL) dlnF/dlnQ2
    //    = 1/(2 CF) (dNq/dL) PxF/F
    double PxF_over_F = P_otimes_F_over_F();
    double dh1 = Nee_.dh1_dxL(xL);  //< single hemisphere
    return 0.5/qcd_.CF() * PxF_over_F * dh1;
  }
  
protected:
  EEZMultiplicity Nee_;

  // the convoltuion (P x F)/F
  virtual double P_otimes_F_over_F() const override {
    PDF pdf1  = HR_(x1_, log(M_));
    PDF pdf2  = HR_(x2_, log(M_));
    PDF dpdf1 = HR_.pdf_convolution(x1_, log(M_), nloops_, qcd_.nf());
    PDF dpdf2 = HR_.pdf_convolution(x2_, log(M_), nloops_, qcd_.nf());

    // the shower currently does ddbar -> Z
    return dpdf1.d()/pdf1.d() + dpdf2.dbar()/pdf2.dbar();
  }
  
};

//================================================================================
/// implementation of the gg->H multiplicity
class PPHMultiplicity : public PPXMultiplicity{
public:
  /// ctor
  PPHMultiplicity(QCDinstance& qcd, Inputs& inputs, HoppetRunner & HR):
    PPXMultiplicity(qcd, inputs, HR), Nee_(qcd, inputs){}

  /// returns the pure DL bit of the gg2H multiplicity
  double h1(double xL) const override{
    // this includes the incopming beams and the produced H
    return Nee_.h1(xL) + 1.0;
  }

  /// returns the NDL accurate gg2H multiplicity (Eq. 2.52 in 2021-03-16 logbook)
  virtual double h2(double xL) const override{
    // Build up the NDL/root(as) as an e+e- result and a ggH correction
    double ndl_epem = Nee_.h2(xL);

    // helpers
    const double &CF = qcd_.CF();
    const double &CA = qcd_.CA();
    
    // now the "pp" correction for gg->H
    //
    // The implementation here is equivalent to Eq (2.54) of the
    // logbook (modulo reorganising the terms)
    //
    // This has 2 contributions: one that multiplies the full PDF
    // evolution (multiplied by ng) and a second that involves the
    // difference between the quark and gluon distributions
    //
    // Note: we rewrite
    //   2/abar ng dlnF/dlnQ2
    //   = 2/abar ng as/(2 pi) PxF/F
    //   = 1/(2  CA) ng PxF/F
    double PxF_over_F = P_otimes_F_over_F();
    double dh1 = Nee_.dh1_dxL(xL);
    double ndl_pp = 0.5/CA * PxF_over_F * dh1;

    // the remaining term is wsritten in terms of 
    //   BSigma = 1/(2CA) \sum_{i=1,2} \int_{xi}^1 dz/z Pgq(z) fSigma(xi/z,Q^2)/fg(xi,Q^2)
    // (see paper for details)
    double BSigma = Pgq_otimes_FSigma_over_F()/(2*CA);
    // Compared to the code we have a - due to our +ve def of xL
    //                     -----------\ here
    // double ndl_extra = 2 * (CF/CA-1) * (2*Bgq_-BSigma) * (dh1 + 2*CA/M_PI*xL);
    double ndl_extra = 2 * (CF/CA-1) * (BSigma-2*Bgq_) * (dh1 - 2*CA/M_PI*xL);
    
    return ndl_epem + ndl_pp + ndl_extra;
  }

protected:
  EEHMultiplicity Nee_;

  // full convolution on the RHS of the DGLAP evolution
  virtual double P_otimes_F_over_F() const override{
    PDF pdf1 = HR_(x1_, log(M_));
    PDF pdf2 = HR_(x2_, log(M_));
    PDF dpdf1 = HR_.pdf_convolution(x1_, log(M_), nloops_, qcd_.nf());
    PDF dpdf2 = HR_.pdf_convolution(x2_, log(M_), nloops_, qcd_.nf());
    
    return dpdf1.g()/pdf1.g() + dpdf2.g()/pdf2.g();
  }

  // Pgq \otimes fSigma part of the DGLAP evolution
  double Pgq_otimes_FSigma_over_F() const{
    PDF pdf1 = HR_(x1_, log(M_));
    PDF pdf2 = HR_(x2_, log(M_));
    double dpdf1 = HR_.pdf_gx_convolution(x1_, log(M_), nloops_, qcd_.nf(), 1);
    double dpdf2 = HR_.pdf_gx_convolution(x2_, log(M_), nloops_, qcd_.nf(), 1);

    return dpdf1/pdf1.g() + dpdf2/pdf2.g();
  }

};

//================================================================================
/// implementation of the qgamma -> q DIS multiplicity
class DISQMultiplicity : public PPXMultiplicity{
public:
  DISQMultiplicity(QCDinstance& qcd, Inputs& inputs, HoppetRunner & HR):
    PPXMultiplicity(qcd, inputs, HR), Nee_(qcd, inputs) {}

  /// returns the pure DL bit of the DY multiplicity
  double h1(double xL) const override{
    // this expression includes the initial-state quark beam and photon
    return Nee_.h1(xL) + 1.0;
  }

  /// NDL function h2(xL) [multiplied by sqrt(alphas) in N_NDL
  ///
  /// In this approach we split the DIS multiplicity as
  /// N_DY = N_DL_only + sqrt(alphas(Q)) * N_NDL_only,
  /// where N_DL_only and N_NDL_only are pure functions of
  /// xL = alphas(Q) * |L| (for definitions see top of file).
  ///
  /// N_DL_only is given by NDY_DL() and N_NDL_only
  /// is given by the following function
  double h2(double xL) const override {
    // ee contribution
    double h2_ee = Nee_.h2(xL);

    // PDF correction
    double h2_correction = h2_pdf_correction(xL);

    return h2_ee + h2_correction;
  }

  /// returns the PDF-related NDL bit of the DIS multiplicity
  double h2_pdf_correction(double xL) const{
    // now the pdf correction for DIS
    //
    // This term is written as
    //   2/abar ng(L) dlnF/dlnQ2
    //    = pi/(as CF) nq(L) dlnF/dlnQ2
    //    = pi/(as CF) (dNq/dL) dlnF/dlnQ2
    //    = 1/(2 CF) (dNq/dL) PxF/F
    double PxF_over_F = P_otimes_F_over_F();
    double dh1 = Nee_.dh1_dxL(xL);  //< single hemisphere
    return 0.5/qcd_.CF() * PxF_over_F * dh1;
  }
  
protected:
  EEZMultiplicity Nee_;

  // the convolution (P x F)/F
  virtual double P_otimes_F_over_F() const override {
    //cout << "Got here" << endl;
    //cout << x1_ << endl;
    //cout << log(M_) << endl;
    PDF pdf1  = HR_(x1_, log(M_));
    PDF dpdf1 = HR_.pdf_convolution(x1_, log(M_), nloops_, qcd_.nf());

    // the shower currently does d + gamma -> d
    return dpdf1.d()/pdf1.d();
  }
  
};

#endif // HAS_HOPPET

//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  
  // set up CmdLine
  CmdLine cmdline(argc,argv,true);
  
  // Get the inputs from the command line
  Inputs inputs(&cmdline);
  // allow cmdline to display options with -h flag
  cmdline.assert_all_options_used();

  // Set up QCD contants
  QCDinstance qcd(inputs.nloops,1.0,log(inputs.Q),-0.1);
  if      (inputs.ca_is_2cf)    qcd.set_CA_2CF();
  else if (inputs.cf_is_halfca) qcd.set_CF_halfCA();

#ifdef HAS_HOPPET
  //set up HoppetRunner for pdf access
  HoppetRunner HR;
  HR.set_scales_mapping(false, log(inputs.Q), 1.0, inputs.nloops); 
  HR.initialise(qcd);
#endif

  // create an object of the appropriate class
  unique_ptr<MultiplicityBase> N;
  if (inputs.npdf == 2){
#ifdef HAS_HOPPET
    cout << "# Watch out: for pp we assume a ddbar/gg initial state" << endl;
    if (inputs.quark) N.reset(new PPZMultiplicity(qcd, inputs, HR));
    else              N.reset(new PPHMultiplicity(qcd, inputs, HR));
#else
    cerr << "No HOPPET support, thus no pp processes available" << endl;
#endif    
  } else if (inputs.npdf == 1){
#ifdef HAS_HOPPET
    cout << "# Watch out: for DIS we assume a d-quark initial state" << endl;
    if (inputs.quark) N.reset(new DISQMultiplicity(qcd, inputs, HR));
#else
    cerr << "No HOPPET support, thus no DIS processes available" << endl;
#endif    
  }
  else { // ee
    if (inputs.quark) N.reset(new EEZMultiplicity(qcd, inputs));
    else              N.reset(new EEHMultiplicity(qcd, inputs));
  }
  
  cout << std::setprecision(15) << N->h1(inputs.xL) << " " << N->h2(inputs.xL) << endl;
	   
  return 0;
}


