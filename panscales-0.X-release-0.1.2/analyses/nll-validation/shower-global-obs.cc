/// Code to compute global event shapes in ee2qq, ee2gg, pp2Z, pp2H and disQ
/// uses the weighted event generation with generation windows and a dynamic cutoff
/// The code automatically divides up the runs in a number of windows
/// example of a command line is (assuming one has build in double precision)
/*
   ./build-double/shower-global-obs -process DIS -Q2 1.0 -pdf-lnQref 0.0 -Q 1.0 -rts 5.0 \
                  -nloops 2 -colour NODS -alphas 0.016  -lnkt-cutoff -33.75 \
                  -shower panlocal -beta 0.5 -use-diff -spin-corr off \
                   -strategy RouletteEnhanced -veto-buffer -15.0 -enhancement-factor 20.0 -ln-obs-buffer 3.5 -nln-obs-div 7 \
                   -lambda-obs -0.5 -beta-obs 1.0 \ 
                   -rseq 1 -nev 20000 -out a \
*/
/// This will compute DIS observables with beta_obs = 1.0 for the PanLocal beta = 0.5
/// shower using 7 evolution windows spanning an lnv range of 3.5 below the 
/// targeted lnv ob the observable (computed from lambda-obs and -alphas)
#include "AnalysisFramework.hh"
#include "ShowerRunner.hh"
#include "DISCambridge.hh"
#include "fjcore_local.hh"
#include "BetaDepDynCutFAPX.hh"
#include "FastY3.hh"
#include "LundEEGenerator.hh"
#include "LargeRapRecombiner.hh"
#include <sstream>
#include <cmath>
#include <memory>

using namespace std;
using namespace fjcore;
using namespace fjcore::contrib;
using namespace panscales;

precision_type sqr(precision_type x){ return x*x; }

// helper class to find the declusterings in DIS
class declustering{
  public:
  declustering(precision_type kt, precision_type eta){
    _kt  = kt;
    _eta = eta;
  }

 void printout(){
    std::cout << " kt = " << _kt << ", eta = " << _eta << std::endl;
  }
  // returns lnkt
  precision_type lnkt() const {
    return log(_kt);
  }
  // returns kt
  precision_type kt() const {
    return _kt;
  }
  // returns eta
  precision_type eta() const{
    return _eta;
  }
  private:
    precision_type _kt;
    precision_type _eta;
};

/// @brief Class managing the showerrun
/// and the analysis to construct a selection
/// of global observables in eeZ/H, ppZ/H, disQ
class GlobalObs : public AnalysisFramework {
public:
  bool _pp_event, _dis_event;
  double _ln_obs_max, _ln_obs_min;
  bool _do_ln_obs_min_veto;
  unsigned long long int _local_nev;
  double _lambda_obs;
  double _alphasQ, _veto_buffer, _enhancement_factor;
  double _beta_ps, _beta_obs, _lnQ, _Q;
  precision_type _sudakov_weight;
  JetDefinition _jet_def;
  JetDefinition _jet_def_alt;
  RecursiveLundEEGenerator _lund_ee_gen;
  FastY3 _fasty3;
  Thrust _thrust;
  FCx _fc10;
  FCx _fc05;
  Broadening _broadening;
  string _beta_tag;
  precision_type (*_eta_exponent_lambda)(precision_type dy);
  
  GlobalObs(CmdLine * cmdline_in,
            double ln_obs_max, double ln_obs_min, //< max and min value of the observable
            bool do_ln_obs_min_veto,
            unsigned long long int requested_nev)          //< when true, veto if obs below min
    : AnalysisFramework(cmdline_in),
      _ln_obs_max(ln_obs_max), _ln_obs_min(ln_obs_min),
      _do_ln_obs_min_veto(do_ln_obs_min_veto),
      _local_nev(requested_nev),
      _lund_ee_gen(0,false), 
      _fc10(FCx(1.0)), _fc05(FCx(0.5)),
      _broadening(Broadening(true)){

    _remove_unwanted_options();

    // decide which type of events we are working with
    _pp_event  = (f_collider->type() == Collider::ColliderType::pp);
    _dis_event = (f_collider->type() == Collider::ColliderType::dis);

    // targetted value of lambda = alphas ln_obs
    _lambda_obs = cmdline->value("-lambda-obs", -0.5);
    assert(_lambda_obs < 0 && "Lambda should be negative");
    
    // get the size of a veto buffer
    _veto_buffer = cmdline->value("-veto-buffer",-18.)
      .help("\n\t size of the buffer zone in ln(v_obs)."
            "\n\t The upper edge of the buffer zone is determined by the"
            "\n\t max change in the observable generated (so far).");
    assert((_veto_buffer < 0) && "\n\t -veto-buffer should be negative." );

    // check if we work w replicas
    _enhancement_factor = cmdline->value("-enhancement-factor", 1.0)
      .help("\n\t Constant factor multiplying the Sudakov exponent for "
            "\n\t selected Elements. Facilitates weighted generation   "
            "\n\t in the context of targetting a specific value in the "
            "\n\t λ_obs spectrum.");
    
    // check shower & observable scalings 
    _beta_obs = cmdline->value("-beta-obs", 0.0).help("observable angular scaling");
    _beta_ps  = cmdline->value("-beta",     0.0);

    // cache the top of the "little" lund plane
    _Q   = f_process->hard_scale();
    _lnQ = log(_Q);

    // append binning info to output filename
    ostringstream oss;
    oss << output_filename << "-lnobs_" << _ln_obs_min << "_" << _ln_obs_max;
    output_filename = oss.str();
  }

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {
    // get the value of alphas at the reference scale
    _alphasQ = f_shower_runner->shower()->qcd().alphasMSbar(_lnQ);

    // Overwrite whatever nev was given on the command line by the nev
    // used to instantiate this class.
    nev = _local_nev;

    // adjust the shower range: this is set by the range we want to
    // cover in lambda as well as the various buffers we impose
    //
    // compute the max possible residual difference between ln(v_ps)
    // and ln(kt) at eta=0
    double max_log_rho = to_double((_beta_ps/2)*log(2/numeric_limit_extras<precision_type>::epsilon()));

    // for beta_ps > beta_obs  the stopping condition is imposed in the collinear limit
    // otherwise it is set at eta=0
    double factor = (_beta_ps > _beta_obs) ? ((1+_beta_ps)/(1+_beta_obs)) : 1;
    f_lnvmin = factor*(_ln_obs_min+_veto_buffer) - max_log_rho;
    if (f_shower_runner->shower()->qcd().lnkt_cutoff() > (_ln_obs_min+_veto_buffer)){
      header << "# WARNING: alphas is cutoff at a kt scale above the low end of the buffer region: "
             << f_shower_runner->shower()->qcd().lnkt_cutoff() << " is larger than "
             << (_ln_obs_min+_veto_buffer) << endl;
      cerr << "# WARNING: alphas is cutoff at a kt scale above the low end of the buffer region: "
           << f_shower_runner->shower()->qcd().lnkt_cutoff() << " is larger than "
           << (_ln_obs_min+_veto_buffer) << endl;
    }
    
    // the shower starts just at the upper end of the observable (targetted) boundary.
    //
    // if beta_obs <= beta_ps, then this is _ln_obs_max
    // if beta_obs >  beta_ps, then we match in the collinear limit, yielding _ln_obs_max (1+beta_ps)/(1+beta_obs)
    f_lnvmax = _ln_obs_max;
    if (_beta_obs >  _beta_ps) f_lnvmax *= ((1+_beta_ps)/(1+_beta_obs));
    
    // weighted generation is handled by enhancement factors so we do
    // not need -weighted-generation on the command line
    if(f_weighted_generation) {
      throw std::runtime_error("Error :\n"
                               "weighted-generation option appears to be present with \n"
                               "either unwgtd, or lnvmax < log(√s), or lnObsMaxTarget\n"
                               "(implicitly setting lnvmax < log(√s)).\n"
                               "The weighted-generation setting only works for the\n"
                               "default lnvmax (and hence no lnObxMaxTarget either).\n"
                               "Exiting.\n");
    }
    assert(f_dynamic_lncutoff == 0.0 &&
           "\n\t Remove -dynamic-lncutoff option."
           "\n\t To use the older dynamic lnv cut-off set -veto-strategy lnv"
           "\n\t & the dynamic-lncutoff value instead using -veto-buffer.");
    
    
    // create the veto
    bool enhanced_emissions = _enhancement_factor == 1. ? false : true;
    BetaDepDynCutFAPX<ShowerBase> *fapx_veto = nullptr;
    if (_do_ln_obs_min_veto){
      fapx_veto = new BetaDepDynCutFAPX<ShowerBase> 
                       (_beta_obs, _veto_buffer, enhanced_emissions,
                        std::numeric_limits<double>::max(),
                        _ln_obs_max, _ln_obs_min);
    } else {
      fapx_veto = new BetaDepDynCutFAPX<ShowerBase> 
                       (_beta_obs, _veto_buffer, enhanced_emissions,
                        std::numeric_limits<double>::max(),
                        _ln_obs_max);
    }
    f_shower_runner->set_veto(fapx_veto);
    header << "# Using a FAPX veto with beta=" << _beta_obs << endl;

    // Set the shower's emission enhancement factor
    f_shower_runner->enhancement_factor(_enhancement_factor);
    
    //--------------------------------------------------
    // declare clustering tools
    if (_pp_event){
      _jet_def = JetDefinition(cambridge_algorithm, 1.0);

      // attach a specific recombiner to get to large rapidities safely
      LargeRapRecombiner *large_rap_recombiner = new LargeRapRecombiner();
      _jet_def.set_recombiner(large_rap_recombiner);
      _jet_def.delete_recombiner_when_unused();                             
    } else if (_dis_event){
      double dcut = -1;
      _jet_def = fjcore::JetDefinition(new DISCambridge(dcut));
    } else {
      // Use fast approx thrust calculations for final evolved events.
      _thrust.set_version(2);
      _thrust.try_local_search(true);
    }

    //--------------------------------------------------
    // observable histograms
    const Binning binning(2*_lambda_obs, 0.0, _lambda_obs);
    
    // only declare histograms for a specific value of beta_obs
    if ((_beta_obs > -0.00001) && (_beta_obs < 0.00001)){
      _beta_tag = "00";
      _eta_exponent_lambda = [](precision_type eta) -> precision_type { return 1.0; }; 

      if (_pp_event){
        cumul_hists_err["boson_pt"].declare(binning);
      } else if (_dis_event){
        // broadening
        cumul_hists_err["Bz_Q"].declare(binning);
      } else {
        cumul_hists_err["sqrt_y3"   ].declare(binning);
        cumul_hists_err["fc10"      ].declare(binning);
        cumul_hists_err["B_total"   ].declare(binning);
        cumul_hists_err["B_wide_jet"].declare(binning);
      }
    } else if ((_beta_obs > 0.499999) && (_beta_obs < 0.50001)){
      _beta_tag = "05";
      _eta_exponent_lambda = [](precision_type dy) -> precision_type { return exp(-0.5*std::abs(dy)); }; 
      if (!_pp_event && !_dis_event){
        cumul_hists_err["fc05"].declare(binning);
      }
    } else if ((_beta_obs > 0.999999) && (_beta_obs < 1.00001)){
      _beta_tag = "10";
      _eta_exponent_lambda = [](precision_type dy) -> precision_type { return exp(-std::abs(dy)); }; 
      if (!_pp_event && !_dis_event){
        cumul_hists_err["thrust"].declare(binning);
      } else if (_dis_event){
        cumul_hists_err["tau_Q"].declare(binning);
      }
    } else {
      cout << "beta_obs should be either 0, 1/2 or 1" << endl;
      exit(1);
    }    

    if (_pp_event) // particle sum
      cumul_hists_err["Sp_beta"+_beta_tag].declare(binning);
    // Lund sum and max
    cumul_hists_err["Sj_beta"+_beta_tag].declare(binning);
    cumul_hists_err["Mj_beta"+_beta_tag].declare(binning);
  }
  
  /// -----------------------------------------------------------------
  /// User-defined calculations after post_startup and before
  /// event generation. at this stage, the basic event is available
  /// -----------------------------------------------------------------
  void user_post_startup() override {
    // compute the sudakov
    if (f_lnvmax < _lnQ){
      f_shower_runner->set_integration_strategy(GQ);
      _sudakov_weight = shower_runner().compute_first_sudakov(f_event_base, _lnQ, f_lnvmax).value;
      header << "# Computed Sudakov weight :  " << _sudakov_weight << endl;;
    } else {
      _sudakov_weight = 1.0;
    }
  }
  
  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    // Use also the histogram because it is easier to read from python
    double evwgt = event_weight();
    
    /// inlude the initial Sudakov weight
    evwgt *= to_double(_sudakov_weight);

    if (_pp_event)
      _analyse_pp(evwgt);
    else if (_dis_event)
      _analyse_dis(evwgt);
    else
      _analyse_ee(evwgt);

  }

protected:
  //----------------------------------------------------------------------
  // analyses for ee events
  void _analyse_ee(double evwgt){
    // For brevity
    unsigned int N = f_event.size();

    // For all β_obs = 0, ½, 1 cases we need the Thrust-axis n
    // for FC_1 (β_obs = 0), FC_½ (β_obs = ½), thrust (β_obs = 1).
    _thrust.analyse(f_event);
    Momentum n = _thrust.event_axis();

    // For all β_obs = 0, ½, 1 we want to observables formed from 
    // Lund declusterings
    // NB for N<=2 FastY3 does not reset the CS, so the test is crucial!
    _fasty3.analyse(f_event);
    std::vector<LundEEDeclustering> declusterings;
    if(N>2) declusterings = _lund_ee_gen.result(_fasty3.cs());

    //-------------------------------------------------------------
    // First compute the max and scalar sum of
    //   ν_obs = kt exp(-β_obs|η|) / Q
    // from the primary declusterings.
    precision_type sum=0.0;
    precision_type max=0.0;
    for (const auto & d : declusterings){
      precision_type v = d.kt()*_eta_exponent_lambda(d.eta());
      sum += v;
      if (v>max){ max = v; } 
    }
    cumul_hists_err["Sj_beta"+_beta_tag].add_entry(_lambda(sum/_Q), evwgt);
    cumul_hists_err["Mj_beta"+_beta_tag].add_entry(_lambda(max/_Q), evwgt);

    //-------------------------------------------------------------
    // All the other observables explicitly depend on beta_obs

    // β_obs = 0
    if( _beta_obs == 0.0 ) {
      /// y3 jet rate (watch out for the square root)
      cumul_hists_err["sqrt_y3"].add_entry(_lambda(_fasty3())/2., evwgt);

      // FC_1
      _fc10.analyse(f_event,n,true);
      cumul_hists_err["fc10"].add_entry(_lambda(_fc10()), evwgt);

      // Total and wide broadening
      _broadening.analyse(f_event,n);
      cumul_hists_err["B_total"   ].add_entry(_lambda(_broadening.total()),      evwgt);
      cumul_hists_err["B_wide_jet"].add_entry(_lambda(_broadening.wide_jet()),   evwgt);
    } else
    // β_obs = ½
    if( _beta_obs == 0.5 ) {
      // FC_½
      _fc05.analyse(f_event,n,true);
      cumul_hists_err["fc05"].add_entry(_lambda(_fc05()), evwgt);
    } else 
    // β_obs = 1
    if( _beta_obs == 1.0 ) {
      // 1-T
      double omt = to_double(_thrust.one_minus_thrust());
      cumul_hists_err["thrust"].add_entry(_lambda(omt), evwgt);
    } else {
      assert(false && "beta_obs should be one of 0, 1/2, or 1");
    }

  }


  //----------------------------------------------------------------------
  // analyses for dis events
  void _analyse_dis(double evwgt){
    // get event information
    _Q   = to_double(sqrt(f_event.Q2()));
    // in case of no emissions, don't do anything
    if(f_event.particles().size() == 3) return;

    // now we compute various quantities and create pseudojets
    precision_type sum_current = 0.0; //< sum of pt in current hemisphere
    precision_type tau_Q       = 0.0; //< thrust normalised by Q
    // dot product is needed to normalise sudakov decompositions
    precision_type dot_prod_refs = dot_product(f_event.ref_in(), f_event.ref_out());
    // initialise the list of pseudojets
    vector<fjcore::PseudoJet> particles;
    for (unsigned i = 0; i < f_event.particles().size(); i++) {
      const Particle & p        = f_event.particles()[i];
      // only enter if the particle is a parton (so skip photon)
      // initial state is included
      if(p.is_parton()){
        particles.push_back(p);
        // then check it is final-state for the particle sum observables
        if(!p.is_initial_state()){
          // get the pt and the obs-eta dependence
          precision_type pt         = p.pt();
          precision_type expeta     = _eta_exponent_lambda(p.rap());
          // pi = alphai p_in + betai p_out + pT
          // betai > alphai means we are in the current hemisphere
          precision_type betai  = dot_product(f_event.ref_in(), p)/dot_prod_refs;
          precision_type alphai = dot_product(f_event.ref_out(), p)/dot_prod_refs;
          if(_beta_obs > 0.99) tau_Q += fmin(alphai, betai);
          if(betai > alphai){
            sum_current += pt * expeta;
          }
        }
      }
    }
    // generate the cluster sequence
    ClusterSequence cs(particles, _jet_def);
    auto jets = cs.inclusive_jets();
    
    // find the final-state jet with the largest dot product with the incoming reference vector
    // this will be our primary jet that we will decluster to define the DIS sum and max observables
    bool FS_jet_found = false;
    unsigned int iFS = 0;
    precision_type dot_with_in = -1.;
    for(unsigned i = 0; i < jets.size(); i++){
      precision_type dotin_local = dot_product(f_event.ref_in(), jets[i]);
      // update if we find a larger one
      if(dotin_local > dot_with_in){
        dot_with_in = dotin_local;
        iFS          = i;
        FS_jet_found = true;
      }
    }
    // safety check
    assert(FS_jet_found);
    // find the declusterings
    vector<declustering> declusts; 
    for(unsigned i = 0; i < jets.size() - 1; i++){
      // for the final-state jet, get the declustering
      if(i==iFS) _follow_energetic_branch(jets[i], declusts);
      else{
        // for all the initial-state jets just get the pt and the rapidity
        declustering d(jets[i].pt(), std::abs(jets[i].rap()));
        declusts.push_back(d);
      }
    }
    // get the sum and max of the declusterings
    precision_type max_v_obs = 0., sum_v_obs = 0.;
    _max_sum_v_obs(declusts, max_v_obs, sum_v_obs);
    
    // fill the histograms
    cumul_hists_err["Sj_beta"+_beta_tag].add_entry(_alphasQ * _safe_log(sum_v_obs/_Q), evwgt);
    cumul_hists_err["Mj_beta"+_beta_tag].add_entry(_alphasQ * _safe_log(max_v_obs/_Q), evwgt);
    // particle histograms
    if(_beta_obs > 0.99){ // for beta = 1.0
      cumul_hists_err["tau_Q"].add_entry(_alphasQ * _safe_log(tau_Q), evwgt);
    }
    if(_beta_obs < 0.01){ // for beta = 0.0
      //for beta=0.0 (definition of broadening has an additional factor of 2)
      // this broadening defn has the same NLL expression as the one normalised by E in the current hemisphere
      cumul_hists_err["Bz_Q"].add_entry(_alphasQ * _safe_log(sum_current/_Q), evwgt);
    }
  }

  // helpers for declustering (DIS)
  // recursive function that follows the most energetic particle in a jet
  // and fills the kT and eta
  void _follow_energetic_branch(const PseudoJet & jet, vector<declustering> & declusterings){
    PseudoJet parent_1, parent_2;
    
    if(jet.has_parents(parent_1, parent_2)){
      // make sure parent_1 is the most energetic
      if(parent_2.e() > parent_1.e()) std::swap(parent_1, parent_2);
      // build eta and lnkt
      // 2.p1.p2 = 2E1 E2 - 2|p1|.|p2| cos(theta)
      //   cos(theta) = (E1 E2 - p1.p2)/|p1|.|p2|
      precision_type omc = one_minus_costheta(panscales::Particle::fromP3M2(Momentum3<precision_type>(parent_1.px(), parent_1.py(), parent_1.pz()), parent_1.m2()),
                                              panscales::Particle::fromP3M2(Momentum3<precision_type>(parent_2.px(), parent_2.py(), parent_2.pz()), parent_2.m2()));
      precision_type eta = std::abs(-0.5 * log((omc)/(2 - omc)));
      precision_type kt  = parent_2.e() * sqrt(omc*(2 - omc)); //< E*sin(theta)
      declustering d(kt, eta);
      declusterings.push_back(d);
      _follow_energetic_branch(parent_1, declusterings);
    
    }
  }
  // get the max and sum of the declusterings, return max_v_obs and sum_v_obs
  void _max_sum_v_obs(const vector<declustering> dec, precision_type & max_v_obs, precision_type & sum_v_obs) {
    // Loop over declusterings
    for( auto d : dec) {
      // Compute contribution to observable from declustering d.
      precision_type v_obs = d.kt() * _eta_exponent_lambda(d.eta());
      // Update scalar sum observable.
      sum_v_obs += v_obs;
      // Update max observable.
      if( v_obs > max_v_obs ) max_v_obs = v_obs;
    }
  }

  Momentum find_jet_axis(){
    vector<Particle> partons;
    int event_size = f_event.size();
    for(int i = 0; i < event_size; i++){
      // don't take the photon
      if(!f_event[i].is_parton()) continue;
      partons.push_back(f_event[i]);
    }
    // then begin the jet algorithm
    // we want to end up with two jets: an initial-state one and a final-state one
    // we start with a born event with 3 partons -> go up to
    // int nemns = event_size - 3;
    // need to do the jet recombination until we have just two jets
    // in which case partons.size == 2
    while(partons.size() > 2){
      // reset in every iteration
      int j = -1;
      bool isFSR;
      // set the distances 
      precision_type diB, diq, dimin_now;
      precision_type dimin = std::numeric_limits<double>::max();

      // the first parton was the incoming quark
      // the second parton is the outgoing quark
      auto pin  = partons[0];
      auto pout = partons[1];
      // determine the distance with the beam and the final-state quark
      for(unsigned int i = 2; i < partons.size(); i++){
        precision_type Ei = partons[i].E();
        diB       = dot_product(partons[i], pin)/Ei/pin.E();
        diq       = dot_product(partons[i], pout)/Ei/pout.E();
        dimin_now = min(diB, diq);
        if(dimin_now < dimin){
          dimin = dimin_now;
          j     = i;
          isFSR = (dimin == diq);
        }
      }

      // recombine with parton[1] if from final state
      if (isFSR){
        auto p   = partons[1].momentum().p3() + partons[j].momentum().p3();
	      auto m2  = (partons[1].p4() + partons[j].p4()).m2(); //max(pow2(E) - p.modpsq(), 0.);
        if (m2 < -100*std::numeric_limits<precision_type>::epsilon()){
          f_event.print_following_dipoles();
          std::cout << "New recombined FSR: " << p << ", " << m2 << std::endl;
          assert(false && "Combined momentum has a mass less than 0");
        } else if (m2 < 0){
          m2 = 0.;
        }
        auto mom = Momentum::fromP3M2(p, m2);
        // reset the momentum of the final-state quark
        partons[1].reset_momentum(mom);

      }
      // otherwise do nothing, the beam (=parton[0]) never changes
      
      // remove the particle we just recombined from the list
      partons.erase(partons.begin()+j);
    }
    // in the end the final-state jet will be this one
    return partons[1].momentum();
  }
  //----------------------------------------------------------------------
  // analyses for pp events
  void _analyse_pp(double evwgt){
    //--------------------------------------------------------------------
    // boson pt
    if (_beta_obs==0.0){
      double ln_ptX = 0.5*_safe_log(f_event.particles()[2].pt2()/f_event.particles()[2].m2());
      cumul_hists_err["boson_pt"].add_entry(_alphasQ * ln_ptX, evwgt);
    }

    //--------------------------------------------------------------------
    // compute particle angularities
    precision_type sum=0.0;
    //precision_type sum_energy=0.0;
    double yX = to_double(f_event.particles()[2].rap());
    for (unsigned int i=3; i<f_event.particles().size(); ++i){
      const Particle & p = f_event.particles()[i];
      precision_type pt      = p.pt();
      precision_type expeta = _eta_exponent_lambda(p.rap()-yX);
      sum += pt*expeta;
      //sum_energy+= p.E();
    }
    cumul_hists_err["Sp_beta"+_beta_tag].add_entry(_lambda(sum/_Q), evwgt);

    //--------------------------------------------------------------------
    // compute jets
    //
    // for this we first need a list of particles build with PtYPhiM
    // and the porper m (if non zero... and we assume 0 mass here).
    assert(f_event.particles().size()>=3);
    unsigned int nparticles = f_event.particles().size()-3;
    if (nparticles == 0){
      cumul_hists_err["Sj_beta"+_beta_tag].add_entry(-std::numeric_limits<double>::max(), evwgt);
      cumul_hists_err["Mj_beta"+_beta_tag].add_entry(-std::numeric_limits<double>::max(), evwgt);
      // none of the rest should be computed|binned
      return;
    }
    
    vector<PseudoJet> fj_particles;
    fj_particles.reserve(nparticles);
    for (unsigned int i=3; i<f_event.particles().size(); ++i){
      const Particle & p = f_event.particles()[i];
      PseudoJet pfj = PtYPhiM(p.pt(), p.rap(), p.phi());
      pfj.set_user_info(new LargeRapMassInfo(0.0));
      fj_particles.push_back(pfj);
    }
    vector<PseudoJet> jets = _jet_def(fj_particles);
        
    //--------------------------------------------------------------------
    // compute jet angularities
    sum=0.0;
    precision_type max=0.0;
    for (const auto & j : jets){
      precision_type pt     = j.pt();
      precision_type expeta = _eta_exponent_lambda(j.rap()-yX);
      precision_type x = pt*expeta;
      
      sum += x;
      if (x>max){ max = x; }  // Note that for beta_obs==0, this should be the 1st jet
    }
    cumul_hists_err["Sj_beta"+_beta_tag].add_entry(_lambda(sum/_Q), evwgt);
    cumul_hists_err["Mj_beta"+_beta_tag].add_entry(_lambda(max/_Q), evwgt);
  }
    

  //----------------------------------------------------------------------
  // helpers

  // log avoiding 0
  double _safe_log(precision_type x) const{
    return x<0 ? -std::numeric_limits<double>::max() : to_double(log(x));
  }

  // log v -> lambda
  double _lambda(precision_type v) const{
    return _alphasQ * _safe_log(v);
  }

  // make sure the command line does not end up with unused options
  void _remove_unwanted_options(){
    cmdline->value("-ln-obs-buffer", 2.5);
    cmdline->value("-nln-obs-div",1);
    cmdline->value("-nln-obs-div-stop",1);
    cmdline->value("-nln-obs-div-start",0);
    cmdline->value<string>("-nev-fractions","");
  }

};

// helper to split a string into a vector of strings (comma as a delimiter)
std::vector<std::string> tokenise(std::string const &str, const char delim=','){
  size_t start;
  size_t end = 0;

  vector<string> result;
  while ((start = str.find_first_not_of(delim, end)) != std::string::npos){
    end = str.find(delim, start);
    result.push_back(str.substr(start, end - start));
  }

  return result;
}

////////////////////////////////////////////////
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);

  // We want to target a specific value of th eobservable
  double lambda_obs = cmdline.value("-lambda-obs", -0.5)
    .help("targetted alphas L value for the observable");
  assert( lambda_obs < 0 && "\n\t -lambda-obs must be < 0" );
  
  // we need to know the coupling
  double alphasQ = cmdline.value("-alphas", 0.01).help("MSbar coupling at the scale Q");
  double ln_obs = lambda_obs/alphasQ;

  // to reach the targetted lambda, we will use several slices in a
  // buffer above the targetted value
  // WATCH OUT" THE BUFFER IS GIVEN IN UNITS OF LOG (NOT ALPHAS LOG)
  double ln_obs_buffer = cmdline.value("-ln-obs-buffer", 2.5)
    .help("buffer (in log units) above the targetted observable value");
  assert( ln_obs_buffer >= 0.0 && "\n\t -ln-obs-buffer must be >= 0" );

  unsigned int nln_obs_div = cmdline.value("-nln-obs-div",1)
    .help("\n\t number of subdivisions of the buffer above lambda-obs");
  unsigned int nln_obs_div_stop  = cmdline.value("-nln-obs-div-stop", nln_obs_div).help("\n\t stop after a given subdivision (0 is the top one, the subdivision given as argument is included)");
  unsigned int nln_obs_div_start = cmdline.value("-nln-obs-div-start", 0).help("\n\t start at the given subdivision (0 is the top one, the subdivision given as argument is included)");

  string nev_fractions = cmdline.value<string>("-nev-fractions", "")
    .help("\n\t when present, a comma-separated list of number of nln-obs-div values with the event fractions in the top slices. Left empty puts half the events in the bottom slice and an equal number in all the others");

  unsigned long long int nev = cmdline.value("-nev",1e0);
  //bool threaded = cmdline.present("-threaded").help("when present, run different subdivisions in different threads");
  
  // loop over subdivisions
  // Note that we have on emore for overflow
  // - subdivision 0   starts at the top of the buffer region
  // - subdivision n-1 ends at the targetted observable value
  // - subdivision n   goes (unbounded) below the targetted value
  // 
  double ln_obs_div = ln_obs_buffer / nln_obs_div;

  // define the event repartitionh across slices
  // (the 0th entry will be the top slice)
  vector<unsigned long long int> nev_per_div;
  if (nev_fractions.empty()){
    unsigned long long int nev_half  = nev/2;
    unsigned long long int nev_upper = nev_half/nln_obs_div;
    for(unsigned int idiv = 0; idiv < nln_obs_div; idiv++) {
      nev_per_div.push_back(nev_upper);
    }
    nev_per_div.push_back(nev_half);
  } else {
    vector<string> tokens = tokenise(nev_fractions);
    assert(tokens.size() == nln_obs_div);
    unsigned long long int nev_left = nev;
    unsigned long long int nev_local;
    for(unsigned int idiv = 0; idiv < nln_obs_div; idiv++) {
      nev_local = (unsigned long long int)(stod(tokens[idiv])*nev+0.1);
      nev_per_div.push_back(nev_local);
      assert(nev_left >= nev_local);
      nev_left -= nev_local;
    }
    nev_per_div.push_back(nev_left);
  }

  auto do_div = [&](unsigned int idiv) -> void {
    // determine the bin edges amd the number of events to generate
    double div_ln_obs_max, div_ln_obs_min;
    unsigned long long int local_nev;

    div_ln_obs_max = ln_obs + ln_obs_buffer - idiv * ln_obs_div;
    
    local_nev = nev_per_div[idiv];
    if (local_nev==0) return;
    if(idiv<nln_obs_div){
      div_ln_obs_min = div_ln_obs_max - ln_obs_div;
      // In this mode half the -nev are divided evenly among the segments
      // for ln[v_obs] from lnObs+lnObsBuffer down to, not including, lnObs.
    } else {
      div_ln_obs_min = ln_obs;
      // In this mode half the -nev used to cover all phase space contributing
      // to the bulk ln[v_obs] = lnObs region.
    }

    // create a driver and run it
    CmdLine local_cmdline(argc,argv);
    // get rid of the argument salready processed above
    bool do_ln_obs_min_veto = (idiv<nln_obs_div) ;
    GlobalObs driver(&local_cmdline,
                     div_ln_obs_max,div_ln_obs_min,
                     do_ln_obs_min_veto, local_nev);
    driver.run();
  };

  for(unsigned int idiv = nln_obs_div_start; idiv <= nln_obs_div_stop; idiv++) {
    do_div(idiv);
  }
   
}
