#!/usr/bin/env python3
"""
  script to help run a series of NLL tests:
   - global observables at a given value of lambda
   - rapidity slice (|eta|=1) at a given value of lambda
   - multiplicity at a given value of xi
   - spin correlations at a given value of lambda 
  To specify one of these, run with --tests {globalobs, slice, multiplicity, spincorr}
  The script will proceed with performing a set of warm-ups, 
  which determine how many events need to be submitted to 
  reach the targeted precision (set through --global-accuracy, --mult-accuracy, etc)

  Notes: 
   - spin is turned off for all but the spin correlation computation
   - colour specified through one of:
       CFHalfCA, CATwoCF, segment, nods
   - process is specified through one of:
       eeZ (default), eeH, ppZ, ppH, DIS
   - shower is one of:
       panglobal00, panglobal05, panglobal10
       panlocal00, panlocal05, panlocal10
       panlocal-antenna00, panlocal-antenna05, panlocal-antenna10 (not for DIS)
  
  Run run-nll-tests.py -h for the complete set of arguments
"""
import sys
import subprocess
import os
import re
import argparse
import glob
from datetime import date, datetime
from JobRunner import *
from math import sqrt
import numpy as np
#sys.path.append("../../submodules/AnalysisTools/python")
sys.path.insert(0, os.path.dirname(__file__)+"/../../submodules/AnalysisTools/python")
from hfile import *
from poly_interp import *
import shutil
import time

import utils
from utils import build_all,yes_no,print_log,stdout_from,execute_queued_jobs,create_outdir
from AnalysisGlobalObs import *
from AnalysisMultiplicity import *
from AnalysisRapSlice import *
from AnalysisSpinCorrelations import *

def main():
    #----------------------------------------------------------------------
    # this script is meant to be run from the directory in which it
    # lives; so move into that directory if run from elsewhere
    script_path = os.path.dirname(os.path.abspath( __file__ ))
    os.chdir(script_path)

    # create the basic objects
    global_obs   = GlobalObs()
    multiplicity = Multiplicity()
    rapslice     = RapSlice()
    spincorr     = SpinCorrelations()
    tests = {'globalobs'    : global_obs,
             'multiplicity' : multiplicity,
             'slice'        : rapslice,
             'spincorr'     : spincorr}

    #----------------------------------------------------------------------
    # command-line parsing
    parser = argparse.ArgumentParser()
    group  = parser.add_argument_group('global arguments')
    group.add_argument('--name', type=str, default="nll-tests-tmp", 
                       help='name for the tests (used as directory name)')
    group.add_argument('--shower', choices=utils.showers.keys(), 
                       required=True,
                       help='which shower to run')
    group.add_argument('--proc', choices=utils.procs.keys(), 
                       default = utils.default_proc,
                       help='which process to simulate')
    group.add_argument('--rts', type=float, default=utils.default_rts,
                       help='sqrts (for hadronic collisions)')
    group.add_argument('--nloops', type=int, default=2,
                       help='number of loops (running coupling)')
    group.add_argument('--colour', choices=utils.colours.keys(),
                       default=utils.default_colour,
                       help='colour option')
    group.add_argument('--extra', default='', 
                       help='additional command-line arguments')
    group.add_argument('--tag', default='', 
                       help='additional tag for output files')
    group.add_argument('--seed-offset', type=int, default=0, 
                       help='shift the seeds by that much')
    group.add_argument('--job-length', type=float, default=1.0, 
                       help='target job length (in days)')
    group.add_argument('--tests', default=tests.keys(), 
                       nargs='+', choices=tests.keys(), 
                       help='list of tests to run')
    group.add_argument('--check-period', type=int, default=60, 
                       help='time interval (s) when checking job status')
    group.add_argument('--assume-yes', action='store_true',
                       help='assume all yes/no questions are answered by "yes"')

    for test in tests.keys():
        tests[test].add_parser_arguments(parser)
        
    runner = Runner(parser)

    global args
    args = parser.parse_args()

    if args.assume_yes:
        utils.yes_no_assume_yes = True

    #----------------------------------------------------------------------
    # basic initialisation
    utils.outdir = args.name+'/'
    create_outdir()
    utils.flog = open(utils.outdir+'log', 'a')
    #set_logfile()
    print_log("", stdout=False)
    print_log("Now: "+datetime.now().strftime("%Y-%m-%d %H:%M:%S"), stdout=False)
    print_log("running: "+' '.join(str(x) for x in sys.argv), stdout=False)
    build_all()


    #----------------------------------------------------------------------
    # warmup jobs
    #runner.queue='short'
    runner.queue=''
    for test in args.tests: 
        tests[test].warmup(runner)
    execute_queued_jobs(runner, args.name,
                        'Warmup jobs are running. They should be done fairly rapidly.',
                        args.check_period, args.assume_yes)
    
    #----------------------------------------------------------------------
    # process warmup jobs 
    for test in args.tests:
        tests[test].process_warmup()
    
    #----------------------------------------------------------------------
    # main series of jobs
    runner.queue=None
    for test in args.tests:
        tests[test].main_jobs(runner)
    execute_queued_jobs(runner, args.name,
                        'Main jobs are running. This can take a bit of time.',
                        args.check_period, args.assume_yes)

    #----------------------------------------------------------------------
    # process main series of jobs
    print_log("The results below include a statistical uncertainty (based on the")
    print_log("lowest 3 values of alphas) and a systematic uncertainty (based on")
    print_log("choosing a different set of alphas values). The tests pass if the")
    print_log("deviation is less than '2 sigmas' with the uncertainties added in")
    print_log("quadrature.")
    for test in args.tests:
        tests[test].process_main()
    

#------------------------------------------------------------------------
# Base analysis class
#------------------------------------------------------------------------
class AnalysisBase(object):
    '''
    initialisation (no arguments)
    '''
    def __init__(self):
        pass

    '''
    define specific command-line arguments
    '''
    def add_parser_arguments(self, parser):
        pass

    '''
    initialisation/warmup
    '''
    def warmup(self, runner):
        pass

    '''
    process the warmup output
    '''
    def process_warmup(self):
        pass

    '''
    construct the main series of jobs
    '''
    def main_jobs(self, runner):
        pass

    '''
    process the main jobs
    '''
    def process_main(self):
        pass

    '''
    add commands to the job runner's queue
    '''
    def queue_commands(self, runner, nevs, njobs):
        pass

if __name__ == '__main__': main()
