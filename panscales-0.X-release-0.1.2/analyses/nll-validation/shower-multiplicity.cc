/// Shower code for multiplicity tests
///
/// Everything is already done in the basic analysis. In principle,
/// we could use the example-framework code in the shower-code folder
/// but for clarity, we added a stripped-down version here (it also
/// avoids potential issues if the example-framework code changes)

#include "AnalysisFramework.hh"

using namespace std;
using namespace panscales;

class Multiplicity : public  AnalysisFramework {
public:
  
  Multiplicity(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in) {}

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {
    // some default binning range -- we will think more about this later...
    this->set_default_binning(-2*f_lnvrange, 0.0, f_lnvrange/20.0);
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
  }

};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv,true);
  Multiplicity driver(&cmdline);
  driver.run();
}
