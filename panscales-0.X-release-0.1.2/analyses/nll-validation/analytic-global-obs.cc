#include "CmdLine.hh"
#include "QCD.hh"
#include <iostream>
#include <iomanip>
#include <math.h>
#include <limits>
#include <fstream>
#include <algorithm>
#include <memory>
#include <gsl/gsl_sf.h>

#ifdef HAS_HOPPET
#include "HoppetRunner.hh"
#include "panhoppet_v1.h"
#endif

using namespace std;
using namespace panscales;

const double eulergamma = 0.577215664901532860606512090082402431042;

// run with 
//   ./analytic-global-obs <proc> -Q 1.0 -rts <rts> -lambda <colour> <observable>
// with
//  - proc one of: eeZ, eeH, ppZ, ppH, dis
//  - colour one of: CA2CF, CFhalfCA, segment or NODS

//
// If we write the NLL cumulative distributions as
//  log(Sigma) = g1(lambda) L + g2(lambda)
// this prints g1 and g2
//
// In this expression, g1 is essentially the LL radiator (depending on
// A1, including 1-loop running-coupling corrections and the
// observables beta)
//
// g2 receives different contributions:
//  - 2-loop running in the CMW scheme (11 and K terms)
//  - hard-collinear terms (B1)
//  - a multiple-emission correction (curly F)
//  - a PDF factor

/// generic class to implement an observable.
///
/// By default this is an e+e- global observable of a given beta
///
/// We implement the building blocks of Eq.(3.6) in the CAESAR
/// paper. Note that our input L is -ve (while the one in CAESAR is +ve)
///
/// We write the observable as
///   ln Sigma = g1(asL) |L| + g2(asL) 
class Observable{
public:

  /// virtual dtor
  virtual ~Observable() = default;

  Observable(QCDinstance & qcd, double beta)
    : _qcd(qcd), _beta(beta) {
    _CF = qcd.CF();
    _CA = qcd.CA();
    _nf = qcd.nf();
    _TR = qcd.TR();

    if (_qcd.nloops()>0){
      _b0 = qcd.b0();
    }
    if (_qcd.nloops()>1){
      _b1 = qcd.b1();
      _K  = (67.0/18.0-M_PI*M_PI/6.0)*_CA - 5.0/9.0*_nf;
    }

    _Bq = -3.0/4.0;
    _Bg = (-11*_CA+4*_nf*_TR)/(12*_CA);
#ifdef HAS_HOPPET
    _hoppet_runner = nullptr;
#endif
  }

#ifdef HAS_HOPPET
  /// set the Hoppet runner
  ///
  /// This needs to be initialised first
  void set_hoppet(const HoppetRunner *hoppet_runner){
    _hoppet_runner = hoppet_runner;
  }

  /// set the PDF fractions (-ve x's for e+e-)
  void set_beams(int pdgid1, double x1,
                 int pdgid2 = -1, double x2 = -1){
    _pdgid1 = pdgid1;   _x1 = x1; // for pp and DIS
    _pdgid2 = pdgid2;   _x2 = x2; // = -1 for DIS
  }
#endif
  
  // get the beta
  double beta() const { return _beta; } 

  /// get the LL functions
  double g1(double asL, bool gluon) const{
    // watch out: we get a factor 2 because of the 2 legs
    return -2*_CR(gluon)*_r1(asL);
  }

  /// get the NLL functions
  /// We assume E=Q/2 and dbar=1
  /// We currently also assume S=1
  double g2(double asL, bool gluon) const{
    double CR = _CR(gluon);
    double r2_term  = -2*CR * _r2(asL);
    double hardcoll = -2*CR * _BR(gluon) * _T(asL/(1+_beta));
    double lncurlyF = _ln_curly_F(2*CR*_rprime(asL));
    double lnpdf    = _ln_pdf(asL, _x1, _pdgid1)
                    + _ln_pdf(asL, _x2, _pdgid2);
    return r2_term + hardcoll + lncurlyF + lnpdf;
  }

protected:

  // appropriate colour factor
  double _CR(bool gluon) const{
    return gluon ? _CA : _CF;
  }

  // appropriate colour factor
  double _BR(bool gluon) const{
    return gluon ? _Bg : _Bq;
  }

  // Eq.(A.1) of the CAESAR paper (with a=1)
  double _r1(double asL) const{
    if (_qcd.nloops()==0) return -asL/((1+_beta)*M_PI);
    
    double lambda = -_b0*asL; // note the sign
    if (_beta==0.0){ // Eq.(A.4)
      return -1.0/(2*M_PI*_b0*lambda) * (2*lambda + log(1-2*lambda));
    } else { //< beta!=0, Eq.(A.1)
      return 1.0/(2*M_PI*_b0*lambda*_beta) *
        ((1-2*lambda)*log(1-2*lambda)
         -(1+_beta-2*lambda)*log(1-2*lambda/(1+_beta)));
    }
  }

  // the NLL corrections to the radiator
  double _r2(double asL) const{
    // this is purely the 2-loop (and CMW) running-coupling correction
    if (_qcd.nloops()<2) return 0.0;

    double lambda = -_b0*asL; // note the sign
    double om2l = 1-2*lambda;
    double lom2l = log(om2l);
    if (_beta==0.0){ // Eq.(A.4)
      return 1.0/(2*M_PI*_b0*_b0) * 
        (_K/(2*M_PI) * (lom2l + 2*lambda/om2l)
         -_b1/_b0 * (0.5*lom2l*lom2l + (lom2l+2*lambda)/om2l));
    } else { //< beta!=0, Eq.(A.1)
      double om2lb = 1-2*lambda/(1+_beta);
      double lom2lb = log(om2lb);
      return 1.0/(2*M_PI*_b0*_b0*_beta) * 
        (_K/(2*M_PI) * ((1+_beta)*lom2lb - lom2l)
         +_b1/_b0 * (0.5*lom2l*lom2l - 0.5*(1+_beta)*lom2lb*lom2lb
                     +lom2l - (1+_beta)*lom2lb));
    }
  }

  // The derivative of the radiator, i.e. r'
  double _rprime(double asL) const{
    if (_qcd.nloops()==0) return -2*asL/((1+_beta)*M_PI);
    
    double lambda = -_b0*asL; // note the sign
    if (_beta==0.0){ // Eq.(A.6)
      return 2/(M_PI*_b0)*lambda/(1-2*lambda);
    } else {
      // we use Eq.(2.23)
      return (_T(asL) - _T(asL/(1+_beta)))/_beta;
    }
  }

  // integration of alphas/pi between L=0 and asL
  double _T(double asL) const{
    //TODO: handle the fixed-coupling case
    assert(_qcd.nloops()>0);
    double lambda = -_b0*asL; // note the sign
    return -1/(M_PI*_b0) * log(1-2*lambda);
  }

  // multiple-emission contribution
  // We assume this is a function of R' (OK at our accuracy)
  virtual double _ln_curly_F(double Rprime) const = 0;

  // PDF contribution for a single leg
  double _ln_pdf(double asL, double x, int pdgid) const{
#ifdef HAS_HOPPET
    if (x == -1)                   return 0.0;
    if (_hoppet_runner == nullptr) return 0.0;
    // we start from scale 0
    // and get to scale asL/(1+b)
    return log((*_hoppet_runner)(x, asL/(1+_beta)).flav(pdgid) / 
               (*_hoppet_runner)(x,           0.0).flav(pdgid));
#else
    return 0.0;
#endif
  }

  const QCDinstance & _qcd;
  double _beta;
#ifdef HAS_HOPPET
  const HoppetRunner * _hoppet_runner;
#endif
  double _CF, _CA, _nf, _TR;
  double _b0, _b1, _K;
  double _Bq, _Bg;

  int _pdgid1, _pdgid2; ///< beams PDGids (if pp or DIS)
  double _x1, _x2;      ///< PDF fractions (if pp or DIS)

};


// a few useful derived classed

/// additive observable
class AdditiveObservable : public Observable{
public:
  AdditiveObservable(QCDinstance & qcd,double beta)
    : Observable(qcd, beta){}
  virtual ~AdditiveObservable() = default;

protected:
  virtual double _ln_curly_F(double Rprime) const override{
    // (3.26) of CAESAR
    return -eulergamma*Rprime - lgamma(1+Rprime);
  }
};

/// max observable
class MaxObservable : public Observable{
public:
  MaxObservable(QCDinstance & qcd,double beta)
    : Observable(qcd, beta){}
  virtual ~MaxObservable() = default;

protected:
  virtual double _ln_curly_F(double Rprime) const override{
    return 0.0;
  }
};

/// vector observable
class VectorPtSum : public Observable{
public:
  VectorPtSum(QCDinstance & qcd) : Observable(qcd, 0.0){}
  virtual ~VectorPtSum() = default;

protected:
  virtual double _ln_curly_F(double Rprime) const override{
    if (Rprime>2){
      std::cout<<"WARNING: R'="<< Rprime <<": cannot evaluate the vector pt sum in this region\n";
      return std::numeric_limits<double>::quiet_NaN();
    }
    //std::cout <<"R'="<< Rprime << std::endl;
    //corresponds to eq. 6 of 2021-05-13-logbook
    return -Rprime*eulergamma+lgamma(1-0.5*Rprime)-lgamma(1+0.5*Rprime); 
  }
};

/// base class for broadening-type observables
class Broadening : public Observable{
public:
  Broadening(QCDinstance & qcd) : Observable(qcd, 0.0){}
  virtual ~Broadening() = default;

protected:
  // This is (w R' the single-hemisphere R')
  //   [\int_1^\inf dx/x^2 ((1+x)/4)^{-R'}]^2
  //   = [4^{R'} 2F1(R', 1+R', 2+R'; -1)]^2
  // Keith found that it is conveniently rewritten as
  //   2^{2R'} [1-\sum_{n=1}^\inf 2^{-n}/(R'+n)]^2
  // and that stopping the sum at 100 was enough
  virtual double _ln_f(double Rprime) const{
    double s = 0.0;
    unsigned int nmax = 100;
    for (unsigned int n=nmax; n>0; --n){
      s += pow(0.5, n)/(Rprime+n);
    }
    return 2*Rprime*M_LN2 + 2*log(1-Rprime*s);    
  }
};

/// total broadening
class TotalBroadening : public Broadening{
public:
  TotalBroadening(QCDinstance & qcd) : Broadening(qcd){}
  virtual ~TotalBroadening() = default;

protected:
  virtual double _ln_curly_F(double Rprime) const override{
    return -Rprime*eulergamma - lgamma(1+Rprime) + _ln_f(0.5*Rprime);
  }
};

/// total broadening
class WideBroadening : public Broadening{
public:
  WideBroadening(QCDinstance & qcd) : Broadening(qcd){}
  virtual ~WideBroadening() = default;

protected:
  virtual double _ln_curly_F(double Rprime) const override{
    return -Rprime*eulergamma - 2*lgamma(1+0.5*Rprime) + _ln_f(0.5*Rprime);
  }
};

// Broadening DIS
class BroadeningDIS : public Broadening{
  public:
  /// virtual dtor
  virtual ~BroadeningDIS() = default;

  BroadeningDIS(QCDinstance & qcd) : Broadening(qcd){}
  
  // g1 is given by Eq. A1 of hep-ph/0110213 
  // g2 is given by Eq. A2 of hep-ph/0110213

  protected:
  virtual double _ln_curly_F(double rprime) const override{
    double logLambda = _logLambda(rprime/2.);
    return -lgamma(1.+rprime) + logLambda - rprime/2. * (log(2.) + 2.*eulergamma);
  }
  //R' is given in Eq. A7 of hep-ph/0110213
  private:
  // Eq 2.22 0110213,
  // Notice that we use Rp not 2*Rp as argument, but it should be read as
  // logLambda(double Rp) =  Log [ Lambda(2R')] of Eq 2.22 0110213
  // With Mathematica one finds the result (for Lambda(2R')) to be
  // ConditionalExpression[ 8^R' Gamma[1 - R'/2] Gamma[1 + 2 R'] Hypergeometric2F1Regularized[(3 R')/ 2, 1 + 2 R', 2 + (3 R')/2, -1], -(1/2) < Re[R'] < 2]
  // Notice Hypergeometric2F1Regularized[a,b,c,x] = Hypergeometric2F1[a,b,c,x]/Gamma(c), with Hypergeometric2F1 = gsl_sf_hyperg_2F
  double _logLambda(double Rp) const{
    if(Rp>2-1e-5){
      std::cout<<"Rprime >= 2! Lambda(2R') is not defined\n";
      return std::numeric_limits<double>::quiet_NaN();
    }
    return Rp * log(8.) + lgamma(1.+2*Rp) + lgamma(1.-Rp/2.) + log(gsl_sf_hyperg_2F1(1.5*Rp, 1.+2*Rp, 2.+1.5*Rp, -1)) - lgamma(2.+1.5*Rp);
  }
};

// thrust Q (DIS)
class ThrustQ : public Observable{
  public:
  /// virtual dtor
  virtual ~ThrustQ() = default;

  ThrustQ(QCDinstance & qcd) : Observable(qcd, 1.0){} //< Thrust is a beta = 1 observable

  virtual double _ln_curly_F(double Rprime) const override{
    // (3.26) of CAESAR
    return -eulergamma*Rprime - lgamma(1+Rprime);
  }
};

//----------------------------------------------------------------------
int main(int argc, char ** argv) {
  CmdLine cmdline(argc,argv);

  // process
  bool quark;
  int  npdf;
  // whether we have | ee         | dis       | pp
  //                 | 0 PDF      | 1 PDF     | 2 PDF
  if      (cmdline.present("-eeZ")) { quark=true;  npdf=0;}
  else if (cmdline.present("-eeH")) { quark=false; npdf=0;}
  else if (cmdline.present("-ppZ")) { quark=true;  npdf=2;}
  else if (cmdline.present("-ppH")) { quark=false; npdf=2;}
  else if (cmdline.present("-disQ")){ quark=true;  npdf=1;}
  else {
    cerr << "Process not found. Please specify one of eeZ, eeH, ppZ, ppH, disq" << endl;
    return 1;
  }
  
  // Read input arguments
  double rts    = cmdline.value<double>("-rts",5.0).help("Center-of-mass energy (ee/pp) or energy proton (DIS)");
  double Q      = cmdline.value<double>("-Q",1.0).help("reference vector (com) or energy quark (DIS)");
  // to compute PDF fractions - either take yQ (for pp) or xDIS (for DIS)
  double yQ     = cmdline.value<double>("-yQ",0.0).help("Q rapidity (used to compute x1,x2)");

#ifndef HAS_HOPPET
  if (npdf > 0){
    cerr << "pp/dis collisions requested but no HOPPET support detected." << endl;
    cerr << "Note: rts (" << rts << "), Q (" << Q << ") and yQ (" << yQ << ") ignored." << endl;
    cerr << "Aborting...." << endl;
    exit(1);      
  }
#endif
	   

  // colour config
  bool ca_is_2cf    = cmdline.present("-CA2CF").help("set CA to 2 CF");
  bool cf_is_halfca = cmdline.present("-CFhalfCA").help("set CF to CA/2");
  cmdline.present("-segment");
  cmdline.present("-NODS");

  unsigned int nloops = cmdline.value<unsigned int>("-nloops", 2);
  
  // set up a QCD instance. We'll only use if to get the basic QCD
  // constants. We use lnktmax=0 and we can set alphas and
  // lnktmin to dummy values.
  QCDinstance qcd(nloops, 1.0, 0.0, -0.1);
  if (ca_is_2cf)   { qcd.set_CA_2CF(); }
  if (cf_is_halfca){ qcd.set_CF_halfCA(); }

  //cout << qcd.description() << endl;

  // observable config
  double lambda_obs = cmdline.value<double>("-lambda_obs",-0.5).help("Specific value of asL to calculate (should be -ve)");
  assert(lambda_obs < 0 && "Lambda should be negative");

  unique_ptr<Observable> observable;
  // additive observables (any beta)
  if ((cmdline.present("-fc10"))      ||
      (cmdline.present("-Sp_beta00")) ||
      (cmdline.present("-Sj_beta00")))
    observable.reset(new AdditiveObservable(qcd, 0.0));
  else if ((cmdline.present("-fc05")) ||
	   (cmdline.present("-Sp_beta05")) ||
	   (cmdline.present("-Sj_beta05")))
    observable.reset(new AdditiveObservable(qcd, 0.5));
  else if ((cmdline.present("-thrust")) ||
	   (cmdline.present("-Sp_beta10")) ||
	   (cmdline.present("-Sj_beta10")))
    observable.reset(new AdditiveObservable(qcd, 1.0));
  // max observables
  else if ((cmdline.present("-sqrt_y3")) ||
	   (cmdline.present("-Mj_beta00")))
    observable.reset(new MaxObservable(qcd, 0.0));
  else if (cmdline.present("-Mj_beta05"))
    observable.reset(new MaxObservable(qcd, 0.5));
  else if (cmdline.present("-Mj_beta10"))
    observable.reset(new MaxObservable(qcd, 1.0));
  // boson pt (pp)
  else if (cmdline.present("-boson_pt"))
    observable.reset(new VectorPtSum(qcd));
  // broadening 
  else if (cmdline.present("-B_total"))
    observable.reset(new TotalBroadening(qcd));
  else if (cmdline.present("-B_wide_jet"))
    observable.reset(new WideBroadening(qcd));
  else if (cmdline.present("-Bz_Q"))
    observable.reset(new BroadeningDIS(qcd));
  else if (cmdline.present("-tau_Q"))
    observable.reset(new ThrustQ(qcd));
  else {
    cerr << "Observable not found" << endl;
    return 1;
  }

  cmdline.assert_all_options_used();


  // extra PDF tools for pp/DIS
  // note that the pdf flavour is hardcoded, so be careful
#ifdef HAS_HOPPET
  unique_ptr<HoppetRunner> hoppet_runner;
  if (npdf==2){
    // build the x values from the boson mass and rapidity (only used for pp)
    const double x1 = Q/rts*exp( yQ);
    const double x2 = Q/rts*exp(-yQ);

    //initialise hoppet runner
    // We can set alphas=1 here and later take lnkt=asL
    hoppet_runner.reset(new HoppetRunner);
    hoppet_runner->initialise(qcd);
    hoppet_runner->set_scales_mapping(false, 0.0, 1.0,
				      (nloops>0) ? 1 : 0);

    observable->set_hoppet(hoppet_runner.get());
    observable->set_beams((quark ? -1 : 21), x1,
			  (quark ?  1 : 21), x2);
  } else if (npdf == 1){
    // build the x valuee
    // const double x1 = Q/sqrt(rts);
    const double x1 = Q/rts;

    //initialise hoppet runner
    // We can set alphas=1 here and later take lnkt=asL
    hoppet_runner.reset(new HoppetRunner);
    hoppet_runner->initialise(qcd);
    hoppet_runner->set_scales_mapping(false, 0.0, 1.0,
				      (nloops>0) ? 1 : 0);

    observable->set_hoppet(hoppet_runner.get());
    observable->set_beams((quark ? 1 : 21), x1);
  } else {
    assert(npdf==0);
    observable->set_beams(11, -1, -11, -1);
  }
#endif
  
  cout << setprecision(12) << observable->g1(lambda_obs, !quark) << " "
       << setprecision(12) << observable->g2(lambda_obs, !quark) << endl;

  return 0;
}
