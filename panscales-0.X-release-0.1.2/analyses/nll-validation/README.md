PanScales NLL validation framework
==================================

This code is part of the PanScales framework. Its goal is to provide a
quick interface to performing NLL accuracy tests.

    ========== BEWARE BEWARE BEWARE BEWARE =============
    Running log-accuracy test is a non-trivial task that
    stretches shower numerical accuracy to its limits. 

    The code provided here is meant as a guideline, but
    you may run into trouble with numerical precision.
    If that happens, run with higher-precision types,
    or larger-alphas / smaller-|L| values.    
    ========== BEWARE BEWARE BEWARE BEWARE =============


Currently, the following tests are included:

  * NLL global observables with $\beta=0, 1/2$ and $1$, computed at a fixed $\lambda=\alpha_s L$
  * single-log (SL) rapidity slice non-global log test, again at a fixed $\lambda=\alpha_s L$
  * next-to-double log (NDL) multiplicity test, at a fixed $\xi=\alpha_s L^2$
  * NLL (collinear and soft) spin-correlation tests, again at a fixed $\lambda=\alpha_s L$

NB The spin tests are currently only available for eeZ.

It contains a series of files:

  * `shower-*.cc` correspond to actually running the PanScales showers
  * `analytic-*.cc` contain implementations of analytic expectations.
    In some cases, these requires external runs which are instead pre-computed.
  * `Analysis*.py` are Python scripts running the analyses and
    outputting summaries with the NLL status
  * various intermediate (C++ and Python) helpers
  * [run-nll-tests.py](run-nll-tests.py) which is the parent script that one needs to run

The main workflow is that one runs [run-nll-tests.py](run-nll-tests.py)(see below for
instructions on how to do this) which proceeds in several steps:

  1. it builds the code
  2. it runs a warmup phase with a few events
  3. based on the output of the warmup phase, it deduces the optimal way
     to distribute the jobs so as to achieve a requested accuracy on the
     final set of runs
  4. it runs the "main" jobs
  5. it combines the results so as to produce NLL accuracy conclusions

Along the way, various logfiles are produced so that one can then have
information on some of the intermediate steps.

Please note that for several aspects, these scripts have not been as
extensively tested as the main PanScales code, so they are provided with
some limitations. For example, job management is handled so as to be
able to either run on our clusters, or on a single computer (using
Python's multiprocessing module, or through `sem`). Only the last of
these options is likely to be functional out-of-the box for generic
users and so this limits the accuracy that can be reached.

The logarithmic tests give an outcome based on statistical agreement
with an expected reference result. The user should be aware of the
following aspects:

* the code will conclude that a test is "OK" if there is agreement with
  the reference to within 2 sigma (statistical plus an estimate of the
  systematic uncertainty on the extrapolation); between 2 and 4 sigma,
  the status is labelled as "RE-RUN", indicating that a more definitive
  conclusions requires a run with higher statistics; above 4 sigma the
  test is labelled as "FAIL".
* Some violations of logarithmic accuracy are small effects, and when
  running at moderate statistical accuracy it is possible to erroneously
  get an "OK" even for a shower that should fail.
* Reference results for the slice and the spin correlation tests are
  numerical and have their own statistical errors. This limits the
  ultimate statistical precision that can be reached in the test.

Running the main script
-----------------------

The usage can be obtained by running

      run-nll-tests.py -h

Before giving a more complete description, let us briefly discuss the
main options.

      --name       allows one to give a name for the run. It is used to create a directory of the same name to store the results
  
      --tests {globalobs,multiplicity,slice,spincorr} 
                   list of tests to run (all on by default)
    
      --shower {panglobal00,panglobal05,panglobal10,panlocal-antenna00,panlocal-antenna05,panlocal-antenna10,panlocal00,panlocal05,panlocal10}
                   which shower to run
    
      --proc {eeZ,eeH,ppZ,ppH,disQ}
                   which process to simulate
    
      --extra EXTRA
                   specify additional parameters to pass to the runs 
    
      --job-length JOB_LENGTH
                   target job length (in days) [1 by default]
    
      --assume-yes
                   when the interactive script asks a question to the end-user,
                   just answer "yes" automatically (useful for batch jobs)

Then, one has various options specific to each analysis. These make it
possible to adjust the values of $(\lambda = \alpha_s L,\xi = \alpha_s
L^2)$ at which the test is run, the target accuracy, and more specific
parameters (like the $\alpha_s$ values used for the extrapolation to
$0$, ...) which can, in a first step, be kept at their default value.
For example:

* arguments for global observables:

          --global-lambda=GLOBAL_LAMBDA       target lambda for global obserables (should be negative)
          --global-betas GLOBAL_BETAS         values of the observable betas to consider
  
* arguments for multiplicity:

          --mult-xi MULT_XI                   target xi for multiplicity
  
* arguments for the rapidity slice (Note:taking $C_A=2C_F$):

          --slice-lambda=SLICE_LAMBDA         target lambda=alphas L for multiplicity (should be negative)
  
* arguments for the spin-correlation studies:

          --spincorr-lambda=SPINCORR_LAMBDA   target lambda=alphas L for multiplicity
          --spincorr-modes {coll,soft}        which spin-correlation tests to run (both by default)
  
* Each analysis also has

          --{analysis}-accuracy ACCURACY      target statistical accuracy
          --{analysis}-nev-warmup NEV_WARMUP  number of events for the warmup run
  
  with analysis one of *global, mult, slice* or *spincorr*


Finally, some arguments control how the jobs should be run. At the moment, you can use

      --pool -j <number_of_cores> -s
      --sem  -j <number_of_cores> -s
    
in order to run in parallel through Python's multiprocessing Pool or
through `sem` (the `-s` option tells the job manager to actually run
the jobs rather than just listing them). If the `<number_of_cores>`
argument is omitted, all the available CPU cores will be used.

Unless the `--assume-yes` command-line option is passed, the script is
interactive. There are two consequences of this worth
mentioning. First, the end-user will be given the option to modify the
requested accuracy for each analysis after the warmup phase
(typically, lowering/increasing the accuracy if the estimated number
of jobs is too high or can be increased); second, if one interrupts a
run at some stage and re-runs another job with the same name, one
will be offered the option to keep some of the earlier jobs (or to
regenerate them).

The script will output its summary on the terminal (*i.e.* basic
information about NLL accuracy results for the various observables)
and a series of results in the requested folder. This includes raw
results from the jobs which have been run but, more importantly,
results from the processing scripts. For example, the main results
directory will have a log file with a summary of the run and one file
for each of the analyses that has been run.


Example run
-----------

If running on a single computer, only limited accuracy can be reached,
mostly due to the increased complexity of the global observables with
$\beta_{\rm obs}\neq\beta_{\rm ps}$, and also due to the slightly slower
convergence of the multiplicity tests.

The following command can be run reasonably [around 20 minutes on an Intel(R) i9-9880H CPU]:

      ./run-nll-tests.py --name test --tests globalobs multiplicity slice \
          --shower panglobal00 --proc eeZ --colour NODS --job-length 0.01 \
          --global-lambda=-0.3 --global-accuracy 0.05 --global-nev-warmup 14000 \
          --mult-xi=3.0        --mult-accuracy 0.1    --mult-nev-warmup 20000 \
          --slice-lambda=-0.3  --slice-accuracy 0.01  --slice-nev-warmup 5000 \
          --pool -j -s --assume-yes

Depending on the outcome of the warmup phase, the precise output can
vary. In one example run, the script proposed the following set of "main" runs:

      Global observables job summary (after warmup):
      nevs         0.002       0.004       0.008       0.016
         0.0       14000       14000       14000       14000
         0.5       28000       14000       14000       14000
         1.0       14000       14000       14000       14000
      njobs        0.002       0.004       0.008       0.016
         0.0           1           1           1           1
         0.5           1           1           1           1
         1.0           1           1           1           1
      Total number of jobs: 12 (for a target accuracy of 0.05)
      Accept these settings? (y/n) [assuming y]
      
      Multiplicity job summary (after warmup):
             alphas         nevs        njobs
       0.0000800000      8170000            7
       0.0012800000      7190000            2
       0.0031250000      8740000            1
       0.0051200000      2240000            1
      Total number of jobs: 11 (for a target accuracy of 0.1)
      Accept these settings? (y/n) [assuming y]
      
      RapSlice job summary (after warmup):
                            nevs        njobs
                          784000            1
      target accuracy of 0.01
      Accept these settings? (y/n) [assuming y]

and gave the following summary output

        The results below include a statistical uncertainty (based on the
        lowest 3 values of alphas) and a systematic uncertainty (based on
        choosing a different set of alphas values). The tests pass if the
        deviation is less than '2 sigmas', with the uncertainties added in
        quadrature.
        #-----------------------------------------------------------------
        # LL global observables summary [log(Sigma_PS)/log(Sigma_NLL)-1]
        #-----------------------------------------------------------------
        B_total        (β=0.0)       OK     (+0.1144 +- 0.0791 +- 0.1308)% [ 0.75 σ]
        B_wide_jet     (β=0.0)       OK     (+0.0787 +- 0.0562 +- 0.1346)% [ 0.54 σ]
        Mj_beta00      (β=0.0)       OK     (+0.0190 +- 0.0389 +- 0.0213)% [ 0.43 σ]
        Sj_beta00      (β=0.0)       OK     (+0.0860 +- 0.0848 +- 0.0535)% [ 0.86 σ]
        fc10           (β=0.0)       OK     (+0.0582 +- 0.0855 +- 0.0210)% [ 0.66 σ]
        sqrt_y3        (β=0.0)       OK     (+0.0161 +- 0.0394 +- 0.0218)% [ 0.36 σ]
        Mj_beta05      (β=0.5)       OK     (-0.1423 +- 0.1329 +- 0.1060)% [ 0.84 σ]
        Sj_beta05      (β=0.5)       OK     (-0.1240 +- 0.1591 +- 0.0187)% [ 0.77 σ]
        fc05           (β=0.5)       OK     (-0.1665 +- 0.1632 +- 0.0279)% [ 1.01 σ]
        Mj_beta10      (β=1.0)       OK     (-0.3561 +- 0.1946 +- 0.4105)% [ 0.78 σ]
        Sj_beta10      (β=1.0)       OK     (-0.1895 +- 0.2092 +- 0.1331)% [ 0.76 σ]
        thrust         (β=1.0)       OK     (-0.1917 +- 0.2103 +- 0.1513)% [ 0.74 σ]
        
        
        #-----------------------------------------------------------------
        # NLL global observables summary [Sigma_PS/Sigma_NLL-1]
        #-----------------------------------------------------------------
        B_total        (β=0.0)       OK     (-3.5429 +- 2.6246 +- 3.0324)% [ 0.88 σ]
        B_wide_jet     (β=0.0)       OK     (-1.6168 +- 1.7932 +- 2.3593)% [ 0.55 σ]
        Mj_beta00      (β=0.0)       OK     (-0.6750 +- 1.0488 +- 0.4892)% [ 0.58 σ]
        Sj_beta00      (β=0.0)       OK     (-3.3456 +- 2.8525 +- 1.9826)% [ 0.96 σ]
        fc10           (β=0.0)       OK     (-2.4903 +- 2.9104 +- 1.2463)% [ 0.79 σ]
        sqrt_y3        (β=0.0)       OK     (-0.6080 +- 1.0629 +- 0.4353)% [ 0.53 σ]
        Mj_beta05      (β=0.5)       OK     (+2.9794 +- 3.2258 +- 2.1196)% [ 0.77 σ]
        Sj_beta05      (β=0.5)       OK     (+3.5213 +- 3.8534 +- 1.5749)% [ 0.85 σ]
        fc05           (β=0.5)       OK     (+4.9082 +- 4.0522 +- 2.1332)% [ 1.07 σ]
        Mj_beta10      (β=1.0)       OK     (+5.2101 +- 3.4523 +- 4.2888)% [ 0.95 σ]
        Sj_beta10      (β=1.0)       OK     (+4.0519 +- 3.7791 +- 2.0670)% [ 0.94 σ]
        thrust         (β=1.0)       OK     (+3.9305 +- 3.7873 +- 2.1407)% [ 0.90 σ]
        
        
        #-----------------------------------------------------------------
        # DL multiplicity summary [N_PS-N_NDL]
        #-----------------------------------------------------------------
        multiplicity                 OK     (+0.0907 +- 0.1065 +- 0.0844)% [ 0.67 σ]
        
        
        #-----------------------------------------------------------------
        # NDL multiplicity summary [(N_PS-N_NDL)/sqrt(alphas)]
        #-----------------------------------------------------------------
        multiplicity                 OK     (-1.8841 +- 7.6230 +- 2.4466)% [ 0.24 σ]
        
        
        #-----------------------------------------------------------------
        # NLL(SL) rapidity slice summary [Sigma_PS/Sigma_SL-1]
        #-----------------------------------------------------------------
        rapidity-slice               OK     (-0.1728 +- 0.1164 +- 0.0006)% [ 1.48 σ]


indicating that, to within the requested accuracy, all tests passed
successfully.

Sometimes a test that should pass happens to be outside the 2 sigma
window by a bit. This tends to in particular to happen when the
requested accuracy is not high enough, but it can happen due to the
fluctuations. When the deviation is between 2 and 4 sigma, the status
of the run is "RE-RUN". In this case we suggest increasing the
accuracy by a factor 2, or until the test either clearly passes or
fails.

Note: The above run is insufficient to demonstrate NLL failures for
dipole-kt-like and PanLocal(beta=0) showers. Depending on your setup,
the following command should show failures for `Mj_beta00` and `sqrt_y3`
in both cases [it runs in about half an hour on a AMD Ryzen 9 3950X]
    
      ./run-nll-tests.py --name test --tests globalobs multiplicity slice \
         --shower panglobal00 --proc eeZ --colour NODS --job-length 0.01 \
         --global-lambda=-0.42 --global-accuracy 0.01 --global-nev-warmup 14000 \
         --mult-xi=3           --mult-accuracy 0.05   --mult-nev-warmup 20000   \
         --slice-lambda=-0.35  --slice-accuracy 0.01  --slice-nev-warmup 5000   \
         --pool -j -s --assume-yes
     
