import sys
import os
import argparse
import glob
from math import sqrt
import numpy as np
sys.path.append("../../submodules/AnalysisTools/python")
from hfile import *
from poly_interp import *
import shutil

from utils import yes_no,print_log,stdout_from
import utils # for the variables
from AnalysisBase import *

#------------------------------------------------------------------------
# handles global observables
#
#TODO: for pp collisions, the Z pt is currently not included. We may
#want to fix this with the potential issue that we should run at a
#specific lambda value implying more jobs
#------------------------------------------------------------------------
class GlobalObs(AnalysisBase):
    '''
    initialisation (no arguments)
    '''
    def __init__(self):
        # parameters adjustable from the command line
        self.default_lambda   = -0.5
        self.default_betas    = [0.0, 0.5, 1.0]
        self.default_alphass  = [0.002, 0.004, 0.008, 0.016]
        self.default_buffer   = -15.0
        #self.default_buffer   = -18.0
        self.default_accuracy = 0.0025

        # additional internal parameters
        #self.enhancement_factor = 50.0
        self.enhancement_factor = 20.0

        # systematic errors
        self.syst = {'LL' : 5e-5, 'NLL' : 1e-5}

    '''
    define specific command-line arguments
    '''
    def add_parser_arguments(self, parser):
        self.parser = parser
        group = parser.add_argument_group(f'arguments for global observables')

        group.add_argument('--global-lambda', type=float,
                           default= self.default_lambda, 
                           help='target lambda for global obserables')
        group.add_argument('--global-betas', default=self.default_betas, 
                           nargs='+', 
                           help='values of the observable betas to consider')
        group.add_argument('--global-alphass', default=self.default_alphass, 
                           nargs='+', type=float,
                           help='values of alphas used for the extrapolation to 0')
        group.add_argument('--global-buffer', type=float,
                           default=self.default_buffer, 
                           help='safety buffer')
        group.add_argument('--global-accuracy', type=float,
                           default=self.default_accuracy, 
                           help='target statistical accuracy')
        group.add_argument('--global-nev-warmup', type=int, default=70000, 
                           help='number of events for the warmup run')

    def args(self):
        if not hasattr(self, 'args_'):
            self.args_ = self.parser.parse_args()
        return self.args_
        
    '''
    initialisation/warmup
    '''
    def warmup(self, runner):
        # basic info
        self.betas    = [float(b) for b in self.args().global_betas]
        self.alphass  = self.args().global_alphass
        self.accuracy = self.args().global_accuracy
        self.length   = self.args().job_length
        colour        = utils.colours[self.args().colour]['cmd_args']

        self.warmupdir= utils.outdir+'global_obs/'
        if not os.path.isdir(self.warmupdir): os.mkdir(self.warmupdir)
        self.warmupdir= utils.outdir+'global_obs/warmup/'
        queue_jobs = True
        if os.path.isdir(self.warmupdir): 
            if yes_no('warmup results for the global observables seem to be already available. Do you want to generate new ones?'):
                shutil.rmtree(self.warmupdir)
                os.mkdir(self.warmupdir)
            else:
                queue_jobs = False
        else:
            os.mkdir(self.warmupdir)

        # build a basic command wo
        #  ../build-{float_precisions[ialphas]}/
        #  -alphas <...> -beta-obs <...>
        #  -lnkt-cutoff {lnktcuts[ialphas]} 
        #  -rseq ARG_REPLACE -nev {nevs} 
        #  -out {utils.outdir}/{showertag}-fapx{beta_obs}-lambda{-targetted_lambda}-as{alphas}-nods-rseqARG_REPLACE
        rts = self.args().rts if ("pdf" in utils.procs[self.args().proc]['cmd_args']) else 1.0
        #
        self.command_base = f"""shower-global-obs {utils.procs[self.args().proc]['cmd_args']} \
            -Q 1.0 -rts {rts} -spin-corr off -nloops {self.args().nloops} \
            -lambda-obs {self.args().global_lambda} -veto-buffer {self.args().global_buffer} \
            -enhancement-factor {self.enhancement_factor} -use-diffs \
            -ln-obs-buffer 3.5 -nln-obs-div 7 \
            -shower {utils.showers[self.args().shower]['cmd_args']}  {colour} {self.args().extra} \
            -strategy RouletteEnhanced -output-interval 1000 """

        # get the various cuts and precision types
        self.lnkt_cutoffs = [self.args().global_lambda/alphas + self.args().global_buffer \
                             for alphas in self.args().global_alphass]
        self.lnkt_obs = [self.args().global_lambda/alphas for alphas in self.args().global_alphass]
        more_accurate = 'doubleexp'
        self.precisions = ['double' if cut>-250 else more_accurate \
                           for cut in self.lnkt_cutoffs ]

        self.queue_commands(runner, self.args().global_nev_warmup, 2, self.warmupdir, queue_jobs = queue_jobs)

    '''
    process the warmup output
    '''
    def process_warmup(self):
        
        # we want to get (i) the number of events per job (ii) the number of jobs
        # based on (a) the timing (b) the accuracy for test runs
        nevs_per_day = np.zeros((len(self.betas), len(self.alphass)))
        uncs_one_day = np.zeros((len(self.betas), len(self.alphass)))
        for ibeta,beta in enumerate(self.betas):
            #print(f"{ibeta=}, {beta=}")
            tenb = f'{int(10*beta):02d}'
            for ialpha,alphas in enumerate(self.alphass):
                s  = 0.0
                e2 = 0.0
                t  = 0.0
                n_seq = 0
                for iseq in range(1+self.args().seed_offset, 3+self.args().seed_offset):
                    #for iseq in range(1, 3):
                    filelist = glob.glob(f'{self.outbases[ibeta][ialpha]}{iseq}-lnobs*')
                    
                    if filelist:
                        n_seq += 1
                        for f in filelist:
                            # get the elapsed time
                            elapsed_str = search(open(f, 'r'), '^# time now = .*, elapsed = .*', return_line=True)
                            t += float(elapsed_str[elapsed_str.rfind('=')+1:elapsed_str.rfind(' h')])
                            
                            # get the cumulative distrib (take Sj as a representative)
                            a = get_array(f, f'cumul_hist:Sj_beta{tenb}')
                            # we need the 2nd line
                            s  += a[1,1]
                            e2 += a[1,2]**2
                            #print(f, s, e2)

                # normalise s and e2 as if we were combining the n_seq sequences
                s  /= n_seq
                e2 /= n_seq**2
                        
                # number of events per day
                #   24 hours * n_seq runs * nev_per_run / time_all_runs(hours)
                nevs_per_day[ibeta, ialpha] = 24*self.length*n_seq*self.args().global_nev_warmup/t

                # uncertainty scaled for one day
                uncs_one_day[ibeta,ialpha] = sqrt(e2/(nevs_per_day[ibeta, ialpha]/(n_seq*self.args().global_nev_warmup)))/s
                #print_log(f'{beta} {alphas} {nevs_per_day[ibeta][ialpha]} {uncs_one_day[ibeta][ialpha]}')
            
        while True:
            self.nevs_per_day = np.zeros((len(self.betas), len(self.alphass)))
            self.njobs        = np.zeros((len(self.betas), len(self.alphass)))
            njobs_tot = 0
            for ibeta,beta in enumerate(self.betas):
                # now try to get an optimal distribution of events so as
                # to get the desired stat uncertainty on the extrapolation
                # using all the values of alphas
                # 
                # The expressions below assume that we have values of
                # alphas spaced by factors of 2 and that all values are
                # used for the extrapolation
                if len(self.alphass)==4:
                    coefs = [64.0/21.0, 8.0/3.0, 2.0/3.0, 1.0/21.0]
                elif len(self.alphass)==3:
                    coefs = [8.0/3.0, 2.0, 1.0/3.0]
                elif len(self.alphass)==3:
                    coefs = [2.0, 1.0]
                else:
                    coefs = [1.0]
                D = sum([c*u for c,u in zip(coefs, uncs_one_day[ibeta,:])])
                for i in range(len(self.alphass)):
                    self.njobs[ibeta,i]=coefs[i]*uncs_one_day[ibeta,i]*D/(self.accuracy**2)
            
                # now round things up. For simplicity, we take multiples
                # of 70k events (e.g. so as to have a round number of
                # events in each of the 4 slices)
                for ialpha,alphas in enumerate(self.alphass):
                    # we first round the number of jobs
                    n = int(self.njobs[ibeta,ialpha])+1
                    # then the number of jobs
                    nev = self.njobs[ibeta,ialpha]*nevs_per_day[ibeta,ialpha]/n
                    nev_min = 14000
                    self.nevs_per_day[ibeta,ialpha] = nev_min * (int(nev/nev_min)+1)
                    self.njobs[ibeta,ialpha] = n
                    njobs_tot += n
            
            print_log ("Global observables job summary (after warmup):")
            print_log (f'nevs  ', end='')
            for ialpha,alphas in enumerate(self.alphass):
                print_log (f'{alphas:12.8}', end='')
            print_log()
            for ibeta,beta in enumerate(self.betas):
                print_log (f'{beta:6.1f}', end='')
                for ialpha,alphas in enumerate(self.alphass):
                    print_log (f'{int(self.nevs_per_day[ibeta,ialpha]):12}', end='')
                print_log()
            print_log (f'njobs ', end='')
            for ialpha,alphas in enumerate(self.alphass):
                print_log (f'{alphas:12.8}', end='')
            print_log()
            for ibeta,beta in enumerate(self.betas):
                print_log (f'{beta:6.1f}', end='')
                for ialpha,alphas in enumerate(self.alphass):
                    print_log (f'{int(self.njobs[ibeta,ialpha]):12}', end='')
                print_log()
            print_log(f'Total number of jobs: {njobs_tot} (for a target accuracy of {self.accuracy})')
            if yes_no('Accept these settings?'):
                break
            else:
                self.accuracy = float(input('Enter new target accuracy: '))
        
    '''
    queue the main jobs
    '''
    def main_jobs(self, runner):
        # we build the jobs from
        # - the number of events per day in self.nevs_per_day[ibeta,ialpha]
        # - the number of jobs in self.njobs[ibeta,ialpha]
        self.jobdir= utils.outdir+'global_obs/parts/'
        if os.path.isdir(self.jobdir): 
            if yes_no('(main) results for the global observables seem to be already available. Do you want to generate new ones?'):
                shutil.rmtree(self.jobdir)
            else:
                return
        os.mkdir(self.jobdir)

        self.queue_commands(runner, self.nevs_per_day, self.njobs, self.jobdir)

    '''
    process the main jobs
    '''
    def process_main(self):
        # create the output file
        fout = open(f'{utils.outdir}globalobs-{self.args().shower}-lambda{-self.args().global_lambda}-{self.args().colour}', 'w')

        # first combine the slices for individual values of alphas and beta
        observables = []
        stots = []
        etots = []
        anas  = []
        
        for ibeta,beta in enumerate(self.betas):
            observables.append([])
            # make sure we look in the right directory
            for ialpha, alpha in enumerate(self.alphass):
                self.outbases[ibeta][ialpha] = self.outbases[ibeta][ialpha].replace('warmup', 'parts')
                
            # take a single file and build the list of observables
            f = open(f'{self.outbases[ibeta][0]}{self.args().seed_offset+1}-lnobs_{self.lnkt_obs[0]:g}_{self.lnkt_obs[0]:g}', 'r')
            for l in f.readlines():
                # here we discard a few things temporarily (for debugging, the shower code should have been fixed for the next run)
                if l.startswith('# cumul_hist:') and 'boson_pt' not in l:
                    observables[ibeta].append(l.rstrip()[13:])
                    
            # now process each value of alphas independently
            stot = np.zeros((len(observables[ibeta]), len(self.alphass)))
            etot = np.zeros((len(observables[ibeta]), len(self.alphass)))
            ana  = np.ones ((len(observables[ibeta]), len(self.alphass)))
            for ialpha,alphas in enumerate(self.alphass):
                for iseq in range(1+self.args().seed_offset, int(self.njobs[ibeta,ialpha])+1+self.args().seed_offset):
                    #out = f'{utils.outdir}{self.args().shower}-fapx{beta}-lambda{-self.args().global_lambda}-as{alphas}-{self.args().colour}-rseq'
                    filelist = glob.glob(f'{self.outbases[ibeta][ialpha]}{iseq}-lnobs_*')
                    for iobs, obs in enumerate(observables[ibeta]):
                        s  = 0.0
                        e2 = 0.0
                        if filelist:
                            for f in filelist:
                                # get the cumulative distrib (take Sj as a representative)
                                a = get_array(f, f'cumul_hist:{obs}')
                                # we need the 2nd line
                                s  += a[1,1]
                                e2 += a[1,2]**2
                            stot[iobs, ialpha] += s
                            etot[iobs, ialpha] += e2
                #self.njobs[ibeta,ialpha] = njobs

                # get the final averages
                for iobs, obs in enumerate(observables[ibeta]):
                    stot[iobs, ialpha] = stot[iobs, ialpha]/self.njobs[ibeta,ialpha]
                    etot[iobs, ialpha] = np.sqrt(etot[iobs, ialpha])/self.njobs[ibeta,ialpha]

            # get the analytic expectations for all observables
            for iobs, obs in enumerate(observables[ibeta]):
                g1g2 = stdout_from(f'./build-double/analytic-global-obs -{self.args().proc} -{self.args().colour} -rts {self.args().rts} -Q 1.0 -yQ 0.0 -lambda_obs {self.args().global_lambda} -{obs}').splitlines()[-1].rstrip()
                g1, g2 = [float(x) for x in g1g2.split()]
                for ialpha,alphas in enumerate(self.alphass):
                    L = -self.args().global_lambda/alphas
                    ana[iobs,ialpha] = np.exp(g1*L+g2)
                    
            # print the averages
            for iobs, obs in enumerate(observables[ibeta]):
                print(f'# {obs}-v-alphas', file=fout)
                print (f'# columns: alphas value uncertainty analytic ratio ratio_uncertainty', file=fout)
                for ialpha, alpha in enumerate(self.alphass):
                    print(f'{alpha} {stot[iobs,ialpha]} {etot[iobs,ialpha]} {ana[iobs,ialpha]} {stot[iobs,ialpha]/ana[iobs,ialpha]} {etot[iobs,ialpha]/ana[iobs,ialpha]}', file=fout)
                print(f'', file=fout)
                print(f'', file=fout)            

            # store for later extrapolation
            stots.append(stot)
            etots.append(etot)
            anas .append(ana)

        # do the extrapolations and store them
        if len(self.alphass)==4:
            extraps=[[0,1,2],[0,1,3],[0,2,3],[1,2,3],[0,1,2,3]]
            extrap_iref = 1 + 2 + 4
            extrap_ialt = 1 + 4 + 8
        elif len(self.alphass)==3:
            extraps=[[0,1],[0,2],[1,2],[0,1,2]]
            extrap_iref = 1 + 2 + 4
            extrap_ialt = 1 + 2
        else:
            extraps=[[0,1]]
            extrap_iref = 1 + 2
            extrap_ialt = 1 + 2  # -> extrap_syst=0

        val_ll  = lambda v, e, a : np.log(v)/np.log(a)
        err_ll  = lambda v, e, a : e/(v*np.log(a))
        desc_ll = "log(Sigma_PS)/log(Sigma_NLL)-1"
        val_nll = lambda v, e, a : v/a
        err_nll = lambda v, e, a : e/a
        desc_nll= "Sigma_PS/Sigma_NLL-1"

        for order, fval, ferr, desc in zip(['LL',   'NLL'],
                                           [val_ll, val_nll],
                                           [err_ll, err_nll],
                                           [desc_ll, desc_nll]):
            # do the extrapolations
            extrap_refs = []
            extrap_errs = []
            extrap_alts = []
            for ibeta,beta in enumerate(self.betas):
                ex_refs = []
                ex_errs = []
                ex_alts = []
                for iobs, obs in enumerate(observables[ibeta]):
                    # discard things if any of the errors are nan
                    if np.isnan(etots[ibeta][iobs,:]).any():
                        print (f'# {obs}-{order}-extrapolations', file=fout)
                        print ( '# the barcode is the sum_{i\in indices} 2^i', file=fout) 
                        print (f'# columns: barcode  extrapolation uncertainty deviation(sigma)', file=fout)
                        print ( '# discarded because some uncertainties were found to be NaN', file=fout)
                        ex_refs.append(None)
                        ex_errs.append(None)
                        ex_alts.append(None)
                        continue
                    print (f'# {obs}-{order}-extrapolations', file=fout)
                    print ( '# the barcode is the sum_{i\in indices} 2^i', file=fout) 
                    print (f'# columns: barcode  extrapolation uncertainty deviation(sigma)', file=fout)

                    ys  = fval(stots[ibeta][iobs,:], etots[ibeta][iobs,:], anas[ibeta][iobs,:])
                    dys = ferr(stots[ibeta][iobs,:], etots[ibeta][iobs,:], anas[ibeta][iobs,:])

                    for extrap_indices in extraps:
                        extrap_barcode = 0
                        x  = np.zeros(len(extrap_indices))
                        y  = np.zeros(len(extrap_indices))
                        dy = np.zeros(len(extrap_indices))
                        for i, index in enumerate(extrap_indices):
                            extrap_barcode += 2**index
                            x[i]  = self.alphass[index]
                            y[i]  = ys[index]
                            dy[i] = dys[index]
                        extrap,dextrap = get_polynomial_coefficients(x, y, dy)
                        print (extrap_barcode, extrap[0], dextrap[0], (extrap[0]-1)/dextrap[0], file=fout)
                        if extrap_barcode == extrap_iref:
                            ex_refs.append(extrap[0]-1)
                            ex_errs.append(dextrap[0])
                        elif extrap_barcode == extrap_ialt:
                            ex_alts.append(extrap[0]-1)
                    print ('\n', file=fout)
                extrap_refs.append(ex_refs)
                extrap_errs.append(ex_errs)
                extrap_alts.append(ex_alts)
                    
            # produce the final summary
            print_log( '#-----------------------------------------------------------------', extra_file=fout)
            print_log(f'# {order} global observables summary [{desc}]', extra_file=fout)
            print_log( '#-----------------------------------------------------------------', extra_file=fout)
            for ibeta,beta in enumerate(self.betas):
                for iobs, obs in enumerate(observables[ibeta]):
                    if extrap_refs[ibeta][iobs] is None:
                        print_log(f'{obs:24} unavailable', extra_file=fout)
                    else:
                        val  = extrap_refs[ibeta][iobs]
                        stat = extrap_errs[ibeta][iobs]
                        syst = np.sqrt((extrap_alts[ibeta][iobs]-val)**2+self.syst[order]**2)
                        err  = np.sqrt(stat**2+syst**2)
                        if (abs(val) < 2*err):
                            status = '\033[0;32mOK    \033[0m'
                        elif (abs(val) < 4*err):
                            status = '\033[0;33mRE-RUN\033[0m'
                        else: 
                            status = '\033[0;31mNot OK\033[0m'
#                        status = 'OK    ' if (abs(val) < 2*err) else 'Not OK'
                        sigmas = abs(val)/err
                        print_log(f'{obs:14} (β={beta:3.1f})       {status} ({val*100:+.4f} +- {stat*100:.4f} +- {syst*100:.4f})% [{sigmas:5.2f} σ]', extra_file=fout)
            print_log('\n', extra_file=fout)
                    

            

    '''
    add commands to the job runner's queue
    '''
    def queue_commands(self, runner, nevs, njobs, outdir, queue_jobs=True):
        test = isinstance(nevs,  int)
        self.outbases = []
        for iobs, beta in enumerate(self.args().global_betas):
            outtmp = []            
            for ialphas, alphas in enumerate(self.args().global_alphass):

                nev = nevs  if test else nevs [iobs][ialphas]
                nj  = njobs if test else int(njobs[iobs][ialphas])
                runtime=0.3 if test else 1.2*self.length

                out = f'{outdir}{self.args().shower}-fapx{beta}-lambda{-self.args().global_lambda}-as{alphas}-{self.args().colour}-rseq'
                outtmp.append(out)
                if queue_jobs:
                    command=f'build-{self.precisions[ialphas]}/{self.command_base} -alphas {alphas} -beta-obs {beta} -lnkt-cutoff {self.lnkt_cutoffs[ialphas]} -rseq ARG_REPLACE -nev {nev} -out {out}ARG_REPLACE'
                    runner.run(command, groupname=self.args().name+'-global',
                               range=f'{1+self.args().seed_offset}-{nj+self.args().seed_offset}',days=runtime)
            self.outbases.append(outtmp)
