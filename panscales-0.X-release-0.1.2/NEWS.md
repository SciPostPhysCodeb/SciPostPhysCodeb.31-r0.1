PanScales release notes
=======================

# 2024-04-02: v0.1.2

## Bug fixes
- eliminated an extraneous warning message about LHAPDF when building the Pythia interface
- fixed bug in spin-correlation handling when requesting a specific branching angle
- greater tolerance for PDF sets with very small values of individual PDFs  

## Documentation improvements
- wrote a brief README.md for the unit-tests/ directory

## Internal code changes
- upgrade to a prerelease version of hoppet 1.3.0 (and related compilation structure changes)
- internal changes in the types used for communication within ShowerRunner and with
  the EmissionVeto classes
- upgrades of submodules AnalysisTools and CmdLine to latest versions,
  including reduced frequency of initial output file write-out
- removed obsolete directories tests-matching, validation/old, validation/temp-oldrefruns

# 2024-01-19: v0.1.1

This is a bugfix release. 

- fixed handling of internal and user-defined emission vetoes when the
  event has to be aborted (resolving potential crashes, e.g. with
  double-soft and weighted generation both enabled)
- fixes to RecursiveLundEEGenerator for psibar computations in opposite hemispheres
  (reflecting changes to the LundPlane fastjet contrib module in v2.1.0)
- small cleaning of matching interface and comments

# 2024-01-08: v0.1.0
- first release, intended for research purposes, not for production use
- relative to v0.1.0-beta1, added arXiv number.

# 2023-12-20: v0.1.0-beta1
- beta1 of first release, intended for research purposes, not for production use
