#include "Momentum.hh"
#include "catch.hpp"
#include "utils.hh"
#include "fjcore_local.hh"
#include <limits>
#include <cassert>
using namespace Catch;
using namespace Catch::literals;
using namespace std;
using namespace panscales;

typedef precision_type Float;


class MomentumAndRef : public Momentum {
public: 
  MomentumAndRef(const Momentum & mom_in, const Momentum & ref_in) :
   Momentum(mom_in), _ref(ref_in), _reference_direction(ref_in.direction()), 
   _diff_to_reference(mom_in.direction()-_reference_direction) {}

   bool has_same_ref(const MomentumAndRef & other) const {
     return &(other._ref) == &_ref ;
   }

   const Momentum3<Float> & diff_to_ref() const {return _diff_to_reference;}
private: 
  const Momentum & _ref;
  Momentum3<Float> _reference_direction;
  Momentum3<Float> _diff_to_reference;
};

Float dot_product_alt(const MomentumAndRef & a, const MomentumAndRef & b) {
  assert(a.has_same_ref(b) && a.m2() == 0 && b.m2() == 0);
  // given a triangle of opening angle theta between two unit sides, the
  // length l of the difference is given by l=2sin(theta/2).
  //
  // Using (1-cos theta) = 2 sin^2(theta/2) we get that 
  // dot_product =  E_a E_b l^2/2
  Momentum3<Float> direction_diff = b.diff_to_ref() - a.diff_to_ref();
  return 0.5 * a.E() * b.E() * direction_diff.modpsq();
}    
Float dot_product(const MomentumAndRef & a, const MomentumAndRef & b) {
  Float result = a.E()*b.E() - a.px()*b.px() - a.py()*b.py() - a.pz()*b.pz();

  if (a.m2() == 0 && 
        b.m2() == 0 && 
        result < numeric_limit_extras<Float>::sqrt_epsilon() * a.E()*b.E()) {
      assert(a.has_same_ref(b));
      Momentum3<Float> direction_diff = b.diff_to_ref() - a.diff_to_ref();
      Float thetasq = direction_diff.modpsq();
      return a.E() * b.E() * thetasq/2.0;
  } else {
    return result;
  }
}

TEST_CASE("DirectionDiffs", "[DirectionDiffs]") {

  // see https://github.com/catchorg/Catch2/blob/master/docs/assertions.md#natural-expressions
  // (floating point comparisons subsection)

  vector<double> norms = {1e-60, 1.0, 1e60};
  for (double norm: norms) {

    Momentum mom1 = from_polar(norm, 0.1, 0.07);
    unsigned n = 10;
    for (unsigned i = 1; i <= n; i++) {
      Float theta = 0.1 + (fjcore::pi*i)/pow(16,n);
      Momentum mom2 = from_polar(norm*2, theta, 0.07);
      
      MomentumAndRef mref1(mom1,mom1);
      MomentumAndRef mref2(mom2,mom1);

      //cout << dot_product_alt(mref1,mref2) << " " << dot_product(mom1,mom2) << " " << theta-0.1 << " " << norm*norm*2*sin((theta-0.1)/2)<< endl;
      REQUIRE (dot_product_alt(mref1,mref2) == Catch::Approx(dot_product(mom1,mom2)));
      // Momentum3<Float> dir1 = mom1.direction();
      // Momentum3<Float> dir2 = mom2.direction();
      // Momentum3<Float> dir12 = dir2 - dir1;
      // cout << "norm=" << norm << ", theta_diff= " << theta-0.1 
      //      << " " << cross(dir1,dir2).modp() 
      //      << " " << cross(dir1,dir12).modp() << endl;
      // cout << "          " << dot_product(mom1,mom2) << " " << dot_product(mref1,mref2);
    }
  }
}

TEST_CASE("DirectionDiffsBoostNew", "[DirectionDiffs]") {
  vector<double> norms = {1.0, 1e-10};
  for (double norm: norms) {
    Momentum mom1 = from_polar(norm *  3.0, 0.1, 0.07);
    Momentum mom2 = from_polar(norm * 22.0, 0.5, 0.7);

    LorentzBoost boost(Momentum::fromP3M2(Momentum3<Float>(0.4,0.6,0.2), 0.8));
    
    Momentum mom1_boosted = boost * mom1;
    Momentum mom2_boosted = boost * mom2;

    Momentum3<Float> dirdiff = mom2.direction() - mom1.direction();
    Momentum3<Float> dirdiff_boosted_direct = mom2_boosted.direction() - mom1_boosted.direction();
    Momentum3<Float> dirdiff_boosted = boost.boost_dirdiff(dirdiff, mom1, mom1_boosted);
    REQUIRE(dirdiff_boosted_direct == ApproxM(dirdiff_boosted));

    Momentum3<Float> dirdiff_lintrans = 
        apply_linear_transform_to_dirdiff(boost, mom1.E(), mom2.E(), mom1_boosted, mom2_boosted, dirdiff);

    REQUIRE(dirdiff_boosted_direct == ApproxM(dirdiff_lintrans));

    // try the same thing exchanging 1 and 2
    dirdiff_lintrans = 
        -apply_linear_transform_to_dirdiff(boost, mom2.E(), mom1.E(), mom2_boosted, mom1_boosted, -dirdiff);
    REQUIRE(dirdiff_boosted_direct == ApproxM(dirdiff_lintrans));
  }
}



TEST_CASE("DirectionDiffsBoostOld", "[DirectionDiffs]") {

  // see https://github.com/catchorg/Catch2/blob/master/docs/assertions.md#natural-expressions
  // (floating point comparisons subsection)

  //vector<double> norms = {1e-60, 1.0, 1e60};
  vector<double> norms = {1.0};
  for (double norm: norms) {

    Momentum mom1 = from_polar(norm, 0.1, 0.07);
    Momentum mom2 = from_polar(norm, 0.5, 0.7);

    // double check our 3d dot product
    REQUIRE (mom1.E()*mom2.E() - dot_product_3(mom1,mom2) == Approx(dot_product(mom1,mom2)));


    Momentum unboost = Momentum::fromP3M2(Momentum3<Float>(0.4,0.6,0.2), 0.8);

    Momentum3<Float> dirdiff_ij = mom2.direction() - mom1.direction();
    // cout << "mom1      = " << mom1 << endl;
    // cout << "mom2      = " << mom2 << endl;
    // cout << "dirdiff   = " << dirdiff_ij << endl;
    // cout << "unboost   = " << unboost << endl;

    Float deltaE_ij = -dot_product_3(dirdiff_ij, unboost)/unboost.m();
    Float deltaf_ij = deltaE_ij / (unboost.E() + unboost.m());

    Float Ei = dot_product(mom1, unboost)/unboost.m();
    Float fi = (Ei+mom1.E())/(unboost.E() + unboost.m());

    Float Ej = dot_product(mom2, unboost)/unboost.m();
    Float fj = (Ej+mom2.E())/(unboost.E() + unboost.m());

    Momentum3<Float> mom1_alt3 = mom1.p3() - fi * unboost.p3();
    Momentum mom1_alt(mom1_alt3.px(), mom1_alt3.py(), mom1_alt3.pz(), Ei);

    Momentum3<Float> mom2_alt3 = mom2.p3() - fj * unboost.p3();
    Momentum mom2_alt(mom2_alt3.px(), mom2_alt3.py(), mom2_alt3.pz(), Ej);

    // Momentum3<Float> dirdiff_A_ij = 1.0/(Ei + deltaE_ij) 
    //                         * (dirdiff_ij  - deltaf_ij * unboost.p3() 
    //                            - deltaE_ij/Ei * mom1_alt3);
    Momentum3<Float> dirdiff_A_ij = 1.0/(Ei + deltaE_ij) 
                            * (dirdiff_ij  - deltaf_ij * unboost.p3() 
                               - deltaE_ij * mom1_alt.direction());

    Momentum mom1_new = mom1;
    Momentum mom2_new = mom2;
    mom1_new.unboost(unboost);
    mom2_new.unboost(unboost);
    Momentum3<Float> dirdiff_B_ij = mom2_new.direction() - mom1_new.direction();
    // cout << "deltaE (2-1): " << deltaE_ij << " " << mom2_new.E() - mom1_new.E() << endl;
    // cout << "mom1_alt  = " << mom1_alt << endl;
    // cout << "mom1_new  = " << mom1_new << endl;
    // cout << "mom2_alt  = " << mom2_alt << endl;
    // cout << "mom2_new  = " << mom2_new << endl;
    // cout << "dirdiff (indirect) = " << dirdiff_A_ij << endl;
    // cout << "dirdiff (direct)   = " << dirdiff_B_ij << endl;
    // cout << mom2_alt3 / Ej - mom1_alt3 / Ei  << endl;

    REQUIRE(dirdiff_A_ij.px() == Approx( dirdiff_B_ij.px() ) );
    REQUIRE(dirdiff_A_ij.py() == Approx( dirdiff_B_ij.py() ) );
    REQUIRE(dirdiff_A_ij.pz() == Approx( dirdiff_B_ij.pz() ) );


  }
}
