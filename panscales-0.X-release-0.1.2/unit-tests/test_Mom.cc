#include "Momentum.hh"
#include "MomentumM2.hh"
#include "Approx.hh"
#include "catch.hpp"
#include "utils.hh"
#include <limits>
using namespace Catch;
using namespace Catch::literals;
using namespace std;
using namespace panscales;

typedef precision_type Float;
    
TEST_CASE("Mom3", "[Mom3]") {

  // see https://github.com/catchorg/Catch2/blob/master/docs/assertions.md#natural-expressions
  // (floating point comparisons subsection)

  Momentum3<Float> p(1.0, 2.0, 3.0);
  Momentum3<Float> minusp = -p;
  Momentum3<Float> sum = p + minusp;
  REQUIRE(sum.modpsq() == 0.0);

  Momentum3<Float> p_rev = p.reversed();
  REQUIRE((p+p_rev).modpsq() == 0.0);

}

TEST_CASE("Mom4M2-reverse", "[MomM2],[Mom4]") {
  MomentumM2<Float> p = MomentumM2<Float>::fromP3M2(Momentum3<Float>(3.0, 4.0, 12.0), 87.0);
  Float E_exp = sqrt(pow(13.0, 2) + 87.0);
  MomentumM2<Float> p_rev = p.reversed();
  MomentumM2<Float> sum = p_rev + p;
  REQUIRE(sum.modpsq() == 0.0);
  REQUIRE(sum.E() == Approx(2*E_exp));
  REQUIRE(sum.m() == Approx(2*E_exp));


  Momentum4<Float> p4 = p;
  Momentum4<Float> p4_rev = p4.reversed();
  Momentum4<Float> sum4 = p4_rev + p4;
  REQUIRE(sum4.modpsq() == 0.0);
  REQUIRE(sum4.E() == Approx(2*E_exp));
  REQUIRE(sum4.m() == Approx(2*E_exp));

}

TEST_CASE("Mom4SignedDotProduct", "[Mom4]") {
  // first check things when momenta are substantially different
  Momentum p1 = Momentum::fromP3M2(Momentum3<Float>(+3.0, 4.0, 12.0), 0.0);
  Momentum p2 = Momentum::fromP3M2(Momentum3<Float>(-2.5, 2.0,  7.0), 0.0);
  REQUIRE(dot_product(p1,p2) == ApproxRel(-dot_product(p1,-p2)));

  // then check what happens when they are almost aligned
  Float eps = 1e-3*sqrt(numeric_limits<Float>::epsilon());
  Momentum p3 = Momentum::fromP3M2(Momentum3<Float>(+3.0 + eps, 4.0, 12.0), 0.0);
  REQUIRE(dot_product(p1,p3) == ApproxRel(-dot_product(p1,-p3)));

}

TEST_CASE("LorentzBoost", "[boost]") {
  Momentum p = Momentum::fromP3M2(Momentum3<Float>(3.0, 4.0, 12.0), 87.0);
  Momentum pboost = Momentum::fromP3M2(Momentum3<Float>(0.1,-0.2,0.3),3.0);

  LorentzBoost lboost(pboost);

  Momentum p1 = lboost * p;
  Momentum p2 = p;
  Momentum p3 = p;
  p2.boost(lboost);
  p3.boost(pboost);
  REQUIRE(p1 == ApproxM(p2));
  REQUIRE(p1 == ApproxM(p3));
  REQUIRE(pow2(p1.E()) - p1.modpsq() == Approx(p.m2()));
  // RequireApproxEqual(p1,p2);
  // RequireApproxEqual(p1,p3);

  REQUIRE(lboost.new_energy(p) == Approx(p1.E()));

  LorentzBoost lboost_inv1(pboost.reversed());
  LorentzBoost lboost_inv2 = lboost.inverse();
  p2.boost(lboost_inv1);
  p3.boost(lboost_inv2);
  REQUIRE(p == ApproxM(p2));
  REQUIRE(p == ApproxM(p3));

  LorentzBoost lboost_prod = lboost * lboost_inv2;
  p1 = lboost_prod * p;
  REQUIRE(p == ApproxM(p1));

}


