#include "Matrix3.hh"
#include "Matrix3FJ.hh"
#include "catch.hpp"
#include <limits>
using namespace Catch;
using namespace Catch::literals;
using namespace std;
using namespace fjcore;
using namespace panscales;

typedef precision_type Float;
using namespace Catch;

TEST_CASE("Matrix3", "[Matrix3]") {
  // try it with PseudoJet
  PseudoJet j = PtYPhiM(1, 1.0, 0.3);
  Matrix3 rotmat = Matrix3::from_direction(j);
  PseudoJet new_j = rotmat * PseudoJet(0,0,1,1);
  REQUIRE(new_j.phi() == 0.3_a);
  REQUIRE(new_j.theta() == Approx(j.theta()));

  // try it with Momentum
  for (auto j2:  {Momentum::fromP3M2(Momentum3<Float>(0.2,0.6, 0.8), 0.0),
                  Momentum::fromP3M2(Momentum3<Float>(0.0,0.0, 1.0), 0.0),
                  Momentum::fromP3M2(Momentum3<Float>(1.0,0.0, 0.0), 0.0)}
                  ) {
    j2 = Momentum::fromP3M2(Momentum3<Float>(0.2,0.6, 0.8), 0.0);
    rotmat = Matrix3::from_direction(j2);
    Momentum new_j2 = rotmat * Momentum::fromP3M2(Momentum3<Float>(0.0,0.0, 1.0), 0.0);
    REQUIRE(new_j2.px() == Approx(j2.px()/j2.E()));
    REQUIRE(new_j2.py() == Approx(j2.py()/j2.E()));
    REQUIRE(new_j2.pz() == Approx(j2.pz()/j2.E()));                     
  }

  // and then with Momentum3
  for (auto j2:  {Momentum3<Float>(0.2,0.6, 0.8),
                  Momentum3<Float>(0.0,0.0, 1.0),
                  Momentum3<Float>(1.0,0.0, 0.0)}
                  ) {
    j2 = Momentum::fromP3M2(Momentum3<Float>(0.2,0.6, 0.8), 0.0);
    rotmat = Matrix3::from_direction(j2);
    Momentum new_j2 = rotmat * Momentum::fromP3M2(Momentum3<Float>(0.0,0.0, 1.0), 0.0);
    REQUIRE(new_j2.px() == Approx(j2.px()/j2.modp()));
    REQUIRE(new_j2.py() == Approx(j2.py()/j2.modp()));
    REQUIRE(new_j2.pz() == Approx(j2.pz()/j2.modp()));                     
  }

  // finally, try it without the pre-rotation
  for (auto j2:  {Momentum::fromP3M2(Momentum3<Float>(0.2,0.6, 0.8), 0.0),
                  Momentum::fromP3M2(Momentum3<Float>(0.0,0.0, 1.0), 0.0),
                  Momentum::fromP3M2(Momentum3<Float>(1.0,0.0, 0.0), 0.0)}
                  ) {
    j2 = Momentum::fromP3M2(Momentum3<Float>(0.2,0.6, 0.8), 0.0);
    bool skip_pre_rotation = true;
    rotmat = Matrix3::from_direction(j2, skip_pre_rotation);
    Momentum new_j2 = rotmat * Momentum::fromP3M2(Momentum3<Float>(0.0,0.0, 1.0), 0.0);
    REQUIRE(new_j2.px() == Approx(j2.px()/j2.E()));
    REQUIRE(new_j2.py() == Approx(j2.py()/j2.E()));
    REQUIRE(new_j2.pz() == Approx(j2.pz()/j2.E()));                     
  }



  SECTION("rotation matrix transpose & multiplication") {
    Matrix3 rotmat_transpose = rotmat.transpose();
    Matrix3 should_be_unit = rotmat_transpose * rotmat;
    // cout << "rotmat\n" << rotmat << endl;
    // cout << "rotmat_transpose\n" << rotmat_transpose << endl;
    // cout << "should_be_unit\n" << should_be_unit << endl;
    Approx zero = Approx(0.0).margin(std::numeric_limits<float>::epsilon()*100);
    REQUIRE(should_be_unit(0,0) == 1.0_a);
    REQUIRE(should_be_unit(0,1) == zero );
    REQUIRE(should_be_unit(0,2) == zero );

    REQUIRE(should_be_unit(1,0) == zero );
    REQUIRE(should_be_unit(1,1) == 1.0_a);
    REQUIRE(should_be_unit(1,2) == zero );

    REQUIRE(should_be_unit(2,0) == zero );
    REQUIRE(should_be_unit(2,1) == zero );
    REQUIRE(should_be_unit(2,2) == 1.0_a);
  }
}
