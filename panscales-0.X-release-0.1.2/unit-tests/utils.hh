
#ifndef __PANSCALES_UNITTESTS_UTILS_HH__
#define __PANSCALES_UNITTESTS_UTILS_HH__
#include "Momentum.hh"

namespace panscales
{

// to help with construction of specific directions
inline Momentum from_polar(Float norm, Float theta, Float phi, Float msq = 0)  {
    return Momentum(norm*sin(theta)*cos(phi), norm*sin(theta)*sin(phi), norm*cos(theta), norm, msq);
}


template<class T>
class ApproxMomentumM2 : public MomentumM2<T> {
public:
  ApproxMomentumM2(const MomentumM2<T> & a) : MomentumM2<T>(a) {}
  bool operator==(const MomentumM2<T> & b) const {
    return 
      (this->px() == Catch::Approx(b.px())) &&
      (this->py() == Catch::Approx(b.py())) &&
      (this->pz() == Catch::Approx(b.pz())) &&
      (this->E () == Catch::Approx(b.E ())) &&
      (this->m2() == Catch::Approx(b.m2()));
  }
};
template<class T>
std::ostream & operator<<(std::ostream & ostr, const ApproxMomentumM2<T> & a) {
  // explicitly upcast to MomentumM2<T>, to avoid ambiguous constructor errors on some g++ systems
  const MomentumM2<T> & aa = a;
  return ostr << aa;
}

template<class T> bool operator==(const MomentumM2<T> & a, const ApproxMomentumM2<T> & b) {return b==a;}
inline ApproxMomentumM2<precision_type> ApproxM(const MomentumM2<precision_type> & a) {
  return ApproxMomentumM2<precision_type>(a);
}

template<class T>
class ApproxMomentum3 : public Momentum3<T> {
public:
  ApproxMomentum3(const Momentum3<T> & a) : Momentum3<T>(a) {}
  bool operator==(const Momentum3<T> & b) const {
    return 
      (this->px() == Catch::Approx(b.px())) &&
      (this->py() == Catch::Approx(b.py())) &&
      (this->pz() == Catch::Approx(b.pz()));
  }
};
template<class T>
std::ostream & operator<<(std::ostream & ostr, const ApproxMomentum3<T> & a) {
  return ostr << Momentum3<T>(a);
}

template<class T> bool operator==(const Momentum3<T> & a, const ApproxMomentum3<T> & b) {return b==a;}
inline ApproxMomentum3<precision_type> ApproxM(const Momentum3<precision_type> & a) {
  return ApproxMomentum3<precision_type>(a);
}

} // namespace panscales;

#endif // __PANSCALES_UNITTESTS_UTILS_HH__
