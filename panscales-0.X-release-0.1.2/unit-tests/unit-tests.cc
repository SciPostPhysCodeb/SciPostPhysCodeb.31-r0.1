/// main program for unit testing.
/// NB1: done in such a way that it has an explicit main, so that
///      automated makefile generation picks it up
/// NB2: it is quite slow to compile (about 20s with -O2 and clang),
///      but need only be compiled once.
#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#ifdef __linux
#define PANSCALES_FPETRAP 
#endif // linux 
#ifdef PANSCALES_FPETRAP
// trapping FPE: http://jayconrod.com/posts/33/trapping-floating-point-exceptions-in-linux
//#define _GNU_SOURCE
#include <fenv.h>
#endif // PANSCALES_FPETRAP


int main( int argc, char* argv[] ) {
#ifdef PANSCALES_FPETRAP
     feenableexcept(FE_INVALID | FE_DIVBYZERO);
     //feenableexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW);
     //feenableexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW);
     //feraiseexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW);
#endif // PANSCALES_FPETRAP

  // global setup...
  int result = Catch::Session().run( argc, argv );

  // global clean-up...

  return result;
}

