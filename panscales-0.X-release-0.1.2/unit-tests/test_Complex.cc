#include "Complex.hh"
#include "Approx.hh"
#include "catch.hpp"
#include "utils.hh"
#include <limits>
#include <iostream>
using namespace Catch;
using namespace Catch::literals;
using namespace std;
using namespace panscales;

namespace panscales {
inline bool operator==(complex<double> a, Complex<double> b) {
  return a.real() == Approx(b.real()) && a.imag() == Approx(b.imag());
}
}

#define OPEQTEST(op) {\
  C=A;c=a; C op B; c op b; REQUIRE(C == c);\
  C=A;c=a; C op B.real(); c op b.real(); REQUIRE(C == c);\
}

#define OPTEST(op) {\
  REQUIRE(A op B == a op b);\
  REQUIRE(A op B.real() == a op b.real());\
  REQUIRE(A.real() op B == a.real() op b);\
}

TEST_CASE("Complex","[Complex]") {
  auto a_real = GENERATE(1.3,    10.789, -445.2);
  auto a_imag = GENERATE(0.45,   89.344, -90.7123);
  auto b_real = GENERATE(31.3,  310.789, -3445.2);
  auto b_imag = GENERATE(30.45, 389.344, -390.7123);

  complex<double> A(a_real,a_imag);
  Complex<double> a(a_real,a_imag);
  complex<double> B(b_real,b_imag);
  Complex<double> b(b_real,b_imag);
  complex<double> C;
  Complex<double> c;

  // the following tests should be exact
  REQUIRE(A == a);
  REQUIRE(-A == -a);
  REQUIRE(conj(A) == conj(a));
  REQUIRE(real(A) == real(a));
  REQUIRE(imag(A) == imag(a));

  // this set of tests involves actual calculations
  // so results may differ (at machine precision)
  // between std::complex and panscales::Complex
  REQUIRE(norm(A) == Approx(norm(a)));
  REQUIRE(abs (A) == Approx(abs (a)));
  REQUIRE(arg (A) == Approx(arg (a)));

  // tests of equality and inequality, including cases where
  // either the real or imaginary parts are the same
  Complex<double> ab(a_real, b_imag);
  REQUIRE(a == a);
  REQUIRE((!(a == ab)));
  REQUIRE((!(b == ab)));
  REQUIRE(a != ab);
  REQUIRE(b != ab);
  REQUIRE((! (a != a)));

  OPTEST(+);
  OPTEST(-);
  OPTEST(*);
  OPTEST(/);
 
  OPEQTEST(+=);
  OPEQTEST(-=);
  OPEQTEST(*=);
  OPEQTEST(/=);
}
