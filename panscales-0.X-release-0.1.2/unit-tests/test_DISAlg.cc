#include "DISCambridge.hh"
#include "catch.hpp"
#include "utils.hh"
#include <limits>
using namespace Catch;
using namespace Catch::literals;
using namespace std;
using namespace panscales;
using namespace fjcore;

typedef precision_type Float;


    
TEST_CASE("DISAlg", "[Jets]") {

 
  JetDefinition jd_dis_cam(new DISCambridge(0.25));
  jd_dis_cam.delete_plugin_when_unused();
  JetDefinition jd_dis_aachen(new DISCambridge(-1.0));
  jd_dis_aachen.delete_plugin_when_unused();

  vector<PseudoJet> particles;
  // first try an event with a single particle in the current
  // direction; with Q=1, it's normalised to have energy Q/2
  // as one expects for the Breit frame
  particles.push_back(PseudoJet(0,0,-0.5,0.5));
  ClusterSequence cs(particles, jd_dis_cam);
  auto jets = cs.inclusive_jets();
  REQUIRE(jets.size()  == 1);
  REQUIRE(jets[0].pz() == particles[0].pz());
  //cout << "Cam jets size, pz: " << jets.size() << " " << jets[0].pz() << endl;

  // now try adding two particles: 
  // - one soft & just inside the pi/2 cone of the current jet
  // - one quite forward and hard
  particles.push_back(0.1 * PseudoJet(sqrt(1-0.01),0,-0.1,1.0));
  particles.push_back(PseudoJet(0.1,0,sqrt(1-0.01),1.0));

  // the plain Cambridge version should give us one jet still (current jet)
  cs = ClusterSequence(particles, jd_dis_cam);
  jets = sorted_by_DISdiB(cs.inclusive_jets());
  REQUIRE(jets.size()  == 1);
  REQUIRE(jets[0].pz() == Approx(particles[0].pz()+particles[1].pz()));

  // the Aachen version should give us two jets: a current jet and a fairly forward one
  ClusterSequence cs_aachen(particles, jd_dis_aachen);
  auto jets_aachen = sorted_by_DISdiB(cs_aachen.inclusive_jets());
  REQUIRE(jets_aachen.size()  == 2);
  REQUIRE(jets_aachen[0].pz() == Approx(particles[0].pz()+particles[1].pz()));
  REQUIRE(jets_aachen[1].pz() == Approx(particles[2].pz()));

  // now rescale particles[1] to give it almost as much energy as the original current particle
  // (not exactly as much, otherwise one ends up with a degeneracy in deciding which has
  // the smaller energy, i.e. which gets "frozen")
  particles[1] *= 0.4/particles[1].E();
  std::cout << particles[1].px() << " " << particles[1].py() << " " << particles[1].pz()  << std::endl;

  // the plain Cambridge version should give us two jets given its dcut of 0.25
  // (the clustering will be just below 0.5)
  cs = ClusterSequence(particles, jd_dis_cam);
  jets = sorted_by_DISdiB(cs.inclusive_jets());
  REQUIRE(jets.size()  == 2);
  REQUIRE(jets[0].pz() == Approx(particles[0].pz()));
  REQUIRE(jets[1].pz() == Approx(particles[1].pz()));

  // the Aachen version should still give us its original two jets: a
  // fat current jet and a fairly forward one
  cs_aachen = ClusterSequence(particles, jd_dis_aachen);
  jets_aachen = sorted_by_DISdiB(cs_aachen.inclusive_jets());
  for (const auto & h: cs_aachen.history()) {std::cout << h.parent1 << " " << h.parent2 << " " << h.dij << std::endl;}
  REQUIRE(jets_aachen.size()  == 2);
  REQUIRE(jets_aachen[0].pz() == Approx(particles[0].pz()+particles[1].pz()));
  REQUIRE(jets_aachen[1].pz() == Approx(particles[2].pz()));



  // see https://github.com/catchorg/Catch2/blob/master/docs/assertions.md#natural-expressions
  // (floating point comparisons subsection)

//   Momentum3<Float> p(1.0, 2.0, 3.0);
//   Momentum3<Float> minusp = -p;
//   Momentum3<Float> sum = p + minusp;
//   REQUIRE(sum.modpsq() == 0.0);
// 
//   Momentum3<Float> p_rev = p.reversed();
//   REQUIRE((p+p_rev).modpsq() == 0.0);

}

