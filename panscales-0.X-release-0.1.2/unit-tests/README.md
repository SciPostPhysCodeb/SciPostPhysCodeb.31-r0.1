# Documentation for unit-tests

This directory contains unit-tests for low-level parts of the PanScales
project, using the [Catch2](https://github.com/catchorg/Catch2) library. 

## Building and running the tests

The simplest way of building and running the tests is 

```bash
../scripts/build.py --build-lib -j
./build-double/unit-tests
```

Adding the `-s` option at run-time will list all tests carried out. 

## Adding new tests

To add a new test, create a new file in this directory. For a class or
file (in the shower-code directory) called `Foo`, the tests would
normally be placed in a file called `test_Foo.cc`. To see how to write
the tests it may be useful to inspect
[test_Matrix3.cc](test_Matrix3.cc). For more sophisticated usage it is best to
consult the [Catch2](https://github.com/catchorg/Catch2) documentation.

Note that much of the existing code uses Catch2's `Approx` [functionality](https://github.com/catchorg/Catch2/blob/devel/docs/comparing-floating-point-numbers.md#Approx)
```cpp
  REQUIRE(new_j.phi() == 0.3_a);
  REQUIRE(new_j.theta() == Approx(j.theta()));
```
where `_a` is a suffix defined by Catch2 to indicate that the comparison
need only agree within a certain tolerance. The `Approx` function
behaves similarly. However, for newly written code, the Catch2 authors recommend a
[newer
form](https://github.com/catchorg/Catch2/blob/devel/docs/comparing-floating-point-numbers.md)
of approximate comparisons.

Once a set of tests has been written, the
[CMakeLists.txt](CMakeLists.txt) file should then be edited to add
`test_Foo.cc` to the list of files to be included in the `unit-tests`
target. The Catch2 library then automatically detects and includes the
tests in the `unit-tests` executable.

