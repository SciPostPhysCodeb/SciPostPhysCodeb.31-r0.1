#include "SpinCorrelations.hh"
#include "catch.hpp"
#include "Approx.hh"
#include "utils.hh"
#include <limits>
using namespace Catch;
using namespace Catch::literals;
using namespace std;
using namespace panscales;

typedef precision_type Float;

    
TEST_CASE("Spin", "[Spin]") {

  // see https://github.com/catchorg/Catch2/blob/master/docs/assertions.md#natural-expressions
  // (floating point comparisons subsection)

  Float theta = 0.5;
  Momentum mom1 = from_polar(1.0, theta, 0.7);
  Momentum mom2 = from_polar(1.0, theta+1e-4, 0.7+1e-4);

  //std::cout << "In spin-correl, dot product " << dot_product(mom1,mom2) << std::endl;
  //std::cout << "      mom1 " << mom1 << "\n";
  //std::cout << "      mom2 " << mom2 << "\n";

  Complex<Float> sMin, sPls;
  int reference_direction = 1;
  get_Spls_Smin(mom1, mom2, reference_direction, sPls, sMin);
  // std::cout << "      sMin, sPls, norm(sMin), arg(sMin) " << sMin << " " << sPls << " " << norm(sMin) << " " << arg(sMin) << "\n";
  // std::cout << "(sMin * sPls).imag()" << (sMin * sPls).imag()   << endl;
  REQUIRE(norm(sMin) == 1.0_a);
  REQUIRE((sMin * sPls).real() == -1.0_a);
  REQUIRE((sMin * sPls).imag() ==  ApproxAbs(0.0));

  //auto dirdiff = mom1.direction() - mom2.direction();
  //Complex<Float> dirdiff_sMin = Complex<Float>(dirdiff.px(), dirdiff.py()) / dirdiff.modp();
  //std::cout << "      dirdiff sMin, norm and arg " << dirdiff_sMin << " " << norm(dirdiff_sMin) << " " << arg(dirdiff_sMin) << std::endl;
  //std::cout << std::endl;
}

