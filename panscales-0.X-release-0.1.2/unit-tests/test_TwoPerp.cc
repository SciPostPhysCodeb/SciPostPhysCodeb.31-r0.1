#include "Momentum.hh"
#include "catch.hpp"
#include "utils.hh"
#include "fjcore_local.hh"
#include "Matrix3.hh"

#include <limits>
using namespace Catch;
using namespace Catch::literals;
using namespace std;
using namespace panscales;

typedef precision_type Float;

    
TEST_CASE("TwoPerp", "[TwoPerp]") {

  // see https://github.com/catchorg/Catch2/blob/master/docs/assertions.md#natural-expressions
  // (floating point comparisons subsection)

  //vector<double> norms = {1e-250, 1e-163, 1e-161, 1e-60, 1.0, 1e60, 1e150, 1e161, 1e163};
  // GPS + RV 2022-07-26: removed normalisations whose square would under or over-flow
  // because these cause trouble when internal momentum checks are on
  vector<Float> norms = {1e-150, 1e-60, 1.0, 1e60, 1e150, 1.11111e150};

  // try several event orientations
  vector<Momentum3<Float>> directions = {
    from_polar(1.0, 0.0, 0.0),
    from_polar(1.0, 0.4, 0.8),
  };

  // try several opening angles theta between the two momenta
  vector<double> thetas = {1e-50, 1e-28, 1e-21, 1e-14, 1e-10, 1e-6, 0.1, 0.4, 0.5, M_PI/2, 2.4, M_PI-1e-6, M_PI-1e-10, M_PI-1e-14, M_PI+1e-6};

  // for diagnosing failures in special cases
  // norms = {1.0};
  // directions = {from_polar(1.0,0.0,0.0)};
  // thetas = {1e-50};


  for (const Momentum3<Float> & direction: directions) {
    Matrix3 rotation = Matrix3::from_direction(direction);

    for (Float norm: norms) {
      Momentum dir1 = rotation * from_polar(norm, 0.0, 0.0);

      for (Float theta: thetas) {
        // skip cases where we have a non-aligned direction and a small theta
        // because small theta is supposed to be high-accuracy only when aligned
        if ((direction.px() != 0 || direction.py() != 0) && theta < 0.1) continue;

        Momentum dir2 = rotation * from_polar(norm*2, theta, 0.0);

        Momentum perp1, perp2;
        two_perp(dir1,dir2,perp1,perp2);
        DYNAMIC_SECTION( "norm = " << norm << ", theta= " << theta << ",  pi-theta=" << M_PI-theta 
                         << "\ndirection = " << direction
                         << "\ndir1  = " << Momentum3<Float>(dir1 ) << " " << dir1 .E() << " m2=" << dir1 .m2() 
                         << "\ndir2  = " << Momentum3<Float>(dir2 ) << " " << dir2 .E() << " m2=" << dir2 .m2() 
                         << "\nperp1 = " << Momentum3<Float>(perp1) << " " << perp1.E() << " m2=" << perp1.m2() 
                         << "\nperp2 = " << Momentum3<Float>(perp2) << " " << perp2.E() << " m2=" << perp2.m2() 
                         ){
          REQUIRE(perp1.m2() == -1.0_a);
          REQUIRE(perp2.m2() == -1.0_a);
          // define an approximate zero with a absolute tolerance of
          // 100 * eps
          Approx zero = Approx(0.0).margin(std::numeric_limits<Float>::epsilon()*100);
          //REQUIRE(dot_product(perp1,perp2) ==  0.0_a);
          REQUIRE(dot_product(perp1,perp2) ==  zero);
          // all of these will be of magnitude norm * eps, so
          // in doing the comparison make sure we rescale by 1/(norm * perp.modp())
          REQUIRE(dot_product(perp1,dir1)/(norm*perp1.modp()) ==  zero);
          REQUIRE(dot_product(perp1,dir2)/(norm*perp1.modp()) ==  zero);
          REQUIRE(dot_product(perp2,dir1)/(norm*perp2.modp()) ==  zero);
          REQUIRE(dot_product(perp2,dir2)/(norm*perp2.modp()) ==  zero);
        }
      }
  }
  }
}
