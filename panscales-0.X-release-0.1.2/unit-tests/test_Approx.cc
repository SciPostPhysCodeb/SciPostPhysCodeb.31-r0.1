#include "Approx.hh"
#include "catch.hpp"
#include <limits>
using namespace Catch;
using namespace Catch::literals;
using namespace std;

TEST_CASE("Approx Classes", "[Approx]") {
  // the following values are differences that should, respectively,
  // pass or fail the approximate checks, keeping in mind that
  // the default tolerance is 1e2*numeric_limits<double>::epsilon()
  double eps_sml = 10 * numeric_limits<double>::epsilon();
  double eps_big = 1e3 * numeric_limits<double>::epsilon();
  
  SECTION("ApproxBase::tolerance() values") {
    REQUIRE (eps_sml < ApproxBase::tolerance());
    REQUIRE (eps_big > ApproxBase::tolerance());
  }
  
  SECTION("ApproxAbs") {
    REQUIRE( (1.0==ApproxAbs(1.0)) == true);
    REQUIRE( (1.0==ApproxAbs(1.0 + eps_sml)) == true);
    REQUIRE( (1.0==ApproxAbs(1.0 + eps_big)) != true);
    REQUIRE( (1.0==ApproxAbs(1.0 - eps_big)) != true);
    REQUIRE( (1.0==ApproxAbs(2.0)) != true);
    REQUIRE( (2.0==ApproxAbs(1.0)) != true);
    REQUIRE( (1000.0==ApproxAbs(1000.0 + eps_big)) != true);
  }
  SECTION("ApproxAbsLtGt") {
    REQUIRE( (1000.0 <= ApproxAbs(1000.0 + eps_big)) == true);
    REQUIRE( (1000.0 <= ApproxAbs(1000.0 - eps_sml)) == true);
    REQUIRE( (1000.0 <= ApproxAbs(1000.0 - eps_big)) != true);
                        
    REQUIRE( (1000.0 >= ApproxAbs(1000.0 - eps_big)) == true);
    REQUIRE( (1000.0 >= ApproxAbs(1000.0 + eps_big)) != true);
    REQUIRE( (1000.0 >= ApproxAbs(1000.0 + eps_sml)) == true);

    REQUIRE( (ApproxAbs(1000.0 + eps_big) >= 1000.0) == true);
    REQUIRE( (ApproxAbs(1000.0 - eps_sml) >= 1000.0) == true);
    REQUIRE( (ApproxAbs(1000.0 - eps_big) >= 1000.0) != true);
                                             
    REQUIRE( (ApproxAbs(1000.0 - eps_big) <= 1000.0) == true);
    REQUIRE( (ApproxAbs(1000.0 + eps_big) <= 1000.0) != true);
    REQUIRE( (ApproxAbs(1000.0 + eps_sml) <= 1000.0) == true);
  }
  SECTION("ApproxRel") {
    REQUIRE( (1000.0 == ApproxRel(1000.0 + eps_big      )) == true );
    REQUIRE( (1000.0 == ApproxRel(1000.0 * (1 + eps_big))) == false);
    REQUIRE( (1e-3   == ApproxRel(1e-3   - eps_sml      )) == false);
    REQUIRE( (1e-3   == ApproxRel(1e-3   * (1 - eps_sml))) == true );
  }
  SECTION("ApproxAbsRel") {
    REQUIRE( (1000.0 == ApproxAbsRel(1000.0 + eps_big      )) == true );
    REQUIRE( (1000.0 == ApproxAbsRel(1000.0 * (1 + eps_big))) == false);
    REQUIRE( (1e-3   == ApproxAbsRel(1e-3   - eps_sml      )) == true );
    REQUIRE( (1e-3   == ApproxAbsRel(1e-3   * (1 - eps_sml))) == true );
  }
}
