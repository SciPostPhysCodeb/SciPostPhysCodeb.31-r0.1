PanScales 3rd party code
========================

PanScales is distributed with
------------------------------

- catch.hpp, for unit testing (https://github.com/catchorg/Catch2).
  Boost Software License - Version 1.0 -

- a slightly adapted version of mpfr::real by Christian Schneider
  (http://chschneider.eu/programming/mpfr_real/), GPL license v3
  
- inclusion of automated git version info at compile time from
  https://github.com/andrew-hardin/cmake-git-version-tracking,
  MIT license

- sha1 calculations, from http://www.zedwood.com/article/cpp-sha1-function 
  public domain

- fjcore, cf. https://fastjet.fr/ (including adaptation to use
  higher-precision types), GPL v2 or later

- hoppet, https://github.com/gavinsalam/hoppet, GPL v3 or later

- CmdLine, https://github.com/gavinsalam/CmdLine/
  GPL v2 or later

In addition, PanScales requires
-------------------------------

- the gsl library and headers (https://www.gnu.org/software/gsl/), 
  GNU GPL license v3

Optionally, PanScales can use and/or interface with:

- LHAPDF (https://lhapdf.hepforge.org/), GNU GPL license v3

- Pythia8 (https://pythia.org/), GNU GPL license v2

- tcmalloc, https://github.com/google/tcmalloc, Apache License v2.0

- the QD library by David Bailey, https://www.davidhbailey.com/dhbsoftware/
  released under the LBNL-BSD-License (for building with the ddreal and qdreal types)

- mpfr (for mpfr::real), only if using high-precision mpfr types
  GNU LGPL v3 or later

