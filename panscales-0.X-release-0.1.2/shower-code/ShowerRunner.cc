//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

//#define PSVERBOSE

#include <typeinfo>
#include <iomanip>
#include <fstream>
#include "ShowerRunner.hh"
#include "config.hh"
#include "sha1.hh"
#include "AverageAndError.hh"
#include <cassert>

#include "Weight.hh"

// ShowerRunner makes use of some specific knowledge 
// of the following showers
#include "ShowerPanScaleLocal.hh"
#include "ShowerPanScaleLocalpp.hh"
#include "ShowerPanScaleLocalDIS.hh"
#include "ShowerPanScaleGlobal.hh"
#include "ShowerPanScaleGlobalpp.hh"
#include "ShowerPanScaleGlobalDIS.hh"


using namespace std;

namespace panscales{
  
const unsigned int ShowerRunner::ACCEPTANCE_PROBABILITY_MARGIN = 10;

GSLRandom gsl;

// Declare the warnings we will use
LimitedWarning ShowerRunner::_warning_acceptance_probability_large_x_violation,
               ShowerRunner::_warning_do_kinematics_true_if_accept,
               ShowerRunner::_warning_ISR_rescaling(20),
               ShowerRunner::_warning_ISR_safety_increased(20);
LimitedWarning ShowerRunner::_warning_matched_weighted_needs_checking(1000);

// initialise that we will expect the first printout of a banner
bool ShowerRunner::_first_printout_of_banner = true;

//----------------------------------------------------------------------
// ShowerRunner implementation
//----------------------------------------------------------------------

//-----------------------------------------------------------------
// ctor
ShowerRunner::ShowerRunner(ShowerBase * shower, Strategy strategy) :
  _shower(shower), _strategy(strategy), _spin_cor_tree(SpinCorTree()), 
  _do_enhanced_roulette(false), _integration_strategy(MC), 
  _enhancement_factor(1.),
  _half_central_rap_window(std::numeric_limits<double>::max()), 
  _collinear_rap_window(std::numeric_limits<double>::max()),
  _find_element_alg_bias(0.),
  _matching_ptr(NULL), _hoppet_runner(nullptr),
  _mcatnlo_subtract_ps_for_weighted(true),_use_first_sudakov_for_colour(false),
  _isr_overhead_factor(1.0)
  {
  //_colour_transition_runner(shower->qcd()),
  
  _strategy_no_matching = _strategy;
  _set_strategy_pointers();
  //_spin_cor_tree = SpinCorTree();

  _colour_transition_runner = create_colour_transition_runner(shower->qcd());
  _init_double_soft_defaults();
  _initialise_overhead_factors();
  
}

//-----------------------------------------------------------------
// returns a description of the shower
std::string ShowerRunner::description() const {
  std::ostringstream ostr;
  ostr << _shower->description();
  ostr << " with strategy " << strategy_description();
  if (_shower->use_diffs()) ostr << ", with direction differences";
  return ostr.str();
}


//========================================================================
//
// main functions to run the shower
//
//========================================================================
  
  
//-----------------------------------------------------------------
// main code to run a shower
// This evolves from lnv down to lnv_min and fills event
//   - event               (initialised) event to fill
//   - lnv                 initial value of the evolution variable
//   - lnv_min             minimal value of lnv (evolution is run to lnv_min)
//   - dynamic_lncutoff    if this is negative, then the
//                         generation stops when the evolution
//                         variable lnv < lnvfirst +
//                         dynamic_lncutoff where lnvfirst is
//                         the lnv value corresponding to the
//                         first (hardest) emission
//   - weighted_generation when true the first emission is generated
//                         with a weight
//   - max_emsn            stop if max_emsn have been generated
//                         (-ve means infinity)
void ShowerRunner::run(Event & event, double lnv, double lnv_min,
                       double & weight, bool weighted_generation,
                       double dynamic_lncutoff, int max_emsn, bool continuation) {


  IFPSVERBOSE(std::cout << "Starting event with gsl: " << sha1(gsl.hex_state()) << "\n";)

  if (!continuation) {
    // save the gsl state before we start the run
    if(save_state_on_fail())
      copy_state(gsl);

    // #TRK_ISSUE-516 GS-NEW-CODE: it is tempting to move this into _initialise_run, but
    //initialise_run is called again at the end of matching so it could
    //cause nasty side effects.
    if (_matching_ptr)
      _initialise_matching_for_event();

    // Initialise storage of elements, and cached info
    _initialise_run(event, lnv);
    
    // record the event under request
    if (_do_cache_step_by_step_event){
      _step_by_step_event.reset();
      _step_by_step_event.set_initial_event(event);    
    }
  } else {
    assert(!weighted_generation && "weighted_generation not implemented for continuation");
    assert(!_matching_ptr && "matching not implemented for continuation");
  }

  // Initialise variable to help record number of emissions, to know
  // when to record _lnvfirst
  int nemsn = 0;
  std::unique_ptr<ShowerBase::EmissionInfo> emission_info(_shower->create_emission_info());

  // Generate weighted first emission if required
  if (weighted_generation) {
    // the current routine takes an lnv argument, but generate_weighted_first_emission
    // assumes lnv == _lnvmax, so check that assumption.
    assert(lnv     == ApproxAbsRel(_sudakov_array[0]    .lnv));
    assert(lnv_min == ApproxAbsRel(_sudakov_array.back().lnv));
    bool match_emission = _match_current_emission(event);
    Sudakov sudakov_weight = generate_weighted_first_emission(event, lnv_min, emission_info);
    // #TRK_ISSUE-517 GS-NEW_CODE: should this overwrite or multiply? [I'd say multiply]
    weight = sudakov_weight.value;
    // #TRK_ISSUE-518 GS-NOTE: should we set this lnv value in emission_info and get rid of it in
    // _update_elements_in_store_post_splitting [w Ludo & Rob]
    // (It seems to be the only call to _update_elements_in_store_post_splitting
    // where lnv has not already been stored in emission_info)
    lnv    = sudakov_weight.lnv;
    if (weight == 0. || lnv < lnv_min) {return;}
    nemsn++;
    // record lnvfirst
    _lnvfirst = lnv;

    // rewind power showers
    // #TRK_ISSUE-519 GS-NEW-CODE: check this!!
    if ((match_emission) && (_matching_ptr->is_power_shower()))
      lnv = _lnvmax;
    // #TRK_ISSUE-520 GS-NEW-CODE: moved inside generate_weighted_first_emission
    //_update_elements_in_store_post_splitting(event, lnv, emission_info.get());
  }


  // Shower event until evolution scale goes under its cut-off.
  EmissionAction status = EmissionAction::veto_emission;
  while (true) {
    IFPSVERBOSE(cout << "--- Top of run() main loop, event.size= " << event.size() 
                     << " gsl: " << sha1(gsl.hex_state()) << "\n");

    // If we exceed a user-defined no. of emissions exit the shower.
    if (max_emsn >= 1 && nemsn >= max_emsn) break;
  
    // Generate a candidate lnv + emitter from the event ensemble.
    typename ShowerBase::Element * element = (this->*_find_element_alg_ptr)(event, lnv);
    // If we drop below the shower cut-off stop showering and return.
    if (lnv < lnv_min) break;

    bool match_emission = _match_current_emission(event);
    IFPSVERBOSE(cout << "match_emission = " << match_emission << endl;)

    // handle cache updates prior to dipole splitting
    // #TRK_ISSUE-521 GS-NEW-CODE: it would be nice if this could be hidden somewhere
    //else, on the other hand it only affects the cases w veto and I'm
    //not sure about what we do w vetos
    //
    // #TRK_ISSUE-522 GS-NEW-CODE: note that, in teh presence of a veto (and only in
    //this case) this updates the max astar and min b to the new
    //lnv. This may be problematic when we implement disordered
    //emissions.
    _update_elements_in_store_pre_splitting(event, element, lnv);
    
    // Generate a lnb to go with the lnv generated above.
    double lnb;
    bool lnb_was_generated = (this->*_find_lnb_alg_ptr)(element, lnv, lnb);
    if (!lnb_was_generated) continue;

    // apply the acceptance associated w the full colour structure
    emission_info->set_element(element);
    emission_info->lnv = lnv;
    emission_info->lnb = lnb;

    // proceed by doing the emission
    //                                             unweighted
    status = do_one_emission(event, emission_info, false, weight);
    if (status == EmissionAction::abort_event){ event=Event(); break;}
    if (status == EmissionAction::terminate_shower) break;
    if (status == EmissionAction::accept){
      nemsn++;
      if ( nemsn == 1 ) {
        // cache the lnv of the first emission
        _lnvfirst = lnv;
        _lnbfirst = lnb;
      }
      // rewind power showers
      // #TRK_ISSUE-523 GS-NEW-CODE: check this!!
      if ((match_emission) && (_matching_ptr->is_power_shower()))
        lnv = _lnvmax;
      //lnv_last = lnv;
    }
    //_shower->update_double_soft_overhead(lnv - lnv_last);
    
    // dynamic cutoff is only used if it is negative
    if (dynamic_lncutoff < 0 && lnv - _lnvfirst < dynamic_lncutoff) {
      break;
    }
  }

  // #TRK_ISSUE-???? GPS-note: if event was aborted, do we not need to do something special here?
  //                           Code added accordingly 2023-08-08
  //                           Also: do we not need to communicate this somehow to the calling routine
  //                           e.g. so that it can decide whether to try and repeat the event?
  if (status == EmissionAction::abort_event) {
    assert(event.size() == 0 && "if event is being aborted, event-size must be zero");
    return;
  }

  // if we need to boost before storing the final event we must to so now
  if(_shower->boost_at_end()){
    event.boost_and_realign();
#ifdef PSVERBOSE
    event.print_following_dipoles();
#endif
  }
  

  // the veto may also decide to veto the event at the very end; 
  // if so it is our code's responsibility to set the event to 
  // an empty event
  if (_veto_ptr){
    WeightedAction veto = _veto_ptr->final_event_veto(event);
    assert((veto.action == EmissionAction::accept) || (veto.action == EmissionAction::abort_event));
    if (veto.accept(gsl) != EmissionAction::accept) event = Event(); 
  }
}


//-----------------------------------------------------------------
// returns a weight factor associated with n real emissions
// Arguments:
// - event        initial event (will be returned modified with at
//                most n emissions)
// - lnv_max      max value of the shower variable lnv
// - lnv_min      min value of the shower variable lnv
// - n_emissions  (max) number of requested emissions
// - weight       event weight (should be passed initialised and
//                will be returned updated)
//
// note that this does not include any virtual corrections
//
// #TRK_ISSUE-524 GS-NEW-CODE: not quite sure how to handle (i) _lnvfirst and (ii)
//matching. It would all be OK if we call do_n_real_emissions
//but I'm not sure about the behaviour in subsequent functions.
//For now, I've implemented the same behaviour as in the all-order
//code which is to let do_n_real_emissions handle _lnvfirst and
//ignore it elsewhere. This would likely give an undefined matching
//behaviour e.g. if one decides to call directly
//do_one_real_emission_fixed_lnv_lnb.
void ShowerRunner::do_n_real_emissions(Event & event, double lnv_max, double lnv_min,
                                       unsigned int n_emissions, double &weight) {
  // #TRK_ISSUE-5241 GS-NEW-code: first order had a different code here:
  //The main question is whether we need to tweak the alg bias or not
  //
  //   if (_matching_ptr) {
  //     _initialise_matching_for_event();
  //     // lnv and lnb are simply generated uniformly within their overestimated
  //     // generation range here. So find_element_alg_bias
  //     // is temporarily overwritten with zero.
  //     original_find_element_alg_bias = _matching_ptr->find_element_alg_bias();
  //     _matching_ptr->set_find_element_alg_bias(0.);
  //   }
  
  if (_matching_ptr) {
    _initialise_matching_for_event();
    // alg bias needs to be temporarily overwritten with zero,
    // as lnv and lnb are generated uniformly
    _matching_ptr->set_find_element_alg_bias(0.);
  }

  // Initialise storage of elements, and cached info
  _initialise_run(event, lnv_max);
  
  // Initialise variable to help record number of emissions, to know
  // when to record _lnvfirst
  std::unique_ptr<ShowerBase::EmissionInfo> emission_info(_shower->create_emission_info());

  // Shower event until evolution scale goes under its cut off.
  double lnv = lnv_max;
  unsigned int nemsn = 0;
  for (unsigned int i_emission=0; i_emission<n_emissions; ++i_emission) {
#ifdef PSVERBOSE
    std::cout << "Size of the event: " << event.size() << " gsl: " << sha1(gsl.hex_state()) << "\n";
#endif
    // Generate a candidate lnv
    double lnv_now = lnv;
    weight *= (lnv_now - lnv_min);
    lnv = gsl.uniform(lnv_now, lnv_min);
    
    bool match_emission = (event.can_be_matched() && _matching_ptr);
    
    // call a sub-function to radiate at that value of lnv
    EmissionAction status = do_one_real_emission_with_fixed_lnv(event, lnv, emission_info, weight);
    if (status == EmissionAction::abort_event){ event=Event(); break;}
    if (status == EmissionAction::terminate_shower) break;
    if (status == EmissionAction::accept){
      ++nemsn;
      if (nemsn == 1) { // This is the first emmision
        _lnvfirst = lnv;
        _lnbfirst = emission_info->lnb;
      }
      // rewind power showers
      // #TRK_ISSUE-525 GS-NEW-CODE: check this!!
      if ((match_emission) && (_matching_ptr->is_power_shower()))
        lnv = _lnvmax;
    }
    // no need to go any further if the weight is already 0
    if (weight == 0.0) break;
  }

  // #TRK_ISSUE-526 GS-NEW-CODE: if any of the two following calls are true, this
  //means that we cannot call the subfunctions
  //(do_one_real_emission_with_fixed_lnv(...), ...). We can prevent
  //this by having 2 versions: a private one (w the current code) and
  //a public one w a test that these things are not true, but I'm not
  //sure this would work for the veto?
  
  // if we need to boost before storing the final event we must to so now
  if(_shower->boost_at_end()){
    event.boost_and_realign();
  }
  
  // the veto may also decide to veto the event at the very end; 
  // if so it is our code's responsibility to set the event to 
  // an empty event
  // #TRK_ISSUE-527 GS-NEW-CODE: what do we do with the weight?
  if (_veto_ptr){
    WeightedAction veto = _veto_ptr->final_event_veto(event);
    assert((veto.action == EmissionAction::accept) || (veto.action == EmissionAction::abort_event));
    if (veto.accept(gsl) != EmissionAction::accept) event = Event(); 
  }

}


//-----------------------------------------------------------------
// Generate an array of Sudakov form factors at equally spaced
// values of the squared (logarithm of the) evolution variable lnv
// between lnv_max and lnv_min, automatically working out a sensible
// number of cache points.
// See 2019-03-08-weighted-first-generation for more details
void ShowerRunner::cache_sudakov_values_uniform_lnsudakov(Event & event,
                                                          double lnv_max, double lnv_min,
                                                          double lnsudakov_spacing, uint64_t nev) {
  _sudakov_array_is_uniform = false;

  if (_integration_strategy == MC) {
    cout << "Constructing the Sudakov cache with MC integration " << nev << " events/bin " << endl;
  } else {
    cout << "Constructing the Sudakov cache with Gaussian integration (" << _integration_strategy << ")" << endl;
  }
  cout << "and an automatically calculated number of lnv cache bins" << endl;
  cout << "This may take a while..." << endl;

  // first work out the log of the Sudakov for the full range
  cout << "Estimating Sudakov for full range (" << lnv_min << " < lnv < " << lnv_max << ")" << endl;
  Sudakov sudakov_obj = compute_first_sudakov(event, lnv_max, lnv_min, nev);
  double sudakov = sudakov_obj.value;
  cout << "initial Sudakov = " << sudakov << " ± " << sudakov_obj.error << std::endl;

  // this sets the number of points, and require at least n=2, since
  // later we will have various expressions involving 1/(n-1).
  unsigned int npoints = std::max(2U, (unsigned int)(std::abs(log(sudakov) / lnsudakov_spacing) + 1));
  
  // start off with a Sudakov that is 1 at lnv_max, by definition
  _sudakov_array.clear();
  Sudakov cumulative_sudakov(lnv_max, 1.0, 0.0);
  _sudakov_array.push_back(cumulative_sudakov);

  // then work out the Sudakov incrementally from there
  cout << "Now calculating Sudakov at " << npoints << " lnv values" << endl;
  for (unsigned int i = 1; i < npoints; i++) {
    double lnv = lnv_max - sqrt(pow(lnv_max - lnv_min,2)/(npoints - 1.) * i);
    // work out a Sudakov from the last lnv to the current one
    Sudakov sudakov_factor = compute_first_sudakov(event, _sudakov_array[i-1].lnv, lnv, nev); 
    // then multiply the Sudakov at the last lnv by this factor to get the new Sudakov
    cumulative_sudakov *= sudakov_factor;
    cumulative_sudakov.lnv = lnv;
    cout << i << " of " << npoints << ", " 
         << lnv << " < lnv < " <<  _sudakov_array[i-1].lnv 
         << ", Sudakov_bin="   << sudakov_factor.value      << " ± " << sudakov_factor.error
         << ", Sudakov_total=" << cumulative_sudakov.value  << " ± " << cumulative_sudakov.error
         << endl;
    _sudakov_array.push_back(cumulative_sudakov);
  }
}

//========================================================================
//
// main entry points to do a single emission
//
//========================================================================

// code which does a full emission
// It takes as input:
// - event           the event to which we try adding an emission
// - emission_info   an emission info where lnv, lnb and
//                   the element have already been decided
// - weighted        true for weighted events (a pre-initialised
//                   weight is returned through "weight",
//                   false for unweighted events
// - weight          event weight (for "unweighted" generation
//                   this would still include potential veto weights)
// - phi_mode        how phi is generated (see GenerationMode above)
//                   random (default) means uniform in 0..2pi modulo optional spin effects
// - pdgid_radiation_mode
//                   how the radiation PDGid is generated (see GenerationMode above)
//                   random (default) means according to PDF*ME^2
// - i_splitting_end_mode
//                   how the splitter is selected (see GenerationMode above)
//                   random (default) follows the shower weight; for fixed values:
//                       1: split emitter
//                      -1: split spectator
//
// Note: for weighted events, when a specific splitting is requested
// (abs_pdgid or splitter), the corresponding fraction of the weight
// is included in the returned weight. E.g. if one explicitly asks
// for a gluon emission, the returned weight includes the probability
// for the emission to be a gluon. This is not included if the
// selection is done randomly.
//
// For unweighted emissions, phi, the radiated pdgid and the splitting
// end must all be chosen randomly, i.e. cannot be specified other
// than their default values.
EmissionAction ShowerRunner::do_one_emission(Event &event,
                                             std::unique_ptr<ShowerBase::EmissionInfo> &emission_info,
                                             bool weighted, double& weight,
                                             GenerationMode phi_mode,
                                             GenerationMode pdgid_radiation_mode,
                                             GenerationMode i_splitting_end_mode){
  // go through all the tests needed to compute the emission weight
  // and whether it is accepted or vetoed
  EmissionAction status = do_accept_emission(event, emission_info,
                                             weighted, weight,
                                             phi_mode, pdgid_radiation_mode, i_splitting_end_mode);
  if (status != EmissionAction::accept) return status;

  // actually do the emission and update whatever needs to be updated
  return do_process_emission(event, emission_info, weighted);
}


//-----------------------------------------------------------------
// code which checks if an emission is accepted or not until branching decisions have to be taken
// It takes as input:
// - event             the event to which we try adding an emission
// - emission_info     an emission info where lnv, lnb and
//                     the element have already been decided
// - weighted          true for weighted events (a pre-initialised
//                     weight is returned through "weight",
//                     false for unweighted events
// - weight            event weight (for "unweighted" generation
//                     this would still include potential veto weights)
//
// Once we have reordered the code, we will try to simplify this so
// that the structure is cleaner
// 
EmissionAction ShowerRunner::do_accept_emission(Event &event, std::unique_ptr<ShowerBase::EmissionInfo> &emission_info,
                                                bool weighted, double& global_weight,
                                                GenerationMode phi_mode,
                                                GenerationMode pdgid_radiation_mode,
                                                GenerationMode i_splitting_end_mode){
  // #TRK_ISSUE-537  uniformise the naming convention for "acceptance"
  //functions and variables
  
  // #TRK_ISSUE-???  review/uniformise cerr/assert/throw
  //----------------------------------------------------------------------
  // sanity checks before one starts
  //----------------------------------------------------------------------

  // impose random phi, radiated pdgid and splitting end for
  // unweighted emissions
  //
  // #TRK_ISSUE-533 we may want to relax this. In this case, one would
  //have to insert an acceptance probability (e.g. through
  //handle_acceptance) for enforced values (it should be
  //straightforward)
  if (!weighted){
    if ((phi_mode            !=GenerationRandom) ||
        (pdgid_radiation_mode!=GenerationRandom) ||
        (i_splitting_end_mode!=GenerationRandom)){
      std::cerr << "ShowerRunner::do_accept_emission: passing a specific (fixed) value phi, "
                   "the radiated pdgid, or the splitting end is not allowed for unweighted emissions" << std::endl;
      return EmissionAction::abort_event;
    }
  }
  // for now, we have tied the sum over the pdgid and over the splitter
  assert(! ((pdgid_radiation_mode == GenerationIntegrateOrSum) ^ (i_splitting_end_mode == GenerationIntegrateOrSum))
         && "Sum over only one of the PDGid and the splitting end is not implemented");

  // For the moment, spin correlations cannot be applied unless the
  // shower has do_kinematics_always_true_if_accept().
  //
  // The code handled spin by first assuming a flat (in phi,
  // spin-averaged) soft emission probability, then selecting spin
  // below in a loop. This generates the corect phi modulations.
  //
  // Imagine now that some of the phi domain is not allowed. The phi
  // generation of spin still requires to generate spin over the whole
  // range so as to get an average result that integrates to "2pi". If
  // the selected phi value is in a domain not allowed by the shower,
  // we would have to veto the emission _after_ the value of phi
  // including spin constraints has been drawn.
  //
  // For showers whose do_kinematics_always_true_if_accept() is false,
  // we are not guaranteed to be able to reconstruct the kinematics at
  // all values of phi, hence not able to follow the procedure of
  // imposing spin constraints (which requires a successive call to
  // do_kinematics) and later checking if the phi value is valid.
  //
  // For now, just throw an error
  if ((!_shower->do_kinematics_always_true_if_accept()) &&
      (_spin_cor_tree.do_spin_correlations())){
    throw runtime_error("ShowerRunner::do_accept_emission: the current spin implementation does not work with a shower for which do_kinematics_always_true_if_accept() if false");
  }    
    
  //----------------------------------------------------------------------
  // retrieve basic info
  //----------------------------------------------------------------------

  double lnv = emission_info->lnv;
  double lnb = emission_info->lnb;
  auto * element = emission_info->non_const_element();

  bool match_emission   = _match_current_emission(event, weighted);
  double ps_accept_prob = 0.;

  // range check
  if (!(element->lnb_generation_range(lnv).contains(lnb))){
    if (weighted) global_weight = 0;
    return EmissionAction::veto_emission;
  }

  //----------------------------------------------------------------------
  // helpers (i) to handle acceptance probabilities (ii) to reduce
  // typing for all kinds of returns.
  //----------------------------------------------------------------------

  // Introduce a helper to treat weighted actions.
  // This takes as arguments:
  //  - the weighted action to process
  //  - the emission weight to update (optionally)
  // and returns the emission action
  //
  // the way we treat weighted actions differs if we have unweighted
  // or weighted events.:
  // weighted  : accept with acceptance probability, otherwise veto according to "action"
  // unweighted: accept and reweigh the emission unless the acceptance probability is 0
  std::function<EmissionAction(const WeightedAction&,Weight&)> handle_weighted_action;
  if (weighted){
    // just reweighte with the acceptance probability
    handle_weighted_action = [&](const WeightedAction &acceptance, Weight &emission_weight) -> EmissionAction{
      auto status = acceptance.apply_reweighting(emission_weight);
      if (emission_weight==0) return EmissionAction::veto_emission;
      return status;
    };
  } else { // unweighted case
    handle_weighted_action = [&](const WeightedAction &acceptance, Weight &emission_weight) -> EmissionAction{
      return acceptance.accept(gsl);
    };
  }

  // comments to be added
#define CLEAN_AND_VETO(action) {  \
    if(weighted){weight=0;}       \
    _spin_cor_tree.wipe_cache();  \
    return action;                \
  }

#define VETO_WITH_PROBABILITY(weighted_action) {                             \
    EmissionAction action = handle_weighted_action(weighted_action, weight); \
    if (action != EmissionAction::accept) CLEAN_AND_VETO(action)             \
  }

  //--------------------------------------------------------------------
  // now on to the real work
  //
  // The logical workflow has 3 steps:
  //   1. initial checks
  //   2. Channel selection
  //   3. Phi selection (and spin) + following kinematic-dependent acceptances
  //
  // By default, these 3 steps are done in a linear sequence.
  // However, this is not systematically the case. For example, if one
  // requires an integration over phi (e.g. for the first Sudakov
  // integration), step 3 is inserted in a Gaussian quadrature
  // integration. If one requires a sum over flavour/splitter, split
  // 2&3 are included in the sum [NOT YET VALIDATED]
  //
  // We therefore locally code the 3 steps as internal lambda
  // functions so that we can later articulate them as needed. Note
  // that a reader can safely ignore this subtlety and read the code
  // below discarding the lambdas and the details of the articulation
  // at the end.
  //--------------------------------------------------------------------

  //--------------------------------------------------------------------
  // do_accept_initial_checks 
  //--------------------------------------------------------------------
  // - applies any pre-acceptance veto
  // - handles the alphas acceptance (if it is safe to do so)
  // - make the calls to calculate the acceptance probability
  // - runs checks on validity of the acceptance probability
  // - performs acceptance based on the probability (if unweighted, unmatched)
  auto do_accept_initial_checks = [&](Weight &weight) -> EmissionAction{
    IFPSVERBOSE(std::cout << "entering ShowerRunner::do_accept_initial_checks with weight = " << weight << std::endl;);

    // Allow potential veto of the emission before computation of acc. prob.
    if (_veto_ptr) VETO_WITH_PROBABILITY(_veto_ptr->pre_accept_prob_veto(event, *emission_info, weight));
      
    // running-coupling: the Sudakov uses the "maximal" value of the
    // running coupling. We correct for this by accepting the event
    // with a probability alphas(kt)/alphas_max
    // NB: if coupling does depend on z, postpone until we have info about z
    if (!_shower->coupling_depends_on_z()){
      assert(!_shower->has_alphas2_coeff());
      VETO_WITH_PROBABILITY(_alphas_acceptance_probability(element, emission_info.get()));
    } 

    // Apply accept-reject veto from crude to exact branching probability.
    ps_accept_prob = _acceptance_probability(element, emission_info.get());
    IFPSVERBOSE(_verbose_info_shower_acceptance(ps_accept_prob,event, emission_info.get()));

    // the handling of the acceptance probability depends on whether we are matching or not
    if (match_emission){  // matched emission
      // the acceptance probability is discarded but we still catch phase space boundaries
      if (! emission_info->acceptance_probability_was_true)
        CLEAN_AND_VETO(EmissionAction::veto_emission);
    } else { // no matching
      // include the alphas acceptance in case it depends on z. 
      if (_shower->coupling_depends_on_z())
        ps_accept_prob *= _alphas_acceptance_probability(element, emission_info.get());
      VETO_WITH_PROBABILITY(ps_accept_prob);
    }

    // make sure we have at least one valid phi value
    // past this point, shower with do_kinematics_always_true_if_accept
    // will not be allowed to fail calls to do_kinematics
    if (!element->check_after_acceptance_probability(emission_info.get()))
      CLEAN_AND_VETO(EmissionAction::veto_emission);

    // weighted events should include the lnv lnb density and the overhead factor.
    // For the max density, this compensate for a 1/max_density in the acceptance
    //   probability (or in the normalisation of the matching probability)  
    // For the overhead:
    // - wo matching, compensates a 1/_global_overhead_factor in _acceptqance_probabality
    // - w  matching, compensates a 1/_global_overhead_factor a bit below
    if (weighted)
      weight *= element->lnv_lnb_max_density() * _global_overhead_factor;

    return EmissionAction::accept;
  };

  
  //--------------------------------------------------------------------
  // do_accept_select_channel, selects the splitter and flavour of the radiation
  //--------------------------------------------------------------------
  auto do_accept_select_channel = [&](Weight &weight, int pdgid_radiation, int i_splitting_end) -> EmissionAction{
    // NOTE: at the moment, if the generation is NOT weighted, we have
    // imposed earlier that the radiation PDGId and the splitter
    // selection should be random. We could therefore just take the
    // selected flavour/channel weight and mulriply our "weight" by
    // it. For uniformity, we instead use a lazy acceptance
    //
    // #TRK_ISSUE-5411 GS-NEW-CODE: at the moment this uses the |pdgid|!!!
    // #TRK_ISSUE-XXX  GPS: not clear the procedure in this call is correct with
    //                 match_emission = true -- but it's also not clear it was correct before
    //                 i.e. without the match_emission flag.
    // #TRK_ISSUE-XXX GPS 2023-08-14: with match_emission == true, the
    // following call sets do_split_emitter, but nothing else.
    VETO_WITH_PROBABILITY(_select_channel_details(element, emission_info.get(), 
                                                  abs(pdgid_radiation),
                                                  i_splitting_end, match_emission));
    return EmissionAction::accept;
  };

  //--------------------------------------------------------------------
  // do_accept_kinematics 
  //--------------------------------------------------------------------
  // This involves several steps:
  //  - phi determination loop
  //    . determine phi (or take the requested one
  //    . construct the kinematic map (do_kinematics
  //    . handle the spin acceptance
  //  - apply double-soft corrections: deltaKCMW, real ME, potential swaps
  //  - matching acceptance (not that straightforward)
  //  - colour acceptance (no matching)
  //  - post split veto
  //  - matching vetoing (contour alignment)
  auto do_accept_kinematics= [&](Weight &weight, double phi_emission) -> EmissionAction{
    // by default we loop to find a suitable phi unless the
    // phi_generation argument allows us to take the value directly from
    // the emission info
    bool outcome, accept_phi;
    do {
      // generate a random phi
      emission_info->phi = (phi_emission<0) ? gsl.uniform(0, 2*M_PI) : phi_emission;
      IFPSVERBOSE(_verbose_info_phi_selection(event, emission_info.get()););

      // Construct the post-branching momenta and store in emission_info
      outcome = _do_kinematics(element, emission_info.get());
      IFPSVERBOSE(_verbose_info_do_kinematics(event, emission_info.get()););

      // with matching, we do not need anything else (the spin tree is updated later)
      if (match_emission) break;

      // get the spin acceptance as a pair <weight>/<overhead>. For
      // fixed phi, the overhead should be discarded
      accept_phi = (handle_weighted_action(_spin_acceptance(element, emission_info.get(), phi_emission<0), weight) == EmissionAction::accept);
    } while (!(accept_phi || (phi_emission>=0)));

    // discard the emission if the selected phi returned false for
    // do_kinematics (see GS-NOTE comment above)
    if (!outcome) CLEAN_AND_VETO(EmissionAction::veto_emission);

    //--------------------------------------------------
    // handle the double-soft corrections
    // get delta K_CMW
    VETO_WITH_PROBABILITY(_double_soft_dKCMW_acceptance(element, emission_info.get()));
     
    if (_shower->double_soft()){
      // overall double soft acceptance probability
      VETO_WITH_PROBABILITY(_double_soft_ME_acceptance(element, emission_info.get()));
      // potential swaps
      _double_soft_swaps(element, emission_info.get(), weight);
    } else { // no double soft corrections
      emission_info->colour_flow_swap = false;
    }

    // ME/PS acceptance probability and associated accept/reject for matching.
    if (match_emission) {
      // get the matching acceptance probability and apply it
      bool mcatnlo_matching, discard_emission;
      Weight matching_acceptance = _matching_acceptance_probability(event, element, emission_info.get(), weighted,
                                                                    mcatnlo_matching, discard_emission);
      if (discard_emission)  CLEAN_AND_VETO(EmissionAction::veto_emission);
      if (!mcatnlo_matching) VETO_WITH_PROBABILITY(matching_acceptance);
    
      // update the channel selection, colour acceptance and spin tree
      double colour_acceptance = _matching_post_acceptance_update(event, element, emission_info.get());
      
      // now handle the MC@NLO case
      if (mcatnlo_matching){
        assert(weighted);
        double acc = _matching_finalize_mcatnlo(element, emission_info.get(),
                                                ps_accept_prob, matching_acceptance, colour_acceptance);
        VETO_WITH_PROBABILITY(acc);
      }
    } else {  // no matching
      // Apply sub-leading Nc corrections (already done in case of matching)
      double colour_acceptance = (_use_first_sudakov_for_colour)
        ? _colour_transition_runner->colour_factor_acceptance_first_sudakov(event, *element, *emission_info)
        : _colour_transition_runner->colour_factor_acceptance(event, *element, *emission_info);
      VETO_WITH_PROBABILITY(colour_acceptance);
    }

    // #TRK_ISSUE-548
    // - can we swap the veto w the matching veto?
    if (!_post_matching_contour_veto(match_emission, emission_info.get()))
      CLEAN_AND_VETO(EmissionAction::veto_emission);
    
    // Allow potential veto of the emission after full kinematic construction
    if (_veto_ptr) VETO_WITH_PROBABILITY(_veto_ptr->post_do_split_veto(event, *emission_info,
                                                                       weight, _shower->is_global()));
 
    return EmissionAction::accept;
  };


  //--------------------------------------------------------------------
  // now just articulate things
  //--------------------------------------------------------------------
   
  EmissionAction status;

  // we always start with the initial checks
  Weight emission_weight = Weight::exact_one;
  status = do_accept_initial_checks(emission_weight);
  if (status != EmissionAction::accept){ global_weight *= emission_weight; return status;}

  // We are left with 
  //    (i) selecting the channel                               [do_accept_select_channel]
  //   (ii) selecting phi and finalising the acceptance process [do_accept_kinematics]
  // This procedure depends on whether we just take a single value or sum/integrate.
  //
  // We start by building a helper for the internal part (do_accept_kinematics)
  // In a nutshell: if we integrate over phi, do a Gaussian integration
  //                if we have a single phi value, just call do_accept_kinematics
  // Note: when integrating, EmissionVetoed and AbortEvent are considered as 0-weight;

  // introduce a helper which works transparently for a single phi
  // value or an integration over phi
  std::function<EmissionAction(Weight&)> accept_kinematics_wrapper;
  if (phi_mode != GenerationIntegrateOrSum){ // simple case of a single value
    accept_kinematics_wrapper = [&](Weight& weight) -> EmissionAction{
      double phi = (phi_mode == GenerationRandom) ? -1 : emission_info->phi;
      return do_accept_kinematics(weight, phi);
    };
  } else {    // more involved case where we need to integrate
    assert(weighted);
    // the integrand is meant to be integrated over so can return a double
    auto phi_integrand = [&](double phi) -> double{
      Weight local_weight = 1;
      EmissionAction local_status = do_accept_kinematics(local_weight, phi);
      return (local_status==EmissionAction::accept) ? (double) local_weight : 0.0;
    };
    accept_kinematics_wrapper = [&](Weight& weight) -> EmissionAction{
      weight *= dgauss(0.0, 2*M_PI, phi_integrand, 1e-8, 16, 32)/(2*M_PI);
      return (weight==0) ? EmissionAction::veto_emission : EmissionAction::accept;
    };
  }
  
  // Now we can decide what to do with the channel selection
  
  // Finally, we can decide whether we sum over channel/pdgid or just
  // take a single value.
  //
  // For now, let's simplify things a little bit and assume that if
  // the user requests a sum over the pdgid, they also request a sum
  // over the splitter index.
  //
  // We catch potential AbortEvent 
  if (i_splitting_end_mode==GenerationIntegrateOrSum) assert(pdgid_radiation_mode==GenerationIntegrateOrSum);
  if (pdgid_radiation_mode==GenerationIntegrateOrSum) assert(i_splitting_end_mode==GenerationIntegrateOrSum);

  // first treat the "standard" case whereby we generate a single emission (weighted or not)
  if (i_splitting_end_mode!=GenerationIntegrateOrSum){  // simple case of a single value
    // get the channel arguments
    int ipdgid = (pdgid_radiation_mode==GenerationRandom) ? 0 :  emission_info->radiation_pdgid;
    int isplit = (i_splitting_end_mode==GenerationRandom) ? 0 : (emission_info->do_split_emitter ? 1 : -1);
    // try this channel selection
    status = do_accept_select_channel(emission_weight, ipdgid, isplit);
    if (status != EmissionAction::accept){ global_weight *= emission_weight; return status;}
    // do the kinematic acceptance (with potential phi integration)
    status = accept_kinematics_wrapper(emission_weight);
    global_weight *= emission_weight;
    return status;
  }

  // more involved case where we have to sum/integrate (for Sudakov integration)
  assert(weighted);
          
  // as above start with a helper to handle a single value of splitter/pdgid
  auto accept_single_value = [&](int pdgid_radiation, int i_splitting_end, Weight &weight) -> EmissionAction{
    // do the channel selection
    EmissionAction local_status = do_accept_select_channel(weight, pdgid_radiation, i_splitting_end);
    return (local_status == EmissionAction::accept)
      ? accept_kinematics_wrapper(weight) : local_status;
  };

  vector<int> summed_pdgids = {21}, splitter_indices = {1};
  for (unsigned int i=1;i<=(unsigned int)round(_shower->qcd().nf());++i)
    summed_pdgids.push_back(i);
  if (!_shower->only_emitter_splits()) splitter_indices.push_back(-1);
  double summed_weight = 0.0;
  for (const auto ipdgid : summed_pdgids){
    for (const auto isplit : splitter_indices){
      Weight local_weight = Weight::exact_one;
      accept_single_value(ipdgid, isplit, local_weight);
      summed_weight += local_weight;
    }
  }
  global_weight *= summed_weight;
  return (global_weight==0) ? EmissionAction::veto_emission : EmissionAction::accept;

}


// process the emission: update event, etc...
EmissionAction ShowerRunner::do_process_emission(Event &event, std::unique_ptr<ShowerBase::EmissionInfo> &emission_info, bool weighted){
  double lnv = emission_info->lnv;
  auto * element = emission_info->non_const_element();

  bool match_emission = _match_current_emission(event, weighted);

  // Allow a potential veto to do some maintenance before we update the event
  if (_veto_ptr)
    _veto_ptr->post_emission_acceptance(event, *emission_info);

  // Update the event 
  _update_event(event, element, emission_info.get());

  // Add the new splitting to the tree, and recompute decay matrices
  // Doesn't do anything if spin correlations are turned off
  // #TRK_ISSUE-550 GS-NEW-CODE: this could probably easily be moved into _update_event
  _spin_cor_tree.update_tree(*emission_info, *element, event);
  // record splitting info in the spin correlation tree if needed
  if (_spin_cor_tree.do_spin_correlations() && _spin_cor_tree.do_declustering_analysis()) {
    _spin_cor_tree.do_analysis_step(*emission_info, *element, event);
  }

  // Update the splitter indices in the matching
  if(_matching_ptr && _matching_ptr->matching_needs_vetoing())
    _matching_ptr->update_splitter_index(*emission_info, event);

  // update cache post-splitting
  _update_elements_in_store_post_splitting(event, lnv, emission_info.get());

  // After the first emission has been matched, revert to normal showering
  if (match_emission){
    _terminate_matching_for_event(event, emission_info.get());
    
    // Replace emission_info by the correct type (main shower's class instead
    // of the shower_for_matching one)
    // #TRK_ISSUE-5561 GS-NEW-CODE: can we move the emission info, overheads in _initialise_run?
    double cached_lnb = emission_info->lnb;
    emission_info = 
      std::unique_ptr<ShowerBase::EmissionInfo>(_shower->create_emission_info());
    // set these for diagnostic purposes (e.g. lnbfirst), keeping in mind
    // that they may not apply to the new shower
    emission_info-> lnv = lnv;
    emission_info-> lnb = cached_lnb;
    // The shower may have changed. To be on the safe side, also update the overheads
    _initialise_overhead_factors();
  }

  return EmissionAction::accept;
}


//-----------------------------------------------------------------
// Compute the Sudakov form factor describing the no-emission
// probability between the hard scale lnv_max and a scale lnv_min
// #TRK_ISSUE-628  WARNING: this routine in its current form does not work for
// the global recoil [to be fixed]
ShowerRunner::Sudakov ShowerRunner::compute_first_sudakov(Event & event, double lnv_max, double lnv_min,
                                                          uint64_t nev,
                                                          double precision) {

  // Use the matching shower to compute the Sudakov
  if (_matching_ptr) {
    _shower = _shower_for_matching;
  }

  // Initialise storage of elements, and cached info
  _initialise_run(event, lnv_max); 
  
  // In the present context lnvs are generated in-house by numerical integration
  // machinery rather than any _find_element_* function. It is then essential
  // any find_element_alg_bias in any MatchedProcess is temporarily overwritten
  // s.t. the return values of _matchedPtr->acceptance_probability are consistent
  // with the latter generation. This is true here no matter what
  // _find_element_alg_bias is used/needed in the actual event generation.
  double original_find_element_alg_bias = 0.;
  if (_matching_ptr){
    original_find_element_alg_bias = _matching_ptr->find_element_alg_bias();
    _matching_ptr->set_find_element_alg_bias(0.);
  }
  
  // build a single vector with all elements that appear in the
  // initial dipoles
  std::vector<std::unique_ptr<typename ShowerBase::Element> > elements;
  for (unsigned idip = 0; idip < event.dipoles().size(); ++idip) {
    auto dipole_elements = _shower->elements(event, idip);
    std::move(dipole_elements.begin(), dipole_elements.end(), std::back_inserter(elements));
  }
  
  // #TRK_ISSUE-629  this code is only suitable for showers where the following
  // is true
  //assert(_shower->do_kinematics_always_true_if_accept());

  double lnsudakov = 0.0;
  double lnsudakov_err = 0.0;
  if ((_integration_strategy==GQ) || (_integration_strategy==GQ3)) {
    if ((event.is_pp()) && (_integration_strategy==GQ)){
      cerr << "============================================================" << endl
           << "                        WARNING                             " << endl
           << ""                                                             << endl
           << " Using a Gaussian Quadrature Sudakov integation without phi " << endl
           << " integration can be dangerous for pp events                 " << endl
           << "============================================================" << endl;
    }
    assert(_shower->beta()>=0 && "Gaussian Sudakov integration not valid for β_ps<0 showers.");

    bool integrate_phi = (_integration_strategy==GQ3);
    
    // Integrate the Sudakov exponent for each element in the event
    for (auto & element: elements) {
      lnsudakov += _integrate_over_lnv_gauss(event, element.get(), lnv_min, lnv_max, precision, integrate_phi);
      // a rough estimate of the lnsudakov error
      lnsudakov_err = precision;  
    }
  } else if(_integration_strategy==MC) {
    _integrate_over_lnv_mc(event, elements, lnv_min, lnv_max, nev, lnsudakov, lnsudakov_err) ;    
  } else {
    std::cerr << "Unrecognised IntegrationStrategy: " << _integration_strategy << "\n";
    exit(-1);
  }

  // If we reset the find_element_alg_bias at the start of this function then
  // we restore it back to its original value here.
  if( _matching_ptr ) _matching_ptr->set_find_element_alg_bias(original_find_element_alg_bias);

  return Sudakov(lnv_min, exp(-lnsudakov), exp(-lnsudakov) * lnsudakov_err);
}


//-----------------------------------------------------------------
// Generate the first emission uniformly between lnv_max and lnv_min
// and restore the correct probability distribution via reweighting
// nev = number of events to evaluate the corresponding Sudakov.
// This implementation follows the logbook 2019-03-08-weighted-first-generation
//typename ShowerRunner<Shower>::Sudakov
ShowerRunner::Sudakov ShowerRunner::generate_weighted_first_emission(
        Event & event, double lnv_min, std::unique_ptr<ShowerBase::EmissionInfo> & emission_info) {
  
  double lnv;

  unsigned int n = _sudakov_array.size(); // shorthand
  if (n<2) throw std::runtime_error("We need at least two bins for weighted generation to be sensible");
  double weight;
  unsigned int index_lnv_start;

  if (_sudakov_array_is_uniform) {
    // first factor in weight from logbook/2019-03-08-weighted-first-generation
    weight = _sudakov_array.size();
    // choose lnv[i] (log of the upper scale) uniformly
    index_lnv_start = gsl.uniform_int(_sudakov_array.size());
  } else {
    // alternative when bins are spaced uniformly in ln(sudakov)
    // cf. section 1.3 of logbook 2019-03-08-weighted-first-generation
    // put in a protection that the index should not be greater than n-1
    double r = gsl.uniform();
    // Note the explicit conversion to avoid a -Wfloat-conversion warning
    index_lnv_start = std::min( n-1, (unsigned int) std::floor(n*pow2(r)) );
    // the probability that we choose a given i is
    double p_i = sqrt((index_lnv_start+1.0)/n) - sqrt((index_lnv_start+0.0)/n);
    weight = 1.0/p_i;
  }
  
  // special action if we hit the last element of _sudakov_array,
  // which corresponds to the IR cutoff
  if (index_lnv_start == _sudakov_array.size() - 1) {
    // the lowest bin gets a weight equal to the Sudakov
    auto & sudakov = _sudakov_array[index_lnv_start];
    weight *= sudakov.value;
    lnv = -std::numeric_limits<double>::max();
    return Sudakov(lnv, weight);
  }
  
  // update weight as in Eq. (7) of 2019-03-08-weighted-first-generation
  weight *= _sudakov_array[index_lnv_start].value - _sudakov_array[index_lnv_start+1].value;

  // if the weight is zero (e.g. because we selected a region that has
  // zero density), set lnv to its lowest possible value so as to avoid
  // any extra generation (it doesn't matter what we do in terms of
  // generation given that the event will be ignored in analyses, by
  // virtue of the zero weight; but setting it to the lowest value
  // will avoid wasting time generating something that is essentially
  // nonsense).
  if (weight == 0) {
    lnv = -std::numeric_limits<double>::max();
    return Sudakov(lnv, weight);
  }

  double lnv_start = _sudakov_array[index_lnv_start].lnv;
  lnv = lnv_start;

  // #TRK_ISSUE-648 the code below and run are the same except for the interface 
  // (this takes an emission info ptr, run does not). Several options are possible:
  // 1. if we can do wo the emission_info argument, just call run for 1 emission
  // 2. can we have run and this share some cummon fct
  while (true) {
    typename ShowerBase::Element * element = (this->*_find_element_alg_ptr)(event, lnv);

    // veto emissions below lnv[i+1] (log of the lower scale)
    if (lnv < _sudakov_array[index_lnv_start+1].lnv) {
      lnv = lnv_start;
      continue;
    }

    // handle cache updates prior to dipole splitting (typically
    // astar!=a updates for strict approaches)
    _update_elements_in_store_pre_splitting(event, element, lnv);
    
    // generate lnb variable:
    // no jacobian needed since the generation is done according to the Sudakov
    double lnb;
    bool lnb_was_generated = (this->*_find_lnb_alg_ptr)(element, lnv, lnb);
    if (!lnb_was_generated) continue;

    emission_info->set_element(element);
    emission_info->lnv = lnv;
    emission_info->lnb = lnb;

    // proceed by doing the emission
    //                                                         unweighted
    EmissionAction status = do_one_emission(event, emission_info, false, weight);
    if (status == EmissionAction::abort_event) return Sudakov(lnv, 0.0);
    if (status == EmissionAction::accept)      return Sudakov(lnv, weight);
  }
}


//========================================================================
// 
// Additional ways to generate emissions
// 
//========================================================================

//-----------------------------------------------------------------
// generates a tree-level (fixed-order) event with n_emsn emissions;
// returns the weight associated with that event
double ShowerRunner::tree_level(Event & event, double lnv_max, double lnv_min, int n_emsn) {
  double weight = 1.0;
  do_n_real_emissions(event, lnv_max, lnv_min, n_emsn, weight);
  return weight;
}

//-----------------------------------------------------------------
// returns a weight factor associated with a first order contribution
double ShowerRunner::first_order(Event & event, double lnv_max, double lnv_min) {
  return tree_level(event, lnv_max, lnv_min, 1);
}
 
//-----------------------------------------------------------------
// returns a weight factor associated with a second order contribution
double ShowerRunner::second_order(Event & event, double lnv_max, double lnv_min) {
  return tree_level(event, lnv_max, lnv_min, 2);
}

// Produces one emission with a fixed lnv
// - event          event to read from (and write to)
// - lnv            fixed value of lnv for the emission
// - emission_info  an emission info where lnv has already been decided
// - weight         event weight (should be passed initialised and
//                  will be returned updated)
// - lnb_range      (optional) user-defined lnb range
//                  the appropriate 1/size_of_range factor is included in the weight
// - ielement       (optional) force the index of the element to split
//                  -1 means random (thereby including a factor nelements in the weight)
// this returns the emission status
EmissionAction ShowerRunner::do_one_real_emission_with_fixed_lnv(
    Event & event, double lnv, std::unique_ptr<ShowerBase::EmissionInfo> &emission_info,
    double &weight, Range lnb_range, int ielement){

  // If the user doesn't choose an element, sample it [ielement == -1, the default]

  int nelements = _elements.size();
  if (ielement < 0){
    ielement = gsl.uniform_int(nelements);
    // #TRK_ISSUE-5271 GS-NEW-CODE: before, this was done even if ielement was
    //specified which, I think, was wrong.
    weight *= nelements;
  }
  
  assert(ielement < nelements);
  auto element = _elements[ielement].get();

  // apply pre-splitting updates (only affect cases w vetoes)
  // #TRK_ISSUE-5272 GS-NEW-CODE: can this be moved in do_one_emission?
  _update_elements_in_store_pre_splitting(event, element, lnv);

  // Option to use a restricted lnb_range passed by the user
  // (=Range(0,0) by default)
  if (lnb_range.extent() == 0) {
    lnb_range = element->lnb_generation_range(lnv);
  }
  double lnb = gsl.uniform(lnb_range.min(), lnb_range.max());
  weight *= lnb_range.extent();

  emission_info->set_element(element);
  emission_info->lnv = lnv;
  emission_info->lnb = lnb;

  // proceed by doing the emission ....      weighted
  return do_one_emission(event, emission_info, true, weight);
}


//----------------------------------------------------------------------
// Generate an array of Sudakov form factors at equally spaced
// values of the (logarithm of the) evolution variable lnv
// between lnv_max and lnv_min
void ShowerRunner::cache_sudakov_values_uniform_lnv(Event & event,
                                                    double lnv_max, double lnv_min,
                                                    unsigned int npoints, uint64_t nev) {
  _sudakov_array_is_uniform = true;
  double lnv;
  
  for (unsigned int i = 0; i < npoints; i++) {
    lnv = lnv_max - (lnv_max - lnv_min)/(npoints - 1.) * i;
    // It is likely better to calculate between subsequent lnv values
    // rather than always from the max.  Effectively when calculating
    // this for some large -ve value of lnv, the region between
    // lnv_max and lnv_max-1 will get nev/(lnv_max) points for the
    // integral, whereas earlier in the loop one has already
    // calculated it with nev points.
    if (lnv < lnv_max) {
      Sudakov sudakov = compute_first_sudakov(event, lnv_max, lnv, nev);
      _sudakov_array.push_back(sudakov);
    } else {
      _sudakov_array.push_back(Sudakov(lnv, 1.0));
    }
  }
}

//========================================================================
//
// Deprecated interfaces meant mostly for backwards compatibility
//
//========================================================================
 
//-----------------------------------------------------------------
// version with extra arguments, currently unsupported
// deprecated: Use the above version of first_order instead
double ShowerRunner::first_order(Event & event, double lnv_max, double lnv_min, 
                                 const std::vector<Injection> & injections,
                                 double lnb_window, bool g2qqbar) {
  assert(lnb_window == -1); // this argument is not currently used

  if (injections.size()){
    assert("The injection mechanism for first_order is currently not working (following the merge of thenew code from the master branch. One strategy would be to have some mechanish that first goes through the list of injections and then calls tree_level or do_n_real_emssions (which would then need a continuation flag)" && false); 
    return 0.0; 
  }

  return tree_level(event, lnv_max, lnv_min, 1);
}

//-----------------------------------------------------------------
// version with extra arguments, currently unsupported
// deprecated: Use the above version of second_order instead
double ShowerRunner::second_order(Event & event, double lnv_max, double lnv_min, 
                                  const vector<Injection> & injections, 
                                  double lnb_window, bool g2qqbar) {
  assert(lnb_window == -1); // this argument is not currently used

  if (injections.size()){
    assert("The injection mechanism for second_order is currently not working (following the merge of thenew code from the master branch. One strategy would be to have some mechanish that first goes through the list of injections and then calls tree_level or do_n_real_emssions (which would then need a continuation flag)" && false); 
    return 0.0;
  }

  return tree_level(event, lnv_max, lnv_min, 2);
}
  
//-----------------------------------------------------------------
// Produces one emission with a fixed lnv, lnb, phi
// - event             event to read from (and write to)
// - i_element         index of the element to split
// - lnv               fixed value of lnv for the emission
// - lnb               fixed value of lnb for the emission
// - phi               the azimuth for the emission (phi is supposed to be in [0,2*pi])
// - pdgid_radiation   the pdgid of the radiation (0 = select as normal)
// - i_splitting_end   determines which dipole end splits (0 = select as normal, 1 = emitter, -1 = spectator)
double ShowerRunner::do_one_real_emission_with_fixed_lnv_lnb(Event & event, unsigned int i_element,
                                                             double lnv, double lnb,
                                                             double phi,
                                                             int pdgid_radiation,
                                                             int i_splitting_end){
  std::unique_ptr<ShowerBase::EmissionInfo> emission_info(_shower->create_emission_info());
  return do_one_real_emission_with_fixed_lnv_lnb(event, emission_info, i_element, lnv, lnb, phi,
                                                 pdgid_radiation, i_splitting_end);
}
  
double ShowerRunner::do_one_real_emission_with_fixed_lnv_lnb(Event & event, std::unique_ptr<ShowerBase::EmissionInfo> &emission_info, unsigned int i_element,
                                                             double lnv, double lnb,
                                                             double phi,
                                                             int pdgid_radiation,
                                                             int i_splitting_end){
  double weight  = 1.0;

  auto element = _elements[i_element].get();

  // apply pre-splitting updates (only affect cases w vetoes)
  // #TRK_ISSUE-5273 GS-NEW-CODE: can this be moved in do_one_emission?
  _update_elements_in_store_pre_splitting(event, element, lnv);

  emission_info->set_element(element);
  emission_info->lnv = lnv;
  emission_info->lnb = lnb;

  // fix the constrained parts in emission info
  GenerationMode phi_mode = GenerationRandom;
  if (phi>=0){
    emission_info->phi = phi;
    phi_mode = GenerationFromEmissionInfo;
  }

  GenerationMode pdgid_mode = GenerationRandom;
  if (pdgid_radiation!=0){
    emission_info->radiation_pdgid = pdgid_radiation;
    pdgid_mode = GenerationFromEmissionInfo;
  }
  
  GenerationMode split_mode = GenerationRandom;
  if (i_splitting_end!=0){
    emission_info->do_split_emitter = (i_splitting_end==1);
    split_mode = GenerationFromEmissionInfo;
  }

  EmissionAction status = do_one_emission(event, emission_info, true, weight,
                                          phi_mode, pdgid_mode, split_mode);
  return (status==EmissionAction::accept) ? weight : 0.0;
}


//GS-NEW-CODE: until we have settled on proper naming for the
//1-emission core function, keep this one as a wrapper to
//do_one_real_emission_with_fixed_lnv_lnb. The only caveat is that
//the option of passing an emission_info ptr would not work
double ShowerRunner::do_one_emission_with_fixed_lnv_lnb(Event &event, unsigned int i_element,
                                                        double lnv, double lnb, double phi,
                                                        int pdg_id_radiation, int i_splitting_end,
                                                        ShowerBase::EmissionInfo *emission_info_arg){
  // switch to the new interface
  assert((emission_info_arg==nullptr) && "Calls to ShowerRunner::do_one_emission_with_fixed_lnv_lnb with a non-null emission_info_ptr should switch to the new interface using a unique_ptr instead");
    
  return do_one_real_emission_with_fixed_lnv_lnb(event, i_element, lnv, lnb, phi, pdg_id_radiation, i_splitting_end);
}
  

//========================================================================
// 
// Shower inversion and related tools
// 
//========================================================================


//-----------------------------------------------------------------
// Find lnv, lnb, phi from z, delta, phi in the collinear approximation
// and returns the Jacobian dz ddelta dphi = |J| dlnv dlnb dphi
//
// z_target, delta_target, phi_target are the polar coordinates of the
// emission "k" in the event frame; with z_target an energy fraction
// and phi_target==0 corresponding to the x axis
double ShowerRunner::find_lnv_lnb_phi_from_z_delta_phi_collinear(Event &event, int i_element, bool emitter_splits,
  double z_target, double delta_target, double phi_target, double &lnv, double &lnb, double &phi) {

  bool is_panlocal = dynamic_cast<ShowerPanScaleLocal*>(_shower.get())   || 
                     dynamic_cast<ShowerPanScaleLocalpp*>(_shower.get()) ||
                     dynamic_cast<ShowerPanScaleLocalDIS*>(_shower.get());
  bool is_panglobal = dynamic_cast<ShowerPanScaleGlobal*>(_shower.get())   ||
                      dynamic_cast<ShowerPanScaleGlobalpp*>(_shower.get()) || 
                      dynamic_cast<ShowerPanScaleGlobalDIS*>(_shower.get()) ;
  assert( ( is_panlocal || is_panglobal ) 
            && "Can only do PanGlobal and PanLocal dipole (not antenna)");
  //
  // in the collinear limit we have with the dipole pitilde, pjtilde
  // pitilde = Eitilde(1,0,0,1)
  // pjtilde = Ejtilde(1,0,0,-1)
  // take pitilde = emitter
  // 
  // collinear limit says:
  // pi = (1-z)pitilde + kT + b * n_T
  // pk = z pitilde - kT + c * n_T
  // nT is a vector perpendicular to kT
  // which we write in the frame above as
  // pi = (1-z)Eitilde(1,0,0,1)
  // pk = z*Eitilde(1, sin(delta_target)cos(phi_target), sin(delta_target)sin(phi_target), cos(delta_target))
  // we need pi.pk = Ei Ek (1 - cos delta_target) 
  //               = z(1-z) Eitilde^2 (1 - cos delta_target)
  // 
  // in the actual map we have 
  // pi = ai * pitilde + kT' + bi * pjtilde
  // pk = ak * pitilde - kT' + bk * pjtilde
  // ---> ak = z
  // ---> delta_target with ak determines bk
  //

  auto& element = _elements[i_element];

  double beta = dynamic_cast<ShowerPanScaleBase*>(_shower.get())->beta();

  Momentum Q = event.Q();
  precision_type Q2 = event.Q2();
  precision_type sij = 2*dot_product(element->emitter(), element->spectator());
  precision_type si  = 2*dot_product(element->emitter(), Q);
  precision_type sj  = 2*dot_product(element->spectator(), Q);
  precision_type rho = pow(si*sj/Q2/sij, beta/2);

  // Avoid numerical cancellations in small-angle region
  precision_type omcosdelta_target = 1 - cos(delta_target);
  if (delta_target < 1e-7) {
    omcosdelta_target = delta_target*delta_target/2;
  }

  // Compute ak and bk
  precision_type alphak=0, betak=0;
  if (is_panlocal) {
    // pi = ai pitilde + bi pjtilde \pm kT
    // pj = bj pjtilde
    // pk = ak * pitilde + bk * pjtilde + kT
    assert(emitter_splits);
    alphak = element->emitter().is_initial_state() ? z_target/(1-z_target) : z_target;
    if (element->emitter().is_initial_state() && element->spectator().is_initial_state()) { //< II dipole
      betak = 2*omcosdelta_target*element->emitter().E()*element->emitter().E()/sij * alphak*(1+alphak)*(1+alphak) / ( (alphak*alphak + (1+alphak)*(1+alphak))*pow(1+alphak, 2/(1+beta)) - alphak*(1+alphak) );
    } else if (element->emitter().is_initial_state()) { //<IF dipole
      betak = 2*omcosdelta_target*element->emitter().E()*element->emitter().E()/sij * alphak*pow2(1+alphak) / pow((1 + alphak), 2/(1+beta));
    } else { //< FF and FI dipoles
      // ak = alphak, bk = betak
      betak = 2*(1-alphak)*(1-alphak)*alphak*omcosdelta_target*element->emitter().E()*element->emitter().E()/sij;
    }
    
  }
  else if (is_panglobal) {
    // pi = (1-alphak) pitilde
    // pj = (1-betak)  pjtilde
    // pk = alphak * pitilde + betak * pjtilde + kT
    //
    // we take the explicit parameterisation [this is in event frame]
    // event frame because Ei =/= Ej
    // we may rotate the dipole such that the emitter is along the z axis
    // pitilde = Ei(1,0,0,+1)
    // pjtilde = Ej(1,x,y,-1) (x & y don't matter)
    // pk = Ek(1, cos[phi]sin[t], sin[phi]sin[t], cos[t]) (t = Delta_target)
    // -> 2pitilde.pk = 2 Ei Ek   (1 - cos[t])
    //           = 2 ak Ei^2 (1 - cos[t]) (ignore the betak dependence in Ek)
    // and from mapping = 2 pitilde.pk = bk sij
    // -> bk = 2 ak Ei^2 (1-cos[t])/sij
    // also betak = bk, alphak = ak
    // if instead the spectator is the splitter, the roles of alphak & betak are turned around
    if (emitter_splits) {
      alphak = element->emitter().is_initial_state() ? z_target/(1-z_target) : z_target;
      betak  = 2*alphak*omcosdelta_target*element->emitter().E()*element->emitter().E()/sij;
    }
    else {
      betak = element->spectator().is_initial_state() ? z_target/(1-z_target) : z_target;
      alphak = 2*betak*omcosdelta_target*element->spectator().E()*element->spectator().E()/sij;
    }
  } else {
    assert(false && "find_lnv_lnb_phi_from_z_delta_phi_collinear only implemented for the PanLocal and PanGlobal showers");
  }

  lnb = to_double(0.5*log(si*alphak/sj/betak));
  lnv = to_double(log(sqrt(alphak*betak*sij)/rho) - _shower.get()->beta()*fabs(lnb));

  // Translate from phase space phi to shower phi
  Momentum kperp_1, kperp_2;
  Momentum kperp_shower_1, kperp_shower_2;
  Momentum ref(1,0,0,1);

  // get the perpendicular vector kT in the frame where the emitter is along z axis
  // kT = (sin(phi_target) * kperp_1  + cos(phi_target) * kperp_2)*|k_T|
  // |kT| = z emitter_E * (sin(delta_target))
  if (emitter_splits) {
    two_perp(element->emitter(),   ref, kperp_1, kperp_2);
    // #TRK_ISSUE-610  check
    // std::cout << "kT component: " << alphak * element->emitter().E() * sin(delta_target) * (precision_type(sin(phi_target)) * kperp_1  + precision_type(cos(phi_target)) * kperp_2) << std::endl;
  } else {
    two_perp(element->spectator(), ref, kperp_1, kperp_2);
    // check
    // std::cout << "kT component: " << betak * element->spectator().E() * sin(delta_target) * (precision_type(sin(phi_target)) * kperp_1  + precision_type(cos(phi_target)) * kperp_2) << std::endl;
  }

  // now get the kT as constructed in the shower (from the dipole legs)
  two_perp(element->emitter(), element->spectator(), kperp_shower_1, kperp_shower_2);
  // kT_shower = (sinphi * kperp_shower_1  + cosphi * kperp_shower_2)*|kT'|
  // kT.kperp_shower_1 = (sin(phi_target) * kperp_1.kperp_shower_1 + cos(phi_target) * kperp_2.kperp_shower_1)
  // kT.kperp_shower_2 = (sin(phi_target) * kperp_1.kperp_shower_2 + cos(phi_target) * kperp_2.kperp_shower_2)
  // kT_shower.k_perp_shower_1 = -sinphi (since kperp^2 = -1, kperpi.kperpj = 0)
  // kT_shower.k_perp_shower_2 = -cosphi
  // taking kT = kT_shower (collinear splitting) we find:
  double sinphi = -sin(phi_target)*to_double(dot_product(kperp_shower_1, kperp_1)) - cos(phi_target)*to_double(dot_product(kperp_shower_1, kperp_2));
  double cosphi = -sin(phi_target)*to_double(dot_product(kperp_shower_2, kperp_1)) - cos(phi_target)*to_double(dot_product(kperp_shower_2, kperp_2));
  // 
  if (sinphi > 0) {
    // returns in the interval -pi and +pi
    phi = atan2(sinphi, cosphi);
  } else {
    // make sure phi is from 0 to 2pi
    phi = element->emitter().is_initial_state() ? (atan2(-sinphi, -cosphi) + M_PI) : (atan2(-sinphi, cosphi) + M_PI);
  }

  // Return the small-angle approximation of jacobian
  if (emitter_splits) return to_double(alphak)*delta_target;
  else                return to_double(betak) *delta_target;
}


//-----------------------------------------------------------------
// Find lnv, lnb, phi from z, delta, phi in the soft approximation
// and returns the Jacobian dz ddelta dphi = |J| dlnv dlnb dphi
double ShowerRunner::find_lnv_lnb_phi_from_z_delta_phi_soft(Event &event, int i_element,
  double z_target, double delta_target, double phi_target, double &lnv, double &lnb, double &phi) {

  assert( ( dynamic_cast<ShowerPanScaleLocal*>(_shower.get()) || 
            dynamic_cast<ShowerPanScaleGlobal*>(_shower.get()) ||
            dynamic_cast<ShowerPanScaleLocalpp*>(_shower.get()) ||
            dynamic_cast<ShowerPanScaleGlobalpp*>(_shower.get()) ) 
            && "Can only do PanGlobal and PanLocal");

  auto& element = _elements[i_element];
  
  // check we are dealing with a final-state shower. This is relevant because
  // the perpendicular component is assigned with a relative - sign to the pp showers
  bool final_state_shower = dynamic_cast<ShowerPanScaleLocal*>(_shower.get()) || dynamic_cast<ShowerPanScaleGlobal*>(_shower.get());
  
  // get the emitter and spectator
  Momentum & emitter   = element->emitter();
  Momentum & spectator = element->spectator();
  // get the perp components
  Momentum perp1, perp2;
  two_perp(emitter, spectator, perp1, perp2);

  // the radiation is constructed in the event frame
  // the radiation is soft compared to the CM energy 
  // so not compared to the emitter / spectator
  // pk = E_k (sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta), 1)
  // where E_k = z*Q, theta == delta_target and phi == phi_target
  double cosdelta = cos(delta_target);
  double sindelta = sin(delta_target);
  double Ek       = to_double(sqrt(event.Q2()))*z_target;
  // this means
  Momentum pk(sindelta*cos(phi_target)*Ek, sindelta*sin(phi_target)*Ek, cosdelta*Ek, Ek);
  
  // shower variables are now easily constructed
  // we always have (for every panscales shower)
  // pk = ak * emitter + bk * spectator +/- k_perp
  // the sign of k_perp depends on whether we deal with a final state or initial-state shower 
  // [see bool above]
  // -> ak = 2 pk.spectator / sij
  // -> bk = 2 pk.emitter   / sij
  // so the shower variables become
  precision_type sij     = 2*dot_product(emitter, spectator);
  precision_type alphak  = 2*dot_product(spectator, pk)/sij;
  precision_type betak   = 2*dot_product(emitter, pk)/sij;
  // from this we can extract lnv and lnb
  // note that from ak, bk <-> lnv, lnb (common between all showers) we get
  // lnb = 0.5*log(ak/bk) - 0.5*log(sj/si)
  //     = 0.5*log(si*ak/bk/sj)
  // lnv = ln(kperp) - log(rho) - beta_ps * abs(lnb)
  //     = log(sqrt(ak*bk*sij)/rho) - beta_ps*abs(lnb)
  precision_type si  = 2*dot_product(emitter, event.Q());
  precision_type sj  = 2*dot_product(spectator, event.Q());
  precision_type rho = pow(si*sj/sij/event.Q2(), precision_type(0.5*_shower.get()->beta()));
  lnb = to_double(0.5*log(si*alphak/sj/betak));
  lnv = to_double(log(sqrt(alphak*betak*sij)/rho) - _shower.get()->beta()*fabs(lnb));

  // for the transverse component we note that
  // k_perp = |kT|(sin(phi)*perp1 + cos(phi)*perp2)
  // -> pk.perp1 = -+|kT|sin(phi) (as perp1^2 = -1)
  // -> pk.perp2 = -+|kT|cos(phi) (as perp1^2 = -1)
  // again, sign depends on initial-state shower (-) or final state (+)
  // get the inner products with the perp1 and perp2 vectors
  precision_type sinphi = final_state_shower ? dot_product(perp1, pk) : -dot_product(perp1, pk);
  precision_type cosphi = final_state_shower ? dot_product(perp2, pk) : -dot_product(perp2, pk);

  // now we set phi = arctan(sinphi, cosphi)
  phi = atan2(to_double(sinphi), to_double(cosphi));
  // if phi < 0 add 2pi
  if(phi < 0) phi+= 2.*M_PI;

  // now we get the jacobian to go from dz ddelta dpsi -> |J| dlnv dlnb dphi (in the soft limit)
  // from dpsi -> dphi is trivial
  // dlnv dlnb = 0.5 * dln(ak) dln(bk)
  // lnb = to_double(0.5*log(si*alphak/sj/betak));
  // lnv = to_double(log(sqrt(alphak*betak*sij)/rho) - _shower.get()->beta()*fabs(lnb));

  // Test on-shellness
  // precision_type kt2_one = dot_product(rp.perp1, pk)*dot_product(rp.perp1, pk) + dot_product(rp.perp2, pk)*dot_product(rp.perp2, pk);
  // precision_type kt2_two = ak*bk*sij;


  // Jacobian
  return to_double(4*sij*alphak*betak/event.Q2())/z_target/sindelta;
}


//========================================================================
// 
// Parts related to matching
// 
//========================================================================

/// This function computes the integral of (R-R_s) and |R-R_s| in MC@NLO-style
/// matching, which is used to define the probability of generating
/// a hard event vs. a shower event, as well as the overall event weight
///
/// the answer is returned as a MCatNLOIntegral
ShowerRunner::MCatNLOIntegral ShowerRunner::compute_mcatnlo_integral(Event & event, double lnv_max, double lnv_min,
                                                                     uint64_t nev) {

  MCatNLOIntegral result;
  double integral_sq  = 0;
  
  for (uint64_t iev = 0; iev < nev; iev++) {
    Event base_event = event;
    
    // we take the absolute value here, because if there are 
    // cancellations between negative & positive weights, 
    // we want to make sure that we still generate enough
    // events later on
    double weight = tree_level(base_event, lnv_max, lnv_min, 1);

    result.integral += weight/nev;
    result.integral_abs += fabs(weight)/nev;
    integral_sq  += (weight*weight)/nev;
  }
  result.err = sqrt((integral_sq - pow2(result.integral))/nev);
  result.err_abs = sqrt((integral_sq - pow2(result.integral_abs))/nev);
  return result;
}


//========================================================================
// 
// Extra access for interfacing w external tools (like Pythia)
// 
//========================================================================

// initialise the run
void ShowerRunner::initialise_run(Event & event, double lnv) {
  if (_matching_ptr)
    _initialise_matching_for_event();

  _initialise_run(event, lnv);
  // #TRK_ISSUE-686 can we remove the next lines (the wipe should
  // #not be neeeded and the initialise is done in _initialise_run)
  if(_spin_cor_tree.do_spin_correlations()){
    _spin_cor_tree.wipe_cache();
    _spin_cor_tree.initialise(event);
  }
}

// get the next (candidate) lnb
bool ShowerRunner::find_next_lnb(Event & event,
                                 typename ShowerBase::Element *element,
                                 double lnv, double &lnb){
  // handle cache updates prior to dipole splitting
  _update_elements_in_store_pre_splitting(event, element, lnv);
  
  // Generate a lnb to go with the lnv generated above.
  return (this->*_find_lnb_alg_ptr)(element, lnv, lnb);
}



//========================================================================
// 
// Additional low-level configuration and information 
// 
//========================================================================

// returns a string description of the strategy used to find the next element
std::string ShowerRunner::strategy_description() const {
  std::ostringstream ostr;
  ostr << strategy();
  if (strategy() == CentralRap || strategy() == CentralAndCollinearRap) {
    ostr << ", half_central_rap_window = " << half_central_rap_window();
  } else if (strategy() == CollinearRap || strategy() == CentralAndCollinearRap) {
    ostr << ", collinear_rap_window = " << collinear_rap_window();
  }
  return ostr.str();
}
  
//----------------------------------------------------------------------
// writes out the Sudakov values (e.g. for the header) so that one
// view what is going on
void ShowerRunner::write_sudakov_values(std::ostream & ostr) const {
  ostr << "# Cached Sudakov values Delta(lnvmax, lnv) (col1=lnv col2=sudakov, col3=error-estimate)" << std::endl;
  for (const auto & sudakov: _sudakov_array) {
    ostr << sudakov.lnv << " " << sudakov.value << " " << sudakov.error << std::endl;
  }
  ostr << std::endl << std::endl;
}

//----------------------------------------------------------------------
// Sets the shower runner's default emission enhancement factor for all
// elements from the outside world.
void ShowerRunner::enhancement_factor(double enhancement_factor) {
  assert(enhancement_factor >= 1. &&
         "ShowerRunner: Enhanced emission factor must be >= 1.");
  _enhancement_factor = enhancement_factor;
  if(_do_enhanced_roulette) _enhanced_roulette = EnhancedRoulette(_enhancement_factor);
}
  

//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//                      END OF THE PUBLIC INTERFACE                         //
//                                                                          //
//                BEGINNING OF THE PRIVATE INTERNAL PARTS                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////
  

//========================================================================
// 
// Main steps for doing one emission
// 
//======================================================================== 

//--------------------------------------------------------------------------------
// Initialise shower element info storage based on the input event
void ShowerRunner::_initialise_run(Event &event, double lnv, bool post_matching_reinitialisation) {
  // print some output message
  print_banner();

  // Set flag to indicate whether to perform additional/alternative
  // operations re matching, or proceed with all the usual PS veto
  // algorithm steps.
  //
  // We do this early on so as to have the right shower for the
  // following steps (e.g. to create the element store)
  // #TRK_ISSUE-563 GS-NEW-CODE: I'd put this here instead of later: if (event.can_be_matched() && _matching_ptr)
  // #TRK_ISSUE-564 GS-NEW-CODE: I'd put this here instead of later:   _initialise_matching_for_event();
  
  // Keep track of lnvmax in case we want to run a power shower
  // #TRK_ISSUE-565 GS-NEW-CODE: should this go inside _initialise_matching_for_event?
  _lnvmax = lnv;
 
  // if spin correlations are on, make sure that the shower has
  // do_kinematics_always_true_if_accept returning true
  if (_spin_cor_tree.do_spin_correlations()){
    // Spin correlation tree
    _spin_cor_tree.initialise(event);
    assert((_shower->do_kinematics_always_true_if_accept()) && "Spin correlations are only supported for showers for which do_kinematics_always_true_if_accept is true");
  }
  
  // clear cache
  _elements.clear();
  _cached_ndipoles = event.dipoles().size();
  _tot_a = _tot_b = _tot_astar = 0.0;

  // Set initial max value of astar and min b, and tag associated lnv. 
  this->_max_astar = -std::numeric_limits<double>::max();
  this->_min_b     =  std::numeric_limits<double>::max();
  for (unsigned dipole_index = 0; dipole_index < event.dipoles().size(); ++dipole_index) {
    auto elements = _shower->elements(event, dipole_index);
    for (auto & element : elements) {
      element->enhancement_factor(_enhancement_factor);
      element->update_cache(lnv);
      this->_max_astar = std::max(this->_max_astar,element->norm_astar());
      this->_min_b     = std::min(this->_min_b,element->norm_b());
    }
  }
  _max_astar_o_min_b = _max_astar/_min_b;
  this->_last_searched_lnv = lnv;

  // register a very negative lnv in case there are no emissions.
  _lnvfirst = -std::numeric_limits<double>::max();

  // Initialisation if using EnhancedRoulette.
  if(_do_enhanced_roulette) _enhanced_roulette.init(_enhancement_factor,lnv);

  // For each dipole in the event, create the elements and add them to the store.
  for (unsigned dipole_index = 0; dipole_index < event.dipoles().size(); ++dipole_index)
    _add_dipole_elements_to_store(event, dipole_index, lnv);


  // all the following steops are not needed if we've already initialised things before and ony do the post-matching reinitialisation
  if (post_matching_reinitialisation) return;
  
  // Let emission veto code know it's got a new event.
  if(_veto_ptr) _veto_ptr->initialise(event,lnv);
  // initialise Hoppet (if needed)
  if (event.is_pp()){
    // #TRK_ISSUE-566 GS-NOTE: we need a mechanism to change the PDF set and the Qmax
    //(or all the scales)

    // make sure the _hoppet_runner is set
    assert((_hoppet_runner!=nullptr) && ("Detected a pp collision but no PDF(Hoppet)Runner set. Aborting."));

    // get the PDFs x fractions and values for both beams
    bool beam1_pdf_set = false;
    bool beam2_pdf_set = false;
    double x_beam1=0.1, x_beam2=0.1;
    int pdgid_beam1=21, pdgid_beam2=21;
    for (unsigned int i=0; i<event.particles().size(); ++i){
      const auto & p = event.particles()[i];
      if (p.initial_state()==1){
        // set the beam1 PDF
        x_beam1 = to_double(event.pdf_x(p));
        pdgid_beam1 = p.pdgid();
        event.set_pdf_beam1(i, x_beam1);
        beam1_pdf_set = true;
      } else if (p.initial_state()==2){
        // set the beam2 PDF
        x_beam2 = to_double(event.pdf_x(p));
        pdgid_beam2 = p.pdgid();
        event.set_pdf_beam2(i, x_beam2);
        beam2_pdf_set = true;
      }
    }
    assert(beam1_pdf_set && beam2_pdf_set);

    // initialise the overhead factor
    if (_isr_overhead_factor_handler.get() == nullptr){
      _isr_overhead_factor_handler.reset(new ISROverheadFactor(_shower->qcd(), _hoppet_runner));
      // optionally dump the PDFs to file
      if (_pdf_dump_filename != "") {
        // make sure the overhead factor has been generated for sufficiently extreme x values
        _isr_overhead_factor = (*_isr_overhead_factor_handler)(0.995, 0.995, pdgid_beam1, pdgid_beam2);
        cout << "Outputting PDF info to file: " << _pdf_dump_filename << endl;
        ofstream pdf_file(_pdf_dump_filename);
        pdf_file << "# PDF info: " << _hoppet_runner->description() << endl;
        pdf_file << (*_isr_overhead_factor_handler) << endl;
        pdf_file << endl;
        _hoppet_runner->output_pdf(pdf_file, _lnvmax);

      }
    }

    // set the initial factor based on the current PDFs
    _isr_overhead_factor = (*_isr_overhead_factor_handler)(x_beam1, x_beam2, pdgid_beam1, pdgid_beam2);
    // make sure that the ISR overhead factor is not already larger than the maximum allowed value
    // cout << x_beam1 << " " << x_beam2 << " " << pdgid_beam1 << " " << pdgid_beam2 << endl;
    // cout << "ISR overhead factor: " << _isr_overhead_factor << endl;
    // cout << "Maximum ISR overhead factor: " << _hoppet_runner->max_ISR_overhead() << endl;
    assert(_isr_overhead_factor <= _hoppet_runner->max_ISR_overhead() &&
           "The ISR overhead factor is larger than the maximum value at initialisation time.");

  } else if (event.is_DIS()){
    // make sure the _hoppet_runner is set
    assert((_hoppet_runner!=nullptr) && ("Detected a DIS collision but no PDF(Hoppet)Runner set. Aborting."));

    // get the PDFs x fractions and values for both beams
    bool beam_pdf_set = false;
    double x_beam=0.1;
    int pdgid_beam=21;
    // force the quark to be the parton 1
    for (unsigned int i=0; i<event.particles().size(); ++i){
      const auto & p = event.particles()[i];
      // if we have beam1 as a parton
      if (p.initial_state()==1 && p.is_parton()){
        x_beam = to_double(event.pdf_x(p));
        pdgid_beam = p.pdgid();
        event.set_pdf_beam1(i, x_beam);
        beam_pdf_set = true;
      } else if (p.initial_state()==2 && p.is_parton()){ 
        // if we have beam2 as a parton
        x_beam = to_double(event.pdf_x(p));
        pdgid_beam = p.pdgid();
        event.set_pdf_beam2(i, x_beam);
        beam_pdf_set = true;
      }
    }
    assert(beam_pdf_set);

    // initialise the overhead factor
    if (_isr_overhead_factor_handler.get() == nullptr){
      _isr_overhead_factor_handler.reset(new ISROverheadFactor(_shower->qcd(), _hoppet_runner));
    }

    // set the initial factor based on the current PDFs
    _isr_overhead_factor = (*_isr_overhead_factor_handler)(x_beam, pdgid_beam);
  } else {
    // make sure the factor is reset to 1 for non-pp events (in case
    // we use the same ShowerRunner to generate both ee and pp)
    _isr_overhead_factor = 1.0;
  }

  // Set flag to indicate whether to perform additional/alternative
  // operations re matching, or proceed with all the usual PS veto
  // algorithm steps.
  if (event.can_be_matched() && _matching_ptr)
    _initialise_matching_for_event();

  // set overhead factors associated with uncertainty estimates
  // and compute the global overhead factor 
  _initialise_overhead_factors();

#ifdef PSVERBOSE
  std::cout << "Initial event (at the end of initialisation):" << std::endl;
  event.print_following_dipoles();
  for (auto & dipole: event.dipoles()) {
      std::cout << "Dipoles " << dipole.index_3() << " " << dipole.index_3bar() << " " << dipole.dirdiff_3_minus_3bar << std::endl;

  }
#endif // PSVERBOSE
}
  

//--------------------------------------------------
// returns the acceptance probability associated with alphas
// #TRK_ISSUE-568  GS-NEW-CODE: add info about what is used/set in emisison_info 
Weight ShowerRunner::_alphas_acceptance_probability(const typename ShowerBase::Element *element,
                                                    typename ShowerBase::EmissionInfo *emission_info) const{
  double lnkt = element->alphas_lnkt(emission_info->lnv, emission_info->lnb);
  emission_info->alphas_lnkt = lnkt;

  // #TRK_ISSUE-569  GPS 2022-09-11: we should really call this lnmu at this stage?
  // (there's also an open question of what we want to set in 
  // emission_info->alphas_lnkt, but that depends on what we use it
  // for -- not clear we use it at all at the moment)
  // #TRK_ISSUE-570  GS 2023-04-06: just used w add_branching_history_element(...)
  // which keeps track of the alphas_lnkt of the branching
  // history. Not sure for what purpose!
  lnkt += _shower->qcd().lnxmuR();

  // if the coupling is fixed, we just need to check if lnkt>lnktmin
  if (_shower->qcd().nloops()==0) {
    return (lnkt >= _shower->qcd().lnkt_cutoff())
      ? Weight::exact_one : Weight::exact_zero;
  }

  // for a running coupling, we need to get an acceptance of alphas/alphasmax
  double alphas_max = _shower->max_alphas();
  double alphas     = _shower->qcd().alphas(lnkt);

  if (_shower->has_alphas2_coeff() && _shower->qcd().xmuR_compensation()) {
         
    double coeff2 = element->alphas2_coeff(emission_info);
    double alphas_MSbar = _shower->qcd().alphasMSbar(lnkt); //< wo K_CMW factor
    // cout << "lnv = " << emission_info->lnv
    //      << ", lnb = " << emission_info->lnb
    //      << ", z=" << emission_info->z_radiation_wrt_splitter() 
    //      << ", coeff2=" << coeff2
    //      << endl;
    // cout << "lnxmuR=" << _shower->qcd().lnxmuR()  << ", adding " << coeff2 * pow2(alphas_MSbar) << endl;
    alphas += coeff2 * pow2(alphas_MSbar);
  }

  assert(alphas_max>=alphas);
  return alphas/alphas_max;
}

//--------------------------------------------------
// helper to compute the acceptance probability
//
// This combines information from the shower's
// acceptance_probability and, when needed the PDFs.
//
// Shower::acecptance_probability fills
//   emission_info::emitter_splitting_weight
//   emission_info::emitter_weight_rad_gluon
//   emission_info::emitter_weight_rad_quark
//   emission_info::spectator_weight_rad_gluon
//   emission_info::spectator_weight_rad_quark
//   emission_info.z_radiation_wrt_emitter
//   emission_info.z_radiation_wrt_spectator
// from which this method computes and returns a unique acceptance
// probability.
// #TRK_ISSUE-572 GS-NEW-CODE: add info about what is needed/set in emission_info
// #TRK_ISSUE-573 GS-NEW-CODE: not cleaned yet
inline Weight ShowerRunner::_acceptance_probability(const typename ShowerBase::Element *element,
                                                    typename ShowerBase::EmissionInfo *emission_info){

  // set reasonable defaults in emission info
  emission_info->emitter_splitting_weight   = 1.0;
  emission_info->emitter_weight_rad_gluon   = 1.0;
  emission_info->emitter_weight_rad_quark   = 0.0;
  emission_info->spectator_weight_rad_gluon = 0.0;
  emission_info->spectator_weight_rad_quark = 0.0;
  emission_info->z_radiation_wrt_emitter    = 0.0;
  emission_info->z_radiation_wrt_spectator  = 0.0;

  
  // step 1: compute the splitting probabilities at the emitter and
  // (optionally) spectator end of the element. This calls the element.
  emission_info->acceptance_probability_was_true = element->acceptance_probability(emission_info);
  IFPSVERBOSE(cout << "acceptance_probability_was_true=" << emission_info->acceptance_probability_was_true << endl;);
  if (!emission_info->acceptance_probability_was_true) return Weight::exact_zero;
  
  // step 2: compute (and multiply by) PDF factors
  
  precision_type pdf_tolerance = 1e-10;
  
  const Event & event = element->event();
  
  // compute the new x's
  precision_type new_x_beam1 = emission_info->beam1_pdf_new_x_over_old * event.pdf_x_beam1();
  precision_type new_x_beam2 = emission_info->beam2_pdf_new_x_over_old * event.pdf_x_beam2();
  if ((new_x_beam1 >= 1) || (new_x_beam2 >= 1)) return Weight::exact_zero;  
  
  // get the splitting factor at the emitter potentially including PDFs
  //
  /// #TRK_ISSUE-574 GS-NOTE: for debugging. Remove once things are fully settled
  ///                AK+MvB+GPS 2024-02-02: actually this is now REQUIRED for proper running
  emission_info->pdf_info_beam1.reset(event.pdf_x_beam1());
  emission_info->pdf_info_beam2.reset(event.pdf_x_beam2());
  //emission_info->pdf_beam1_set = emission_info->pdf_beam2_set = false;

  double emitter_pdf_factor_quark, emitter_pdf_factor_gluon;
  _compute_pdf_factor(emission_info,
                      element->emitter(), !(element->emitter_is_3bar_end_of_dipole()),
                      new_x_beam1, new_x_beam2,
                      emitter_pdf_factor_quark, emitter_pdf_factor_gluon);
  emission_info->emitter_weight_rad_gluon *= emitter_pdf_factor_gluon;
  emission_info->emitter_weight_rad_quark *= emitter_pdf_factor_quark;
  
  // get the factor for the spectator
  //if (emission_info->emitter_splitting_weight<1){
  if ((emission_info->spectator_weight_rad_gluon>0) ||
      (emission_info->spectator_weight_rad_quark>0)){
    double spectator_pdf_factor_quark, spectator_pdf_factor_gluon;
    _compute_pdf_factor(emission_info,
                        element->spectator(), element->emitter_is_3bar_end_of_dipole(),
                        new_x_beam1, new_x_beam2,
                    spectator_pdf_factor_quark, spectator_pdf_factor_gluon);
    emission_info->spectator_weight_rad_gluon *= spectator_pdf_factor_gluon;
    emission_info->spectator_weight_rad_quark *= spectator_pdf_factor_quark;
  }
  
  // #TRK_ISSUE-576 GS-NOTE: this assumes an "additive" combination for antenna showers
  //
  //MvB+GS-NOTE: if we wanted to restore the "multiplicative" approach, we could use
  //  (emission_info->emitter_weight_rad_gluon+emission_info->emitter_weight_rad_quark)
  // *(emission_info->spectator_weight_rad_gluon+emission_info->spectator_weight_rad_quark)
  //
  // Notes:
  // #TRK_ISSUE-577  - potentially make an assert that we're dealing w an antenna shower
  // #TRK_ISSUE-578  - potentially make an assert that we're dealing w an ee shower
  //   [for pp showeers, check that the splitting functions are
  //   normalised s.t. weight_rad_gluon + weight_rad_quark goes to 1
  //   in the soft limit]
  double acceptance_probability;
  if (_shower->only_emitter_splits()){
    acceptance_probability = (emission_info->emitter_weight_rad_gluon+
                              emission_info->emitter_weight_rad_quark);
  } else {
    double wE = emission_info->  emitter_weight_rad_gluon + emission_info->  emitter_weight_rad_quark;
    double wS = emission_info->spectator_weight_rad_gluon + emission_info->spectator_weight_rad_quark;
    acceptance_probability = wE + wS;
    // The following line evaluates the emitter splitting_weight so as
    // to match the exact weight of the emitter in the above expression
    // Note that the other multiplicative factors inserted below won't
    // change the relative emitter/spectator weights.
    emission_info->emitter_splitting_weight = wE/acceptance_probability;
  }

  IFPSVERBOSE(cout << "l." << __LINE__ << " acceptance_probability=" << acceptance_probability 
                   << ", em_spl_wgt=" << emission_info->emitter_splitting_weight 
                   << ", sp_spl_wgt=" << 1-emission_info->emitter_splitting_weight 
                   << ", em_wgt_glu=" << emission_info->emitter_weight_rad_gluon 
                   << ", em_wgt_qrk=" << emission_info->emitter_weight_rad_quark 
                   << ", sp_wgt_glu=" << emission_info->spectator_weight_rad_gluon 
                   << ", sp_wgt_qrk=" << emission_info->spectator_weight_rad_quark 
                   << endl;
  )

  // include potential remaining beam PDF factors
  //
  // Note: no need to cache the PDFs in this case as we only need
  // cached values for select_channel and this uses either the emitter
  // or the spectator which have already been handled above.
  //
  // Also, these only involve flavour-diagonal PDF ratios
  // cout << "acceptance = " << acceptance_probability << endl;
  if ((!emission_info->pdf_info_beam1.set) && (emission_info->beam1_pdf_new_x_over_old>1+pdf_tolerance)){
    double lnmuF = max(element->pdf_lnkt(emission_info->lnv, emission_info->lnb),
                     _shower->qcd().lnkt_cutoff());
    int beam1_id = event.incoming1().pdgid();
    double pdf_old = (*_hoppet_runner)(event.pdf_x_beam1(), lnmuF, beam1_id);
    double pdf_new = (*_hoppet_runner)(to_double(new_x_beam1), lnmuF, beam1_id);
    acceptance_probability *= pdf_new/pdf_old;
    cout << "We are in the diagonal PDF evaluation part" << endl;
    if (pdf_old <= 0) throw runtime_error("pdf_old <= 0 in beam1 diagonal PDF ratio computation");
  }
  if ((!emission_info->pdf_info_beam2.set) && (emission_info->beam2_pdf_new_x_over_old>1+pdf_tolerance)){
    double lnmuF = max(element->pdf_lnkt(emission_info->lnv, emission_info->lnb),
                     _shower->qcd().lnkt_cutoff());
    int beam2_id = event.incoming2().pdgid();
    double pdf_old = (*_hoppet_runner)(event.pdf_x_beam2(), lnmuF, beam2_id);
    double pdf_new = (*_hoppet_runner)(to_double(new_x_beam2), lnmuF, beam2_id);
    acceptance_probability *= pdf_new/pdf_old;
    if (pdf_old <= 0) throw runtime_error("pdf_old <= 0 in beam2 diagonal PDF ratio computation");
  }

  // the acceptance probability so far is just the PDF part. Record it for future use
  // in case we see violation
  _isr_radiation_factor = acceptance_probability;

  // include a potential factor associated with uncertainties in the hard 
  // matrix element
  //
  // Note: as long as the calculation of lnkt_approx only depends on lnv 
  // and lnb (or on the things cached in Shower::acceptance_probability()) 
  // this can be computed here. If it starts depending on the full kinematics,
  // we need to ove it to a later stage. 
  //
  // Also, this correction does not apply when matching corrections are 
  // enabled (remember that once we have applied the matching correction, 
  // _do_matching is set to false)
  if ((!_match_current_emission(event)) && (_shower->uncertainty_x_hard() != 1.0)){
    // insert a factor 
    //   1 + (x-1) exp(2 *min(lnkt-lnmuF,0))
    double lnkt_approx = element->lnkt_approx(emission_info->lnv,emission_info->lnb);

    precision_type muhard2 = event.Q2();
    //handle VBF with two dis chains
    if(event.is_pp() and event.Q2_dis(1) > 0 and  event.Q2_dis(2) > 0 and event.Q2_dis(1) != event.Q2_dis(2)){
      int ichain = element->emitter().get_DIS_chain();
      muhard2 = event.Q2_dis(ichain);
    }

    
    double correction_factor = 1 + (_shower->uncertainty_x_hard()-1) 
      * exp(min(2*lnkt_approx-to_double(log(muhard2/4)), 0.0));
    acceptance_probability *= correction_factor;
  }

  // include a potential factor associated with uncertainties for
  // emissions with commensurate kt
  //
  // These are meant to include an uncertainty associated with
  // spurious recoil effects (inducing NLL failures) in some
  // kt-ordered dipole showers
  if (_shower->uncertainty_x_simkt() != 1.0){
    // insert a factor 
    //   1 + (x-1) exp(-2 * min(|lnkt2-lnkt1_em|,
    //                          |lnkt2-lnkt1_sp|))
    double lnkt1_emitter   = element->emitter()  .radiated_lnkt();
    double lnkt1_spectator = element->spectator().radiated_lnkt();
    double lnkt2_approx    = element->lnkt_approx(emission_info->lnv,emission_info->lnb);
    double correction_factor = 1 + (_shower->uncertainty_x_simkt()-1) 
      * min(1.0,
            4*exp(-2*min(std::abs(lnkt2_approx-lnkt1_emitter),
                         std::abs(lnkt2_approx-lnkt1_spectator))));
    acceptance_probability *= correction_factor;
  }

  // we insert a factor 1/_isr_overhead_factor to (i) compensate for the
  // _isr_overhead_factor in the Sudakov exponent and (ii) guarantee that
  // the returned probability be smaller than 1.
  //
  // #TRK_ISSUE-580 Note that this factor is computed assuming a collinear kinematics
  // and assuming only 1 PDF x splitting factor. One may wonder if
  // this captures all the physics cases we're facing in the various
  // showers.
  return acceptance_probability / _global_overhead_factor;
}

//-------------------------------------------------------------
// helper to decide, based on the information provided by
// Element::acceptance_probability, which end of the dipole splits
// and into which channel.
//
// This uses
//  - emission_info::emitter_splitting_weight            (possibly only for antenna showers)
//  - emission_info::emitter_splitting_weight_g2qqbar
//  - emission_info::spectator_splitting_weight_g2qqbar  (only for antenna showers)
// and fills
//  - emission_info.do_split_emitter
//  - emission_info.radiation_abs_pdgid
//  - emission_info.splitter_out_pdgid
//
// The requested_radiation_abs_pdgid argument allows one to enforce
// the PDGid of the radiation. Setting it to 0 (the default) yields
// to a random selection (using the information in emission_info)
//
// Similarly, a splitter can be enforced through the "reqyuested_channel" argument:
//   0 correspoinds to a random selection w the appropriate weight (the defaukt)
//  -1 corresponds to splitting the spectator
//  +1 corresponds to splitting the emitter
//
// This method returns the weight corresponding the (optional)
// requested pdgid and channel.
//
// Note that if the weight is 0, the entries in emission_info
// supposedly set by this function are not guaranteed to be valid.
//
// If this is called with pre_matching=true, then it only sets
// do_split_emitter, and the other two are left ill-defined 
// (because they will get set only by a separate call after matching)
Weight ShowerRunner::_select_channel_details(const typename ShowerBase::Element *element,
                                             typename ShowerBase::EmissionInfo * emission_info, 
                                             unsigned int requested_radiation_abs_pdgid,
                                             int requested_channel,
                                             bool pre_matching){
  IFPSVERBOSE(cout << "entering _select_channel_details with gsl = " << sha1(gsl.hex_state())<< endl;);

  // unless they are forced, select the "random" details of the
  // splitting: emitter/spectator and channel (emission pdgid)
  Weight weight(Weight::exact_one);

  if (requested_channel==0){
    emission_info->do_split_emitter = _shower->only_emitter_splits() ||
      gsl.accept(emission_info->emitter_splitting_weight);
  } else if (requested_channel==1){
    // update the weight
    emission_info->do_split_emitter = true;
    if (!_shower->only_emitter_splits()){
      weight *= emission_info->emitter_splitting_weight;
      if (weight==0) return 0.0;
    }
  } else { //< requested_channel==-1
    emission_info->do_split_emitter = false;
    if (_shower->only_emitter_splits()) return Weight::exact_zero;
    weight *= (1-emission_info->emitter_splitting_weight);
    if (weight==0) return 0.0;
  }    

  // if we are doing matching and this is called before the actual
  // matching step then do not yet decide the channel
  if (pre_matching) {
    // #TRK_ISSUE-XXX GPS-note: open question of what the right behaviour is here.
    //                          The "return" changes random number for MC@NLO, but results seem stable
    //
    //_warning_matched_weighted_needs_checking.warn("Combination of matching + weighting needs double-checking in "
    // "_select_channel_details, notably question of which factors to include in weight"
    // " and which of them should be included around the call to select_channel_details that comes"
    // " after the matched event kinematics have been determined");
    // NB: warning was causing validation to fail, because of extra warning at end of file. 
    return weight;
  }

  // first decide whether we radiate a quark or a gluon
  bool radiate_gluon = true;

  // we only need to bother rolling a die if there is a non-zero
  // quark-radiation probability
  double weight_rad_quark = emission_info->splitter_weight_fraction_quark();
  IFPSVERBOSE(cout << "weight_rad_quark = " << weight_rad_quark << endl;);
    
  if (requested_radiation_abs_pdgid==0){
    if (weight_rad_quark > 0){
      radiate_gluon = !gsl.accept(weight_rad_quark);
    }
  } else if (requested_radiation_abs_pdgid==21){
    weight *= (1-weight_rad_quark);
    if (weight==0) return 0.0;
  } else {
    radiate_gluon = false;
    weight *= weight_rad_quark;
    if (weight==0) return 0.0;
  }

  // now decide the exact channel
  const Particle & splitter = emission_info->splitter();
  if (radiate_gluon) {
    emission_info->radiation_pdgid = 21;
    emission_info->splitter_out_pdgid = splitter.pdgid();
  } else { // quark radiation
    // here we need to decide the exact quark or antiquark pdgid of
    // the radiation
    //
    // this depends on the pdgid of the splitter
    if (splitter.pdgid()!=21){
      // if the splitter is a quark, that means we have a
      // g_is->{q|qbar}_is+{qbar|q}_fs splitting and the pdg id is just
      // the opposite of the splitter's
      assert(splitter.is_initial_state());
      emission_info->radiation_pdgid = -splitter.pdgid();
      emission_info->splitter_out_pdgid = 21;
    } else { // splitter is a gluon
      // Our FS convention is that if we are at the 3 end of a dipole
      // the splitter will become a quark, and so the radiation is an anti-quark
      //
      // For IS: the 3end of an IS dipole is actually an incoming 3bar, so the 
      // splitter becomes an anti-quark and the radiation is also an anti-quark
      //
      // in all cases, we'll need to know if we're at the 3 or 3bar
      // end of the dipole
      bool splitter_3_end = emission_info->splitter_is_3_end();
      
      // this case can either be a FSR g->qqbar or an ISR backwards
      // evolution of a gluon into a {q|qbar}
      if (splitter.is_initial_state()){
        // The radiation pdgid is proportional to the individual PDFs
        //
        // if the splitter is at the 3 end, it's an anti-quark,
        // otherwise it's a quark
        ShowerBase::EmissionInfo::PDFInfo * beam = emission_info->pdf_info(splitter);
        // #TRK_ISSUE-591 GS-NOTE: safety test (can be removed down the line)
        assert(beam->set);
        const PDF & pdf_new = beam->pdf_new;
        //const PDF &pdf_new = (splitter.initial_state()==1)
        //  ? emission_info->pdf_beam1 : emission_info->pdf_beam2;
        //if (splitter.initial_state()==1) assert(emission_info->pdf_beam1_set);
        //else                             assert(emission_info->pdf_beam2_set);
        std::vector<double> qweights;
        if (splitter_3_end)
          qweights = pdf_new.relative_antiquark_weights();
        else 
          qweights = pdf_new.relative_quark_weights();

        if (requested_radiation_abs_pdgid==0){
          // take a random number and see where it goes in the list of weights
          double r = gsl.uniform();
          unsigned int iweight=0;
          // the code below should in principle avoid rounding arrors at
          // the upper edge (i.e. case where r~1 and the weights does
          // not exactly add up to 1)
          while ((iweight<qweights.size()-1) && (r>qweights[iweight])){
            r -= qweights[iweight];
            ++iweight; // this line could go inside the previous one as iweight++
          }
        
          emission_info->radiation_pdgid = 1+iweight;
        } else {
          if (requested_radiation_abs_pdgid>qweights.size())
            return Weight::exact_zero;
          emission_info->radiation_pdgid = requested_radiation_abs_pdgid;
          weight *= qweights[requested_radiation_abs_pdgid-1];
        }

        // if the splitter is at the 3 end of the dipole, it is a
        // gluon backwards evolving to an anti-quark. The radiation is
        // therefore an anti-quark as well.
        if (splitter_3_end) emission_info->radiation_pdgid *= -1;
        emission_info->splitter_out_pdgid = emission_info->radiation_pdgid;
      } else { // final-state splitting
        if (requested_radiation_abs_pdgid==0){
          // decide the flavour uniformly
          emission_info->radiation_pdgid = 1+gsl.uniform_int(_shower->qcd().nf());
        } else {
          if (requested_radiation_abs_pdgid>_shower->qcd().nf())
            return Weight::exact_zero;
          emission_info->radiation_pdgid = requested_radiation_abs_pdgid;
          weight *= 1.0/_shower->qcd().nf();
        }

        // if we're at the 3 end of the dipole, our convention is that
        // the gluon becomes a quark and the new (radiated) particle
        // is an anti-quark
        if (splitter_3_end) emission_info->radiation_pdgid *= -1;
        emission_info->splitter_out_pdgid = - emission_info->radiation_pdgid;
      }
    }
  }

#ifdef PSVERBOSE
    std::cout << "  " << " gsl: " << sha1(gsl.hex_state()) << " post _select_channel_details " 
              << "(emission pdg_id = " << emission_info->radiation_pdgid <<  ")\n";
#endif

  return weight;
}


//--------------------------------------------------
// helper to compute the PDF factors associated with a given particle
// (emitter or spectator)
//
// This fills the pdf_factor_quark and pdf_factor_gluon corresponding
// quark and gluon radiation, respectively
// #TRK_ISSUE-581 GS-NEW-CODE: not cleaned yet
inline void ShowerRunner::_compute_pdf_factor(typename ShowerBase::EmissionInfo *emission_info,
                                const Particle & particle, bool is_particle_at_3_end,
                                precision_type new_x_beam1, precision_type new_x_beam2,
                                double & pdf_factor_quark_rad, double & pdf_factor_gluon_rad) const{
  pdf_factor_quark_rad = 1.0;
  pdf_factor_gluon_rad = 1.0;

  // nothing to be done for final-state particles
  if (particle.initial_state() == 0) return;

  // grab basic quantities
  const auto & element = emission_info->element();
  double lnmuF_raw = _shower->qcd().lnxmuF() + max(element->pdf_lnkt(emission_info->lnv, emission_info->lnb),
                                               _shower->qcd().lnkt_cutoff());
  
  // prepare things to handle vanishing of PDFs near mass thresholds
  // precision_type muF2 = exp(2*precision_type(lnmuF)); 
  // precision_type m2  = pow2(_hoppet_runner->m_iflv(particle.pdgid()));
  // precision_type massive_quark_damping_factor = muF2/(m2+muF2);
  // lnmuF += to_double(0.5*log(1 + m2/muF2));

  double massive_quark_damping_factor, lnmuF;
  _hoppet_runner->get_massive_quark_damping_factor(particle.pdgid(), lnmuF_raw, massive_quark_damping_factor, lnmuF);

  // get the beam we're dealing with for this particle
  ShowerBase::EmissionInfo::PDFInfo * beam = emission_info->pdf_info(particle);
  assert(beam);
  assert(!beam->set);
  beam->set = true;
  beam->ibeam = particle.initial_state();
  if (particle.initial_state() == 1) {
    beam->x_new  = to_double(new_x_beam1);
  } else {
    beam->x_new  = to_double(new_x_beam2);
  }
  beam->lnmuF = lnmuF;
  beam->pdf_old  = (*_hoppet_runner)(beam->x_old, lnmuF, particle.pdgid());
  beam->pdf_new  = (*_hoppet_runner)(beam->x_new , lnmuF);
  beam->flav_old = particle.pdgid();
  beam->damping_factor = massive_quark_damping_factor;
  
  // when the old PDF is near zero, replace it with a very small value to
  // avoid division by zero and then correct things later
  constexpr double dummy_for_zero = 1e-150;
  if (beam->pdf_old == 0.0) beam->pdf_old = dummy_for_zero;

  // deduce the factors
  //
  // if we radiate a gluon, the flavour of the initial-state does not change
  pdf_factor_gluon_rad = beam->pdf_new.flav(particle.pdgid())/beam->pdf_old;
  if (particle.pdgid() == 21){ // gluon
    // for quark emission, this is a gluon backwards evolving to quark
    // this depends on 3 (sum_antiquarks) v. 3bar (sum_quarks)
    if (is_particle_at_3_end){
      pdf_factor_quark_rad = beam->pdf_new.sum_antiquarks()/beam->pdf_old;
    } else {
      pdf_factor_quark_rad = beam->pdf_new.sum_quarks()/beam->pdf_old;
    }
  } else { // quark emitter
    // for quark emission, this is a quark backwards evolving to a gluon
    pdf_factor_quark_rad = beam->pdf_new.flav(21)/beam->pdf_old * massive_quark_damping_factor;
  }

  // now try to arrange for sensible overhead bounding
  // #TRK_ISSUE-583  (GPS 2023-01-29: this code has quite a bit of repetition with get_weight
  // and also duplicates effort wrt the acceptance probability calculation
  // -- I'm not sure how best to minimise this).
  double z = 1.0 - beam->x_old / beam->x_new;
  double w_rad_gluon, w_rad_quark;
  if (particle.pdgid() == 21){
    w_rad_gluon = _shower->qcd().splitting_isr_zomzhalfPg2gg_normalised(z);
    w_rad_quark = _shower->qcd().splitting_isr_zomzPq2gq_normalised(z);
  } else {
    w_rad_gluon = _shower->qcd().splitting_isr_zomzPq2qg_normalised(z);
    w_rad_quark = _shower->qcd().splitting_isr_zomzPg2qqbar_normalised(z);
  }      

  double weight_sum = w_rad_quark * pdf_factor_quark_rad + w_rad_gluon * pdf_factor_gluon_rad;
  if (weight_sum > _hoppet_runner->max_ISR_overhead()){    
    // if the overhead is too large, we need to rescale the weights
    // so that the sum is at most max_ISR_overhead
    double rescaling = _hoppet_runner->max_ISR_overhead()/weight_sum;
    string warn_str = "rescaling ISR weights";
    if (_warning_ISR_rescaling.n_warn_so_far() < _warning_ISR_rescaling.max_warn()){      
      ostringstream ostr;
      ostr << " by " << rescaling 
           << ", to avoid overhead of " << weight_sum << " > " << _hoppet_runner->max_ISR_overhead()
           << ", when lnmuF = " << lnmuF 
           << ", particle.pdg_id() = " << particle.pdgid()
           << ", pdf_old = " << beam->pdf_old
           << ", old_x = " << beam->x_old
           << ", new_x = " << beam->x_new
           << ", w_gluon * pdf_fact_gluon = " << w_rad_gluon * pdf_factor_gluon_rad
           << ", w_quark * pdf_fact_quark = " << w_rad_quark * pdf_factor_quark_rad
           ;
      warn_str += ostr.str();
    }
    _warning_ISR_rescaling.warn(warn_str);

    // this keeps the pattern of non-zero branchings, but if anything in
    // the PDF numerator is zero, we will maintain a zero weight
    pdf_factor_quark_rad *= rescaling;
    pdf_factor_gluon_rad *= rescaling;
  }
  
}
  


//----------------------------------------------------------------------
// Apply the kinematic map, i.e. fill the 4-momenta of the post-branching emitter, spectator and radiation
// Do kinematics can return false for 2 reasons:
//  - the shower do_kinematics failed
//  - a PDF fraction goes above 1
// #TRK_ISSUE-594 GS-NEW-CODE: review this
bool ShowerRunner::_do_kinematics(typename ShowerBase::Element * element,
                                  typename ShowerBase::EmissionInfo * emission_info) const {
  
  // first cache where the emission comes from
  // #TRK_ISSUE-595 TODO4GAVIN: check if you're OK w this
  //  Re the 2nd line: some functions down the line only have access
  //  to the emission info and not the element and need to know if
  //  they should use dirdiffs
  emission_info->emitter_was_3 = element->emitter_is_quark_end_of_dipole();
  emission_info->use_diffs = element->use_diffs();


  typedef typename ShowerBase::Element::RotatedPieces RotatedPieces;
  // the simple, default version, which doesn't do any rotations
  if (!element->use_diffs()) {
    RotatedPieces rp;
    rp.emitter   = element->emitter();
    rp.spectator = element->spectator();
    two_perp(rp.emitter, rp.spectator, rp.perp1, rp.perp2);
    // try the new routine
    // #TRK_ISSUE-596 GS-NOTE: is the next line OK?
    if (! element->do_kinematics(emission_info, rp)){
      if (_shower->do_kinematics_always_true_if_accept())
        assert(false && "ShowerRunner::_do_kinematics: the shower do_kinematics(...) returned false while do_kinematics_always_true_if_accept() is true");
      return false;
    }
  } else {  
    // case with direction differences
    //--------- now proceed with the use_diffs version
    // (the next line ensures that set_rotated_pieces
    // doesn't try to guess the right ref direction)
    const bool emitter_is_always_ref = true;
    RotatedPieces rp_em;
    element->set_rotated_pieces(emission_info, rp_em, emitter_is_always_ref);
    two_perp(rp_em.emitter, rp_em.spectator, rp_em.perp1, rp_em.perp2);
    bool outcome = element->do_kinematics(emission_info, rp_em);
    if (!outcome){
      if (_shower->do_kinematics_always_true_if_accept())
        assert(false && "ShowerRunner::_do_kinematics: the shower do_kinematics(...) returned false while do_kinematics_always_true_if_accept() is true");
      return false;
    }
    
    /// #TRK_ISSUE-597 -------------- new code
    // we store the new momenta, because they will get overwritten
    Momentum em_out_emref = emission_info->emitter_out;
    Momentum rd_out_emref = emission_info->radiation;
    Momentum k_perp_out_emref = emission_info->k_perp;

    // then we get the spectator and radiation in a frame
    // in which the spectator is the z-aligned reference
    // (note that the next line also rotates the perp vectors)
    RotatedPieces rp_sp = rp_em.copy_aligned_to_spectator();
    // and recalculate the kinematics in this new frame
    outcome = element->do_kinematics(emission_info, rp_sp);
    if (!outcome){
      if (_shower->do_kinematics_always_true_if_accept())
        assert(false && "ShowerRunner::_do_kinematics: the shower do_kinematics(...) returned false while do_kinematics_always_true_if_accept() is true");
      return false;
    }
    // finally we transfer things to temporary momenta 
    // #TRK_ISSUE-598  (this is not strictly needed, so maybe we could use refs?)
    Momentum sp_out_spref = emission_info->spectator_out;
    Momentum rd_out_spref = emission_info->radiation;
    Momentum k_perp_out_spref = emission_info->k_perp;
    
    // finally we transfer things back to emission_info
    // in the right frame; the emitter and spectator
    // are straightforward and one always uses the original
    // one as reference
    emission_info->d_emitter_out   = rp_em.rotn_from_z * em_out_emref.direction_diff_wrt_z();
    emission_info->d_spectator_out = rp_sp.rotn_from_z * sp_out_spref.direction_diff_wrt_z();
    // emission_info->emitter_out   = rp_em.rotn_from_z * em_out_emref;
    // emission_info->spectator_out = rp_sp.rotn_from_z * sp_out_spref;
    // #TRK_ISSUE-600  GPS 2020-07 attempt to get out momenta from differences, in hope
    // that this will be more stable than using rotations of the full
    // momenta (cf. also below for the radiation)

    emission_info->emitter_out = Momentum::fromRefDiffNew(
               element->emitter(), emission_info->d_emitter_out, em_out_emref);
    emission_info->spectator_out = Momentum::fromRefDiffNew(
               element->spectator(), emission_info->d_spectator_out, sp_out_spref);

    
    // for the radiation, we see whether we are closer in angle to the 
    // parent emitter or spectator, store the outcome in emission_info
    // for subsequent use, and then calculate/transfer the kinematic
    // info about the radiation to emission_info
    Momentum3<Float> rd_out_ddiff_emref = rd_out_emref.direction_diff_wrt_z();
    Momentum3<Float> rd_out_ddiff_spref = rd_out_spref.direction_diff_wrt_z();
    emission_info->d_radiation_is_wrt_emitter = 
      rd_out_ddiff_emref.modpsq() < rd_out_ddiff_spref.modpsq();
    
    if (emission_info->d_radiation_is_wrt_emitter) {
      emission_info->d_radiation = rp_em.rotn_from_z * rd_out_ddiff_emref;
      emission_info->  radiation = Momentum::fromRefDiffNew(
        element->emitter(), emission_info->d_radiation, rd_out_emref);
      emission_info->k_perp = rp_em.rotn_from_z * k_perp_out_emref;
      //auto p3 = rp_em.rotn_from_z * k_perp_out_emref.p3();
      //emission_info->k_perp = MomentumM2<precision_type>(p3.px(), p3.py(), p3.pz(), k_perp_out_emref.E());

    } else {
      emission_info->d_radiation = rp_sp.rotn_from_z * rd_out_ddiff_spref;
      emission_info->  radiation = Momentum::fromRefDiffNew(
        element->spectator(), emission_info->d_radiation, rd_out_spref);
      emission_info->k_perp = rp_sp.rotn_from_z * k_perp_out_spref;
      //auto p3 = rp_sp.rotn_from_z * k_perp_out_spref.p3();
      //emission_info->k_perp = MomentumM2<precision_type>(p3.px(), p3.py(), p3.pz(), k_perp_out_spref.E());
    }
    // #TRK_ISSUE-601  KH note/todo: k_perp computation from rotation can have room
    // for improvement for same reason noted in GPS comment ~30 lines above.

    // #TRK_ISSUE-602  cache a few extra things (moved from dirdiffs_pre_update)
    const Dipole & this_dipole = element->dipole();
    emission_info->cached_dirdiff_3_minus_3bar = this_dipole.dirdiff_3_minus_3bar;
  }

  // not needed for e+e- showers
  // check the PDF fractions after splitting
  const Event & event = element->event();

  if (event.is_pp() || event.is_DIS()){
    double max_allowed_x = _hoppet_runner->pdf_max_reliable_x();
    // also check for whether we are dealing with a parton, otherwise (for DIS) we do not need to check for the PDF
    if (event.pdf_x_beam1() * emission_info->beam1_pdf_new_x_over_old >= max_allowed_x && event[0].is_parton())
      return false;
    if (event.pdf_x_beam2() * emission_info->beam2_pdf_new_x_over_old >= max_allowed_x && event[1].is_parton())
      return false;
  }

  return true;
}

//--------------------------------------------------
// returns the spin acceptance probability
Weight ShowerRunner::_spin_acceptance(typename ShowerBase::Element * element,
                                      typename ShowerBase::EmissionInfo * emission_info,
                                      bool inside_phi_loop) {
  // If spin correlations are on, compute the weight associated to the chosen
  // value of phi, and potentially veto it
  // Always returns 1 if spin correlations are disabled
  pair<Weight,Weight> phi_wgt_and_ovh = _spin_cor_tree.compute_phi_weight_and_overhead(*element, *emission_info);

  // for a fixed value of phi, we return the spin ME weight, i.e. the
  // first of the weights returned by the spin correlation tree.
  //
  // for a ranom phi, we instead use
  //   \int dphi/(2pi) weight = \int dphi/(2pi) overhead weight/overhead
  // meaning that if we use a loop over phi to get the value, we
  // should use weight/overhead as the acceptance probability
  Weight accept_prob_phi = inside_phi_loop
    ? phi_wgt_and_ovh.first/phi_wgt_and_ovh.second : phi_wgt_and_ovh.first;
  
#ifdef PSVERBOSE
  cout << "phi acceptance prob (from spin corr) = " << accept_prob_phi << endl;
#endif //PSVERBOSE
  assert(accept_prob_phi - 1.0 <= ACCEPTANCE_PROBABILITY_MARGIN*std::numeric_limits<double>::epsilon());

  return accept_prob_phi;
}

//--------------------------------------------------
// double-soft deltaK correction
Weight ShowerRunner::_double_soft_dKCMW_acceptance(typename ShowerBase::Element * element,
                                                   typename ShowerBase::EmissionInfo * emission_info) const{
  if (!_shower->double_soft_dKCMW()) return Weight::exact_one;
  
  double dK_acceptance = _shower->delta_K_CMW_acceptance(emission_info);
  IFPSVERBOSE(std::cout << "  " << " gsl: " << gsl.hash_hex_state() << " dKCMW acceptance = " << dK_acceptance;);
  return dK_acceptance;
}
  
// double-soft matrix-element correction
double ShowerRunner::_double_soft_ME_acceptance(typename ShowerBase::Element * element,
                                                typename ShowerBase::EmissionInfo * emission_info) const{
  double double_soft_acceptance = element->double_soft_acceptance_probability(emission_info);
#ifdef PSVERBOSE
  std::cout << "  " << " gsl: " << gsl.hash_hex_state()
            << " double soft accept = " << double_soft_acceptance << std::endl
            << " flavour swap prob  = " << emission_info->flavour_swap_probability << std::endl
            << " colour swap prob gg  = " << emission_info->colour_flow_swap_probability_gg
            << " , qq = " << emission_info->colour_flow_swap_probability_qq << std::endl;
#endif
  return double_soft_acceptance;
}

// double-soft swaps
void ShowerRunner::_double_soft_swaps(typename ShowerBase::Element * element,
                                      typename ShowerBase::EmissionInfo * emission_info,
                                      Weight &weight){
  // check if the double soft requires a flavour change
  //GS-NEW-CODE: check what the fixed-order does
  switch (_double_soft_fixed_emission_pdgid){
  case 0:  // random sampling
    if (gsl.accept(emission_info->flavour_swap_probability)){
      _double_soft_flavour_swap(emission_info);
    }
    break;
  case 21:
    if (emission_info->radiation_pdgid != 21){ // swap
      weight *= emission_info->flavour_swap_probability;
      if (weight>0) _double_soft_flavour_swap(emission_info);
    } else { // no swap
      weight *= (1-emission_info->flavour_swap_probability);
    }
    break;
  default:  // quark
    assert(abs(_double_soft_fixed_emission_pdgid)<=6);
    if (emission_info->radiation_pdgid == 21){ // swap
      weight *= emission_info->flavour_swap_probability;
      if (weight>0) _double_soft_flavour_swap(emission_info);
    } else { // no swap
      weight *= (1-emission_info->flavour_swap_probability);
    }
    break;
  };
  
  // decide whether we should do a colour swap
  //
  // If we decide to swap, it will effectively be done in
  // ShowerBase::update_event when updating the event
  double colour_flow_swap_probability = (emission_info->radiation_pdgid==21)
    ? emission_info->colour_flow_swap_probability_gg
    : emission_info->colour_flow_swap_probability_qq;
  emission_info->colour_flow_swap = (!_double_soft_no_colour_flow_swap)
    && gsl.accept(colour_flow_swap_probability);
  if (emission_info->colour_flow_swap){
    _double_soft_set_splitter_to_partner(emission_info);
  }

}


//------------------------------------------------------------------------
// matching acceptance probability
// this is suited for a treatment with handle_probability_extended.
//
// The flag postpone_for_mcatnlo will be set to true in case of MC@NLO
// matching. In this case, the acceptance probability only needs to be
// imposed after the treatment of spin and colour (the physics reason
// behind comes from a subtraction term in the colour step).
// 
// The treatment of the outcome is a bit delicate because in the
// weighted case, the "acceptance" can be anything. Technically, we
// have 3 cases:
// - we discard things immediately
// - MC@NLO matching: we postpone the matching acceptance probability
// - not MC@NLO matching: we handle the matching acceptance probability immediately
Weight ShowerRunner::_matching_acceptance_probability(Event & event,
                                                      typename ShowerBase::Element * element,
                                                      typename ShowerBase::EmissionInfo * emission_info,
                                                      bool weighted, 
                                                      bool &mcatnlo_matching,
                                                      bool &discard_emission){
  double matching_accept_prob = 0.0;
  // #TRK_ISSUE-5451 GS-NEW-CODE: handle the cases where the radiated PDGID is specified
  _matching_ptr->acceptance_probability(event, *element, *emission_info,
                                        matching_accept_prob);

  // if the alphas acceptance has not yet been handled (because
  // the doupling depends on z), include it in the matching
  // acceptance probability
  // #TRK_ISSUE-546 GS-NEW-CODE: not sure that the "coupling depend on z part is
  // at the correct place for MC@NLO matching
  if (_shower->coupling_depends_on_z()){
    matching_accept_prob *= _alphas_acceptance_probability(element, emission_info);
  } 
  IFPSVERBOSE(cout << "Matching acceptance probability = " << matching_accept_prob << endl;);
  discard_emission=false;
  if ((weighted) && (matching_accept_prob == 0.0)){
    discard_emission=true;
    return 0;
  }

  // #TRK_ISSUE-5461 GS-NEW-CODE: check this!!!
  // The handling of the acceptance probability is a bit sublte.
  // In principle we do the following:
  //  1. check the matching probability (weigthed or unweighted)
  //  2. if accepted (or "re-weighted") call select_channel_details again
  //  3. update the colour and spin acceptance
  //
  // steps 2 and 3 are needed because the matching acceptance can
  // decide to switch the flavours in EmissionInfo. MatchedProcesses
  // is responsible for setting the relevant weights in emission_info
  // such that this gives the desired outcome.
  //
  // This is all fine except for the fact that in the weighted case,
  // we also need to handle MC@NLO matching. In this case
  //  (i) the acceptance weight should be corrected for the shower
  //      acceptance (including the colour acceptance)
  // (ii) the acceptance weight can be anything (including -ve or >1)
  // The code below therefore has a special treatment for MC@NLO matching
  // for weighted events.
  //
  // Our strategy is therefore the following:
  //  1. we compute the basic acceptance probability
  //  2. for non MC@NLO matching, we apply the matching acceptance probability
  //     this step is (temporarily) skipped for MC@NLO
  //  3. we call do_select_channel_details and the colour acceptance again
  //  4. for aMC@NLO matching, we correct the matching weight and apply it
  mcatnlo_matching = (_matching_ptr->get_matching_scheme() == MCAtNLOStyle) && (_mcatnlo_subtract_ps_for_weighted);
  if (mcatnlo_matching) return matching_accept_prob;

  // if not in MC@NLO matching mode, proceed 

  // apply the acceptance probability for matching
  //
  // #TRK_ISSUE-5463 I think the next factor is needed since:
  // - for unweigthhed emissions the overhead factor is included in
  //   the lnv/element selection.
  // - For unmatched emissions, the 1/overhead is in _acceptance probability.
  // - for matched emisisons, it is not in _matching_ptr->acceptance_probability(...)
  //   therefore we need it here
  // for the current ee 3j matching, this is only relevant for scale variations
  //
  // #TRK_ISSUE-2024-01-26-001: this probably has to be swapped with
  // #the previous line so that MC@NLO matching also gets the overhead
  // #normalisation?
  matching_accept_prob /= _global_overhead_factor;
      
  if (!weighted){
    if ((matching_accept_prob != matching_accept_prob) || (matching_accept_prob- 1.0 > ACCEPTANCE_PROBABILITY_MARGIN*numeric_limit_extras<precision_type>::epsilon())){
      cerr << event << endl;
      cerr << "Got a matching probability of " << matching_accept_prob << " (p-1=" << matching_accept_prob-1 << "); overhead=" << _global_overhead_factor << endl;
    }
    assert(matching_accept_prob - 1.0 <= ACCEPTANCE_PROBABILITY_MARGIN*numeric_limit_extras<precision_type>::epsilon());
  }
      
  return matching_accept_prob;
}  

// apply the updates needed once we know a matched emission has been accepted
// this returns the colour acceptance (needed later for MC@NLO matching)
double ShowerRunner::_matching_post_acceptance_update(Event & event,
                                                      typename ShowerBase::Element * element,
                                                      typename ShowerBase::EmissionInfo * emission_info){
    
  // #TRK_ISSUE-5464 GS:
  // - I've only had a brief look at this but we should do sth if phi, etc... are enforced
  // - we could think of using the "constrained" version of
  //   _select_channel_details but this would alter the weight so
  //   we'd have to be careful not to use it. Not doing it would
  //   however not work if one specifies the pdgid/splitter from
  //   the arguments
  // - if were not for the MC@NLO matching below, the three lines
  //   below could easily be moved to do_one_emission so as to avoid
  //   the extra penalty when computing the 1st Sudakov
  //
  // Note that the goal of this call is to enforce what matching
  // has put in emission_info, so we just let things go wo
  // imposing a potential user-requested flavour or channel.
  _select_channel_details(element, emission_info);

  // #TRK_ISSUE-5465 GS: apart from MC@NLO matching, it is not clear to me
  // why this call is needed (TBC e.g. checking if anything in
  // ColourRunner has changed)
  double colour_acceptance = (_use_first_sudakov_for_colour)
    ? _colour_transition_runner->colour_factor_acceptance_first_sudakov(event, *element, *emission_info)
    : _colour_transition_runner->colour_factor_acceptance              (event, *element, *emission_info);

  // finally, update the spin tree
  _spin_cor_tree.compute_phi_weight_and_overhead(*element, *emission_info);

  return colour_acceptance;
}

// apply the updates needed once we know a matched emission has been accepted
// this returns the colour acceptance (needed later for MC@NLO matching)
// This returns a probability that can go through handle_probability_lazy
double ShowerRunner::_matching_finalize_mcatnlo(typename ShowerBase::Element * element,
                                                typename ShowerBase::EmissionInfo * emission_info,
                                                double ps_acceptance,
                                                double matching_acceptance,
                                                double colour_acceptance){
  // #TRK_ISSUE-5466 GS: why does this return the difference between the
  //matching probability and the shower one? It seems to me like
  //we're only generating the 2nd contrution in Eq.(2.4) of the
  //matching paper?
  matching_acceptance -= colour_acceptance*ps_acceptance;
  // do not forget to include the global overhead
  matching_acceptance /= _global_overhead_factor;
  // the acceptance would just be a multiplicative factor
  return matching_acceptance;
}

// return true if the emission is accepted, false if it is vetoed
bool ShowerRunner::_post_matching_contour_veto(bool match_emission,
                                               typename ShowerBase::EmissionInfo * emission_info){

  // this only applies for the emissions after matching has been done 
  if (match_emission) return true;

  // this only applies if matching is active and needs vetoing
  if (!(_matching_ptr && _matching_ptr->matching_needs_vetoing())) return true;

  // check if the emission passes the veto, if the splitter is Born-labelled
  if (_matching_ptr->veto_emission(_lnvfirst, *emission_info)) return false;

  // veto on lnv if the splitter is not Born-labelled (to take care of secondary emissions
  // in case we're running with power showers)
  // #TRK_ISSUE-549 how does this handle the fact that the
  // "etabar==0" point is the bisector and not the colour
  // transition point?
  if (!_matching_ptr->is_splitter_born(*emission_info) && (emission_info->lnv > _lnvfirst)) return false;

  return true;
}

 
//--------------------------------------------------
// performs all the steps needed to update the event
// #TRK_ISSUE-603 GS-NEW-CODE: review this 
bool ShowerRunner::_update_event(Event & event,
                                 typename ShowerBase::Element * element,
                                 typename ShowerBase::EmissionInfo * emission_info) {

  // first update the event itself
  element->update_event(_colour_transition_runner, emission_info);

  // then, if needed, update the PDFs
  if (event.is_pp()){
    // update the beams. Note that our event evolution is such that
    // the incoming particles keep their initial indices, so we just
    // need to update x.
    event.set_pdf_x_beam1(event.pdf_x_beam1() * to_double(emission_info->beam1_pdf_new_x_over_old));
    event.set_pdf_x_beam2(event.pdf_x_beam2() * to_double(emission_info->beam2_pdf_new_x_over_old));

    // update the overhead factor
    _isr_overhead_factor =(*_isr_overhead_factor_handler)(event.pdf_x_beam1(), event.pdf_x_beam2(),
                                                          event.incoming1().pdgid(),
                                                          event.incoming2().pdgid());
    //if (_isr_overhead_factor>10){
    //  cout << "ISR overhead updated to " << _isr_overhead_factor << endl;
    //}
    _update_global_overhead_factor();
#ifdef PSVERBOSE
    int coutprec = cout.precision(10);
    cout << "pp event update, new overhead factors" 
         << ", x1,flv1 = " << event.pdf_x_beam1() << " " << event.incoming1().pdgid()
         << ", x2,flv2 = " << event.pdf_x_beam2() << " " << event.incoming2().pdgid()
         << ", isr = " << _isr_overhead_factor 
         << ", isr_safety = " << _isr_overhead_factor_handler->safety_factor() 
         << ", unc_hard = " << _unc_hard_overhead_factor 
         << ", unc_simkt = " << _unc_simkt_overhead_factor 
         << endl;
    cout.precision(coutprec);
    // instead of using the following line, remember that one can
    // view the table at creation with the -dump-pdf option
    //cout << *_isr_overhead_factor_handler << endl;
#endif // PSVERBOSE            
  } else if (event.is_DIS()){
    // in this case we have a single beam
    // determine which by asking who the parton is
    double pdf_x_beam;
    int    incoming_pid;
    if(event[0].is_parton() && event[0].is_initial_state()){
      event.set_pdf_x_beam1(event.pdf_x_beam1() * to_double(emission_info->beam1_pdf_new_x_over_old));
      pdf_x_beam   = event.pdf_x_beam1();
      incoming_pid = event.incoming1().pdgid(); 
    } else if (event[1].is_parton() && event[1].is_initial_state()) {
      event.set_pdf_x_beam2(event.pdf_x_beam2() * to_double(emission_info->beam2_pdf_new_x_over_old));
      pdf_x_beam   = event.pdf_x_beam2();
      incoming_pid = event.incoming2().pdgid();
    } else{
      assert(false && "DIS event with no initial-state partons - should not be able to reach here!");
    }

    // update the overhead factor
    _isr_overhead_factor =(*_isr_overhead_factor_handler)(pdf_x_beam, incoming_pid);
    _update_global_overhead_factor();
  }

  // record the event under request
  if (_do_cache_step_by_step_event){
    _step_by_step_event.push_back(element->emitter_index(), element->spectator_index(),
                                  emission_info->do_split_emitter,
                                  emission_info->lnv, emission_info->lnb,
                                  emission_info->phi, event);
  }
  
#ifdef PSVERBOSE
  // The line below validates the cached elements against the
  // dipole list
  event.print_following_dipoles();
  
  for (auto & dipole: event.dipoles()) {
    std::cout << "Dipoles [" << dipole.index_3bar() << "," << dipole.index_3() << "], next: " << dipole.index_next << ", prev: " << dipole.index_previous << " " << dipole.dirdiff_3_minus_3bar << "\n  " << event[dipole.index_3()].direction()-event[dipole.index_3bar()].direction() << std::endl;}
  for (auto & dipole: event.non_splitting_dipoles()) {
    std::cout << "NS Dipoles [" << dipole.index_3bar() << "," << dipole.index_3() << "], next: " << dipole.index_next << ", prev: " << dipole.index_previous << " " << dipole.dirdiff_3_minus_3bar << "\n  " << event[dipole.index_3()].direction()-event[dipole.index_3bar()].direction() << std::endl;}
  bool momcheck = event.check_momentum();
  cout  << "momentum check " << (momcheck ? "passes" : "FAILS") << endl;
  assert(momcheck);
  if (!event.check_dipoles_pdgids()){
    event.print_following_dipoles();
    assert(false && "found critical event inconsistency. Aborted.");
  } 
#endif // PSVERBOSE


  return true;
}
  

// Set up everything we need to match the upcoming emission
// Other strategies can be used for matched runs,
// by using RoulettePeriodic for the first emission only
// and then switching to the one requested by the user.
//  
// #TRK_ISSUE-683   - we still need to work out what happens if the main
//    strategy needs some kind of initialisation (not
//    the case for CentralRap, but we haven't ruled it out for others)
void ShowerRunner::_initialise_matching_for_event() {
  assert(_matching_ptr != nullptr && _shower_for_matching != nullptr);
  
  // set the shower to the matching shower
  _shower = _shower_for_matching;

  // Resets the tracked indices in case vetoing is needed after matching
  _matching_ptr->reset_splitter_index();

  // set up the matching strategy (set it to RouletteEnhanced or RoulettePeriodic)
  Strategy matching_strategy = (_strategy == RouletteEnhanced)
    ? RouletteEnhanced
    : RoulettePeriodic;
  
  if (_strategy != matching_strategy) {
    // #TRK_ISSUE-684  WATCH OUT: if the strategy used for matching gets changed, 
    // remember to change it also in the 3-jet-matching section of
    // AnalysisFramework, where it is hard-coded 
    _strategy = matching_strategy;
    _set_strategy_pointers();
    _matching_ptr->check_strategy_is_compatible(_strategy);
  }
  
  // RoulettePeriodic related stuff
  _find_element_alg_bias = _matching_ptr->find_element_alg_bias();
  if( _do_enhanced_roulette ) _enhanced_roulette.set_find_element_alg_bias(_find_element_alg_bias);
}

// Turn the matching off for the rest of the emissions in the event
void ShowerRunner::_terminate_matching_for_event(Event & event,
                                                 typename ShowerBase::EmissionInfo * emission_info) {
  // #TRK_ISSUE-556 GS-NEW-CODE: the ordering of the various steps here seems
  //important. We need to check the details of this to avoid
  //mistakes. Note that there is always the option of caching
  //extra things in emission info. One option might even be to
  //wrap emission_info in a bigger object that containg
  //matched_emission, emission_info, etc... that we can easily
  //carry around in ShowerRunner.
  
  // reset the shower to the main shower
  _shower = _main_shower;

  // decide which lnv to restart from
  // For PanScales showers with same beta, we should be able to just
  // continue from the same value of lnv.
  // If power shower is on, then start from the top.
  double lnv = emission_info->lnv;
  if (_matching_ptr->is_power_shower()) lnv = _lnvmax;

  //NOTE: at the moment, the code in the next line will not
  //re-initialise matching since it is only done if the size of the
  //event is 2. This would have to be rethought when we do matching
  //w more than 2 legs.
  _initialise_run(event, lnv, true);
  

  // reset the strategy to the original strategy
  if (_strategy != _strategy_no_matching) {
    _strategy = _strategy_no_matching;
    _set_strategy_pointers();
  }
  // RoulettePeriodic related stuff
  _find_element_alg_bias = 0.;
  if (_do_enhanced_roulette) _enhanced_roulette.set_find_element_alg_bias(0.);
}

// Returns true if Powheg-style matching is currently on for that event, i.e. if:
//   - the current trial emission is the first (event.can_be_matched() return value)
//   - matching is on, at all (_matching_ptr is set)
//   - the matching scheme is Powheg-like (only for unweighted events)
bool ShowerRunner::_match_current_emission(const Event & event, bool weighted) const {
  bool basic_cdt = (event.can_be_matched() && _matching_ptr);

  // for unweighted emissions, only PowHEG matching should be considered
  return (weighted)
    ? basic_cdt
    : (basic_cdt && (_matching_ptr && _matching_ptr->get_matching_scheme() == MatchingScheme::PowhegStyle));
}



//-----------------------------------------------------------------
// double-soft-related material
//-----------------------------------------------------------------

// guarantee that the splitter is set to the doube-soft partner
void ShowerRunner::_double_soft_set_splitter_to_partner(typename ShowerBase::EmissionInfo * emission_info) const{

  // make sure the splitting happens on the "double-soft
  // partner" side of the dipole
  if (emission_info->double_soft_partner_at_3_end){
    emission_info->do_split_emitter =  emission_info->emitter_was_3;
  } else {
    emission_info->do_split_emitter = !emission_info->emitter_was_3;
  }

  // make sure that it is a gluon!
  assert(emission_info->splitter().pdgid() == 21);

  // make sure that the outgoing splitter PDGid is correct
  if (emission_info->radiation_pdgid == 21){
    emission_info->splitter_out_pdgid=21;
  } else {
    emission_info->splitter_out_pdgid=-emission_info->radiation_pdgid;
  }

  //GS-NOTE: we should also check that the partner gluon is in the
  //final state
}
  
// handle the flavour swap
void ShowerRunner::_double_soft_flavour_swap(typename ShowerBase::EmissionInfo * emission_info) const{
  //Update on 2022-12-12: the next 2 lines are no longer valis: we
  //only update the splitter at this stage in case of a DS flavour
  //swap.
  // 
  // the "correct" splitter has already been updated by the shower
  // we need to set the PDG ids

  // We need to update the radiation id and potentially the
  // splitter_out_pdgid.  We also need to pay attention to the
  // identification of the splitter:
  //
  //   . If we have a g->qqbar splitting which becomes a g->gg
  //     splitting, gluon radiation can happen at any side of the
  //     dipole so we have some freedom to choose what we do. [in
  //     practice, we make sure the emitter is the DS partner]
  //
  //   . If instead we had a g->gg splitting which need to become a
  //     g->qqbar splitting, we neeed to make sure that we properly
  //     set the splitting side and update the splitter_out_pdgid

  _double_soft_set_splitter_to_partner(emission_info);
  
  //GS-NOTE-DS: think about a potential more clever way to do this?
  if (emission_info->radiation_pdgid == 21){
    // we had a g->gg splitting and should make this g->qqbar
    emission_info->radiation_pdgid = 1+gsl.uniform_int(_shower->qcd().nf());
    if (emission_info->splitter_is_3_end()){
      // the gluon that splits will become the quark. The emission the qbar
      emission_info->radiation_pdgid = -emission_info->radiation_pdgid;
    }
    // splitter get the opposite flavour
    emission_info->splitter_out_pdgid = -emission_info->radiation_pdgid;

  } else { // g->qqbar becomes g->gg
    emission_info->splitter_out_pdgid = emission_info->radiation_pdgid = 21;

    // here the PDG id of the splitter should remain the same as the
    // pre-branching one
  }
}


  

//========================================================================
//
// Helpers for approximately weighting the first emissions according to the Sudakov
//
//========================================================================


//-----------------------------------------------------------------
// Given a density that goes as a + b*lnv (i.e. implicitly a
// fixed-coupling approximation), and a current value of lnv, this
// routine generates and returns a random choice for the next lnv
inline double ShowerRunner::_sudakov_next_lnv(double current_lnv, double a, double b) {
  // density of emissions at the current value of lnv
  double density_now = a + b*current_lnv;

  // require lnv to be small enough that the density is at least zero
  if (density_now < 0.0) {
    current_lnv = -a/b;
    density_now = 0.0;
  }

  // the random number to decide the next emission
  double r = gsl.uniform();

  // figure out the next lnv value by solving the equation
  // r = exp(-\int^{current_lnv}_{current_lnv - delta_minus_lnv} dlnv (a + b*lnv) )
  //
  // Note that r is in the [0:1) range (including 0 but excluding
  // 1). Inverting the above expression would give a log(r) which is
  // dangerous if r=0. Instead, we use log1p(-r)=log(1-r) which does
  // not have that issue (it is equivalent to replacing r by 1-r in
  // the above expression.
  double delta_minus_lnv = (density_now - sqrt(pow2(density_now) + 2*b*log1p(-r)))/b;
  return current_lnv - delta_minus_lnv;
}


//-----------------------------------------------------------------
// Compute the Sudakov form factor integrand w.r.t lnv and lnb for
// the given element.
//
// #TRK_ISSUE-616  GS-NOTE: haven't checked yet if this needed adjustments for pp (left for later)
double ShowerRunner::_lnv_lnb_integrand(
  Event & event, typename ShowerBase::Element * element, double lnv, double lnb,
  bool integrate_phi, double precision) {

  // Fake(ish) EmissionInfo to use as input to acceptance_probability
  // and _colour_factor_acceptance.
  std::unique_ptr<ShowerBase::EmissionInfo> emission_info(_shower->create_emission_info());
  //typename Shower::EmissionInfo emission_info;
  emission_info->set_element(element);
  emission_info->lnv = lnv;
  emission_info->lnb = lnb;

  // #TRK_ISSUE-617 GS-NEW-CODE: the new code would be used by using the few lines
  //below instead of the ones after. We have not done the switch yet
  //because the code is quite slower. We have checked that it gives
  //the same Sudakov as the "old" code but does not produce the same
  //events as it does not keep random numbers in sync.
  //
  // #TRK_ISSUE-618 GS-NEW-CODE: TODO: for the integrated case, also sum over
  //splitting ends and PDGId [also need to add support for that in
  //do_one_emission_split]
  //
  // #TRK_ISSUE-619 GS-NEW-CODE: in this case we also need to disable the veto so as
  //to avoid having cases where the event is aborted
  double weight = 1.0;
  GenerationMode phi_mode = GenerationIntegrateOrSum;
  if (!integrate_phi){
    phi_mode = GenerationFromEmissionInfo;
    emission_info->phi = 0.0;
  }  
  bool mcatnlo_subtract_ps_for_weighted_orig = _mcatnlo_subtract_ps_for_weighted;
  bool use_first_sudakov_for_colour_orig = _use_first_sudakov_for_colour;
  _mcatnlo_subtract_ps_for_weighted = false;
  _use_first_sudakov_for_colour = true;
  unique_ptr<EmissionVeto<ShowerBase>> current_veto = std::move(_veto_ptr);
  EmissionAction status = do_accept_emission(event, emission_info, true, weight, phi_mode);
  _veto_ptr = std::move(current_veto);
  _mcatnlo_subtract_ps_for_weighted = mcatnlo_subtract_ps_for_weighted_orig;
  _use_first_sudakov_for_colour = use_first_sudakov_for_colour_orig;
  return (status==EmissionAction::accept) ? weight : 0.0;

  
  //Non-split-version: auto emission_weight = [&](double phi) -> double{
  //Non-split-version:   double weight = 1.0;
  //Non-split-version:   bool mcatnlo_subtract_ps_for_weighted_orig = _mcatnlo_subtract_ps_for_weighted;
  //Non-split-version:   bool use_first_sudakov_for_colour_orig = _use_first_sudakov_for_colour;
  //Non-split-version:   _mcatnlo_subtract_ps_for_weighted = false;
  //Non-split-version:   _use_first_sudakov_for_colour = true;
  //Non-split-version:   EmissionAction status = do_accept_emission(event, emission_info, true, weight, phi);
  //Non-split-version:   _mcatnlo_subtract_ps_for_weighted = mcatnlo_subtract_ps_for_weighted_orig;
  //Non-split-version:   _use_first_sudakov_for_colour = use_first_sudakov_for_colour_orig;
  //Non-split-version:   return (status==EmissionSuccess) ? weight : 0.0;    
  //Non-split-version: };
  //Non-split-version: 
  //Non-split-version: if (!integrate_phi){
  //Non-split-version:   return emission_weight(0.0);
  //Non-split-version: }
  //Non-split-version: 
  //Non-split-version: // for stability reasons, simplify the periodicity:
  //Non-split-version: //   \int_0^(2pi) dphi/(2pi) f(phi)
  //Non-split-version: //   = \int_0^(pi/2) dphi/(2pi) [f(phi)+f(phi+pi/2)+f(phi+pi)+f(phi+3pi/2)]
  //Non-split-version: auto symmetrised_emission_weight = [&](double phi) -> double{
  //Non-split-version:   return emission_weight(phi) + emission_weight(phi+0.5*M_PI) + emission_weight(phi+M_PI) + emission_weight(phi+1.5*M_PI);
  //Non-split-version: };
  //Non-split-version: return dgauss(0.0, 0.5*M_PI, symmetrised_emission_weight, precision/100.0, 64, 128)/(2*M_PI);
  
  //old: // The Sudakov integrand for the current element, w.r.t its lnv and
  //old: // lnb, modulo the running coupling (to follow). 
  //old: double integrand = 0.;
  //old: 
  //old: if(!_matching_ptr) {
  //old:   integrand = _acceptance_probability(element, emission_info.get());
  //old: 
  //old:   // this is only needed if we had something non-trivial
  //old:   if (integrand==0){ return 0.0; }
  //old:   if (!element->check_after_acceptance_probability(emission_info.get()))
  //old:     return 0.0;
  //old: 
  //old:   // colour factor corrections
  //old:   integrand *= _colour_transition_runner->colour_factor_acceptance_first_sudakov(event, *element, *emission_info);
  //old: } else {
  //old:   // #TRK_ISSUE-620 GS-NOTE: matching will not work w pp events. We probably should do sth about this
  //old:   if (event.is_pp() || event.is_DIS())
  //old:     assert(false && "ShowerRunner::_lnv_lnb_integrand: Matching is not implemented for pp and DIS events");
  //old:  
  //old:   //
  //old:   // Next steps follow ShowerRunner::run for the case _do_matching = true.
  //old:   //
  //old:   // The MatchedProcess will almost always overwrite this PS acceptance
  //old:   // probability, unless it hits some highly singular & numerically 
  //old:   // unstable kinematic point, in which case it will return this value.
  //old:   double accept_prob = _acceptance_probability(element, emission_info.get());
  //old:   if (! emission_info->acceptance_probability_was_true) return 0.0;
  //old:   
  //old:   // #TRK_ISSUE-621 GS-NOTE: Do we want to bail out if the acceptance probability is 0?
  //old:   //         What happens if this fails?
  //old:   //         Do we want to ensure do_kinematics_always_true_if_accept()?
  //old:   if (!element->check_after_acceptance_probability(emission_info.get()))
  //old:     return 0.0;
  //old: 
  //old:   // Thus far we are only considering ee → qqg hence the following 4 lines.
  //old:   // In dipole showers only the emitter splits. Here we also pretend that
  //old:   // this holds for antenna showers: in the present context we are simply 
  //old:   // trying to integrate over the dipole phase space, with the present function
  //old:   // being called from within a loop over dipole elements.
  //old:   //
  //old:   // #TRK_ISSUE-622  LS-2022-11-08: now we have H->gg we should modify this accordingly
  //old:   //
  //old:   emission_info->do_split_emitter = true;
  //old:   emission_info->radiation_pdgid = 21;
  //old:   emission_info->splitter_out_pdgid = emission_info->splitter().pdgid();
  //old:   emission_info->phi = 0;
  //old:   //
  //old:   // Construct the post-branching momenta and store in emission_info
  //old:   bool outcome = _do_kinematics(element, emission_info.get());
  //old:   if (_shower->do_kinematics_always_true_if_accept())
  //old:     assert(outcome && "outcome must be true when do_kinematics_always_true_if_accept() is and integrand > 0");
  //old:   if (!outcome) integrand = 0;
  //old:   _matching_ptr->acceptance_probability(event, *element, *emission_info, accept_prob);
  //old:   integrand = accept_prob;
  //old: 
  //old:   // #TRK_ISSUE-623 GS-NOTE: don't we need a colour acceptance probability here? [has it disappeared in a merging?]
  //old: }
  //old: 
  //old: // If the integrand is zero now, it will be zero come what may.
  //old: if(integrand==0.0) return 0.0;
  //old: 
  //old: // Including running coupling contribution to the Sudakov integrand.
  //old: integrand *= _alphas_acceptance_probability(element, emission_info.get());
  //old: 
  //old: // handle the phi integration if needed
  //old: if (integrate_phi){
  //old:   auto phi_integrand = [&](double phi) {
  //old:     emission_info->phi = phi;
  //old:     _select_channel_details(element, emission_info.get());
  //old:     if (! _do_kinematics(element, emission_info.get())) return 0.0;
  //old:     // include potential double-soft corrections
  //old:     return element->double_soft_acceptance_probability(emission_info.get());
  //old:   };
  //old:   // #TRK_ISSUE-625  GPS-QUERY: is some comment needed about 64/128 choice (e.g. is it
  //old:   // that high to avoid problems when there is a small phase-space
  //old:   // region that is problematic?)
  //old:   integrand *= dgauss(0.0, 2*M_PI, phi_integrand, precision/100.0, 64, 128)/(2*M_PI);
  //old: } else {
  //old:   // for pp events we still need to check if the kinematics is fine
  //old:   // at phi=0 (assuming phi symmetry)
  //old:   if (event.is_pp() || event.is_DIS() ){
  //old:     emission_info->phi = 0.0;
  //old:     _select_channel_details(element, emission_info.get());
  //old:     if (!_do_kinematics(element, emission_info.get())) return 0.0;
  //old:   }
  //old:   // include potential double-soft corrections
  //old:   integrand *= element->double_soft_acceptance_probability(emission_info.get());
  //old: }    
  //old: 
  //old: // Constants from crude PS distribution
  //old: // #TRK_ISSUE-626 GS-NOTE: this needs to be multiplied by the overhead factor
  //old: integrand *= _global_overhead_factor * element->lnv_lnb_max_density();
  //old: 
  //old: // #TRK_ISSUE-627 debug:
  //old: //std::cout << element->dipole_index() << " " << element->emitter_index() << " " << lnb << " " << integrand << std::endl;
  //old:   
  //old: return integrand;
}


// integration of the single-emission weight between lnvmin and
// lnvmax using a Gaussian Quadrature approach.
//  - event      the initial event to emit from
//  - element    within the event, emit from this element
//  - lnv_min    lower integration bound
//  - lnv_max    upper integration bound
//  - precision  target accuracy
//  - integrate_phi    when true, integrate over phi and
//                     test the outcome of do_kineamtics.
double ShowerRunner::_integrate_over_lnv_gauss(Event & event, typename ShowerBase::Element *element,
                                               double lnv_min, double lnv_max,
                                               double precision,
                                               bool integrate_phi) {
  // an alternative version
  // lambda function returning the Sudakov integrand w.r.t dlnv for element
  auto lnv_integrand_v2 = [&](double lnv) -> double {
    
    // Make sure element is 'up-to-date' in case any following functions
    // in _lnv_lnb_integrand / _gq_integration_lnb_limits use a/astar/b.
    element->update_cache(lnv);

    // lambda function returning Sudakov integrand w.r.t dlnv dlnb for element
    auto lnb_integrand_at_fixed_lnv = [&](double lnb) {
      return _lnv_lnb_integrand(event,element,lnv,lnb,integrate_phi,precision);
    };
    
    Range lnb_approx_range = element->lnb_generation_range(lnv);
    if (lnb_approx_range.extent() <= 0) return 0.;

    // split the range in 2 in case the middle is below lnktcut
    
    double lnb_range_min = lnb_approx_range.min();
    double lnb_range_max = lnb_approx_range.max();
    double lnb_range_mid = 0.5*(lnb_range_min+lnb_range_max);
    
    double lnb_integral = 0;

    for (unsigned int i_half=0; i_half<2; ++i_half){
      double lnb_approx_min, lnb_approx_max;
      if (i_half==0){
        lnb_approx_min = lnb_range_min;
        lnb_approx_max = lnb_range_mid;
      } else {
        lnb_approx_min = lnb_range_mid;
        lnb_approx_max = lnb_range_max;
      } 

      // tune the bounds
      //
      // first check we are not below the cut-off in αS for all lnb associated
      // to the current lnv. 
      // #TRK_ISSUE-632  ATTENTION
      // This step assumes:
      // i)  β_ps>=0 showers
      // ii) for the 1st emission alphas_lnkt = lnv + β_ps |lnb| 
      // where at time of writing ii) is true for all our showers.
      // We might want to re-investigate whether this step is needed.
      double alphaslnkt_min = element->alphas_lnkt(lnv, lnb_approx_min);
      double alphaslnkt_max = element->alphas_lnkt(lnv, lnb_approx_max);
      if((alphaslnkt_min <=_shower->qcd().lnkt_cutoff()) &&
         (alphaslnkt_max <=_shower->qcd().lnkt_cutoff()))
        continue;

      // if only one of the 2 edges is below the cutoff, adjust that edge
      const double sqrt_epsilon = numeric_limit_extras<double>::sqrt_epsilon();
      double lnb_below, lnb_above;
      auto bin_search = [&]() -> void {
        // lnb_below is below the cutoff
        // lnb_above is above the cutoff
        while(std::abs(1.-lnb_above/lnb_below)>sqrt_epsilon) {
          double lnb_mid=(lnb_below+lnb_above)/2.;
          if (element->alphas_lnkt(lnv, lnb_mid)<=_shower->qcd().lnkt_cutoff())
            lnb_below=lnb_mid;
          else
            lnb_above=lnb_mid;
        }
      };
    
      // the min is below, the max is above. Adapt the min
      if (alphaslnkt_min <=_shower->qcd().lnkt_cutoff()){
        lnb_below = lnb_approx_min;
        lnb_above = lnb_approx_max;
        bin_search();
        // here we're making an O(sqrt(epsilon)) mistake ensuring positivity
        lnb_approx_min = lnb_above;
        _gq_integration_lnb_limits(event,element,lnv,integrate_phi,precision,
                                   lnb_approx_min,lnb_approx_max);
        // undo the O(sqrt(epsilon)) 
        lnb_approx_min = lnb_below-std::abs(lnb_above-lnb_below);
      } else if (alphaslnkt_max <=_shower->qcd().lnkt_cutoff()){
        // the max is below, the min is above. Adapt the max
        lnb_below = lnb_approx_max;
        lnb_above = lnb_approx_min;
        bin_search();
        // here we're making an O(sqrt(epsilon)) mistake ensuring positivity
        lnb_approx_max = lnb_above;
        _gq_integration_lnb_limits(event,element,lnv,integrate_phi,precision,
                                   lnb_approx_min,lnb_approx_max);
        // undo the  O(sqrt(epsilon)) 
        lnb_approx_max = lnb_below+std::abs(lnb_above-lnb_below);
      } else {
        // adjust both ends
        _gq_integration_lnb_limits(event,element,lnv,integrate_phi,precision,
                                   lnb_approx_min,lnb_approx_max);
      }

      // now that we have an interval, we split the interval in 3        
      unsigned n_intervals  = 3;

      // we want to put more emphasis on the edges. If possible, we
      // just integrate a few units in lnb at each edge
      double delta_lnb_approx = (lnb_approx_max-lnb_approx_min);
      const double targetted_dlnb = 5.0;
      std::vector<double> bounds;
      if (delta_lnb_approx > 3*targetted_dlnb){
        // we take two margins on the side and split the middle
        // interval in three
        double mid_min = lnb_approx_min+targetted_dlnb;
        double mid_max = lnb_approx_max-targetted_dlnb;
        bounds = {lnb_approx_min, mid_min, (2*mid_min+mid_max)/3,
                  (mid_min+2*mid_max)/3, mid_max, lnb_approx_max};
        n_intervals = 5;
      } else {
        bounds = {lnb_approx_min, (2*lnb_approx_min+lnb_approx_max)/3,
                  (lnb_approx_min+2*lnb_approx_max)/3, lnb_approx_max};
      }

      for (unsigned i_int=0; i_int<n_intervals; i_int++) {
        
        // Limits for current lnb interval integration
        double lnb_approx_int_min = bounds[i_int];
        double lnb_approx_int_max = bounds[i_int+1];
        double integral = dgauss(lnb_approx_int_min, lnb_approx_int_max,
                                 lnb_integrand_at_fixed_lnv, precision/10, 64, 128);
        lnb_integral += integral;
      }
    }
    return lnb_integral;
  };

  //
  // GQ integration over dlnv integrand: dlnb will be integrated at each lnv.
  return dgauss(lnv_min, lnv_max, lnv_integrand_v2, precision, 64, 128);
}


// integration of the single-emission weight between lnvmin and
// lnvmax using a Monte Carlo approach.
//  - event      the initial event to emit from
//  - elements   list of elements to include in the integral
//  - lnv_min    lower integration bound
//  - lnv_max    upper integration bound
//  - nev        number of Monte Carlo points
//  - result     filled by this function with the result of the integration
//  - error      filled by this function with the estimated integration error
void ShowerRunner::_integrate_over_lnv_mc(Event & event,
                                          std::vector<std::unique_ptr<typename ShowerBase::Element> > &elements,
                                          double lnv_min, double lnv_max,
                                          uint64_t nev,
                                          double &result, double &error){

  AverageAndError avg_lnsudakov;
  // Integrate Sudakov by MC
  //
  // #TRK_ISSUE-634  GPS-20190330: perhaps call this log_sudakov?
  for (uint64_t iev = 0; iev < nev; iev++) {

    double weight = 1.0;
    unsigned nelements = elements.size();

    // work out which of the elements we will use
    //
    // #TRK_ISSUE-635 GS-NOTE: does the lines below assume that all the elements
    //have the same weight?
    weight *= nelements;
    unsigned ielement = gsl.uniform_int(nelements);
    auto first_element = elements[ielement].get();

    // generate lnv
    double lnv = gsl.uniform(lnv_max, lnv_min);
    weight *= (lnv_max - lnv_min);
    
    // now generate lnb
    Range range = first_element->lnb_generation_range(lnv);
    weight *= range.extent();
    double lnb = gsl.uniform(range.min(), range.max());

    // #TRK_ISSUE-637 GS-NOTE: how do we handle cases where
    //  (this->*_find_lnb_alg_ptr)(element, lnv, lnb);
    // would return false? [we probably don't care?]

    // include the kinematic acceptance and the alphas acceptance
    //GPS
    std::unique_ptr<ShowerBase::EmissionInfo> emission_info(_shower->create_emission_info());
    emission_info->set_element(first_element);
    emission_info->lnv = lnv;
    emission_info->lnb = lnb;

    // #TRK_ISSUE-639 GS-NEW-CODE: the next 5 lines are sufficient except for a few things:
    //
    // - we systematically call do_kinematics, hence there's no more
    //   need for the "_first_sudakov" version of the colour
    //   acceptance
    //
    // - for matched events, one needs to cann
    //   "select_channel_details" a second time in the "old" code in
    //   order to recover the behaviour of the 5 lines below (this
    //   only affects the synchronisation of the random numbers with
    //   no real physical effects)
    //
    // - the default weighted behaviour for MC@NLO matching is to
    //   include the subtraction term in the matching
    //   probability. This is not the case for the computation of
    //   the first Sudakov. As noted above, it is not totally clear
    //   to me how weighted events work w MC@NLO, so I am not sure
    //   how to fix this properly. For now, we apply a dirty fix of
    //   using a variable in the class.
    //
    // See the GS-NEW-CODE[validation] comments below about tweaks
    // to the "old" code needed to match the new one.
    //
    // We also need to temporarily disable a potential veto (and
    // restore it immediately after).  Note also that the colour
    // treatment here can live with the default setup and is not
    // forced to use the "_first_sudakov" version because MC
    // integration can handle the randomness of the channel selection.
  
    bool mcatnlo_subtract_ps_for_weighted_orig = _mcatnlo_subtract_ps_for_weighted;
    _mcatnlo_subtract_ps_for_weighted = false;
    unique_ptr<EmissionVeto<ShowerBase>> current_veto = std::move(_veto_ptr);    
    EmissionAction status = do_accept_emission(event, emission_info, true, weight);
    _veto_ptr = std::move(current_veto);
    _mcatnlo_subtract_ps_for_weighted = mcatnlo_subtract_ps_for_weighted_orig;
    avg_lnsudakov += ((status==EmissionAction::accept) ? weight : 0.0);
  }
  assert(avg_lnsudakov.n_entries() == nev);

  result = avg_lnsudakov.average();
  error  = avg_lnsudakov.error();
}
  
  
//-----------------------------------------------------------------
// Compute the lnb integration limits for the Gaussian Quadrature
// integration mode inside compute_first_sudakov.
//
// #TRK_ISSUE-615  GS-NOTE: haven't checked yet if this needed adjustments for pp (left for later)
// But I think this one and the next one need adjustments to include phi
void ShowerRunner::_gq_integration_lnb_limits(Event & event, 
                                              typename ShowerBase::Element * element, double lnv,
                                              bool integrate_phi, double precision,
                                              double & lnb_min, double & lnb_max ) {

  // Copy initial envelope lnb_min & lnb_max values to work from.
  double lnb_approx_min = lnb_min;
  double lnb_approx_max = lnb_max;

  double lnb_lo, lnb_hi;
  double sqrt_epsilon = numeric_limit_extras<double>::sqrt_epsilon();

  // A function that assumes _lnv_lnb_integrand transitions from <=0
  // at lnb_lo to >0 by lnb_hi and tries to find to find the transition
  // point up to relative sqrt_epsilon uncertainty. 
  auto bin_search = [&]() -> void {
    while(std::abs(1.-lnb_hi/lnb_lo)>sqrt_epsilon) {
      double lnb_mid=(lnb_lo+lnb_hi)/2.;
      if(_lnv_lnb_integrand(event,element,lnv,lnb_mid,integrate_phi,precision)>0.) lnb_hi=lnb_mid;
      else lnb_lo=lnb_mid;
    }
  };

  // First : cases where _lnv_lnb_integrand is <=0 at lnb_approx_min
  if(_lnv_lnb_integrand(event,element,lnv,lnb_approx_min,integrate_phi,precision)<=0) {

    // Working assumption for this first if(...) block:
    // _lnv_lnb_integrand has one and only one transition from 0 
    // to >0 in [lnb_approx_min,lnb_approx_max]. Otherwise, the
    // code below will just find one of the number of transitions.
    //
    // If _lnv_lnb_integrand is <=0 at lnb_approx_min and >0 at 
    // lnb_approx_max, set lnb_min to transition point (-margin)
    // and lnb_max to lnb_approx_max.
    if(_lnv_lnb_integrand(event,element,lnv,lnb_approx_max,integrate_phi,precision)>0) {
      lnb_lo=lnb_approx_min;
      lnb_hi=lnb_approx_max;
      bin_search(); // get _lnv_lnb_integrand transition from <=0 to >0
      lnb_min=std::max(lnb_lo-std::abs(lnb_hi-lnb_lo),lnb_approx_min);
      lnb_max=lnb_approx_max;

    // Working assumption for next part of if(...) block:
    // _lnv_lnb_integrand is only zero directly above lnb_approx_min
    // and directly below lnb_approx_max, i.e. has form __/‾‾\__ .
    //
    // If _lnv_lnb_integrand <=0 at lnb_approx_min & <=0 lnb_approx_max,
    // we first look for a positive integrand along lnb with a crude
    // linear search, then use bin_search to locate transitions from
    // <=0 to >0 precisely on each side. If no 'hump' is detected we
    // assume we were too crude in looking for it, and revert to original
    // overestimated lnb limits.
    } else {
      // lnb_min = lnb_approx_min;
      // lnb_max = lnb_approx_max;
      // return;
      //
      // Crude linear search to look for a point on a possible 'hump'
      // between lnb_approx_min and lnb_approx_max.
      double lnb_search_increment = std::abs(lnb_approx_max-lnb_approx_min)/100;
      double lnb_search_point     = lnb_approx_min-lnb_search_increment;
      unsigned n_failures   =  0;
      unsigned max_failures =  4;
      bool   found_positive_integrand_in_min_max = true;
      while(true) {
        lnb_search_point+=lnb_search_increment;
        if(_lnv_lnb_integrand(event,element,lnv,lnb_search_point,integrate_phi,precision)>0) break;
        // If we searched through the region and didn't find any +ve
        // point, search again with 10x finer lnb increments.
        if(lnb_search_point>=lnb_approx_max) {
          n_failures++;
          if(n_failures==max_failures) {
            found_positive_integrand_in_min_max = false;
            break;
          }
          lnb_search_increment/=10;
          lnb_search_point=lnb_approx_min-lnb_search_increment;
        }
      }
      // If we find a point >0 in [lnb_approx_min, lnb_approx_max], bin_search
      // on both sides to determine start & end of >0 hump region (-/+margin)
      if(found_positive_integrand_in_min_max) {
        // Get low lnb end of hump.
        lnb_lo=lnb_approx_min;
        lnb_hi=lnb_search_point; // point on the >0 hump from linear search.
        bin_search();
        lnb_min=std::max(lnb_lo-std::abs(lnb_hi-lnb_lo),lnb_approx_min);

        // Get high lnb end of hump.
        lnb_lo=lnb_approx_max;
        lnb_hi=lnb_search_point; // point on the >0 hump from linear search.
        bin_search();
        lnb_max=std::min(lnb_lo+std::abs(lnb_hi-lnb_lo),lnb_approx_max);

        // If the approximate linear search didn't find any sign of a hump
        // it may not have been fine enough. So we use overestimated
        // lnb_min=lnb_approx_min & lnb_max=lnb_approx_max to cover ourselves.
      } else {
        lnb_min = lnb_approx_min;
        lnb_max = lnb_approx_max;
      }
    }
  } else {
    // Second : cases where the integrand is >0 at lnb_approx_min
    //
    // If _lnv_lnb_integrand>0 at lnb_approx_max too, must integrate whole interval.
    if(_lnv_lnb_integrand(event,element,lnv,lnb_approx_max,integrate_phi,precision)>0) {
      lnb_min = lnb_approx_min;
      lnb_max = lnb_approx_max;
      // Working assumption for next bit of if(...).
      // _lnv_lnb_integrand has one and only one transition from >0 to <=0 in 
      // [lnb_approx_min,lnb_approx_max]. Otherwise, the code below will find
      // just one of the number of transition points.
      //  
      // If _lnv_lnb_integrand<=0 at lnb_approx_max bin_search sets upper integration
      // limit to point where _lnv_lnb_integrand transitions from >0 to <=0 (+margin).
    } else {
      lnb_lo=lnb_approx_max;
      lnb_hi=lnb_approx_min;
      bin_search();
      lnb_min = lnb_approx_min;
      lnb_max=std::min(lnb_lo+std::abs(lnb_hi-lnb_lo),lnb_approx_max);
    }
  }
  return;
}

    
//========================================================================
///
/// Helpers for handling the element store
///
//========================================================================
  

//-----------------------------------------------------------------
// Initialise shower element info storage based on the input event
void ShowerRunner::_add_dipole_elements_to_store(Event &event, unsigned int dipole_index, double lnv){

  // create the elements
  // append them to the store an process the cached info
  auto elements = _shower->elements(event, dipole_index);
  for (auto & element : elements){
    element->enhancement_factor(_enhancement_factor);
    element->update_cache(lnv);
    double norm_a     = element->norm_a();
    double norm_astar = element->norm_astar();
    double norm_b     = element->norm_b();

    // KH 8-Sep-2021 : max_astar can actually increase in here (very rarely).
    if(norm_astar>this->_max_astar) {
      this->_max_astar   = norm_astar;
      _max_astar_o_min_b = _max_astar/_min_b;
    }
    if(norm_b<this->_min_b) {
      this->_min_b   = norm_b;
      _max_astar_o_min_b = _max_astar/_min_b;
    }
    // std::cout << "Norm_b for dipole index = " << dipole_index << ", " << norm_b << " " << _shower->max_alphas() << " qcd.lnkt_cutoff() = " << _shower->qcd().lnkt_cutoff() << std::endl;
    _tot_a     += norm_a;
    _tot_b     += norm_b;
    _tot_astar += norm_astar;

    _elements.push_back(std::move(element));
    if (_do_enhanced_roulette)
      _enhanced_roulette.update(_elements.size()-1, _enhancement_factor,
                                norm_astar, norm_a, norm_b);
  }
}

//-----------------------------------------------------------------
// Initialise shower element info storage based on the input event
//
// #TRK_ISSUE-605  GS-NOTE: with the switch to the use of unique_ptr, I've had to
// write this independently of the next one. There's certainly a
// cleaner way of doing this but it might depend on whether we find a
// more clever way of creating the elements() so it's left aside for
// now.
inline void ShowerRunner::_update_element_in_store(unsigned int element_index, double lnv) {
   typename ShowerBase::Element * element = _elements[element_index].get();
 
   // get element's old astar, a, b
   double last_astar = element->norm_astar();
   double last_a     = element->norm_a();
   double last_b     = element->norm_b();
   
   // update the elements and get their values
   double new_a, new_b, new_astar;
   element->update_cache(lnv, new_a, new_b, new_astar);
 
   // #TRK_ISSUE-606  It seems dipoles neighbouring the parent dipole can INCREASE
   // their mass from pre- to post-branching, hence:
   if(new_astar>this->_max_astar) {
     this->_max_astar   = new_astar;
     _max_astar_o_min_b = _max_astar/_min_b;
   }
   if(new_b<this->_min_b) {
     this->_min_b   = new_b;
     _max_astar_o_min_b = _max_astar/_min_b; 
   }

   // update the total
   _tot_astar += new_astar - last_astar;
   _tot_a     += new_a     - last_a;
   _tot_b     += new_b     - last_b;

  // Update book-keeping inside EnhancedRoulette
  if(_do_enhanced_roulette)
    _enhanced_roulette.update(element_index, element->enhancement_factor(),
                              element->enhancement_factor(),
                              last_astar, new_astar, new_a, last_b, new_b);
  return;
}

//-----------------------------------------------------------------
// Initialise shower element info storage based on the input event
void ShowerRunner::_update_element_in_store(unsigned int element_index, double lnv,
                                            std::unique_ptr<typename ShowerBase::Element> & new_element) {
  // get the current element
  typename ShowerBase::Element * last_element = _elements[element_index].get();

  // get element's old astar, a, b
  double last_astar = last_element->norm_astar();
  double last_a     = last_element->norm_a();
  double last_b     = last_element->norm_b();

  auto last_enhancement_factor = last_element->enhancement_factor();

  // replace the element in store
  _elements[element_index] = std::move(new_element);
  
  // update the elements and get their values
  double new_a, new_b, new_astar;
  _elements[element_index]->enhancement_factor(last_enhancement_factor);
  _elements[element_index]->update_cache(lnv, new_a, new_b, new_astar);

  // #TRK_ISSUE-607  It seems dipoles neighbouring the parent dipole can INCREASE
  // their mass from pre- to post-branching, hence:
  if(new_astar>this->_max_astar) {
    this->_max_astar   = new_astar;
    _max_astar_o_min_b = _max_astar/_min_b;
  }
  if(new_b<this->_min_b) {
    this->_min_b   = new_b;
    _max_astar_o_min_b = _max_astar/_min_b; 
  }

  // update the total
  _tot_astar += new_astar - last_astar;
  _tot_a     += new_a     - last_a;
  _tot_b     += new_b     - last_b;

  // Update book-keeping inside EnhancedRoulette
  if(_do_enhanced_roulette)
    _enhanced_roulette.update(element_index, last_enhancement_factor,
                              _elements[element_index]->enhancement_factor(),
                              last_astar, new_astar, new_a, last_b, new_b);
}


//-----------------------------------------------------------------
// Initialise shower element info storage based on the input event
void ShowerRunner::_update_elements_in_store_pre_splitting(Event &event, typename ShowerBase::Element * element, double lnv) {

  // EmissionVeto may want to modify element properties at this point.
  // If so, update_element_in_store takes care of tot_astar, tot_a, tot_b
  // in shower cache, and update_max_astar_min_b takes care of the rest.
  if(_veto_ptr && _veto_ptr->modify_elements_pre_splitting(event,*element,lnv)) {
    for (unsigned int i_ele=0; i_ele<_elements.size(); i_ele++) {
      bool update_element_in_store = _veto_ptr->modify_element_pre_splitting(event,*(_elements[i_ele].get()),lnv);
      if(update_element_in_store) _update_element_in_store(i_ele,lnv);
    }
    _update_max_astar_min_b(lnv);
  }
}



//-----------------------------------------------------------------
// Update elements in dipoles kinematically affected by last branching
void ShowerRunner::_update_elements_in_store_post_splitting(Event &event, double lnv, typename ShowerBase::EmissionInfo *emission_info) {

  // register the info about the lnkt for this branching
  event.add_branching_history_element(emission_info->alphas_lnkt);

  // Indices of the new dipole, the parent dipole, and its neighbours
  const std::vector<unsigned int> & dipole_indices = event.touched_dipole_indices();
  assert(dipole_indices.size() >= 2U 
         && "following code expects at least two dipoles have been touched");
  unsigned int n = _shower->n_elements_per_dipole(); // for brevity

  //double lnv = emission_info->lnv;
  
  // if the number of dipoles has changed by 1, we need to insert the
  // elements associated with this dipole
  unsigned int starting_update_index=0;
  unsigned int last_update_index=1;
  if (event.dipoles().size() == (_cached_ndipoles+1)){
    // dipole_indices is built so that entry 0 is the the newly created dipole.
    _add_dipole_elements_to_store(event,dipole_indices[0], lnv);
    starting_update_index = 1;
    _cached_ndipoles += 1;
  } else {
    // make sure the number of dipoles stayed the same
    assert(event.dipoles().size() == _cached_ndipoles);
  }

  // handle the case where we have had a momentum swap in double-soft
  if (emission_info->colour_flow_swap){
    // 4 cases: gluon/quark emission on the 3/3bar end for the quark
    // case, the update is already correct (see comment below)
    if (emission_info->radiation_pdgid == 21){ // gluon radiation
      if (emission_info->splitter_is_3_end()){
        // the touched chain is ([...] means optional)
        //   [new, existing, previous[, next]}
        // existing and previous should be fully updated
        //
        // This is a bit overkill as the entry 0 will be updated twice
        // (once fully, once kinematically)
        last_update_index = 2;
      } else { // splitter is 3bar end
        // unless we want to edit the const vector, we have to update
        // the whole chain
        last_update_index = dipole_indices.size()-1;
      }
    }    
  }
  

  // then, element(s) up to (and including) 1, which include the
  // recoiling dipole) also need a special treatment as their indices
  // may have changed
  //
  //GS-NOTE-DS(2022-04-29). I think we're doing a bit too much here.
  //
  // For the cases with a new dipole, this has
  // starting_update_index==1 and we just update the entry [1] which
  // is the existing dipole. This is fine.
  //
  // For the cases wo a new dipole, entry[0] is the existing dipole
  // and entry[1] is enther the previous or next dipole (the one that
  // received the new emission). In principles, we only need to update
  // the indices of entry[1], while we could live with updating only
  // the kinematics of [0].
  //
  // Note that in the treatment of double-soft, we're using the fact
  // that the first 2 elements are upated, so if the code below gets
  // updated, we need to make sure it does not backfire on the
  // treatment of double-soft.
  //
  // TBC.
  for (unsigned int update_index = starting_update_index; update_index <= last_update_index; ++update_index){
    unsigned int dipole_index = dipole_indices[update_index];

    auto new_elements = _shower->elements(event, dipole_index);
    ///
    for (unsigned int elm_idx_in_dipole=0; elm_idx_in_dipole<n; ++elm_idx_in_dipole) {
      auto & new_elm = new_elements[elm_idx_in_dipole];
      unsigned int element_idx = n*dipole_index + elm_idx_in_dipole;
      _update_element_in_store(element_idx, lnv, new_elm);
    }
  }

  // #TRK_ISSUE-608  07-Sept-2020
  // CentralRap and CollinearRap only need the number of elements at any stage
  // in order to generate an lnv and an element index. In that
  // scenario, we can swap the whole shower caching update below,
  // _and_ the O(N) update_kinematics calls for global recoil,
  // with just one call to update_kinematics for each trial
  // element that goes on to have an lnb generated from it.
  // The latter approach improves event generation speed by
  // ~x4-x6 (for CentralRap), depending on aS, for global recoil. For local recoil
  // it pays to stick with the default updates below.
  if(_shower->is_global() && 
     (_strategy==CentralRap || _strategy==CollinearRap || _strategy==CentralAndCollinearRap)) return;

  // all the other touched dipoles just need to be update
  if (_shower->is_global() || (emission_info->full_store_update_required) ){
    // for a global shower, all the dipoles need to be updated
    // for a non-global shower, update all the dipoles when requested by emission info
    for (unsigned int dipole_index=0; dipole_index<event.dipoles().size(); ++dipole_index){
      if ((dipole_index == dipole_indices[0]) || (dipole_index == dipole_indices[1]))
        continue;
      for ( unsigned elm_idx_in_dipole = 0; elm_idx_in_dipole < n; ++elm_idx_in_dipole) {
        unsigned int element_idx = n * dipole_index + elm_idx_in_dipole;
        _elements[element_idx]->update_kinematics();
        _update_element_in_store(element_idx, lnv);
      }
    }    
  } else {
    // local shower. Update the remaining touched dipoles
    for ( unsigned idx=2; idx<dipole_indices.size(); ++idx){
      for ( unsigned elm_idx_in_dipole = 0; elm_idx_in_dipole < n; ++elm_idx_in_dipole) {
        unsigned int element_idx = n * dipole_indices[idx] + elm_idx_in_dipole;
        _elements[element_idx]->update_kinematics();
        _update_element_in_store(element_idx, lnv);
      }
    }
  }
  // Update max value of astar and min b, and tag associated lnv.
  //
  // #TRK_ISSUE-609 GS-NOTE: TODO: instead of using a step of 1, use sth proportional
  //to 1/alphas or 1/sqrt(alphas)
  if( std::abs(this->_last_searched_lnv - lnv) > 1. ) {
    _update_max_astar_min_b(lnv);
  }

#ifdef PSVERBOSE
  assert(_check_cached_element_indices(event));
#endif
}


//-----------------------------------------------------------------
// Initialise shower element info storage based on the input event
void ShowerRunner::_update_max_astar_min_b(double lnv) {
  // Update book-keeping inside EnhancedRoulette
  if(_do_enhanced_roulette) { _enhanced_roulette.update_max_min_mod_EF(lnv); this->_last_searched_lnv = lnv; return; }

  // Update max value of astar and min b, and tag associated lnv. 
  this->_max_astar = -std::numeric_limits<double>::max();
  this->_min_b     =  std::numeric_limits<double>::max();
  for (const auto & element : _elements) {
    this->_max_astar = std::max(this->_max_astar,element->norm_astar());
    this->_min_b     = std::min(this->_min_b,element->norm_b());
  }
  _max_astar_o_min_b = _max_astar/_min_b;
  this->_last_searched_lnv = lnv;
}


/// Set pointers according to the chosen generation strategy
void ShowerRunner::_set_strategy_pointers() {
  _do_enhanced_roulette = false; // a default except when otherwise set
  switch (_strategy){
  case PythiaHistorical: 
    _find_element_alg_ptr = &ShowerRunner::_find_element_pythia_historical_alg;
    _find_lnb_alg_ptr     = &ShowerRunner::_find_lnb_default_alg;
    break;
  case RoulettePeriodic:
    _find_element_alg_ptr = &ShowerRunner::_find_element_RoulettePeriodic_alg;
    _find_lnb_alg_ptr     = &ShowerRunner::_find_lnb_default_alg;
    break;
  case RouletteOneShot:
    _find_element_alg_ptr = &ShowerRunner::_find_element_RouletteOneShot_alg;
    _find_lnb_alg_ptr     = &ShowerRunner::_find_lnb_default_alg;
    break;
  case RouletteEnhanced:
    _do_enhanced_roulette = true;
    _enhanced_roulette    = EnhancedRoulette(_enhancement_factor);
    _find_element_alg_ptr = &ShowerRunner::_find_element_EnhancedRoulette_alg;
    _find_lnb_alg_ptr     = &ShowerRunner::_find_lnb_default_alg;
    break;
  case CentralRap:
    _find_element_alg_ptr = &ShowerRunner::_find_element_central_rap_alg;
    _find_lnb_alg_ptr     = &ShowerRunner::_find_lnb_central_rap_alg;
    break;
  case CollinearRap:
    _find_element_alg_ptr = &ShowerRunner::_find_element_collinear_rap_alg;
    _find_lnb_alg_ptr     = &ShowerRunner::_find_lnb_collinear_rap_alg;
    break;
  case CentralAndCollinearRap:
    _find_element_alg_ptr = &ShowerRunner::_find_element_central_and_collinear_rap_alg;
    _find_lnb_alg_ptr     = &ShowerRunner::_find_lnb_central_and_collinear_rap_alg;
    break;
  default:
    assert("ShowerRunner: invalid or unimplemented strategy choice" && false);
  };
}


//-----------------------------------------------------------------
// An N2 algorithm which uses the pythia approach where we try to
// split each single dipole and select the one with the max lnv. In
// this "historical" version, no caching is used and all the
// elements are re-generated on the spot.
typename ShowerBase::Element* ShowerRunner::_find_element_pythia_historical_alg(Event & event, double & lnv) {
  assert(_enhancement_factor == 1. &&
         "Pythia lnv+element generation not compatible with enhanced emission.");
  auto & dipoles = event.dipoles();
  typename ShowerBase::Element * element_that_splits = _elements[0].get();
  double next_lnv = -1e200;
  for (unsigned dipole_index = 0; dipole_index < dipoles.size(); dipole_index++) {
    for (auto & element : _shower->elements(event, dipole_index)) {
      // Set enhancement factor used directly below.
      // Including the _global_overhead_factor here guarantees that it
      // multiplies the full Sudakov exponent.
      double norm = _global_overhead_factor * element->lnv_lnb_max_density();
      double a = norm * element->lnb_extent_const();
      double b = norm * element->lnb_extent_lnv_coeff();
      double trial_lnv = _sudakov_next_lnv(lnv, a, b);
      if (trial_lnv > next_lnv) {
        next_lnv = trial_lnv;
        element_that_splits = element.get();
      }
    }
  }
  lnv = next_lnv;
  return element_that_splits;
}


//-----------------------------------------------------------------
// Initial Sudakov trial lnv generation is as in N2linear. Channel
// selection is done with stochastic roulette, using an overestimated
// weight for all channels min(b)*lnv+max(astar), where min(b) and
// max(astar) are determined periodically, i) every time the event
// evolves a unit of lnv, and ii) every time there is a full-fledged
// branching. The min(b), max(astar) determination here is hence
// identical to RouletteOneShot. The biasing factor C is set to zero
// by default, and so has no effect. C can be turned on (+ve) to
// facilitate throwing points in regions that would otherwise not be
// populated, in particular lnv = lnQ. This has been found to be 
// necessary for ME corrections, where the density was found to be
// zero in certain showers, in certain regions of phase space. N.B.
// if C is non-zero the parton shower's acceptance probability needs
// to be modified to take this into account : turning C on here 
// effectively changes each trial emission probability like so,
//    dP_trial → dP_trial_new
//             = dP_trial * ( b ln(v) + a - b C ) / ( b ln(v) + a ).
// The latter additional factor needs to be accounted for in the PS
// acceptance probability: something which is currently not in place,
// but not needed either, since we can keep C = 0 for general purposes.
// For ME corrections, we are using C > 0, but in that case the ME 
// correction acceptance probability explicitly takes C into account.
typename ShowerBase::Element * ShowerRunner::_find_element_RoulettePeriodic_alg(Event & event, double & lnv){

  // Number of elements
  unsigned int N   = _elements.size();

  // the overhead_factor_ should multiply all a's and b's.
  // two_o_tot_b therefore gets a 1/_enhancement_factor and
  // tot_astar_o_tot_b being a ratio gets nothing.  Similarly,
  // max_density, over_density and true_density are only ever used as
  // ratios so do not need a _global_overhead_factor.
  double two_o_tot_b = 2/(_tot_b*_global_overhead_factor);
  double tot_astar_o_tot_b = _tot_astar/_tot_b - _find_element_alg_bias;

  while(true) {

    // Get next lnv using crude overestimated Sudakov
    double r = gsl.uniform();
    lnv = -tot_astar_o_tot_b - sqrt(pow2(tot_astar_o_tot_b + lnv) + two_o_tot_b*log1p(-r));
#ifdef PSVERBOSE
    int coutprec = cout.precision(10);
    std::cout << "RoulettePeriodic: r = " << r << ", lnv = " << lnv 
              << ", tot_astar_o_tot_b = " << tot_astar_o_tot_b 
              << ", two_o_tot_b = " << two_o_tot_b
              << ", _tot_b = " << _tot_b
              << ", _tot_astar = " << _tot_astar
              << ", _global_overhead_factor = " << _global_overhead_factor
              << std::endl;
    cout.precision(coutprec);
#endif //PSVERBOSE

    // #TRK_ISSUE-652  KH+PM: check for NaNs [something safer should be done about this]
    if ((lnv != lnv) || std::isinf(lnv)) {
      lnv = - std::numeric_limits<double>::max();
      return _elements[0].get();
    }
    
    // Maximum possible density in the event (or an overestimate of it)
    double max_density  = _min_b * lnv + _max_astar - _min_b * _find_element_alg_bias;
    
    // Do stochastic roulette selection of the element
    while(true) {

      // Pick random element
      auto element = _elements[gsl.uniform_int(N)].get();

      // Get element's overstimated density
      double b     = element->norm_b();
      double astar = element->norm_astar();
      double a     = element->norm_a();
      double over_density = b * lnv + astar - b * _find_element_alg_bias;

      // Acc/rej element by comparing density to overestimated max density
      if (max_density == 0) break;
      if (max_density*gsl.uniform()<over_density) {
          if(astar==a) return element;
          double true_density = std::max(b * lnv + a,0.0) - b * _find_element_alg_bias;
          if(over_density*gsl.uniform()<true_density) return element;
          else break;
      }
    }

  }

  assert("ShowerRunner::_find_element_RoulettePeriodic_alg: reached the end without an element" && false);
  return _elements[0].get();
}


//-----------------------------------------------------------------
// Sudakov is product of identical Sudakovs for each element, each one
// having a->max(astar), b->min(b), where max(astar) and min(b) overestimate
// a and b, and are periodically updated (every unit of lnv) as the event
// evolves.
typename ShowerBase::Element * ShowerRunner::_find_element_RouletteOneShot_alg(Event & event, double & lnv){

  // A few trivial abbreviations
  //
  // the _overhead factor multiplies all a's and b's. We have not
  // included it whenever it explicitly cancels in ratios (i.e. in
  // _max_astar_o_min_b or when comparing two densities)
  unsigned int N       = _elements.size();
  double two_o_N_min_b = 2/(_global_overhead_factor * N*_min_b);

  // Trial a lnv, select random channel, acc/rej it based on its true
  // vs. (possibly very) over estimated density
  while (true) {

    // Get next lnv using crude overestimated Sudakov
    double r = gsl.uniform();
    lnv = -_max_astar_o_min_b - sqrt(pow2(_max_astar_o_min_b + lnv) + two_o_N_min_b*log1p(-r));

    // #TRK_ISSUE-653  KH+PM: check for NaNs [something safer should be done about this]
    if ((lnv != lnv) || std::isinf(lnv)) {
      lnv = - std::numeric_limits<double>::max();
      return _elements[0].get();
    }
    
    // Density each of the N elements is overestimated to have
    double over_density = _min_b * lnv + _max_astar;

    // Pick element at random; consistent with trial Sudakov construction
    auto element = _elements[gsl.uniform_int(N)].get();

    // Reject channel down from min(b)lnv + max(astar) -> b lnv + a
    double true_density = std::max(element->norm_b() * lnv + element->norm_a(),0.0);
    if ( (over_density==true_density) ||
         (over_density*gsl.uniform()<true_density) ) return element;
  }

  assert("ShowerRunner::_find_element_RouletteOneShot_alg: reached the end without an element" && false);
  return _elements[0].get();
}

//-----------------------------------------------------------------
// RoulettePeriodic optimised for elements with different enhancement factors
typename ShowerBase::Element* ShowerRunner::_find_element_EnhancedRoulette_alg(Event & event, double & lnv){
  return _elements[_enhanced_roulette.find_element(gsl,lnv,_global_overhead_factor)].get();
}


//-----------------------------------------------------------------
// default strategy for lnb choice (based on the element range)
bool ShowerRunner::_find_lnb_default_alg(const typename ShowerBase::Element * element, double lnv, double & lnb){
  Range range = element->lnb_generation_range(lnv);
  lnb = gsl.uniform(range.min(), range.max());
  return true;
}

//-----------------------------------------------------------------
// strategy to find the next lnv and associated element using a
// strategy that only generates in a fixed rapidity range
typename ShowerBase::Element * ShowerRunner::_find_element_central_rap_alg(Event & event, double & lnv){
  auto nelements = _elements.size();
  // this includes the overhead factor
  double lnv_density = nelements * _elements[0]->lnv_lnb_max_density() * 2 * _half_central_rap_window *_global_overhead_factor;
  lnv -= gsl.exponential(1.0/lnv_density);
  auto ielement = gsl.uniform_int(_elements.size());

  // #TRK_ISSUE-654  07-Sept-2020
  // The only evolving variable that this lnv+element scheme depends
  // on is the number of elements. Hence it's possible to skip the
  // entire shower caching in _update_elements_in_store_post_splitting
  // and any update_kinematics calls in there. We have the freedom to
  // do update_kinematics calls here instead, on the selected element.
  // This leads to substantial speed-up in the case of global recoil
  // (x4-x6, depending on aS), whereas for local recoil it's better
  // to stick with the default _update_elements_in_store_post_splitting.
  if (_shower->is_global()) _elements[ielement]->update_kinematics();

  return _elements[ielement].get();
}

bool ShowerRunner::_find_lnb_central_rap_alg(const typename ShowerBase::Element * element, double lnv, double & lnb){
  double lnb_for_min_abseta = element->lnb_for_min_abseta(lnv);
  Range range = element->lnb_generation_range(lnv);
  lnb = gsl.uniform(lnb_for_min_abseta - _half_central_rap_window, lnb_for_min_abseta + _half_central_rap_window);
  // for success, the range should contain the lnb
  bool success = range.contains(lnb);
  // and we also want to avoid generating rapidities larger than 
  // lnb_for_min_abseta
  success &= (std::abs(element->eta_approx(lnv, lnb)) < _half_central_rap_window);
  return success;
}

//-----------------------------------------------------------------
// strategy to find the next lnv and associated element using a
// strategy that only generates in a fixed rapidity range
typename ShowerBase::Element * ShowerRunner::_find_element_collinear_rap_alg(Event & event, double & lnv){
  auto nelements = _elements.size();
  // this includes the overhead factor
  double lnv_density = nelements * _elements[0]->lnv_lnb_max_density() * _collinear_rap_window * _global_overhead_factor;

  // antenna showers (only_emitter_splits() is false), we need to
  // include a factor of two, to account for both dipole ends
  if (!_shower->only_emitter_splits()) lnv_density *= 2.0;

  lnv -= gsl.exponential(1.0/lnv_density);
  auto ielement = gsl.uniform_int(_elements.size());
  //
  // #TRK_ISSUE-655  07-Sept-2020 The only evolving variable that this lnv+element
  // scheme depends on is the number of elements. Hence it's possible to
  // skip the entire shower caching in
  // _update_elements_in_store_post_splitting and any update_kinematics
  // calls in there. We have the freedom to do update_kinematics calls
  // here instead, on the selected element. For the central_rap alg,
  // this led to substantial speed-up in the case of global recoil
  // (x4-x6, depending on aS), whereas for local recoil it's better to
  // stick with the default _update_elements_in_store_post_splitting.
  if (_shower->is_global()) _elements[ielement]->update_kinematics();
  //
  return _elements[ielement].get();
}

bool ShowerRunner::_find_lnb_collinear_rap_alg(const typename ShowerBase::Element * element, double lnv, double & lnb){
  Range range = element->lnb_generation_range(lnv);
  bool success;
  if (_shower->only_emitter_splits()) {
    // in this case things are simple: 
    // - generate full collinear_rap_window
    // - accept if we're within range
    lnb = gsl.uniform(range.max() - _collinear_rap_window, range.max());
    success = range.contains(lnb);
  } else {
    // in this antenna-shower case we need to generate
    // collinear_rap_window at both ends; if the overall range 
    // is less than 2*collinear_rap_window, we need to be careful
    // to reject overlap region, which is most naturally done by
    // rejecting any lnb that is beyond the midpoint

    // one subtlety is that the midpoint can't be calculated directly
    // from 0.5*(min+max) because of situations where min>max, which
    // means zero range. So we establish the actual range (which may be
    // zero) and make sure that our lnb wrt the end is less than half of
    // that and then decide from which end we wish to use it
    double lnb_from_end = gsl.uniform(0.0, _collinear_rap_window);
    success = (lnb_from_end < 0.5 * range.extent());
    if (success) {
      if (gsl.sign() == +1) lnb = range.max() - lnb_from_end;
      else                  lnb = range.min() + lnb_from_end;
    }
  }
  return success;
}

//-----------------------------------------------------------------
// functions for handling case where the above two strategies are combined
typename ShowerBase::Element * ShowerRunner::_find_element_central_and_collinear_rap_alg(Event & event, double & lnv) {
  auto nelements = _elements.size();
  
  double lnb_extent = 2 * _half_central_rap_window;
  if (_shower->only_emitter_splits()) lnb_extent +=     _collinear_rap_window;
  else                                lnb_extent += 2 * _collinear_rap_window;

  double lnv_density = nelements * _elements[0]->lnv_lnb_max_density() * lnb_extent;
  lnv -= gsl.exponential(1.0/lnv_density);
  auto ielement = gsl.uniform_int(_elements.size());
  if (_shower->is_global()) _elements[ielement]->update_kinematics();

  return _elements[ielement].get();
}

bool ShowerRunner::_find_lnb_central_and_collinear_rap_alg(const typename ShowerBase::Element * element, double lnv, double & lnb) {

  Range lnb_valid_range = element->lnb_generation_range(lnv);
  bool success;

  // first work out the probability for generating in the central range
  // taking into account dipole (only_emitter_splits) v antenna showers
  double lnb_extent = 2 * _half_central_rap_window;
  if (_shower->only_emitter_splits()) lnb_extent +=     _collinear_rap_window;
  else                                lnb_extent += 2 * _collinear_rap_window;
  double central_prob = (2 * _half_central_rap_window) / lnb_extent;

  // then work out what the central generation range will be
  double lnb_for_min_abseta = element->lnb_for_min_abseta(lnv);
  Range central_gen_range(lnb_for_min_abseta - _half_central_rap_window, lnb_for_min_abseta + _half_central_rap_window);

  if (gsl.accept(central_prob)) {
    lnb = gsl.uniform(central_gen_range.min(), central_gen_range.max());
    success = lnb_valid_range.contains(lnb);
    // avoid generating things that are at angles that are too small (e.g. for 
    // a dipole that itself has a small opening angle)
    success &= (std::abs(element->eta_approx(lnv, lnb)) < _half_central_rap_window);
  } else {
    // generate in the collinear region, in such a way that any overlap
    // region between soft and collinear is treated correctly
    //
    if (_shower->only_emitter_splits()) {
      lnb = gsl.uniform(lnb_valid_range.max() - _collinear_rap_window, lnb_valid_range.max());
      success = lnb_valid_range.contains(lnb);
    } else {
      double lnb_from_end = gsl.uniform(0.0, _collinear_rap_window);
      success = (lnb_from_end < 0.5 * lnb_valid_range.extent());
      if (success) {
        if (gsl.sign() == +1) lnb = lnb_valid_range.max() - lnb_from_end;
        else                  lnb = lnb_valid_range.min() + lnb_from_end;
      }
    }

    // Deal with overlap with soft region, supposing an lnb range and various
    // generation and acceptance regions as follows
    //
    // <---------------------x------------------------->
    //             |----central-gen----|
    //               |--central-acc--|
    //                             |-------coll-gen---->
    //                               |-----coll-acc---->
    //
    // The coll-acc should exclude anything that is in the central-acc
    // (which is at most the eta approx condition above in the central prob 
    // part)
    success &= (std::abs(element->eta_approx(lnv, lnb)) >= _half_central_rap_window);
  }

  return success;
}


//========================================================================
//
// Additional helpers:
// - initialisations
// - overhead updates
//
//========================================================================

// set the default behaviour for the double-soft treatment
void ShowerRunner::_init_double_soft_defaults(){
  // for the time being: off by defaut
  enable_double_soft(false, false, 0.0, false);
  disable_double_soft_colour_flow_swaps(false);
  set_double_soft_abspdgid_for_fixed_emission(0);
}

// set all the uncertainty-related overhead factors based on the shower 
// uncertainty parameters
void ShowerRunner::_initialise_overhead_factors(){
  _unc_hard_overhead_factor  = std::max(_shower->uncertainty_x_hard(),  1.0);
  _unc_simkt_overhead_factor = std::max(_shower->uncertainty_x_simkt(), 1.0);
  _update_global_overhead_factor();
}

// global overhead factor (easier to have a single number to 
// insert in various probbilities and Sudakov weights)
// This HAS TO be updated whenever any of the individual acceptance factors changes
void ShowerRunner::_update_global_overhead_factor(){
  _global_overhead_factor = _isr_overhead_factor
                          * _unc_hard_overhead_factor
                          * _unc_simkt_overhead_factor;
}


//========================================================================
//
// Verbose output and validation tools
//
//========================================================================

void ShowerRunner::_verbose_info_shower_acceptance(double ps_accept_prob,
                                                   const Event &event,
                                                   typename ShowerBase::EmissionInfo *emission_info) const{
  int coutprec = cout.precision();
  const auto * element = emission_info->element();
  cout.precision(10);
  std::cout << "  " << " gsl: " << sha1(gsl.hex_state()) 
            << ", lnv = " << emission_info->lnv << ", lnb = " << emission_info->lnb
            << ", ps_accept_prob = " << ps_accept_prob << "\n";
  std::cout << "element type: " << typeid(*element).name() 
            << ", emitter-spectator: " << element->emitter_index() 
            << "-" << element->spectator_index()
            << ", emitter_is_3end=" << element->emitter_is_3_end_of_dipole()
            << std::endl;
  std::cout << "emitter  : " << element->emitter() << "\n" 
            << "spectator: " << element->spectator() 
            << std::endl;
  cout.precision(coutprec);
}

  

void ShowerRunner::_verbose_info_acceptance_above_one_pp(double accept_prob,
                                                         const Event &event,
                                                         typename ShowerBase::EmissionInfo *emission_info) const{
  cout << "got acceptance probability " << accept_prob;
  cout
       << "\n    Beam 1:" 
       << "  old_x = " << event.pdf_x_beam1() 
       << ", new_x = " << emission_info->beam1_pdf_new_x_over_old * event.pdf_x_beam1() 
       << ", lnmuF = " << emission_info->pdf_info_beam1.lnmuF 
       << ", flav =  " << emission_info->pdf_info_beam1.flav_old
       << ", xf_new(flav) = " << emission_info->pdf_info_beam1.pdf_new.flav(emission_info->pdf_info_beam1.flav_old) 

       << "\n    Beam 2:" 
       << "  old_x = " << event.pdf_x_beam2() 
       << ", new_x = " << emission_info->beam2_pdf_new_x_over_old * event.pdf_x_beam2() 
       << ", lnmuF = " << emission_info->pdf_info_beam2.lnmuF 
       << ", flav =  " << emission_info->pdf_info_beam2.flav_old
       << ", xf_new(flav) = " << emission_info->pdf_info_beam2.pdf_new.flav(emission_info->pdf_info_beam2.flav_old) 

       << "\n    and _isr_overhead_factor=" << _isr_overhead_factor << endl;
  cout << "emitter   = " << emission_info->element()->emitter()   << endl;
  cout << "spectator = " << emission_info->element()->spectator() << endl;
  cout << "emitter weight rad gluon, quark: " 
       << emission_info->emitter_weight_rad_gluon << ", "
       << emission_info->emitter_weight_rad_quark << endl;
  cout << "spectator weight rad gluon, quark: " 
       << emission_info->spectator_weight_rad_gluon << ", "
       << emission_info->spectator_weight_rad_quark << endl;
  cout << *_isr_overhead_factor_handler << endl;

  cout << "beam1 iofh->_get_weight = " << _isr_overhead_factor_handler->_get_weight(
                emission_info->pdf_info_beam1.flav_old, event.pdf_x_beam1(), {}, 
                to_double(emission_info->beam1_pdf_new_x_over_old * event.pdf_x_beam1()),
                emission_info->pdf_info_beam1.lnmuF, 
                emission_info->pdf_info_beam1.damping_factor) << endl;
  cout << "beam2 iofh->_get_weight = " << _isr_overhead_factor_handler->_get_weight(
                emission_info->pdf_info_beam2.flav_old, event.pdf_x_beam2(), {}, 
                to_double(emission_info->beam2_pdf_new_x_over_old * event.pdf_x_beam2()),
                emission_info->pdf_info_beam2.lnmuF, 
                emission_info->pdf_info_beam2.damping_factor) << endl;
  cout << "direct call to iofh cached estimate = " << (*_isr_overhead_factor_handler)(
              event.pdf_x_beam1(), event.pdf_x_beam2(),
              emission_info->pdf_info_beam1.flav_old, emission_info->pdf_info_beam2.flav_old) << endl;

}

void ShowerRunner::_verbose_info_phi_selection(const Event &event, typename ShowerBase::EmissionInfo *emission_info) const{
  int coutprec = cout.precision();
  cout.precision(10);
  cout << "  " << " gsl: " << sha1(gsl.hex_state()) << ", phi = " << emission_info->phi << endl;
  cout.precision(coutprec);
}

  
void ShowerRunner::_verbose_info_do_kinematics(const Event &event, typename ShowerBase::EmissionInfo *emission_info) const{
  auto * element = emission_info->element();
  std::cout << "eminfo em  " << emission_info->emitter_out << "\n";
  std::cout << "eminfo sp  " << emission_info->spectator_out << "\n";
  std::cout << "eminfo rd  " << emission_info->radiation << "\n";
  if (shower()->use_diffs()) {
    std::cout << "eminfo dem " << emission_info->d_emitter_out << "\n";
    std::cout << "eminfo dsp " << emission_info->d_spectator_out << "\n";
    std::cout << "eminfo drd wrt emitter " << emission_info->d_radiation_is_wrt_emitter << "\n";
    if (emission_info->d_radiation_is_wrt_emitter) {
      std::cout << "eminfo drd_em " << emission_info->d_radiation << "\n";
      std::cout << "eminfo drd_sp " << element->emitter().direction() + emission_info->d_radiation - element->spectator().direction() << "\n";
    } else {
      std::cout << "eminfo drd_em " << element->spectator().direction() + emission_info->d_radiation - element->emitter().direction() << "\n";
      std::cout << "eminfo drd_sp " << emission_info->d_radiation<< "\n";
    }
  } else {
    std::cout << "eminfo dem " << emission_info->emitter_out  .direction() - element->emitter()  .direction() << "\n";
    std::cout << "eminfo dsp " << emission_info->spectator_out.direction() - element->spectator().direction() << "\n";
    std::cout << "eminfo drd_em " << emission_info->radiation.direction() - element->emitter().direction() << "\n";
    std::cout << "eminfo drd_sp " << emission_info->radiation.direction() - element->spectator().direction() << "\n";
  }
}

void ShowerRunner::_verbose_info_acceptance_above_one_DIS(double accept_prob,
                                                         const Event &event,
                                                         typename ShowerBase::EmissionInfo *emission_info) const{                                            
  // check separate for beam1 or beam2 to stem from the proton
  if (_hoppet_runner->is_pdf_reliable(event.pdf_x_beam1()) && event[0].is_parton()){
    cout << "got acceptance probability " << accept_prob
         << "\n    with old_x1,new_x1,lnmuF1,flav,new_xf(flav)=" 
         << event.pdf_x_beam1() << "," 
         << emission_info->beam1_pdf_new_x_over_old * event.pdf_x_beam1() << ", "
         << emission_info->pdf_info_beam1.lnmuF << ", "
         << emission_info->pdf_info_beam1.flav_old << ", "                  
         << emission_info->pdf_info_beam1.pdf_new.flav(emission_info->pdf_info_beam1.flav_old) << endl
         << " and _isr_overhead_factor=" << _isr_overhead_factor << endl;
    cout << "emitter weight rad gluon, quark: " 
         << emission_info->emitter_weight_rad_gluon << ", "
         << emission_info->emitter_weight_rad_quark << endl;
    cout << "spectator weight rad gluon, quark: " 
         << emission_info->spectator_weight_rad_gluon << ", "
         << emission_info->spectator_weight_rad_quark << endl;
    cout << *_isr_overhead_factor_handler << endl;
  } else if (_hoppet_runner->is_pdf_reliable(event.pdf_x_beam2()) && event[1].is_parton()){
    cout << "got acceptance probability " << accept_prob
         << "\n     with old_x2,new_x2,lnmuF2,flav,new_xf(flav)=" 
         << event.pdf_x_beam2() << "," 
         << emission_info->beam2_pdf_new_x_over_old * event.pdf_x_beam2() << ", "
         << emission_info->pdf_info_beam2.lnmuF << ", "
         << emission_info->pdf_info_beam2.flav_old << ", "
         << emission_info->pdf_info_beam2.pdf_new.flav(emission_info->pdf_info_beam2.flav_old) << endl
         << " and _isr_overhead_factor=" << _isr_overhead_factor << endl;
    cout << "emitter weight rad gluon, quark: " 
         << emission_info->emitter_weight_rad_gluon << ", "
         << emission_info->emitter_weight_rad_quark << endl;
    cout << "spectator weight rad gluon, quark: " 
         << emission_info->spectator_weight_rad_gluon << ", "
         << emission_info->spectator_weight_rad_quark << endl;
    cout << *_isr_overhead_factor_handler << endl;
  }
} 

//----------------------------------------------------------------------
// check that the acceptance p[roability computedby the shower is valid
// This throws an error if some inconsistency is found
// void ShoewrRunner::_check_ps_accept_prob_validity() const{
// 
// 
// 
// }


//----------------------------------------------------------------------
// check that the q/qbar indices of the cached elements match with what
// one should expect from the set of dipoles in the event
bool ShowerRunner::_check_cached_element_indices(Event &event) const{
  // index in te cached vector
  unsigned int cached_element_index=0;

  // loop over dipoles
  for (unsigned dipole_index = 0; dipole_index < event.dipoles().size(); ++dipole_index) {
    auto elements = _shower->elements(event, dipole_index);
    for (auto & element : elements) {
      // Not necessary to set the newly created elements' enhancement
      // factors as this function is just for debugging, wherein it only
      // checks emitter/spectator indexing).
      // make sure we have a cached element available
      if (cached_element_index >= _elements.size()){
        std::cerr << "not enough elements in cache" << std::endl;
        return false;
      }

      // compare indices
      const auto * cached_element = _elements[cached_element_index].get();
      if ((cached_element->emitter_index()   != element->emitter_index()  ) ||
          (cached_element->spectator_index() != element->spectator_index()) ){
        std::cerr << "mismatch in element indices: " << std::endl;
        std::cerr << "  cached: (" << cached_element->emitter_index() << ", " << cached_element->spectator_index() << ")" << std::endl;
        std::cerr << "  expect: (" <<        element->emitter_index() << ", " <<        element->spectator_index() << ")" << std::endl;
        return false;
      }

      // check the kinematics of the elements (if implemented in the shower)
      if (!element->check_equal_to(cached_element)){
        std::cerr << "mismatch in element kinematics" << std::endl;
        return false;
      }
      
      // increment cached index
      ++cached_element_index;
    }
  }

  // make sure we do not have leftovers in the cache
  if (cached_element_index != _elements.size()){
    std::cerr << "too many elements in cache" << std::endl;
    return false;
  }
  
  return true;
}

//========================================================================
//
// Unsorted
//
//========================================================================


//----------------------------------------------------------------------
// implementation of StepByStepEvent
//----------------------------------------------------------------------
  
// reset the stored data
void StepByStepEvent::reset(){
  initial_event_ = Event(); 
  emitter_indices_.clear();
  spectator_indices_.clear();
  split_emitter_indices_.clear();
  lnvs_.clear();
  lnbs_.clear();
  phis_.clear();
  events_.clear();
}

// append an event+info after one step of the shower
void StepByStepEvent::push_back(unsigned int emitter_index_in,
                                unsigned int spectator_index_in,
                                unsigned int split_index_in,
                                double lnv_in, double lnb_in, double phi_in,
                                const Event &event_in){
  emitter_indices_.push_back(emitter_index_in);
  spectator_indices_.push_back(spectator_index_in);
  split_emitter_indices_.push_back(split_index_in);
  lnvs_.push_back(lnv_in);
  lnbs_.push_back(lnb_in);
  phis_.push_back(phi_in);
  events_.push_back(event_in);
}
  

// print of the banner
void ShowerRunner::print_banner(){
    if (!_first_printout_of_banner) return;
    ostream * ostr = &std::cout;
    if (!ostr) return;  
    (*ostr) << "#-----------------------------------------------------------------------\n";
    (*ostr) << "#                     PanScales version " << PANSCALES_VERSION      << "\n";
    (*ostr) << "#     A software package for logarithmically accurate parton showers    \n";
    (*ostr) << "#	                                                                      \n";
    (*ostr) << "#  M. van Beekveld, M. Dasgupta, B. K. El-Menoufi, S. Ferrario Ravasio, \n";
    (*ostr) << "#    K. Hamilton, J. Helliwell,  A. Karlberg, R. Medves, P. F. Monni,   \n";
    (*ostr) << "#     G. P. Salam, L. Scyboz, A. Soto-Ontoso, G. Soyez, R. Verheyen     \n";
    (*ostr) << "#	                                                                      \n";
    (*ostr) << "# Please cite arXiv:2312.13275 if you use this package for scientific   \n";
    (*ostr) << "# work, plus the relevant references therein.                           \n";
    (*ostr) << "#                                                                       \n";
    (*ostr) << "# PanScales is provided without warranty under the GNU GPL v3 or higher.\n";
    (*ostr) << "# Dependencies are listed in 3rdPartyCode.md.                           \n";
    (*ostr) << "#-----------------------------------------------------------------------\n";
    ostr->flush();
    // reset the variable
    _first_printout_of_banner = false;
}

//------------------------------------------------------------------------
// One emission effective ME implementation as needed for matching
//------------------------------------------------------------------------
// MvB - this should be part of process 
// comment this out here (but do not remove!)
// double ShowerRunner::one_emission_effective_ME(const Event event) const{
//   PRINT_EXPERIMENTAL("ShowerRunner::one_emission_effective_ME");

//   double ME = 0.0;

//   switch (_hard_process_id) {
//     default:
//       assert(false && "one_emission_effective_ME not implemented for current process!!");
//     case ProcID::pp2Z: // pp2Z
//       ME = _shower->pp2Z_one_emission_effective_ME(event, _hoppet_runner);
//       break;
//     case ProcID::pp2H: // pp2H
//       ME = _shower->pp2H_one_emission_effective_ME(event, _hoppet_runner);
//       break;
//   }

//   return ME;

// }

} // namespace panscales
#include "autogen/auto_ShowerRunner_global-cc.hh"
