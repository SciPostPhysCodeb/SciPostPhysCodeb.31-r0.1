//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
///
/// Code to run basic tests that the reweighting is functioning correctly
/// 
#include "AnalysisFramework.hh"
#include "FastY3.hh"
#include "ShowerRunner.hh"

using namespace std;
using namespace panscales;

class ExampleFramework : public  AnalysisFramework {
public:
  bool do_obs;
  FastY3 fasty3;
  double alphas;
  
  ExampleFramework(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in) {}

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {

    do_obs = ! cmdline->present("-no-obs");
    
    double lambda_max = cmdline->value("-lambda-max", 1.4);
    double dlambda    = cmdline->value("-dlambda",   0.05);
    this->set_default_binning(-lambda_max, 0.0, dlambda);

    // the shower runner should already have been created, including
    // alphas being set
    //
    // Note that if we use a 2-loop running, we need to make sure we
    // take this reference alphas in the MSbar scheme
    alphas = f_shower_runner->shower()->qcd().alphasMSbar(log(f_rts));

    header << "# lambda_max = " << lambda_max << endl;
    
    // now set a number of defaults
    // #TRK_ISSUE-710  FD+PM: -rts should be set from cmdline as the shower gets set up before
    // user_startup is called
    //f_rts = 1e152;

    f_lnvmax = log(f_rts);
    f_lnvrange = lambda_max / alphas - log(numeric_limits<double>::epsilon());
    f_lnvmin   = f_lnvmax - f_lnvrange;
    assert(f_dynamic_lncutoff < 0.0 &&
           "You must specify a negative -dynamic-lncutoff argument (recommended value 0.5*log(epsilon)=-18");
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    double evwgt = event_weight();
    if (!do_obs) return;
    
    //cout << f_event << endl;
    fasty3.analyse(f_event);
    //cout << fasty3() << endl;
    precision_type y3 = fasty3();
    double lny3;
    if (y3 > 0) {
      lny3 = to_double(log(y3));
    } else {
      // some very small value that is not NaN or -Inf, to avoid
      // triggering FPE when we are checking those.
      lny3 = -1e100;
    }

    cumul_hists["lambda_halflny3"].add_entry(0.5*alphas*lny3, evwgt);
  }
  
  
};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  ExampleFramework driver(&cmdline);
  driver.run();
}
