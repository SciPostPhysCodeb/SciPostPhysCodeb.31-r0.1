//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

//======================================================================
//
//    !!!   DISCLAIMER   !!!   DISCLAIMER     !!!   DISCLAIMER  !!!
//
// This is the PanScales implementation of a shower with the same
// kinematic mapping as the Pythia8 shower. This is purely meant for
// tests of logarithmic accuracy and should, under no circumstance be
// taken as a full implementation of the actual Pythia8 shower. For
// practical phenomenology with the Pythia8 shower, use the
// implementation provided in Pythia8 (https://pythia.org/)
//
//======================================================================

#ifndef __SHOWERPYTHIA8_HH__
#define __SHOWERPYTHIA8_HH__

#include "ShowerBase.hh"

namespace panscales{

//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerToyPythia8
/// Implementation of the Pythia8 shower algorithm for e+e- collisions 
/// introduced in arXiv:1805.09327. The implementation is based on arXiv:0408302 
/// and inspection of the Pythia8 code v8.266. Beware that some differences between  
/// this implementation and the original Pythia8 shower are expected.  
/// 
class ShowerToyPythia8 : public ShowerBase {
public:
  /// default ctor
  ShowerToyPythia8(const QCDinstance & qcd) : ShowerBase(qcd) {}

  /// virtual dtor
  virtual ~ShowerToyPythia8() {}

  /// implements the element and splitting info as subclasses
  class Element;
  class EmissionInfo;
  virtual ShowerBase::EmissionInfo* create_emission_info() const override;

  /// description of the class
  std::string description() const override {
    std::ostringstream ostr;
    ostr << name() << " shower (PanScales implementation; if g->qq is on, it differs from Pythia8's implementation)";
    return ostr.str();
  }
  /// name of the shower
  std::string name() const override{return "ToyPythia8-ee";}

  /// meaning of kinematic variables
  std::string name_of_lnv() const override {return "0.5*ln(t)";}
  std::string name_of_lnb() const override {return "ln(1-z)";}

  /// this is a dipole shower (2 elements per dipole)
  virtual unsigned int n_elements_per_dipole() const override { return 2; }

  /// allocates unique_ptrs for for the "elements" associated with dipole
  /// index i in the event and returns them as a vector of unique_ptrs
  virtual std::vector<std::unique_ptr<ShowerBase::Element> > elements(Event & event, int dipole_index) const override;

  /// the Pythia8 shower is kt-ordered (beta=0)
  double beta() const override {return 0.0;}

  /// this is a dipole-like shower, so only the emitter splits
  bool only_emitter_splits() const override {return true;}
};


//--------------------------------------------------------------
/// \class ShowerToyPythia8::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs for the Pythia8 shower algorithm
///
/// Notes about the g->qqbar splitting function as implemented in
/// Pythia8
///
/// Notes:
/// - Looked at src/TimeShower.cc, line 2285 in Pythia8.2.30
///
/// - dip.mFlavour is set to 0 (all massless flavours)
///   Implies  ratioQ = 0
///            betaQ  = 1
///
/// - The default weightGluonToQuark seems to be 1, in which case the
///   splitting functions for massless quarks are the same as ours.
///
/// - For larger weightGluonToQuark, the doc seems to imply also the
///   same structure function in the massless-quark limit.
///   dip.m2 seems to be the quark mass?
///
/// - larger weightGluonToQuark values change alphas. We can probably
///   stay away from these for now
///
/// All in all, it seems that Pythia is simply using z^2 + (1-z)^2
///
class ShowerToyPythia8::Element : public ShowerBase::Element {
public:
  /// dummy ctor
  Element() {}

  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerToyPythia8 * shower) :
    ShowerBase::Element(emitter_index, spectator_index, dipole_index, event),
    _shower(shower) {
    update_kinematics();
  }

  /// given that we have virtual functions in a number of places,
  /// we need a virtual destructor to avoid compiler warnings
  virtual ~Element() {}

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------

  /// Approximate calculation of the log of the kT of the emission relative
  /// to its parents in their rest frame, in rho->0 limit
  /// (eq. 36 of logbook 2019-07-02).
  const double alphas_lnkt(double lnv, double lnb) const override {
    return lnv;
  }

  /// shower evolution variable
  const double betaPS() const override { return 0.; }

  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  /// the lnb total extent, as a function of lnv, is parametrised
  /// as  const + lnv_coeff * lnv.
  double lnb_extent_const()     const override {
    return 0.5*to_double(log_T(_dipole_m2));
  }
  
  double lnb_extent_lnv_coeff() const override {return -1.0;}
  Range lnb_generation_range(double lnv) const override;
  bool has_exact_lnb_range() const override {return true;}
  Range lnb_exact_range(double lnv) const override;
  double lnv_lnb_max_density() const override;

  /// jacobian for this shower is not computed
  precision_type dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const override {
    throw std::runtime_error("Tried to access the exact jacobian an element of shower " + _shower->name() + ", which is not implemented");
    return 0.;
  }

  /// this function need only contain a non-trivial implementation if
  /// matching is to be performed
  void get_emit_spec_rad_kinematics_for_matching(typename ShowerBase::EmissionInfo * emission_info, Momentum & emit, Momentum & spec, Momentum & rad, bool include_boost = false) const override{
    throw std::runtime_error("Tried to access kinematics for matching of the shower " + _shower->name() + ", for which matching is not implemented");
  }


  //--------------------------------------------------
  // important definitions to be implemented in all derived classed
  //--------------------------------------------------

  /// if the kinematic point in EmissionInfo is accessible, return
  /// true and set various splitting weights and derived kinematic
  /// variables in emission_info. Otherwise return false.
  /// (See parent class ShowerBase::Element for more details)
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// Carry out the splitting for this Element (and associated dipole),
  /// and set the emitter_out, radiation and spectator_out members
  ///
  /// The function returns true if the splitting is kinematically
  /// accessible, otherwise false. of the EmissionInfo object.
  ///
  /// In addition to the EmissionInfo class, the function also takes
  /// RotatedPieces. This contains the pre-branching emitter and
  /// spectator particles as well as two perpendicular vectors. Note
  /// that if using direction differences, these will be in a (rotated)
  /// frame, where either the emitter or spectator is aligned along the
  /// z axis. The output is to be stored in that same frame in the
  /// EmissionInfo class.
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;

  //----------------------------------------------------
  // caching info to facilitate the work of ShowerRunner
  //----------------------------------------------------

  /// update the internal kinematic variables (e.g. dipole mass), to be
  /// called, e.g., whenever one or other end of the dipole has been
  /// touched
  virtual void update_kinematics() override;

  /// update the element indices and kinematics
  void update_indices(unsigned emitter_index, unsigned spectator_index);
  
  /// for a given lnv,lnb, returns approximate lnkt in the
  /// emitter-spectator dipole rest frame, which is equivalent to the
  /// frame-independent lnkt wrt to the emitter-spectator system.
  /// This lnkt will be correct in the soft+collinear limit but may
  /// differ from the true lnkt in the soft large-angle limit and the
  /// hard-collinear limit
  virtual double lnkt_approx(double lnv, double lnb) const override {
    return lnv;
  }

  /// for a given lnv,lnb, returns the pseudorapidity with respect to
  /// the closer of the emitter/spectator, defined in the event
  /// centre-of-mass frame. This eta will be correct in the
  /// soft+collinear limit but may differ from the true eta in the
  /// soft large-angle limit and the hard-collinear limit.
  virtual double eta_approx(double lnv, double lnb) const override;

  virtual double lnb_for_min_abseta(double lnv) const override;

  bool use_diffs() const override {return _shower->use_diffs();}
  bool double_soft() const override {return _shower-> double_soft();}
private:
  precision_type rhoevol2(precision_type lnv) const {return exp(2*lnv) / _dipole_m2;}

  const ShowerToyPythia8 * _shower;
  precision_type _dipole_m2;
};

//--------------------------------------------------------------
/// \class ShowerToyPythia8::EmissionInfo
/// emission information specific to the Pythia8 shower
class ShowerToyPythia8::EmissionInfo : public ShowerBase::EmissionInfo {
public:
  /// kinematic map variables
  precision_type omz, z, r2;
};
  
} // namespace panscales

#endif // __SHOWERPYTHIA8_HH__
