#ifndef __EECAMBRIDGEDIRDIFF_HH__
#define __EECAMBRIDGEDIRDIFF_HH__

//FJSTARTHEADER
//
// Copyright (c) 2005-2018, Matteo Cacciari, Gavin P. Salam and Gregory Soyez
//
//----------------------------------------------------------------------
//  This file is distributed as part of PanScales but is HEAVILY based 
//  on a part of FastJet.
//
//  FastJet is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  The algorithms that underlie FastJet have required considerable
//  development. They are described in the original FastJet paper,
//  hep-ph/0512210 and in the manual, arXiv:1111.6097. If you use
//  FastJet as part of work towards a scientific publication, please
//  quote the version you use and include a citation to the manual and
//  optionally also to hep-ph/0512210.
//
//  FastJet is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with FastJet. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------
//FJENDHEADER

#include <cstddef>
#include "Type.hh"
#include "fjcore_local.hh"

// forward declaration to reduce includes
namespace fjcore {
  class ClusterSequence;
}

#include "Event.hh"
#include <stdexcept>


namespace panscales {
//----------------------------------------------------------------------
/// \class EventPairwiseDistances
/// class to compute and cache pairwise direction differences
/// (for events originally generated with direction differences)
class EventPairwiseDistances {
public:
  EventPairwiseDistances(const Event & event) {
    // locate the qbar (for now we handle just case with a single
    // qbar-q) -- if one day we get "fake dipoles" this won't be needed
    // anymore 
    //
    // While looping over the dipoles, check where is the largest dirdiff
    const Dipole * initial_dipole = nullptr;
    precision_type largest_dirdiff = 0.0;
    unsigned int idipole_largest_dirdiff = 0;
    for (unsigned i = 0; i < event.dipoles().size(); i++) {
      precision_type this_diff = event.dipoles()[i].dirdiff_3_minus_3bar.modpsq();
      if (this_diff > largest_dirdiff){
        idipole_largest_dirdiff = i;
        largest_dirdiff = this_diff;
      }
      
      // if we have a qbar not associated with a non-splitting dipole,
      // take this as a starting point. For this, we check if there's
      // no dipole (neither real nor non-splitting) at the 3bar end of
      // this dipole
      int pdgid_3bar = event.dipoles()[i].particle_3bar().pdgid();
      if (pdgid_3bar >= -6 && pdgid_3bar <= -1 &&
          (! (event.dipoles()[i].dipole_at_3bar_end(true)))){
        if (initial_dipole) throw std::runtime_error("Found two anti-quarks (without a quark associated through a non-splitting dipole) in dipole chain");
        initial_dipole = &event.dipoles()[i];
      }
    }
    
    // maybe it was a gg event so we didn't find an anti-quark
    bool discard_last_dipole = false;
    if (initial_dipole == nullptr){
      // we use the largest dirdiff to decide where to start
      // We essentially take just the next dipole to start
      const Dipole * dipole1 = &event.dipoles()[idipole_largest_dirdiff];
      initial_dipole = dipole1->dipole_at_3_end(true);

      // In this case, we should also discard the last dipole (which
      // would only give a large cyclic distance), so as to make sure
      // that this largest dipole is never used
      // and that we keep the number of dipoles equal to the 
      // number of particles - 1 regardless of whether we are in 
      // a cyclic or non-cyclic case.
      discard_last_dipole = true;
    }

    // decant the dipoles in the dipole-chain order
    unsigned int ndipoles = event.dipoles().size() + event.non_splitting_dipoles().size();
    _dipoles.resize(ndipoles);
    const Dipole * dipole1 = initial_dipole;
    std::size_t i = 0;
    while (true) {
      _dipoles[i] = dipole1;
      dipole1 = dipole1->dipole_at_3_end(true);
      ++i;
      if (dipole1 == nullptr || dipole1 == initial_dipole) break;
    }
    if (discard_last_dipole) _dipoles.pop_back();

    // construct a matrix of 1-cos(theta) values:
    // first allocate the space.
    // Our convention will be that matrix_omc[i][j] with j < i will 
    // be the 1-cos(theta) value between particles i & j
    _omc.resize(event.size());
    for (i = 0; i < event.size(); i++) {
      _omc[i].resize(i);
    }

    // also initialise the maps from particles to dipoles (they start
    // with zero size, so will all acquire a default value of -1)
    _idip_3bar.resize(event.size(),-1);
    _idip_3   .resize(event.size(),-1);

    // fill the matrix
    for (i = 0; i < _dipoles.size(); ++i) {
      // build look-up table from particles to dipoles
      _idip_3bar[_dipoles[i]->index_3bar()] = i;
      _idip_3   [_dipoles[i]->index_3   ()] = i;
      // loop over dipoles, adding the difference between ends at each
      // stage
      Momentum3<precision_type> diff3m3bar(0,0,0);
      for (size_t j = i; j < _dipoles.size(); ++j) {
        diff3m3bar += _dipoles[j]->dirdiff_3_minus_3bar;
        register_omc(_dipoles[i], _dipoles[j], diff3m3bar);
      }
    }
  }

  // returns cached value of 1-cos(theta_{ij})
  precision_type cached_omc(int i, int j) const {
    if (i == j) return 0.0;
    if (i >  j) return _omc[i][j];
    else        return _omc[j][i];
  }

  /// given the indices of particles i and j, returns the 3-vector
  /// direction difference pj/pj.modp() - pi/pi.modp(). It works it out
  /// on the fly and can take a time of order(N)
  Momentum3<precision_type> dirdiff_j_minus_i(int i, int j) const {
    Momentum3<precision_type> diff_j_minus_i(0,0,0);
    // find the locations within the dipole chain; first make a try
    // with the assumption that i as the 3bar end of a dipole will come
    // before j as the 3 end of a dipole
    int idip_i_3bar = _idip_3bar[i];
    int idip_j_3    = _idip_3   [j];
    if (idip_i_3bar >= 0 && idip_i_3bar <= idip_j_3) {
      for (int idip = idip_i_3bar; idip <= idip_j_3; idip++) {
        diff_j_minus_i += _dipoles[idip]->dirdiff_3_minus_3bar;
      }
    } else {
      // if the original assumption fails, then try swapping the ends
      int idip_j_3bar = _idip_3bar[j];
      int idip_i_3    = _idip_3   [i];
      assert(idip_j_3bar >= 0 && idip_i_3 >= idip_j_3bar);
      for (int idip = idip_j_3bar; idip <= idip_i_3; idip++) {
        diff_j_minus_i -= _dipoles[idip]->dirdiff_3_minus_3bar;
      }
    }
    return diff_j_minus_i;
  }

private:
  /// register the 1-cos(theta) distance from the 3bar end of the "3bar"
  /// dipole to the 3 end of the "3" dipole
  void register_omc(const Dipole * dipole_3bar, //< dipole whose 3bar end we register
                    const Dipole * dipole_3,    //< dipole whose 3 end we register
                    const Momentum3<precision_type> & dirdiff) {
    // get the indices of the particles at the two ends
    int ii = dipole_3bar->index_3bar();
    int jj = dipole_3->index_3();
    if (ii < jj) std::swap(ii,jj);

    // As for dot_product_with_dirdiff, use the following code
    //   1 - cos theta = 1 - cos^2 theta/2 + sin^2 theta/2 = 2 sin^2 theta/2
    // 
    // Then observe that dirdiff.modp()/2 = sin (theta/2)
    // so 2 sin^2(theta/2) = 0.5 * dirdiff.modpsq()
    // NB: strictly, we could take this factor 0.5 out and
    //     instead put it in when we record (e.g.) the clustering distance
    _omc[ii][jj] = 0.5 * dirdiff.modpsq();
  }
  
  /// holds the 1-cos(theta) values
  std::vector<std::vector<precision_type>> _omc;
  std::vector<const Dipole *> _dipoles;
  // for a particle j, idip_3bar[j] is the dipole index (in our _dipoles
  // structure) whose 3bar end is particle j
  std::vector<int> _idip_3bar, _idip_3;

};

/// Direction Differences User Info for Pseudojets
/// This is the only easy way we've found so far of carrying the
/// the info into the clustering without just giving it to the 
/// JetDefinition.
class DirDiffUserInfo : public fjcore::PseudoJet::UserInfoBase {
public:
  DirDiffUserInfo(const EventPairwiseDistances * evpwd_in, int i_in) : 
    _evpwd(evpwd_in), _i(i_in) {}

  /// returns a pointer to the EventPairwiseDistances object
  const EventPairwiseDistances * evpwd() const {return _evpwd;}

  /// returns the index of this jet within the EventPairwiseDistances
  /// object
  int i() const {return _i;}

  /// return 1-cos(theta_{this,other})
  precision_type omc_to(const fjcore::PseudoJet & other) const {
    return _evpwd->cached_omc(_i, other.user_info<DirDiffUserInfo>()._i);
  }

  /// returns the 3-vector direction difference of other - this
  fjcore::PseudoJet dirdiff_to(const fjcore::PseudoJet & other) const {
    Momentum3<precision_type> diff = _evpwd->dirdiff_j_minus_i(i(), other.user_info<DirDiffUserInfo>().i());
    return fjcore::PseudoJet(diff.px(), diff.py(), diff.pz(), 0.0);
  }


private:
  const EventPairwiseDistances * _evpwd;
  int _i;
};
//----------------------------------------------------------------------
/// @ingroup jet_algorithms
/// \class EECamBriefJetDD
///  class to help run an e+e- Cambridge algorithm with direction differences
///  (normally this would be hidden elsewhere, but it is
///  useful for our high precision Lund Plane implementation)
class EECamBriefJetDD {
public:
  void init(const fjcore::PseudoJet & jet) {
    // jets need to have a DirDiffUserInfo user_info
    const DirDiffUserInfo & dirdiff_userinfo = jet.user_info<DirDiffUserInfo>();
    // we extract the pointer to the container for the event pairwise distances
    _event_pairwise_distances = dirdiff_userinfo.evpwd();
    // and the index of this jet within that container
    _i = dirdiff_userinfo.i();
  }

  precision_type distance(const EECamBriefJetDD * jet) const {
    // get the 1-cos(theta) value from the cache
    precision_type dij = _event_pairwise_distances->cached_omc(_i, jet->_i);
    return dij;
  }

  precision_type beam_distance() const {
    return numeric_limit_extras<precision_type>::max();
  }

private:
  /// a pointer to the store of event pairwise distances
  const EventPairwiseDistances * _event_pairwise_distances;
  /// the index of this particle within the pairwise_distance store
  int _i;
};

//----------------------------------------------------------------------
//
/// @ingroup plugins
/// \class EECambridgeDirDiff
/// Implementation of the e+e- Cambridge algorithm (plugin for fastjet v2.4 upwards)
///
/// EECambridgeDirDiff is a plugin for fastjet (v2.4 upwards)
/// It implements the Cambridge algorithm, as defined in 
/// 
/// Better jet clustering algorithms
/// Yuri Dokshitzer, Garth Leder, Stefano Moretti,  Bryan Webber 
/// JHEP 9708 (1997) 001
/// http://www-spires.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+j+JHEPA%2C9708%2C001
///
/// On construction one must supply a ycut value.
///
/// To get the jets at the end call ClusterSequence::inclusive_jets();
class EECambridgeDirDiff : public fjcore::JetDefinition::Plugin {
public:
  /// Main constructor for the EECambridgeDirDiff Plugin class.  
  /// It takes the dimensionless parameter ycut (the Q value for normalisation
  /// of the kt-distances is taken from the sum of all particle energies).
  EECambridgeDirDiff (precision_type ycut_in) : _ycut(ycut_in) {}

  /// copy constructor
  EECambridgeDirDiff (const EECambridgeDirDiff & plugin) {
    *this = plugin;
  }

  // the things that are required by base class
  virtual std::string description () const override;
  virtual void run_clustering(fjcore::ClusterSequence &) const override;

  precision_type ycut() const {return _ycut;}

  /// the plugin mechanism's standard way of accessing the jet radius.
  /// This must be set to return something sensible, even if R
  /// does not make sense for this algorithm!
  virtual precision_type R() const override {return precision_type(1.0);}

  /// avoid the warning whenever the user requests "exclusive" jets
  /// from the cluster sequence
  virtual bool exclusive_sequence_meaningful() const override {return true;}

  /// returns true because this plugin is intended for spherical
  /// geometries (i.e. it's an e+e- algorithm).
  virtual bool is_spherical() const override {return true;}

private:
  precision_type _ycut;
};
//
} // end namespace panscales
#endif // __EECAMBRIDGEDIRDIFF_HH_
//
