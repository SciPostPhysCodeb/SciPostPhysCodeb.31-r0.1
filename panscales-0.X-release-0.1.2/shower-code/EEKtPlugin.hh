//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __EEKTPLUGIN_HH__
#define __EEKTPLUGIN_HH__

#include "Type.hh"
#include "fjcore_local.hh"

FJCORE_BEGIN_NAMESPACE      // defined in fastjet/internal/base.hh

// forward declaration to reduce includes
class ClusterSequence;

//----------------------------------------------------------------------
/// @ingroup plugins
/// \class EEKtPlugin
/// this is an implementation of the e+e- kt algorithm which uses
/// PanScales 4-momenta so as to perform a precise calculation of
/// (1-cos(theta))
///
/// Note: with the current version of fjcore, only the NNH strategy is
/// available. Code for the other strategies is (temporarily?)
/// commented out.
class EEKtPlugin : public JetDefinition::Plugin {
public:
  /// enum that contains the two clustering strategy options; for
  /// higher multiplicities, strategy_NNFJN2Plain is about a factor of
  /// two faster.
  ///
  /// #TRK_ISSUE-106  (we could add a NNFJN2Simple using
  /// ClusterSequence::plugin_simple_N2_cluster<MyBJ>() but this
  /// requires modifications of fjcore and should not bring much
  /// compared to NNFJN2Plain. Some code in the cc file handles this)
  enum Strategy { strategy_NNH = 0, strategy_NNFJN2Plain = 1};
  
  /// Main constructor for the eeKT Plugin class.
  EEKtPlugin (Strategy strategy = strategy_NNFJN2Plain) : _strategy(strategy) {}
  //EEKtPlugin (Strategy strategy = strategy_NNH) : _strategy(strategy) {}

  /// copy constructor
  EEKtPlugin (const EEKtPlugin & plugin) {
    *this = plugin;
  }

  // the things that are required by base class
  virtual std::string description () const;
  virtual void run_clustering(ClusterSequence &) const;

  /// the plugin mechanism's standard way of accessing the jet radius.
  /// This must be set to return something sensible, even if R
  /// does not make sense for this algorithm!
  ///
  /// Note; FJ requires R>2 but at the same times set _invR2 to 1 for
  /// native clustering and to 1/R2 for plugins. This means that all
  /// distances obtained from this plugin would have to be multiplied
  /// by R2 to get the native behaviour (alternatively, one would have
  /// to multiply ycut by 1/R^2)
  /// 
  virtual precision_type R() const {return 4.0;}

  /// avoid the warning whenever the user requests "exclusive" jets
  /// from the cluster sequence
  virtual bool exclusive_sequence_meaningful() const {return true;}

private:
  template<class N> void _actual_run_clustering(ClusterSequence &) const;
  
  Strategy _strategy;
};

FJCORE_END_NAMESPACE        // defined in fastjet/internal/base.hh

#endif // __EEKTPLUGIN_HH__

