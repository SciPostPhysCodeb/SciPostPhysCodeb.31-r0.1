//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __OBSERVABLES_HH__
#define __OBSERVABLES_HH__

#include "Event.hh"
#include <cmath>
#include <stdexcept>
#include <GSLRandom.hh>
#include <cassert>

//--------------------------------------------------------------------
/// @ingroup observable_classes
/// \class Observable
/// Observable base class. 

class Observable {
public:
  // Constructor.
  Observable(int select_in = 1) : select(select_in), n_few(0) {}

  // Analyse event.
  virtual bool analyse(const panscales::Event& event) = 0;

  virtual bool analyse(const panscales::Event& event,
                       const panscales::Momentum& thrust_axis) {return false;}

  // deprecated US spelling
  bool analyze(const panscales::Event& event){
    std::cerr << "Observable::analyze is deprecated and will be removed, use analyse instead" << std::endl;
    return analyse(event);
  }

  bool analyze(const panscales::Event& event,
               const panscales::Momentum& thrust_axis) {
    std::cerr << "Observable::analyze is deprecated and will be removed, use analyse instead" << std::endl;
    return analyse(event, thrust_axis);
  }

  // Provide a listing of the info.
  virtual void list() const = 0;

  // define operator() to return the observable of interest
  virtual precision_type operator()() const = 0;

  // Tell how many events could not be analysed.
  int nError() const {return n_few;}
  
protected:
  
  // Constants: could only be changed in the code itself.
  static const int    NSTUDYMIN, TIMESTOPRINT;

  // Particle selection
  int select;
  
  // Error statistics;
  int    n_few;
  
};
  
//==========================================================================
/// @ingroup observable_classes
/// \class Thrust 
/// class that calculates thrust

class Thrust : public Observable {
public:
  // Constructor
  Thrust(int select_in = 1)
    : Observable(select_in),
      _thrust(1.0), _one_minus_thrust(0.0){
    _version = 1;
    _emit_version_warning = true;
    _try_local_search = false;
    _intermediate_bound_margin = 10.;
    _local_search_failed = false;
    _max_approx_omthrust = 1e-4;
  }
  
  // Set the thrust axis
  bool set_thrust_axis(const panscales::Event& event);

  // Set the thrust axis
  bool set_thrust_axis_v2(const panscales::Event& event);

  // Set the thrust axis feeding in a vector of momenta instead
  // of an event. This is a bit of a hacky way around the fact
  // that we can't cast e.g. Event<dd_real> to Event<double>, which
  // is something we'd like to do in order to check 1-T evaluations
  // with different precision options.
  bool set_thrust_axis_v2_mom(std::vector<panscales::Momentum>& p, const precision_type& E_Tot);

  // Analyse event.
  virtual bool analyse(const panscales::Event& event) override {
    if( _version == 2 ) return set_thrust_axis_v2(event);
    else return set_thrust_axis(event);
  }

  // return the thrust axis
  panscales::Momentum event_axis() const {return _thrust_axis;}
  
  // Provide a listing of the info.
  virtual void list() const override;

  // Return info on results of analysis.
  virtual precision_type operator()() const override {return thrust();}
  precision_type thrust()             const {return _thrust;}
  precision_type one_minus_thrust() const {
    return _version == 2 ? _one_minus_thrust : 1.-_thrust;
  }

  // Interface to turn on/off v2 thrust code usage
  void set_version(unsigned int the_version) {
    if( the_version == 1 || the_version == 2 )
      _version = the_version;
    else {
      if(_emit_version_warning) {
        std::cout << "#\n#\n#\t";
        std::cout << "WARNING: thrust observable run with unimplemented version no." << "\n#";
        std::cout << "\n#\t";
        std::cout << "Defaulting to run with version = 1" << "\n#";
        _emit_version_warning = false;
      }
      _version = 1;
    }
  }
  
  // Interface to select use of fast approximate local search based
  // calculation or go direct to the full thing.
  void try_local_search(bool try_with_local_search) { _try_local_search = try_with_local_search; }

  // See detailed notes on the _max_approx_omthrust variable further
  // down this file to understand the use of the following.
  void setMaxApproxOMThrust(precision_type max_approx_omthrust) { _max_approx_omthrust = max_approx_omthrust; }

  // Interface to select use of fast approximate local search based
  // calculation or go direct to the full thing.
  void intermediate_bound_margin(precision_type intermediate_bound_margin) { _intermediate_bound_margin = intermediate_bound_margin; }

  // Enable outside world to analyse number of times the local search
  // has failed to converge for the current event being studied
  bool local_search_failed() { return _local_search_failed; }

protected:

  // What thrust computation to use
  unsigned int _version;

  // Flag to signal whether to emit warnings about version numbers of not
  bool _emit_version_warning;

  // A buffer on the 1-T upper bound -- that facilitates skipping in the
  // k-index loop --  to build some factor of safety into the
  // pt4.E() - pt4.modp() term of the skipping trigger
  precision_type _intermediate_bound_margin;

  // Maximum possible 1-T value that we will accept from the fast
  // local search calculation: if the local search comes up with
  // a 1-T value above this, it will always be abandoned and an
  // exhaustive search will be initiated instead to replace it.
  // The default value for this parameter has been 1e-4 here from
  // ~2018/2019 -> (present). This may well have been a major
  // case of excessive caution, since this threshold is NOT the
  // ONLY criterion determining whether the local search result
  // is scrapped: we also abandon the local search and initiate an
  // exhaustive search if the 1-T results obtained from 8 different
  // starting seeds are not all identical up to a relative error
  // ~machine-epsilon. (Note also that we may have been happy to
  // live with this threshold set so low because it would cause
  // little to no slowdown in the log-tests that use it, because
  // these throw events preferentially, and sometimes exclusively,
  // into regions of tiny thrust below the 1e-4 threshold.)
  precision_type _max_approx_omthrust;

  // Run with fast, approximate, local-searches for the thrust axis, with
  // some quality control checks, which, should they fail, run the fast, full
  // v2 thrust computation. Otherwise the exhaustive thrust axis search is
  // done directly instead.
  bool _try_local_search;

  // Shorthand for numeric_limit_extras<precision_type>::epsilon() and its square.
  const precision_type _eps  = fabs(numeric_limit_extras<precision_type>::epsilon());
  const precision_type _eps2 = _eps*_eps;

  // Track number of times we decide not to trust the local search result.
  bool _local_search_failed;
  
  // Outcome of analysis
  precision_type _thrust;
  precision_type _one_minus_thrust;
  panscales::Momentum _thrust_axis;
};

//======================================================================
/// @ingroup observable_classes
/// \class FCx 
/// class that calculates moments of energy-energy correlation (FCx) 
/// as defined in the appendix of ref. 0407286.

class FCx : public Thrust {

public:
  // Constructor.
  // x is the (fractional) parameter of the moment of EEC
  // by default select_in=1 -> keep all final state particles in the event
  FCx(precision_type x = 1.0, int select_in = 1)
    : Thrust(select_in), _x(x), _fcx(0.0) {}

  // Analyse event
  virtual bool analyse(const panscales::Event& event) override{
    this->set_thrust_axis(event);
    return analyse(event, this->_thrust_axis);
  }
  
  // Analyse event (thrust axis as input).
  virtual bool analyse(const panscales::Event& event,
                       const panscales::Momentum& thrust_axis) override;

  // As above, but just for massless particles, making use of the
  // dot and cross products, for greater numerical stability.
  virtual bool analyse(const panscales::Event& event,
                       const panscales::Momentum& n,
                       bool all_massless);

  // Provide a listing of the info.
  virtual void list() const override;
  
  // Return info on results of analysis
  virtual precision_type operator()() const override {return fcx();}
  precision_type fcx()  const {return _fcx;}
  precision_type taux() const {return _taux;}

private:
  // Outcome of analysis
  precision_type _x;
  precision_type _fcx;
  precision_type _taux;
};

//======================================================================
/// @ingroup observable_classes
/// \class Broadening 
/// class that calculates broadening
class Broadening : public Thrust {

public:
  // Constructor.
  // By default select_in=1 -> keep all final state particles in the event.
  // This function assumes: 1) ALL input momenta are MASSLESS
  //                        2) we are in the EVENT COM
  //                        3) ALL FS particles contribute
  Broadening(bool all_massless, int select_in = 1) :
    Thrust(select_in),
    _total(0.0), _wide_jet(0.0), _single_jet(0.0){
      /// Make sure no-one tries feeding in massive particles ...
      assert(all_massless && "Broadening:"
         "\n for now this code is only intended for use with events"
         "\n comprising of just massless particles. To use it, set"
         "\n the first constructor argument to true, to indicate you"
         "\n understand its domain of validity.");
    }

  // Analyse event
  virtual bool analyse(const panscales::Event& event) override {
    this->set_thrust_axis(event);
    return analyse(event, this->_thrust_axis);
  }

  // Analyse event (thrust axis as input).
  virtual bool analyse(const panscales::Event& event,
                       const panscales::Momentum& n) override;

  // Analyse event using code very close to EvShLib (thrust axis as input).
  virtual bool analyseEvShpLib(const panscales::Event& event,
                               const panscales::Momentum& n);

  // Provide a listing of the info.
  virtual void list() const override;
  
  // Access to results

  // Total broadening
  precision_type total() const {return _total;}

  // Wide-jet broadening
  precision_type wide_jet() const {return _wide_jet;}

  // Single-jet broadening
  precision_type single_jet() const {return _single_jet;}

private:

  // Total broadening
  precision_type _total;

  // Wide-jet broadening
  precision_type _wide_jet;

  // Single-jet broadening
  precision_type _single_jet;

};

//======================================================================
/// @ingroup observable_classes
/// \class Patch 
/// class that calculates energy flow in an η-φ patch

class Patch : public Thrust {
public:
  // default constructor defining an η-φ patch.
  // by default select_in=1 -> keep all final state particles in the event.
  // Notes on phi computation:
  // - use old phi: if true,  measure phi wrt z axis
  //                if false, measure phi using the perp vector defined by the thrust axis
  // - use random phi: when true, add a random offset to phi
  Patch(precision_type rap_range = 1.0, precision_type phi_range = 2.*M_PI,
        precision_type rap_center = 0.0, precision_type phi_center = 0.0,
        int select_in = 1)
    : Thrust(select_in),_rap_range(rap_range), _phi_range(phi_range),
      _rap_center(rap_center), _phi_center(phi_center),
      _patch_E(0.0), _patch_Et(0.0),
      _use_old_phi(true), _use_random_phi(false), _gsl_ptr(nullptr) {}
  
  // Analyse event
  virtual bool analyse(const panscales::Event& event) override {
    this->set_thrust_axis(event);
    return analyse(event, this->_thrust_axis);
  }

  // Analyse event (thrust axis as input).
  virtual bool analyse(const panscales::Event& event,
                       const panscales::Momentum& thrust_axis) override;

  // Return info on results of analysis
  virtual precision_type operator()() const override {return patch_E();}
  // return the patch energy, normalised to the total event energy  
  precision_type patch_E()  const {return _patch_E; }
  // return the patch transverse energy, normalised to the total energy  
  precision_type patch_Et() const {return _patch_Et;}

  // Allow the user to edit the |η| and φ extent of the patch.
  void set_rap_range(precision_type rap_range) { _rap_range = rap_range; }
  void set_phi_range(precision_type phi_range) { _phi_range = phi_range; }

  // Return the |η| and φ extent of the patch.
  precision_type get_rap_range() const { return _rap_range; }
  precision_type get_phi_range() const { return _phi_range; }

  // when set to true, a patch will add a random shift 
  void set_use_old_phi   (bool old   ){ _use_old_phi    = old;    }
  void set_use_random_phi(bool random, GSLRandom *gsl_ptr=nullptr){
    _use_random_phi = random;
    _gsl_ptr = gsl_ptr;
    if (_use_random_phi) assert(_gsl_ptr);
  }
  bool get_use_old_phi   () const { return _use_old_phi;    }
  bool get_use_random_phi() const { return _use_random_phi; }

private:
  precision_type _rap_range,  _phi_range;
  precision_type _rap_center, _phi_center;
  precision_type _patch_E, _patch_Et;
  bool _use_old_phi, _use_random_phi;
  GSLRandom *_gsl_ptr;
};

//======================================================================
/// IsolationConeEnergy
class IsolationConeEnergy : public Thrust {
public:
  // Constructor
  // - use old phi: if true,  measure phi wrt z axis
  //                if false, measure phi using the perp vector defined by the thrust axis
  // - use random phi: when true, add a random offset to phi
  IsolationConeEnergy(precision_type radius, int select_in = 1)
    : Thrust(select_in), _radius(radius),
      _cone_E(0.0), _use_random_phi(false), _gsl_ptr(nullptr)  {}
  
  // Analyse event
  virtual bool analyse(const panscales::Event& event) override {
    this->set_thrust_axis(event);
    return analyse(event, this->_thrust_axis);
  }

  // Analyse event (thrust axis as input).
  virtual bool analyse(const panscales::Event& event,
                       const panscales::Momentum& thrust_axis) override;

  // Return info on results of analysis
  virtual precision_type operator()() const override {return cone_E();}
  // return the patch energy, normalised to the total event energy  
  precision_type cone_E()  const {return _cone_E; }

  // Allow the user to edit the radius
  void set_radius(precision_type radius) { _radius = radius; }
  precision_type get_radius() const { return _radius; }

  // when set to true, a patch will add a random shift 
  void set_use_random_phi(bool random, GSLRandom *gsl_ptr=nullptr){
    _use_random_phi = random;
    _gsl_ptr = gsl_ptr;
    if (_use_random_phi) assert(_gsl_ptr);
  }
  bool get_use_random_phi() const { return _use_random_phi; }

private:
  precision_type _radius;
  precision_type _cone_E;
  bool _use_random_phi;
  GSLRandom *_gsl_ptr;
};

//======================================================================
/// @ingroup observable_classes
/// \class Y3 
/// class that calculates Durham Y3
class Y3 : public Observable {

public:
  // by default select_in=1 -> keep all final state particles in the event
  Y3(precision_type rts = 1.0, int select_in = 1)
    : Observable(select_in), _rts(rts) {}

  virtual bool analyse(const panscales::Event& event) override;
  
  // Provide a listing of the info.
  virtual void list() const override;
  
  // Return info on results of analysis
  virtual precision_type operator()() const override {return y3();}
  precision_type y3() const {return _y3;}

private:
  // Outcome of analysis
  precision_type _rts;
  precision_type _y3;

};

//======================================================================
/// @ingroup observable_classes
/// \class CParam 
/// class that calculates C-parameter in a N^2 way
class CParam : public Observable {
public:
  CParam(int select_in = 1) : Observable(select_in) {}
         
  // Provide a listing of the info.
  virtual void list() const override {}
  virtual bool analyse(const panscales::Event& event) override;

  virtual precision_type operator()() const override {return _Cparam;}
  
private:
  precision_type _Cparam;
  
};


#endif // end __OBSERVABLES_HH__
