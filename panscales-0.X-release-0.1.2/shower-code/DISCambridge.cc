
//FJSTARTHEADER
//
// Copyright (c) 2007-2018, Matteo Cacciari, Gavin P. Salam and Gregory Soyez
//
//----------------------------------------------------------------------
//  This file is distributed as part of PanScales but is HEAVILY based 
//  on a part of FastJet.
//
//  FastJet is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  The algorithms that underlie FastJet have required considerable
//  development. They are described in the original FastJet paper,
//  hep-ph/0512210 and in the manual, arXiv:1111.6097. If you use
//  FastJet as part of work towards a scientific publication, please
//  quote the version you use and include a citation to the manual and
//  optionally also to hep-ph/0512210.
//
//  FastJet is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with FastJet. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------
//FJENDHEADER

// fastjet stuff
#include "DISCambridge.hh"

// other stuff
#include <sstream>
#include <limits>

using namespace std;
using namespace panscales;

string DISCambridge::description () const {
  ostringstream desc;
  desc << "DISCambridge plugin with dcut = " << dcut() 
       << " and beam direction " << beam_direction();
  return desc.str();
}

void DISCambridge::run_clustering(fjcore::ClusterSequence & cs) const {
  int njets = cs.jets().size();
  fjcore::NNH<DISCamBriefJet> nnh(cs.jets());



  while (njets > 0) {
    int i, j, k;
    // here we get a minimum based on the purely angular variable from the NNH class
    // (called dij there, but vij in the Cambridge article (which uses dij for 
    // a kt distance...)
    precision_type vij = nnh.dij_min(i, j); // i,j are return values...

    // We can have either an ij case (j>=0) or iB (j<0).
    // In the mode of operation with dcut()>= 0 (like ee Cambridge), there are four scenarios:
    // - it's an ij and dij < dcut: recombine
    // - it's an ij and dij > dcut: softer of i & j -> inclusive jet, other one remains in clustering list
    // - it's an iB and diB < dcut: remove i from clustering list (but it's not an inclusive jet)
    // - it's an iB and diB > dcut: remove i from clustering list and make it an inclusive jet
    //
    // In the mode of operation with dcut() < 0 (like Aachen), there are two scenarios
    // - it's an ij: recombine
    // - it's an iB: call i a jet
    //

    if (j>= 0 && cs.jets()[i].E() > cs.jets()[j].E()) std::swap(i,j);
    precision_type scale = cs.jets()[i].E();
    precision_type dij = 2 * vij * scale * scale;

    /*if (j >= 0) {
      // make i the softer one
      if (dcut() >= 0 && dij > dcut()) {
        // we'll call the softer partner a "beam" jet
        cs.plugin_record_iB_recombination(i, dij);
        nnh.remove_jet(i);
      } else {
        cs.plugin_record_ij_recombination(i, j, dij, k);
        nnh.merge_jets(i, j, cs.jets()[k], k);
      }
    } else {
      if (dij > dcut() || dcut() < 0) cs.plugin_record_iB_recombination(i, dij);
      nnh.remove_jet(i);
    }*/

    /*std::cout<<"************************************* \n";
    std::cout<<"Pseudo jets in the cluster sequence \n";
    for(unsigned int l=0; l<cs.jets().size(); l++){
      std::cout<<"l = "<<l<<" px="<<cs.jets()[l].px()<<" py="<<cs.jets()[l].py()<<" pz="<<cs.jets()[l].pz()<<" E="<<cs.jets()[l].e()<<std::endl;
    }
    std::cout<<"************************************* \n";*/


    if(j==0){
      // this means the initial-state parton is the nearest neighbour
      // in this case we want to create a beam yet
      if (dij > dcut() || dcut() < 0) cs.plugin_record_iB_recombination(i, dij);
      nnh.remove_jet(i);
    } else if(j>0){ 
      // now the nearest neighbour is a final-state particle
      // in this case we do normal clustering 
      if (dcut() >= 0 && dij > dcut()) {
        // we'll call the softer partner a "beam" jet
        cs.plugin_record_iB_recombination(i, dij);
        nnh.remove_jet(i);
      } else {
        cs.plugin_record_ij_recombination(i, j, dij, k);
        nnh.merge_jets(i, j, cs.jets()[k], k);
      }
    } else {
      // if only one jet left in the event, either make this jet or do nothing
      if (dij > dcut() || dcut() < 0) cs.plugin_record_iB_recombination(i, dij);
      nnh.remove_jet(i);
    }
    njets--;
  }
    
}

std::vector<fjcore::PseudoJet> sorted_by_DISdiB(const std::vector<fjcore::PseudoJet> & jets) {
  if (jets.size() == 0) {
    std::vector<fjcore::PseudoJet> sorted_jets;
    return sorted_jets;
  }

  // prepare the diB values, with a view to using them for sorting subsequently
  const auto *cs = jets[0].associated_cluster_sequence();
  const auto & history = cs->history();
  std::vector<precision_type> diB_values;  
  diB_values.reserve(jets.size());
  for (const auto & j: jets) {
    diB_values.push_back(-history[j.cluster_sequence_history_index()].dij);
  }

  return objects_sorted_by_values(jets, diB_values);
}

//
