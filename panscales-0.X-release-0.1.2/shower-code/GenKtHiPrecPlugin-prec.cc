//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
// File generated automatically from ['GenKtHiPrecPlugin.cc'] by /Users/gsalam/work/2020-flavour-jet-alg/IRSafety-prec/convert-flav.py
// start of GenKtHiPrecPlugin.cc
#include "GenKtHiPrecPlugin-prec.hh"

#include <sstream>

// // to facilitate use with fjcore
// #ifndef __FJC_FLAVINFO_USEFJCORE__
// #include "fastjet/ClusterSequence.hh"
// #include "fastjet/SharedPtr.hh"
// #include "fastjet/NNFJN2Plain.hh"
// #endif

FASTJET_BEGIN_NAMESPACE  // defined in fastjet/internal/base.hh

using namespace std;


// forward declaration
class GenKtHiPrecBriefJet;

/// Info class for particle-independent information
class GenKtHiPrecNNInfo {
public:
 GenKtHiPrecNNInfo(const GenKtHiPrecPlugin * plugin)
     : _plugin(plugin) {}
 const GenKtHiPrecPlugin * plugin() const { return _plugin; }

private:
  const GenKtHiPrecPlugin * _plugin;

friend GenKtHiPrecBriefJet;
  
};


/// Class to run the generalised kt algorithm, with special care
/// in the handling of the rapidity calculation near rap=0
/// and of delta phi calculations for particles close to the 
/// x or y transverse axes.
///
/// The intention is to be able to perform clustering of extremely
/// collinear congifurations so long as we align the hard emitters
/// alog the x or y axes.
class GenKtHiPrecBriefJet {
public:
  void init(const PseudoJet & jet, const GenKtHiPrecNNInfo * info) {

    // first decant parameters
    _p  = info->plugin()->p();
    _R2 = pow(info->plugin()->R(),2);

    // then get the part of the kinematics that we want
    _pt2 = jet.pt2();
    precision_type pt = sqrt(_pt2);
    _nx = jet.px() / pt;
    _ny = jet.py() / pt;

    _phi = jet.phi();
    _rap = jet.rap();
    // transition is somewhat arbitrary
    const precision_type rap_transition = 0.1;
    if (fabs(_rap) < rap_transition) {
      // for moderately small rapidities switch to special rapidity formula
      //
      // rap = 1/2 log[(E+pz)/(E-pz)]
      //     = 1/2 log[1 + 2pz/(E-pz)]
      // 
      // and use log1p for the latter
      _rap = 0.5 * log1p(2*jet.pz()/(jet.E() - jet.pz()));
      //cout << "     using rap trans" << endl;
    }
    //cout << jet.cluster_hist_index() << " " << jet.px() << " " << jet.py() << " " << jet.pz() << " r=" << _rap << " " << _phi << endl;
  }

  /// geometric distance between this and other BriefJet
  precision_type geometrical_distance(const GenKtHiPrecBriefJet * other) const {

    // do straight rapidity difference, because we already took
    // care of making the rapidity as accurate as possible
    precision_type delta_y = _rap - other->_rap;
    precision_type delta_phi = std::fabs(_phi - other->_phi);
    if (delta_phi > pi) delta_phi = twopi - delta_phi;

    // transition is somewhat arbitrary, but should be such that
    // we are in a region where arcsin() is unambiguous; can be
    // O(1), but must be < pi/2
    const precision_type phi_transition = 0.1;

    if (delta_phi < phi_transition) {
      // take a cross product of the n's (normalised), which 
      // is simply equal to sin(delta_phi)
      precision_type cross = _nx * other->_ny - other->_nx * _ny;      
      // the sign can come out negative, but this isn't an issue
      // because we will use it in a context where the sign 
      // disappears
      delta_phi = asin(cross);
    }

    return delta_y*delta_y + delta_phi*delta_phi;
  }

  precision_type geometrical_beam_distance() const {return _R2;}

  precision_type momentum_factor() const {
    return pow(_pt2, _p);
  }


private:
  precision_type _pt2, _rap, _phi, _nx, _ny;
  // "info" quantities
  precision_type _p, _R2;
};


string GenKtHiPrecPlugin::description() const {
  ostringstream desc;
  desc << "Generalised-kt (hi-prec) plugin with R = " << R() << " and p = " << p();
  return desc.str();
}

void GenKtHiPrecPlugin::run_clustering(ClusterSequence & cs) const {

  GenKtHiPrecNNInfo info(this);
  NNFJN2Plain<GenKtHiPrecBriefJet, GenKtHiPrecNNInfo> nn(cs.jets(), &info);
  precision_type R2 = pow(R(),2);
  int njets = cs.jets().size();
  while (njets > 0) {
    int i, j, k;
    // get the i and j that minimize the distance
    // Divide by R2, because we had left 1/R^2 out of the
    // geometrical distances
    precision_type dij = nn.dij_min(i, j) / R2;  
  
    // do the appropriate recombination and update the nn
    if (j >= 0) {    // interparticle recombination
      cs.plugin_record_ij_recombination(i, j, dij, k);
      nn.merge_jets(i, j, cs.jets()[k], k); 
    } else {         // bbeam recombination
      cs.plugin_record_iB_recombination(i, dij);
      nn.remove_jet(i);
    }
    njets--;
  }

}

FASTJET_END_NAMESPACE
// end of GenKtHiPrecPlugin.cc
