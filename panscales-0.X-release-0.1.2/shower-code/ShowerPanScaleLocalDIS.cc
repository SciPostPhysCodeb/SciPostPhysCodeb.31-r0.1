//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerPanScaleLocalDIS.hh"

namespace panscales{

  //----------------------------------------------------------------------
  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerPanScaleLocalDIS::elements(
    Event & event, int dipole_index) const {
    int i3    = event.dipoles()[dipole_index].index_3()   ;
    int i3bar = event.dipoles()[dipole_index].index_3bar();
    ShowerPanScaleLocalDIS::Element *elm1, *elm2; 
    if (event.particles()[i3].is_initial_state()){
        // 3 is I, 3bar is F
        elm1 = new ShowerPanScaleLocalDIS::ElementIF(i3, i3bar, dipole_index, &event, this);
        elm2 = new ShowerPanScaleLocalDIS::ElementFI(i3bar, i3, dipole_index, &event, this);
    } else {
      if (event.particles()[i3bar].is_initial_state()){
        // 3 is F, 3bar is I
        elm1 = new ShowerPanScaleLocalDIS::ElementFI(i3, i3bar, dipole_index, &event, this);
        elm2 = new ShowerPanScaleLocalDIS::ElementIF(i3bar, i3, dipole_index, &event, this);
      } else {
        // 3 and 3bar are F (mapping is the same as for pp)
        elm1 = new ShowerPanScaleLocalDIS::ElementFF(i3, i3bar, dipole_index, &event, this);
        elm2 = new ShowerPanScaleLocalDIS::ElementFF(i3bar, i3, dipole_index, &event, this);
      }
    }      
    std::unique_ptr<typename ShowerBase::Element> elms[] = {
      std::unique_ptr<typename ShowerBase::Element>(elm1),
      std::unique_ptr<typename ShowerBase::Element>(elm2)
      };
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }

  ShowerBase::EmissionInfo* ShowerPanScaleLocalDIS::create_emission_info() const{
    return new ShowerPanScaleLocalDIS::EmissionInfo(); }

  //----------------------------------------------------------------------
  // base construction of the acceptance_probability;
  // fills the alphak, betak, kappat variables
  // same for every element so we put it here
  bool ShowerPanScaleLocalDIS::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalDIS::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;
    
    // see eqn (2.2) and (2.3) of the pp logbook
    precision_type kappat = _rho*exp(precision_type(lnv + _shower->_beta*abs(lnb))); 
    precision_type alphak = sqrt(_sjtilde/(_dipole_m2*_sitilde))*kappat*exp(precision_type(lnb)); 
    // now compute betak
    // betak  = sqrt(_sitilde/(dipole_m2_*_sjtilde))*kappat*exp(-lnb);
    // but we can use 1/alpha = sqrt(_sitilde/_sjtilde)*sqrt(dipole_m2_)*1/kappat*exp(-lnb)
    // so betak = kappat^2 / dipole_m2_ * 1/alphak
    precision_type betak = pow2(kappat)/_dipole_m2/alphak;

    emission_info.kappat = kappat;
    emission_info.alphak = alphak;
    emission_info.betak  = betak;

    // this is a dipole shower: only the emitter can split
    emission_info.emitter_weight_rad_gluon   = 0.0;
    emission_info.emitter_weight_rad_quark   = 0.0;
    emission_info.spectator_weight_rad_gluon = 0.0;
    emission_info.spectator_weight_rad_quark = 0.0;

    // in case this II element will be accepted we need to update all elements in the dipole cache
    emission_info.full_store_update_required = true;

    return true;
  }

  // do lorentz boost and then realign the tilted event with z-axis
  void ShowerPanScaleLocalDIS::Element::boost_all_in_chain(typename ShowerBase::EmissionInfo * emission_info_base, int dis_chain) const {
    // first we determine the needed coefficients
    // retrieve the reference directions
    const Momentum & p_ref_in  = _event->ref_in(dis_chain);
    const Momentum & p_ref_out = _event->ref_out(dis_chain);
    precision_type Q2_DIS      = _event->Q2_dis(dis_chain);

    // make the initial-state aligned with the z-axis
    // this induces a momentum imbalance because the photon is not touched
    Momentum pa = (*_event)[1].momentum();
    if((*_event)[1].get_DIS_chain() != dis_chain) pa = (*_event)[0].momentum();
    // reverse components if pz < 0 (because of rotation matrix below)
    if(p_ref_in.pz() < 0) pa = pa.reversed();

    Matrix3 rot_pa_to_z = Matrix3::from_direction(Momentum3<precision_type>(pa.px(), pa.py(), pa.pz())).transpose();
    
    for(unsigned i = 0; i < _event->size(); ++i) {
      Particle & p = (*_event)[i];
      // skip the partons not part of the chain
      if(p.get_DIS_chain() != dis_chain) continue;
      Momentum3<precision_type> new_mom_p3;
      // enforce the exact kinematics for incoming particles
      if(p.is_initial_state()){
        if(p_ref_in.pz() < 0) new_mom_p3 = Momentum3<precision_type>(0,0,-p.E());
        else                  new_mom_p3 = Momentum3<precision_type>(0,0, p.E());
      } else {// particle is final state, do the rotation
        new_mom_p3 = rot_pa_to_z * Momentum3<precision_type>(p.px(), p.py(), p.pz());
      }
      // reset the momenta
      (*_event)[i].reset_momentum(Momentum::fromP3M2(new_mom_p3, p.m2()));
    }
    // rotate the directional differences as well 
    if(use_diffs()){
       // then handle the dipole differences
      for (auto & dipole: _event->dipoles()) {
        if((*_event)[dipole.index_q].get_DIS_chain() != dis_chain) continue;
        dipole.dirdiff_3_minus_3bar = rot_pa_to_z *dipole.dirdiff_3_minus_3bar; 
      }
      // then handle the dipole differences for non-splitting dipoles
      for (auto & dipole: _event->non_splitting_dipoles()) {
        if((*_event)[dipole.index_q].get_DIS_chain() != dis_chain) continue;
        dipole.dirdiff_3_minus_3bar = rot_pa_to_z *dipole.dirdiff_3_minus_3bar;        
      }
    }

    // now we perform the boost
    const Momentum pf = _event->sum_momentum_out_in_chain(dis_chain); 
    // get the sudakov components along the in and out reference momenta
    precision_type af = dot_product(pf, p_ref_out) / dot_product(p_ref_in, p_ref_out);
    precision_type bf = dot_product(pf, p_ref_in)  / dot_product(p_ref_in, p_ref_out);
    
    LorentzBoostDIS<MomentumM2<precision_type>> boost(p_ref_in, p_ref_out, pf, Q2_DIS, af, bf);
    if(use_diffs()){
      // we need to do the boost but also store the energies of the particles before they enter the boost
      // then handle the boost, making sure we store the energies
      std::vector<precision_type> Ei(_event->size());
      for(unsigned i = 0; i < _event->size(); ++i) {
          Particle & p = (*_event)[i];
          Ei[i] = p.E();
          if(p.get_DIS_chain() != dis_chain) continue;
          // and boost the momenta
          (*_event)[i].reset_momentum(Momentum::fromP3M2((boost*p).p3(), p.m2()));
      }

       // then handle the dipole differences
      for (auto & dipole: _event->dipoles()) {
        if((*_event)[dipole.index_q].get_DIS_chain() != dis_chain) continue;
        int i3bar = dipole.index_3bar();
        int i3    = dipole.index_3();    
        dipole.dirdiff_3_minus_3bar = boost.apply_linear_transform_to_dirdiff(Ei[i3bar], (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);
      }

      // then handle the dipole differences for non-splitting dipoles as well
      for (auto & dipole: _event->non_splitting_dipoles()) {
        if((*_event)[dipole.index_q].get_DIS_chain() != dis_chain) continue;
        int i3bar = dipole.index_3bar();
        int i3    = dipole.index_3();    
        dipole.dirdiff_3_minus_3bar = boost.apply_linear_transform_to_dirdiff(Ei[i3bar], (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);
      }
    } else{
      // just do the boost on the momenta
      for(unsigned i = 0; i < _event->size(); ++i) {
          Particle & p = (*_event)[i];
          if(p.get_DIS_chain() != dis_chain) continue;
          (*_event)[i].reset_momentum(Momentum::fromP3M2((boost*p).p3(), p.m2()));
      }
    }
  } 
  //======================================================================
  // ShowerPanScaleLocalDIS::ElementIF implementation
  //
  //======================================================================

  double ShowerPanScaleLocalDIS::ElementIF::lnb_extent_const() const {
    double xi      = to_double(event().pdf_x(emitter()));
    double xi_over_omxi = xi / (1 - xi);
    double lnxij_over_omxij = log(xi_over_omxi);
    return to_double(_log_dipole_m2 - 2.*_log_rho - lnxij_over_omxij)/(1. + _shower->_beta);
  }

  Range ShowerPanScaleLocalDIS::ElementIF::lnb_generation_range(double lnv) const{
    // We assume here that alphak and betak are small
    double xi      = to_double(event().pdf_x(emitter()));
    double xi_over_omxi = xi / (1 - xi);
    double lnxi_over_omxi = log(xi_over_omxi);
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv                  ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv - lnxi_over_omxi ) / (1+_shower->_beta));
  }

  // return the acceptance probability for the given kinematic point
  bool ShowerPanScaleLocalDIS::ElementIF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // construct alphak, betak which is the same for every element
    ShowerPanScaleLocalDIS::Element::acceptance_probability(emission_info_base);

    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalDIS::EmissionInfo*>(emission_info_base));
    
    precision_type z   = emission_info.alphak / (1. + emission_info.alphak);

    // here we should make a decision based on flavour.
    //
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), z,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);
    // see DIS logbook
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = (1. + emission_info.alphak);   
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = (1. + emission_info.alphak);  
    }
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = z;
    emission_info.z_radiation_wrt_spectator = 0.;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    normalisation_factor *= g_fcn(emission_info.lnb);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;

    // in case this IF element will be accepted we do not need to update all elements in the dipole cache
    emission_info.full_store_update_required = true;
    return true;
  }
  

  // check after doing the acceptance prob whether the actual kinematics can return false
  bool ShowerPanScaleLocalDIS::ElementIF::check_after_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // retrieve emission info
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalDIS::EmissionInfo*>(emission_info_base));
    
    // build the radiated mapping coefficients
    double gamma = 1./(1.+_shower->beta());
    emission_info.ak = emission_info.alphak; 
    emission_info.bk = pow(1 + emission_info.alphak, (2.*gamma))*emission_info.betak;
    // following can be found in
    // Eqs. (3.110a, 3.110b, 3.110c, 3.110d) with f=1 in logbook/2020-12-09-pp-schemes.pdf
    emission_info.ai = 1 + emission_info.ak;
    emission_info.bi = emission_info.ak * emission_info.bk / emission_info.ai;
    emission_info.bj = 1 - emission_info.bk / emission_info.ai;

    // check whether bj can fall below zero. Possible when ai < bk
    // approximately betak*(1+alphak^2)/(1+alphak) , alphak > 1 but betak < 1
    if(emission_info.bj <= 0) return false;

    return true;

  }

  // carry out the splitting and update the event
  bool ShowerPanScaleLocalDIS::ElementIF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalDIS::EmissionInfo*>(emission_info_base));
    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);
    // make sure we are on the same chain and did not mix up
    assert(emitter().get_DIS_chain() == spectator().get_DIS_chain() && "Error - emitter and spectator should belong to same DIS chain");
    // retrieve cached info
    const double & phi = emission_info.phi;
    const precision_type & ak = emission_info.ak; 
    const precision_type & bk = emission_info.bk;
    const precision_type & ai = emission_info.ai;
    const precision_type & bi = emission_info.bi; 
    const precision_type & bj = emission_info.bj; 
    
    // perp normalisation 
    precision_type norm_perp = sqrt(ak*bk*_dipole_m2);

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);

    //fill the emitter, radiator and spectator 3-momenta
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> spectator_out3 = bj * rp.spectator.p3();

    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    // update the PDF x fraction (with the ones after the boost)
    // we need the momentum of the hard system here
    const Momentum pf_event_frame = _event->sum_momentum_out_in_chain(emitter().get_DIS_chain());
    const precision_type & pf_m2 = pf_event_frame.m2();
    Momentum pf_old      = Momentum::fromP3M2(pf_event_frame.p3(), pf_m2);
    // add the difference in momentum
    // note we get - spectator + spectator' + radiation
    //  =          - spectator + bj * spectator + ak * emitter + bk * spectator + kT
    //  =          (bk + bj - 1) * spectator + ak * emitter + kT
    //  =          ak * bk / ai * spectator  + ak * emitter + kT
    // M2   = (ak^2 * bk / ai - ai ak * bk/ ai)dipole_m2 
    // M2   = (ak^2 * bk / ai - (ak + ak^2) * bk/ ai)dipole_m2 
    //      =  ak * (-1 + bj) = - ak * bk /ai
    // since bj = 1 - bk / ai = 1 - bk / ( 1 + ak) = (1 + ak - bk)/(1 + ak)
    // same would be
    Momentum3<precision_type> pf_diff3 = ak * bk / ai * spectator().p3() + ak * emitter().p3() + rp.rotn_from_z*(perp.p3());
    precision_type            pf_diff0 = ak * bk / ai * spectator().E() + ak * emitter().E() + perp.E();
    Momentum pf_diff = Momentum4<precision_type>(pf_diff3.px(), pf_diff3.py(), pf_diff3.pz(), pf_diff0);

    // new get the new momentum of the final state, of which we only need the mass
    Momentum4<precision_type> pf_new = pf_old.p4() + pf_diff.p4();  
    // after this we rotate & boost this system (apart from photon)
    // but the pf mass is preserved in this process
    // hence we may use pf_new.m2() to determine the new x fraction
    if (emitter().initial_state()==1){ // emitter is from beam1
      emission_info.beam1_pdf_new_x_over_old = (_event->Q2_dis(emitter().get_DIS_chain()) + pf_new.m2()) /(_event->Q2_dis(emitter().get_DIS_chain()) + pf_old.m2());  
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // emitter is from beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = (_event->Q2_dis(emitter().get_DIS_chain()) + pf_new.m2()) /(_event->Q2_dis(emitter().get_DIS_chain()) + pf_old.m2());  
    }  
    
    return true;
  }
  
  /// do the final boost
  void ShowerPanScaleLocalDIS::ElementIF::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {

    // add all the new particles to the event
    ShowerBase::Element::update_event(transition_runner, emission_info_base);

    // do the boost
    ShowerPanScaleLocalDIS::Element::boost_all_in_chain(emission_info_base, emitter().get_DIS_chain());
  }
  
  //======================================================================
  // ShowerPanScaleLocalDIS::ElementFI implementation
  //======================================================================

  double ShowerPanScaleLocalDIS::ElementFI::lnb_extent_const() const{
    double xj      = to_double(event().pdf_x(spectator()));
    double xj_over_omxj = xj / (1 - xj);
    double lnxj_over_omxj = log(xj_over_omxj);
    return to_double(_log_dipole_m2 - 2.*_log_rho - lnxj_over_omxj)/(1. + _shower->_beta);
  }
  
  Range ShowerPanScaleLocalDIS::ElementFI::lnb_generation_range(double lnv) const{
    // We assume here that alphak and betak are small
    double xj      = to_double(event().pdf_x(spectator()));
    double xj_over_omxj = xj / (1 - xj);
    double lnxj_over_omxj = log(xj_over_omxj); 
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv + lnxj_over_omxj) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv                 ) / (1+_shower->_beta));
  }
  
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleLocalDIS::ElementFI::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    //construct alphak, betak which is the same for every element
    ShowerPanScaleLocalDIS::Element::acceptance_probability(emission_info_base);

    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalDIS::EmissionInfo*>(emission_info_base));

    // impose some constraints so that all the mapping coefficients
    // are well-behaved
    // We want that ak < 1 with ak = alphak 
    if (emission_info.alphak >= 1) return false;
    
    // for a final-state splitting we 
    // relate the z fraction directly to alphak
    precision_type z   = emission_info.alphak;

    // here we should make a decision based on flavour.
    //
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), z,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = z;
    emission_info.z_radiation_wrt_spectator = 0.;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    //
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    normalisation_factor *= g_fcn(emission_info.lnb);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;
  
    // The PDF rescaling factors of the spectator to be updated 
    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    // exact form is for the initial-state parton
    // new_x_over_old =  1.0 + emission_info.alphak/(1.0-emission_info.betak) = 1.0 approximated
    // so both rescaling factors are 1 -> do not need to check which one is initial-state
    emission_info.beam1_pdf_new_x_over_old = 1.0;
    emission_info.beam2_pdf_new_x_over_old = 1.0;

    return true;
  }
  
  /// carry out the splitting and update the event
  bool ShowerPanScaleLocalDIS::ElementFI::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalDIS::EmissionInfo*>(emission_info_base));
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);

    // build the radiated mapping coefficients
    // same as for the pp showers
    precision_type ak = emission_info.alphak;
    precision_type bk = emission_info.betak;
    precision_type kt = emission_info.kappat;
    
    precision_type ai = 1 - ak;
    precision_type bj = 1 + bk/ai;
    precision_type bi = ak * bk / ai;

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = kt*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    
    // fill the 3-momenta of the emitter, radiated and spectator
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> spectator_out3 =                        bj * rp.spectator.p3();
    
     // Now construct the final 4-momenta by imposing the masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
 
    // fill in the exact PDF rescaling
    // now the rescaling is simply given by bj as there is no boost to perform
    if (spectator().initial_state()==1){ // spectator is beam1
      emission_info.beam1_pdf_new_x_over_old = bj;   
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // spectator is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = bj;
    }
    return true;
  }
    
  /// update the event with chain information
  void ShowerPanScaleLocalDIS::ElementFI::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {

    // add all the new particles to the event
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
  }
  //======================================================================
  // ShowerPanScaleLocalDIS::ElementFF implementation
  //======================================================================
  double ShowerPanScaleLocalDIS::ElementFF::lnb_extent_const() const {
    return to_double(_log_dipole_m2 - 2.*_log_rho)/(1. + _shower->_beta);
  }
  
  Range ShowerPanScaleLocalDIS::ElementFF::lnb_generation_range(double lnv) const{  
    // this corresponds to Eq. (52 - 53) of 2018-07-notes.pdf (and to the soft limit used in the Sudakov)
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv ) / (1+_shower->_beta));
  }

  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleLocalDIS::ElementFF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    //construct alphak, betak which is the same for every element
    ShowerPanScaleLocalDIS::Element::acceptance_probability(emission_info_base);
    
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalDIS::EmissionInfo*>(emission_info_base));
    
    // test phase space boundary (Eq. (42) of 2018-07-notes.pdf)
    if (emission_info.alphak + emission_info.betak >= 1.0) {return 0;}
    
    precision_type z   = emission_info.alphak;
    
    // here we should make a decision based on flavour.
    //
    // pg (pq) corresponds to the emission of a gluon (quark)
    _shower->fill_dglap_splitting_weights(emitter(), z,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark); 

    // pdf x fractions do not change
    emission_info.beam1_pdf_new_x_over_old = 1.0;
    emission_info.beam2_pdf_new_x_over_old = 1.0;

    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter   = z;
    emission_info.z_radiation_wrt_spectator = 0.;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    //
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    normalisation_factor *= g_fcn(emission_info.lnb);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;

    return true;
  }
  
  /// carry out the splitting and update the event
  bool ShowerPanScaleLocalDIS::ElementFF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{

    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalDIS::EmissionInfo*>(emission_info_base));
    
    const double & phi = emission_info.phi;

    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);

    // build the radiated mapping coefficients
    const precision_type & ak = emission_info.alphak;
    const precision_type & bk = emission_info.betak;
    const precision_type & kt = emission_info.kappat;

    // same as for the pp shower
    precision_type ai = 1.-ak;
    precision_type bi = ak * bk / ai;
    precision_type bj = 1. - bk / ai; 
    
    // for cases with a plane, phi=0 means in-plane
    Momentum perp = kt * (sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> spectator_out3 =                        bj * rp.spectator.p3();

    // Now construct the final 4-momenta by imposing the masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    return true;
  }  

  /// update the event with chain information
  void ShowerPanScaleLocalDIS::ElementFF::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {

    // add all the new particles to the event
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
  }
  
} // namespace panscales
