//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerPanScaleGlobalpp.hh"
#include "WarningMap.hh"

using namespace std;

namespace panscales{

  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerPanScaleGlobalpp::elements(
    // This is an antenna shower, so we need one element per dipole
    // Definition is that we have [3end, 3bar-end] for each dipole
    // For IF and FI, the definition is then that the first letter corresponds
    // to the 3end of the dipole, and the 2nd letter corresponds to the 3bar-end
    Event & event, int dipole_index) const {
    int i3    = event.dipoles()[dipole_index].index_3()   ;
    int i3bar = event.dipoles()[dipole_index].index_3bar();
    ShowerPanScaleGlobalpp::Element *elm;
    if (event.particles()[i3].is_initial_state()){
      if (event.particles()[i3bar].is_initial_state()){
        // II dipole
        elm = new ShowerPanScaleGlobalpp::ElementII(i3, i3bar, dipole_index, &event, this);
      } else {
        // 3 is I, 3bar is F
        elm = new ShowerPanScaleGlobalpp::ElementIF(i3, i3bar, dipole_index, &event, this);
      }
    } else {
      if (event.particles()[i3bar].is_initial_state()){
        // 3 is F, 3bar is I
        // we make this choice such that we do not copy the kinematics of IF for an FI element
        elm = new ShowerPanScaleGlobalpp::ElementIF(i3bar, i3, dipole_index, &event, this);
      } else {
        // 3 and 3bar are F
        elm = new ShowerPanScaleGlobalpp::ElementFF(i3, i3bar, dipole_index, &event, this);
      }
    }      
    std::unique_ptr<typename ShowerBase::Element> elms[] = {std::unique_ptr<typename ShowerBase::Element>(elm)};
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }

  ShowerBase::EmissionInfo* ShowerPanScaleGlobalpp::create_emission_info() const{ return new ShowerPanScaleGlobalpp::EmissionInfo(); }

  //----------------------------------------------------------------------
  Range ShowerPanScaleGlobalpp::Element::lnb_generation_range(double lnv) const {
    assert(false && "this should be reimplemented in the derived element classes");
    return Range(0,0);
  }
  
  /// MB-NOTE not sure what this function is doing
  Range ShowerPanScaleGlobalpp::Element::lnb_exact_range(double lnv) const {
      std::cerr << "Error: We do not expect lnb_exact_range to be called " << std::endl;
      return Range(0,0);
  }

  //----------------------------------------------------------------------

  /// bit of acceptance probability that is common to all the elements
  bool ShowerPanScaleGlobalpp::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalpp
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalpp::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;

    // see Eqs. (4.3) and (4.6) in arXiv:2205.02237
    precision_type kappat = _rho*exp(precision_type(lnv + _shower->_beta*abs(lnb)));
    precision_type alphak = sqrt(_sjtilde/(_dipole_m2*_sitilde))*kappat*exp(precision_type(lnb));
    // now compute betak
    // betak  = sqrt(_sitilde/(_dipole_m2*_sjtilde))*kappat*exp(-lnb);
    // but we can use 1/alpha = sqrt(_sitilde/_sjtilde)*sqrt(_dipole_m2)*1/kappat*exp(-lnb)
    // so betak = kappat^2 / _dipole_m2 * 1/alphak
    precision_type betak = pow2(kappat)/_dipole_m2/alphak;

    emission_info.kappat = kappat;
    emission_info.alphak = alphak;
    emission_info.betak  = betak;

    // this is an antenna shower
    // either the emitter can split or the spectator
    f_fcn(lnb, emission_info.f, emission_info.omf);
    emission_info.emitter_weight_rad_gluon   = 0.0;
    emission_info.emitter_weight_rad_quark   = 0.0;
    emission_info.spectator_weight_rad_gluon = 0.0;
    emission_info.spectator_weight_rad_quark = 0.0;
    
    return true;
  }


  // do_kinematics should be reimplemented in the derived classes
  bool ShowerPanScaleGlobalpp::Element::do_kinematics(
    typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const {
        assert(false && "this should be reimplemented in the derived element classes");
        return false; 
  }

  // These go into the derived classes
  void ShowerPanScaleGlobalpp::Element::update_event(const ColourTransitionRunnerBase *transition_runner,
                                                   typename ShowerBase::EmissionInfo * emission_info) {
    // Call update event of base shower class
    assert(false && "this should be reimplemented in the derived element classes");
  }

  // Eq. (4.16) of 2020-12-09-pp-schemes.pdf
  // B^mu_nu = g^mu_nu + 2 Q^mu Q'_nu / Q^2 - 2 (Q + Q')^mu (Q + Q')_nu / (Q + Q')^2
  // where Q = p_hard , Q'= p_hard post branching
  void ShowerPanScaleGlobalpp::Element::lorentz_boost_hard_system(const Momentum& pH_tilde, const Momentum& pH_boosted) const {
    // set up the lorentz boost
    // Eq. (4.16) of 2020-12-09-pp-schemes.pdf
    // B^mu_nu = g^mu_nu + 2 Q^mu Q'_nu / Q^2 - 2 (Q + Q')^mu (Q + Q')_nu / (Q + Q')^2
    // where Q = p_hard , Q'= p_hard post branching
    LorentzBoostPG<MomentumM2<precision_type>> lboost(pH_tilde, pH_boosted);

    // if not using directional differences just boost the hard system
    if(!use_diffs()){
      // need to get loop over hard system particles only here
      for(unsigned i = 0; i < _event->size(); ++i ){
        Particle & p = (*_event)[i];
        if(p.is_initial_state() || !p.is_in_hard_system()) continue;
        Momentum3<precision_type> result = (lboost*p).p3();
        p.reset_momentum(Momentum::fromP3M2(result, p.m2()));
      }
    } else{ 
      // when using dir diff we have to make sure that directional
      // differences get updated if there are partons in the hard system
      // 
      // the boost acts only on particles in the hard system
      std::vector<MomentumM2<precision_type>> pre_boost_momenta(_event->size());
      for(unsigned i = 0; i < _event->size(); ++i) {
        Particle & p = (*_event)[i];
        pre_boost_momenta[i] = (*_event)[i];
        if(p.is_initial_state() || !p.is_in_hard_system()) continue;
        Momentum3<precision_type> result = (lboost*p).p3();
        p.reset_momentum(Momentum::fromP3M2(result, p.m2()));
      }

      // then handle the dipole differences
      for (auto & dipole: _event->dipoles()) {

        unsigned int i3bar = dipole.index_3bar();
        unsigned int i3    = dipole.index_3();

        // the boost acts only on the hard-system partons
        bool i3_unboosted    = (*_event)[i3].is_initial_state()    || !(*_event)[i3]   .is_in_hard_system();
        bool i3bar_unboosted = (*_event)[i3bar].is_initial_state() || !(*_event)[i3bar].is_in_hard_system();

        if (i3_unboosted && i3bar_unboosted) continue;
        else if (i3_unboosted){
          // here we are in a situation where the 3bar end of the dipole is boosted
          precision_type E3bar  = pre_boost_momenta[i3bar].E();
          precision_type EB3bar = (*_event)[i3bar].E();
          
          // Note that using the definition
          // delta_d3bar = lboost.lambda(pre_boost_momenta[i3bar])/E3bar + (1/E3bar - 1/EB3bar)*(*_event)[i3bar];
          // this is too unstable, but we may use that 
          // (1/E3bar - 1/EB3bar) = (EB3bar-E3bar) /E3bar EB3bar = - lboost.lambda(pre_boost_momenta[i3bar]).E()/E3bar EB3bar
          // same is done below when i3bar_unboosted=true
          Momentum4<precision_type> lambda_boost_i3bar = lboost.lambda(pre_boost_momenta[i3bar]);
          Momentum4<precision_type> delta_d3bar = lambda_boost_i3bar/E3bar - (lambda_boost_i3bar.E())/(E3bar*EB3bar)*(*_event)[i3bar];
          dipole.dirdiff_3_minus_3bar = dipole.dirdiff_3_minus_3bar + delta_d3bar;
        } else if (i3bar_unboosted){
          // here we are in a situation where the 3 end of the dipole is boosted
          precision_type E3  = pre_boost_momenta[i3].E();
          precision_type EB3 = (*_event)[i3].E();
          Momentum4<precision_type>  lambda_boost_i3 = lboost.lambda(pre_boost_momenta[i3]);
          Momentum4<precision_type>  delta_d3 = lambda_boost_i3/E3 - (lambda_boost_i3.E())/(E3*EB3)*(*_event)[i3];
          
          dipole.dirdiff_3_minus_3bar = dipole.dirdiff_3_minus_3bar - delta_d3;
        } else{
          //both ends need to be boosted
          dipole.dirdiff_3_minus_3bar = apply_linear_transform_to_dirdiff(lboost, pre_boost_momenta[i3bar].E(), pre_boost_momenta[i3].E(), (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);      
        }
      }
      // then handle the dipole differences for non-splitting dipoles as well
      for (auto & dipole: _event->non_splitting_dipoles()) {
        unsigned int i3bar = dipole.index_3bar();
        unsigned int i3    = dipole.index_3();
        // we check whether the 3 or 3bar end of the dipole needs boosting
        // the boost acts only on the hard-system partons
        bool i3_unboosted    = (*_event)[i3].is_initial_state()    || !(*_event)[i3]   .is_in_hard_system();
        bool i3bar_unboosted = (*_event)[i3bar].is_initial_state() || !(*_event)[i3bar].is_in_hard_system();

        if (i3_unboosted && i3bar_unboosted) continue; // if both are true we do not need to do anything
        else if (i3_unboosted){
          // here we are in a situation where the 3bar end of the dipole is boosted
          precision_type E3bar  = pre_boost_momenta[i3bar].E();
          precision_type EB3bar = (*_event)[i3bar].E();
          Momentum4<precision_type>  lambda_boost_i3bar = lboost.lambda(pre_boost_momenta[i3bar]);
          Momentum4<precision_type>  delta_d3bar = lambda_boost_i3bar/E3bar - (lambda_boost_i3bar.E())/(E3bar*EB3bar)*(*_event)[i3bar];
          dipole.dirdiff_3_minus_3bar = dipole.dirdiff_3_minus_3bar + delta_d3bar;
        } else if (i3bar_unboosted){
          // here we are in a situation where the 3 end of the dipole is boosted
          precision_type E3  = pre_boost_momenta[i3].E();
          precision_type EB3 = (*_event)[i3].E();
          Momentum4<precision_type>  lambda_boost_i3 = lboost.lambda(pre_boost_momenta[i3]);
          Momentum4<precision_type>  delta_d3 = lambda_boost_i3/E3 - (lambda_boost_i3.E())/(E3*EB3)*(*_event)[i3];
          dipole.dirdiff_3_minus_3bar = dipole.dirdiff_3_minus_3bar - delta_d3;
        } else{
          dipole.dirdiff_3_minus_3bar = apply_linear_transform_to_dirdiff(lboost, pre_boost_momenta[i3bar].E(), pre_boost_momenta[i3].E(), (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);      
        }
      }
    }

  }


  //======================================================================
  // ShowerPanScaleGlobalpp::ElementII implementation
  //======================================================================
  double ShowerPanScaleGlobalpp::ElementII::lnb_extent_const() const {
    double xi      = to_double(event().pdf_x(emitter()));
    double xj      = to_double(event().pdf_x(spectator()));
    double xi_over_omxi = xi / (1 - xi);
    double xj_over_omxj = xj / (1 - xj);
    double lnxij_over_omxij = log(xi_over_omxi*xj_over_omxj);
    return to_double(_log_dipole_m2 - 2.*_log_rho - lnxij_over_omxij)/(1. + _shower->_beta);
  }

  Range ShowerPanScaleGlobalpp::ElementII::lnb_generation_range(double lnv) const{
    // We assume here that alphak and betak are small
    double xi      = to_double(event().pdf_x(emitter()));
    double xj      = to_double(event().pdf_x(spectator()));
    double xi_over_omxi = xi / (1 - xi);
    double xj_over_omxj = xj / (1 - xj);
    double lnxi_over_omxi = log(xi_over_omxi);
    double lnxj_over_omxj = log(xj_over_omxj); 
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv + lnxj_over_omxj ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv - lnxi_over_omxi ) / (1+_shower->_beta));
  }
  
  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleGlobalpp::ElementII::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{

    //construct alphak, betak which is the same for every element
    ShowerPanScaleGlobalpp::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalpp
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalpp::EmissionInfo*>(emission_info_base));

    // determine the z factors that go into the splitting kernels
    // See Eq. (4.8) in arXiv:2205.02237
    // we say that za = ak/ai with ak = alphak
    // the rescaling factor for the initial-state is ra = ai = 1 + ak + omega*aH
    // omega = 0 in the soft limit, so we take ra = 1 + ak
    // this means in the soft limit we approximate za = ak/ra, zb = bk/rb
    
    // before applying the rescaling factor to restore momenta (ra and rb)
    // these expressions are exact
    precision_type za   = emission_info.alphak / (1. + emission_info.alphak);
    precision_type omza = 1.                   / (1. + emission_info.alphak); // =~ 1/ra
    precision_type zb   = emission_info.betak  / (1. + emission_info.betak);
    precision_type omzb = 1.                   / (1. + emission_info.betak);  // =~ 1/rb

    
    // here we should make a decision based on flavour
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), za,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    _shower->fill_dglap_splitting_weights(spectator(), zb,
                                          emission_info.spectator_weight_rad_gluon,
                                          emission_info.spectator_weight_rad_quark);

    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    // we use the omza = 1/ra and omzb = 1/rb approximated expressions from above 
    if (emitter().initial_state()==1){ // emitter is beam1, spectator is beam 2
      emission_info.beam1_pdf_new_x_over_old = 1.0/omza; // =~ ra
      emission_info.beam2_pdf_new_x_over_old = 1.0/omzb; // =~ rb 
    } else {                           // emitter is beam2, spectator is beam 1
      emission_info.beam1_pdf_new_x_over_old = 1.0/omzb; // =~ rb
      emission_info.beam2_pdf_new_x_over_old = 1.0/omza; // =~ ra
    }
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = za;
    emission_info.z_radiation_wrt_spectator = zb;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    
    emission_info.emitter_weight_rad_gluon   *= emission_info.  f * normalisation_factor;
    emission_info.emitter_weight_rad_quark   *= emission_info.  f * normalisation_factor;
    emission_info.spectator_weight_rad_gluon *= emission_info.omf * normalisation_factor;
    emission_info.spectator_weight_rad_quark *= emission_info.omf * normalisation_factor;
    
    return true;
  }

  /// carry out the splitting and update the event
  bool ShowerPanScaleGlobalpp::ElementII::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalpp
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalpp::EmissionInfo*>(emission_info_base));
    
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    // build the radiated mapping coefficients
    // that can be found in
    // Eqs. (B.28a-c) in arxiV:2205.02237
    precision_type ak = emission_info.alphak;
    precision_type bk = emission_info.betak;
    precision_type kt2 = pow2(emission_info.kappat);
    precision_type norm_perp = emission_info.kappat;

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    // store for spin, when using dir-diffs this get rotated to the right plane
    emission_info.k_perp = perp;
    
    // store the hard momentum in the normal event frame
    emission_info.p_hard_tilde = _event->momentum_sum_hard_system();
    
    Momentum3<precision_type> p_hard3 = rp.rotn_from_z.transpose()* emission_info.p_hard_tilde;
    Momentum p_hard = Momentum::fromP3M2(p_hard3, emission_info.p_hard_tilde.m2());

    // construct the sudakov components of the hard momentum
    precision_type aH = 2.*dot_product(p_hard, rp.spectator)/_dipole_m2;
    precision_type bH = 2.*dot_product(p_hard, rp.emitter)  /_dipole_m2;
    // Note that dot_product(p_hard, perp) = dot_product(Hperp, perp) in II elements
    precision_type ra = 0; // ai rescaling coefficients ra'*(1+ak) = ra
    precision_type rb = 0; // bj rescaling coefficients 

    // For the first emission we want the rapidity of the Z boson to be conserved
    // not of the hard system
    // so here if this gets filled, we just want to add the flag that the 
    // radiated particle becomes part of the hard system !after the boost!
    // This is contrary to what we do in IF, where the radiated particle
    // becomes part of the hard system ! before the boost !
    // if( kt2 > factor*_shower->Q2()) -> emission_info.radiation_is_hard = true;
    // then we need to store the radiation in the hard system after the boost (in update_event)

    emission_info.radiation_is_hard = false;
    precision_type omega = sqrt(1. + (2 * dot_product(p_hard, perp) + kt2)/(aH * bH * _dipole_m2)) - 1.;
    emission_info.boost_pa = omega*aH; // these are invariants
    emission_info.boost_pb = omega*bH; 

    // define the rescaling coefficients   
    ra = 1. + ak + emission_info.boost_pa;
    rb = 1. + bk + emission_info.boost_pb;
    
    // build the momenta
    Momentum3<precision_type> emitter_out3   = ra * rp.emitter.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> spectator_out3 =                        rb * rp.spectator.p3();
    
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
   
    // update the PDF x fractions
    // needs to be implemented
    if (emitter().initial_state()==1){ // emitter is beam1, spectator is beam 2
      emission_info.beam1_pdf_new_x_over_old = ra;
      emission_info.beam2_pdf_new_x_over_old = rb;
    } else {                           // emitter is beam2, spectator is beam 1
      emission_info.beam1_pdf_new_x_over_old = rb;
      emission_info.beam2_pdf_new_x_over_old = ra;
    }
 
    return true;
  }

  /// do the final boost (see Appendix (B2) in arXiv:2205.02237)
  void ShowerPanScaleGlobalpp::ElementII::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {
    
    
    const Momentum pa_tilde = emitter();
    const Momentum pb_tilde = spectator();

    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalpp
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    const typename ShowerPanScaleGlobalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalpp::EmissionInfo*>(emission_info_base));
    
    const Momentum p_hard_tilde   = emission_info.p_hard_tilde;
    Momentum p_hard_boosted = p_hard_tilde - emission_info.k_perp + emission_info.boost_pa*pa_tilde + emission_info.boost_pb*pb_tilde;
    
    ShowerPanScaleGlobalpp::Element::lorentz_boost_hard_system(p_hard_tilde, p_hard_boosted);

    // Reset the hard system status of the new radiated particle
    (*_event)[_event->size()-1].set_hard_system(emission_info.radiation_is_hard);
    
  }

  //======================================================================
  // ShowerPanScaleGlobalpp::ElementIF implementation
  //
  //======================================================================
  double ShowerPanScaleGlobalpp::ElementIF::lnb_extent_const() const {
    double xi      = to_double(event().pdf_x(emitter()));
    double xi_over_omxi = xi / (1 - xi);
    double lnxi_over_omxi = log(xi_over_omxi);
    return to_double(_log_dipole_m2 - 2.*_log_rho - lnxi_over_omxi)/(1. + _shower->_beta);
  }

  Range ShowerPanScaleGlobalpp::ElementIF::lnb_generation_range(double lnv) const{
    // We assume here that alphak and betak are small
    double xi      = to_double(event().pdf_x(emitter()));
    double xi_over_omxi = xi / (1 - xi);
    double lnxi_over_omxi = log(xi_over_omxi);
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv                  ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv - lnxi_over_omxi ) / (1+_shower->_beta));
  }
  
  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleGlobalpp::ElementIF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    
    //construct alphak, betak which is the same for every element
    ShowerPanScaleGlobalpp::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalpp
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalpp::EmissionInfo*>(emission_info_base));
    // bk needs to be smaller then 1 to prevent negative energies for the spectator
    if(emission_info.betak >= 1){
      return false;
    }

    // determine the z factors that go into the splitting kernels
    // See Eq. (4.8) in 2205.02237
    // we say that za = ak/ai with ak = alphak
    // the rescaling factor for the initial-state is ra = ai = 1 + ak + omega*aH
    // omega = 0 in the soft limit, so we take ra = 1 + ak
    // this means in the soft limit we approximate za = ak/ra, zb = bk
    precision_type za   = emission_info.alphak / (1. + emission_info.alphak);
    precision_type omza = 1.                   / (1. + emission_info.alphak); // =~ 1/ra
    precision_type zb   = emission_info.betak;
    
    // here we should make a decision based on flavour.
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), za,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    _shower->fill_dglap_splitting_weights(spectator(), zb,
                                          emission_info.spectator_weight_rad_gluon,
                                          emission_info.spectator_weight_rad_quark);

    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    // we use the omza = 1/ra approximated expressions from above 
    if (emitter().initial_state()==1){ // emitter is beam1, spectator is beam 2
      emission_info.beam1_pdf_new_x_over_old = 1.0/omza; // =~ ra
      emission_info.beam2_pdf_new_x_over_old = 1.0; 
    } else {                           // emitter is beam2, spectator is beam 1
      emission_info.beam1_pdf_new_x_over_old = 1.0; 
      emission_info.beam2_pdf_new_x_over_old = 1.0/omza; // =~ ra
    }

      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = za;
    emission_info.z_radiation_wrt_spectator = zb;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    
    emission_info.emitter_weight_rad_gluon   *= emission_info.  f * normalisation_factor;
    emission_info.emitter_weight_rad_quark   *= emission_info.  f * normalisation_factor;
    emission_info.spectator_weight_rad_gluon *= emission_info.omf * normalisation_factor;
    emission_info.spectator_weight_rad_quark *= emission_info.omf * normalisation_factor;
    
    return true;
    
  }
  
  /// carry out the splitting and update the event
  bool ShowerPanScaleGlobalpp::ElementIF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalpp
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalpp::EmissionInfo*>(emission_info_base));

    // retrieve cached info
    const double & phi = emission_info.phi;
    
    // build the radiated mapping coefficients
    // that can be found in
    // Eqs. (B33a-d) in arXiv:2205.02237
    precision_type ak = emission_info.alphak;
    precision_type bk = emission_info.betak;
    precision_type norm_perp = emission_info.kappat;

    precision_type bj = 1. - bk;

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    // store for spin, when using dir-diffs this get rotated to the right plane
    emission_info.k_perp = perp;
    // need the unrotated form as well
    Momentum k_perp_org = rp.rotn_from_z*perp;
    
    // Now construct the boost coefficients
    // initial state partons:
    // if emitter belongs to beam 1 (initial_state == 1), we need the beam 2 particle (index = 1)
    // if emitter belongs to beam 2 (initial_state == 2), we need the beam 1 particle (index = 0)
    const Momentum & pa_tilde = emitter();
    const Momentum & pb_tilde = (*_event)[emitter().initial_state()%2];
    // get their dot product - used for normalisation
    precision_type dAB = dot_product(pa_tilde, pb_tilde);
    
    // get the hard momentum
    const Momentum & p_hard = _event->momentum_sum_hard_system();

    // Let us get some Sudakov components
    // 1. of the hard system
    precision_type aH = dot_product(p_hard, pb_tilde) / dAB;
    precision_type bH = dot_product(p_hard, pa_tilde) / dAB;
    // 2. of the kperp
    precision_type aT = dot_product(k_perp_org, pb_tilde) / dAB;
    // precision_type bT = dot_product(k_perp_org, pa_tilde) / dAB;
    // bT = 0 as pi_tilde.k_perp = pa_tilde.k_perp = 0
    // 3. define c
    precision_type c;

    // now we construct the case-dependent coefficients
    // in every case we write the pHbar (i.e. the hard momentum after the map) as
    // pHbar = (aH + del_a)pa_tilde + (bH + del_b)pb_tilde + ptH + del_k
    // and the required boosted pHbar
    // Lambda(pHbar) = (Omega_a + aH + Delta_a)pa_tilde + (Omega_b + bH + Delta_b)pb_tilde + ptH + Delta_k + del_k
    //               = (aH + del_a)omega pa_tilde + (bH + del_b)omega pb_tilde + boost_kperp
    // with ra = 1 + Omega_a, rb = 1 + Omega_b
    // Omega_a = (aH + del_a)*omega - aH - Delta_a
    // Omega_b = (bH + del_b)*omega - bH - Delta_b
    // where omega = sqrt(1 + c / ((aH + del_a)(bH + del_b)sab))
    // and c depends on the cases
    // also the form of del_a, del_b, Delta_a, Delta_b, boost_kperp is case-dependent
    precision_type Delta_a, Delta_b, del_a, del_b;
    Momentum boost_kperp;
    if(spectator().is_in_hard_system()){ // pjtilde in H [case 2, 3 in notes]
      // get the sudakov decomposition of
      // the spectator
      precision_type a_sp = dot_product(spectator(), pb_tilde) / dAB; 
      precision_type b_sp = dot_product(spectator(), pa_tilde) / dAB;
      if((emission_info.f < 0.5) && !_shower->hard_system_fixed()){ // pjtilde, pk in H [case 3 in notes]
        emission_info.radiation_is_hard = true;
        Delta_a = 0.; Delta_b = 0.;
        del_a       = ak + aT;
        del_b       = 0.;
        boost_kperp = Momentum(p_hard.px(), p_hard.py(), 0., 0.);
        // the defn of  c       = (delta_perp.m2() + 2. * dot_product(delta_perp, p_hard));
        // is equal to:
        c       = -(ak * bk * _dipole_m2 + 2. * (p_hard.px() * k_perp_org.px() + p_hard.py() * k_perp_org.py()));
      } else{
        emission_info.radiation_is_hard = false;
        del_a    = -bk*a_sp;
        del_b    = -bk*b_sp;
        Delta_a  = -(ak+ aT) + del_a;
        Delta_b  = del_b;
        Momentum Delta_perp  = Momentum(-k_perp_org.px(),  -k_perp_org.py() , 0., 0.);
        Momentum delta_perp  = Momentum(-bk * spectator().px(), -bk * spectator().py(), 0., 0.);
        boost_kperp = Momentum(p_hard.px()  + Delta_perp.px() + delta_perp.px(),
                                p_hard.py() + Delta_perp.py() + delta_perp.py(), 0., 0.);
        c       = - (Delta_perp.m2() + 2. * dot_product(Delta_perp, p_hard  + delta_perp));
      }
    } else{                              // pjtilde, pk not in H [case 1 in notes]
      // note that this solution assumes that pk^2 = 0
      emission_info.radiation_is_hard = false;
      del_a = 0.; del_b = 0.;
      Delta_b     = 0.0;
      Delta_a     = - (ak + aT);
      c           = - 2. * (p_hard.px() * k_perp_org.px() + p_hard.py() * k_perp_org.py()) + (ak * bk) * _dipole_m2;
      boost_kperp = Momentum(p_hard.px() - k_perp_org.px(),
                             p_hard.py() - k_perp_org.py(), 0., 0.);
    }
    // build omega
    precision_type x     = c/(2*(aH + del_a)*(bH+del_b)*dAB);
    precision_type omega_minus_one, omega;
    
    // protect against cases that x is very small
    if(std::abs(x) < numeric_limit_extras<precision_type>::sqrt_epsilon()){
      // take the x->0 expansion of sqrt(1+x) - 1
      omega_minus_one = x/2; //< higher-order contributions are beyond accuracy
    } else{
      omega_minus_one = sqrt(1+x) - 1;
    }
    omega = omega_minus_one + 1;
    
    // store the rescaling coefficient for the initial-state partons
    emission_info.ra = 1. + aH * omega_minus_one + del_a * omega - Delta_a;
    emission_info.rb = 1. + bH * omega_minus_one + del_b * omega - Delta_b;
    // store the boosted hard momentum 
    emission_info.p_hard_boosted = (aH + del_a) * omega * pa_tilde + (bH + del_b) * omega * pb_tilde + boost_kperp;

    // build the momenta
    Momentum3<precision_type> emitter_out3   = emission_info.ra * rp.emitter.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> spectator_out3 =                        bj * rp.spectator.p3();
  
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    // update the PDF x fractions
    if (emitter().initial_state()==1){ // emitter is beam1, spectator is beam 2
      emission_info.beam1_pdf_new_x_over_old = emission_info.ra;
      emission_info.beam2_pdf_new_x_over_old = emission_info.rb;
    } else {                           // emitter is beam2, spectator is beam 1
      emission_info.beam1_pdf_new_x_over_old = emission_info.rb;
      emission_info.beam2_pdf_new_x_over_old = emission_info.ra;
    }

   // return if successful
   return true;
  }
  
  /// do the final boost
  void ShowerPanScaleGlobalpp::ElementIF::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalpp
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    const typename ShowerPanScaleGlobalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalpp::EmissionInfo*>(emission_info_base));

    const Momentum pb_tilde = (*_event)[emitter().initial_state()%2]; //< no reference - otherwise it will change after the update event

    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    // Reset the momentum of the initial-state particle opposite to the emitter
    (*_event)[emitter().initial_state()%2].reset_momentum(emission_info.rb * pb_tilde);
    // set the new particle in the hard system
    (*_event)[_event->size()-1].set_hard_system(emission_info.radiation_is_hard);
    // Retrieve the momentum of the hard system pre-boost but post splitting
    const Momentum p_hard_tilde = (*_event).momentum_sum_hard_system();

    // Do the boost
    ShowerPanScaleGlobalpp::Element::lorentz_boost_hard_system(p_hard_tilde, emission_info.p_hard_boosted);
    
  }
  
  //======================================================================
  // ShowerPanScaleGlobalpp::ElementFF implementation
  //======================================================================
  double ShowerPanScaleGlobalpp::ElementFF::lnb_extent_const() const {
    return to_double(_log_dipole_m2 - 2.*_log_rho)/(1. + _shower->_beta);
  }

  Range ShowerPanScaleGlobalpp::ElementFF::lnb_generation_range(double lnv) const{  
    // this corresponds to Eq. (52 - 53) of 2018-07-notes.pdf (and to the soft limit used in the Sudakov)
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv ) / (1+_shower->_beta));
  }

  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleGlobalpp::ElementFF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    
    //construct alphak, betak which is the same for every element
    ShowerPanScaleGlobalpp::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalpp
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalpp::EmissionInfo*>(emission_info_base));

    // impose that ak and bk are smaller then 1
    // if they are larger, they will result in negative energies of our emitter/spectator
    // after rescaling them
    if((emission_info.alphak >= 1) || (emission_info.betak >= 1)){
      return false;
    }
    // both are final state so za = ak, zb = bk
    precision_type za   = emission_info.alphak;
    precision_type zb   = emission_info.betak;
    
    // here we should make a decision based on flavour.
    //
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), za,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    _shower->fill_dglap_splitting_weights(spectator(), zb,
                                          emission_info.spectator_weight_rad_gluon,
                                          emission_info.spectator_weight_rad_quark);

    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    emission_info.beam1_pdf_new_x_over_old = 1.0;
    emission_info.beam2_pdf_new_x_over_old = 1.0; 
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = za;
    emission_info.z_radiation_wrt_spectator = zb;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    
    emission_info.emitter_weight_rad_gluon   *= emission_info.  f * normalisation_factor;
    emission_info.emitter_weight_rad_quark   *= emission_info.  f * normalisation_factor;
    emission_info.spectator_weight_rad_gluon *= emission_info.omf * normalisation_factor;
    emission_info.spectator_weight_rad_quark *= emission_info.omf * normalisation_factor;
    return true;
  }
  
  /// carry out the splitting and update the event
  bool ShowerPanScaleGlobalpp::ElementFF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    // retrieve emission_info
    typename ShowerPanScaleGlobalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalpp::EmissionInfo*>(emission_info_base));
    
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    // build the radiated mapping coefficients
    // that can be found in
    // Eqs. (B38a-e) in arXiv:2205.02237
    precision_type ak = emission_info.alphak;
    precision_type bk = emission_info.betak;
    precision_type norm_perp = emission_info.kappat;

    // rescaling coefficients for the emitter (ai) and spectator (bj)
    precision_type ai = 1. - ak;
    precision_type bj = 1. - bk;

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    // store for spin, when using dir-diffs this get rotated to the right plane
    emission_info.k_perp = perp;
    // store kperp with respect to z axis
    Momentum k_perp_org = rp.rotn_from_z*perp;
    
    // now we first get the rescaling coefficient, as this features in the mapping coefficients below
    precision_type rl, rl_minus_one;
    if(_shower->rescale_option() == PanGlobalRescaleOption::LocalRescaling){
      // we need to tame the energy of the perp component (and avoid a negative rescaling coefficient)
      // we then rescale the emission through
      // rl           = Q.((1+2ak) pitilde + pjtilde) / Q.((1+2ak) pitilde + pjtilde + kT)
      // rl_minus_one = - Q.kT / Q.((1+2ak) pitilde + pjtilde + kT)
      precision_type QkT = 2.*dot_product(_event->Q(), k_perp_org);
      rl_minus_one = - QkT / (_sitilde + _sjtilde + QkT);
      rl           = 1. + rl_minus_one;
    } else if (_shower->rescale_option() == PanGlobalRescaleOption::GlobalRescalingDeprecated){
      rl = 1.; rl_minus_one = 0.;
    } else{
      assert(false && "Unknown rescaling option");
    }

    // the general solution, for all 6 cases described below, will always be of the following form:
    // boost(pHbar) = boost_a * pa + boost_b * pb + boost_kperp
    // omega = sqrt(1 + (aH*(del_b + eps_b) + bH*(del_a + eps_a) + (Del_a + del_a)*(Del_b + del_b))/(aH*bH) + (2*ptH.(eps_perp - Del_perp) + del_k*_sijtilde/(aH*bH*sab)) 
    // boost_a = aH*omega
    // boost_b = bH*omega
    // ra = 1 + boost_a - aH - Del_a - del_a
    // rb = 1 + boost_b - bH - Del_b - del_b
    // if numerical issues arise one might want to think about expanding the argument of the square root
    // eps_perp - Del_perp = (rl-1)* (pitilde_perp + pjtilde_perp) + rl * perp_perp
    // boost_kperp = pTH + Del_perp + del_perp
    //
    // or in other form
    // omega = sqrt(1 + (aH*(del_b + eps_b) + bH*(del_a + eps_a) + (Del'_a)*(Del'_b))/(aH*bH) + (2*ptH.(eps'_perp) + del_k*_sijtilde/(aH*bH*sab))  \equiv sqrt(1+x) (which may need to be expanded for ra, rb)
    // ra = 1 + boost_a - aH - Del'_a
    // rb = 1 + boost_b - bH - Del'_b
    // eps'_perp = (rl-1)* (pitilde_perp + pjtilde_perp) + rl * perp_perp
    // and we define c == 2*pTH.eps'_perp
    // boost_kperp = ptH + Del'_perp
    //
    // Note that aH, bH, sab, _sijtilde, c = 2*pTH.eps'_perp are universal, do not depend on the case so they are calculated just once
    // all other coefficients, i.e. {del_a, del_b, del_k, eps_a, eps_b, Del'_a, Del'_b, Del'_perp}, depend on what is in the hard system
    
    // now we define the relevant quantities
    // initial state partons:
    const Momentum & pa_tilde = (*_event)[_event->incoming1_idx()]; // along +(-)z
    const Momentum & pb_tilde = (*_event)[_event->incoming2_idx()]; // along -(+)z
    // get their dot product - used for normalisation (sab = 2*dAB)
    precision_type dAB = dot_product(pa_tilde, pb_tilde);
    
    // get the hard momentum
    const Momentum & p_hard = _event->momentum_sum_hard_system();

    // first get the universal coefficients
    // 1. of the hard system
    precision_type aH = dot_product(p_hard, pb_tilde) / dAB;
    precision_type bH = dot_product(p_hard, pa_tilde) / dAB;
    // 2. of the emitter and spectator
    precision_type a_em, b_em, a_sp, b_sp;
    bool calculate_sudakovs; // to keep track of whether we still need to calculate the sudakov values (done this way because for the different cases below not all are needed for rl = 1)
    if(rl_minus_one != 0){   // in this case we need all sudakovs no matter what case we enter
      a_em = dot_product(emitter(), pb_tilde)   / dAB; 
      b_em = dot_product(emitter(), pa_tilde)   / dAB;
      a_sp = dot_product(spectator(), pb_tilde) / dAB; 
      b_sp = dot_product(spectator(), pa_tilde) / dAB;
      calculate_sudakovs = false;
    } else{                  // in this case we mostly will not need the sudakov components - let us not waste time on calculating them! 
      a_em = 0.; b_em = 0.; a_sp = 0.; b_sp = 0.;
      calculate_sudakovs = true;
    } 
    // 3. of the kperp
    precision_type aT = dot_product(k_perp_org, pb_tilde) / dAB;
    precision_type bT = dot_product(k_perp_org, pa_tilde) / dAB; 
    // 4. get c = 2* pTH.eps'_perp = 2 * pTH *  (rl-1)* (pitilde_perp + pjtilde_perp) + rl * perp_perp
    precision_type c = - 2. * (   p_hard.px() * (rl_minus_one * (emitter().px() + spectator().px()) + rl * k_perp_org.px()) 
                                + p_hard.py() * (rl_minus_one * (emitter().py() + spectator().py()) + rl * k_perp_org.py()));

    // now we construct the case-dependent coefficients
    precision_type del_a, del_b, eps_a, eps_b, Delp_a, Delp_b, del_k;
    Momentum boost_kperp;
    if(emitter().is_in_hard_system()){ // pitilde in H [case 2, 4, 6, 7 in notes]
      if(spectator().is_in_hard_system()){ // pjtilde in H [case 6, 7 in notes]
        if(_shower->hard_system_fixed()){  // pitilde, pjtilde in H, pk not in H
          assert(false && "Need to derive the mapping coefficients for pitilde, pjtilde in H, pk not in H");
          emission_info.radiation_is_hard = false;
        } else{                            // pitilde, pjtilde, pk in H [case 6 in notes]
          emission_info.radiation_is_hard = true;
          del_a = 0.; del_b = 0.; Delp_a = 0.; Delp_b = 0.;
          eps_a = rl_minus_one * (a_sp + a_em) + rl * aT;
          eps_b = rl_minus_one * (b_sp + b_em) + rl * bT;
          del_k       = (pow2(rl_minus_one) - pow2(rl)*ak*bk)*_dipole_m2;
          boost_kperp = Momentum(p_hard.px(), p_hard.py(), 0., 0.);
        }
      } else{                              //pjtilde not in H [case 2, 4 in notes]
        if((emission_info.f > 0.5) && !_shower->hard_system_fixed()){ // pitilde, pk in H [case 4 in notes]
          emission_info.radiation_is_hard = true;
          if(calculate_sudakovs){ // sudakov for emitter not needed for rl = 1
            a_sp = dot_product(spectator(), pb_tilde) / dAB; 
            b_sp = dot_product(spectator(), pa_tilde) / dAB;
          }
          eps_a = rl_minus_one * a_em + rl * aT;
          eps_b = rl_minus_one * b_em + rl * bT;
          del_a = rl * bk * a_sp;
          del_b = rl * bk * b_sp;
          Delp_a      = del_a - rl_minus_one * a_sp;
          Delp_b      = del_b - rl_minus_one * b_sp;
          del_k       = (rl*rl_minus_one*bk - pow2(rl)*ak*bk)*_dipole_m2;
          boost_kperp = Momentum(p_hard.px() +(rl * bk - rl_minus_one) * spectator().px(),
                                 p_hard.py() +(rl * bk - rl_minus_one) * spectator().py(), 0., 0.);
        } else{                                                            // pitilde in H, pk not in H [case 2 in notes]
          emission_info.radiation_is_hard = false;
          if(calculate_sudakovs){ // sudakov for spectator not needed for rl = 1
            a_em = dot_product(emitter(), pb_tilde) / dAB; 
            b_em = dot_product(emitter(), pa_tilde) / dAB;
          }
          eps_a = rl_minus_one * a_em;
          eps_b = rl_minus_one * b_em;
          del_a = -rl * ak * a_em ;
          del_b = -rl * ak * b_em ;
          Delp_a      = del_a - rl_minus_one * a_sp - rl * aT;
          Delp_b      = del_b - rl_minus_one * b_sp - rl * bT;
          del_k       = (- rl*rl_minus_one*ak + pow2(rl)*ak*bk)*_dipole_m2;
          boost_kperp = Momentum(p_hard.px() - rl_minus_one * spectator().px() - rl * (ak * emitter().px() + k_perp_org.px()),
                                 p_hard.py() - rl_minus_one * spectator().py() - rl * (ak * emitter().py() + k_perp_org.py()), 0., 0.);
        }
      }
    } else{ // pitilde not in H [case 1, 3, 5 in notes]
      if(spectator().is_in_hard_system()){ // pjtilde in H [case 3, 5 in notes]
        if((emission_info.f < 0.5) && !_shower->hard_system_fixed()){ // pjtilde, pk in H [case 5 in notes]
          emission_info.radiation_is_hard = true;
          if(calculate_sudakovs){ // sudakov for spectator not needed for rl = 1
            a_em = dot_product(emitter(), pb_tilde) / dAB; 
            b_em = dot_product(emitter(), pa_tilde) / dAB;
          }
          eps_a = rl_minus_one * a_sp + rl * aT;
          eps_b = rl_minus_one * b_sp + rl * bT;
          del_a = rl * ak * a_em;
          del_b = rl * ak * b_em;
          Delp_a      = del_a - rl_minus_one * a_em;
          Delp_b      = del_b - rl_minus_one * b_em;
          del_k       = (rl*rl_minus_one*ak - pow2(rl)*ak*bk)*_dipole_m2;
          boost_kperp = Momentum(p_hard.px() +(rl * ak - rl_minus_one) * emitter().px(),
                                 p_hard.py() +(rl * ak - rl_minus_one) * emitter().py(), 0., 0.);

        } else{                                                            // pjtilde in H, pk not in H [case 3 in notes]
          emission_info.radiation_is_hard = false;
          if(calculate_sudakovs){ // sudakov for emitter not needed for rl = 1
            a_sp = dot_product(spectator(), pb_tilde) / dAB; 
            b_sp = dot_product(spectator(), pa_tilde) / dAB;
          }
          eps_a = rl_minus_one * a_sp;
          eps_b = rl_minus_one * b_sp;
          del_a = -rl * bk * a_sp ;
          del_b = -rl * bk * b_sp ;
          Delp_a      = del_a - rl_minus_one * a_em - rl * aT;
          Delp_b      = del_b - rl_minus_one * b_em - rl * bT;
          del_k       = (- rl*rl_minus_one*bk + pow2(rl)*ak*bk)*_dipole_m2;
          boost_kperp = Momentum(p_hard.px() - rl_minus_one * emitter().px() - rl * (bk * spectator().px() + k_perp_org.px()),
                                 p_hard.py() - rl_minus_one * emitter().py() - rl * (bk * spectator().py() + k_perp_org.py()), 0., 0.);
        }
      } else{ // pitilde, pjtilde, pk not in H [case 1 in notes]
        emission_info.radiation_is_hard = false;
        del_a = 0.; del_b = 0.; eps_a = 0.; eps_b = 0.;
        Delp_a      = -rl_minus_one * (a_em + a_sp) - rl * aT;
        Delp_b      = -rl_minus_one * (b_em + b_sp) - rl * bT;
        del_k       = (- pow2(rl_minus_one) + pow2(rl)*ak*bk)*_dipole_m2;
        boost_kperp = Momentum(p_hard.px() - rl_minus_one * (emitter().px() + spectator().px()) - rl * k_perp_org.px(),
                               p_hard.py() - rl_minus_one * (emitter().py() + spectator().py()) - rl * k_perp_org.py(), 0., 0.);
      }
    }


    // build omega
    precision_type omega, omega_minus_one;
    precision_type x = (del_b + eps_b)/bH + (del_a + eps_a)/aH + Delp_a*Delp_b/(aH*bH) + (c + del_k)/(2*aH*bH*dAB);
    // #TRK_ISSUE-398  check numerical precision [todo]
    omega_minus_one  = sqrt(1. + x) - 1.;
    omega            = omega_minus_one + 1.;

    // store the rescaling coefficient for the initial-state partons
    emission_info.ra = 1. + aH*(omega_minus_one) - Delp_a;
    emission_info.rb = 1. + bH*(omega_minus_one) - Delp_b;
    // store the boosted hard momentum 
    emission_info.p_hard_boosted = aH * omega * pa_tilde + bH * omega * pb_tilde + boost_kperp;

    // build the radiated momenta
    Momentum3<precision_type> emitter_out3   = rl * (ai * rp.emitter.p3());
    Momentum3<precision_type> radiation3     = rl * (ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3());
    Momentum3<precision_type> spectator_out3 = rl * (                       bj * rp.spectator.p3()); 

    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
   
    // update the PDF x fractions
    emission_info.beam1_pdf_new_x_over_old = emission_info.ra;
    emission_info.beam2_pdf_new_x_over_old = emission_info.rb;

    // return if succesfull 
    return true;
  }  

  // do the final boost
  void ShowerPanScaleGlobalpp::ElementFF::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalpp
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    const typename ShowerPanScaleGlobalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalpp::EmissionInfo*>(emission_info_base));

    const Momentum pa_tilde = (*_event)[_event->incoming1_idx()];
    const Momentum pb_tilde = (*_event)[_event->incoming2_idx()];

    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);

    // Set the hard system status of the new radiated particle
    (*_event)[_event->size()-1].set_hard_system(emission_info.radiation_is_hard);

    // Reset the momentum of the initial-state particles
    (*_event)[_event->incoming1_idx()].reset_momentum(emission_info.ra * pa_tilde);
    (*_event)[_event->incoming2_idx()].reset_momentum(emission_info.rb * pb_tilde);

    // Construct the momentum of the hard system pre-boost
    const Momentum p_hard_tilde = _event->momentum_sum_hard_system();
    
    // Do the boost
    ShowerPanScaleGlobalpp::Element::lorentz_boost_hard_system(p_hard_tilde, emission_info.p_hard_boosted);
    
  }

  /// PanGlobal one emission ME as computed in 2022-07-25-pp-matching
  // This weight needs to be multiplied by dlnv dlnb
  // MvB 30-10-2023: why is this here - comment it all out for now
  // do not remove these comments!
  // double ShowerPanScaleGlobalpp::pp2Z_one_emission_effective_ME(const Event event, HoppetRunner *hoppet_runner) const{

  //   assert(event.size() == 4 && "This is the shower ME for Zj, which requires exactly 4 particles");
  //   assert((_rescaling_option == PanGlobalRescaleOption::GlobalRescalingDeprecated) && "Check matching for this rescaling option!");

  //   double beta = _beta;  
  //   precision_type rts = event.root_s(); 

  //   double Xrap       = to_double(event[2].momentum().rap());
  //   precision_type mX = event[2].momentum().m(); 
  //   double etaj       = to_double(event[3].momentum().rap());
  //   precision_type pt = event[3].momentum().pt();


  //   // Now convert to shower variables according to eq. 31
  //   precision_type ak = pt * exp( etaj - Xrap) / mX;
  //   precision_type bk = pt * exp(-etaj + Xrap) / mX;
    
  //   double lnb  = 0.5 * to_double(log(ak/bk)); // Inverting eqs. 31+32
  //   double lnkt = 0.5 * to_double(log(ak*bk));
  //   double lnv = lnkt - beta * abs(lnb) + to_double(log(mX));
    
  //   int flavap = event[0].pdgid();
  //   int flavbp = event[1].pdgid();
    
  //   double alphas = qcd().alphasMSbar(lnkt);

  //   precision_type za = ak / (1.0 + ak);
  //   precision_type zb = bk / (1.0 + bk);

  //   // Initial x fractions
  //   precision_type xa = exp(+Xrap) * mX / rts;
  //   precision_type xb = exp(-Xrap) * mX / rts;    
    
  //   precision_type xap = (1.0 + ak) * xa;
  //   precision_type xbp = (1.0 + bk) * xb;
    
  //   // This does not work when β/=0
  //   double muPDF  = lnv / (1.0 + beta);
  //   PDF pdfsxa  = (*hoppet_runner)(to_double(xa),  muPDF);
  //   PDF pdfsxb  = (*hoppet_runner)(to_double(xb),  muPDF);
  //   PDF pdfsxap = (*hoppet_runner)(to_double(xap), muPDF);
  //   PDF pdfsxbp = (*hoppet_runner)(to_double(xbp), muPDF);

  //   double flava = 0, flavb = 0;
  //   precision_type splita = 0.0, splitb = 0.0;

  //   if(flavap == -flavbp) { // q-> gq emission
  //     flava = flavap;
  //     flavb = flavbp;
  //     splita = qcd().splitting_isr_zomzPq2qg(za); // Comment suggests this is z(1-z)Pq2gq
  //     splitb = qcd().splitting_isr_zomzPq2qg(zb); 
  //   }
  //   else if(flavap == 21) { // Means ga -> qq
  //     flava = - flavbp;
  //     flavb =   flavbp;
  //     splita = qcd().splitting_isr_zomzPg2qqbar(za); 
  //     splitb = 0.0; //qcd().splitting_isr_zomzPq2qg(zb); 
  //   }
  //   else if(flavbp == 21) { // Means gb -> qq
  //     flava =   flavap;
  //     flavb = - flavap;
  //     splitb = qcd().splitting_isr_zomzPg2qqbar(zb); 
  //     splita = 0.0 ;//qcd().splitting_isr_zomzPq2qg(za);
  //   }
  //   else{
  //     cout << "Error in flavour assigment " << flavap << " " << flavbp << endl;
  //   }
  //   // The luminosities are x*f(x)
  //   double lumi_apa = pdfsxap.flav(flavap) / pdfsxa.flav(flava);
  //   double lumi_bpb = pdfsxbp.flav(flavbp) / pdfsxb.flav(flavb);

  //   double weight = to_double(alphas / M_PI  * (f_fcn(lnb) * splita * lumi_apa + f_fcn(-lnb) * splitb * lumi_bpb));
  //   return weight;
  // }

  /// PanGlobal one emission ME as computed in 2022-07-25-pp-matching
  // This weight needs to be multiplied by dlnv dlnb
  // The assumption is that this ME is the same as the Zj above modulo 
  // a different splitting function and factor of two for two identical 
  // dipoles in the initial state.
  // double ShowerPanScaleGlobalpp::pp2H_one_emission_effective_ME(const Event event, HoppetRunner *hoppet_runner) const{

  //   assert(event.size() == 4 && "This is the shower ME for Hj, which requires exactly 4 particles");
  //   assert((_rescaling_option == PanGlobalRescaleOption::GlobalRescalingDeprecated) && "Check matching for this rescaling option!");

  //   double beta = _beta;  
  //   precision_type rts  = event.root_s(); 

  //   double Xrap       = to_double(event[2].momentum().rap());
  //   precision_type mX = event[2].momentum().m(); 
  //   double etaj       = to_double(event[3].momentum().rap());
  //   precision_type pt = event[3].momentum().pt();


  //   // Now convert to shower variables according to eq. 31
  //   precision_type ak = pt * exp( etaj - Xrap) / mX;
  //   precision_type bk = pt * exp(-etaj + Xrap) / mX;
    
  //   double lnb  = 0.5 * to_double(log(ak/bk)); // Inverting eqs. 31+32
  //   double lnkt = 0.5 * to_double(log(ak*bk));
  //   double lnv = lnkt - beta * abs(lnb) + to_double(log(mX));
    
  //   int flavap = event[0].pdgid();
  //   int flavbp = event[1].pdgid();
    
  //   double alphas = qcd().alphasMSbar(lnkt);

  //   precision_type za = ak / (1.0 + ak);
  //   precision_type zb = bk / (1.0 + bk);

  //   // Initial x fractions
  //   precision_type xa = exp(+Xrap) * mX / rts;
  //   precision_type xb = exp(-Xrap) * mX / rts;    
    
  //   precision_type xap = (1.0 + ak) * xa;
  //   precision_type xbp = (1.0 + bk) * xb;
    
  //   // This does not work when β/=0
  //   double muPDF  = lnv / (1.0 + beta);
  //   PDF pdfsxa  = (*hoppet_runner)(to_double(xa ), muPDF);
  //   PDF pdfsxb  = (*hoppet_runner)(to_double(xb ), muPDF);
  //   PDF pdfsxap = (*hoppet_runner)(to_double(xap), muPDF);
  //   PDF pdfsxbp = (*hoppet_runner)(to_double(xbp), muPDF);

  //   double flava = 0, flavb = 0;
  //   precision_type splita = 0.0, splitb = 0.0;

  //   // Underlying Born is always gg
  //   flava = 21;
  //   flavb = 21;

  //   if(flavap == flavbp) { // g-> gg emission
  //     splita = 2.0 * qcd().splitting_isr_zomzhalfPg2gg(za); 
  //     splitb = 2.0 * qcd().splitting_isr_zomzhalfPg2gg(zb); 
  //   } else if(flavap != 21) { // Means qa -> qg
  //     splita = 2.0 * qcd().splitting_isr_zomzPq2gq(za); 
  //     splitb = 0.0; //qcd().splitting_isr_zomzPq2qg(zb); 
  //   } else if(flavbp != 21) { // Means gb -> qq
  //     splitb = 2.0 * qcd().splitting_isr_zomzPq2gq(zb); 
  //     splita = 0.0; //qcd().splitting_isr_zomzPq2qg(za);
  //   } else{
  //     cout << "Error in flavour assigment " << flavap << " " << flavbp << endl;
  //   }
    
  //   // The luminosities are x*f(x)
  //   double lumi_apa = pdfsxap.flav(flavap) / pdfsxa.flav(flava);
  //   double lumi_bpb = pdfsxbp.flav(flavbp) / pdfsxb.flav(flavb);

  //   double weight = to_double(alphas / M_PI  * (f_fcn(lnb) * splita * lumi_apa + f_fcn(-lnb) * splitb * lumi_bpb));
  //   return weight;
  // }


} // namespace panscales
