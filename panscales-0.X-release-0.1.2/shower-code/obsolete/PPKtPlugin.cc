// fastjet stuff
#include "Type.hh"
#include "fjcore_local.hh"
#include "NNFJN2Plain-local.hh"
#include "PPKtPlugin.hh"

#include "Momentum.hh"

// other stuff
#include <iomanip>
#include <vector>
#include <sstream>
#include <limits>


using namespace std;
using namespace panscales;

FJCORE_BEGIN_NAMESPACE      // defined in fastjet/internal/base.hh


//----------------------------------------------------------------------
/// class to help run a pp exclusive kt algorithm
class PPKtBriefJet {
public:
  PPKtBriefJet(){}
  
  void init(const PseudoJet & jet){
    // From the point of view of calculating the angular distances
    // we just need a massless unit-length vector 
    precision_type modp2 = jet.modp2();
    pt2 = jet.pt2();
    //E2 = jet.E()*jet.E();
    if (modp2 > 0) {
      precision_type norm = 1.0/sqrt(modp2);
      precision_type nx = jet.px() * norm;
      precision_type ny = jet.py() * norm;
      precision_type nz = jet.pz() * norm;
      direction = Momentum(nx, ny, nz, 1, 0);
    } else {
      // occasionally (e.g. at end of clustering), we get a momentum
      // that has zero modp2. In that case, choose an arbitrary
      // direction (here, z): in cases where this happens it should
      // either not make any difference (end of clustering), or the
      // bevaviour of the algorithm was anyway arbitrary.
      direction = Momentum(0.0, 0.0, 1.0, 1, 0);
    }
  }

  // Here we are calculating dij
  precision_type distance(const PPKtBriefJet * jet) const {
    precision_type deltaRij2 = geometrical_distance(jet);
    return min(pt2, jet->pt2) * deltaRij2;
  }

  // Here we are calculating delta R_ij^2
  precision_type geometrical_distance(const PPKtBriefJet * jet) const {
    precision_type yi    = direction.rap();
    precision_type yj    = jet->direction.rap();
    precision_type phi_i = direction.phi();
    precision_type phi_j = jet->direction.phi();
    precision_type dphi  = phi_i-phi_j;
    dphi = min(dphi, 2*M_PI-dphi);
    return pow2(yi-yj) + pow2(dphi);
  }

  precision_type momentum_factor() const {
    return pt2;
  }

  precision_type beam_distance() const {
    return pt2;
  }
  
  precision_type geometrical_beam_distance() const {
    // here anything larger than 4 would do
    return 5.0;
  }

protected:
  Momentum direction;
  precision_type pt2;
};

//----------------------------------------------------------------------
string PPKtPlugin::description () const {
  ostringstream desc;
  desc << "pp Kt algorithm plugin";

  return desc.str();
}

//----------------------------------------------------------------------
// clustering
//----------------------------------------------------------------------

template<class NNType> void PPKtPlugin::_actual_run_clustering(ClusterSequence & cs) const {

  int njets = cs.jets().size();

  NNType nn(cs.jets());

  while (njets > 0) {
    int i, j, k;
    precision_type dij = nn.dij_min(i, j);
    if (j >= 0) {
      cs.plugin_record_ij_recombination(i, j, dij, k);
      nn.merge_jets(i, j, cs.jets()[k], k);
    } else {
      precision_type diB = cs.jets()[i].pt2(); // get new diB
      cs.plugin_record_iB_recombination(i, diB);
      nn.remove_jet(i);
    }
    njets--;
  }

}

//----------------------------------------------------------------------
void PPKtPlugin::run_clustering(ClusterSequence & cs) const {

  switch(_strategy) {
  case strategy_NNH:
    _actual_run_clustering<NNH<PPKtBriefJet> >(cs);
    break;
  case strategy_NNFJN2Plain:
    _actual_run_clustering<NNFJN2Plain<PPKtBriefJet> >(cs);
    break;
  default:
    throw Error("Unrecognized strategy in PPKtPlugin");
  }
}



FJCORE_END_NAMESPACE      // defined in fastjet/internal/base.hh
