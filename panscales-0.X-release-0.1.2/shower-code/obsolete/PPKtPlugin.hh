#ifndef __PPKTPLUGIN_HH__
#define __PPKTPLUGIN_HH__

/// this is an implementation of the pp-kt algorithm which uses
/// PanScales 4-momenta so as to perform a precise calculation of
/// (1-cos(theta))

#include "Type.hh"
#include "fjcore_local.hh"

FJCORE_BEGIN_NAMESPACE      // defined in fastjet/internal/base.hh

// forward declaration to reduce includes
class ClusterSequence;

//----------------------------------------------------------------------
/// \class PPKtPlugin
/// Implementation of the pp Kt algorithm
///
/// Note: with the current version of fjcore, only the NNH strategy is
/// available. Code for the other strategies is (temporarily?)
/// commented out.
class PPKtPlugin : public JetDefinition::Plugin {
public:  
  /// Main constructor for the ppKT Plugin class.
    /// enum that contains the two clustering strategy options; for
  /// higher multiplicities, strategy_NNFJN2Plain is about a factor of
  /// two faster.
  enum Strategy { strategy_NNH = 0, strategy_NNFJN2Plain = 1};
  
  /// Main constructor for the ppKT Plugin class.
  PPKtPlugin (Strategy strategy = strategy_NNFJN2Plain) : _strategy(strategy) {}
  
  /// copy constructor
  PPKtPlugin (const PPKtPlugin & plugin) {
    *this = plugin;
  }

  // the things that are required by base class
  virtual std::string description () const;
  virtual void run_clustering(ClusterSequence &) const;

  /// the plugin mechanism's standard way of accessing the jet radius.
  /// This must be set to return something sensible, even if R
  /// does not make sense for this algorithm!
  ///
  /// Note; FJ requires R>2 but at the same times set _invR2 to 1 for
  /// nativee clustering and to 1/R2 for plugins. This means thwt all
  /// distances obtained from this plugin would have to be multiplied
  /// by R2 to get the native ehaviour(alternatively, one would have
  /// to multiply ycut by 1/R^2)
  /// 
  virtual precision_type R() const {return 4.0;}

  /// avoid the warning whenever the user requests "exclusive" jets
  /// from the cluster sequence
  virtual bool exclusive_sequence_meaningful() const {return true;}

private:
  template<class N> void _actual_run_clustering(ClusterSequence &) const;
  
  Strategy _strategy;
};

FJCORE_END_NAMESPACE        // defined in fastjet/internal/base.hh

#endif // __PPKtPlugin_HH__

