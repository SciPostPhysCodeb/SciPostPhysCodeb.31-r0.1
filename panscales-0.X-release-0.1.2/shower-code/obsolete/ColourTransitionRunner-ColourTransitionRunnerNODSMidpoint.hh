// This code was part of ColourTransitionRunner.hh
//
// It is an earlier version of the NODS method where we were applying
// a 3-point correction imposing that we had to decide which side of
// the dipole we were closer to (taken as the midpoint). Ultimately,
// this method got replaced by the current NODS approach where a
// unique correction (global to the full dipole) is applied.
//
// We simnply store the code below for posterity.

//------------------------------------------------------------------------
/// \class ColourTransitionRunnerNODSMidpoint
/// class to handle the physics associated with colour transitions
///
/// different strategies are possible (see above)
class ColourTransitionRunnerNODSMidpoint : public ColourTransitionRunnerSegment{
public:
  /// default ctor
  ///
  /// COLOUR_TODO set the default to colour_transition_matrix_alphas2
  ColourTransitionRunnerNODSMidpoint(const QCDinstance &qcd)
    : ColourTransitionRunnerSegment(qcd){}

  /// dummy virtual dtor
  virtual ~ColourTransitionRunnerNODS(){}
  
  /// add a description
  virtual std::string description() const override{
    return "colour factor from transition segments with matrix-element reweighting (midpoint split)";
  }

  /// get the acceptance probability due to colour associated w an emission
  ///
  /// The acceptance probability is meant to be used w accept_lazy().
  /// This fille the "eta" and "transition_index" members of the emission info
  ///
  /// COLOUR_TODO: at the moment, this is coded so that the colour
  /// factor acceptance is applied after having computed the
  /// splitting kinematics. For most of the strategies, this is not needed
  ///
  /// event          reference to the full event
  /// element        refernce to the element that is splitting
  /// emission_info  info about the splitting (non-const because we cache esxtra info)
  virtual double colour_factor_acceptance(const Event & event,
                                          const typename ShowerBase::Element &element,
                                          typename ShowerBase::EmissionInfo &emission_info) override{
    // grab simple info
    const Dipole & dipole = event.dipoles()[element.dipole_index()];
    const ColourTransitionVector & transitions = dipole.colour_transitions;

    // cache eta_approx once and for all
    double eta_approx = element.eta_approx(emission_info.lnv, emission_info.lnb);
    if (!element.emitter_is_quark_end_of_dipole()){
      eta_approx = -eta_approx;
    }
    emission_info.eta_approx = eta_approx;
    emission_info.colour_segment_index = transitions.get_transition_index_above(eta_approx);

    // compute the insertion eta
    //
    // Note that is is forced to stay within the same colour segment
    // so no need for an update of the transition point index
    emission_info.eta_insertion = _compute_insertion_eta(dipole, emission_info);

    // with no transition points, the acceptance is just given by colour factors
    if (transitions.size() == 0){
      bool is_quark = transitions.is_3bar_end_CF();
      emission_info.colour_is_quark = is_quark;
      emission_info.colour_segment_index = 0;
      return is_quark ? _two_CF_over_CA : 1.0;
    }

    // for a g->qqbar splitting, always return 1
    if (emission_info.radiation_abs_pdgid != 21){
      emission_info.colour_is_quark = false;
      return 1.0;
    }

    // COLOUR_TODO: we may want to get rid of
    // emission_info.colour_is_quark which is not really used for
    // anything else than debugging purpose
    bool is_quark = transitions.is_quark_below_transition(emission_info.colour_segment_index);
    emission_info.colour_is_quark = is_quark;

    // for a "q-g-qbar" vertex (q->a, g->b, qbar->c), the
    // reweighting factor, symmetric in a and c, for an emission k
    // is given by
    //
    //   W(abc) = 1 - (1-2CF/CA) (k|ac)/[(k|ab)+ (k|bc)]
    //
    // This can be rewritten as
    //
    //   W(abc) = 1 - (1-2CF/CA) (1-cos(ac))(1-cos(bk))/[(1-cos(ab))(1-cos(ck)) + (1-cos(bc))(1-cos(ak))]
    //          = 1 - (1-2CF/CA) d_ac d_bk/[d_ab d_ck + d_bc d_ak]
    //
    // w d_ij = 2 (1-cos(ij)) = |i-j|^2
    //
    // We first detemine a, b and c knowing that
    //
    //   x=3bar, y=3, z=aux, k=emission
    //
    // Since up to a qqbar flip we can either have
    //
    //   a=x, b=y, c=z     or    a=y, b=x, c=z
    //
    // the method below returns true if the order flips x and y
    // (2nd option) or false in the case of the 1st option
    Momentum3<precision_type> auxiliary_dirdiff;
    bool is_auxiliary_wrt_3bar=true;
    bool do_flip = _do_arguments_of_W_need_flip(transitions,
                                                eta_approx,
                                                emission_info.colour_segment_index,
                                                auxiliary_dirdiff, is_auxiliary_wrt_3bar);

    // then we compute the distance (d_ij=2*(1-cos(theta_ij))
    //  {d_yz, d_zx, d_xy, d_xk, d_yk, d_zk}
    // x=3bar, y=3, z=aux, k=emission
    Momentum px = element.emitter().momentum();
    Momentum py = element.spectator().momentum();
    if (element.emitter_is_quark_end_of_dipole()){
      std::swap(px, py);
    }

    // We actually need to compute all the differences
    //   dxy, dxz, dyz
    //   dxk, dyk, dzk
    precision_type dxy=0.0, dxz=0.0, dyz=0.0, dxk=0.0, dyk=0.0, dzk=0.0;

    // convention: vec_ab = b-a
    Momentum3<precision_type> vec_xy, vec_xk, vec_yk;
      
    // We proceed as folows:
    //   - dxy, dxk, dyk can be obtained from the internal dipole kinematics
    //   - the auxiliary info includes dxz or dyz
    //   - the remining two are obtained from combinations
    if (element.use_diffs()){
      vec_xy = emission_info.cached_dirdiff_3_minus_3bar;
      vec_xk = emission_info.d_radiation_wrt_3bar();
      vec_yk = emission_info.d_radiation_wrt_3   ();
    } else {
      const Momentum & pk = emission_info.radiation;
      vec_xy = py.direction_diff(px);
      vec_xk = pk.direction_diff(px);
      vec_yk = pk.direction_diff(py);
    }
    
    dxy = vec_xy.modpsq();
    dxk = vec_xk.modpsq();
    dyk = vec_yk.modpsq();
    
    if (is_auxiliary_wrt_3bar){
      // z-x = aux
      // z-y = (z-x) - (y-x)
      // k-z = (k-x) - (z-x)
      dxz = auxiliary_dirdiff.modpsq();
      dyz = (auxiliary_dirdiff - vec_xy).modpsq();
      dzk = (vec_xk-auxiliary_dirdiff).modpsq();
    } else {
      // z-y = aux
      // z-x = (z-y) + (y-x)
      // k-z = (k-y) - (z-y)
      dyz = auxiliary_dirdiff.modpsq();
      dxz = (auxiliary_dirdiff + vec_xy).modpsq();
      dzk = (vec_yk-auxiliary_dirdiff).modpsq();            
    }
    
    // and we map them onto
    //  {d_bc, d_ca, d_ab, d_ak, d_bk, d_ck}
    if (do_flip){
      // flip x and y
      std::swap(dxz, dyz);
      std::swap(dxk, dyk);
    }
    // now we can use a=x, b=y, c=z
    //
    // COLOUR_TODO: decide what to do in the case where the
    // denominator is 0. I'd naively flip back to the "segment"
    // prediction (since we cannot really do better than what the
    // collinear limit gives)
    if (dxy*dzk+dyz*dxk<=0){
      _ColourTransitionRunnerNODSWeightDivisionByZero.warn("ColourTransitionRunnerNODS::colour_factor_acceptance: zero denominator in acceptance weight. Using (segment) colour factor only.");
      return is_quark ? _two_CF_over_CA : 1.0;
    }
    return 1.0 - (1.0-_two_CF_over_CA)*to_double((dxz*dyk)/(dxy*dzk+dyz*dxk));
  }

  //--------------------------------------------------
  /// get the acceptance probability due to colour associated w an
  /// emission when one compute the Sudakov associated w the first
  /// emission.
  ///
  /// Since the "first-emisison Sudakov" calculation does not
  /// compute the full kinematics, this decides the colour factor
  /// based on eta_approx.
  ///
  /// A warning is issued if another strategy is used and there are
  /// more than 2 particles in the initial event.
  ///
  /// See colour_factor_acceptance for more details
  virtual double colour_factor_acceptance_first_sudakov(const Event & event,
                                                        const typename ShowerBase::Element &element,
                                                        typename ShowerBase::EmissionInfo &emission_info) override{
    if (event.size()>2){
      _ColourTransitionMEas2RunnerFirstSudakov.warn("ColourTransitionRunnerNODS::colour_factor_acceptance_first_sudakov will use the colour_transition_approx_eta strategy to compute the first-emission Sudakov");
    }

    return ColourTransitionRunnerSegment::colour_factor_acceptance_first_sudakov(event, element, emission_info);
  }

protected:
  //--------------------------------------------------
  /// handle calculation of distances for auxiliary momentum in the case of a gluon emission from a quark line
  virtual void _create_transitions_gluon_emission_from_quark(typename ShowerBase::EmissionInfo & emission_info,
                                                             double eta_left, double eta_right,
                                                             ColourTransitionPoint & transition_left,
                                                             ColourTransitionPoint & transition_right) const override{
    // store the size of the discontinuity:
    // 
    // for +ve eta_transition, we have
    //   eta_left  = eta_transition   with (half) discontinuity 0
    //   eta_right = 0                with (half) discontinuity eta_transition
    // 
    // for -ve eta_transition, we have
    //   eta_left  = 0                with (half) discontinuity -eta_transition
    //   eta_right = eta_transition   with (half) discontinuity 0
    //
    // in both cases, this gives
    //   half_disc_left  = -eta_right
    //   half_disc_right =  eta_left
    double half_disc_left  = -eta_right;
    double half_disc_right =  eta_left;
    
    // info about auxiliaries
    Momentum3<precision_type> diff_3bar_minus_3, diff_3bar_minus_new, diff_3_minus_new;

    // See ColourTransitionRunnerSegment for the details on how to
    // reorganise the transition vectors.
    //
    // For the left  dipole, the inserted transition point auxiliary variable is "3"
    // For the right dipole, the inserted transition point auxiliary variable is "3bar"
    //
    // Auxiliaries: [currently left==new, right==existing]
    //  - emitter at the 3 end
    //     left_dipole  = (spectator,radiation)  w auxiliary  emitter
    //     right_dipole = (radiation,emitter)    w auxiliary  spectator
    //  - emitter at the 3bar end
    //     left_dipole  = (emitter,radiation)    w auxiliary  spectator
    //     right_dipole = (radiation,spectator)  w auxiliary  emitter
    if (emission_info.use_diffs){
      // TODO: - make sure that I understood properly Gavin's code
      //       - do we want to avoid the repetition of tests?
      diff_3bar_minus_3   = emission_info.d_3bar_end() - emission_info.cached_dirdiff_3_minus_3bar - emission_info.d_3_end();
      diff_3bar_minus_new = emission_info.d_3bar_end() - emission_info.d_radiation_wrt_3bar();
      diff_3_minus_new    = emission_info.d_3_end()    - emission_info.d_radiation_wrt_3();
    } else {
      const Momentum & pout_3bar = emission_info.momentum_3bar_out();
      const Momentum & pout_3    = emission_info.momentum_3_out();
    
      diff_3bar_minus_3   = pout_3bar.direction_diff(pout_3);
      diff_3bar_minus_new = pout_3bar.direction_diff(emission_info.radiation);
      diff_3_minus_new    = pout_3   .direction_diff(emission_info.radiation);  
    }

    transition_left  = ColourTransitionPoint(eta_left,   half_disc_left, -diff_3bar_minus_3,   diff_3_minus_new );
    transition_right = ColourTransitionPoint(eta_right,  half_disc_right, diff_3bar_minus_new, diff_3bar_minus_3);
  }

  /// handle calculation of distances for auxiliary momentum in the
  /// case of a gluon->qqbar splitting at the quark end of the
  /// dipole
  ///
  /// auxiliary for the existing dipole
  virtual ColourTransitionPoint _create_transition_existing_gluon2qqbar_3_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                              double abseta) const override{
    
    // the existing qbar-g dipole becomes qbar-q' with auxiliary qbar'
    // The distances are then  aux-qbar = radiation - (dip 3bar_out)
    //                    and  aux-q'   = radiation - (dip 3_out)
    Momentum3<precision_type> diff_newqbar_minus_oldqbar, diff_newqbar_minus_newq;
    if (emission_info.use_diffs){
      diff_newqbar_minus_oldqbar = emission_info.d_radiation_wrt_3bar() - emission_info.d_3bar_end();
      diff_newqbar_minus_newq    = emission_info.d_radiation_wrt_3()    - emission_info.d_3_end();
    } else {
      const Momentum & pout_3bar = emission_info.momentum_3bar_out();
      const Momentum & pout_3    = emission_info.momentum_3_out();
      diff_newqbar_minus_oldqbar = emission_info.radiation.direction_diff(pout_3bar);
      diff_newqbar_minus_newq    = emission_info.radiation.direction_diff(pout_3);
    }

    // Note: no discontinuity here
    return ColourTransitionPoint(abseta, 0.0, diff_newqbar_minus_oldqbar, diff_newqbar_minus_newq);
  }
  
  ///
  /// auxiliary for the previous dipole
  virtual ColourTransitionPoint _create_transition_previous_gluon2qqbar_3_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                              double minus_abseta,
                                                                              const Dipole & previous_dipole) const override{
    // the previous g-q dipole becomes qbar'-q with auxiliary q'
    //
    // This situation is a bit more complex because we need to
    // access the 3-3bar difference in the previous dipole.
    //
    // Wo dirdiffs, we just compute things locally
    //
    // W dirdiffs, things we use the fact that each dipole stores
    // its 3-3bar direction difference. A critical point is that
    // this distance is updated t othe "out" momenta _before_ the
    // call to this function
    // 
    // The distances are then  aux-qbar'  = (existing dip 3_out) - radiation
    //                    and  aux-prev q = (aux-qbar') + (qbar'-prev_q)
    //                                    = (aux-qbar') - (prev 3-3bar)
    Momentum3<precision_type> diff_newq_minus_newqbar, diff_newq_minus_prevq;
    if (emission_info.use_diffs){
      diff_newq_minus_newqbar = emission_info.d_3_end() - emission_info.d_radiation_wrt_3();
      diff_newq_minus_prevq   = diff_newq_minus_newqbar - previous_dipole.dirdiff_3_minus_3bar;
    } else {
      const Momentum & prev_q = (*(previous_dipole.event()))[previous_dipole.index_q];
      const Momentum & pout_3 = emission_info.momentum_3_out();
      diff_newq_minus_newqbar = pout_3.direction_diff(emission_info.radiation);
      diff_newq_minus_prevq   = pout_3.direction_diff(prev_q);
    }
    
    // Note: no discontinuity here
    return ColourTransitionPoint(minus_abseta, 0.0, diff_newq_minus_newqbar, diff_newq_minus_prevq);
  }

  /// handle the creation of transition points w optional calculation
  /// of the auxiliary variable in the case of a gluon->qqbar
  /// splitting at the gluon anti-quark end
  ///
  /// auxiliary for the existing dipole
  virtual ColourTransitionPoint _create_transition_existing_gluon2qqbar_3bar_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                                 double minus_abseta) const override{
    // the existing g-q dipole becomes qbar'-q with auxiliary q'
    // The distances are then  aux-qbar' = radiation - (dip 3bar_out)
    //                    and  aux-q     = radiation - (dip 3_out)
    Momentum3<precision_type> diff_newq_minus_newqbar, diff_newq_minus_oldq;
    if (emission_info.use_diffs){
      diff_newq_minus_newqbar = emission_info.d_radiation_wrt_3bar() - emission_info.d_3bar_end();
      diff_newq_minus_oldq    = emission_info.d_radiation_wrt_3()    - emission_info.d_3_end();
    } else {
      const Momentum & pout_3bar = emission_info.momentum_3bar_out();
      const Momentum & pout_3    = emission_info.momentum_3_out();
      diff_newq_minus_newqbar = emission_info.radiation.direction_diff(pout_3bar);
      diff_newq_minus_oldq    = emission_info.radiation.direction_diff(pout_3);
    }
    
    // Note: no discontinuity here
    return ColourTransitionPoint(minus_abseta, 0.0, diff_newq_minus_newqbar, diff_newq_minus_oldq);
  }    
  ///
  /// auxiliary for the next dipole
  virtual ColourTransitionPoint _create_transition_next_gluon2qqbar_3bar_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                             double abseta,
                                                                             const Dipole & next_dipole) const override{
    // the next qbar-g dipole becomes qbar-q' with auxiliary qbar'
    //
    // This situation is a bit more complex because we need to
    // access the 3-3bar difference in the next dipole.
    //
    // Wo dirdiffs, we just compute things locally
    //
    // W dirdiffs, we use the fact that each dipole stores
    // its 3-3bar direction difference. A critical point is that
    // this distance is updated t othe "out" momenta _before_ the
    // call to this function
    // 
    // The distances are then  aux-q'        = (existing dip 3bar_out) - radiation
    //                    and  aux-next qbar = (aux-q') + (q'-next_qbar)
    //                                       = (aux-q') + (next 3-3bar)
    Momentum3<precision_type> diff_newqbar_minus_newq, diff_newqbar_minus_nextqbar;
    if (emission_info.use_diffs){
      diff_newqbar_minus_newq     = emission_info.d_3bar_end() - emission_info.d_radiation_wrt_3bar();
      diff_newqbar_minus_nextqbar = diff_newqbar_minus_newq + next_dipole.dirdiff_3_minus_3bar;
    } else {
      const Momentum & next_qbar = (*next_dipole.event())[next_dipole.index_qbar];
      const Momentum & pout_3bar = emission_info.momentum_3bar_out();
      diff_newqbar_minus_newq     = pout_3bar.direction_diff(emission_info.radiation);
      diff_newqbar_minus_nextqbar = pout_3bar.direction_diff(next_qbar);
    }

    // Note: no discontinuity here
    return ColourTransitionPoint(abseta, 0.0, diff_newqbar_minus_nextqbar, diff_newqbar_minus_newq);
  }    

  
private:

  /// determine the abc arguments of the reweighting W_{abc}
  ///
  /// the list of transisiotn points. emission eta and transition
  /// index are passed as arguments
  ///
  /// Assuming we have a dipole with
  ///   x at the 3bar end
  ///   y at the 3 end
  ///   z as auxilliary
  /// if this returns true we should use
  ///   a=y, b=x, c=z
  /// otherwise use
  ///   a=x, b=y, c=z
  /// (see details below)
  ///
  /// The "auxiliary_dirdiff" and "is_auxiliary_3bar" are filled
  /// with the 3-vector pointing from either the 3bar end
  /// (is_auiliary_3bar=true) or 3 end (is_auxiliary_3bar=false) end
  /// of the element
  ///
  /// outdated: The "auxiliary_index" argument is filled to contain the
  /// outdated: correct auxiliary index (on the correc 3bar or 3 side of the
  /// outdated: colour segment)
  ///
  /// Note that this makes use of the q-qbar symmetry Wabc=Wcba
  bool _do_arguments_of_W_need_flip(const ColourTransitionVector & transitions,
                                    double eta_new,
                                    unsigned int transition_index,
                                    Momentum3<precision_type> &auxiliary_dirdiff,
                                    bool &is_axiliary_3bar) const{
    //unsigned int &auxiliary_index) const{
    
    // We first detemine a, b and c. This depends to which end of
    // the colour segment we arecloser to. Say we have a dipole
    // (x,y) (w x at the 3bar end) and an auxiliary z.
    // 
    //  1. we're on a CA/2 segment closer to the 3bar end
    //       W(a=x, b=y, c=z)
    //  2. we're on a CA/2 segment closer to the 3 end
    //       W(a=z, b=x, c=y)
    //  3. we're on a CF segment closer to the 3bar end
    //       W(a=z, b=x, c=y)
    //  4. we're on a CF segment closer to the 3 end
    //       W(a=x, b=y, c=z)
    //
    // cases 1&4 and 2&3 are the same. We also need to take care
    // of "edge" cases where we are on a semi-infinite segment
    // (the case with no transition points has already been
    // handled):
    //  5. index=0

    // first determine if we're on a CA/2 of CF segment
    bool is_quark_segment = transitions.is_quark_below_transition(transition_index);

    // the "flip" situation is exactly the opposite between CF and
    // CA/2 segments. So no need for an overall "if"
    //
    // so we directly check for the segment end we're attached to
    if (eta_new < transitions.get_midpoint_below_transition(transition_index)){
      // covers both the +inf side anf the left half of all central segments
      //auxiliary_index = transitions[n_transitions-1].auxiliary();
      const ColourTransitionPoint & transition_point = transitions[transition_index-1];
      auxiliary_dirdiff = transition_point.dirdiff();
      is_axiliary_3bar  = transition_point.is_dirdiff_wrt_3bar();
      return is_quark_segment;
    }
    // covers both the -inf side anf the right half of all central segments
    // upper (3) end
    //auxiliary_index = transitions[transition_index].auxiliary();
    const ColourTransitionPoint & transition_point = transitions[transition_index];
    auxiliary_dirdiff = transition_point.dirdiff();
    is_axiliary_3bar  = transition_point.is_dirdiff_wrt_3bar();
    return !is_quark_segment;      
  }

};


// The corresponding augmented ColourTransitionPoint, including the rapidity of the midpoint, is as follows
  class ColourTransitionPoint{
  public:
    /// ctor with full initialisation
    ///   eta               rapidity of the transition point (wrt emitter, in event frame)
    ColourTransitionPoint(double eta_in=0.0)
      : _eta(eta_in),
        //_eta_half_disc(0.0),
        _dirdiff(Momentum3<precision_type>()), _is_dirdiff_wrt_3bar(true) {}
   
    /// ctor with full initialisation
    ///   eta               rapidity of the transition point (wrt emitter, in event frame)
    ///   dirdiff           distance from the auxiliary to either the 3 or 3bar end of the associated dipole
    ///                     (i.e. aux-3bar or aux-3)
    ///   is_dirdiff_wrt_3bar  true if distance if to the 3bar and, false if to 3
    ColourTransitionPoint(double eta_in,
                          const Momentum3<precision_type> &dirdiff_in,
                          bool is_dirdiff_wrt_3bar_in)
      : _eta(eta_in),
        //_eta_half_disc(0.0),
        _dirdiff(dirdiff_in), _is_dirdiff_wrt_3bar(is_dirdiff_wrt_3bar_in) {}

    // /// ctor with initialisation from 2 distances.
    // /// The smallest is taken as a reference
    // ///
    // ///   eta               rapidity of the transition point (wrt emitter, in event frame)
    // ///   eta_half_disc     (half) the discontinuity associated w the rapidity being measured
    // ///                     wrt either the 3 or 3bar end of a dipole
    // ///   dirdiff_wrt_3bar  distance from the 3bar end of the associated dipole (i.e. aux-3bar)
    // ///   dirdiff_wrt_3     distance from the 3    end of the associated dipole (i.e. aux-3   )
    // ColourTransitionPoint(double eta_in,
    //                       double eta_half_disc,
    //                       const Momentum3<precision_type> &dirdiff_wrt_3bar,
    //                       const Momentum3<precision_type> &dirdiff_wrt_3)                          
    //   : _eta(eta_in), _eta_half_disc(eta_half_disc){
    //   /// only store the smallest of the 2 distances
    //   if (dirdiff_wrt_3bar.modpsq() < dirdiff_wrt_3.modpsq()){
    //     _dirdiff = dirdiff_wrt_3bar;
    //     _is_dirdiff_wrt_3bar = true;
    //   } else {
    //     _dirdiff = dirdiff_wrt_3;
    //     _is_dirdiff_wrt_3bar = false;
    //   }
    // }

    /// access info
    const double eta() const { return _eta; }
    //const double eta_half_disc() const { return _eta_half_disc; }
    const Momentum3<precision_type> & dirdiff() const{ return _dirdiff; }
    bool is_dirdiff_wrt_3bar() const {return _is_dirdiff_wrt_3bar; }

    /// set info
    void set_eta(double eta_in){ _eta = eta_in; }
    //void set_eta_half_disc(double eta_half_disc_in){ _eta_half_disc = eta_half_disc_in; }

  private:
    double _eta;           ///< rapidity of the transition point in the event frame
    //double _eta_half_disc; ///< (half) the discontinuity between emissions
    //                       ///  from the 3 and 3bar ends of the dipole

    /// dir difference wrt either the 3bar or the 3 end of the dipole
    Momentum3<precision_type> _dirdiff;

    /// true if closer end is the 3bar
    bool _is_dirdiff_wrt_3bar;
  };


  class ColourTransitionVector{
    //...
    
    // /// get the midpoint of the segment below the given transition
    // /// index i.e. of segmen [i-1,i]
    // double get_midpoint_below_transition(unsigned int index) const{
    //   // segment at the -inf end
    //   if (index == 0)
    //     return - std::numeric_limits<double>::max();
    // 
    //   // segment at the +inf end
    //   if (index == _transition_points.size())
    //     return   std::numeric_limits<double>::max();
    // 
    //   // segment in the midle
    //   double etaL = _transition_points[index-1].eta() + _transition_points[index-1].eta_half_disc();
    //   double etaR = _transition_points[index  ].eta() - _transition_points[index  ].eta_half_disc();
    //   
    //   //uncomment the lines below to get an idea of good v. wrong orderings
    //   // note that the _olourTransitionRunnerNODSWringMidpointOrdering warnings need to be declared
    //   // not done now so the code will not compile
    //   // if (etaL>etaR)
    //   //   _ColourTransitionRunnerNODSWrongMidpointOrdering.warn("ColourTransitionRunnerNODS: inverted ordering in values used for the midpoint calculation");
    //   // else
    //   //   _ColourTransitionRunnerNODSMidpointOrdering.warn("ColourTransitionRunnerNODS: correct ordering in values used for the midpoint calculation");
    //     
    //   return 0.5*(etaL+etaR);
    // }

    //...
  };
