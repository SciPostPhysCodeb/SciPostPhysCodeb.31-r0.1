//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __ShowerPanScaleGlobalDIS_HH__
#define __ShowerPanScaleGlobalDIS_HH__

#include "ShowerPanScaleDISBase.hh"

namespace panscales{

//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerPanScaleGlobalDIS
/// the PanScales shower w global recoil for DIS and VBF introduced in 
/// arxiV:2305.08645. Note that VBF is treated as two DIS chains. 

class ShowerPanScaleGlobalDIS : public ShowerPanScaleBase {
public:
  /// default ctor
  ShowerPanScaleGlobalDIS(const QCDinstance & qcd, double beta = 0.0, 
                       PanGlobalRescaleOption rescaling_option = PanGlobalRescaleOption::LocalRescaling)
    : ShowerPanScaleBase(qcd, beta), _rescaling_option(rescaling_option) {
    }
  
  /// virtual dtor
  virtual ~ShowerPanScaleGlobalDIS() {}

  /// implements the element and splitting info as subclasses
  class Element;    //< base class
  class ElementIF;  //< handles an element w I emitter and F spectator
  class ElementFF;  //< handles an element w F emitter and F spectator
  class EmissionInfo;
  virtual ShowerBase::EmissionInfo* create_emission_info() const override;

  /// description of the shower
  std::string description() const override {
    std::ostringstream ostr;
    ostr << name() << " shower with beta = " << _beta;
    ostr << ", rescaling according to scheme: " << _rescaling_option;
    return ostr.str();
  }
  /// name of the shower
  std::string name() const override{return "PanScaleGlobal-DIS";}

  /// returns true if the shower is global 
  bool is_global() const override { return true; }

  /// this is an antenna shower (1 element per dipole)
  virtual unsigned int n_elements_per_dipole() const override{ return 1;}

  /// element store
  virtual std::vector<std::unique_ptr<ShowerBase::Element> > elements(Event & event, int dipole_index) const override;

  // do_kinematics can return false, it is a DIS shower
  virtual bool do_kinematics_always_true_if_accept() const override {return true;} 

  /// this is an antenna-like shower, so both the emitter and spectator
  /// can split
  bool only_emitter_splits() const override {return false;}
  
  /// rescaling option
  PanGlobalRescaleOption rescale_option() const {return _rescaling_option;}
  private:
  PanGlobalRescaleOption _rescaling_option;

};


//--------------------------------------------------------------
/// \class ShowerPanScaleGlobalDIS::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs for the PanScaleGlobalDIS shower 
class ShowerPanScaleGlobalDIS::Element : public ShowerPanScaleBase::Element {

public:
  /// dummy ctor
  Element() {}

  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleGlobalDIS * shower) :
    ShowerPanScaleBase::Element(emitter_index, spectator_index, dipole_index, event, shower),  _shower(shower) {}

  /// given that we have virtual functions in a number of places,
  /// we need a virtual destructor to avoid compiler warnings
  virtual ~Element() {}

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------
  /// Approximate calculation of the log of the kT of the emission relative
  /// to its parents in their rest frame, (eq. 57 of logbook 2019-07-02).
  const double alphas_lnkt(double lnv, double lnb) const override {
    precision_type lnkt = log_T(_rho) + lnv + _shower->_beta*fabs(lnb);
    return to_double(lnkt);
  }

  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  /// the lnb total extent, as a function of lnv, is parametrised
  /// as  const + lnv_deriv * lnv.
  virtual double lnb_extent_const() const override = 0;

  double lnb_extent_lnv_coeff() const override {return -2.0/(1. + _shower->_beta);}
  
  Range lnb_generation_range(double lnv) const override = 0;
  
  bool has_exact_lnb_range() const override {return false;}
  /// jacobian for this shower is not computed
  precision_type dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const override {
    throw std::runtime_error("Tried to access the exact jacobian an element of shower " + _shower->name() + ", which is not implemented");
    return 0.;
  }
  /// this function should not be accessed by any function
  void get_emit_spec_rad_kinematics_for_matching(typename ShowerBase::EmissionInfo * emission_info, Momentum & emit, Momentum & spec, Momentum & rad, bool include_boost = false) const override{
    throw std::runtime_error("Tried to access kinematics for matching of the shower " + _shower->name() + ", for which matching is not implemented");
  }


  /// returns the exact range of lnb that is allowed; returns a a
  /// zero-extent range in cases where there in no range of lnb
  /// allowed. (Note that the zero extent range may take various
  /// forms, e.g. two identical points, or inverted points)
  Range lnb_exact_range(double lnv) const override;

  virtual double lnv_lnb_max_density() const override {
    return _shower->max_alphas() * _shower->qcd().CA() / M_PI;
  }
  
  //--------------------------------------------------
  // important definitions to be implemented in all derived classed
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const override = 0;

  /// carry out the splitting and update the event; return true if
  /// successful; the RotatedPieces pass on a potentially rotated version
  /// of the emitter, spectator, and perpendicular vectors, and these
  /// are the ones that should be used, not those from emission_info; 
  /// the output is stored back in emission_info (possibly still rotated)
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const override = 0;
  
  /// Overridden update event function to apply boost and rescaling
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

protected:
  // computes the Sudakov components of the final state
  // and returns the rescaling factor for the initial state
  void get_sudakov_and_rescaling(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const;
  // boost particles in DIS chain
  void boost_all_in_chain(typename ShowerBase::EmissionInfo * emission_info_base, int dis_chain) const;

  const ShowerPanScaleGlobalDIS * _shower;
};

//--------------------------------------------------------------
// different specific elements for PanScaleGlobalDIS
//--------------------------------------------------------------
/// \class ShowerPanScaleGlobalDIS::ElementIF
/// implementation of the elements for IF splittings  
class ShowerPanScaleGlobalDIS::ElementIF : public ShowerPanScaleGlobalDIS::Element {
public:
  /// full ctor w initialisation
  ElementIF(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleGlobalDIS * shower) :
    ShowerPanScaleGlobalDIS::Element(emitter_index, spectator_index, dipole_index, event, shower){}

  //--------------------------------------------------
  // generation ranges 
  //--------------------------------------------------
  virtual double lnb_extent_const() const override;
  
  virtual Range lnb_generation_range(double lnv) const override;

  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// carry out the splitting and update the event
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;
  
  /// do the final boost
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

};

//--------------------------------------------------------------
/// \class ShowerPanScaleGlobalDIS::ElementFF
/// implementation of the elements for FF splittings   
class ShowerPanScaleGlobalDIS::ElementFF : public ShowerPanScaleGlobalDIS::Element {
public:
  /// full ctor w initialisation
  ElementFF(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleGlobalDIS * shower) :
    ShowerPanScaleGlobalDIS::Element(emitter_index, spectator_index, dipole_index, event, shower){}

  virtual double lnb_extent_const() const override;

  /// exact range available for the FF case
  virtual Range lnb_generation_range(double lnv) const override;

  /// return the acceptance probability for the given kinematic point
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// carry out the splitting and update the event
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;

  /// do the final boost
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

};

//--------------------------------------------------------------
/// \class ShowerPanScaleGlobalDIS::EmissionInfo
/// emission information specific to the PanGlobal DIS shower
class ShowerPanScaleGlobalDIS::EmissionInfo : public ShowerPanScaleBase::EmissionInfo {
public:
  Momentum4<precision_type> pf, kperp_f;
  // needed for the boost
  precision_type af, bf, r;
  // needed for the mappings
  precision_type alphak, betak, kappat;
};

} // namespace panscales

#endif // __ShowerPanScaleGlobalDIS_HH__
