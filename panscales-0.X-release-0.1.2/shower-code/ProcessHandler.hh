//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef __PROCESSHANLDLER_HH__
#define __PROCESSHANLDLER_HH__

#include <string>
#include "Event.hh"
#include "CmdLine.hh"
#include "Momentum.hh"
//#include "ShowerRunner.hh"
#include "MatchingHardMatrixElements.hh"

namespace panscales{

/// An integer identifying the underlying hard process.
///
enum class ProcID {
  /// e+e- -> γ -> q qbar
  ee2qq, 
  /// e+e- -> h -> g g 
  ee2gg, 
  /// pp -> Z
  pp2Z, 
  /// pp -> H
  pp2H, 
  /// pp -> X j (X is specified in the class itself)
  pp2Xj, 
  /// pp -> j j 
  pp2jj, 
  /// Deep inelastic scattering
  DIS, 
  /// pp -> H j j (vector boson scattering in factorised DIS approach)
  VBF
};

//----------------------------------------------------------------------
/// \class ProcessBase
/// base class for the hard scattering process. The current list of
/// implemented processes is (see enum):
///   - e+e- : Z->q qbar, H->gg
///   - pp : pp -> Z, pp-> H, pp-> H/Z + jet
///   - q gamma* -> q q (DIS)
///   - q q -> H q q (VBF)
class ProcessBase {
  public:
  /// \brief  dummy constructor to allow this class to be an abstract member
  ProcessBase() {}

  /// \brief default constructor for the base process class, which sets the 
  /// collider CM energy rts
  ProcessBase(double rts) {
   _sqrts = rts;
   // as default, set the hard scale at rts
   _hard_scale = _sqrts;
  }

  /// 
  virtual ~ProcessBase() {}

  /// \brief gives a brief description of the process
  /// \return string
  virtual std::string description() const = 0;

  /// \brief if true, we do not generate a new event every time a new shower run is initalised,
  ///        if false, a new born event is generated / read from a LHE file
  /// \return true false
  bool use_fixed_born_event() const { return _use_fixed_born_event;}

  /// \brief generates the next event for the process at hand
  ///        if the underlying born is with fixed kinematics
  ///        we create the event just once and after that return
  ///        the stored event. 
  /// \return event
  virtual panscales::Event generate_event() = 0;

  /// \brief sets up the hard matching matrix element when available
  /// \param qcd 
  virtual void set_hard_matching_me(const QCDinstance & qcd){
   throw std::runtime_error("hard matching matrix element is not implemented for this process");
  }

  /// \brief access to a pointer for the hard matrix element
  panscales::MatchingHardMatrixElement * hard_me(){ return _hard_me.get();}

  double hard_scale(){
   return _hard_scale;
  };

  protected:
  double _sqrts;              ///< collider CM energy
  double _hard_scale;         ///< scale at which to start the shower
  bool _use_fixed_born_event; ///< if true, we use a fixed born event in generate_event of AnalysisFramework
  panscales::Event _event;    ///< stores the fixed / current event
  std::unique_ptr<MatchingHardMatrixElement> _hard_me; ///< gives access to the hard matrix element
};

//----------------------------------------------------------------------
/// \class ProcessZ2qq
/// derived class for  e+e- -> Z -> qqbar
class ProcessZ2qq : public ProcessBase{
public:
  ProcessZ2qq(CmdLine * cmdline, double rts);

  std::string description() const {
    std::string desc = "e+e- -> Z -> qqbar (flavour = +/-"+std::to_string(_born_flav)+") with sqrt(s)="+std::to_string(_sqrts)+", skew_angle="+std::to_string(_skew_angle);
    if (_has_skew_eta) desc += ", skew_eta="+std::to_string(_skew_eta);
    else desc += ", skew_eta=n/a";
    desc += ", random_generation="+std::to_string(_random_axis);
    return desc;
  }

  panscales::Event generate_event();
   
  void set_hard_matching_me(const QCDinstance & qcd) {
    _hard_me.reset(new ZqqHardME(qcd, _random_axis));
  }

private:
  bool _event_is_initialised = false; ///< if false, we need to generate the fixed underlying born event
  int _born_flav;                     ///< flavour of the born partons q qbar
  double _skew_angle;                 ///< work with an angle between the two outgoing partons if nonzero
  bool _has_skew_eta;                 ///< true when the -skew-qq-photon-eta flag is present on the command line
  double _skew_eta;                   ///< work with the (rapidity of the) angle between the two outgoing partons if nonzero
  bool _random_axis, _fixed_random_axis, _x_axis; ///< options for different orientations of the event
}; // end class ProcessZ2qq

//----------------------------------------------------------------------
/// \class ProcessH2gg
/// derived class for  e+e- -> H -> gg
class ProcessH2gg : public ProcessBase{
   public:
   ProcessH2gg(CmdLine * cmdline, double rts);

   std::string description() const {return "e+e- -> H -> gg with sqrt(s)="+std::to_string(_sqrts);}

   panscales::Event generate_event();
   
   void set_hard_matching_me(const QCDinstance & qcd){
    _hard_me.reset(new HggHardME(qcd, _random_axis));
   }

   private:
   bool _event_is_initialised = false; ///< if false, we need to generate the fixed underlying born event
   bool _random_axis;                  ///< options for different orientations of the event
}; // end class ProcessH2gg

//----------------------------------------------------------------------
/// \class Processpp2Z
/// derived class for the pp -> Z process
class Processpp2Z : public ProcessBase{
   public:
   Processpp2Z(CmdLine * cmdline, double rts);

   std::string description() const {return "pp -> Z (born flavour = +/-"+std::to_string(_born_flav)+") with hadronic sqrt(s)="+std::to_string(_sqrts)+", mZ="+std::to_string(_mZ)+",yZ="+std::to_string(_yrap);}

   panscales::Event generate_event();

   double mZ() const {return _mZ;}
   
   private:
   bool _event_is_initialised = false; ///< if false, we need to generate the fixed underlying born event
   double _mZ;                         ///< mass of Z/gamma* boson
   double _yrap;                       ///< rapidity of the Z/gamma* boson
   int _born_flav;                     ///< flavour of the born partons q qbar
}; // end class Processpp2Z

//----------------------------------------------------------------------
/// \class Processpp2H
/// derived class for the pp -> H process
class Processpp2H : public ProcessBase{
   public:
   Processpp2H(CmdLine * cmdline, double rts);

   std::string description() const {return "pp -> H with hadronic sqrt(s)="+std::to_string(_sqrts)+", mH="+std::to_string(_mH)+",yH="+std::to_string(_yrap);}

   panscales::Event generate_event();
   
   private:
   bool _event_is_initialised = false; ///< if false, we need to generate a fixed underlying born event
   double _mH;                         ///< mass of the Higgs boson
   double _yrap;                       ///< rapidity of the Z/gamma* boson
}; // end class Processpp2H

//----------------------------------------------------------------------
/// \class Processpp2Xj
/// derived class for the pp -> H/Z + jet process 

class Processpp2Xj : public ProcessBase{
   public:

   /// structure to hold the type of process
   enum ProcessType {
      pp2Zj, ///< pp -> Z + j
      pp2Hj, ///< pp -> H + j
   };

   Processpp2Xj(CmdLine * cmdline, double rts);

   std::string description() const {
    if(_process_type == ProcessType::pp2Hj){
        return "pp -> Hj with hadronic sqrt(s)="+std::to_string(_sqrts)+", mH="+std::to_string(_mX)+", yH="+std::to_string(_yX)+", pTj="+std::to_string(_pTj)+", yJ="+std::to_string(_yJ);
    } else{
        return "pp -> Zj (flavour="+std::to_string(_incoming_flavour)+"), hadronic sqrt(s)="+std::to_string(_sqrts)+", mZ="+std::to_string(_mX)+", yZ="+std::to_string(_yX)+", pTj="+std::to_string(_pTj)+", yJ="+std::to_string(_yJ);
    }
   }

   panscales::Event generate_event();
   
   private:
   ProcessType _process_type;          ///< H+jet or Z+jet
   bool _event_is_initialised = false; ///< if false, we need to generate a fixed underlying born event
   double _mX;                         ///< mass of the Z/Higgs boson
   double _yX;                         ///< rapidity of the Z/Higgs
   double _yJ;                         ///< rapidity of the jet
   double _pTj;                        ///< transverse momentum of the jet
   double _phi;                        ///< phi direction of the jet
   int _jet_flavour, _incoming_flavour;///< pdgids of the jet and incoming partons
   bool _jet_is_hard;                  ///< make the born jet part of the hard system
#include "autogen/auto_ProcessHandler_Processpp2Xj.hh"
}; // end class Processpp2Xj

//----------------------------------------------------------------------
/// \class Processpp2jj
/// derived class for the pp -> dijet process
class Processpp2jj : public ProcessBase{
   public:
   Processpp2jj(CmdLine * cmdline, double rts);

   std::string description() const {
    return "pp -> jj (flavours="+std::to_string(_incoming_flavour_parton_1)+", "+std::to_string(_incoming_flavour_parton_2)+"), hadronic sqrt(s)="+std::to_string(_sqrts)+", yR="+std::to_string(_yR)+", pTj="+std::to_string(_pTj)+", yJ="+std::to_string(_yJ);
   }

   panscales::Event generate_event();

   private:
   bool _event_is_initialised = false; ///< if false, we need to generate a fixed underlying born event
   double _yJ;                         ///< rapidity of the jet
   double _yR;                         ///< rapidity of the recoiling jet
   double _pTj;                        ///< transverse momentum of the jet
   double _phi;                        ///< phi direction of the jet
   int _incoming_flavour_parton_1, _incoming_flavour_parton_2;///< pdgids of the incoming partons
   bool _jet_is_hard;                  ///< make the born jet part of the hard system
}; // end class Processpp2jj

//----------------------------------------------------------------------
/// \class ProcessDIS
/// derived class for the q gamma* -> q channel in deep-inelastic scattering
class ProcessDIS : public ProcessBase{
   public:
   ProcessDIS(CmdLine * cmdline, double rts);

   std::string description() const {
      return "q gamma* -> q with Q^2="+std::to_string(_Q2_dis)
               +", y="+std::to_string(_y) 
               +", rts=" + std::to_string(_sqrts);}

   panscales::Event generate_event();

   // access to Q^2
   double Q2_dis() const {return _Q2_dis;}

   private:
   bool _event_is_initialised = false; ///< if false, we need to generate a fixed underlying born event
   double _Q2_dis;                     ///< -Mass^2 of the photon, invariant mass of qq system
   double _y;                          ///< (1-cth)/2 between the outgoing and incoming quark in the Breit frame
   double _x_dis;                      ///< Q2_DIS/pow2(f_rts)/y_dis
}; // end class ProcessDIS

//----------------------------------------------------------------------
/// \class ProcessVBF
/// derived class for the q q -> H q q, i.e. vector-boson fusion in pp 
class ProcessVBF : public ProcessBase{
   public:
   ProcessVBF(CmdLine * cmdline, double rts);

   std::string description() const {return "q q -> H q q (flavour =)"+std::to_string(_in_flav1)+", "+std::to_string(_in_flav2)+", invariant mass="+std::to_string(_Q)+", rapidity="+std::to_string(_ytot);}

   panscales::Event generate_event();
   
   private:
   bool _event_is_initialised = false; ///< if false, we need to generate a fixed underlying born event
   double _Q;                          ///< invariant mass of the Higgs+dijet system
   double _ytot;                       ///< rapidity of the higgs+ditjet system
   int _in_flav1, _in_flav2;           ///< flavours of the partons
   double _mH, _yH, _pTH;              ///< mass, rapidity and transverse momentum of the Higgs boson
}; // end class ProcessVBF


} // namespace
#include "autogen/auto_ProcessHandler_global-hh.hh"
#endif //__PROCESS_HH_
