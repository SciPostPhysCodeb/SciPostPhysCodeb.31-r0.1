//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "MatchingHardMatrixElements.hh"

using namespace std;

namespace panscales{

  /// @brief computes the hard matrix element for e+e- -> h -> gg
  ///        and fills the splitting weights vector
  /// @param event 
  /// @param element 
  /// @param emission_info 
  /// @param hoppet_runner 
  /// @param emit 
  /// @param spec 
  /// @param rad 
  void HggHardME::compute_hard_me(const Event & event, const ShowerBase::Element & element,
      const ShowerBase::EmissionInfo & emission_info, const HoppetRunner * hoppet_runner,
      const Momentum & emit, const Momentum & spec, const Momentum & rad){
    // we need to remember to reset all the splitting weights to 0
    reset_splitting_weights();
    
    precision_type emit_dot_rad  = emission_info.emit_dot_rad;
    precision_type spec_dot_rad  = emission_info.spec_dot_rad;
    precision_type emit_dot_spec = emission_info.emit_dot_spec;

    // ggg final state
    //      
    // P_ggg_v?_esr = P_ggg^v?(e,s,r) ; {e,s,r} = {g,g,g}
    precision_type P_ggg_v1_esr = event.Q2()*emit_dot_spec/emit_dot_rad/(emit_dot_rad+spec_dot_rad)
                                + 2*emit_dot_spec*(emit_dot_rad+spec_dot_rad)/emit_dot_rad/event.Q2()
                                + 1.;

    precision_type P_ggg_v2_esr = (event.Q2()/(2.*emit_dot_rad))
                                * (event.Q2()/(2.*emit_dot_rad+2.*spec_dot_rad))
                                * (1+pow3(2.*emit_dot_spec/event.Q2()));

    // P_ggg_v?_esr = P_ggg^v?(s,e,r) ; {s,e,r} = {g,g,g}
    precision_type P_ggg_v1_ser = event.Q2()*emit_dot_spec/spec_dot_rad/(spec_dot_rad+emit_dot_rad)
                                + 2*emit_dot_spec*(spec_dot_rad+emit_dot_rad)/spec_dot_rad/event.Q2()
                                + 1.;

    precision_type P_ggg_v2_ser = (event.Q2()/(2.*spec_dot_rad))
                                * (event.Q2()/(2.*spec_dot_rad+2.*emit_dot_rad))
                                * (1+pow3(2.*emit_dot_spec/event.Q2()));

    
    // Weighted sum of h->ggg components:
    _splitting_weights[QCDinstance::SplittingChannel::fs_g_esr_to_gg] = _w_gg_splitting * P_ggg_v1_esr + (1-_w_gg_splitting) * P_ggg_v2_esr;
    _splitting_weights[QCDinstance::SplittingChannel::fs_g_ser_to_gg] = _w_gg_splitting * P_ggg_v1_ser + (1-_w_gg_splitting) * P_ggg_v2_ser;


    // gqq final state
    //
    // If g->qqbar is disabled in QCD, do not compute the gqqbar
    // contributions (set to 0 in "reset")
    if (! _qcd.gluon_uses_qqbar()) return;
                                     
    // Common prefactor for all P_gqq_vW_XYZ components
    precision_type P_gqq_factor = 2 * _qcd.nf()*_qcd.TR() / _qcd.CA();

    // P_gqq_v?_esr = P_gqq^v?(e,s,r) ; {e,s,r} = {q,g,qbar}
    precision_type P_gqq_v1_esr = P_gqq_factor
                                * (pow2(2*emit_dot_spec) + pow2(2*spec_dot_rad))
                                / 2 / event.Q2() / (2*emit_dot_rad);

    precision_type P_gqq_v2_esr = P_gqq_factor
                                * pow2(2.*emit_dot_spec)/event.Q2()/(2.*emit_dot_rad);

    // P_gqq_v?_esr = P_gqq^v?(s,e,r) ; {s,e,r} = {g,q,qbar}
    precision_type P_gqq_v1_ser = P_gqq_factor
                                * (pow2(2*emit_dot_spec) + pow2(2*emit_dot_rad))
                                / 2 / event.Q2() / (2*spec_dot_rad);
                               
    precision_type P_gqq_v2_ser = P_gqq_factor
                                * pow2(2.*emit_dot_spec)/event.Q2()/(2.*spec_dot_rad);

    // Weighted sum of h->gqq components:
    _splitting_weights[QCDinstance::SplittingChannel::fs_g_esr_to_qq] = _w_qq_splitting * P_gqq_v1_esr + (1-_w_qq_splitting) * P_gqq_v2_esr;
    _splitting_weights[QCDinstance::SplittingChannel::fs_g_ser_to_qq] = _w_qq_splitting * P_gqq_v1_ser + (1-_w_qq_splitting) * P_gqq_v2_ser;
  }



  // computes the hard matrix element for e+e- -> Z -> qq
  // and fills the splitting weights vector
  void ZqqHardME::compute_hard_me(const Event & event, const ShowerBase::Element & element,
      const ShowerBase::EmissionInfo & emission_info, const HoppetRunner * hoppet_runner,
      const Momentum & emit, const Momentum & spec, const Momentum & rad){
    // we need to remember to reset all the splitting weights to 0
    reset_splitting_weights();
      
    // retrieve the dot products already computed in emission info
    precision_type emit_dot_rad  = emission_info.emit_dot_rad;
    precision_type spec_dot_rad  = emission_info.spec_dot_rad;
    precision_type emit_dot_spec = emission_info.emit_dot_spec;

    // compute additional dot products if needed
    precision_type iQ2_plus_jQ2;  
    // With Euler angles (i.e. sampling over the full Born polar angle)
    if (_random_axis) {
      const Momentum & p_eminus    = event.beam1();
      const Momentum & p_eplus     = event.beam2();
      // compute the remaining dot products
      precision_type emin_dot_emit = dot_product(p_eminus, emit);
      precision_type epls_dot_emit = dot_product(p_eplus , emit);
      precision_type emin_dot_spec = dot_product(p_eminus, spec);
      precision_type epls_dot_spec = dot_product(p_eplus , spec);
      iQ2_plus_jQ2 = (epls_dot_emit*epls_dot_emit + emin_dot_emit*emin_dot_emit
                    + epls_dot_spec*epls_dot_spec + emin_dot_spec*emin_dot_spec) / event.born_me();
    } else {
      iQ2_plus_jQ2 = pow2(emit_dot_rad + emit_dot_spec) + pow2(emit_dot_spec + spec_dot_rad);
    }
  
    // compute the separate pieces
    _splitting_weights[QCDinstance::SplittingChannel::fs_q_esr_to_qg] =  2 * _qcd.CF() / _qcd.CA() * iQ2_plus_jQ2 / (emit_dot_rad + spec_dot_rad) / emit_dot_rad;
    _splitting_weights[QCDinstance::SplittingChannel::fs_q_ser_to_qg] =  2 * _qcd.CF() / _qcd.CA() * iQ2_plus_jQ2 / (emit_dot_rad + spec_dot_rad) / spec_dot_rad;
  }
}
