// 
// dgauss.hh
// 
//  Created on: Jul 27, 2008
//      Author: salam
// 

#ifndef __DGAUSS_HH_
#define __DGAUSS_HH_

#include <cmath>
#include <vector>
#include <stdexcept>

// storage for the Gaussian integration weights
extern const std::vector<double> wgts1024, xx1024;
extern const std::vector<double> wgts512 , xx512;
extern const std::vector<double> wgts256 , xx256;
extern const std::vector<double> wgts128 , xx128;
extern const std::vector<double> wgts64  , xx64;
extern const std::vector<double> wgts32  , xx32;
extern const std::vector<double> wgts16  , xx16;
extern const std::vector<double> wgts8   , xx8 ;

//----------------------------------------------------------------------
/// class that gets thrown on precision errors from dgauss
class dgaussPrecisionError {
public:
  dgaussPrecisionError(double xlo_in, double xhi_in)
    : xlo(xlo_in), xhi(xhi_in) {}
  double xlo, xhi;
};


//----------------------------------------------------------------------
/// Integrator that is essentially CERNLIB's dgauss, but written
/// recursively.
///
/// It returns \int_xlo^xhi fn(x)
///
/// The "fn" can either be a normal function or alternatively
/// a class with a defined operator()
///
/// On positive-definite integrands usually returns a result
/// whose absolute accuracy is max(eps, eps*abs(result)).
///
/// If it cannot return a result to the requested accuracy, it throws a
/// dgaussPrecisionError.
///
/// It is dirty, but simple, and usually quite effective.
///
//----------------------------------------------------------------------
template<class F>
double dgauss(double xlo, double xhi, const F& fn, double eps,
              unsigned degree_lo = 8, unsigned degree_hi = 16) {

  if (degree_hi<=degree_lo)
    throw std::domain_error("dgauss.hh: the low degree of GQ integration must be lower than the requested high degree.");

  const std::vector<double> *xx_lo   = &xx8;
  const std::vector<double> *wgts_lo = &wgts8;
  switch(degree_lo) {
    case    8:
      xx_lo =    &xx8 ; wgts_lo =    &wgts8; break;
    case   16:
      xx_lo =   &xx16 ; wgts_lo =   &wgts16; break;
    case   32:
      xx_lo =   &xx32 ; wgts_lo =   &wgts32; break;
    case   64:
      xx_lo =   &xx64 ; wgts_lo =   &wgts64; break;
    case  128:
      xx_lo =  &xx128 ; wgts_lo =  &wgts128; break;
    case  256:
      xx_lo =  &xx256 ; wgts_lo =  &wgts256; break;
    case  512:
      xx_lo =  &xx512 ; wgts_lo =  &wgts512; break;
    case 1024:
      xx_lo = &xx1024 ; wgts_lo = &wgts1024; break;
    default:
      throw std::domain_error("dgauss.hh: unrecognized lower degree of GQ integration (should be 8, 16, 32, 64, 128, 256, 512 or 1024).");
  }

  const std::vector<double> *xx_hi   = &xx16;
  const std::vector<double> *wgts_hi = &wgts16;
  switch(degree_hi) {
    case    8:
      xx_hi =    &xx8 ; wgts_hi =    &wgts8; break;
    case   16:
      xx_hi =   &xx16 ; wgts_hi =   &wgts16; break;
    case   32:
      xx_hi =   &xx32 ; wgts_hi =   &wgts32; break;
    case   64:
      xx_hi =   &xx64 ; wgts_hi =   &wgts64; break;
    case  128:
      xx_hi =  &xx128 ; wgts_hi =  &wgts128; break;
    case  256:
      xx_hi =  &xx256 ; wgts_hi =  &wgts256; break;
    case  512:
      xx_hi =  &xx512 ; wgts_hi =  &wgts512; break;
    case 1024:
      xx_hi = &xx1024 ; wgts_hi = &wgts1024; break;
    default:
      throw std::domain_error("dgauss.hh: unrecognized high degree of GQ integration (should be 8, 16, 32, 64, 128, 256, 512 or 1024).");
  }

  double res_hi = 0.0, res_lo=0.0;
  double hx=0.5*(xhi-xlo), xx;
  // do a high-precision integration 
  for (unsigned i=0; i<xx_hi->size(); i++) {
    xx = hx*(*xx_hi)[i];
    res_hi += (fn(xlo+xx) + fn(xhi-xx))*(*wgts_hi)[i];
  }
  res_hi *= hx;
  // do a low-precision one
  for (unsigned i=0; i<xx_lo->size(); i++) {
    xx = hx*(*xx_lo)[i];
    res_lo += (fn(xlo+xx) + fn(xhi-xx))*(*wgts_lo)[i];
  }
  res_lo  *= hx;
  // decide whether precision is good enough
  if (std::abs(res_lo-res_hi) <= (1+std::abs(res_hi))*eps) {
    return res_hi;
  } else {
    if (1.0+std::abs(hx) == 1.0) throw dgaussPrecisionError(xlo,xhi);
    double xmid = xlo+hx;
    return dgauss(xlo,xmid,fn,eps) + dgauss(xmid,xhi,fn,eps);
  }
}

#endif /* __DGAUSS_HH_ */
