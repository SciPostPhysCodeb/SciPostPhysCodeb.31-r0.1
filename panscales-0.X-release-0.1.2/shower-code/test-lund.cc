//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "AnalysisFramework.hh"
#include "LundEEGenerator.hh"
#include "FastY3.hh"
#include <iomanip>

using namespace std;
using namespace panscales;


class ExampleFramework : public  AnalysisFramework {
public:
  bool cam_use_WTA, cam_use_diffs; 
  bool print_lund_coords = true;
  FastY3 fasty3;
  fjcore::contrib::RecursiveLundEEGenerator lund_generator;

  
  ExampleFramework(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in) {}

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {

    lund_generator = fjcore::contrib::RecursiveLundEEGenerator(-1);
    
    cam_use_WTA = cmdline->present("-cam-use-WTA").help("use WTA in Cambridge jet clustering");
    cam_use_diffs = cmdline->present("-cam-use-diffs").help("use direction differences in Cambridge jet clustering");
    // other consistency conditions required if using direction
    // differences within the Cambridge algorithm.
    if (cam_use_diffs) assert(cmdline->present("-use-diffs") && cam_use_WTA);
    fasty3 = FastY3(cam_use_WTA, cam_use_diffs);

    print_lund_coords = !cmdline->present("-no-print-lund-coords").help("switch off printing of Lund coordinates");

    // #TRK_ISSUE-712  some default binning range -- we will think more about this later...
    this->set_default_binning(0.0, abs(f_lnvmin*2), 0.5);
  }

  /// dd_real and qd_real return NaN when the argument of the
  /// logarithm is zero; that differs from the behaviour in double
  /// precision (which returns -infinity), so implement that
  /// behaviour here
  precision_type safe_log(precision_type x) const {
    if (x == 0) return -numeric_limit_extras<precision_type>::infinity();
    else        return log(x);
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    //cout << f_event << endl;
    //double evwgt = event_weight();
    fasty3.analyse(f_event);
    auto lund_declusterings = lund_generator.result(fasty3.cs());
    if (print_lund_coords) cout << "iev = " << iev << endl;
    for (unsigned int i = 0; i < lund_declusterings.size(); i++) {
      const auto & ld = lund_declusterings[i];
      hists_err["lund_rap_weight"].add_entry(ld.eta(), event_weight());
      if (!print_lund_coords) continue;
      cout << i << " " << ld.iplane() 
           << " " << ld.eta()
           << " " << ld.lnkt()
           << " " << ld.psibar() 
           << " " << event_weight()
           << endl;
    }
  }
  

//  void test_dirdiff_matrix(const Event & event) {
//    //cout << event << endl;
//    const double cambridge_ycut = 1.0;
//    fjcore::JetDefinition jd1(new fjcore::EECambridgeFastPlugin(cambridge_ycut));
//    jd1.delete_plugin_when_unused();
//    jd1.set_recombination_scheme(fjcore::WTA_modp_scheme);
//    fjcore::ClusterSequence cs1(event.particles(), jd1);
//
//    // set things up for clustering with dirdiff
//    FastY3 fasty3_diffs(true, true); 
//    fasty3_diffs.analyse(event);
//    const fjcore::ClusterSequence & cs2 = fasty3_diffs.cs();
//
//    for (unsigned i = 1; i < event.particles().size(); i++) {
//      double y1 = cs1.exclusive_ymerge(i);
//      double y2 = cs2.exclusive_ymerge(i);
//      
//      cout << setw(3)  << i << " " 
//           << setw(15) << y1 
//           << setw(15) << y2;
//      if (abs(y1-y2) > 1e-10 * max(y1,y2)) cout << "  DIFF=" << y1-y2;
//      cout << endl;
//    }
//    //event.print_following_dipoles();
//    //for (unsigned i = 0; i < event.size(); i++) {
//    //  for (unsigned j = 0; j < event.size(); j++) { 
//    //    double d1 = pairwise(i,j);
//    //    double d2 = dot_product(event[i],event[j]) / event[i].E() / event[j].E();
//    //    cout << i << " " << j << " " 
//    //         << setw(12) << d1 << " " 
//    //         << setw(12) << d2;
//    //    if (abs(d1 - d2) > 1e-10) cout << " BAD";
//    //    cout << endl;
//    //  }
//    //}
//  }

};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv,true);
  ExampleFramework driver(&cmdline);
  driver.run();
}
