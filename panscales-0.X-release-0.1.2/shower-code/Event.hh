//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __EVENT_HH__
#define __EVENT_HH__

#include <memory>
#include <vector>
#include <array>
#include <limits>
#include "Momentum.hh"
#include "ColourTransitions.hh"

namespace panscales{

//----------------------------------------------------------------------
/// @ingroup momentum_classes
/// \class Particle
/// class that supplements a momentum with whatever additional basic 
/// information will be needed in an event record
class Particle: public Momentum {
public:
  Particle() : Momentum(0,0,0,0), _pdgid(0), _barcode(0), _initial_state(0), _hard_system(false),
               _radiated_lnkt(std::numeric_limits<double>::infinity())
  {}

  Particle(const Momentum & p, int id = 0, unsigned int initial_state = 0, bool hard_system = false)
    : Momentum(p), _pdgid(id), _initial_state(initial_state), _hard_system(hard_system),
      _radiated_lnkt(std::numeric_limits<double>::infinity())
  {}

  /// read access to the pdgid
  int pdgid() const {return _pdgid;}

  /// read access to the barcode
  int barcode() const {return _barcode;}

  /// return true if the particle is initial_state
  bool is_initial_state() const {return _initial_state;}

  /// return true if the particle is a parton
  bool is_parton() const {return (abs(_pdgid)<6 || _pdgid == 21);}

  /// checks whether particle is a (anti-)quark (no tops...)
  bool is_quark_or_antiquark() const {return (abs(_pdgid) < 6);}

  /// checks whether particle is a gluon
  bool is_gluon()              const {return (_pdgid == 21);}

  /// return the "initial-state status" of the particle: 
  /// - 0 for final-state
  /// - 1 for initial-state from beam1 
  /// - 2 for initial-state from beam2
  unsigned int initial_state() const {return _initial_state;}

  /// return true if the particle is in the hard_system
  bool is_in_hard_system() const {return _hard_system;}

  /// set the pdgid 
  void set_pdgid(int pdgid_in) {_pdgid = pdgid_in;}

  /// set the barcode 
  void set_barcode(int barcode_in) {_barcode = barcode_in;}

  /// set initial_state (0=final state, 1=beam1, 2=beam2)
  void set_initial_state(unsigned int initial_state_in) {_initial_state = initial_state_in;}

  /// helper to set the hard_system status (0=not in hard system, 1=in hard system)
  void set_hard_system(bool hard_system) {_hard_system = hard_system;}

  /// set/get the radiated lnkt
  void set_radiated_lnkt(double radlnkt) {_radiated_lnkt = radlnkt;}
  double radiated_lnkt() const { return _radiated_lnkt;}

  /// get the DIS chain information (needed for VBF processes)
  // VBF consists of two separate DIS systems
  // this number informs the shower which chain the particle is part of
  // it then automatically selects the _Q and ref vectors
  void set_DIS_chain(int dis_chain) {_dis_chain = dis_chain;}
  int get_DIS_chain() const {return _dis_chain;}
  
  /// read access to the underlying momentum component
  const Momentum & momentum() const {return *this;}

private:
  int _pdgid;
  int _barcode;
  
  /// 0=final state; 1=incoming beam1; 2=incoming beam2
  unsigned int _initial_state;  

  /// false=not in hard system; true=in hard system
  bool _hard_system;

  /// the lnkt at which the particle was emitted (inf for initial partons)
  double _radiated_lnkt;
  
  int _dis_chain = 0;  ///< dis chain 
};

//----------------------------------------------------------------------
/// Outputs a particle
inline std::ostream & operator<<(std::ostream & ostr, const Particle & p) {
  ostr << p.momentum() << " "
       << " ID=" << std::setw(5) << p.pdgid() 
       << " IS=" << std::setw(1) << p.initial_state()
       << " HS=" << std::setw(1) << p.is_in_hard_system()
       << " DIS=" << std::setw(1) << p.get_DIS_chain();
  return ostr;
}

// forward declaration
class Event;

//------------------------------------------------------------------
/// \class Dipole
/// stores the data associated with each dipole
class Dipole {
public: 

  // Dipole(int index_3_in, int index_3bar_in) : 
  /// the indices of the particles at the quark-like and anti-quark-like
  /// ends of this dipole (intended in a large-NC sense)
  int index_q, index_qbar;

  /// returns the index of the particle associated with the
  /// colour-triplet end of this dipole
  int index_3()    const {return index_q   ;}
  /// returns the index of the particle associated with the
  /// colour-anti-triplet end of this dipole
  int index_3bar() const {return index_qbar;}

  /// returns a const reference to the particle at the 3 end of the dipole
  const Particle & particle_3   ()  const;
  /// returns a const reference to the particle at the 3bar end of the dipole
  const Particle & particle_3bar()  const;

  /// If the q end (in large-NC sense) of the dipole belongs to a gluon,
  /// then index_previous corresponds to the other dipole associated
  /// with that gluon. Similarly for qbar end and index_next.
  ///
  /// If the q (qbar) end corresponds to a quark, then index_previous
  /// (index_next) is negative. Negative indices can be of two kinds:
  /// either initial q/qbar (index_{next,previous}=-1), or q/qbar
  /// pairs originating from gluon->qqbar splittings. The information
  /// that a q/qbar pair comes from a gluon->qqbar splitting is kept
  /// under the form of "non_splitting_dipoles" in the event. To
  /// encode the fact that the q (qbar) end of a dipole is associated
  /// with the i-th non-splitting dipole in the event, we set
  /// index_previous (index_next) to -2-i.  Note that since
  /// non-splitting dipoles do not split. their index_{next,previous}
  /// values are positive and point to the "real" dipoles in the event
  ///
  /// To give an example: for a colour-ordered set of gluons (g1...gn)
  /// stretched between a q and a qbar, the q-g1 dipole will have
  /// index_previous=-1, the gn-qbar dipole will have index_next = -1
  /// and all other dipoles will be in a linked list in between.
  int index_previous, index_next;

  /// structure to handle the 1/Nc (CF-CA/2) corrections along a dipole
  ColourTransitionVector colour_transitions;

  /// returns true if the dipole is quark-like (CF) below the given
  /// transition index. Index==0 indicates between eta=-inf and the
  /// first element of colour_transitions;
  /// index==colour_transitions.size() corresponds to the part at
  /// rap=+inf.
  bool is_quark_below_transition(unsigned int index) const{
    return colour_transitions.is_quark_below_transition(index);
  }

  /// the difference in 3-direction between the quark and qbar
  Momentum3<Float> dirdiff_3_minus_3bar;

  /// initialise the difference in direction between the quark
  /// and antiquark, basing the difference on the momenta as
  /// stored in the event associated with this dipole
  void init_dirdiff();

  void set_event(Event * event) {_event = event;}
  Event * event() {return _event;}
  const Event * event() const {return _event;}

  /// returns a pointer to the colour-connected neighbouring dipole at
  /// the colour triplet end of this dipole; if there is no such dipole,
  /// returns nullptr. Beware, since this is a pointer to an element
  /// in a vector of dipoles, if the event changes, then the pointer
  /// may no longer be valid.
  ///
  /// Note: this currently corresponds to the "previous" dipole
  ///
  /// When the (optional) argument is true and the dipole 3 end
  /// connects to a non-splitting dipole, the latter is returned;
  /// otherwise, return a null pointer in this case.
  Dipole * dipole_at_3_end(bool include_non_splitting = false);
  /// const version
  const Dipole * dipole_at_3_end(bool include_non_splitting = false) const;

  /// returns a pointer to the colour-connected neighbouring dipole at
  /// the colour anti-triplet end of this dipole; if there is no such dipole,
  /// returns nullptr
  ///
  /// Note: this currently corresponds to the "next" dipole
  ///
  /// When the (optional) argument is true and the dipole 3 end
  /// connects to a non-splitting dipole, the latter is returned;
  /// otherwise, return a null pointer in this case.
  Dipole * dipole_at_3bar_end(bool include_non_splitting = false);
  /// const version
  const Dipole * dipole_at_3bar_end(bool include_non_splitting = false) const;

private:
  /// a pointer to the event
  Event * _event = nullptr;
};

  
//----------------------------------------------------------------------
/// \class EventData 
///
/// Stores the data associated with the event. This class ensures ease
/// of copying and assignment of that underlying data, allowing the
/// main Event class below to just enforce consistency of things like
/// dipole pointers back to the event, etc. (without manually 
/// having to write out the copy of all information).
class EventData {
public:
  EventData() : Q_(Momentum(0,0,0,0)), _barcode_counter(0),
                _incoming1_idx(0), _incoming2_idx(0),
                _pdf_x_beam1(0.0), _pdf_x_beam2(0.0),
                _Q2_dis_1(0.0), _Q2_dis_2(0.0),
                _born_me(0.0){};
  void copy_event_data(EventData other) {*this = other;}

  class BranchingHistoryElement {
  public:
    BranchingHistoryElement(double alphas_lnkt_in) : alphas_lnkt(alphas_lnkt_in) {}
    /// the value of lnkt used in the alphas calculation at each
    /// branching stage
    double alphas_lnkt = std::numeric_limits<double>::max(); 
  };

protected:

  
  std::vector<Particle> _particles;  ///< particles in the event
  std::vector<Dipole>   _dipoles;    ///< 3-3bar colour dipoles
  std::vector<BranchingHistoryElement> _branching_history; ///< information on the branching history
  Momentum Q_;

  std::vector<unsigned int> _touched_dipole_indices;

  int _barcode_counter = 0;

  /// non-splitting dipoles (associated with g->qqbar)
  ///
  /// Note that in a g->qqbar splitting, the 3 (resp. 3bar) end of the
  /// non-splitting dipole would correspond to the 3bar (resp. 3) end
  /// of the splitting gluon. We adopt this convention so that we can
  /// compute dirdiffs between distant particles by just summing the
  /// dirdiff_3_minus_3bar along a dipole chain.
  std::vector<Dipole> _non_splitting_dipoles; 

  /// the beam particles (not currently set for e+e- events)
  /// and PDF information
  Particle _beam1, _beam2;                      ///< 4-momentum of the beams
  unsigned int _incoming1_idx, _incoming2_idx;  ///< incoming partons
  double _pdf_x_beam1, _pdf_x_beam2;            ///< PDF x-fractions of incoming partons

  /// the particle index of the original Born particles
  /// we start by setting them to 0, 1, and the indices
  /// get updated if the current index splits
  int _born1 = 0;
  int _born2 = 1;

  /// for DIS events (at most two chains)
  Momentum _p_ref_in_1, _p_ref_out_1;
  Momentum _p_ref_in_2, _p_ref_out_2;
  double _Q2_dis_1, _Q2_dis_2;
  Momentum _Q_dis_1, _Q_dis_2;

  /// For matching we need access to the underlying Born ME. 
  /// For qq/gg, where the ME is trivial, the easiest seems to be 
  /// to simply save this value when we generate the underlying event.
  precision_type _born_me;
};

//----------------------------------------------------------------------
/// \class Event
/// stores an event
///
/// This class provides a user's view of a full event.
///
/// Note that all data within the event is actually stored
/// in the base EventData class
///
class Event : public EventData {
public:
  /// default ctor
  Event() {}

  /// ctor from a set of particles, dipoles and a pair of beams
  ///
  /// BEWARE, it does not currently handle dipole creation at the moment
  /// so the event is unusable in practice
  Event(const std::vector<Particle> & p_in, const std::vector<Dipole> & dipoles_in,
        const Particle & beam1_in = Particle(), const Particle & beam2_in = Particle()) {
    _particles = p_in;  
    _dipoles   = dipoles_in;
    _beam1 = beam1_in;
    _beam2 = beam2_in;
    finish_init();
  }


  /// ctor from a set of particles, dipoles, non-splitting dipoles and a pair of beams
  Event(const std::vector<Particle> & p_in, 
        const std::vector<Dipole> & dipoles_in,
        const std::vector<Dipole> & non_splitting_dipoles_in,
        const Particle & beam1_in = Particle(), const Particle & beam2_in = Particle()) {
    _particles = p_in;  
    _dipoles   = dipoles_in;
    _non_splitting_dipoles = non_splitting_dipoles_in;
    _beam1 = beam1_in;
    _beam2 = beam2_in;
    finish_init();
  }


  /// ctor from a set of particles only
  ///
  /// BEWARE, it does not currently handle dipole creation at the moment
  /// so the event is unusable in practice
  Event(const std::vector<Particle> & p_in,
        const Particle & beam1_in = Particle(), const Particle & beam2_in = Particle()) {
    _particles = p_in;  
    Q_     = momentum_sum();
    _beam1 = beam1_in;
    _beam2 = beam2_in;
    finish_init();
  }


  Event(const Event & ev) : EventData(ev) {
    // dipoles point back to the event, so in the copy of
    // the dipoles we need to point back to this event
    set_internal_pointers();
  }

  Event & operator=(const Event ev)  {
    copy_event_data(ev);
    // dipoles point back to the event, so in the copy of
    // the dipoles we need to point back to this event
    set_internal_pointers();
    return *this;
  }

  /// function to be called at the end of each constructor
  /// 1. sets the internal barcode and pointer information
  /// 2. updates the event Q
  /// 3. updates the directional differences in the (ns) dipoles
  void finish_init();
  
  /// returns the sum of the outgoing momenta in the event
  Momentum momentum_sum() const;

  /// returns the sum of the hard system momenta
  Momentum momentum_sum_hard_system() const;

  /// returns the sum of the incoming parton momenta in the event
  /// (beams not currently accounted for)
  Momentum momentum_partons_in() const;

  /// returns sum of the momenta in a DIS chain
  Momentum sum_momentum_out_in_chain(int dis_chain) const;

  /// returns true if the momentum check passes
  bool check_momentum() const;

  /// returns a const reference to the vector of event particles
  const std::vector<Particle> & particles() const {return _particles;}

  /// returns a non const reference to the vector of event particles, use with care
  std::vector<Particle> & particles() {return _particles;}

  /// pp events related information
  ///
  /// the 2 beams
  const Particle & beam1() const {return _beam1;}
  const Particle & beam2() const {return _beam2;}
  /// Get root_s directly from event
  precision_type root_s() const {return _beam1.E() + _beam2.E();} 
  precision_type rts()    const {return _beam1.E() + _beam2.E();} 

  /// reset the beams and Q
  void reset_beam1(const Momentum & p){ _beam1.reset_momentum(p);}
  void reset_beam2(const Momentum & p){ _beam2.reset_momentum(p);}
  void reset_Q(const Momentum p){ Q_ = p;}

  /// the incoming partons
  const Particle & incoming1() const {return _particles[_incoming1_idx];}
  const Particle & incoming2() const {return _particles[_incoming2_idx];}
  unsigned int incoming1_idx() const {return _incoming1_idx;}
  unsigned int incoming2_idx() const {return _incoming2_idx;}

  /// the PDFs x fractions for both beams
  double pdf_x_beam1() const {return _pdf_x_beam1;}
  double pdf_x_beam2() const {return _pdf_x_beam2;}
  void set_pdf_x_beam1(double x) {_pdf_x_beam1=x;}
  void set_pdf_x_beam2(double x) {_pdf_x_beam2=x;}
  
  void set_pdf_beam1(unsigned int incoming1_idx, double x_beam1){
    _incoming1_idx = incoming1_idx;
    _pdf_x_beam1   = x_beam1;
  }
  void set_pdf_beam2(unsigned int incoming2_idx, double x_beam2){
    _incoming2_idx = incoming2_idx;
    _pdf_x_beam2   = x_beam2;
  }

  /// DIS information
  void set_incoming_ref_momenta(Particle p_in, Particle p_out, int chain_number = 1);
  
  /// access to the reference momenta and the Q values
  const Momentum & ref_in (int chain_number = 1) const;
  const Momentum & ref_out(int chain_number = 1) const;
  const double   & Q2_dis (int chain_number = 1) const;
  const Momentum & Q_dis  (int chain_number = 1) const;

  /// Function to boost and realign the whole event. Needed for PanLocalpp
  void boost_and_realign();
  
  /// boost to Breit frame where the ref vectors (in and out) are back-to-back
  /// needed for DIS
  void boost_to_breit_frame();

  /// returns true if this is a pp event
  /// Currently this is based on testing if the first 2 particles are
  /// initial-state
  bool is_pp() const;

  /// returns true if this is a DIS event
  /// one incoming parton and one incoming non-parton
  bool is_DIS() const;
  
  /// returns the (incoming) beam particle that is on the 
  /// opposite side to the incoming particle p supplied
  /// as an argument
  const Particle & opposite_beam(const Particle & p) const;

  /// returns a const reference to particles()[i]
  const Particle & operator[](unsigned i) const {return _particles[i];}

  /// returns a non-const reference to particles()[i], use with care
  Particle & operator[](unsigned i) {return _particles[i];}

  /// returns a const reference to the vector of event dipoles
  const std::vector<Dipole> & dipoles() const {return _dipoles;}

  /// returns a non-const reference to the vector of event dipoles; use with care.
  std::vector<Dipole> & dipoles() {return _dipoles;}

  /// returns a const reference to the vector of non-splitting dipoles
  const std::vector<Dipole> & non_splitting_dipoles() const {return _non_splitting_dipoles;}

  /// returns a non-const reference to the vector of non-splitting dipoles; use with care.
  std::vector<Dipole> & non_splitting_dipoles() {return _non_splitting_dipoles;}

  /// returns the number of particles in the event
  unsigned size() const {return _particles.size();}

  /// returns the next barcode
  int next_barcode() {
    _barcode_counter++;
    return _barcode_counter;
  }

  /// updates the Born indices
  ///
  ///GS-NOTE: this is not collinear safe. Do we care?
  void update_Born_index(const int splitter_index);

  /// get Born indices
  void get_Born_indices(int & born1, int & born2) const {
    born1 = _born1; born2 = _born2;
  }

  /// returns true if the next emission of this event can be matched
  bool can_be_matched() const;

  /// returns the cached centre of mass energy (assumed to be
  /// conserved during event evolution)
  const Momentum & Q() const {return Q_;}

  /// returns the cached squared centre of mass energy (assumed
  /// to be conserved during event evolution)
  precision_type Q2() const {return Q_.m2();}

  /// for initial-state particles, return the PDF fraction along their
  /// beam of origin (choice of beam is based on info inside particle p)
  precision_type pdf_x(const Particle &p) const;

  /// returns a const ref to the branching history vector
  const std::vector<BranchingHistoryElement> & branching_history() const {return _branching_history;}
  
  /// add an entry to the vector of branching history elements
  /// (currently just contains alphas_lnkt)
  void add_branching_history_element(double alphas_lnkt) {_branching_history.push_back({alphas_lnkt});}

  /// adds the Particle to the end of the event record and returns
  /// its index
  unsigned add_particle_return_index(const Particle & m) {
    _particles.push_back(m);
    _particles.back().set_barcode(next_barcode());
    return _particles.size() -1;
  }

  /// adds the dipole to the end of the dipole list and returns its
  /// index
  unsigned add_dipole_return_index(const Dipole & m) {
    _dipoles.push_back(m);
    _dipoles.back().set_event(this);
    return _dipoles.size() -1;
  }

  /// adds the dipole to the end of the non-splitting dipole list and returns its
  /// index
  unsigned add_non_splitting_dipole_return_index(const Dipole & m) {
    _non_splitting_dipoles.push_back(m);
    _non_splitting_dipoles.back().set_event(this);
    return _non_splitting_dipoles.size() -1;
  }

  /// update indices, etc. associated with the new gluon being
  /// inserted into the dipole. The gluon whose index is passed
  /// is assumed to have been just created, and it is also assumed
  /// that the other particles associated with it have also 
  /// had their momenta updated.
  ///
  /// It returns the index of the new dipole
  int insert_gluon_in_dipole_return_index(int igluon, int idipole);
  
  /// Arrange internal indices for case where a gluon at the quark-like
  /// end of a dipole splits to qqbar. 
  ///
  /// - the dipole that is splitting is idipole
  /// - the splitting gluon has its quark end in the idipole dipole,
  ///   its qbar end in the "previous" dipole in the chain
  /// - iqbar is the index of a new particle which will be the
  ///   anti-quark produced in the splitting (goes w previous dipole)
  /// - the q particle from the splitting is assumed to be
  ///   dipoles[idipole].index_q
  /// - we assume that PDG IDs have been correctly set (by the calling
  ///   routine) for the q and qbar
  ///
  /// it returns the index of the "previous" dipole
  int split_gluon2qqbar_at_dipole_q_end(int iqbar, int idipole);
  
  /// Arrange internal indices for case where a gluon at the qbar-like
  /// end of a dipole splits to qqbar. 
  ///
  /// - the dipole that is splitting is idipole
  /// - the splitting gluon has its qbar end in the idipole dipole,
  ///   its quark end in the "next" dipole in the chain
  /// - iq is the index of a new particle which will be the
  ///   quark produced in the splitting (goes w "next" dipole)
  /// - the qbar particle from the splitting is assumed to be
  ///   dipoles[idipole].index_qbar
  /// - we assume that PDG IDs have been correctly set (by the calling
  ///   routine) for the q and qbar
  ///
  /// it returns the index of the "next" dipole
  int split_gluon2qqbar_at_dipole_qbar_end(int iq, int idipole);

  // Arrange internal indices for cases where a quark in the initial
  // state (i.e. at the 3bar end of a dipole) evolves backwards so
  // that the new incoming parton is a gluon, with emission of an
  // anti-quark in the final state, i.e. we have a splitting
  //   g_in -> q_in + qbar_out
  //
  // This does several things:
  //   - the 3bar end of the existing dipole becomes the IS gluon
  //   - a new dipole is created with the IS gluon as its 3 end and
  //     the emitted qbar at its 3bar end
  //   - the existing "next" dipole is set to the newly created dipole
  //   - the new dipole "previous" dipole is the existing one
  //   - if the existing dipole had a (non-splitting) "next" dipole:
  //     . it becomes the "next" of the new dipole
  //     . its "previous" points to the new dipole
  int split_backwards_q2gluon(int iqbar, int idipole);

  // Arrange internal indices for cases where an anti quark in the
  // initial state (i.e. at the 3 end of a dipole) evolves backwards
  // so that the new incoming parton is a gluon, with emission of an
  // quark in the final state, i.e. we have a splitting
  //   g_in -> qbar_in + q_out
  //
  // This does several things:
  //   - the 3 end of the existing dipole becomes the IS gluon
  //   - a new dipole is created with the IS gluon as its 3bar end
  //     and the emitted q at its 3 end
  //   - the existing "previous" dipole is set to the new dipole
  //   - the new dipole "next" dipole is the existing one
  //   - if the existing dipole had a (non-splitting) "previous" dipole:
  //     . it becomes the "previous" of the new dipole
  //     . its "next" points to the new dipole
  int split_backwards_qbar2gluon(int iqbar, int idipole);


  //--------------------------------------------------------------------------------
  // new event update tools

  /// insert a new dipole in the chain
  ///
  /// currently returns the index of the newly-created dipole (+ve
  /// means splitting, -ve means non-splitting)
  int update_dipole_after_emission(int i_new_particle, int idipole,
                                   int splitter_pdgid,
                                   bool splitter_is_3_end,
                                   int radiation_pdgid);
  
  //--------------------------------------------------------------------------------

  /// returns a vector of indices in the _dipoles array corresponding
  /// to those dipoles whose masses are affected by the insertion of 
  /// the last gluon, i.e. in the last call to
  /// insert_gluon_in_dipole_return_index
  /// with the first index in the array corresponding to the location
  /// of the just-created dipole in the dipoles() array above (n.b.
  /// the latter number should equal the size() of the dipoles() array
  /// minus one.
  const std::vector<unsigned int> & touched_dipole_indices() const {return _touched_dipole_indices;}

  //--------------------------------------------------------------------------------
  // tool to swap 2 momenta in the event record (currently used by the
  // double-soft corrections)
  void swap_momenta(unsigned int index1, unsigned int index2){
    auto tmp = _particles[index1].momentum();
    _particles[index1].reset_momentum(_particles[index2].momentum());
    _particles[index2].reset_momentum(tmp);
  }
  

  //--------------------------------------------------------------------------------

  /// print the particles in the event following the dipole chains
  /// for simplicity we start with the q end of the first dipole
  void print_following_dipoles() const;

  /// check that the PDG Ids at the 3 and 3bar ends of the dipoles are
  /// coherent (e.g. no final-state quark at the 3bar end of a dipole)
  bool check_dipoles_pdgids() const;

  /// retrieve/set the acceptance probability associated with the
  /// Born-level (2->2) matrix-element when matching is turned on. Only
  /// needed for ee->Z->qqbar with Euler angles
  precision_type born_me() const {return _born_me;}
  void set_born_me(precision_type born_me) {_born_me = born_me;}
  
protected:

  void set_internal_pointers() {
    for(auto & dipole: dipoles()) {dipole.set_event(this);}
    for(auto & dipole: non_splitting_dipoles()) {dipole.set_event(this);}
  }

  void set_initial_barcodes() {
    _beam1.set_barcode(-1);
    _beam2.set_barcode(-2);
    for(auto & particle: _particles) {particle.set_barcode(next_barcode());}
  }

  // MAKE SURE THAT NO DATA MEMBERS ARE INCLUDED HERE. 
  // THEY SHOULD INSTEAD GO IN THE EventData CLASS
  // (THIS IS FOR EASE OF COPYING AND ASSIGNMENT)
};

/// returns an event with electron beam1 and beam2 colliding, giving a
/// pair of partons p1_out and p2_out, which can be qqbar or gg.
/// The last argument allows to add extra dummy particles (tyically
/// used to pass an additional photon to balance Q2 and create an
/// event w a skewed photon)
Event create_ee2X_event(const Particle & beam1, const Particle & beam2,
                        const Particle & p1_out, const Particle &p2_out,
                        const std::vector<Particle> &additions = std::vector<Particle>());

/// returns an event with beam1 and beam2 colliding, giving
/// a X boson through the collision of partons p1_in and p2_in.
///
/// if p1_in and p2_in are both gluons, X is a "Higgs"
/// if p1_in and p2_in are a matching qqbar pair, X is a Z
Event create_pp2X_event(const Particle & beam1, const Particle & beam2, 
                        const Particle & p1_in, const Particle & p2_in);

/// returns a DIS event with beam1 = proton, beam2 = photon 
Event create_DIS_event(const Particle & beam1, const Particle & beam2, 
                        const Particle & p1_in, const Particle & p2_out);
/// creates a VBF event
Event create_VBF_event(const Particle & beam1, const Particle & beam2, 
                         const Particle & p1_in, const Particle & p2_in,
                         const Particle & pH,    
                         const Particle & pj1, const Particle & pj2);

/// create a pp->Zj(Hj) event
Event create_pp2Xj_event(const Particle & beam1, const Particle & beam2, 
                         const Particle & p1_in, const Particle & p2_in,
                         const Particle & pX,    const Particle & pj_out);

/// create a pp->jj event
Event create_pp2jj_event(const Particle & beam1,  const Particle & beam2, 
                         const Particle & p1_in,  const Particle & p2_in,
                         const Particle & pj_out, const Particle & pr_out);

/// Outputs a whole event
inline std::ostream & operator<<(std::ostream & ostr, const Event & event) {
  ostr << "Q =   " << event.Q() << std::endl;
  for (unsigned i = 0; i < event.size(); i++) {
    ostr << std::setw(5) << i << " " << event[i] << std::endl;
  }
  return ostr;
}


inline Dipole * Dipole::dipole_at_3_end(bool include_non_splitting) {
  assert(_event != nullptr);
  if (index_previous >= 0) return &(_event->dipoles()[index_previous]);
  if ((include_non_splitting) && (index_previous<-1)){
    return &(_event->non_splitting_dipoles()[-2-index_previous]);
  }
  return nullptr;
}
inline Dipole * Dipole::dipole_at_3bar_end(bool include_non_splitting) {
  assert(_event != nullptr);
  if (index_next >= 0) return &(_event->dipoles()[index_next]);
  if ((include_non_splitting) && (index_next<-1)){
    return &(_event->non_splitting_dipoles()[-2-index_next]);
  }
  return nullptr;
}
inline const Dipole * Dipole::dipole_at_3_end(bool include_non_splitting) const {
  assert(_event != nullptr);
  if (index_previous >= 0) return &(_event->dipoles()[index_previous]);
  if ((include_non_splitting) && (index_previous<-1)){
    return &(_event->non_splitting_dipoles()[-2-index_previous]);
  }
  return nullptr;
}
inline const Dipole * Dipole::dipole_at_3bar_end(bool include_non_splitting) const {
  assert(_event != nullptr);
  if (index_next >= 0) return &(_event->dipoles()[index_next]);
  if ((include_non_splitting) && (index_next<-1)){
    return &(_event->non_splitting_dipoles()[-2-index_next]);
  }
  return nullptr;
}

inline const Particle & Dipole::particle_3   ()  const {return (*_event)[index_3   ()];}
inline const Particle & Dipole::particle_3bar()  const {return (*_event)[index_3bar()];}

} // namespace panscales
  
#endif // __EVENT_HH__
