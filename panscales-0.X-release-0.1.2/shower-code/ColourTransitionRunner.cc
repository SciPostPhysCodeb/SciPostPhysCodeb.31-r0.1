//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
/// \file ColourTransitionRunner.cc
///
/// This file contains the code that handles the physics of the colour transitions.
///
/// See ColourTransitions.hh for more details

#include "ColourTransitionRunner.hh"
#include <cassert>

using namespace std;

namespace panscales{
  LimitedWarning ColourTransitionRunnerBase::_warning_incomplete_colour_for_isr;
  LimitedWarning ColourTransitionRunnerNODS::_ColourTransitionRunnerNODSWeightDivisionByZero;
  LimitedWarning ColourTransitionRunnerNODS::_ColourTransitionNODSRunnerFirstSudakov;

  /// helper that returns a pointer to the right colour runner class
  /// based on the "QCD" settings
  ColourTransitionRunnerBase * create_colour_transition_runner(const QCDinstance &qcd){
    // NB: if this code changes, remember to update QCD description
    switch (qcd.colour_scheme()){
    case ColourScheme::CFHalfCA:
    case ColourScheme::CATwoCF:  return new ColourTransitionRunnerBase(qcd);
    case ColourScheme::NODS:     return new ColourTransitionRunnerNODS(qcd);
    case ColourScheme::Segment:  return new ColourTransitionRunnerSegment(qcd);
    case ColourScheme::CFFE:     return new ColourTransitionRunnerEmitter(qcd);
    default:
      assert(false && "create_colour_transition_runner: unrecognised colour scheme");
    };
    return nullptr;

    // // at large-Nc, we can use the trivial runner
    // if (qcd.CA_is_2CF())    
    //   return new ColourTransitionRunnerBase(qcd);
    // 
    // if (qcd.colour_factor_nods())
    //   return new ColourTransitionRunnerNODS(qcd);
    // 
    // // case where we want the colour factor based on the emitter
    // if (qcd.colour_factor_from_emitter())
    //   return new ColourTransitionRunnerEmitter(qcd);
    // 
    // return  new ColourTransitionRunnerSegment(qcd); 
  }

  //------------------------------------------------------------------------
  // implementation of elements from the ColourTransitionRunnerBase class
  //------------------------------------------------------------------------
  

  //------------------------------------------------------------------------
  // implementation of elements from the ColourTransitionRunnerEmitter class
  //------------------------------------------------------------------------
  

  //------------------------------------------------------------------------
  // implementation of elements from the ColourTransitionRunnerSegment class
  //------------------------------------------------------------------------
  

  //------------------------------------------------------------------------
  // implementation of elements from the ColourTransitionRunnerMatrixElement2 class
  //------------------------------------------------------------------------
  

}
