#ifndef __PANSCALES_WEIGHT_HH__
#define __PANSCALES_WEIGHT_HH__

#include <vector>
#include <Type.hh>
#include "GSLRandom.hh"

namespace panscales{

/// \class Weight
/// Helper to carry a weight
///
/// This weight can either be an exact 0, an exact 1 or a double Note
/// that, internally, when the weight is an exact 0 (1), the double is
/// correctly stored as 0 (1) so that arithmetic operations can safely
/// be carried on.
class Weight {
public:
  /// enum to store exact cases
  enum Exact {
    exact_one,  ///< weight is exactly one (no need for a random number)
    exact_zero, ///< weight is exactly one (no need for a random number)
    inexact,    ///< weight is stored as a double value
  };

  /// ctor for exact cases
  Weight(Exact exact=exact_zero);
  
  /// ctor for inexact cases
  Weight(double value) : _exact(inexact), _value(value) {}

  /// retrieve the value as a double
  operator double() const { return _value; }
  double value() const { return _value; }

  /// helpers to know if th estored value is exact
  bool is_exact()      const { return _exact != inexact; }
  bool is_exact_zero() const { return _exact == exact_zero; }
  bool is_exact_one()  const { return _exact == exact_one; }

  /// multiplication by another weight
  Weight& operator*=(const Weight& other);
  Weight  operator* (const Weight& other) const;

  /// multiplication by a double
  Weight& operator*=(double other);
  Weight  operator* (double other) const;
  
  /// division by another weight
  Weight operator/(const Weight & other) const;
  
  /// division by a double
  Weight operator/(double other) const;
  
  /// randomly decide whether to accept or not, using the random
  /// generator only if the weight is not an exact zero or one. Note
  /// that the weight is expected to be bounded in [0,1] (if it is not
  /// the ensuing wrong results are the user's responsibility).
  bool accept(GSLRandom &rng) const {
    if (is_exact_zero()) return false;
    if (is_exact_one())  return true;
    return rng.accept(_value);
  }

  /// return this weight after a multiplication by another weight w
  Weight & reweight(const Weight &w) {
    *this *= w;
    return *this;
  }

private:
  Exact _exact;  
  double _value;
#include "autogen/auto_Weight_Weight.hh"
}; // end class Weight

/// multiplication of two weights
Weight operator*(double a, const Weight &w);

/// action to take when asking whether the emission should be vetoed
enum class EmissionAction{  
  accept,           ///< do not veto the emission (at least at this stage)
  veto_emission,    ///< veto the emission and carry on shower evolution
  terminate_shower, ///< veto the emission and terminate shower evolution [keep the event]
  abort_event,      ///< abort the event [i.e. set the event to an empty one]
};

/// structure to handle possible vetoing actions if an emission has a
/// certain probability to be accepted.
///
/// If "action" is accept, the emission should be accepted regardless
/// of the value of accept_probability [accept_probability is assumed
/// to be an exact one in this case].
///
/// If action is anything else, the emission should be accepted with a
/// probability accept_probability, otherwise it shuld be vetoed
/// according to what "action" says.  
class WeightedAction{
public:
  /// ctor from a (guaranteed) action
  WeightedAction(EmissionAction action_in) : action(action_in){
    accept_probability = (action==EmissionAction::accept)
      ? Weight::exact_one : Weight::exact_zero;
  }

  /// ctor from a Weight. In this case, the outcome is either accept or veto
  WeightedAction(const Weight &accept_probability_in)
    : action(EmissionAction::veto_emission), accept_probability(accept_probability_in){}

  /// ctor from a double, interpreted a a weight
  WeightedAction(double accept_probability_in)
    : action(EmissionAction::veto_emission), accept_probability(accept_probability_in){}

  /// ctor from an action and an optional weight
  WeightedAction(EmissionAction action_in, const Weight &accept_probability_in)
    : action(action_in), accept_probability(accept_probability_in){}

  /// the action to take 
  EmissionAction action;     ///< the action to take
  Weight accept_probability; ///< probability to accept even if action says
                             ///< differently. Otherwise, veto with "action"

  /// randomly decide, based on accept_proability, which action need to
  /// be taken.
  ///
  /// A random number is used only when accept_probability is not an
  /// exact zero or one. Note that  accept_probability is expected to be
  /// bounded in [0,1] (if it is not the ensuing wrong results are the
  /// user's responsibility).
  EmissionAction accept(GSLRandom &rng) const{
    if (action==EmissionAction::accept) return EmissionAction::accept;
    return accept_probability.accept(rng) ? EmissionAction::accept : action;
  }

  /// if the acceptance probability is not an exact 0, reweight w and return accept.
  /// Otherwise, set weight to 0 amd return "action"
  EmissionAction apply_reweighting(Weight &w) const{
    if (action==EmissionAction::accept) return EmissionAction::accept;
    w *= accept_probability;
    return (accept_probability.is_exact_zero()) ? action : EmissionAction::accept;
  }
};

} // end of namespace panscales

#include "autogen/auto_Weight_global-hh.hh"
#endif // __PANSCALES_WEIGHT_HH__
