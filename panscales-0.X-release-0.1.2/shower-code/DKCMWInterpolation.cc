//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include "Type.hh"
#include "DKCMWInterpolation.hh"
#include <string>
#include <cmath>
#include <cassert>
#include <fstream>
#include <iostream>

namespace panscales{
  
  // ctor with initilialisation:
  //  - filename            file from which to read the full deltaKCMW table (summed over colours/flavours)
  //  - error_offset_factor offset dKCMW by theis factor time the estimated numerical uncertainty
  //  - subtract_fit        if true, subtract our hardcoded estimated fit and interpolate the remainder
  DeltaKCMWInterpolationTable::DeltaKCMWInterpolationTable(const std::string &filename, double error_offset_factor, bool subtract_fit){
    // hardcode a few things for the sake of getting there 
    _eta_skews = {-18, -6, -4, -2, -1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 5, 6, 8, 10, 12};
    _eta1s = {0, 0.01, 0.02, 0.05, 0.1, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 2.6, 2.8, 3, 3.2, 3.4, 3.6, 3.8, 4, 4.5, 5, 6, 8, 10};
    _phi1s={0, 0.01, 0.02, 0.05, 0.1};
    for (unsigned int i=1; i<=24; ++i) _phi1s.push_back(i*M_PI/24);
    _nskews = _eta_skews.size();
    _neta1s = _eta1s.size();
    _nphi1s = _phi1s.size();

    _subtract_rough_fit = subtract_fit;

    // read the table
    std::ifstream f(filename.c_str());
    assert(f.is_open());
    std::string line;
    double sk, eta1, phi1, dk, dk_err;

    // normalisation is 1/(2*alphas) and the table has been obtained with alphas=0.1
    //double norm = 1/(2*0.1);
    // WITH THE NEW TABLE, we've fixed the normalisation so that it simply becomes 1/alphas
    double norm = 1/(0.1);

    unsigned int nread = 0;
    unsigned int nexp  = _nskews*_neta1s*_nphi1s;
    _dKCMWs.reserve(nexp);
    while (getline(f,line)) {
      // don't read comment lines or blank lines
      if (line.size() == 0 || line[0] == '#') continue;
      std::istringstream linestream(line);
      linestream >> sk >> eta1 >> phi1 >> dk >> dk_err;
      if (_subtract_rough_fit){
        double eta_eff = fabs(eta1);
        double phi_eff = (phi1<=M_PI) ? phi1 : 2*M_PI-phi1;        
        dk -= _rough_fit_function(sk, eta_eff, phi_eff)/norm;
      }
      _dKCMWs.push_back(norm*(dk+error_offset_factor*dk_err));
      ++nread;
    }

    std::cout << "DeltaKCMWInterpolationTable read a table of " << nread << " values (expected " << nexp << ")" << std::endl;
    assert(nread == nexp && "nread and nexp should be equal");
  }
  
  // ctor with separate initilialisation for each colour channel
  //  - filenameCF          file from which to read the deltaKCMW table for the CF colour channel (w CF=1.5)
  //  - filenameCA          file from which to read the deltaKCMW table for the CA colour channel (w CA=3)
  //  - filenameTR          file from which to read the deltaKCMW table for the nfTR colour channel (w nf=5)
  //  - error_offset_factor offset dKCMW by theis factor time the estimated numerical uncertainty
  //  - subtract_fit        if true, subtract our hardcoded estimated fit and interpolate the remainder
  DeltaKCMWInterpolationTable::DeltaKCMWInterpolationTable(const std::string &filenameCF,const std::string &filenameCA,const std::string &filenameTR,
			      double error_offset_factor, bool subtract_fit){
    // hardcode a few things for the sake of getting there 
    _eta_skews = {-18, -6, -4, -2, -1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 5, 6, 8, 10, 12};
    _eta1s = {0, 0.01, 0.02, 0.05, 0.1, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 2.6, 2.8, 3, 3.2, 3.4, 3.6, 3.8, 4, 4.5, 5, 6, 8, 10};
    _phi1s={0, 0.01, 0.02, 0.05, 0.1};
    //for (unsigned int i=1; i<=24; ++i) _phi1s.push_back(i*M_PI/24);
    for (unsigned int i=1; i<=12; ++i) _phi1s.push_back(i*M_PI/12);
    _nskews = _eta_skews.size();
    _neta1s = _eta1s.size();
    _nphi1s = _phi1s.size();

    _subtract_rough_fit = false; //< unsupported for now

    _init_table();
    _add_to_table(filenameCF, 1.0, error_offset_factor); //< proper CF/(3/2) or (CA/2)/(3/2) may be needed in the long run
    _add_to_table(filenameCA, 1.0, error_offset_factor); //< proper CA/(3)   may be needed in the long run
    _add_to_table(filenameTR, 1.0, error_offset_factor); //< proper nf/(5)   may be needed in the long run
  }

  // returns DeltaKCMW for a given phase-space point
  //  - eta_skew   skewedness (log(tan(theta_ij)/2) of the double-soft pair parent dipole
  //  - eta1       lnb of the first of the two double-soft emissions
  //  - phi1       phi of the first of the two double-soft emissions
  double DeltaKCMWInterpolationTable::operator()(double eta_skew, double eta1, double phi1) const{
    // fit subtraction not valid at the moment
    assert (!_subtract_rough_fit && "rough fit subtraction needs to be updated (e.g. for overflow eta1) so is currently not allowed");
    // for overflow eta1 values beyond the largest, just return 0
    if (eta1>_eta1s.back()) return 0.0;
    unsigned int iskew, ieta1, iphi1;
    double       fskew, feta1, fphi1;
    double eta_eff = fabs(eta1);
    assert(phi1 >= 0);
    double phi_eff = (phi1<=M_PI) ? phi1 : 2*M_PI-phi1;
    iskew = _vector_index(eta_skew, _eta_skews, fskew);
    ieta1 = _vector_index(eta_eff,  _eta1s,     feta1);
    iphi1 = _vector_index(phi_eff,  _phi1s,     fphi1);

    //TODO: insert a warning when iskew==_nskews-1, ieta1==0, iphi1==0, fskew==0, feta1==1, fphi1==1 
    double offset = _subtract_rough_fit ? _rough_fit_function(eta_skew, eta_eff, phi_eff) : 0.0;

    //assert(_dKCMWs.size() == (_nskews*_neta1s*_nphi1s));
    
    return offset
      +    fskew  *    feta1  *    fphi1  * _dKCMWs[_table_index(iskew,   ieta1,   iphi1  )]
      +    fskew  *    feta1  * (1-fphi1) * _dKCMWs[_table_index(iskew,   ieta1,   iphi1+1)]
      +    fskew  * (1-feta1) *    fphi1  * _dKCMWs[_table_index(iskew,   ieta1+1, iphi1  )]
      +    fskew  * (1-feta1) * (1-fphi1) * _dKCMWs[_table_index(iskew,   ieta1+1, iphi1+1)]
      + (1-fskew) *    feta1  *    fphi1  * _dKCMWs[_table_index(iskew+1, ieta1,   iphi1  )]
      + (1-fskew) *    feta1  * (1-fphi1) * _dKCMWs[_table_index(iskew+1, ieta1,   iphi1+1)]
      + (1-fskew) * (1-feta1) *    fphi1  * _dKCMWs[_table_index(iskew+1, ieta1+1, iphi1  )]
      + (1-fskew) * (1-feta1) * (1-fphi1) * _dKCMWs[_table_index(iskew+1, ieta1+1, iphi1+1)];
  }
  
  // read a file and add the values to a locally-stored array
  void DeltaKCMWInterpolationTable::_add_to_table(const std::string &filename, double norm, double error_offset_factor){
    // read the table
    std::ifstream f(filename.c_str());
    assert(f.is_open());
    std::string line;
    double sk, eta1, phi1, dk, dk_err;

    unsigned int nread = 0;
    unsigned int nexp  = _nskews*_neta1s*_nphi1s;
    while (getline(f,line)) {
      // don't read comment lines or blank lines
      if (line.size() == 0 || line[0] == '#') continue;
      std::istringstream linestream(line);
      linestream >> sk >> eta1 >> phi1 >> dk >> dk_err;
      if (_subtract_rough_fit){
        double eta_eff = fabs(eta1);
        double phi_eff = (phi1<=M_PI) ? phi1 : 2*M_PI-phi1;        
        dk -= _rough_fit_function(sk, eta_eff, phi_eff)/norm;
      }
      _dKCMWs[nread] += norm*(dk + error_offset_factor*dk_err);
      ++nread;
    }

    std::cout << "DeltaKCMWInterpolationTable read a table of " << nread << " values (expected " << nexp << ")" << std::endl;
    assert(nread == nexp && "nread and nexp should be equal");
  }

  // fitted function that one could subtract in order to be left with something hopefully smoother to interpolate
  double DeltaKCMWInterpolationTable::_rough_fit_function(double eta_skew, double eta1, double phi1) const{
    auto fskew = [&] (double etaskew) -> double{
      return (eta_skew>0)
        ? exp(-2*eta_skew)/(1+exp(-2*eta_skew))
        : 1.0/(1+exp(+2*eta_skew));
    };
    double fphi = 4*pow2(sin(phi1/2));

    assert(eta1>=0);
    double e_minus_eta = exp(-eta1);
    double feta= pow2(1-e_minus_eta)/(1+e_minus_eta*(e_minus_eta-1));

    double fskew_plus  = fskew( eta_skew);
    double fskew_minus = fskew(-eta_skew);
    
    return -0.725 * ( log(fskew_plus + feta + fskew_minus*fphi) 
                    - log(fskew_plus +    1 + fskew_minus*fphi) );
  }

  // returns i s.t.
  //
  //  - vec[i] < val < vec[i+1]   (=> i>=0 and i<size-1)
  //  - underflow goes w i=0
  //  - overflow goes w i=size-2
  //
  // frac is set such that the interpolation from a "value" table is
  //   frac * value[i] + (1-frac) value[i+1]
  unsigned int DeltaKCMWInterpolationTable::_vector_index(double val, const std::vector<double> &vec, double & frac) const{
    // handle the underflow
    if (val<=vec[0]){ frac=1; return 0; }
    // handle the overflow
    if (val>=vec[vec.size()-1]) { frac=0; return vec.size()-2 ;}

    // for now do a linear search
    unsigned int i=0;
    while ((i<vec.size()-1) && (val>vec[i+1])) ++i;
    frac = (vec[i+1]-val)/(vec[i+1]-vec[i]);
    return i;
  }    

}

