
#ifndef __DISktalgorithm_HH__
#define __DISktalgorithm_HH__

//FJSTARTHEADER
//
// Copyright (c) 2005-2018, Matteo Cacciari, Gavin P. Salam and Gregory Soyez
//
//----------------------------------------------------------------------
//  This file is distributed as part of PanScales but is HEAVILY based 
//  on a part of FastJet.
//
//  FastJet is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  The algorithms that underlie FastJet have required considerable
//  development. They are described in the original FastJet paper,
//  hep-ph/0512210 and in the manual, arXiv:1111.6097. If you use
//  FastJet as part of work towards a scientific publication, please
//  quote the version you use and include a citation to the manual and
//  optionally also to hep-ph/0512210.
//
//  FastJet is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with FastJet. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------
//FJENDHEADER

#include "Type.hh"
#include "fjcore_local.hh"

// forward declaration to reduce includes
namespace fjcore {
  class ClusterSequence;
}

#include "Momentum.hh"

//----------------------------------------------------------------------
/// @ingroup jet_algorithms
/// \class DISktBriefJet
/// class to help run a DIS kt algorithm
class DISktBriefJet {
public:
  void init(const fjcore::PseudoJet & jet) {
    // take the energy squared
    _E2 = jet.E()*jet.E();
    // take the massless unit direction
    precision_type modp2 = jet.modp2();
    if (modp2 > 0) {
      precision_type norm = 1.0/sqrt(modp2);
      precision_type nx = jet.px() * norm;
      precision_type ny = jet.py() * norm;
      precision_type nz = jet.pz() * norm;
      _direction = panscales::Momentum(nx, ny, nz, 1, 0);
    } else {
      // occasionally (e.g. at end of clustering), we get a momentum
      // that has zero modp2. In that case, choose an arbitrary
      // direction (here, z): in cases where this happens it should
      // either not make any difference (end of clustering), or the
      // bevaviour of the algorithm was anyway arbitrary.
      _direction = panscales::Momentum(0.0, 0.0, 1.0, 1, 0);
    }
  }

  precision_type distance(const DISktBriefJet * jet) const {
    return 2 * one_minus_costheta(_direction,jet->_direction) * fmin(_E2, jet->_E2);
  }

  precision_type beam_distance() const {
    // assume beam is in the negative z direction (Breit frame)
    // We want 1 - cos(theta_iz), which is 
    // 1 - nz
    precision_type diB = 2.*_E2*(1.0 + _direction.pz());
    return diB;
  }

private:
  /// store the direction and energy
  panscales::Momentum _direction;
  precision_type _E2;
};

//----------------------------------------------------------------------
//
/// @ingroup plugins
/// \class DISktalgorithm
/// Implementation of a DIS version of the kt algorithm 
/// follows
/// The k⊥-clustering algorithm for jets in deep inelastic scattering and hadron collisions
/// by S. Catani, Yu.L. Dokshitzer, B.R. Webber
class DISktalgorithm : public fjcore::JetDefinition::Plugin {
public:
  /// Main constructor for the DISktalgorithm Plugin class.
  ///
  /// The algorithm runs in two modes
  DISktalgorithm (precision_type dcut_in) : _dcut(dcut_in) {}

  /// copy constructor
  DISktalgorithm (const DISktalgorithm & plugin) {
    *this = plugin;
  }

  // the things that are required by base class
  virtual std::string description () const override;
  virtual void run_clustering(fjcore::ClusterSequence &) const override;

  precision_type dcut() const {return _dcut;}

  // exclusive mode - set R = 1
  virtual precision_type R() const override {return precision_type(1.0);}

  /// avoid the warning whenever the user requests "exclusive" jets
  /// from the cluster sequence
  virtual bool exclusive_sequence_meaningful() const override {return false;}

  /// returns false because this plugin is not intended for spherical
  /// geometries (i.e. it's a DIS algorithm).
  virtual bool is_spherical() const override {return false;}

private:
  precision_type _dcut;
};
//
#endif // __DISktalgorithm_HH_
//
