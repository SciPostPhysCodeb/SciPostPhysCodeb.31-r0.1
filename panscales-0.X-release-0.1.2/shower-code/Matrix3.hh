//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __MATRIX3_HH__
#define __MATRIX3_HH__

#include <array>
#include <limits>
#include "Type.hh"
#include "Momentum.hh"

namespace panscales {

//----------------------------------------------------------------------
/// \class Matrix3
/// Contains a few useful definitions of 3x3 matrices

class Matrix3 {
public:
  /// constructs an empty matrix
  Matrix3() : matrix_({{{{0,0,0}},   {{0,0,0}},   {{0,0,0}}}}) {}

  /// constructs a diagonal matrix with "unit" along each diagonal entry
  Matrix3(precision_type unit) : matrix_({{{{unit,0,0}},   {{0,unit,0}},   {{0,0,unit}}}}) {}

  /// constructs a matrix from the array<array<...,3>,3> object
  Matrix3(const std::array<std::array<precision_type,3>,3> & mat) : matrix_(mat) {}

  /// returns the entry at row i, column j
  inline precision_type operator()(int i, int j) const {
    return matrix_[i][j];
  }

  /// returns a matrix for carrying out azimuthal rotation
  /// around the z direction by an angle phi
  static Matrix3 azimuthal_rotation(precision_type phi) {
    precision_type cos_phi = cos(phi);
    precision_type sin_phi = sin(phi);
    Matrix3 phi_rot( {{ {{cos_phi, sin_phi, 0}},
	             {{-sin_phi, cos_phi, 0}},
	             {{0,0,1}}}});
    return phi_rot;
  }

  /// returns a matrix for carrying out a polar-angle
  /// rotation, in the z-x plane, by an angle theta
  static Matrix3 polar_rotation(precision_type theta) {
    precision_type cos_theta = cos(theta);
    precision_type sin_theta = sin(theta);
    Matrix3 theta_rot( {{ {{cos_theta, 0, sin_theta}},
	               {{0,1,0}},
	               {{-sin_theta, 0, cos_theta}}}});
    return theta_rot;
  }

  /// This provides a rotation matrix that takes the z axis to the
  /// direction of p. With skip_pre_rotation = false (the default), it
  /// has the characteristic that if p is close to the z axis then the
  /// azimuthal angle of anything at much larger angle is conserved.
  ///
  /// This implements Eq.(1) of 2019-02-27-ee-phi-definition
  ///
  /// If skip_pre_rotation is true, and when theta is small, the azimuth
  /// of p with angles much larger than theta are not conserved
  template<class T>
  static Matrix3 from_direction(const T & p, bool skip_pre_rotation = false){
    precision_type pmax_perp = std::max(std::abs(p.px()), std::abs(p.py()));
    precision_type pmax      = std::max(pmax_perp,        std::abs(p.pz()));

    precision_type pt;
    if (pmax_perp>0){
      // switch to a more accurate calculation if we are close to the denormalised limit
      if (pmax_perp*pmax_perp*std::numeric_limits<precision_type>::epsilon() < std::numeric_limits<precision_type>::min()){
        precision_type norm_perp = 1/pmax_perp;
        pt = sqrt(pow2(norm_perp*p.px()) + pow2(norm_perp*p.py())) * pmax_perp;
      } else {
        pt = p.pt();
      }
    } else {
      pt = 0.0;
    }

    precision_type modp;
    // switch to a more accurate calculation if we are close to the denormalised limit
    if (pmax*pmax*std::numeric_limits<precision_type>::epsilon() < std::numeric_limits<precision_type>::min()){
      precision_type norm = 1/pmax;
      modp = sqrt(pow2(norm*p.px()) + pow2(norm*p.py()) + pow2(norm*p.pz())) * pmax;
    } else {
      modp = p.modp();
    }
    
    //precision_type pt   = p.pt();
    //precision_type modp = p.modp();
    precision_type cos_theta = p.pz()/modp;
    precision_type sin_theta = pt    /modp;
    precision_type cos_phi, sin_phi;
    if (pt > 0.0) {
      cos_phi = p.px()/pt;
      sin_phi = p.py()/pt;
    } else {
      cos_phi = 1.0;
      sin_phi = 0.0;
    }

    Matrix3 phi_rot({{ {{ cos_phi,-sin_phi, 0 }},
                       {{ sin_phi, cos_phi, 0 }},
                       {{       0,       0, 1 }} }});
    Matrix3 theta_rot( {{ {{ cos_theta, 0, sin_theta }},
                          {{         0, 1,         0 }},
                          {{-sin_theta, 0, cos_theta }} }});

    
    // since we have orthogonal matrices, the inverse and transpose
    // are identical; we use the transpose for the frontmost rotation
    // because 
    if (skip_pre_rotation) {
      return phi_rot * theta_rot;
    } else {
      return phi_rot * (theta_rot * phi_rot.transpose());
    }
  }

  // /// This provides a rotation matrix that takes the z axis to the
  // /// direction of p. With skip_pre_rotation = false (the default), it
  // /// has the characteristic that if p is close to the z axis then the
  // /// azimuthal angle of anything at much larger angle is conserved.
  // ///
  // /// This implements Eq.(1) of 2019-02-27-ee-phi-definition
  // ///
  // /// If skip_pre_rotation is true, and when theta is small, the azimuth
  // /// of p with angles much larger than theta are not conserved
  // template<class T, std::enable_if_t<std::is_base_of<Momentum3<precision_type>, T>::value, bool> = true>
  // static Matrix3 from_direction(const T & p, bool skip_pre_rotation = false) {
  //   std::cout << "In MomM3 specialisation" << std::endl;
  //   //...  p.normalised_by_max_component_3()
  //   precision_type pt = p.pt();
  //   precision_type modp = p.modp();
  //   precision_type cos_theta = p.pz() / modp;
  //   precision_type sin_theta = pt / modp;
  //   precision_type cos_phi, sin_phi;
  //   if (pt > 0.0) {
  //     cos_phi = p.px()/pt;
  //     sin_phi = p.py()/pt;
  //   } else {
  //     cos_phi = 1.0;
  //     sin_phi = 0.0;
  //   }
  // 
  //   Matrix3 phi_rot({{ {{ cos_phi,-sin_phi, 0 }},
  //                      {{ sin_phi, cos_phi, 0 }},
  //                      {{       0,       0, 1 }} }});
  //   Matrix3 theta_rot( {{ {{ cos_theta, 0, sin_theta }},
  //                         {{         0, 1,         0 }},
  //                         {{-sin_theta, 0, cos_theta }} }});
  // 
  //   // since we have orthogonal matrices, the inverse and transpose
  //   // are identical; we use the transpose for the frontmost rotation
  //   // because 
  //   if (skip_pre_rotation) {
  //     return phi_rot * theta_rot;
  //   } else {
  //     return phi_rot * (theta_rot * phi_rot.transpose());
  //   }
  // }

  
  template<class T>
  static Matrix3 from_direction_no_pre_rotn(const T & p) {
    return from_direction(p,true);
  }



  /// returns the transposed matrix
  Matrix3 transpose() const {
    // 00 01 02
    // 10 11 12
    // 20 21 22
    Matrix3 result = *this;
    std::swap(result.matrix_[0][1],result.matrix_[1][0]);
    std::swap(result.matrix_[0][2],result.matrix_[2][0]);
    std::swap(result.matrix_[1][2],result.matrix_[2][1]);
    return result;
  }

  /// returns the product with another matrix
  Matrix3 operator*(const Matrix3 & other) const {
    Matrix3 result;
    // r_{ij} = sum_k this_{ik} & other_{kj}
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        for (int k = 0; k < 3; k++) {
          result.matrix_[i][j] += this->matrix_[i][k] * other.matrix_[k][j];
        }
      }
    }
    return result;
  }
  
  friend Momentum operator*(const Matrix3 &, const Momentum &);
  friend Momentum3<precision_type> operator*(const Matrix3 &, const Momentum3<precision_type> &);
  friend std::ostream & operator<<(std::ostream & ostr, const Matrix3 & mat);
private:
  std::array<std::array<precision_type,3>,3> matrix_;
};


/// returns the product of this matrix with the PseudoJet,
/// maintaining the 4th component of the PseudoJet unchanged
inline Momentum3<precision_type> operator*(const Matrix3 & mat, const Momentum3<precision_type> & p) {
  // r_{i} = m_{ij} p_j
  std::array<precision_type,3> res3{{0,0,0}};
  for (unsigned i = 0; i < 3; i++) {
    res3[i] += mat.matrix_[i][0] * p.px();
    res3[i] += mat.matrix_[i][1] * p.py();
    res3[i] += mat.matrix_[i][2] * p.pz();
  }
  return Momentum3<precision_type>(res3[0], res3[1], res3[2]);
}

inline Momentum operator*(const Matrix3 & mat, const Momentum & p) {
  Momentum3<precision_type> newp3 = mat * p.p3();
  return Momentum(newp3.px(), newp3.py(), newp3.pz(), p.E(), p.m2());
}


inline std::ostream & operator<<(std::ostream & ostr, const Matrix3 & mat) {
  ostr << mat.matrix_[0][0] << " " << mat.matrix_[0][1] << " " << mat.matrix_[0][2] << std::endl;
  ostr << mat.matrix_[1][0] << " " << mat.matrix_[1][1] << " " << mat.matrix_[1][2] << std::endl;
  ostr << mat.matrix_[2][0] << " " << mat.matrix_[2][1] << " " << mat.matrix_[2][2] << std::endl;
  return ostr;
}

}


#endif // __MATRIX3_HH__
