//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __EMISSIONVETO_HH__
#define __EMISSIONVETO_HH__

#include <string>
#include <vector>
#include <cassert>
#include "Weight.hh"
#include "Event.hh"

namespace panscales{

//NOTE: The following files have to be checked for the double->Weight weight change
// analyses/rapidity-slice/GridVeto.hh
// analyses/nll-validation/EmissionVetoSoftSpin.hh
// analyses/global-obs-double-log/global-obs-double-log.cc
// analyses/soft-spin-all-order/EmissionVetoSoftSpin.hh
// analyses/contour-plots/LundDistanceVeto.hh
// analyses/rapidity-slice-double-soft-nll-tests/GridVeto.hh
// analyses/event-shapes-nndl-tests/event-shapes-nndl-tests.cc


  
//----------------------------------------------------------------------
/// @ingroup emveto_classes
/// \class EmissionVeto
/// base class for vetoing individual emissions.
///
template <class Shower>
class EmissionVeto {
public:
  /// Default constructor
  EmissionVeto() {}

  /// virtual dtor sa that we can easily have derived classes
  virtual ~EmissionVeto() {}

  /// returns a string describing the veto
  virtual std::string description() const {return "no description available";}

  /// (Re)Set anything that needs initialisation on an event-by-event basis.
  virtual void initialise(Event & event, double lnv) {
    return;
  }

  /// Allow potential veto of the emission before full kinematic construction
  /// and before computation of the acceptance probability. Recording of any
  /// desired info, related to the veto or otherwise, can also be carried out
  /// in here.
  ///
  /// See WeightedActionabove for how to interpret the returned value
  ///
  /// The event weight should be passed as "weight" and derived veto
  /// classes are allowed to modify it.
  virtual WeightedAction pre_accept_prob_veto(const Event & event,
                                              const typename Shower::EmissionInfo & emission_info,
                                              Weight & weight, bool is_global = false) {
    return EmissionAction::accept;
  }

  /// Evaluate whether an emission has passed a final veto criterion (set in
  /// this function) based on i) having made it to the final point of acceptance
  /// in the veto algorithm, and possibly also, ii) the full kinematic info here.
  /// If it passes, we update the event with the new emission and keep evolving
  /// down from the lnv of the emission, as usual. If it fails, we don't update
  /// the event with the emission, (element.update_event is not called), but we
  /// still continue evolution down from the scale of the just-vetoed branching.
  virtual WeightedAction post_do_split_veto(const Event & event,
                                            const typename Shower::EmissionInfo & emission_info,
                                            Weight & weight, bool is_global = false) {
    return EmissionAction::accept;
  }

  /// allow to do some internal maintenance once we know the emission
  /// has been accepted and we are going to update the event
  ///
  /// Note that this will be called before we actually update the event
  /// so as to have access to the Emission info (which could otherwise
  /// be reset, e.g. by matching)
  virtual void post_emission_acceptance(const Event & event, 
                                        const typename Shower::EmissionInfo & emission_info) {}

  /// Called at the end of the event generation and returns true if the
  /// the event is to be rejected
  ///
  /// This is only allowed to return the accept and abort_event
  /// actions (with an optional weight)
  virtual WeightedAction final_event_veto(const Event & event) const{
    return EmissionAction::accept;
  }

  /// Signal that we want to call modify_element_in_store_pre_splitting
  /// for all elements (depending on lnv). This function is called just
  /// after find_element_alg_ptr runs to select a tentative lnv & element.
  virtual bool modify_elements_pre_splitting(
    Event & event, ShowerBase::Element & element, const double & lnv) {
    return false;
  }

  /// Facilitate modification of elements just after the find_element_alg_ptr
  /// has run to select a tentative lnv and element, provided that
  /// modify_elements_in_store_pre_splitting returned true first.
  virtual bool modify_element_pre_splitting(
    Event & event, ShowerBase::Element & element, const double & lnv) {
    return false;
  }  
};
  
//----------------------------------------------------------------------
/// @ingroup emveto_classes
/// \class EmissionVetoLnZmin
/// Class to veto emissions whose (approximate) energy fraction
/// relative to the centre-of-mass energy is below some threshold
template<class Shower>
class EmissionVetoLnZmin : public EmissionVeto<Shower> {

public:
  /// constructor that specifies the minimum log(z) that
  /// will be accepted for emissions
  EmissionVetoLnZmin(double lnzmin) : _lnzmin(lnzmin) {}

  /// Initialisation of the veto paraneters
  virtual void initialise(Event & event, double lnv) override {
    _lnQ = 0.5 * to_double(log( event.Q2() ));
  }

  /// potential veto prior to the shower acceptance probability
  virtual WeightedAction pre_accept_prob_veto(const Event & event,
                                              const typename Shower::EmissionInfo & emission_info,
                                              Weight& weight, bool is_global = false) override {
    // kt/Q = z exp(-eta) -> z = kt/Q exp(eta)
    const auto & element = *(emission_info.element());
    double lnkt = element.lnkt_approx(emission_info.lnv, emission_info.lnb);
    double eta  = element.eta_approx (emission_info.lnv, emission_info.lnb);
    double lnz  = lnkt + std::abs(eta) - _lnQ;
    return (lnz < _lnzmin) ? EmissionAction::veto_emission : EmissionAction::accept;
  }
  
private:
  double _lnzmin;
  double _lnQ = 0;
};
  
//----------------------------------------------------------------------
/// @ingroup emveto_classes
/// \class EmissionVetoEta
/// Class to veto emissions whose (approximate) eta in the 
/// event frame is above some threshold
template<class Shower>
class EmissionVetoEta : public EmissionVeto<Shower> {

public:
  /// constructor that specifies the minimum log(z) that
  /// will be accepted for emissions
  EmissionVetoEta(double eta_cut, bool from_above) : _eta_cut_(eta_cut), _from_above(from_above){}

  /// Initialisation of the veto paraneters
  virtual void initialise(Event & event, double lnv) override {
    _lnQ = 0.5 * to_double(log( event.Q2() ));
  }

  /// potential veto prior to the shower acceptance probability
  virtual WeightedAction pre_accept_prob_veto(const Event & event,
                                              const typename Shower::EmissionInfo & emission_info,
                                              Weight& weight, bool is_global=false) override {
    // calculate eta approx                      
    double eta = emission_info.element()->eta_approx(emission_info.lnv,
                                                     emission_info.lnb);
    // from above: veto everything between eta_c and eta = infinity
    if(_from_above) return (eta > _eta_cut_) ? EmissionAction::veto_emission : EmissionAction::accept;
    else            return (eta < _eta_cut_) ? EmissionAction::veto_emission : EmissionAction::accept; //< otherwise veto if -infinity < eta < eta_c
  }
private:
  double _eta_cut_;
  bool   _from_above;
  double _lnQ = 0;

};

//----------------------------------------------------------------------
/// @ingroup emveto_classes
/// \class EmissionVetoVBF
/// Class to veto emissions whose lnv is above the invariant mass of 
/// the IF dipole in VBF processes
template<class Shower>
class EmissionVetoVBF : public EmissionVeto<Shower> {

public:
  /// constructor that specifies if the starting scale is Q2 for each chain,
  // or Q2*(1-x)/x = tip of the big lund plane
  EmissionVetoVBF(bool use_kin_limit) {
    _use_kin_limit = use_kin_limit;
    if(use_kin_limit) std::cout<<"VBF veto: v < Q (1-x)/x\n";
    else              std::cout<<"VBF veto: v < Q \n";
  }

  /// Initialisation of the veto paraneters
  virtual void initialise(Event & event, double lnv) override {		
    // use Q2
    _lnQ_chain1_max = 0.5 * log(event.Q2_dis(1));
    _lnQ_chain2_max = 0.5 * log(event.Q2_dis(2));
    // add log(1-x/x) if use_kin_lim = true
    if(_use_kin_limit){ // use Q2*(1-x)/x
      precision_type x1 = event.ref_in(1).E()/event.beam1().E();
      precision_type x2 = event.ref_in(2).E()/event.beam2().E();
      _lnQ_chain1_max += to_double(0.5*log((1-x1)/x1));
      _lnQ_chain2_max += to_double(0.5*log((1-x2)/x2));
    } 
  }

  /// potential veto prior to the shower acceptance probability
  virtual WeightedAction pre_accept_prob_veto(const Event & event,
                                              const typename Shower::EmissionInfo & emission_info,
                                              Weight& weight, bool is_global=false) override {
    // get the dipole chain
    const auto & element = *(emission_info.element());
    double lnv = emission_info.lnv;
    int chain = element.emitter().get_DIS_chain();
    assert(chain == element.spectator().get_DIS_chain() && "Emitter and spectator do not belong together in the same chain");
    if        (chain == 1){
      // emission needs to be vetoed if lnv is above the dipole invariant mass
      return (lnv > _lnQ_chain1_max) ? EmissionAction::veto_emission : EmissionAction::accept;
    } else if (chain == 2){
      // emission needs to be vetoed if lnv is above the dipole invariant mass
      return (lnv > _lnQ_chain2_max) ? EmissionAction::veto_emission : EmissionAction::accept;
    } else assert(false && "Unknown chain number");
    return EmissionAction::veto_emission;
  }
private:
  double _lnQ_chain1_max = 0; 
  double _lnQ_chain2_max = 0;
  bool _use_kin_limit = false;

};

//----------------------------------------------------------------------
/// @ingroup emveto_classes
/// \class EmissionVetoKtVBF
/// Class to veto emissions whose lnkt is above the invariant mass of 
/// the IF dipole in VBF processes
template<class Shower>
class EmissionVetoKtVBF : public EmissionVeto<Shower> {

public:
  /// constructor that specifies the kT2max is Q2 for each chain,
  EmissionVetoKtVBF(){std::cout<<"VBF veto: kT < Q";}

  /// Initialisation of the veto paraneters
  virtual void initialise(Event & event, double lnv) override {		
    // use Q2
    _lnQ_chain1_max = 0.5 * log(event.Q2_dis(1));
    _lnQ_chain2_max = 0.5 * log(event.Q2_dis(2));
  }

  /// potential veto prior to the shower acceptance probability
  virtual WeightedAction pre_accept_prob_veto(const Event & event,
                                              const typename Shower::EmissionInfo & emission_info,
                                              Weight& weight, bool is_global=false) override {

    const auto & element = *(emission_info.element());
    double lnkt =  element.lnkt_approx(emission_info.lnv, emission_info.lnb);
    // get the dipole chain 
    int chain = element.emitter().get_DIS_chain();
    if (chain != element.spectator().get_DIS_chain()) assert(false && "Emitter and spectator do not belong together in the same chain");
    if      (chain == 1){
      // emission needs to be vetoed if lnkt is above the dipole invariant mass
      return (lnkt > _lnQ_chain1_max) ? EmissionAction::veto_emission : EmissionAction::accept; 
    } else if (chain == 2){
      // emission needs to be vetoed if lnkt is above the dipole invariant mass
      return (lnkt > _lnQ_chain2_max) ? EmissionAction::veto_emission : EmissionAction::accept;
    } else assert(false && "Unknown chain number");
    return EmissionAction::veto_emission;
  }
private:
  double _lnQ_chain1_max = 0; 
  double _lnQ_chain2_max = 0;
};


} // namespace panscales


#endif // __EMISSIONVETO_HH__
