//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __MATCHINGHARDMATRIXLEMENTS_HH__
#define __MATCHINGHARDMATRIXLEMENTS_HH__

#include "MatchedProcess.hh"

namespace panscales {

  // forward declaration to QCD
  class QCDinstance;

  //----------------------------------------------------------------------
  /// @ingroup matching
  /// \class MatchingHardMatrixElement
  /// Class to hold the tree-level exact matrix elements
  /// At the moment, only e+e-->H/Z have been implemented and tested.
  /// See arXiv:2301.09645 for more details.
  class MatchingHardMatrixElement {
  public:
    /// ctor
    MatchingHardMatrixElement(const QCDinstance & qcd, bool random_axis = false)
        : _qcd(qcd), _random_axis(random_axis) {
      // set splitting weights
      _w_gg_splitting = qcd.splitting_finite_weight_g2gg();
      _w_qq_splitting = qcd.splitting_finite_weight_g2qqbar();
      // fill the splitting weights with zero of a specified length
      _splitting_weights = std::vector<precision_type>(qcd.number_of_splitting_channels());
    }
    /// implemented in derived classes, gives a description of the hard ME
    virtual std::string description() const = 0;

    /// function to reset the splitting weights to 0
    void reset_splitting_weights() {
      // make sure all entries are zero
      std::fill(_splitting_weights.begin(), _splitting_weights.end(), 0.);
    }
    
    /// \brief computes the hard matrix element for e+e- -> Z -> qq
    ///        and fills the splitting weights vector
    /// \param event 
    /// \param element 
    /// \param emission_info 
    /// \param hoppet_runner 
    /// \param emit 
    /// \param spec 
    /// \param rad 
    /// should be implemented in the derived classes
    virtual void compute_hard_me(const Event & event,
                         const ShowerBase::Element & element,
                         const ShowerBase::EmissionInfo & emission_info,
                         const HoppetRunner * hoppet_runner,
                         const Momentum & emit,
                         const Momentum & spec,
                         const Momentum & rad) = 0;
    
    /// getting the weight that we should multiply the born with
    /// given a tree-level event to generate correctly an orientated distribution
    virtual double get_tree_level_weight(Event & event) = 0;

    // Dtor
    virtual ~MatchingHardMatrixElement() {}

    // sets the g->gg and g->qqbar splitting function symmetrisation
    // !DO NOT CHANGE THEM FOR POWHEG MATCHING!
    void set_g2gg_splitting_weight(double wgg) { _w_gg_splitting = wgg; }
    void set_g2qq_splitting_weight(double wqq) { _w_qq_splitting = wqq; }

    // todo: return not a copy
    /// Returns the splitting weights filled in compute_hard_me
    std::vector<precision_type> splitting_weights() const {return _splitting_weights;}

  protected:

    const QCDinstance _qcd; ///< QCD instance

    // vector holding the weights for each of the splitting channels.
    // Splitting channels themselves are defined in an enum in QCD.hh
    std::vector<precision_type> _splitting_weights;

    // The g->gg and g->qqbar splitting function symmetrisation
    // They can in principle be chosen differently from the shower's partitioning,
    // though other choices than the shower's break NNDL for Powheg-style matching!
    // FOR TESTING ONLY
    precision_type _w_gg_splitting, _w_qq_splitting;

    // whether we're generating over the full Born polar angle
    bool _random_axis;

  };

  //----------------------------------------------------------------------
  /// @ingroup matching
  /// \class HggHardME
  /// Derived class for 3-jet matrix element in the colour-singlet
  /// H->gg process
  class HggHardME : public MatchingHardMatrixElement {
  public:

    // Ctor
    HggHardME(const QCDinstance & qcd, bool random_axis = false)
      : MatchingHardMatrixElement(qcd, random_axis) {}
    
    /// Description of the hard ME
    virtual std::string description() const {
        std::ostringstream ostr;
        ostr << "Matrix element for ee → H -> ggg/gqqbar"
            << " with splitting weights wgg = " << _w_gg_splitting << ", wqq = " << _w_qq_splitting;
        return ostr.str();
    }

    /// Hard ME partition for H->gg
    /// (see 2021-11-11.../Matching-Notes.pdf)
    virtual void compute_hard_me(const Event & event,
            const ShowerBase::Element & element,
            const ShowerBase::EmissionInfo & emission_info,
            const HoppetRunner * hoppet_runner,
            const Momentum & emit,
            const Momentum & spec,
            const Momentum & rad);
    
    // born weight not implemented
    double get_tree_level_weight(Event & event){
      assert(false && "Not implemented");
      return 0.;
    }
  };

  //----------------------------------------------------------------------
  /// @ingroup matching
  /// \class ZqqHardME
  /// Derived class for 3-jet matrix element in the Z->qqbar process
  class ZqqHardME : public MatchingHardMatrixElement {
  public:

    // Ctor
    ZqqHardME(const QCDinstance & qcd, bool random_axis = false)
      : MatchingHardMatrixElement(qcd, random_axis) {}
    
    /// Description of the hard ME
    virtual std::string description() const {
        std::ostringstream ostr;
        ostr << "Matrix element for ee → Z → qqbarg"
            << " with splitting weights wgg = " << _w_gg_splitting << ", wqq = " << _w_qq_splitting
            << (_random_axis ? " (with full Born polar angle)" : "");
        return ostr.str();
    }

    /// Hard ME
    ///
    ///   |M|^2 = ( x_em^2 + x_sp^2 ) / [ (1-x_em) . (1-x_sp) ]  integrated over Euler angles
    /// or
    ///   |M|^2 = ((e+.q)^2 + (e+.qbar)^2
    ///           + (e-.q)^2 + (e-.qbar)^2)
    ///           / (q.g) / (qbar.g) / Q^2
    virtual void compute_hard_me(const Event & event,
            const ShowerBase::Element & element,
            const ShowerBase::EmissionInfo & emission_info,
            const HoppetRunner * hoppet_runner,
            const Momentum & emit,
            const Momentum & spec,
            const Momentum & rad);
    
    // born weight is the (1+cos^2)/2 angle
    double get_tree_level_weight(Event & event){
      // we ignore a constant normalisation
      double costheta = to_double(event.particles()[event.particles().size()-1].pz()/event.particles()[event.particles().size()-1].E());
      return (1 + costheta*costheta)/2;
    }
  };
} // namespace panscales

#endif // __MATCHINGHARDMATRIXLEMENTS_HH__
