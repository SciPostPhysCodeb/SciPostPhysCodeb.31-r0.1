//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
// fastjet classes
#include "Type.hh"
#include "fjcore_local.hh"
#include "NNFJN2Plain-local.hh"
#include "EEKtPlugin.hh"

#include "Momentum.hh"

#include <iomanip>
#include <vector>
#include <sstream>
#include <limits>


using namespace std;
using namespace panscales;

FJCORE_BEGIN_NAMESPACE      // defined in fastjet/internal/base.hh


//----------------------------------------------------------------------
/// class to help run a e+e- kt algorithm
class EEKtBriefJet {
public:
  EEKtBriefJet(){}
  
  void init(const PseudoJet & jet){
    // From the point of view of calculating the angular distances
    // we just need a massless unit-length vector 
    precision_type modp2 = jet.modp2();
    _E2 = jet.E()*jet.E();
    if (modp2 > 0) {
      precision_type norm = 1.0/sqrt(modp2);
      precision_type nx = jet.px() * norm;
      precision_type ny = jet.py() * norm;
      precision_type nz = jet.pz() * norm;
      _direction = Momentum(nx, ny, nz, 1, 0);
    } else {
      // occasionally (e.g. at end of clustering), we get a momentum
      // that has zero modp2. In that case, choose an arbitrary
      // direction (here, z): in cases where this happens it should
      // either not make any difference (end of clustering), or the
      // bevaviour of the algorithm was anyway arbitrary.
      _direction = Momentum(0.0, 0.0, 1.0, 1, 0);
    }
  }

  precision_type distance(const EEKtBriefJet * jet) const {
    return 2 * one_minus_costheta(_direction,jet->_direction) * min(_E2, jet->_E2);
  }

  precision_type geometrical_distance(const EEKtBriefJet * jet) const {
    return 2*one_minus_costheta(_direction,jet->_direction);
  }

  precision_type momentum_factor() const {
    return _E2;
  }

  precision_type beam_distance() const {
    return numeric_limit_extras<precision_type>::max();
  }
  
  precision_type geometrical_beam_distance() const {
    // here anything larger than 4 would do
    return 5.0;
  }

protected:
  Momentum _direction;
  precision_type _E2;
};

//----------------------------------------------------------------------
string EEKtPlugin::description() const {
  ostringstream desc;
  desc << "e+e- Kt algorithm plugin";
  switch(_strategy) {
  case strategy_NNH:
    desc << ", using NNH strategy"; break;
  case strategy_NNFJN2Plain:
    desc << ", using NNFJN2Plain strategy"; break;
  //case strategy_NNFJN2Simple:
  //  desc << ", using NNFJN2Simple strategy"; break;
  default:
    throw Error("Unrecognized strategy in EEKtPlugin");
  }

  return desc.str();
}



// //----------------------------------------------------------------------
// // The version using FJ's simple_N2_cluster
// //----------------------------------------------------------------------
// 
// /// fundamental structure for e+e- clustering (a variant of the one in
// /// fastjet/ClusterSequence.hh which uses PanScales omenta
// struct EESimpleKtBriefJet {
//   precision_type NN_dist;  // obligatorily present
//   precision_type kt2;      // obligatorily present == E^2 in general
//   EESimpleKtBriefJet * NN; // must be present too
//   int _jets_index; // must also be present!
//   //...........................................................
//   Momentum direction;
// };
// 
// 
// //----------------------------------------------------------------------
// template<> inline void ClusterSequence::_bj_set_jetinfo(EESimpleKtBriefJet * const jetA,
//                                                         const int _jets_index) const {
//   const PseudoJet & jet = _jets[_jets_index];
//   double E = jet.E();
//   jetA->kt2  = E*E;
// 
//   precision_type modp2 = jet.modp2();
//   if (modp2 > 0) {
//     precision_type norm = 1.0/sqrt(modp2);
//     precision_type nx = jet.px() * norm;
//     precision_type ny = jet.py() * norm;
//     precision_type nz = jet.pz() * norm;
//     jetA->direction = Momentum(nx, ny, nz, 1, 0);
//   } else {
//     // occasionally (e.g. at end of clustering), we get a momentum
//     // that has zero modp2. In that case, choose an arbitrary
//     // direction (here, z): in cases where this happens it should
//     // either not make any difference (end of clustering), or the
//     // bevaviour of the algorithm was anyway arbitrary.
//     jetA->direction = Momentum(0.0, 0.0, 1.0, 1, 0);
//   }
//   jetA->_jets_index = _jets_index;
// 
//   // initialise NN info as well
//   jetA->NN_dist = 4.0;
//   jetA->NN      = NULL;
// }
// 
// //----------------------------------------------------------------------
// // returns the angular distance between the two jets
// template<> double ClusterSequence::_bj_dist(const EESimpleKtBriefJet * const jeta, 
//                                             const EESimpleKtBriefJet * const jetb) const {
//   return 2*one_minus_costheta(jeta->direction,jetb->direction);
// }


//----------------------------------------------------------------------
// actual clustering for NNH and friends
//----------------------------------------------------------------------

template<class NNType> void EEKtPlugin::_actual_run_clustering(ClusterSequence & cs) const {

  int njets = cs.jets().size();

  NNType nn(cs.jets());

  while (njets > 0) {
    int i, j, k;
    precision_type dij = nn.dij_min(i, j);

    if (j >= 0) {
      cs.plugin_record_ij_recombination(i, j, dij, k);
      nn.merge_jets(i, j, cs.jets()[k], k);
    } else {
      precision_type diB = cs.jets()[i].E()*cs.jets()[i].E(); // get new diB
      cs.plugin_record_iB_recombination(i, diB);
      nn.remove_jet(i);
    }
    njets--;
  }

}

//----------------------------------------------------------------------
void EEKtPlugin::run_clustering(ClusterSequence & cs) const {

  switch(_strategy) {
  case strategy_NNH:
    _actual_run_clustering<NNH<EEKtBriefJet> >(cs);
    break;
  case strategy_NNFJN2Plain:
    _actual_run_clustering<NNFJN2Plain<EEKtBriefJet> >(cs);
    break;
  // case strategy_NNFJN2Simple:
  //   cs.plugin_simple_N2_cluster<EESimpleKtBriefJet>();
  //   break;
  default:
    throw Error("Unrecognized strategy in EEKtPlugin");
  }
}


FJCORE_END_NAMESPACE      // defined in fastjet/internal/base.hh
