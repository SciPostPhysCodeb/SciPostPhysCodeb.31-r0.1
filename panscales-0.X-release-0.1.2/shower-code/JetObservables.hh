//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __JETOBSERVABLES_HH__
#define __JETOBSERVABLES_HH__

#include "Observables.hh"
#include "Type.hh"
#include "fjcore_local.hh"

//----------------------------------------------------------------------
/// @ingroup observable_classes
/// \class mMDTmass 
/// class that calculates the groomed jet mass

class mMDTmass : public Observable {

public:
  // Constructor.
  // zcut is the cut on the symmetry measure
  // by default select_in=1 -> keep all final state particles in the event
  mMDTmass(double zcut = 0.1, int select_in = 1)
    : Observable(select_in), _zcut(zcut),
      _jet_def(fjcore::ee_genkt_algorithm, 1.0, 0.0, fjcore::E_scheme) {}

  virtual bool analyse(const panscales::Event& event) override;

  precision_type groomed_mass(const fjcore::PseudoJet& jet);
  
  // Provide a listing of the info.
  virtual void list() const override;
  
  // Return info on results of analysis
  virtual precision_type operator()() const override {return mass();}
  precision_type mass() const {return _mMDTmass;}

private:
  // Outcome of analysis
  double _zcut;
  precision_type _mMDTmass;
  fjcore::JetDefinition _jet_def;
};

#endif // __JETOBSERVABLES_HH__
