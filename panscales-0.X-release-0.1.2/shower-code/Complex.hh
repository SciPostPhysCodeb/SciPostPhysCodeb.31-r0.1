//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef __PANSCALES_COMPLEX_HH__
#define __PANSCALES_COMPLEX_HH__

#include <cmath>
#include <ostream>

namespace panscales {

/// Implementation of complex numbers, intended to work with generic
/// numerical types T, e.g. double_exp as well as in-built types. 
///
/// The reason for needing this is that according to
/// https://en.cppreference.com/w/cpp/numeric/complex, the behavior of
/// std::complex<T> is unspecified (and may fail to compile) if T is not a
/// cv-unqualified standard (until C++23) floating-point type.
///
/// As of 2023-10-22, the class has two main limitations:
///
/// - special functions with complex numbers are not yet implemented
/// - division operations and abs() can fail when involving numbers
///   that are outside the range sqrt(numeric_limits<T>::min()) to 
///   sqrt(numeric_limits<T>::max()), because they go via the 
///   squares of the numbers.
///
template<class T>
class Complex {
public:
  Complex(T real, T imag) : _real(real), _imag(imag) {}
  Complex(T real)         : _real(real), _imag(0.0)  {}
  Complex()               : _real(0.0),  _imag(0.0)  {}

  T real() const { return _real; }
  T imag() const { return _imag; }
  T norm() const { return _real*_real + _imag*_imag;}

  Complex<T> operator=(T a) {
    _real = a;
    _imag = 0.0;
    return *this;}

  /// arithmetic operators with other Complex numbers
  ///@{ 

  Complex<T> operator-() const {
    return Complex<T>(-_real, -_imag);
  }

  Complex<T> operator+(const Complex<T>& rhs) const {
    return Complex<T>(_real + rhs._real, _imag + rhs._imag);
  }
  Complex<T> operator-(const Complex<T>& rhs) const {
    return Complex<T>(_real - rhs._real, _imag - rhs._imag);
  }
  Complex<T> operator*(const Complex<T>& rhs) const {
    return Complex<T>(_real*rhs._real - _imag*rhs._imag,
                      _real*rhs._imag + _imag*rhs._real);
  }
  Complex<T> operator/(const Complex<T>& rhs) const {
    T denom = rhs.norm();
    return Complex<T>((_real*rhs._real + _imag*rhs._imag)/denom,
                      (_imag*rhs._real - _real*rhs._imag)/denom);
  }

  Complex<T> & operator+=(const Complex<T>& rhs) {
    _real += rhs._real;
    _imag += rhs._imag;
    return *this;
  }
  Complex<T> & operator-=(const Complex<T>& rhs) {
    _real -= rhs._real;
    _imag -= rhs._imag;
    return *this;
  }
  Complex<T> & operator*=(const Complex<T>& rhs) {
    T tmp_real = _real*rhs._real - _imag*rhs._imag;
    _imag = _real*rhs._imag + _imag*rhs._real;
    _real = tmp_real;
    return *this;
  }
  Complex<T> & operator/=(const Complex<T>& rhs) {
    T denom = rhs.norm();
    T tmp_real = (_real*rhs._real + _imag*rhs._imag)/denom;
    _imag = (_imag*rhs._real - _real*rhs._imag)/denom;
    _real = tmp_real;
    return *this;
  }


  ///@}

  /// arithmetic operators with real numbers
  ///@{
  Complex<T> operator+(T rhs) const {
    return Complex<T>(_real + rhs, _imag);
  }
  Complex<T> operator-(T rhs) const {
    return Complex<T>(_real - rhs, _imag);
  }
  Complex<T> operator*(T rhs) const {
    return Complex<T>(_real*rhs, _imag*rhs);
  }
  Complex<T> operator/(T rhs) const {
    return Complex<T>(_real/rhs, _imag/rhs);
  }

  Complex<T> & operator+=(T rhs) {
    _real += rhs;
    return *this;
  }
  Complex<T> & operator-=(T rhs) {
    _real -= rhs;
    return *this;
  }
  Complex<T> & operator*=(T rhs) {
    _real *= rhs;
    _imag *= rhs;
    return *this;
  }
  Complex<T> & operator/=(T rhs) {
    _real /= rhs;
    _imag /= rhs;
    return *this;
  }
  
  ///@}
private:
  T _real;
  T _imag;
};


/// arithmetic operators with real numbers on LHS and Complex on RHS
///@{
template<class T>
inline Complex<T> operator+(T a, const Complex<T>& b) {return b + a;}
template<class T>
inline Complex<T> operator-(T a, const Complex<T>& b) {return Complex<T>(a-b.real(),-b.imag());}
template<class T>
inline Complex<T> operator*(T a, const Complex<T>& b) {return b*a;}
template<class T>
inline Complex<T> operator/(T a, const Complex<T>& b) {
  T denom = b.norm();
  return Complex<T>((a*b.real())/denom,(-a*b.imag())/denom);
}
template<class T>
bool operator==(const Complex<T>& a, T b) {return a.real() == b && a.imag() == 0.0;}
template<class T>
bool operator==(T b, const Complex<T>& a) {return a.real() == b && a.imag() == 0.0;}
template<class T>
bool operator==(const Complex<T>& a, const Complex<T> & b) {return a.real() == b.real() && a.imag() == b.imag();}
template<class T>
bool operator!=(const Complex<T>& a, T b) {return a.real() != b || a.imag() != 0.0;}
template<class T>
bool operator!=(T b, const Complex<T>& a) {return a.real() != b || a.imag() != 0.0;}
template<class T>
bool operator!=(const Complex<T>& a, const Complex<T> & b) {return a.real() != b.real() || a.imag() != b.imag();}
///@}

template<class T>
inline std::ostream & operator<<(std::ostream & os, const Complex<T> & c) {
  os << "(" << c.real() << "," << c.imag() << ")";
  return os;
}


} // end namespace panscales

namespace std {

template<class T>
inline T real(const panscales::Complex<T> & z) { return z.real();}

template<class T>
inline T imag(const panscales::Complex<T> & z) { return z.imag();}

template<class T>
inline T norm(const panscales::Complex<T> & z) { return z.norm();}

template<class T>
inline T arg(const panscales::Complex<T> & z) { return std::atan2(z.imag(), z.real());}

template<class T>
inline panscales::Complex<T> conj(const panscales::Complex<T> & z) { return panscales::Complex<T>(z.real(), -z.imag());}

template<class T>
inline T abs(const panscales::Complex<T> & z) { return std::sqrt(norm(z));}
}

// std::ostream & operator<<(std::ostream & os, const panscales::Complex<double> & c) {
//   os << "(" << c.real() << "," << c.imag() << ")";
//   return os;
// }


#endif // __PANSCALES_COMPLEX_HH__
