//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef __SHOWERUSERDEFINED_HH__
#define __SHOWERUSERDEFINED_HH__

#include "ShowerBase.hh"

namespace panscales{

//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerUserDefined
/// Implementation of the [USER-TODO: insert name] shower algorithm.
///
/// [USER-TODO: insert description, reference, ...]
///
/// Comment from PanScales developers: the template below includes the
/// basic functionalities to write a new shower in the PanScales
/// framework. Additional features like ISR, matching, double-soft,...
/// require additional work. We refer to the documentation of the
/// ShowerBase class to see additional methods that can be overwritten
/// by specific showers.
class ShowerUserDefined : public ShowerBase {
public:
  /// default ctor
  ShowerUserDefined(const QCDinstance & qcd) : ShowerBase(qcd) {}

  /// virtual dtor
  virtual ~ShowerUserDefined() {}

  /// forward declare Element as subclass (see below for full definition)
  class Element;
  /// forward declare EmissionInfo as subclass (see below for full definition)
  class EmissionInfo;

  /// helper to allocate (and return) a pointer to
  /// ShowerUserDefined::EmissionInfo object (that class is defined
  /// below)
  ShowerBase::EmissionInfo* create_emission_info() const override;

  /// description of the class
  std::string description() const override {
    std::ostringstream ostr;
    ostr << name() << " user-defined shower (PanScales implementation)";
    return ostr.str();
  }
  /// name of the shower
  /// USER-TODO: give the shower an appropriate name
  std::string name() const override {return "UserDefined-ee";}

  /// meaning of kinematic variables USER-TODO: insert the name of the
  /// evolution variable (lnv) and auxiliary one (lnb). 
  std::string name_of_lnv() const override {return "unspecified";}
  std::string name_of_lnb() const override {return "unspecified";}

  /// Checks whether event-wide updates need to be performed after
  /// emitting a particle from a dipole
  /// USER-TODO: check if one has a dipole-local (the default) or global shower
  virtual bool is_global() const override { return false; }

  /// this is a dipole shower (2 elements per dipole) [an antenna shower would have 1]
  /// USER-TODO: check dipole (2) or antenna (1)
  unsigned int n_elements_per_dipole() const override { return 2; }

  /// allocates unique_ptrs for the "elements" associated with dipole
  /// index i in the event and returns them as a vector of unique_ptrs
  /// USER-TODO: this function is responsible for creating the elements 
  /// given a single dipole (two elements/dipole for a dipole shower,
  /// one element/dipole for an antenna shower)
  /// One should make sure the appropriate elements are created
  /// (see example code in cc)
  std::vector<std::unique_ptr<ShowerBase::Element>> elements(Event & event, int dipole_index) const override;

  /// The shower beta scaling: v = kt exp(beta*|eta|)
  /// kt ordering corresponds to beta=0, virtuality ordering to beta=1
  /// USER-TODO: specify the shower scaling
  double beta() const override {return 0.0;}

  /// true for dipole showers (only the emitter splits); false for
  /// antenna shower (emitter or spectator can split)
  /// USER-TODO: specify [true for dipole showers, false for antenna showers]
  bool only_emitter_splits() const override {return true;}
};


//--------------------------------------------------------------
/// \class ShowerUserDefined::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs for this shower algorithm and provides a set of 
/// member functions used by ShowerRunner to carry out the showering
class ShowerUserDefined::Element : public ShowerBase::Element {
public:
  /// dummy ctor
  Element() {};

  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerUserDefined * shower) :
    ShowerBase::Element(emitter_index, spectator_index, dipole_index, event),
    _shower(shower) {
    update_kinematics();
  }

  /// given that we have virtual functions in a number of places,
  /// we need a virtual destructor to avoid compiler warnings
  ~Element() override {}

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------

  /// For given shower variables, this returns the kt scale that is
  /// used to evaluate the strong coupling
  /// USER-TODO: fill-in the coupling scale [defaults to the approx lnkt]
  const double alphas_lnkt(double lnv, double lnb) const override {
    return lnkt_approx(lnv, lnb);
  }

  /// the beta value for the shower, (see ShowerUserDefined::beta())
  const double betaPS() const override { return _shower->beta(); }

  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------

  /// the lnb total extent, as a function of lnv, is parametrised
  /// as  const + lnv_deriv * lnv.
  /// USER-TODO: fill the constant and lnv_deriv coefficients
  ///            watch out: the lnv_coeff is usually negative
  double lnb_extent_const()     const override ;
  double lnb_extent_lnv_coeff() const override ;

  /// Returns the lnb range for the given lnv. The Range 
  /// class contains lower and upper limits. The extent of the range
  /// must coincide with  lnb_extent_const() + lnv * lnb_extent_lnv_coeff().
  ///
  /// In many cases the Range may be an overestimate of the
  /// kinematically allowed range, and acceptance_probability(...) below
  /// should be used to check if a given kinematic point is allowed.
  /// USER-TODO: for a given shower evolution
  /// scale lnv, this function needs to return
  /// the range over which the lnb auxiliary
  /// variable should be generated
  Range lnb_generation_range(double lnv) const override;

  /// returns the max density in lnv * lnb plane
  /// USER-TODO: This is the soft-collinear, large-Nc limit of the
  /// emission density. Note that it should use the maximum value of
  /// alphas (as running-coupling effects are taken into account
  /// elsewhere).
  ///
  /// For example, for a shower that uses kt as an evolution variable
  /// and rapidity as an auxiliary, the emission density in the
  /// soft-collinear & large-Nc limit is
  ///    alphas CA/pi
  /// so this would return
  ///    _shower->max_alphas() * _shower->qcd().CA() / M_PI;
  double lnv_lnb_max_density() const override;
  
  /// for a given lnv,lnb, returns approximate lnkt in the
  /// emitter-spectator dipole rest frame, which is equivalent to the
  /// frame-independent lnkt wrt to the emitter-spectator system.
  /// This lnkt will be correct in the soft+collinear limit but may
  /// differ from the true lnkt in the soft large-angle limit and the
  /// hard-collinear limit
  virtual double lnkt_approx(double lnv, double lnb) const override;


  /// USER-TODO: for a given lnv,lnb, this function returns
  /// the pseudorapidity with respect to
  /// the closer of the emitter/spectator, defined in the event
  /// centre-of-mass frame. This eta will be correct in the
  /// soft-collinear limit but may differ from the true eta in the
  /// soft large-angle limit and the hard-collinear limit.
  /// Typically, one would get an expression the rapidity wrt the emitter
  ///    double eta_emitter = ...;
  /// and a similar one wrt the spectator
  ///   double eta_spectator = ...;
  /// and then decide which side we are closer to, making sure that
  /// the positive end is closer to the emitter and the negative end
  /// closer to the spectator
  virtual double eta_approx(double lnv, double lnb) const override;

  /// USER-TODO: for a given lnv, returns the lnv for which eta_approx is the smallest  
  virtual double lnb_for_min_abseta(double lnv) const override;

  //---------------------------------------------------------------
  // important definitions to be implemented in all derived classed 
  // (i.e. when considering more than one dipole type)
  //---------------------------------------------------------------

  /// USER-TODO: 
  /// From the given lnv and lnb, "acceptance_probability(...)"
  ///   ( i) returns true or false depending on whether the phase-space is accessible or not
  ///   (ii) computes the ingredients needed to calculate the acceptance probability for the emission
  ///
  /// Step (ii) requires the following emission_info variables to be filled (defaults in bracket):
  ///    emission_info->emitter_weight_rad_gluon   [1.0]
  ///    emission_info->emitter_weight_rad_quark   [0.0]
  ///    emission_info->spectator_weight_rad_gluon [0.0]
  ///    emission_info->spectator_weight_rad_quark [0.0]
  ///    emission_info->z_radiation_wrt_emitter    [0.0]
  ///    emission_info->z_radiation_wrt_spectator  [0.0]
  ///
  /// The
  ///    _shower->fill_dglap_splitting_weights(<emitter>, <radiation z fraction>,
  ///                                          &<weight_gluon,
  ///                                          &<weight_quark);
  ///
  /// helper can be used to compute the weights based on our implementation of
  /// the DGLAP splitting kernels (more precisely, it computes zP(z) with z the
  /// momentum fraction of the emitted gluon or quark)
  /// For clarity a simple sketch is included in the cc file. 
  bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// USER-TODO: Based on the full kinematic set of
  /// variables (lnv, lnb, phi, and everything cached in
  /// acceptance_probability), choice of whether the emitter or
  /// spectator split, and flavour information, this method should
  /// compute the radiation, emitter_out and spectator_out members of
  /// emission info.
  ///
  /// Note that in order to allow for the use of directional
  /// differences, the mapping implemented here should use the
  /// momenta provided via "RotatedPieces rp, including
  ///   rp.emitter, rp.spectator, rp.perp1, rp.perp2
  /// A sketch is included in the cc file
  ///
  /// Although a bool, this function may not return
  /// false when turning on the implementation of the spin-correlations.
  /// That means that phase-space bounds should have been checked before
  /// running this function
  bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;

  // USER-TODO: 
  // For a global shower, one can also apply some operations at the end of the emission process.
  // In this case, one should make sure that one calls
  //    ShowerBase::Element::update_event(transition_runner, emission_info);
  /// which carries out everything that is needed for showers with dipole-local recoil. 
  // see the implementation of ShowerPanScaleGlobal for extra details
  // virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
  //                           typename ShowerBase::EmissionInfo * emission_info) override;

  //----------------------------------------------------
  // caching info to facilitate the work of ShowerRunner
  //----------------------------------------------------

  /// USER-TODO: this is the place where one should update the
  /// element's cached information (e.g. the dipole mass) when the
  /// element gets updated after a branching.
  virtual void update_kinematics() override;
  

  /// Some helpers to access information from the shower class.
  /// Note that this cannot go into the ShowerBase::Element class as
  /// only the derived classes hold a pointer to the shower.
  bool use_diffs() const override   {return _shower->use_diffs();}
  bool double_soft() const override {return _shower-> double_soft();}
  
private:
  const ShowerUserDefined * _shower;

  // USER-TODO: add potential variables to be cached for this shower's element
};

//--------------------------------------------------------------
/// \class ShowerUserDefined::EmissionInfo
/// emission information specific to the Pythia8 shower
class ShowerUserDefined::EmissionInfo : public ShowerBase::EmissionInfo {
public:
  // USER-TODO: cache the variables associated with an emission (e.g. z, ...)
  //
  // These should e.g. include variables that will be cached by
  // "acceptance_probability" and then used in order to reconstruct
  // the full splitting kinematics in "do_kinematic".
  // Note that some common information --- e.g. lnv, lnb and phi ---
  // is already available from the base class.
  precision_type z;
};
  
} // namespace panscales

#endif // __SHOWERUSERDEFINED_HH__
