//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerToyPythia8.hh"

namespace panscales{

  //----------------------------------------------------------------------
  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerToyPythia8::elements(
    Event & event, int dipole_index) const {
    int i3    = event.dipoles()[dipole_index].index_3()   ;
    int i3bar = event.dipoles()[dipole_index].index_3bar();

    assert(!event.particles()[i3].is_initial_state() && !event.particles()[i3bar].is_initial_state() && 
      "Pythia8 only handles final-state showers");
      
    std::unique_ptr<typename ShowerBase::Element> elms[] = {
      std::unique_ptr<typename ShowerBase::Element>(new ShowerToyPythia8::Element(i3, i3bar, dipole_index, &event, this)),
      std::unique_ptr<typename ShowerBase::Element>(new ShowerToyPythia8::Element(i3bar, i3, dipole_index, &event, this))
    };
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }
  ShowerBase::EmissionInfo* ShowerToyPythia8::create_emission_info() const{ return new ShowerToyPythia8::EmissionInfo(); }

  //----------------------------------------------------------------------
  Range ShowerToyPythia8::Element::lnb_generation_range(double lnv) const {
    // return something that corresponds to ln(zmin=rho_evol) < ln(1-z) < 0
    // or equivalently 0 < z < 1-rho_evol, where
    // rho_evol^2 = exp(2*lnv)/_dipole_m2
    return Range(lnv - 0.5 * to_double(log_T(_dipole_m2)), 0.0);
  }

  Range ShowerToyPythia8::Element::lnb_exact_range(double lnv) const {
    // #TRK_ISSUE-499  
    // - changed things here to use T for intermediate results
    // - not clear what should be done for zmin >= 1/2 (should it not be
    //   Range(-ln2,-ln2)?)
    precision_type zmin = sqrt(rhoevol2(precision_type(lnv)));
    //
    // added preprocessor if ... else ... endif to get compilation
    // working in QDREAL and DDREAL. Code _totally_unchanged_ for compilation in
    // double. Reminder log1p(x) = log(1+x) [http://www.cplusplus.com/reference/cmath/log1p/].
    // (And to date this function is never even called by anything).
#if !defined(PSDDREAL) && !defined(PSQDREAL)
    return Range(to_double(log_T(zmin)), to_double(log1p(-zmin)));
#else
    return Range(to_double(log_T(zmin)), to_double(log(1.-zmin)));
#endif
  }

  double ShowerToyPythia8::Element::lnv_lnb_max_density() const {
    // For now we work in a large-NC limit, with CF defined to be equal
    // to CA/2; the density that is returned here corresponds to that
    // for a quark or for half a gluon
    return _shower->max_alphas() * _shower->qcd().CA() / M_PI;
  }

  //----------------------------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  bool ShowerToyPythia8::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerToyPythia8
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerToyPythia8::EmissionInfo & emission_info = *(static_cast<typename ShowerToyPythia8::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;
  
    // We will often be working with 1-z close to 0, because (1-z) is
    // the gluon momentum fraction.  To retain precision, we explicitly
    // introduce a 1-z variable called omz, and use that wherever
    // we would otherwise call 1-z
    precision_type omz = exp(precision_type(lnb));
    precision_type z = 1.0 - omz;

    precision_type r2 = rhoevol2(precision_type(lnv));

    // cache this info for future usage
    emission_info.omz = omz;
    emission_info.z   = z;
    emission_info.r2  = r2;
  
    // the following conditions are taken from Eqs. (2.18) & (2.28) of nll-shower-notes.pdf
    // original formulation (Eq.2.9 in arXiv:1805.09327)
    // kt2_orig = (pow2(z) - r2)*(pow2(omz) - r2)/pow2(z*omz - r2)*exp((precision_type) 2.0*lnv);
    // triggers over/underflow
    // rewrite in steps
    precision_type zomz_minus_r2 = z*omz - r2;
    precision_type kt2 = ((pow2(z) - r2)*(pow2(omz) - r2)
                          /zomz_minus_r2) / zomz_minus_r2 * exp((precision_type) 2.0*lnv);

    // if outside the kinematic range, then return false
    if (kt2 < 0 || r2 > 0.25 || kt2 != kt2) {return false;}

    // The code below makes a decision about flavour. Keep in mind
    // that What we implement below for g->qqbar is closer to what the
    // PanScales showers do than when Pythia8 actually do.
    //
    // Note that the following lines include 2 extra things:
    //  - we have dln(1-z) as our integration measure so need
    //    to include a dz/dln(1-z) factor here
    //  -  we use lnv = 0.5*ln(t) as our evolution variable, so
    //    include a factor of two in the density
    //
    // Since we have already handled the CF|CA colour factors, here we
    // use the normalised splitting functions and assume a CA factor for
    // both quarks and gluons.
    _shower->fill_dglap_splitting_weights(emitter(), omz,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();

    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter   = omz;
    emission_info.z_radiation_wrt_spectator = 0.;

    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;

    return true;
  }


  //----------------------------------------------------------------------
  ///
  bool ShowerToyPythia8::Element::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerToyPythia8
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerToyPythia8::EmissionInfo & emission_info = *(static_cast<typename ShowerToyPythia8::EmissionInfo*>(emission_info_base));

    // retrieve cached info
    const double & phi = emission_info.phi;
    const precision_type & omz = emission_info.omz;
    const precision_type & z   = emission_info.z;
    const precision_type & r2  = emission_info.r2;

    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);

    // from Eq. (2.6) of arXiv:1805.09327
    precision_type ybcr = r2/z/omz;

    // Sanity check based on the denominator Eq. (2.14) of nll-shower-notes.pdf
    if (ybcr > 1.0) return false;

    // from Eq. (2.6) of arXiv:1805.09327
    precision_type ztilde = omz*(pow2(z) - r2)/(z*omz - r2);
    precision_type omztilde = z*(pow2(omz) - r2)/(z*omz - r2);
  
    // get the Pythia8 actions for the transverse parts
    // the transverse normalisation is given by masslessness condition
    // for (A.4a)
    precision_type norm_perp2 = 2 * dot_product(rp.spectator,rp.emitter)
                                 * ztilde * omztilde * ybcr;
    if (norm_perp2 < 0) return false;
    precision_type norm_perp = sqrt(norm_perp2);

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp * (sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    emission_info.k_perp = perp;

    // Eqs. (2.15) - (2.17) of nll-shower-notes.pdf
    // pb = emitter
    // pc = radiation
    // pr = spectator
    // Note: we use p3 in the operations here to avoid going via special "mass preserving" operations
    // (and mass tests) which bring no benefit in a case where the final mass should be zero.
    Momentum3<precision_type> emitter_out3   = ztilde     * rp.emitter.p3() + (ybcr * omztilde) * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> radiation3     = omztilde   * rp.emitter.p3() + (ybcr * ztilde  ) * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> spectator_out3 = (1 - ybcr) * rp.spectator.p3();

    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    return true;
  }

  void ShowerToyPythia8::Element::update_kinematics() {
    if (use_diffs()) {
      _dipole_m2 = 2*dot_product_with_dirdiff(emitter(), spectator(), dipole().dirdiff_3_minus_3bar); 
    } else {
      _dipole_m2 = (emitter() + spectator()).m2();
    }
  }


  //----------------------------------------------------------------------
  // update the element indices and kinematics
  void ShowerToyPythia8::Element::update_indices(unsigned emitter_index, unsigned spectator_index) {
    _emitter_index   = emitter_index;
    _spectator_index = spectator_index;
    update_kinematics();
  }

  //------------------------------------------------------------------------
  // for a given lnv,lnb, returns the pseudorapidity with respect to
  // the closer of the emitter/spectator, defined in the event
  // centre-of-mass frame. This eta will be correct in the
  // soft+collinear limit but may differ from the true eta in the
  // soft large-angle limit and the hard-collinear limit.
  double ShowerToyPythia8::Element::eta_approx(double lnv, double lnb) const {
    // take formulas from dire, including use of kappa -> rho2evol
    //
    // work out eta wrt the emitter, keeping in mind that lnb is 0 for a hard
    // collinear emission, so eta there should be given by log(2E/kt) where
    // E is the emitter energy in the event centre-of-mass frame.
    // Decreasing lnb (i.e. going to negative lnb) decreases the rapidity.
    double eta_emitter = -lnv + to_double(log_T(2.0*dot_product(emitter(), _event->Q())/_event->Q().m())) + lnb;
    // similar formula wrt spectator, keeping in mind a map lnb ->
    // log(rho2evol) - lnb gives us the symmetric point in lnb (swapping
    // spectator/emitter)
    double eta_spectator = -lnv + to_double(log_T(2.0*dot_product(spectator(), _event->Q())/_event->Q().m()))
      + (to_double(log_T(rhoevol2(lnv)))-lnb);

    // now decide which of the rapidities is smaller and return that
    // one, signing things such that we're positive if closer to the
    // emitter negative otherwise.
    if (eta_emitter > eta_spectator) return eta_emitter;
    else                             return -eta_spectator;
  }  

  double ShowerToyPythia8::Element::lnb_for_min_abseta(double lnv) const {
    // we need to work out the lnb value for which eta_emitter and
    // eta_spectator are equal in the eta_approx function
    return 0.5*(to_double(log_T(2.0*dot_product(spectator(), _event->Q())/_event->Q().m()))
                + to_double(log_T(rhoevol2(lnv)))
                - to_double(log_T(2.0*dot_product(emitter(), _event->Q())/_event->Q().m())));
  }


  
} // namespace panscales
