//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include "alphas.hh"
#include <gsl/gsl_sf_zeta.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>
#include <functional>
#include <iostream>
#include <sstream>
#include <cmath>

namespace panscales{

  namespace internal{  
    int gsl_unwrap_ode_func(double t, const double y[], double f[], void *p) {
      auto fp = static_cast<std::function<int(double t, const double y[], double f[])> *>(p);
      return (*fp)(t, y, f);
    }
  }

  //----------------------------------------------------------------------
  // implementation of AlphasBase
  //----------------------------------------------------------------------
  // default ctor
  //  - nloops_in        number of loops
  //  - alphasMSbarQ_in  strong coupling in the MSbar scheme at the scale Q
  //  - lnQ_in           logarithm od the reference scale Q
  //  - lnkt_cutoff_in   kt scale below which we set alphas to 0
  AlphasBase::AlphasBase(unsigned int nloops_in, double alphasMSbarQ_in,
                         double lnQ_in, double lnkt_cutoff_in)
    : _nloops(nloops_in), _alphasMSbarQ(alphasMSbarQ_in),
      _lnQ(lnQ_in), _lnkt_cutoff(lnkt_cutoff_in){
    _use_CMW = true;
    _global_CMW = false;
    _K = _K2 = _effective_A3 = 0.0;
  }

  // possibilitty to handle QCD colour factors.
  // since this is the case for all options except the fixed-coupling
  // one, return true by default
  void AlphasBase::set_qcd_constants(double CF_in, double CA_in, double TR_in){
    _CF = CF_in;
    _CA = CA_in;
    _TR = TR_in;
    _check_and_update_if_needed();
  }

  // possibilitty to handle mass thresholds.
  void AlphasBase::set_ln_mass_thresholds(double lnmc_in, double lnmb_in){
    _lnmc = lnmc_in;
    _lnmb = lnmb_in;
    if ((_lnmc>_lnkt_cutoff) && (_lnmb>_lnkt_cutoff))
      assert((_lnmb>=_lnmc) && "The charm mass is not allowed to be larger than the bottom quark");
    _check_and_update_if_needed();
  }

  // returns the value of alphas at a given scale (no cutoff imposed)
  // At two or three loops, if the CMW flag is on, then it is the physical
  // (CMW-scheme) coupling that is returned.
  //
  // We make this virtual so that we can include mass threshold
  // effects in our treatment of KCMW in classes below
  double AlphasBase::alphas_no_cutoff(double lnkt) const{
    // in the current code,
    assert(((_effective_A3==0) || (_global_CMW))
           && "an effective A3 is only supported w global K_CMW");
    if (!_global_CMW) return _alphas_calc(lnkt, _use_CMW);
    double a = _alphas_calc(lnkt, false);
    if (!_use_CMW){ return a; }
    double a2pi = a/(2*M_PI);
    double cmw_factor = 0.0;
    if (_nloops>=3) cmw_factor = a2pi * cmw_factor + _K2;
    if (_nloops>=2) cmw_factor = a2pi * cmw_factor + _K;    
    return a * (1+a2pi*cmw_factor+_effective_A3*a*a);
  }

  //----------------------------------------------------------------------
  // implementation of AlphasRunning
  //----------------------------------------------------------------------
  // ctor
  //  - nloops_in        number of loops
  //  - alphasMSbarQ_in  strong coupling in the MSbar scheme at the scale Q
  //  - lnQ_in           logarithm od the reference scale Q
  //  - lnkt_cutoff_in   kt scale below which we set alphas to 0
  //  - nf_in            (fixed) number of active flavours
  // 
  AlphasRunning::AlphasRunning(unsigned int nloops_in, double alphasMSbarQ_in,
                               double lnQ_in, double lnkt_cutoff_in,
                               unsigned int nf_in)
    : AlphasBase(nloops_in, alphasMSbarQ_in, lnQ_in, lnkt_cutoff_in){
    // Full QCD by default
    _CA = 3.0;
    _CF = 4.0/3.0;
    _TR = 0.5;
    _nf = nf_in;
    
    _use_CMW = true;
    
    // other initialisations
    _check_and_update_if_needed();
    _check_cutoff_validity();
  }
  
  // description
  std::string AlphasRunning::description() const{
    std::ostringstream ostr;
    if(_nloops == 0){
      ostr << "fixed alpha_s = " << _alphasMSbarQ;
    } else {
      ostr << "(" << _nloops << "-loop) alpha_s^MSbar(lnQ=" << _lnQ << ") = " << _alphasMSbarQ;
    }
    ostr << ", QCD constants[CF=" << _CF << ", CA=" << _CA << ", TR=" << _TR 
         << ", nf=" << _nf << "]";
    if (_use_CMW)
      ostr << ", CMW scheme returned for alphas(lnkt) calls "
           << (_global_CMW ? "(included globally)" : "(included locally)");
    return ostr.str();
  }
  
  // actual calculation 
  double AlphasRunning::_alphas_calc(double lnkt, bool include_CMW) const{
    if (_nloops==3){ // following Eqs. 3.17-3.19 in 1807.11487
      double tmpmo = 2*_alphasMSbarQ*_b0*(lnkt-_lnQ);
      double tmp   = 1+tmpmo;
      double lntmp = log(tmp);
      double Keff  = include_CMW ? _K  : 0.0;
      double K2eff = include_CMW ? _K2 : 0.0;
      double as1 = _alphasMSbarQ/tmp;
      // Note: Eq. 3.19 has a typo in the last term  (misplaced squared bracket)
      return as1 * (1.0
                    + as1 * ((-_b1/_b0*lntmp + 0.5*Keff/M_PI)
                             + as1 * (-_b2/_b0*tmpmo + pow2(_b1/_b0)*(tmpmo+(lntmp-1)*lntmp)
                                      + K2eff/pow2(2*M_PI) - _b1*Keff/(_b0*M_PI)*lntmp)
                             )
                    ); 
    }
    if (_nloops==2){
      double tmp  = 1+2*_alphasMSbarQ*_b0*(lnkt-_lnQ);
      double Keff = include_CMW ? _K : 0.0;
      return _alphasMSbarQ/tmp*(1.0+_alphasMSbarQ/tmp*(0.5*Keff/M_PI-_b1/_b0*log(tmp)));
    }

    if (_nloops==1) return _alphasMSbarQ/(1+2*_alphasMSbarQ*_b0*(lnkt-_lnQ));
    if (_nloops==0) return _alphasMSbarQ;
    
    assert(false && "AlphasRunning: nloops should be either 0, 1, 2 or 3");
    return 0.0;
  }

  // update the coefficients of the beta function and cusp anomalous
  // dimension needed for the running of the coupling
  void AlphasRunning::_check_and_update_if_needed(){
    // update the running coupling coefficients
    double zeta3 = gsl_sf_zeta(3);
    double TF = _nf*_TR;
    _b0 = (11*_CA-4*_nf*_TR)/(12*M_PI);
    _b1 = (17*_CA*_CA-10*_CA*_nf*_TR-6*_CF*_nf*_TR)/(24*M_PI*M_PI);
    _b2 = (2857*pow3(_CA) + (54*pow2(_CF)-615*_CF*_CA-1415*pow2(_CA))*2*TF + (66*_CF + 79*_CA)*pow2(2*TF))/(3456*pow3(M_PI));
    _K  = (67.0/18.0-M_PI*M_PI/6.0)*_CA - 10.0/9.0*_nf*_TR;
    _K2 = (245.0/24.0-67.0*M_PI*M_PI/54.0+11.0/6.0*zeta3+11.0*M_PI*M_PI*M_PI*M_PI/180.0)*_CA*_CA 
        + (-55.0/24.0+2*zeta3)*_CF*_nf 
        + (-209.0/108.0 + 5.0*M_PI*M_PI/27.0 - 7.0*zeta3/3.0)*_CA*_nf
        - _nf*_nf/27.0 + 0.5*M_PI*_b0*(_CA*(808.0/27.0-28*zeta3)-224.0*_nf/54.0);

    _check_cutoff_validity();
  }

  // check if the requested lnkt_cutoff is above the Landau pole
  void AlphasRunning::_check_cutoff_validity() const{
    if (_nloops==0) return;

    // make sure that the cutoff is at a lower kt than the hard scale
    assert((_lnkt_cutoff<_lnQ) && "AlphasRunning: with running coupling, the cutoff scale should be smaller than the reference scale Q");

    // make sure that the cutoff is above the Landau pole
    if (1+2*_alphasMSbarQ*_b0*(_lnkt_cutoff-_lnQ)<=0){
      double min_cutoff = _lnQ-1/(2*_alphasMSbarQ*_b0);
      std::ostringstream oss;
      std::cerr << "AlphasRunning: the alphas cutoff should be above the Landau pole (min cutoff="
                << min_cutoff << ")" << std::endl;
      assert(1+2*_alphasMSbarQ*_b0*(_lnkt_cutoff-_lnQ)>0);
    }
  }

  
  //----------------------------------------------------------------------
  // implementation of AlphasRGRunning
  //----------------------------------------------------------------------
  // ctor
  AlphasRGRunning::AlphasRGRunning(unsigned int nloops_in, double alphasMSbarQ_in,
                                   double lnQ_in, double lnkt_cutoff_in,
                                   unsigned int nf_in)
    : AlphasRunning(nloops_in, alphasMSbarQ_in, lnQ_in, lnkt_cutoff_in, nf_in),
      _tabulated_one_over_alphas_below(4), _tabulated_one_over_alphas_above(4){
    assert(nloops_in==3 && "AlphasRGRunning only implemented for 3-loop running at the moment");
    _update_interp_table();
  }

  // description
  std::string AlphasRGRunning::description() const{
    std::ostringstream ostr;
    ostr << "(" << _nloops << "-loop) alpha_s^MSbar(lnQ=" << _lnQ << ") = " << _alphasMSbarQ << " (from RGE)";
    ostr << ", QCD constants[CF=" << _CF << ", CA=" << _CA << ", TR=" << _TR << "]";
    if (_use_CMW)
      ostr << ", CMW scheme returned for alphas(lnkt) calls "
           << (_global_CMW ? "(included globally)" : "(included locally)");
    return ostr.str();
  }


  // update of the interpolation table used
  void AlphasRGRunning::_update_interp_table(){
    double b0 = (_nloops>=1) ? _b0 : 0.0;
    double b1 = (_nloops>=2) ? _b1 : 0.0;
    double b2 = (_nloops>=3) ? _b2 : 0.0;
    // tabulate below
    //
    // We're going down to lnkt_cutoff (or just one step below to keep
    // the dlnkt step fixed))
    std::function<int(double, const double y[], double f[])> func_below =
      [&](double, const double y[], double f[]) -> int{
        // we want the derivative wrt log(Q/kt) => factor -2 [-1 for backwars, 2 for kt^2->kt]
        f[0] = -2.0*(b0 + (b1 + b2/y[0])/y[0]);
        return GSL_SUCCESS;
      };    
    gsl_odeiv2_system sys = {internal::gsl_unwrap_ode_func, nullptr, 1, (void*) &func_below};
    gsl_odeiv2_driver * d = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd,
                                                          _tabulation_lnkt_spacing, 1e-9, 0.0);

    unsigned int n = std::ceil((_lnQ-_lnkt_cutoff)/_tabulation_lnkt_spacing);
    _n_tabulated_below = n+1;
    _tabulated_lnkt_below.resize(n+1);
    _tabulated_one_over_alphas_below.resize(n+1);

    _tabulated_lnkt_below           [0] = _lnQ;
    _tabulated_one_over_alphas_below[0] = 1/_alphasMSbarQ;

    double t = 0;
    double dt = _tabulation_lnkt_spacing; //< note that the stepsize has to be +ve
    double y[1] = {1/_alphasMSbarQ};

    for (unsigned int i=1; i<=n; ++i){
      double ti = t + dt;
      int status = gsl_odeiv2_driver_apply (d, &t, ti, y);
      assert(status == GSL_SUCCESS);
      _tabulated_lnkt_below           [i] = _lnQ-ti;
      _tabulated_one_over_alphas_below[i] = y[0];
      t=ti;
    }
    gsl_odeiv2_driver_free (d);

    // tabulate above
    std::function<int(double, const double yv[], double f[])> func_above =
      [&](double, const double yv[], double f[]) -> int{
        // we want the derivative wrt log(kt/Q) => factor 2 [for kt^2->kt]
        f[0] = 2*(b0 + (b1 + b2/yv[0])/yv[0]);
        return GSL_SUCCESS;
      };    
    sys = {internal::gsl_unwrap_ode_func, nullptr, 1, (void*) &func_above};
    d = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd,
                                      _tabulation_lnkt_spacing, 1e-9, 0.0);
    
    n = std::ceil(_upper_lnkt_margin/_tabulation_lnkt_spacing);
    _n_tabulated_above = n+1;
    _tabulated_lnkt_above.resize(n+1);
    _tabulated_one_over_alphas_above.resize(n+1);

    _tabulated_lnkt_above           [0] = _lnQ;
    _tabulated_one_over_alphas_above[0] = 1/_alphasMSbarQ;

    t = _lnQ;
    y[0] = 1/_alphasMSbarQ;
    
    for (unsigned int i=1; i<=n; ++i){
      double ti = t+dt;
      int status = gsl_odeiv2_driver_apply (d, &t, ti, y);
      assert(status == GSL_SUCCESS);
      _tabulated_lnkt_above[i] = ti;
      _tabulated_one_over_alphas_above[i] = y[0];
      t=ti;
    }
    gsl_odeiv2_driver_free (d);
  }


  // actually calculate alphas
  double AlphasRGRunning::_alphas_calc(double lnkt, bool include_CMW) const{
    if (lnkt<_lnkt_cutoff){ return 0.0;}

    auto alphas_from_inverse = [&] (double inverse) -> double{
      double a = 1/inverse;
      return include_CMW ? a*(1+a/(2*M_PI)*(_K + a/(2*M_PI)*_K2)) : a;
    };
    
    // tabulated "above"
    if (lnkt>=_lnQ){
      double x = (lnkt-_lnQ)/_tabulation_lnkt_spacing;
      return alphas_from_inverse(_tabulated_one_over_alphas_above(x));
    }
    double x = (_lnQ-lnkt)/_tabulation_lnkt_spacing;
    return alphas_from_inverse(_tabulated_one_over_alphas_below(x));
  }


  //--------------------------------------------------------------------------------
  // Alphas5VFS implementation
  Alphas5VFS::Alphas5VFS(unsigned int nloops_in, double alphasMSbarQ_in, double lnQ_in, double lnkt_cutoff_in,
                         double lnmc_in, double lnmb_in, bool use_rg)
    : AlphasBase(nloops_in, alphasMSbarQ_in, lnQ_in, lnkt_cutoff_in), _use_rg(use_rg){

    assert((_nloops>0) && "Use AlphasRunning with nloops = 0 for fixed-coupling evolution");

    set_ln_mass_thresholds(lnmc_in, lnmb_in);
    // the previous call triggers a check+update automatically, so no
    // longer needed here
    //_check_and_update_if_needed();
    _CF = _alphas5->_CF;
    _CA = _alphas5->_CA;
    _TR = _alphas5->_TR;
  }

  // description
  std::string Alphas5VFS::description() const{
    std::ostringstream ostr;
    ostr << "(" << _nloops << "-loop) alpha_s^MSbar(lnQ=" << _lnQ << ") = " << _alphasMSbarQ
         << ", with variable number of active flavour at thresolds ln(mb) = " << _lnmb
         << ", and ln(mc) = " << _lnmc;
    if (_use_rg)  ostr << ", underlying fixed-nf using the renormalisation-group equation";
    else          ostr << ", underlying fixed-nf using the resummation expression";
    ostr << ", QCD constants[CF=" << _CF << ", CA=" << _CA << ", TR=" << _TR << "]";
    if (_use_CMW) ostr << ", CMW scheme returned for alphas(lnkt) calls";
    if (_global_CMW) ostr << " (applied globally)";
    else             ostr << " (applied locally)";
    if (_effective_A3>0) ostr << ", with effective A3=" << _effective_A3;
    return ostr.str();
  }
    
  // relay info to individual pieces
  void Alphas5VFS::set_use_CMW(bool use_CMW_in){
    _use_CMW = use_CMW_in;
    if (_alphas3) _alphas3->set_use_CMW(use_CMW_in);
    if (_alphas4) _alphas4->set_use_CMW(use_CMW_in);
    _alphas5->set_use_CMW(use_CMW_in);
  }
  void Alphas5VFS::set_global_CMW(bool global_CMW_in){
    _global_CMW = global_CMW_in;
    if (_alphas3) _alphas3->set_global_CMW(global_CMW_in);
    if (_alphas4) _alphas4->set_global_CMW(global_CMW_in);
    _alphas5->set_global_CMW(global_CMW_in);
  }
  void Alphas5VFS::set_effective_A3_CMW(double effective_A3_in){
    _effective_A3 = effective_A3_in;
    if (_alphas3) _alphas3->set_effective_A3_CMW(effective_A3_in);
    if (_alphas4) _alphas4->set_effective_A3_CMW(effective_A3_in);
    _alphas5->set_effective_A3_CMW(effective_A3_in);
  }

  // check if things need to be updated
  void Alphas5VFS::_check_and_update_if_needed(){
    // with CMW corection, we set the transition at a scale
    //   mstar = e^{5/6} m
    // where 5/6 = 1/2 1/(2pi) (K_{n+1}-K_n)/(b_{0,n+1}-b_{0,n})
    //
    // Also, shifting the threshold by +offset is equivalent to
    // shifting lnkt by -offset.

    _alphas5.reset(_create_alphas_fixed_nf(_alphasMSbarQ, _lnQ, 5));
    if (_lnmb>_lnkt_cutoff){
      _alphas4.reset(_create_alphas_fixed_nf(_alphas5->alphasMSbar(_lnmb), _lnmb, 4));
      _lnmb_CMW = (_use_rg)
        ? _compute_alphasCMW_thresholds(_alphas5.get(), _alphas4.get(), _lnmb)
        : _lnmb + 5/6.0;
    }
    if (_lnmc>_lnkt_cutoff){
      _alphas3.reset(_create_alphas_fixed_nf(_alphas4->alphasMSbar(_lnmc), _lnmc, 3));
      _lnmc_CMW = (_use_rg)
        ? _compute_alphasCMW_thresholds(_alphas5.get(), _alphas4.get(), _lnmc)
        : _lnmc + 5/6.0;
    }
  }

  // simple tool to create the right instance for the underlying
  // classes handling the running
  AlphasRunning * Alphas5VFS::_create_alphas_fixed_nf(double alphasQ, double lnQ,
                                                      unsigned int nf) const{
    if (_use_rg) return new AlphasRGRunning(_nloops, alphasQ, lnQ, _lnkt_cutoff, nf);
    return new AlphasRunning(_nloops, alphasQ, lnQ, _lnkt_cutoff, nf);
  }

  // compute the point at which alphas_CMW is continuous
  double Alphas5VFS::_compute_alphasCMW_thresholds(AlphasRunning *alphas_above,
                                                   AlphasRunning *alphas_below,
                                                   double lnm) const{
    if (!_use_CMW) return lnm;

    // K_above<K_below
    // => as_above<as_below at lnm
    double lmax = lnm+1.0;
    while (alphas_above->alphas(lmax) < alphas_below->alphas(lmax)) lmax += 1.0;

    double lmin = lmax-1.0;
    do {
      double l = 0.5*(lmin+lmax);
      if (alphas_above->alphas(l) < alphas_below->alphas(l)) lmin = l;
      else                                                   lmax = l;
    } while (lmax-lmin>1e-8);

    return 0.5*(lmin+lmax);
  }

  // check if the lnkt_cutoff is above the Landau pole
  void Alphas5VFS::_check_cutoff_validity() const{
    _alphas5->_check_cutoff_validity();
    if (_alphas4) _alphas4->_check_cutoff_validity();
    if (_alphas3) _alphas3->_check_cutoff_validity();
  }

  // returns one of _alphas{3,4,5} depending on which one is relevant
  // at the scale lnkt
  const AlphasRunning * Alphas5VFS::_get_alphas_object_at_scale(double lnkt, bool include_CMW) const{
    double lnb = include_CMW ? _lnmb_CMW : _lnmb;
    double lnc = include_CMW ? _lnmc_CMW : _lnmc;
    
    // 5 flavour region
    if (lnkt > lnb) return _alphas5.get();

    // 4 flavour region (make sure the region exists)
    if (lnkt > lnc){
      if (_alphas4) return _alphas4.get();
      return _alphas5.get();
    }

    // 3 flavour region (make sure the region exists)
    if (_alphas3) return _alphas3.get();
    if (_alphas4) return _alphas4.get();
    return _alphas5.get();
  }

  //----------------------------------------------------------------------
  // implementation of AlphasPowhegStyleVFS
  //----------------------------------------------------------------------
  // description
  std::string AlphasPowhegStyleVFS::description() const{
    std::ostringstream ostr;
    ostr << "(" << _nloops << "-loop) alpha_s^MSbar(lnQ=" << _lnQ << ") = " << _alphasMSbarQ
         << ", with old (hard-coded) inplementation of flavour thresholds";
    if (_use_CMW) ostr << ", CMW scheme returned for alphas(lnkt) calls";
    return ostr.str();
  }

  
  // actual calculation of alphas
  double AlphasPowhegStyleVFS::_alphas_calc(double lnkt, bool include_CMW) const{
    double xlam = 0.239;  //lambda5MSB
    double xlq = 2*(lnkt -log(xlam));
    double xllq = log(xlq);
    //hvq (=charm and bottom) masses
    double xmc = 1.51;
    double xmb = 4.92;
    //b0 and b1 with 5,4,3 flavours
    double b5 = (33. -2*5.)/M_PI/12;
    double bp5 = (153. - 19.*5) / M_PI / 2 / (33 - 2*5);
    double b4= (33.-2.*4.)/M_PI/12.;
    double bp4 = (153. - 19.*4) / M_PI / 2 / (33 - 2*4);
    double b3= (33.-2*3.)/M_PI/12;
    double bp3 = (153. - 19.*3) / M_PI / 2 / (33 - 2*3);
    // xlq= 2*log(mq/lambda)
    double xlc = 2 * log(xmc/xlam);
    double xlb = 2 * log(xmb/xlam);
    // xxlq =log(xlq)
    double xllc = log(xlc);
    double xllb = log(xlb);
    // matching coefs to have continuity at the threshold 
    double c45  =  1./( 1./(b5 * xlb) - xllb*bp5/pow2(b5 * xlb) ) - 1./( 1./(b4 * xlb) - xllb*bp4/pow2(b4 * xlb) );
    double c35  =  1./( 1./(b4 * xlc) - xllc*bp4/pow2(b4 * xlc) ) - 1./( 1./(b3 * xlc) - xllc*bp3/pow2(b3 * xlc) ) + c45;
  
    if (_nloops==0) return _alphasMSbarQ;
    
    //switch off two-loop running 
    if (_nloops==1){
      bp5 = bp4 = bp3 = 0;
    }
    
    double alphas;
    if (xlq>xlb){
      alphas = 1/(b5 * xlq) -  bp5/pow2(b5 * xlq) * xllq;
      if(include_CMW && _nloops ==2 ) alphas*=(1.+alphas/(2*M_PI)*((67./18-pow2(M_PI)/6)*_CA-5./9*5));
    } else if (xlq>xlc){
      alphas = 1./( 1./(1./(b4 * xlq) - bp4/pow2(b4 * xlq) * xllq) + c45 );
      if(include_CMW && _nloops ==2 ) alphas*=(1.+alphas/(2*M_PI)*((67./18-pow2(M_PI)/6)*_CA-5./9*4));
    } else {
      alphas = 1./( 1./(1./(b3 * xlq) - bp3/pow2(b3 * xlq) * xllq) + c35 );
      if(include_CMW && _nloops ==2 ) alphas*=(1.+alphas/(2*M_PI)*((67./18-pow2(M_PI)/6)*_CA-5./9*3));
    }
    return alphas;
  }


} // namespace panscales
