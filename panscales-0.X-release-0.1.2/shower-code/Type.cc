//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "Type.hh"
#include <limits>
#include <cmath>
#include <sstream>
using namespace std;

///
/// Here we fill in the information numeric_limit_extras, which is a
/// class that plays the role of numeric_limits, but is made slightly
/// more robust across different numeric types
///

#if defined(PSQUAD) // for libquadmath, use hardcoded value of sqrt(epsilon) etc

template <>
double numeric_limit_extras<precision_type>::sqrt_epsilon_ =
  1.38777878078144567552953958511352539e-17;
template <>
double numeric_limit_extras<precision_type>::epsilon_ =
  1.92592994438723585305597794258492732e-34;
template <> 
precision_type numeric_limit_extras<precision_type>::infinity_ =
  precision_type(1.0)/precision_type(0.0);
template <> 
precision_type numeric_limit_extras<precision_type>::max_ =
  precision_type(1.18973149535723176508575932662800702e4932Q);

#else

//------ first, the quantities that return double, with precision_type
//       as their template argument
// template <> 
// double numeric_limit_extras<precision_type>::sqrt_epsilon_ =
//   to_double(sqrt(numeric_limits<precision_type>::epsilon()));
// template <> 
// double numeric_limit_extras<precision_type>::epsilon_ =
//   to_double(numeric_limits<precision_type>::epsilon());
// template <>
// double numeric_limit_extras<precision_type>::log_max_ =
//   to_double(log(std::numeric_limits<precision_type>::max()));

template <> 
precision_type numeric_limit_extras<precision_type>::sqrt_epsilon_ =
  (sqrt(numeric_limits<precision_type>::epsilon()));
template <> 
precision_type numeric_limit_extras<precision_type>::epsilon_ =
  (numeric_limits<precision_type>::epsilon());
template <>
double numeric_limit_extras<precision_type>::log_max_ =
  to_double(log(std::numeric_limits<precision_type>::max()));

//----- then the quantities where we need to be able to
//      take not just precision type but also double as
//      as the template argument (don't re-define these
//      if precision type is double)
#ifndef PSDOUBLE
template <> 
double numeric_limit_extras<double>::sqrt_epsilon_ =
  sqrt(numeric_limits<double>::epsilon());
template <> 
double numeric_limit_extras<double>::epsilon_ =
  numeric_limits<double>::epsilon();
template <>
double numeric_limit_extras<double>::log_max_ =
  log(std::numeric_limits<double>::max());
#endif // not defined(PSDOUBLE)

//----- finally, define infinity and max for specific cases
//      of precision type
#if defined(PSDDREAL)
//------------- dd_real case -------------------
template <> 
dd_real numeric_limit_extras<precision_type>::infinity_ =
  dd_real(numeric_limits<double>::infinity(),
          numeric_limits<double>::infinity());
template <> 
dd_real numeric_limit_extras<dd_real>::max_ =
  dd_real(1.79769313486231570815e+308, 9.97920154767359795037e+291);
#elif defined(PSQDREAL)
//------------- qd_real case -------------------
template <> 
qd_real numeric_limit_extras<precision_type>::infinity_ =
  qd_real(numeric_limits<double>::infinity(),
          numeric_limits<double>::infinity(),
          numeric_limits<double>::infinity(),
          numeric_limits<double>::infinity());
template <> 
qd_real numeric_limit_extras<precision_type>::max_ =
  qd_real(1.79769313486231570815e+308, 9.97920154767359795037e+291, 
          5.53956966280111259858e+275, 3.07507889307840487279e+259);
#else
template <> 
precision_type numeric_limit_extras<precision_type>::infinity_ =
  numeric_limits<precision_type>::infinity();
template <> 
precision_type numeric_limit_extras<precision_type>::max_ =
  numeric_limits<precision_type>::max();
#endif

#endif


#if defined(PSDDREAL) || defined(PSQDREAL)
template <>
init_type<precision_type>::init_type()  {fpu_fix_start(&old_cw);}
template <>
init_type<precision_type>::~init_type() {fpu_fix_end  (&old_cw);}
#else
template <>
init_type<precision_type>::init_type()  {}
template <>
init_type<precision_type>::~init_type() {}
#endif

/// returns a header with information about the precision type
/// being used. Each line of the header has the specified prefix
std::string precision_header(const std::string & prefix) {
  ostringstream ostr;
  // first get a string for the precision mode
  string precmode;
#if defined PSQUAD
  precmode = "quadmath";
#elif defined PSDDREAL
  precmode = "dd real";
#elif defined PSQDREAL
  precmode = "qd real";
#elif defined PSARPREC // Not implemented
  precmode = "arprec";
#elif defined PSDOUBLEEXP
  precmode = "double_exp";
#else
  precmode = "standard double";
#endif
  ostr << prefix << "precision mode = " << precmode << "\n";
  precision_type test = sqrt(precision_type(2.0));
  ostr << prefix 
       << "precision estimate [2 - pow(sqrt(2.0),2)] = " << 2 - test*test 
       << ", epsilon = " << numeric_limits<precision_type>::epsilon()
       << ", min = " << numeric_limits<precision_type>::min()
       << ", max = " << numeric_limits<precision_type>::max()
       << endl;
  return ostr.str();
}

