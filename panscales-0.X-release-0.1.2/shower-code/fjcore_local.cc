//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
// This file is provided to work around the issue that fjcore comes 2 as sets
// of files:
//
//   fjcore.{hh,cc}            for double (or float) precision types
//   fjcore_convert.{hh,cc}    for other precision types (e.g. double_exp, qd)
//
// The choice between the 2 files is decided based on pre-processor
// PSFJCORECONVERT variable, which is automatically set in config.hh,
// through the CMake build system. 
//
// #TRK_ISSUE-707  GS-Note:
//   I think we could work with fjcore_convert provided we define
//   "to_double(double)" in the case where double is used. This could
//   be done directly i fjcore.
#include "config.hh"

#if defined PSFJCORECONVERT
#  include "fjcore_convert.cc"
#else
#  include "fjcore.cc"
#endif
