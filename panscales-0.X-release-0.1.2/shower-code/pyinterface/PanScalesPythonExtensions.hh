#ifndef __PANSCALES_PYTHON_EXTENSIONS_HH__
#define __PANSCALES_PYTHON_EXTENSIONS_HH__

#include <memory>
#include "ShowerRunner.hh"
#include "ShowerFromCmdLine.hh"
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>

// write a small tool that creates a ShowerRunner based on a string of
// arguments
//
// This requires
//
//   %shared_ptr(panscales::ShowerRunner)
//
// to appear close to the top of the interface file (before the
// %{... %} block)
namespace panscales{
  shared_ptr<ShowerRunner> _internal_pypanscales_shower_runner;
  shared_ptr<HoppetRunner> _internal_pypanscales_hoppet_runner;

  //std::shared_ptr<ShowerRunner> create_runner(const std::string & options){
  ShowerRunner & create_runner(const std::string & options){
    std::string full_options = options;
    // convert the option string into a CmdLine object
    std::vector<std::string> tokens;
    std::istringstream iss(full_options);
    std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(),
              std::back_inserter(tokens));
    CmdLine cmdline(tokens);


    double f_rts      = cmdline.value("-rts",1.0);
    double f_lnvmax   = cmdline.value("-lnvmax", log(f_rts));

    // make a dummy header
    std::ostringstream header;

    //return std::shared_ptr<ShowerRunnerBase>(create_class_with_shower<ShowerRunnerBase>(cmdline, header));
    //return std::shared_ptr<ShowerRunner>(create_class_with_shower(cmdline, header));

    _internal_pypanscales_shower_runner =  std::shared_ptr<ShowerRunner>(create_class_with_shower(cmdline, header));
    _internal_pypanscales_hoppet_runner =  std::shared_ptr<HoppetRunner>(new HoppetRunner());
    _internal_pypanscales_hoppet_runner->initialise(_internal_pypanscales_shower_runner->shower()->qcd());
    _internal_pypanscales_hoppet_runner->set_scales_mapping(false, f_lnvmax ,
                                          _internal_pypanscales_shower_runner->shower()->qcd().alphasMSbar(f_lnvmax),
                                          _internal_pypanscales_shower_runner->shower()->qcd().nloops());
    
    _internal_pypanscales_shower_runner->set_PDF_runner(_internal_pypanscales_hoppet_runner.get());

    std::cout << header.str() << std::endl;

    return *_internal_pypanscales_shower_runner;
  }

  //%include <typemaps.i>
  //%apply double& OUTPUT { double& weight };
  double run_runner(Event & event, double lnv, double lnv_min,
                    bool weighted_generation, double dynamic_lncutoff, int nmax=-1){
    double weight = 1.0;
    _internal_pypanscales_shower_runner->run(event, lnv, lnv_min, weight,
                                             weighted_generation, dynamic_lncutoff, nmax);
    return weight;
  }
}



#endif // __PANSCALES_PYTHON_EXTENSIONS_HH__
