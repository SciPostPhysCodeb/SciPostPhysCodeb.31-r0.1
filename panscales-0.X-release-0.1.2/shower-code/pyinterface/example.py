import panscales #importing the module that has been created by SWIG
from panscales import Momentum3, Momentum, dot_product, cross, one_minus_costheta #makes our life easier

#declare momenta a and b
a = Momentum(1.,1.,0,1.)
b = Momentum(-1.,0,1,2.)

print(a)

print("The four-vector (px,py,pz,E) of a = ("+str(a.px())+","+str(a.py())+","+str(a.pz())+","+str(a.E())+")")
print("The mass of the particle a is "+str(a.m()))
print("The four-vector (px,py,pz,E) of b = ("+str(b.px())+","+str(b.py())+","+str(b.pz())+","+str(b.E())+")")
print("The mass of the particle b is "+str(b.m()))
print("The dot_product product a.b="+str(dot_product(a,b)))
c = cross(a,b)
print("Their cross product is the three-vector c= ("+str(c.px())+","+str(c.py())+","+str(c.pz())+")")
print("one_minus_costheta "+str(one_minus_costheta(a,b)))
d = Momentum(0,0,0,b.m())
e = b.boost(d)
print("Check if the boost works. We define d = (0,0,0,b.m()). Then b.boost(d) should give b. ")
print("Result: b.boost(d) = ("+str(e.px())+","+str(e.py())+","+str(e.pz())+","+str(e.E())+")")
f = e.unboost(b)
print("Result: (b.boost(d)).unboost(b) = ("+str(f.px())+","+str(f.py())+","+str(f.pz())+","+str(f.E())+")")


print("")
print("Testing operator overloading:")
f = a + b
print("a + b = ("+str(f.px())+","+str(f.py())+","+str(f.pz())+","+str(f.E())+")")
f = a-  b
print("a - b = ("+str(f.px())+","+str(f.py())+","+str(f.pz())+","+str(f.E())+")")
g = Momentum(a.px(),a.py(),a.pz(),a.E())
g = a/3.
print("a/3 = ("+str(g.px())+","+str(g.py())+","+str(g.pz())+","+str(g.E())+")")
a = a*3.
print("a*3 = ("+str(a.px())+","+str(a.py())+","+str(a.pz())+","+str(a.E())+")")
print(f"a[0] = {a[0]}, a[1] = {a[1]}, a[2] = {a[2]}, a[3] = {a[3]}")

print("")
print("Testing the Particle class:")
p = panscales.Particle(a, 2)
print (f"p = ({p.momentum()}), p.pdgid={p.pdgid()}")

