#!/usr/bin/env python3
"""
Script to demonstrate access to shower runner, using a string of arguments
"""
import panscales as ps
import copy

def main(writeout=True):

    # create a shower runner
    runner = ps.create_runner("-shower panglobal")
    if writeout: print(runner.description())

    # Alternatively, one can create an AnalysisFramework and access the
    # shower runner inside it
    #   an=ps.AnalysisFramework(cmdline)
    #   runner=an.shower_runner()

    # specify the initial event
    p1=ps.Particle(ps.Momentum(0, 0, +0.5, 0.5),+1)
    p2=ps.Particle(ps.Momentum(0, 0, -0.5, 0.5),-1)
    event=ps.Event(p1,p2)

    if writeout: print(event)

    runner.do_one_emission_with_fixed_lnv_lnb(event, 0, -5, 2.0, 0.4)
    if writeout: print(event)
    

if __name__ == '__main__': main()
