// -*- C++ -*-
/* File : panscales.i */

%define DOCSTRING
"Python interface to the PanScales ee shower code.

Fot the moment, only a few classes are wrapped:
 - all momentum classes
 - Event and friends (Particle, Dipole, ...)

All the classes which are templated over the precision type are
defined here without the template argument (using the specific
procision_type used to build the panscale lib)
"
%enddef

%module(docstring=DOCSTRING) panscales

%include "std_string.i"
%include "std_vector.i"
%include "std_shared_ptr.i"
%shared_ptr(panscales::ShowerRunnerBase)
%shared_ptr(panscales::ShowerRunner)
//[mvb] old version of above:
//%shared_ptr(panscales::ShowerRunnerBase)
//note the difference in the template brackets!
//without those the error
// "use of undeclared identifier 'smartarg1'
//  (void)arg1; delete smartarg1;"
// is produced

//%include "smartptr.i"

%{
#include <sstream>

#include "config.hh"
#include "Momentum3.hh"
#include "Momentum4.hh"
#include "MomentumM2.hh"
#include "Momentum.hh"
#include "LimitedWarning.hh"

#include "ColourTransitions.hh"
#include "Event.hh"
#include "CmdLine.hh"

#include "Tensor.hh"

#include "SpinCorrelations.hh"

#include "HoppetRunner.hh"
#include "ShowerRunner.hh"
#include "AnalysisFramework.hh"
#include "ShowerFromCmdLine.hh"

#include "fjcore_convert.hh"
#include "fjcore.hh"
#include "fjcore_local.hh"
#include "DISCambridge.hh"

#include "PanScalesPythonExtensions.hh"

%}


// the list of headers we want to wrap
%include "config.hh"
%include "Type.hh"
%include "Momentum3.hh"
namespace panscales{
  // This leads to some
  //   warning 315: Nothing known about 'panscales::Momentum3< double >::px_
  // If we instead replace precision_type w double, these warnings go
  // away. So it looks like Momentum3<precision_type> is only properly
  // interpreted as Momentum3<double> at compilation/link time
  //
  // It also appears that the warnings are harmless so we leave things
  // as they are for the moment
  %template(Momentum3) Momentum3<precision_type>;
}
%include "Momentum4.hh"

%include "MomentumM2.hh"
%include "Momentum.hh"

// declare the templates we want
namespace panscales{
  // At the moment, we have not been able to reuse the typedef'ed
  // Momentum class.  Instead, we define it manually
  #if PANSCALES_MOMENTUM == 4
  %template(Momentum) Momentum4<precision_type>;
  #elif PANSCALES_MOMENTUM == 2
  %template(Momentum4) Momentum4<precision_type>;
  %template(Momentum) MomentumM2<precision_type>;
  #endif

  %template(dot_product) dot_product<precision_type>;
  %template(cross) cross<precision_type>;
  %template(one_minus_costheta) one_minus_costheta<precision_type>;
  %template(twoPerp) twoPerp<precision_type>;
  %rename(__iadd__) operator+=;
  %rename(__isub__) operator-=;
  %rename(__imul__) operator*=;
  %rename(__itruediv__) operator/=;

  // Here it's not 100% clear why we need to extend both Momentum4 and MomentumM2
  // It may again have to do with the use of templates and of precision_type
  //
  // We extend several things:
  //  - math operations
  //  - [] operator (in get mode)
  //  - printing (<< used for __str__())
  %extend Momentum4<precision_type>
  {
    Momentum4<precision_type> __add__(const Momentum4<precision_type>& rhs) { return operator+(*$self, rhs); }
    Momentum4<precision_type> __sub__(const Momentum4<precision_type>& rhs) { return operator-(*$self, rhs); }
    Momentum4<precision_type> __truediv__(const double& rhs) { return operator/(*$self, rhs); }
    Momentum4<precision_type> __mul__(const double& rhs) { return operator*(rhs, *$self); }

    precision_type __getitem__(int i) const{
      return (*$self)[i];
    }

    std::string __str__() const{
      std::ostringstream o;
      o << *$self;
      return o.str();
    }
  };

  %extend MomentumM2<precision_type>
  {
    MomentumM2<precision_type> __add__(const MomentumM2<precision_type>& rhs) { return operator+(*$self, rhs); }
    MomentumM2<precision_type> __sub__(const MomentumM2<precision_type>& rhs) { return operator-(*$self, rhs); }
    MomentumM2<precision_type> __truediv__(const double& rhs) { return operator/(*$self, rhs); }
    MomentumM2<precision_type> __mul__(const double& rhs) { return operator*(rhs, *$self); }

    precision_type __getitem__(int i) const{
      return (*$self)[i];
    }

    std::string __str__() const{
      std::ostringstream o;
      o << *$self;
      return o.str();
    }
  };
}

// This is needed by the Dipole class in Event.hh
//
// It is not clear if it can be used in python code but we do not need
// it at the moment
%include "ColourTransitions.hh"

// we can finally wrap the classes in Event
%include "Event.hh"
namespace panscales{

  %extend Event {
      std::string __str__() const{
        std::ostringstream o;
        o << *$self;
        return o.str();
      }
  };
}

// a few helpers
%template(VectorParticles) std::vector<panscales::Particle>;
%template(VectorDipoles)   std::vector<panscales::Dipole>;

// add the CmdLine tools to the interface as well
%template(StringVector) std::vector<std::string>;
%include "CmdLine.hh"
%include "LimitedWarning.hh"
// try to get Analysis tools
%include "QCD.hh"
%include "Range.hh"
%include "Tensor.hh"
%include "EmissionVeto.hh"
%include "MatchedProcess.hh"
%include "ShowerBase.hh"
%include "SpinCorrelations.hh"
%include "HoppetRunner.hh"
%include "ShowerRunner.hh"
%include "ShowerFromCmdLine.hh"
%include "AnalysisBase.hh"
%include "AnalysisFramework.hh"
%ignore fjcore::LimitedWarning;
%ignore fjcore::dot_product;
%include "fjcore.hh"
%include "fjcore_convert.hh"
%include "fjcore_local.hh"
%include "DISCambridge.hh"


// 
// // apparently swig does not work well w references
// //
// // so convert them as outputs
// namespace panscales{
//   %include <typemaps.i>
//   
//   %apply double& OUTPUT { double& weight };
//   void ShowerRunner::run(Event & event, double lnv, double lnv_min, double & weight,
//                          bool weighted_generation, double dynamic_lncutoff, int max_emsn = -1);
// }
// 

%include "PanScalesPythonExtensions.hh"
