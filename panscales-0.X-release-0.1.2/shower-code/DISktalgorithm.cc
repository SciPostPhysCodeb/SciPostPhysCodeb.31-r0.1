//FJSTARTHEADER
//
// Copyright (c) 2007-2018, Matteo Cacciari, Gavin P. Salam and Gregory Soyez
//
//----------------------------------------------------------------------
//  This file is distributed as part of PanScales but is HEAVILY based 
//  on a part of FastJet.
//
//  FastJet is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  The algorithms that underlie FastJet have required considerable
//  development. They are described in the original FastJet paper,
//  hep-ph/0512210 and in the manual, arXiv:1111.6097. If you use
//  FastJet as part of work towards a scientific publication, please
//  quote the version you use and include a citation to the manual and
//  optionally also to hep-ph/0512210.
//
//  FastJet is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with FastJet. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------
//FJENDHEADER

// fastjet stuff
#include "DISktalgorithm.hh"

// other stuff
#include <sstream>
#include <limits>

using namespace std;
using namespace panscales;

string DISktalgorithm::description () const {
  ostringstream desc;
  desc << "DISktalgorithm plugin with dcut = " << dcut() ;
  return desc.str();
}

void DISktalgorithm::run_clustering(fjcore::ClusterSequence & cs) const {
  // step A:
  // cluster them all into a beam jet + final-state macro jets
  // for every final-state parton we compute
  // d_kp = 2(1-cos(theta_kp)) E_k^2 / E_t^2, where E_t^2 is a hard scale to be defined
  // here theta_kp = the angle of the parton to the initial-state hadron direction (in Breit frame)
  // d_kl = 2(1-cos(theta_kl)) min(E_k^2, E_l^2)/E_t^2
  // then get the smallest value of all d_kp, d_kl -> if d_ij is the smallest, make a new pseudoparticle (p_ij = p_i + p_j)
  // otherwise include p_i in the beam jet (one fat object)
  // one ends the clustering when all d_kp, d_kl found are larger than 1. We have beam jets + many final-state macro jets
  // in the cluster sequence we may see the substructure of the final-state macro jets (step B)

  // we start with the list of pseudopartons given to us 
  int njets = cs.jets().size();
  // construct the list of normalised objects
  fjcore::NNH<DISktBriefJet> nnh(cs.jets());
  // now we cluster into a collection of beam and parton jets
  // std::cout << "Starting out with njets = " << njets << std::endl;
  while (njets > 0) {
    int i, j, k;
    // get the minumum
    precision_type dij = nnh.dij_min(i, j); // i,j are return values
    // Et^2 does not matter here as it is just a normalising factor
    // std::cout << "Found the minimum: " << dij << ", i = " << i << ", j = " << j << std::endl;
    // if j > 0 then we merge
    if ((j >= 0)) { 
      if((dij < _dcut) || (_dcut < 0)){//_dcut < 0 is inclusive
        // std::cout << "Recombining i and j " << std::endl;
        cs.plugin_record_ij_recombination(i, j, dij, k);
        nnh.merge_jets(i, j, cs.jets()[k], k);
        // std::cout << "Merged into k " << std::endl;
      } else{
        // exclusive jet
        cs.plugin_record_iB_recombination(i, dij);
        nnh.remove_jet(i);
      }
    } else {
      // beam jet recombination (disregard it)
      // std::cout << "Beam recomb - disregard it!" << std::endl;
      cs.plugin_record_iB_recombination(i, dij);
      nnh.remove_jet(i);
    }
    njets--;
  }

  // for(int i = 0; i < cs.jets().size(); i++){
  //   std::cout << "jets i = " << i << " " << cs.jets()[i].px() << ", "<< cs.jets()[i].py() << ", "<< cs.jets()[i].pz() << ", "<< cs.jets()[i].E() <<std::endl;
  // }
  // for(int i = 0; i < cs.inclusive_jets().size(); i++){
  //   std::cout << "inclusive jets i = " << i << " " << cs.jets()[i].px() << ", "<< cs.jets()[i].py() << ", "<< cs.jets()[i].pz() << ", "<< cs.jets()[i].E() <<std::endl;
  // }
  // for(int i = 0; i < cs.exclusive_jets().size(); i++){
  //   std::cout << "exclusive jets i = " << i << " " << cs.jets()[i].px() << ", "<< cs.jets()[i].py() << ", "<< cs.jets()[i].pz() << ", "<< cs.jets()[i].E() <<std::endl;
  // }
  // step B - resolving the jet structure
  // this involves the resolution parameter d_cut = Q^2/E_t^2
  // then only look at the particles inside a final-state macro-jet
  // consider d_ij and define final-state jets as:
  // calculate y_kl = 2(1-cos(theta_kl))*min(Ek^2, E_l^2)/Q^2
  // if min(.....y_kl) < y_cut -> combine those two partons, if all y_kl > y_cut the algorithm terminates
}
//
