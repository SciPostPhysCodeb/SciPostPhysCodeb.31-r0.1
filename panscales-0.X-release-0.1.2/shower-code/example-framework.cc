//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "AnalysisFramework.hh"
#include "FastY3.hh"
#include <iomanip>

using namespace std;
using namespace panscales;


class ExampleFramework : public AnalysisFramework {
public:
  bool do_obs;
  bool do_evshp;
  bool do_frag;
  bool do_allyn;
  bool cam_use_WTA, cam_use_diffs; 
  bool print_checksum;
  FastY3 fasty3;
  
  ExampleFramework(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in) {}

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {


    cmdline->start_section("Choice of observables"); //---------

    do_obs = ! cmdline->present("-no-obs").help("turn off calculation of observables");
    do_evshp = cmdline->present("-do-evshp").help("add calculation of event shapes");
    do_frag = cmdline->present("-do-frag").help("add calculation of fragmentation functions");
    do_allyn = cmdline->present("-do-allyn").help("outputs a histogram of all yn values");
    
    cam_use_WTA = cmdline->present("-cam-use-WTA").help("use WTA in Cambridge jet clustering");
    cam_use_diffs = cmdline->present("-cam-use-diffs").help("use direction differences in Cambridge jet clustering");
    // other consistency conditions required if using direction
    // differences within the Cambridge algorithm.
    if (cam_use_diffs) assert(cmdline->present("-use-diffs") && cam_use_WTA);
    fasty3 = FastY3(cam_use_WTA, cam_use_diffs);


    cmdline->end_section("Choice of observables"); //-----------

    cmdline->section("Diagnostics");
    print_checksum = cmdline->present("-print-checksum")
                      .help("print a checksum for each event, e.g. to help diagnose "
                            "where event sequences start to differ between runs on two different systems");
    cmdline->end_section("Diagnostics");



    if (do_frag) {
      Binning frag_binning(-10.0, 1e-8, 0.2);
      hists_err["lnx_gluon"].declare(frag_binning);
      hists_err["lnx_qqbar"].declare(frag_binning);
    }

    // #TRK_ISSUE-705  some default binning range -- we will think more about this later...
    this->set_default_binning(-2*f_lnvrange, 0.0, f_lnvrange/20.0);
  }

  /// dd_real and qd_real return NaN when the argument of the
  /// logarithm is zero; that differs from the behaviour in double
  /// precision (which returns -infinity), so implement that
  /// behaviour here
  precision_type safe_log(precision_type x) const {
    if (x == 0) return -numeric_limit_extras<precision_type>::infinity();
    else        return log(x);
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    //cout << f_event << endl;
    double evwgt = event_weight();
    //test_dirdiff_matrix(f_event);
    if (do_frag) {
      for (const auto & p: f_event.particles()) {
        double lnx = to_double(log(2*p.E()/f_event.Q().E()));
        if (p.pdgid() == 21) hists_err["lnx_gluon"].add_entry(lnx, evwgt);
        else                 hists_err["lnx_qqbar"].add_entry(lnx, evwgt);
      }
    }
    if (!do_obs) return;
    
    fasty3.analyse(f_event);
    cumul_hists_err["lny3"].add_entry(to_double(safe_log(fasty3())), double(evwgt));
    if (do_allyn) {
      //cout << "iev = " << iev << endl;
      const fjcore::ClusterSequence & cs = fasty3.cs();
      for (unsigned i = 1; i < f_event.particles().size(); i++) {
        hists["all_ln_yn"].add_entry(to_double(safe_log(cs.exclusive_ymerge(i))), evwgt);
        //cout << i << " " << setprecision(8) << to_double(safe_log(cs.exclusive_ymerge(i))) << endl;
      }
    }

    // a few event shapes just to check things look sensible
    if (do_evshp) {
      auto obs_broad = Broadening(true); 
      obs_broad.analyse(f_event);
      //obs_broad.list();
      hists_err["evshp:1-T"].set_lims_add_entry(0.0, 0.5, 0.01, to_double(1.0-obs_broad.thrust()), evwgt);
      hists_err["evshp:BT" ].set_lims_add_entry(0.0, 0.5, 0.01, to_double(obs_broad.total()     ), evwgt);
      hists_err["evshp:BW" ].set_lims_add_entry(0.0, 0.5, 0.01, to_double(obs_broad.wide_jet()  ), evwgt);
      auto obs_cparam = CParam(); 
      obs_cparam.analyse(f_event);
      hists_err["evshp:CParam"].set_lims_add_entry(0.0, 1.0, 0.02, to_double(obs_cparam()), evwgt);
    }

    if (print_checksum) {
      precision_type checksum = 0;
      precision_type norm = 1.0;
      for (const auto & p: f_event.particles()) {
        checksum += norm*(log(p.E())/8.78 + p.px()/1.2 + p.py()/1.3 + p.pz()/1.4 + p.pdgid() / 11.23);
        norm *= 0.97;
      }
      int prec = cout.precision(7);
      cout << "iev = " << iev       
           << ", evwgt = " << evwgt
           << ", checksum = " << checksum 
           << ", mult = " << f_event.particles().size()
           << ", lny3 = " << safe_log(fasty3())
           << endl;
      cout.precision(prec);
    }
  }
  

//  void test_dirdiff_matrix(const Event & event) {
//    //cout << event << endl;
//    const double cambridge_ycut = 1.0;
//    fjcore::JetDefinition jd1(new fjcore::EECambridgeFastPlugin(cambridge_ycut));
//    jd1.delete_plugin_when_unused();
//    jd1.set_recombination_scheme(fjcore::WTA_modp_scheme);
//    fjcore::ClusterSequence cs1(event.particles(), jd1);
//
//    // set things up for clustering with dirdiff
//    FastY3 fasty3_diffs(true, true); 
//    fasty3_diffs.analyse(event);
//    const fjcore::ClusterSequence & cs2 = fasty3_diffs.cs();
//
//    for (unsigned i = 1; i < event.particles().size(); i++) {
//      double y1 = cs1.exclusive_ymerge(i);
//      double y2 = cs2.exclusive_ymerge(i);
//      
//      cout << setw(3)  << i << " " 
//           << setw(15) << y1 
//           << setw(15) << y2;
//      if (abs(y1-y2) > 1e-10 * max(y1,y2)) cout << "  DIFF=" << y1-y2;
//      cout << endl;
//    }
//    //event.print_following_dipoles();
//    //for (unsigned i = 0; i < event.size(); i++) {
//    //  for (unsigned j = 0; j < event.size(); j++) { 
//    //    double d1 = pairwise(i,j);
//    //    double d2 = dot_product(event[i],event[j]) / event[i].E() / event[j].E();
//    //    cout << i << " " << j << " " 
//    //         << setw(12) << d1 << " " 
//    //         << setw(12) << d2;
//    //    if (abs(d1 - d2) > 1e-10) cout << " BAD";
//    //    cout << endl;
//    //  }
//    //}
//  }

};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv,true);
  ExampleFramework driver(&cmdline);
  driver.run();
}
