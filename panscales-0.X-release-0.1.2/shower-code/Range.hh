//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __PANSCALES_RANGE_HH__
#define __PANSCALES_RANGE_HH__

#include <iostream>
#include <string>
#include <vector>
#include "Type.hh"

namespace panscales{


//-------------------------------------------------------------------
/// \class Range
/// Holds a range, and helps manipulate it
class Range {
public:
  Range(double min_in, double max_in) : min_(min_in), max_(max_in) {}
  double min()    const {return min_;}
  double max()    const {return max_;}
  /// returns the extent of the range, bounded to be at least zero
  double extent() const {double e = max_-min_; return e > 0 ? e : 0;}
  double point(int i, int n) const {return min_ + i*(max_-min_)/n;}
  double point(double x)     const {return min_ + x*(max_-min_);}

  /// returns true if the value is contained in the range
  bool contains(double value) const {return min_ <= value && value <= max_;}
  
  /// returns a set of points within the range with mid_spacing in the bulk;
  /// in the neighbourhood it decreases the spacing by a factor of two as one
  /// approaches the edge, down to a minimum spacing of edge_spacing
  std::vector<double> points_hd_edge(double mid_spacing, double edge_spacing) const;
  
  /// returns a new range, refined with respect to the original so
  /// that all points in it satisfy is_good() == true and that it
  /// approaches to within epsilon of the edge of the range where
  /// is_good() is true.
  ///
  /// It requires the quarter,mid or three-quarter point to be is_good
  /// in order for the search to start.
  template <class T>
  Range refine_inner(const T& is_good, double epsilon=1e-4) const{
    // look for a point "mid" that satisfies is_good(mid) == true (it doesn't
    // need to be a mid-point). For now just consider a limited range of
    // options.
    auto mids = {(min_+max_)/2, 0.25*min_+0.75*max_, 0.75*min_+0.25*max_};
    double mid;
    bool found_good = false;
    for (auto midval: mids) {
      if (is_good(midval)) {
        found_good = true;
        mid = midval;
        break;
      }
    }
    // if not found, then give up, with a range that has the limits inverted
    // (zero total extent)
    if (!found_good) return Range (max_,min_);
  
    // refine the minimum end of the range; is_good(rh) should always be
    // true, while is_good(lh) should always be false; when they are
    // within epsilon of each other, stop and use rh as the new minimum
    double lh = min_, rh=mid;
    while (rh-lh > epsilon) {
      double new_mid = (lh+rh)/2;
      if (is_good(new_mid)) {
        rh = new_mid;
      } else {
        lh = new_mid;
      }
    }
    double new_min = rh;
  
    // refine the maximum end in a similar way
    lh = mid;
    rh = max_;
    while (rh-lh > epsilon) {
      double new_mid = (lh+rh)/2;
      if (is_good(new_mid)) {
        lh = new_mid;
      } else {
        rh = new_mid;
      }
    }
    double new_max = lh;
    return Range(new_min, new_max);
  }

  
private:
  double min_, max_;
};

inline std::ostream & operator<<(std::ostream & ostr, const Range & range) {
  ostr << "[" << range.min() << "," << range.max() << "]";
  ostr << "(" << range.extent() << ")";
  return ostr;
} 


} // namesace panscales

#endif // __PANSCALES_RANGE_HH__
