//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __QCD_HH__
#define __QCD_HH__

#include <sstream>
#include <cassert>
#include <cmath>
#include "extra-math.hh"
#include "alphas.hh"
#include <limits>
#include "Momentum.hh"

namespace panscales{

/// \file QCD.hh
/// This file contains the code that allows to access the values of
/// of QCD-related parameters and also defines the splitting functions. 

//------------------------------------------------------------------------
/// \enum ColourScheme
/// possible options for treating colour
enum class ColourScheme{
  CFHalfCA = 0,   ///< no subleading colour treatment (sets CF to CA/2 = 3/2)
  CATwoCF  = 1,   ///< no subleading colour treatment (sets CA to 2CF = 8/3)
  Segment  = 2,   ///< subleading Nc using the segment method (cite arXiv:2011.10054)
  NODS     = 3,   ///< subleading colour from the NODS method (cite arXiv:2011.10054)
  CFFE     = 4,   ///< colour factor deduced from the emitter
};
  

//----------------------------------------------------------------------
/// \class QCDinstance 
/// class that initialises all QCD related parameters including: 
/// the number of loops for the running of the strong coupling and its
/// initial value, the shower starting and ending scales, the heavy-quark
/// (charm and bottom) masses, the number of active flavours and the 
/// possibility to incldue mass-thresholds in the running of the coupling
/// together with using the soft limit of the splitting functions.  

class QCDinstance {
public:
  // /// ctor for fixed coupling
  // ///
  // /// see _init_defaults() below for the default setup
  // QCDinstance(double alphas_in, double nf_in = 5.0, precision_type mc_in = 0, precision_type mb_in = 0, 
  //             bool set_pure_soft_splitting = false, bool mass_thresholds = false ) 
  //   : _nloops(0), _alphasMSbarQ(alphas_in), _lnQ(0.0), _lnkt_cutoff(1.0),
  //     _nf(nf_in), _mc(mc_in), _mb(mb_in), _soft_limit(set_pure_soft_splitting), 
  //     _mass_thresholds(mass_thresholds) {
  //   _init_defaults();
  // }

  /// ctor for generic coupling
  ///
  /// see _init_defaults() below for the default setup
  ///
  /// Note: when the cutoff scale is put at a value larger than the
  /// reference scale, we discard the cutoff
  QCDinstance(unsigned int nloops_in, double alphasMSbarQ_in, double lnQ_in, double lnkt_cutoff_in,
              double nf_in = 5.0, precision_type mc_in = 0, precision_type mb_in = 0, 
              bool set_pure_soft_splitting = false, bool mass_thresholds = false ) 
    : _nloops(nloops_in), _alphasMSbarQ(alphasMSbarQ_in), _lnQ(lnQ_in), _lnkt_cutoff(lnkt_cutoff_in),
      _nf(nf_in), _soft_limit(set_pure_soft_splitting), 
      _mass_thresholds(mass_thresholds) {
    _init_defaults();
    set_mb(mb_in); //< WATCH OUT: IT IS CRUCIAL TO SET mb FIRST
    set_mc(mc_in); //< SO THAT WE KEEP mb>mc
  }


  //----------------------------------------------------------------------
  // methods to set/retrieve the behaviour
  //----------------------------------------------------------------------
  /// set/get basic QCD constants
  void set_basic_constants(double CF_in, double CA_in, double nf_in, double TR_in){
    _CF = CF_in;
    _CA = CA_in;
    _nf = nf_in;
    _TR = TR_in;
    _check_casimirs_update_betas();
  }
  void set_CF(double CF_in){ _CF = CF_in; _check_casimirs_update_betas();}
  void set_CA(double CA_in){ _CA = CA_in; _check_casimirs_update_betas();}
  void set_nf(double nf_in){ _nf = nf_in; _create_alphas(); _check_casimirs_update_betas();}
  void set_TR(double TR_in){ _TR = TR_in; _check_casimirs_update_betas();}
  void set_mc(precision_type mc_in){
    _mc   = mc_in;
    _lnmc = (_mc>0) ? to_double(log(_mc)) : -numeric_limit_extras<precision_type>::log_max();
    _check_casimirs_update_betas();
  }
  void set_mb(precision_type mb_in){
    _mb   = mb_in;
    _lnmb = (_mb>0) ? to_double(log(_mb)) : -numeric_limit_extras<precision_type>::log_max();
    _check_casimirs_update_betas();
  }
  void set_lnmc(double lnmc_in){
    _mc   = exp(precision_type(lnmc_in));
    _lnmc = lnmc_in;
    _check_casimirs_update_betas();
  }
  void set_lnmb(double lnmb_in){
    _mb   = exp(precision_type(lnmb_in));
    _lnmb = lnmb_in;
    _check_casimirs_update_betas();
  }

  // use a coupling that runs based on an explicit solution of the QCD
  // RG equation instead of the analytic expresions valid for
  // "resummation". Beware that this is work in progress.
  void set_alphas_use_rg(bool value){
    _alphas_use_rg = value;
    _create_alphas();
  }
  bool alphas_use_rg() const { return _alphas_use_rg; }

  // switch to/from the powheg-style alphas (only used for running
  // coupling w mass thresholds)
  void set_powheg_alphas(bool value){
    _use_powheg_alphas = value;
    _create_alphas();
  }
  bool powheg_alphas() const { return _use_powheg_alphas; }

  // further tweaks of the RG
  void set_effective_A3_CMW(double A3_in=0.0){
    _alphas->set_effective_A3_CMW(A3_in);
  }
  void set_global_CMW(bool global_CMW = true){
    _alphas->set_global_CMW(global_CMW);
  }
  
  // accessors
  double CF() const { return _CF;}
  double CA() const { return _CA;}
  double nf() const { return _nf;}
  double TR() const { return _TR;}
  /// return the coefficient of alpha^2 in the QCD RGE (i.e. (11CA-4*TR*nf)/(12pi))
  /// NB: this is non-zero even for nloops = 0
  double b0() const { return _alphas->b0();}
  /// return the coefficient of alpha^3 in the QCD RGE
  /// NB: this is non-zero even for nloops <= 1
  double b1() const { return _alphas->b1();}
  /// return the coefficient of alpha^4 in the QCD RGE
  /// NB: this is non-zero even for nloops <= 2
  double b2() const { return _alphas->b2();}
  /// return the alpha^2 KCMW coefficient
  double KCMW() const { return _alphas->K();}
  double K()    const { return _alphas->K();}
  /// return the alpha^3 KCMW coefficient 
  double K2() const { return _alphas->K2();}
  /// same thing including a potential scale-dependence (e.g. due to
  /// mass thresholds
  double b0(double lnkt) const { return _alphas->b0(lnkt);}
  double b1(double lnkt) const { return _alphas->b1(lnkt);}
  
  /// return the charm quark mass
  precision_type mc() const { return _mc;}
  /// return the bottom  quark mass  
  precision_type mb() const { return _mb;}
  /// return log(mc)
  double lnmc() const { return _lnmc;}
  /// return log(mb)
  double lnmb() const { return _lnmb;}
  
  /// function to return the mass of a particle given a pdgid
  precision_type mass_from_pdgid (int pdgid) const{
    if((abs(pdgid) != 21) && (abs(pdgid) > _nf)) assert(false && "mass_from_pdgid: mass for this PDG id not available");
    switch(abs(pdgid)){
      case 4: // charm quark
        return _mc;
      case 5: // bottom quark
        return _mb;
      default:
        // in this case massless parton -> return 0
        return 0;
    }
  }

  // tests for large-Nc
  bool CA_is_2CF() const { return std::abs(_CA-2*_CF) < 10*std::numeric_limits<double>::epsilon(); }
  void set_CA_2CF()   { set_CA(2*_CF); }
  void set_CF_halfCA(){ set_CF(_CA/2); }

  /// get the reference scale and the coupling there
  double lnQ() const { return _alphas->lnQ(); }
  double alphasMSbarQ() const { return _alphas->alphasMSbarQ(); }

  /// sets xmuR, the value of the ratio of the renormalisation scale to its default
  void set_xmuR(double xmuR_in) {_xmuR = xmuR_in; _lnxmuR = log(_xmuR);}
  /// returns xmuR
  double xmuR()   const {return _xmuR;}
  /// returns log(xmuR)
  double lnxmuR() const {return _lnxmuR;}
  /// set whether or not to compensate for the xmuR dependence at NLO
  /// (by default true; only affects outcome if shower allows it)
  void set_xmuR_compensation(bool value=true){_xmuR_compensation = value;}
  /// returns bool according to whether xmuR compensation is on/off
  bool xmuR_compensation() const {return _xmuR_compensation;}


  /// sets xmuF, the value of the ratio of the renormalisation scale to its default
  void set_xmuF(double xmuF_in) {_xmuF = xmuF_in; _lnxmuF = log(_xmuF);}
  /// returns xmuF, the ratio of the factorisation scale over the standard kt scale
  /// suggested by the shower
  double xmuF()   const {return _xmuF;}
  /// returns log(xmuF)
  double lnxmuF() const {return _lnxmuF;}

  /// check whether or not we're using a pure soft limit of QCD
  bool soft_limit()       const{ return _soft_limit; }

  /// control if (half) gluons split like quarks
  /// Note: To be allowed to set this to "true", gluon->qqbar must be set to false
  void set_gluon_uses_q2qg(bool value=true){
    if (_gluon_uses_qqbar){
      assert((!value) && ("to set gluons to split as quarks, one should first disable gluon to qqbar splittings"));
    }
    _gluon_uses_q2qg = value;
  }
  bool gluon_uses_q2qg()  const{ return _gluon_uses_q2qg; }

  /// control if (half) the gluon splitting function includes
  /// splitting to qqbar
  /// Note: to set this to "true", _gluon_uses_q2qg should be false
  void set_gluon_uses_qqbar(bool value=true){
    if (_gluon_uses_q2qg){
      assert((!value) && ("to allow gluon to qqbar splittings, one should first disable gluons splitting as q->qg"));
    }
    _gluon_uses_qqbar = value;
  }
  bool gluon_uses_qqbar() const{ return _gluon_uses_qqbar; }
  
  /// return number of loops in the running
  unsigned int nloops() const{ return _nloops; }
  /// return shower stopping scale
  double lnkt_cutoff() const {return _lnkt_cutoff; }
  
  /// set the running coupling
  void set_alphas(unsigned int nloops_in, double alphasQ_in, double lnQ_in, double lnkt_cutoff_in,
                  bool mass_thresholds = false){
    _nloops       = nloops_in;
    _alphasMSbarQ = alphasQ_in;
    _lnQ          = lnQ_in;
    _lnkt_cutoff  = lnkt_cutoff_in;
    _mass_thresholds = mass_thresholds;

    _create_alphas();
  }

  /// switch on/of the CMW scheme for the running coupling
  void set_use_CMW(bool use_CMW_in=true){ _alphas->set_use_CMW(use_CMW_in); } 
  bool use_CMW() const{ return _alphas->use_CMW(); }

  /// set the scale at which the running coupling is cut off
  void set_alphas_cutoff(double lnkt_cutoff_in){
    _alphas->set_lnkt_cutoff(lnkt_cutoff_in);
  }

  /// adjust the lnkt cutoff so as to get the requested maximal value
  /// for alphas
  ///
  /// This only works for coupling of the type AlphasRunning and uses
  /// a 1-loop approximation to get the max
  void set_max_alphas(double alphas_max){
    assert(_nloops>0);
    AlphasRunning* alphas_ptr = dynamic_cast<AlphasRunning*>(_alphas.get());
    assert(alphas_ptr && "Setting alphas_max is only possible for alphas of type AlphasRunning");
    alphas_ptr->set_max_alphas(alphas_max);
  }

  //--------------------------------------------------
  // colour treatment
  void set_colour_scheme(ColourScheme colour_scheme_in){
    _colour_scheme = colour_scheme_in;
    if (_colour_scheme == ColourScheme::CFHalfCA) set_CF_halfCA();
    if (_colour_scheme == ColourScheme::CATwoCF)  set_CA_2CF();
  }
  ColourScheme colour_scheme() const { return _colour_scheme; }

  bool colour_no_g2qqbar() const { return _colour_no_g2qqbar; }
  void set_colour_no_g2qqbar(bool value){ _colour_no_g2qqbar = value; }

  
  //----------------------------------------------------------------------
  // methods to query information
  //----------------------------------------------------------------------
  /// description 
  std::string description() const;
  
  /// returns the value of alphas at a given scale;
  /// At two loops, if the CMW flag is on, then is is the physical
  /// (CMW-scheme) coupling that is returned.
  ///
  /// Note: the xmuR factor is not accounted for
  double alphas(double lnkt) const{
    return _alphas->alphas(lnkt);
  }

  /// returns the value of the MSbar-scheme alphas at a given scale.
  /// Note: the xmuR factor is not accounted for
  double alphasMSbar(double lnkt) const{
    return _alphas->alphasMSbar(lnkt);
  }

  /// returns the value of alphas at a given scale (no cutoff imposed)
  /// At two loops, if the CMW flag is on, then it is the physical
  /// (CMW-scheme) coupling that is returned.
  /// Note: the xmuR factor is not accounted for.
  double alphas_no_cutoff(double lnkt) const{
    return _alphas->alphas_no_cutoff(lnkt);
  }

  /// returns the value of the MSbar-scheme alphas at a given scale (no cutoff imposed)
  double alphasMSbar_no_cutoff(double lnkt) const{
    return _alphas->alphasMSbar_no_cutoff(lnkt);
  }

  /// a shorter version valid for fixed coupling
  double alphas() const{
    assert((_alphas->nloops()==0) && "QCDinstance: alphas() without arguments is only allowed for fixed coupling (nloops==0)");
    return _alphas->alphasMSbarQ();
  }
        
  /// running the largest value alphas can take at scale not smaller
  /// than lnkt_min (in the same scheme as the alphas(lnkt) function)
  double max_alphas_above_lnkt(double lnkt_min=0.0) const{
    // if lnkt_min is below the cutoff scale, alphas has to be taken
    // at the cutoff scale. otherwise, it is taken at lnkt_min.
    return _alphas->alphas_no_cutoff(std::max(lnkt_min,_alphas->lnkt_cutoff()));
  }

  // physical coupling
  void set_physical_coupling(bool value){
    _physical_coupling = value;
  }
  // access to the setting
  bool physical_coupling() const { 
    return _physical_coupling;
  }

  //----------------------------------------------------------------------
  /// splitting functions
  ///
  /// We consider 2 kinds of splitting functions:
  ///  - final-state particles for which splitting functions are
  ///    written in such a way that the singularities are at z=0.
  ///  - initial-state particle for which we have to keep the exact
  ///    splitting functions
  ///
  /// We therefore have the following cases:
  ///  - splitting_zPq2qg         final-state q_fs -> q_fs(1-z) + g_fs(z)
  ///  - splitting_zhalfPg2gg     final-state q_fs -> g_fs(1-z) + g_fs(z)
  ///  - splitting_zhalfPg2qqbar  final-state g_fs -> q_fs(1-z) + qb_fs(z)
  ///  - splitting_isr_zomzPa2bc  initial-state a_is -> b_is(1-z) + c_fs(z)
  ///       a, b, c can be  q, q, g
  ///                       q, g, q
  ///                       g, g, g
  ///                       g, q/qbar, qbar/q  (no nf factor)
  ///
  /// Final-state splitting functions are multiplied by z so as to
  /// regulate the soft divergence. Similarly, initial-state splitting
  /// functions are multiplied by z*(1-z).
  ///
  /// Splitting functions with a _normalised suffix are written wo the
  /// overall colour factor [assuming CF=CA/2, with the difference
  /// handled by our treatment of colour in ColourTransitionRunner]
  ///
  /// For gluon splitting, g->gg/qq, we implement "half" the splitting
  /// function to put emphasis on the fact that gluons are spread over
  /// 2 dipoles and we need both contributions to reproduce the full
  /// splitting function:
  ///
  ///    halfPg2gg(z) + halfPg2gg(1-z) = Pg2gg(z)
  ///
  /// with the constraint that halfPg2gg(z) has a singularity only at
  /// z=0.
  ///
  /// This leaves freedom as to how we treat the finite part of the
  /// splitting function, and we parametrise that freedom through a
  /// "weight" parameter w. Details are given bbelow for each of the
  /// splitting functions.
  ///
  /// Note on common w choice for g->qq and g->gg: 
  ///   When z->1, we have z (Pg2gg+Pg2qq) <= CA iff
  ///     nf TR <= CA   or    w<= CA/(nf TR-CA) 
  ///   When z->0, we have z (Pg2gg+Pg2qq) <= CA iff
  ///     nf TR <= CA   or    w>= (2 nf TR - 3 CA)/(nf TR-CA)
  ///   The full 0<w<=1 range is therefore allowed as long as
  ///     2 nf TR <= 3 CA
  ///
  /// More generally, we check that the soft limit is an over-estimate
  /// of the total g->XX probability
  ///
  /// All splitting functions come in 2 variants:
  ///   splitting_zPa2bc  z P_{a->bc}(z) with c carrying a momentum fraction z
  ///   splitting_zPa2bc  is z P_{a->bc}(z)/(2 CR) w CR=CA(CF) for a=g(q)

  /// functions to adjust the freedom of choice for the finite part of
  /// the splitting functions
  double splitting_finite_weight_g2gg()    const{ return _splitting_finite_weight_g2gg; }
  double splitting_finite_weight_g2qqbar() const{ return _splitting_finite_weight_g2qqbar; }
  void set_splitting_finite_weight_g2gg(double w=0.0){
    _splitting_finite_weight_g2gg = w;
    _check_casimirs_update_betas();
  }
  void set_splitting_finite_weight_g2qqbar(double w=0.0){
    _splitting_finite_weight_g2qqbar = w;
    _check_casimirs_update_betas();
  }

  // #TRK_ISSUE-235  QUERY from RV: why is all of this templated?
  //
  //GS-NOTE: I think that the logic was to keep the core classes
  // (e.g. Momentum classes) templated. Practically, I think that
  // nothing would change if we were to remove the template. At the
  // very least, the code does seem to compile if we (include Type.hh
  // and) use precision_type explicitly instead of the template. I
  // have no strong feeling here.

  //----------------------------------------------------------------------
  // final-state splittings
  /// splitting q->q+g (multiplied by z)
  ///
  /// the emitted gluon carries a momentum fraction z and the quark
  /// takes the remaining (1-z)
  ///
  /// This is Eq.(55) of 2018-07-notes.pdf
  template <typename T>
  T splitting_zPq2qg(T z) const{
    return 2*_CF * splitting_zPq2qg_normalised(z);
  }

  /// splitting q->q+g (multiplied by z and normalised by 1/(2CF))
  ///
  /// the emitted gluon carries a momentum fraction z and the quark
  /// takes the remaining (1-z)
  ///
  /// This is Eq.(55) of 2018-07-notes.pdf
  template <typename T>
  T splitting_zPq2qg_normalised(T z) const{
    if (_soft_limit)
      return 1;
    else
      return 0.5 * (1 + pow2(1-z));
  }

  /// splitting g->g+g (multiplied by z)
  ///
  /// the emitted gluon carries a momentum fraction z. The extra
  /// argument w allows to vary how non-singular terms are reshuffled
  /// between the P(z) aand P(1-z) contributions
  ///
  /// #TRK_ISSUE-238  Notes: (this should go in some latex/PDF notes)
  ///   P(z) = 2 CA [(1-z)/z + z(1-z)/2]     [w=1]
  /// or
  ///   P(z) = CA (1+(1-z)^3)/z              [w=0]
  /// and we interpolate between the 2 by using
  ///   P(z) = CA (1-w) (1+(1-z)^3)/z + 2 CA w  [(1-z)/z + z(1-z)/2]
  ///        = CA (1+(1-z)^3)/z + w CA [1-3z+z^2+z(1-z)]
  ///        = CA (1+(1-z)^3)/z + w CA [1-2z]
  ///
  /// This is Eq.(57) of 2018-07-notes.pdf with the 2nd term of the
  /// square bracket turned into an overal factor 2 (and the extra
  /// weight w)
  ///
  /// Watch out: this is half the gluon splitting (see comment above)
  template <typename T>
  T splitting_zhalfPg2gg(T z) const{
    if (_gluon_uses_q2qg) return splitting_zPq2qg(z);
    return _CA * splitting_zhalfPg2gg_normalised(z);
  }

  template <typename T>
  T splitting_zhalfPg2gg_normalised(T z) const{
    if (_gluon_uses_q2qg) return splitting_zPq2qg_normalised(z);
    if (_soft_limit)
      return 1.0;
    else
      return 0.5 * ((1 + pow3(1-z)) + z*_splitting_finite_weight_g2gg*(1-2*z));
  }
  
  /// splitting g->qqbar
  ///
  /// As for the gg case, we can use te z<->(1-z) symmetry to write
  /// either
  ///   P(z) = nf TR (z^2 + (1-z)^2)           [w=1]
  /// or (to avoid increasing the contribution when z->1)
  ///   P(z) = 2 nf TR (1-z)^2                 [w=0]
  ///
  /// The first option is Eq.(56) of 2018-07-notes.pdf
  ///
  /// Again we can introduce a weight as
  ///   P(z) = nf TR ((2-w)(1-z)^2 + w z^2)
  ///        = nf TR [2(1-z)^2 - w (1-2z)]
  ///
  /// Watch out: this is half the gluon splitting (see comment above)
  template <typename T>
  T splitting_zhalfPg2qqbar(T z) const{
    return _CA * splitting_zhalfPg2qqbar_normalised(z);
  }

  template <typename T>
  T splitting_zhalfPg2qqbar_normalised(T z) const{
    if ((_soft_limit) || (!_gluon_uses_qqbar))
      return 0.0;
    else
      return _nf*_TR/_CA * z * (pow2(1-z) - _splitting_finite_weight_g2qqbar*(0.5-z));
  }


  /// total gluon splitting function
  template <typename T>
  T splitting_zhalfPg2any(T z) const{
    return splitting_zhalfPg2gg(z) + splitting_zhalfPg2qqbar(z);
  }

  template <typename T>
  T splitting_zhalfPg2any_normalised(T z) const{
    return splitting_zhalfPg2gg_normalised(z) + splitting_zhalfPg2qqbar_normalised(z);
  }

  /// fraction of the total splitting function spent on g->qqbar
  ///
  /// Note that in this case the normalised version is the same
  template <typename T>
  T splitting_g2qqbar_fraction(T z) const{
    T Pq = splitting_zhalfPg2qqbar_normalised(z);
    T Pg = splitting_zhalfPg2gg_normalised(z);
    assert(Pq+Pg>0);
    return Pq/(Pg+Pq);
  }

  //----------------------------------------------------------------------
  // initial-state splittings
  //
  // #TRK_ISSUE-239 GS-NOTE: . the functions below should be checked (I may have missed
  //           e.g. overal factors)
  //         . I think we should get rid of the non-normalised versions
  //
  /// splitting q_is -> q_is(1-z) + g_fs(z) (multiplied by z)
  template <typename T>
  T splitting_isr_zomzPq2qg(T z) const{
    return 2*_CF * splitting_isr_zomzPq2qg_normalised(z);
  }

  /// splitting q_is -> q_is(1-z) + g_fs(z) 
  /// multiplied by z(1-z) and normalised by 1/(2CF)
  ///
  /// The backwards evolution of an initial-state quark into an
  /// initial-state quark has contributions from a single dipole and
  /// should be normalised by 1/CA=1/(2CF)
  template <typename T>
  T splitting_isr_zomzPq2qg_normalised(T z) const{
    if (_soft_limit) return 1;
    T omz = 1-z;
    return 0.5 * omz * (1 + pow2(omz));
  }
  
  /// splitting q_is -> g_is(1-z) + q_fs(z) (multiplied by z)
  template <typename T>
  T splitting_isr_zomzPq2gq(T z) const{
    return 2*_CF * splitting_isr_zomzPq2gq_normalised(z);
  }

  /// splitting q_is -> g_is(1-z) + q_fs(z) 
  /// multiplied by z(1-z) and normalised by 1/CA (see comment below)
  ///
  /// This only includes one quark flavour
  ///
  /// In this case,
  ///  - the colour from ColourTransitionRunner gives an acceptance
  ///    factor of 1 since we radiate a quark.
  ///  - we need an overall CF factor
  ///  - we get a CA/2 from the element selection
  /// This gives an overall normalisation of CF/(CA/2)
  template <typename T>
  T splitting_isr_zomzPq2gq_normalised(T z) const{
    if (_soft_limit) return 0;
    return (2*_CF/_CA) * 0.5 * z * (1 + pow2(z));
  }
  
  /// splitting g_is -> g_is(1-z) + g_fs(z) (multiplied by z(1-z))
  ///
  /// Watch out: this is half the gluon splitting (see comment above)
  template <typename T>
  T splitting_isr_zomzhalfPg2gg(T z) const{
    // #TRK_ISSUE-240 GS-NOTE: do we want to keep this? [not clear what to do]
    if (_gluon_uses_q2qg) return splitting_isr_zomzPq2qg(z);
    return _CA * splitting_isr_zomzhalfPg2gg_normalised(z);
  }

  /// splitting g_is -> g_is(1-z) + g_fs(z) 
  /// multiplied by z(1-z) and normalised by 1/CA
  ///
  /// Watch out: this is half the gluon splitting (see comment above)
  /// From the g->gg splitting function 
  ///   2 CA [1-z(1-z)]^2/[z(1-z)]
  /// we should therefore include a factor 1/2 (two dipoles) and 1/CA
  /// (normalisation)
  template <typename T>
  T splitting_isr_zomzhalfPg2gg_normalised(T z) const{
    // #TRK_ISSUE-241 GS-NOTE: do we want to keep the _gluon_uses_q2qg option?
    if (_gluon_uses_q2qg) return splitting_isr_zomzPq2qg_normalised(z);
    if (_soft_limit)      return 1.0;
    return pow2(1-z*(1-z));
  }
  
  /// splitting g_is -> q/qbar_is(1-z) + qbar/q_fs(z)
  /// multiplied by z:
  ///   P(z) = TR (z^2 + (1-z)^2)           [w=1]
  ///
  /// Watch out: this is half the gluon splitting (see comment above)
  template <typename T>
  T splitting_isr_zomzPg2qqbar(T z) const{
    return _CA * splitting_isr_zomzPg2qqbar_normalised(z);
  }

  template <typename T>
  T splitting_isr_zomzPg2qqbar_normalised(T z) const{
    if ((_soft_limit) || (!_gluon_uses_qqbar)) return 0.0;
    T omz = 1-z;
    return _TR/_CA * z * omz * (pow2(omz) + pow2(z));
  }

  // A(a,b,c,d) as in "Colour flow decomposition of NNLO double emission current"
  // -> corresponds exactly to Ant[a, b, c, d] below
  precision_type AA(Momentum a, Momentum b, Momentum c, Momentum d) const {
    precision_type D = 4;
    return (D-2)*4/pow2(s(b,c))*pow2(1 - s(a,b)/s(a,b+c) - s(c,d)/s(b+c,d))
      + 8*pow2(s(a,d)) / (s(a,b)*s(c,d)*s(a,b+c)*s(b+c,d))
      + 2*s(a,d)/s(b,c)*(4/(s(a,b)*s(b+c,d)) + 4/(s(a,b)*s(c,d)) + 4/(s(a,b+c)*s(c,d)) - 16/(s(a,b+c)*s(b+c,d)));
  }

  /// Double Soft Matrix Elements
  /// Ant[a, b, c] building block
  precision_type ant(Momentum a, Momentum b, Momentum c) const {
      return 4*s(a,c)/s(a,b)/s(b,c);
  }
  /// Ant[a, b, c, d] building block
  precision_type ant(Momentum a, Momentum b, Momentum c, Momentum d) const {
      return 8*s(a,d)*s(a,d)/s(a,b)/s(c,d)/(s(a,b)+s(a,c))/(s(b,d)+s(c,d))
      + 8*s(a,d)/s(a,b)/s(b,c)*(1/s(c,d) + 1/(s(b,d)+s(c,d)))
      + 8*s(a,d)/s(b,c)/(s(a,b)+s(a,c))*(1/s(c,d) - 4/(s(b,d)+s(c,d)))
      + 8/s(b,c)/s(b,c)*pow(1 - s(a,b)/(s(a,b)+s(a,c)) - s(c,d)/(s(b,d) + s(c,d)),2);
  /// KH 12-May-20 for tmw : maybe check replacing last line above,
  /// with view to stability/possible dir-diff formulation, by, for starters,
  ///    + 2 * pow( ( (s(a,b) - s(a,c)) / (s(a,b) + s(a,c))
  ///               - (s(b,d) - s(c,d)) / (s(b,d) + s(c,d))
  ///               ) / s(b,c) , 2);
  ///
  }
  /// Ant[a, b, c, d] building block
  ///
  /// This version is written in terms of
  ///  - the dij pairwise angles (dij = 2*[1-cos(theta_ij)])
  ///  - the energy fraction Eb/(Eb+Ec)
  ///
  /// below, dcross = dab dcd - dac dbd
  ///
  /// Compared to the above (raw) expresion, this is multiplied by
  ///    sbc (Eb+Ec)^2
  /// to make it dimensionless
  precision_type ant_sbc(precision_type dab, precision_type dac,
                         precision_type dad, precision_type dbc,
                         precision_type dbd, precision_type dcd,
                         precision_type zb, precision_type zc,
                         precision_type dcross) const {
    return 8*((dad/dcd)*(dad/dab)*(dbc/(zb*dab+zc*dac))/(zb*dbd+zc*dcd)
              + (dad/(zb*dab))*(1/(zc*dcd) + 1/(zb*dbd+zc*dcd))
              + dad/(zb*dab+zc*dac)*(1/(zc*dcd) - 4/(zb*dbd+zc*dcd))
              + zb*zc*pow(dcross/((zb*dab+zc*dac)*(zb*dbd+zc*dcd)),2)/dbc);
  }

  /// The above antenna (ant_sbc) is computed in the large-Nc approximation.
  /// This is the full CA coefficient, i.e. discarding the
  /// independent-emission contribution proportional to CF
  precision_type ant_sbc_fullCA(precision_type dab, precision_type dac,
                                precision_type dad, precision_type dbc,
                                precision_type dbd, precision_type dcd,
                                precision_type zb, precision_type zc,
                                precision_type dcross) const {
    return 8*((dad/dcd)*(dad/dab)*(dbc/(zb*dab+zc*dac))/(zb*dbd+zc*dcd)
              + (dad/(zb*dab))*(1/(zc*dcd) + 1/(zb*dbd+zc*dcd))
              + dad/(zb*dab+zc*dac)*(1/(zc*dcd) - 4/(zb*dbd+zc*dcd))
              + zb*zc*pow(dcross/((zb*dab+zc*dac)*(zb*dbd+zc*dcd)),2)/dbc
              - 1/(zb*zc)*(dad/dab)*(dad/dcd)*dbc/(dac*dbd));
  }

  /// The above antenna (ant_sbc) is computed in the large-Nc approximation.
  /// This is the CF coefficient, i.e. including just the independent
  /// (primary) emissions.
  precision_type ant_sbc_fullCF(precision_type dab, precision_type dac,
                                precision_type dad, precision_type dbc,
                                precision_type dbd, precision_type dcd,
                                precision_type zb, precision_type zc,
                                precision_type dcross) const {
    return 8/(zb*zc)*(dad/dab)*(dad/dcd)*dbc/(dac*dbd);
  }

  
  /// same as above with extra messages for debugging purpose
  ///
  /// below, dcross = dab dcd - dac dbd
  precision_type ant_sbc_verbose(precision_type dab, precision_type dac,
                                 precision_type dad, precision_type dbc,
                                 precision_type dbd, precision_type dcd,
                                 precision_type zb, precision_type zc,
                                 precision_type dcross) const {
    precision_type term1 = (dad/dcd)*(dad/dab)*(dbc/(zb*dab+zc*dac))/(zb*dbd+zc*dcd);
    precision_type term2 = (dad/(zb*dab))*(1/(zc*dcd) + 1/(zb*dbd+zc*dcd));
    precision_type term3 = dad/(zb*dab+zc*dac)*(1/(zc*dcd) - 4/(zb*dbd+zc*dcd));
    //precision_type term4 = 0.25/(zb*zc*dbc)*pow((zc*dac-zb*dab)/(zb*dab+zc*dac)
    //                                            + (zb*dbd-zc*dcd)/(zb*dbd+zc*dcd),2);
    precision_type term4 = zb*zc/dbc*pow(dcross/((zb*dab+zc*dac)*(zb*dbd+zc*dcd)),2);
    std::cerr << "  dab=" << dab << ", dac=" << dac << ", dad=" << dad << ", dbc=" << dbc << ", dbd=" << dbd << ", dcd=" << dcd << ", zb=" << zb << std::endl;
    std::cerr << "  term1 = " << term1 << std::endl;
    std::cerr << "  term2 = " << term2 << std::endl;
    std::cerr << "  term3 = " << term3 << std::endl;
    std::cerr << "  term4 = " << term4 << std::endl;
    // std::cerr << "  dad/dcd       = " << sad/scd       << std::endl;
    // std::cerr << "  dad/dab       = " << sad/sab       << std::endl;
    // std::cerr << "  dbc/(sab+sac) = " << sbc/(sab+sac) << std::endl;
    // std::cerr << "  1/(sbd+scd)   = " << 1/(sbd+scd)   << std::endl;
    return 8*(term1+term2+term3+term4);
  }

  /// Matrix element for q qbar q' qbar' in the limit where g1 is soft,
  /// for g1 -> q' qbar' (exact in N_C)
  ///          qbar'   q'
  ///              \  /
  ///               ||
  ///   q           ||         qbar
  ///   -----------/  \---------
  /// This is multiplied by s(qp, qbarp)
  ///
  /// below, dcross = d_q_qbp d_qb_qp - d_q_qp d_qb_qbp
  ///               = d_qb_qp ((q-qp)+(q-qbp)).(qbp-qp) - d_q_qp ((qb-qp)+(qb-qbp)).(qbp-qp)
  precision_type ant_qqbar_sqp_qbp(precision_type d_q_qb,   precision_type d_q_qp,
                                   precision_type d_q_qbp,  precision_type d_qb_qp,
                                   precision_type d_qb_qbp, precision_type d_qp_qbp,
                                   precision_type zqbp,     precision_type zqp,
                                   precision_type d_cross) const {
    // It seems like the sume of the 3 contributions can be rewriten as follows:
    return 4 * _nf*_TR * _CA *
      (d_q_qb/(zqbp*d_q_qbp+zqp *d_q_qp)/(zqp *d_qb_qp+zqbp*d_qb_qbp)
       - zqp*zqbp*pow(d_cross/((zqbp*d_q_qbp+zqp *d_q_qp)*(zqp *d_qb_qp+zqbp*d_qb_qbp)),2)/d_qp_qbp);

    // return 8 * _nf*_TR*_CF *
    //   (d_q_qb/(zqbp*d_q_qbp+zqp *d_q_qp)/(zqp *d_qb_qp+zqbp*d_qb_qbp)
    //    - 0.25/(zqp*zqbp*d_qp_qbp)*pow( (zqp *d_q_qp  -zqbp*d_q_qbp)/(zqbp*d_q_qbp+zqp *d_q_qp  )
    //                                   +(zqbp*d_qb_qbp-zqp *d_qb_qp)/(zqp *d_qb_qp+zqbp*d_qb_qbp),2));
  }
  // qq[i,j] building block
  precision_type qq(Momentum i, Momentum j, Momentum qp, Momentum qbarp) const{
      return (s(i,qp)*s(j,qbarp)+s(j,qp)*s(i,qbarp)-s(i,j)*s(qp,qbarp))/s(qp,qbarp)/s(qp,qbarp)/(s(i,qp)+s(i,qbarp))/(s(j,qp)+s(j,qbarp));
  }

  /// QQ[i,j] building block
  precision_type QQ(Momentum i, Momentum j, Momentum qp, Momentum qbarp) const {
      return 2/s(qp,qbarp)*(s(i,j)/(s(i,qp)+s(i,qbarp))/(s(j,qp)+s(j,qbarp))
      - 0.25/s(qp,qbarp)*pow((s(i,qp)-s(i,qbarp))/(s(i,qp)+s(i,qbarp)) - (s(j,qp)-s(j,qbarp))/(s(j,qp)+s(j,qbarp)), 2));
  }
  /// QQ[i,j,k] building block
  precision_type QQ(Momentum i, Momentum j, Momentum k, Momentum qp, Momentum qbarp) const {
      return 1/s(qp,qbarp)*(s(i,j)/(s(i,qp)+s(i,qbarp))/(s(j,qp)+s(j,qbarp))
                          + s(i,k)/(s(i,qp)+s(i,qbarp))/(s(k,qp)+s(k,qbarp))
                          - s(j,k)/(s(j,qp)+s(j,qbarp))/(s(k,qp)+s(k,qbarp))
                          - 0.5/s(qp,qbarp)
                          * ((s(i,qp)-s(i,qbarp))/(s(i,qp)+s(i,qbarp)) - (s(j,qp)-s(j,qbarp))/(s(j,qp)+s(j,qbarp)))
                          * ((s(i,qp)-s(i,qbarp))/(s(i,qp)+s(i,qbarp)) - (s(k,qp)-s(k,qbarp))/(s(k,qp)+s(k,qbarp))));
  }

  /// Matrix element for γ -> q qbar q' qbar' in the limit where g1 is soft,
  /// for g1 -> q' qbar' (exact in N_C)
  precision_type gamma_to_qqbarqqbar(Momentum q, Momentum qbar, Momentum qp, Momentum qbarp) const {
      // return 4*_nf*_TR*_CF*(qq(q,q,qp,qbarp)+qq(qbar,qbar,qp,qbarp)-2*qq(q,qbar,qp,qbarp));
      // KH 14-May-2022. Replaced form above by the four-argument 'QQ' fn below, arrived
      // at in the soft spin paper, when instabilities were detected there.  Comparing terminal
      // output in ddreal vs double, selecting results where ddreal/double evaluations with the
      // old form (above) differ by >2 or <0.5, the 'QQ' form below, in double, agrees with the
      // ddreal evaluation of the old form above to better than per-mille precision in ~98/99%
      // of cases (note the old form is usually off by orders of magnitude whenever it is off).
      // The form below can still have issues in (weird) cases where first is collinear to
      // aux_first and (simultaneously?) new is collinear to aux_new. The old form appears no
      // better in such configurations but perhaps we can further improve those.
      return 4*_nf*_TR*_CF*QQ(q,qbar,qp,qbarp);
  }

  /// Matrix element for γ -> q qbar g1 g2, where g1 and g2 are soft (but not necessarily
  /// ordered in energy), and we assume large-NC
  precision_type gamma_to_qqbarg1g2(Momentum q, Momentum qbar, Momentum g1, Momentum g2) const {
      return _CA*_CA/4*(ant(q, g1, g2, qbar) + ant(q, g2, g1, qbar));
  }

  /// Matrix element for γ -> g g q' qbar', where q'/qbar' are soft (but not necessarily
  /// ordered in energy), and we assume large-NC
  precision_type Higgs_to_ggqqbar(Momentum g1, Momentum g2, Momentum q_prime, Momentum qbar_prime) const {
      //return gamma_to_qqbarg1g2(q_prime, qbar_prime, g1, g2);
      return gamma_to_qqbarqqbar(g1, g2, q_prime, qbar_prime)/_CF*_CA;
  }

  /// Matrix element for γ -> g1 g2 g3 g4, where g3 and g4 are soft (but not necessarily
  /// ordered in energy), and we assume large-NC
  precision_type Higgs_to_gggg(Momentum g1, Momentum g2, Momentum g3, Momentum g4) const {
      return _CA*_CA/4*(2*ant(g1, g3, g2)*ant(g1, g4, g2)
                      + 2*ant(g1, g3, g4, g2) + 2*ant(g1, g4, g3, g2));
  }

  // Enumerator for the choice of splitting channel.
  // The matrix element carries the responsibility
  // of choosing who splits (emitter <-> spectator).
  // and the flavour of the splitting.
  // Note that the last entry is not a channel
  // we use it to set the number of splitting channels
  // - esr/ser: the matrix element is further divided into pieces
  // containing only one collinear singularity, e.g. when i || k
  // (for esr) and j || k (for ser)
  // - fs/is: final-state vs initial-state splitting
  enum SplittingChannel : unsigned int {
      fs_q_esr_to_qg, //0
      fs_q_ser_to_qg, //1
      fs_g_esr_to_gg, //2
      fs_g_ser_to_gg, //3
      fs_g_esr_to_qq, //4
      fs_g_ser_to_qq, //5
      // note that for the IS case we would get something like:
      // is_g_esr_to_gg,
      // is_g_ser_to_gg,
      // is_g_esr_to_qqbar,
      // is_g_ser_to_qqbar,
      // is_q_esr_to_qg,
      // is_q_ser_to_qg,
      MAX_NUMBER_CHANNEL
  };

  /// access to the maximal number of splitting channels (length of the enum defined above)
  static constexpr unsigned int number_of_splitting_channels() {return _number_of_splitting_channels;}
  
private:
  static constexpr unsigned int _number_of_splitting_channels = MAX_NUMBER_CHANNEL;
  
  /// initialise all variables to their default values
  void _init_defaults(){
    // Full QCD
    _CA = 3.0;
    _CF = 4.0/3.0;
    _TR = 0.5;

    // make things massless by default
    _mb   = _mc   = 0.0;
    _lnmb = _lnmc = -numeric_limit_extras<precision_type>::log_max();

    // create the alphas object
    _use_powheg_alphas = false;
    _alphas_use_rg     = false;
    _create_alphas();
    set_global_CMW(true);
    set_effective_A3_CMW(0.0);
    
    // splitting configuration
    //
    // Note that the defaults below are meant for backwwards
    // compatibility, they should be changed to their physical value in the future
    _gluon_uses_q2qg  = false;
    _gluon_uses_qqbar = true;
    _splitting_finite_weight_g2gg    = 0.0; //< freedom w splitting finite terms
    _splitting_finite_weight_g2qqbar = 0.0; //< freedom w splitting finite terms
    
    // initialise the beta values, casimir factors and check the cutoff
    _check_casimirs_update_betas();

    // // colour factors
    _colour_scheme = ColourScheme::NODS;
  }

  /// update the coefficients of the beta function and cusp anomalous
  /// dimension needed for the running of the coupling
  void _check_casimirs_update_betas(){
    if (_alphas->has_qcd_constants())
      _alphas->set_qcd_constants(_CF, _CA, _TR);
    if (_alphas->has_mass_thresholds())
      _alphas->set_ln_mass_thresholds(_lnmc, _lnmb);

    // if you are using massive c or b, we need to update  
    // _splitting_finite_weight_g2qqbar to be 1. (otherwise massive is not bounded by massless)
    if((_mb != 0) || (_mc != 0)){
      _splitting_finite_weight_g2qqbar = 1.;
      //_splitting_finite_weight_g2gg    = 1.;
      // for gg, when running with very small as it seems to be important to do that as well...
      assert(_nf == 5 && "Massive shower cannot be run with anything else than 5 flavours!");
    }

    // check that the g->XX splitting functions is such that the soft
    // approx Pg2xx=2CA/z is an overestimate of the actual splitting.
    //
    // We want
    //   CA/2 (1 + (1-z)^3 + z wg (1-2z)) + nfTR z ((1-z)^2 - wq (1/2-z)) <= CA
    // We focus on both the z->1 and z->0 limits, yielding
    //   -3 CA + 2 nfTR <= wq nfTR - wg CA <= CA
    double splitting_w_tmp = _splitting_finite_weight_g2qqbar * _nf*_TR
                           - _splitting_finite_weight_g2gg    * _CA;
    assert((-3*_CA + 2*_nf*_TR <= splitting_w_tmp) && ( splitting_w_tmp <= _CA)
           && ("the provided QCD constants and splitting weight parameters are not allowed"));
  }

  void _create_alphas(){
    if (_nloops==0){
      _alphas.reset(new AlphasRunning(0, _alphasMSbarQ, _lnQ, _lnkt_cutoff, (unsigned int) _nf));
    } else if (_mass_thresholds){
      if (_use_powheg_alphas){
        _alphas.reset(new AlphasPowhegStyleVFS(_nloops, _alphasMSbarQ, _lnQ, _lnkt_cutoff));
      } else {
        _alphas.reset(new Alphas5VFS(_nloops, _alphasMSbarQ, _lnQ, _lnkt_cutoff, _lnmc, _lnmb, _alphas_use_rg));
      }
    } else {
      if (_alphas_use_rg){
        _alphas.reset(new AlphasRGRunning(_nloops, _alphasMSbarQ, _lnQ, _lnkt_cutoff, (unsigned int) _nf));
      } else {
        _alphas.reset(new AlphasRunning(_nloops, _alphasMSbarQ, _lnQ, _lnkt_cutoff, (unsigned int) _nf));
      }
    }
    _alphas->set_qcd_constants(_CF, _CA, _TR);
  }

  //----------------------------------------------------------------------
  // internal variables
  /// s(i,j) Lorentz invariant for double soft
  precision_type s(Momentum i, Momentum j) const {
      return 2*dot_product(i,j);
  }
  
  unsigned int _nloops;   ///< number of loops in the running coupling (0 is fixed coupling)
  double _alphasMSbarQ;   ///< value of the strong coupling at the hard scale (Q), in the MSbar scheme
  double _lnQ;            ///< log(Q) with Q the scale at which the coupling is specified
  double _lnkt_cutoff;    ///< log(kt_value) at which alphas is set to 0
  std::shared_ptr<AlphasBase> _alphas; ///< pointer to the alphas instance
  bool   _physical_coupling = false;   ///< flag that tells you whether we are using a physical coupling (default false)

  /// basic QCD constants
  double _CF, _CA, _nf, _TR;
  //double halfCA; if this is uncommented, remember to initialise it and include its output below?

  /// charm and bottom mass
  precision_type _mc, _mb;
  // logs of the above masses
  double _lnmc, _lnmb;

  // a set of flags used to configure the parton splittings
  bool _soft_limit;        ///< when true, limit oneselves to the soft limit of QCD
  bool _mass_thresholds;   ///< when true, return alphas with mass thresholds
  bool _use_powheg_alphas; ///< when true (and mass thresholds are on), use the powheg-style alphas running
  bool _alphas_use_rg;     ///< when true, alphas uses an explicit solution of the RGE (preliminary)
  bool _gluon_uses_q2qg;   ///< when true, the (half) gluon splitting function is
                           ///< taken to be the same as q->qg
  bool _gluon_uses_qqbar;  ///< when true, g->qqbar splittings are allowed
  /// vary the splitting functions exploiting the freedom of choice
  /// for the dispatching the non-divergent contribution over the 2
  /// dipoles contributing to 1 gluon
  double _splitting_finite_weight_g2gg, _splitting_finite_weight_g2qqbar;

  /// colour treatment
  ColourScheme _colour_scheme;
  bool _colour_no_g2qqbar = false;

  /// the ratio of muR over the default muR
  /// #TRK_ISSUE-246  (GPS 2022-09-11: not entirely clear if it should be here
  /// or in the shower; ultimately it may even become an array)
  double _xmuR = 1.0, _lnxmuR = 0.0;
  /// in showers that allow for it, allow xmuR compensation to be used
  bool _xmuR_compensation = true;
  /// the ratio of muF over the standard shower kt scale
  double _xmuF = 1.0, _lnxmuF = 0.0;
  
#include "autogen/auto_QCD_QCDinstance.hh"
}; // end class QCDinstance

} // namespace panscales

#include "autogen/auto_QCD_global-hh.hh"
#endif //  __QCD_HH__
