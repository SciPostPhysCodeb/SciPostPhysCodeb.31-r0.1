//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "AnalysisFramework.hh"

using namespace std;
using namespace fjcore;
using namespace panscales;
/// ----------------------------------------------------------------------
/// Code to examine the energy distribution in a rapidity slice.
///
/// In order to do a test of the SL accuracy for energy flow in 
/// slice, (see and) run the example-nonglobal-nll-ee.py script.
///
/// An example of a command line that is uses 
/**
    ./build-doubleexp/example-nonglobal-nll-ee -process ee2qq -Q 1.0 -shower panglobal -beta 0.0 \
      -spin-corr off -nloops 1 -colour CATwoCF -alphas 1e-09 -lnkt-cutoff -501000000.0 \
      -strategy CentralRap -half-central-rap-window 10 -lambda-obs-min -0.5  \
      -slice-maxrap 1.0 -ln-obs-margin -10 -nev 10000 \
      -out example-results/example-nonglobal-nll-ee.dat
*/
/// This should run in seconds
///
class ExampleNonGlobalObsEE : public AnalysisFramework {
public:  
  ExampleNonGlobalObsEE(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in) {
    // get info from the command line
    // Note: _ln_obs_margin can be set to 0 if we run w a tiny alphas
    cmdline_in->start_section("Analysis-specific options");
    _lambda_obs_min = cmdline->value("-lambda-obs-min",std::numeric_limits<double>::max())
      .help("\n\t minimal value of ln(v_obs) that we want to reach");
    _slice_maxrap  = cmdline->value("-slice-maxrap",  1.0)
      .help("\n\t half-size of the rapidity slice we are probing");
    _ln_obs_margin = cmdline->value("-ln-obs-margin", log(numeric_limit_extras<double>::sqrt_epsilon()))
      .help("\n\t additional margin, in log(pt_slice), beyond lambda_min/alphas");
    cmdline_in->end_section("Analysis-specific options");

    if(!cmdline_in->help_requested()){
      _maxrap_margin = cmdline->value<double>("-half-central-rap-window") - _slice_maxrap;
      assert((_ln_obs_margin<=0.0) && "ln-obs-margin should be negative");
    }
    
    _Q   = cmdline->value<double>("-Q",1);
    _lnQ = to_double(log_T(_Q));

  }

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {
    // get the value of alphas at the reference scale
    _alphasQ = f_shower_runner->shower()->qcd().alphasMSbar(_lnQ);
    // β_ps, as in ν_ps = kt exp(-β_ps|η|) / ρ_ps , is needed to work
    // out the lnv_ps range needed to cover the asked-for lmdaObsMin.
    _beta_ps = f_shower_runner->shower()->beta();
    //----------------------------------------------------------------
    // adjust the lnvmin according to the various buffers
    //----------------------------------------------------------------    
    //
    //
    //   |\            : 
    //   | \           : 
    //   |  \          : 
    //   |  |\         : 
    //   |  | \        : 
    //   |  |  \       : 
    //   |  | / \      : 
    //   |  |/   \     : 
    //   |--|-----\    : this is lambda/aS + margin  [lnktcut-off should be below this
    //   | /|      \   : 
    //   |/ |       \  : 
    //   +             : this should be lnvmin
    //      +          : rapidity slice limit
    f_lnvmin = (_lambda_obs_min/_alphasQ+_ln_obs_margin) + (1+_beta_ps) * _slice_maxrap;
    
    if (f_shower_runner->shower()->qcd().lnkt_cutoff() > (_lambda_obs_min/_alphasQ+_ln_obs_margin)){
      header << "# WARNING: alphas is cutoff at a kt scale above the low end of the buffer region: "
             << f_shower_runner->shower()->qcd().lnkt_cutoff() << " is larger than "
             << (_lambda_obs_min/_alphasQ+_ln_obs_margin) << endl;
      cerr << "# WARNING: alphas is cutoff at a kt scale above the low end of the buffer region: "
           << f_shower_runner->shower()->qcd().lnkt_cutoff() << " is larger than "
           << (_lambda_obs_min/_alphasQ+_ln_obs_margin) << endl;
    }

    // only generate emissions at central rapidity (rapidity slice + margin)
    f_shower_runner->half_central_rap_window(_slice_maxrap + _maxrap_margin);
    
    //--------------------------------------------------
    // observable histograms
    const double dasL  = 0.01;
    const double asLmin= _lambda_obs_min;
    const double asLmax= 0.0;
    
    cumul_hists_err["slice_scalar_pt"].declare( asLmin, asLmax, dasL);
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    double evwgt = event_weight();

    //--------------------------------------------------------------------
    // compute the energy in the rapidity slice (excluding the initial q qbar pair)
    precision_type sum_pt = 0.0;
    for (unsigned int i=2; i<f_event.particles().size(); ++i){
      const Particle & p = f_event.particles()[i];
      if (fabs(to_double(p.rap())) > _slice_maxrap) continue;
      sum_pt += p.pt();;
    }
    // normalise by Q
    sum_pt /= _Q;;
  
    double lambda = _alphasQ * safe_log(sum_pt); // lambda < 0
    cumul_hists_err["slice_scalar_pt"].add_entry(lambda, evwgt);
  }

  //----------------------------------------------------------------------
  // helpers
  //
  // log avoiding 0
  double safe_log(precision_type x) const{
    return x<=0 ? -std::numeric_limits<double>::max() : to_double(log_T(x));
  }

protected:
  double _alphasQ, _qcd_b0, _beta_ps;
  double _lambda_obs_min, _lnQ;
  double _Q;

  double _slice_maxrap;     ///< extent of the tested rapidity slice
  double _maxrap_margin;    ///< controls the rapidity-window allowed in the shower
  double _ln_obs_margin;    ///< additional security in lnvmin at the observable boundary

};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  ExampleNonGlobalObsEE driver(&cmdline);
  driver.run();
}


