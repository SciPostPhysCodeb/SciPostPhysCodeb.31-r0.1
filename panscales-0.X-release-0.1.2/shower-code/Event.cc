//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "Event.hh"
#include "ColourTransitionRunner.hh"
#include <numeric>
#include "Matrix3.hh"

using namespace std;

namespace panscales{

  

  /// initialise the difference in direction between the quark
  /// and anti-quark
  void Dipole::init_dirdiff() {
    assert(_event != nullptr);
    dirdiff_3_minus_3bar = (*_event)[index_q].direction() - (*_event)[index_qbar].direction();
  }

  //------------------------------------------------------------------------
  // Event implementation
  //------------------------------------------------------------------------
  
  // sets the internal barcode and pointer information
  // updates the event Q
  // updates the directional differences in the dipoles
  void Event::finish_init(){
    // set the internal pointers
    set_initial_barcodes();
    set_internal_pointers();
    // set the Q reference vector
    if(is_DIS()){
      // use the two quarks as reference momenta
      auto & q1 = _particles[0].is_parton() ? _particles[0] : _particles[1];
      auto & q2 = _particles[2];
      set_incoming_ref_momenta(q1, q2);
      Q_ = q1+q2;

    } else {
      Q_ = momentum_sum();
    }
    // update directional differences
    // of the dipoles
    for (auto & dipole: _dipoles) {
      dipole.init_dirdiff();
    }
    // of the non-splitting dipoles
    for (auto & dipole: _non_splitting_dipoles) {
      dipole.init_dirdiff();
    }
  }

  // returns the sum of the outgoing momenta in the event
  Momentum Event::momentum_sum() const {
    Momentum sum;
    for (const auto & particle : _particles) {
      if (particle.initial_state()) continue;
      sum += particle.momentum();
    }
    return sum;
  }
  
  // returns the sum of the hard momenta in the event
  Momentum Event::momentum_sum_hard_system() const {
    Momentum sum;
    for (const auto & particle : _particles) {
      if (particle.initial_state() || !particle.is_in_hard_system()) continue;
      sum += particle.momentum();
    }
    return sum;
  }

  Momentum Event::momentum_partons_in() const {

    Momentum sum;
    if(is_DIS() && _particles.size()==3 && _particles[2].is_parton()) {
      // TRK_ISSUE-GEN-XXX [GPS]: is this safe? What if the incoming
      // particle has a mass?
      sum = Momentum::fromP3M2(_particles[2].p3(), 0); //avoid rounding errors 
    } else {
      for (const auto & particle : _particles) {
        if (particle.initial_state()) sum += particle.momentum();
      }
    }
    return sum;    
  }


  Momentum Event::sum_momentum_out_in_chain(int dis_chain) const {
    Momentum sum;
    for (const auto & particle : _particles) {
      if (!particle.initial_state() && particle.get_DIS_chain() == dis_chain) sum += particle.momentum();
    }
    return sum;    
  }

  bool Event::check_momentum() const {
    Momentum momentum_in = momentum_partons_in();
    // for e+e-, the momentum in should just be the initial momentum
    // as evaluated before showering, but we don't have that
    // here, so just return
    if (momentum_in.m2() == 0) return true; //momentum_in = initial_momentum;
    Momentum momentum_difference = momentum_sum() - momentum_in;

    precision_type E = momentum_in.E();
    for (unsigned i = 0; i<4; i++) {
      double reldiff = to_double(momentum_difference[i]/E);
      if (fabs(reldiff) > 1e-6 || (reldiff != reldiff)) {
        ostringstream ostr;
        ostr << "Relative difference " << reldiff
             << " in component " << i
             << " of total event momentum (before/after generation) "
             << "exceeds threshold or is NaN" << endl;
        ostr << "Incoming momentum is " << momentum_in << endl;
        ostr << "Final    momentum is " << momentum_sum() << endl;
        ostr << "Difference is        " << momentum_difference << endl;
        cout << ostr.str();
        return false;
      }
    }
    return true;
  }


  //----------------------------------------------------------------------
  // DIS information
  void Event::set_incoming_ref_momenta(Particle p_in, Particle p_out, int chain_number){
    if(chain_number == 1){
      _p_ref_in_1  = p_in;
      _p_ref_out_1 = p_out;
      _Q2_dis_1    = to_double(2.*dot_product(p_in, p_out));
      _Q_dis_1     = p_in + p_out;
    } else if (chain_number == 2){
      _p_ref_in_2  = p_in;
      _p_ref_out_2 = p_out;
      _Q2_dis_2    = to_double(2.*dot_product(p_in, p_out));
      _Q_dis_2     = p_in + p_out;
    } else{
      assert(false && "Unknown chain number");
    }
  }
  // access to the reference momenta and the Q values
  const Momentum & Event::ref_in (int chain_number) const {
    assert((chain_number == 1 || chain_number == 2) && "DIS chain needs to be 1 or 2");
    return (chain_number == 1) ? _p_ref_in_1 : _p_ref_in_2 ;
  }
  const Momentum & Event::ref_out(int chain_number) const {
    assert((chain_number == 1 || chain_number == 2) && "DIS chain needs to be 1 or 2");
    return (chain_number == 1) ? _p_ref_out_1 : _p_ref_out_2;}
  
  const double   & Event::Q2_dis(int chain_number)  const {
    assert((chain_number == 1 || chain_number == 2) && "DIS chain needs to be 1 or 2");
    return (chain_number == 1) ? _Q2_dis_1 : _Q2_dis_2;
  }
  const Momentum & Event::Q_dis(int chain_number)  const {
    assert((chain_number == 1 || chain_number == 2) && "DIS chain needs to be 1 or 2");
    return (chain_number == 1) ? _Q_dis_1 : _Q_dis_2;
  }


  //----------------------------------------------------------------------
  // Function to boost and realign the whole event. Needed for PanLocalpp
  void Event::boost_and_realign() { 

    MomentumM2<precision_type> Q_CM = beam1() + beam2();
    MomentumM2<precision_type> shower_Q = Q();

    LorentzBoost boost(Q_CM.reversed()); 
    // first do the longitudinal boost to go to the COM frame
    for(auto & particle : _particles){
      MomentumM2<precision_type> p = particle.momentum();
      particle.reset_momentum(p.boost(boost));
    }
    // boost the shower Q
    shower_Q = shower_Q.boost(boost);

    // and now we do the rotation to align the incoming particles with the z-axis
     const MomentumM2<precision_type> & pa_dir = incoming1().pz() > 0 ? incoming1() : incoming2();
     Matrix3 rot_pa_to_z = Matrix3::from_direction(pa_dir).transpose();
 
    for(auto & particle : _particles){
      Momentum3<precision_type> new_mom_p3;
      MomentumM2<precision_type> p = particle.momentum();
      if(particle.is_initial_state()){
        if(p.pz() < 0) new_mom_p3 = Momentum3<precision_type>(0,0,-p.E());
        else           new_mom_p3 = Momentum3<precision_type>(0,0, p.E());
      } else {// particle is final state, do the rotation
        new_mom_p3 = rot_pa_to_z * p;
      }
      particle.reset_momentum(Momentum::fromP3M2(new_mom_p3, p.m2()));

    }

    // update the beams
    reset_beam1(_particles[0]/precision_type(_pdf_x_beam1));
    reset_beam2(_particles[1]/precision_type(_pdf_x_beam2));

    // and finally we reset Q
    Momentum3<precision_type> new_showerQ_p3 = rot_pa_to_z*shower_Q;
    reset_Q(Momentum::fromP3M2(new_showerQ_p3, Q2()));
  }

  // boost to Breit frame where the ref vectors (in and out) are back-to-back
  // needed for DIS
  void Event::boost_to_breit_frame() { 
    // only for DIS
    assert(is_DIS() && "boost to Breit frame only makes sense for a DIS event");
    MomentumM2<precision_type> q_current = ref_in() + ref_out();
    // this is the inverse Lorentz boost
    LorentzBoost boost(q_current.reversed()); 
    // do the longitudinal boost to go to the CoM frame
    for(auto & particle : _particles){
      MomentumM2<precision_type> p = particle.momentum();
      particle.reset_momentum(p.boost(boost));
    }
    // boost the ref vectors
    _p_ref_in_1  = boost * ref_in();
    _p_ref_out_1 = boost * ref_out();
    
    // more operations are needed if after this we want to make a new momentum!

    // boost the shower
    // MvB NOTE - needs to be implemented
    //  shower_Q_ = shower_Q.boost(boost);
    // reset_beam1(_particles[0]/precision_type(_pdf_x_beam1));
    // reset_beam2(_particles[1]/precision_type(_pdf_x_beam2));

    // boost and re-align
    Matrix3 rot_pa_to_z = Matrix3::from_direction(ref_in()).transpose();

    for(auto & particle : _particles){
      MomentumM2<precision_type> p = particle.momentum();
      Momentum3<precision_type> new_mom_p3 = p.p3();

      if(particle.is_initial_state() && particle.is_parton()){
        if(ref_in().pz() < 0) new_mom_p3 = Momentum3<precision_type>(0,0,-p.E());
        else                  new_mom_p3 = Momentum3<precision_type>(0,0, p.E());
      } else if (particle.is_initial_state() && !particle.is_parton()){
        if(ref_in().pz() < 0) new_mom_p3 = Momentum3<precision_type>(0,0, sqrt(Q2_dis()));
        else                  new_mom_p3 = Momentum3<precision_type>(0,0,-sqrt(Q2_dis()));
      }
      else {// particle is final state, do the rotation
        new_mom_p3 = rot_pa_to_z * p;
      }
      // reset momenta
      if(p.m2()>=0) particle.reset_momentum(Momentum::fromP3M2(new_mom_p3, p.m2()));
      else          particle.reset_momentum(MomentumM2<precision_type>(new_mom_p3.px(), new_mom_p3.py(), new_mom_p3.pz(), 0., p.m2()));
    }

    // rotate also the reference vectors
    _p_ref_in_1  = rot_pa_to_z * ref_in();
    _p_ref_out_1 = rot_pa_to_z * ref_out();

  }

  //----------------------------------------------------------------------
  // returns true if this is a pp event
  // Currently this is based on testing if the first 2 particles are
  // initial-state
  bool Event::is_pp() const{
    return (_particles.size()>=2)
      && (_particles[0].is_initial_state()) && (_particles[1].is_initial_state())
      && (_particles[0].is_parton())        && (_particles[1].is_parton());
  }

  /// returns true if this is a DIS event
  /// one incoming parton and one incoming non-parton
  bool Event::is_DIS() const{
    return (_particles.size()>=2)
      && (_particles[0].is_initial_state()) && (_particles[1].is_initial_state())
      && (   (!_particles[0].is_parton()  &&  _particles[1].is_parton())
	        || ( _particles[0].is_parton()  && !_particles[1].is_parton()));
  }


  // returns the (incoming) beam particle that is on the 
  // opposite side to the incoming particle p supplied
  // as an argument
  const Particle & Event::opposite_beam(const Particle & p) const {
    if (p.initial_state() == 1) {
      return _beam2;
    } else if (p.initial_state() == 2) {
      return _beam1;
    } else {
      assert(false && "expected p.initial_state() to be 1 or 2");
    }
  }

  //----------------------------------------------------------------------
  // updates the Born indices
  //
  //GS-NOTE: this is not collinear safe. Do we care?
  void Event::update_Born_index(const int splitter_index) {
    // LuSi: maybe we should look at the rapidity rather than the energy? 
    //       we think it's fine.
    assert(unsigned(splitter_index) < size());
    if (splitter_index == _born1) {
      // get the radiation and check if its energy is larger than the splitter's
      if (_particles[_particles.size()-1].E() > _particles[splitter_index].E()) {
        // then update the index
        // HARDCODED as the last particle, which should be fine
        _born1 = _particles.size()-1;
      }
    } else if (splitter_index == _born2) {
      // get the radiation and check if its energy is larger than the splitter's
      if (_particles[_particles.size()-1].E() > _particles[splitter_index].E()) {
        // then update the index
        // HARDCODED as the last particle, which should be fine
        _born2 = _particles.size()-1;
      }
    }
  }


  //----------------------------------------------------------------------
  // returns true if the next emission of this event can be matched
  bool Event::can_be_matched() const {
    if (is_pp()) return false;
    else {
      if(size() == 2) return true;
      else return false;
    }
  }

  //----------------------------------------------------------------------
  // for initial-state particles, return the PDF fraction along their
  // beam of origin (choice of beam is based on info inside particle p)
  precision_type Event::pdf_x(const Particle &p) const{
    /// this should only ever be called for initial-state particles
    assert(p.is_initial_state());
    // check if the particle is from beam1
    if(is_pp()){
      if (p.initial_state()==1)       // particle from beam1
        return dot_product(p, _beam2)/dot_product(_beam1, _beam2);
      else if (p.initial_state()==2)  // particle from beam2      
        return dot_product(p, _beam1)/dot_product(_beam1, _beam2);
      else throw std::runtime_error("beam for particle was neither 1 nor 2 to but instead "+to_string(p.initial_state()));
    } else if (is_DIS()){
      if(_beam1.pdgid() != 11 && _beam2.pdgid() != 11){
        // below same as dot_product(p, momentum_sum())/dot_product(_beam1, _beam2)
        return dot_product(p, ref_out())/dot_product(_beam1, _beam2);
      } else{
        if (p.initial_state()==1)  // particle from beam1
          return dot_product(p, _beam2)/dot_product(_beam1, _beam2);
        else                       // particle from beam2      
          return dot_product(p, _beam1)/dot_product(_beam1, _beam2);
      }
    } else {
      assert(false && "unknown event!");
    }
  }


  //----------------------------------------------------------------------
  int Event::update_dipole_after_emission(int i_new_particle, int idipole,
                                          int splitter_pdgid_pre_branching,
                                          bool splitter_is_3_end,
                                          int radiation_pdgid){
    
    // first handle the case where the radiation is a gluon
    if (radiation_pdgid==21){
      return insert_gluon_in_dipole_return_index(i_new_particle, idipole);
    } else { // radiation is a quark or anti-quark
      // we have 4 possible cases depending on which dipole end splits
      // and of the flavour of the splitting parton
      if (splitter_is_3_end){ // radiation at the 3 end of the dipole
        // 2 cases here depending on the splitter pdgid
        if (splitter_pdgid_pre_branching == 21){
          // the pre-branching splitter is a gluon
          // i.e. g->qqbar (FSR) or qbarin -> qbarout + gin (ISR)
          return split_gluon2qqbar_at_dipole_q_end(i_new_particle, idipole);
        } else {
          // the pre-branching splitter is an antiquark
          // i.e. gin -> qout + qbarin  (ISR)
          return split_backwards_qbar2gluon(i_new_particle, idipole);          
        }
      } else { // splitting at the 3bar end
        // 2 cases here depending on the splitter PDGid
        if (splitter_pdgid_pre_branching == 21){
          // the pre-branching splitter is a gluon
          // i.e. g->qqbar (FSR) or qin -> qout + gin (ISR)
          return split_gluon2qqbar_at_dipole_qbar_end(i_new_particle, idipole);
        } else {
          // the pre-branching splitter is a quark
          // i.e. gin -> qbarout + qin  (ISR)
          return split_backwards_q2gluon(i_new_particle, idipole);
        }
      }
    }      
  }

  
  //----------------------------------------------------------------------
  /// update indices, etc. associated with the new gluon being
  /// inserted into the dipole. The gluon whose index is passed
  /// is assumed to have been just created, and it is also assumed
  /// that the other particles associated with it have also 
  /// had their momenta updated.
  ///
  /// It returns the index of the new dipole
  int Event::insert_gluon_in_dipole_return_index(int igluon, int idipole) {
    // create the dipole -- and then worry about updating it...
    unsigned new_dipole_index = add_dipole_return_index(Dipole());

    Dipole & existing_dipole = dipoles()[idipole];
    Dipole & new_dipole      = dipoles()[new_dipole_index];
    
    // suppose we started off with a u-ubar dipole and we are creating a gluon
    // - That original dipole index will be assigned to the u-g dipole
    //   (with the gluon being the qbar-like part)
    // - The new dipole index will be assigned to the g-ubar dipole (the
    //   gluon is the quark-like part)
    new_dipole.index_q    = igluon;
    new_dipole.index_qbar = existing_dipole.index_qbar;
    existing_dipole.index_qbar = igluon;

    // the new dipole needs to be inserted in the chain
    new_dipole.index_previous  = idipole;
    new_dipole.index_next      = existing_dipole.index_next;
    // the existing dipole needs to point to it
    existing_dipole.index_next = new_dipole_index;
    // if there was a valid next dipole, it needs to point to the 
    // new one (This also updates non-splitting dipoles)
    Dipole * next_dipole = new_dipole.dipole_at_3bar_end(true);
    if (next_dipole){
      next_dipole->index_previous = new_dipole_index;
    }

    // update the list of touched dipoles
    // this includes the newly created dipole (goes first)
    //               the initial dipole (goes second)
    //               the previous and next dipoles (go at the end)
    // Note: this does not cover global showers
    _touched_dipole_indices.clear();
    _touched_dipole_indices.push_back(new_dipole_index);
    _touched_dipole_indices.push_back(idipole);
    if (existing_dipole.index_previous>=0) _touched_dipole_indices.push_back(existing_dipole.index_previous);
    if (new_dipole.index_next         >=0) _touched_dipole_indices.push_back(new_dipole.index_next);
    
    return new_dipole_index;
  }
  
  //----------------------------------------------------------------------
  // Arrange internal indices for case where a gluon at the quark-like
  // end of a dipole splits to qqbar. 
  //
  // - the dipole that is splitting is idipole
  // - the splitting gluon has its quark end in the idipole dipole,
  //   its qbar end in the "previous" dipole in the chain
  // - iqbar is the index of a new particle which will be the
  //   anti-quark produced in the splitting (goes w previous dipole)
  // - the q particle from the splitting is assumed to be
  //   dipoles[idipole].index_q
  // - we assume that PDG IDs have been correctly set (by the calling
  //   routine) for the q and qbar
  //
  // A "non_splitting_dipole" is created (at the end of the
  // event.nont_splitting_dipole list). It is such that:
  // - its q and qbar end correspond respectively to the qbar and q
  //   products of the splitting
  // - the existing dipole "index_previous" points to the non_splitting dipole
  // - the previous dipole "index_next" points to the non_splitting dipole
  // - the non_splitting dipole "index_previous" points to the previous dipole
  // - the non_splitting dipole "index_next" points to idipole
  //
  // it returns the index of the "previous" dipole
  int Event::split_gluon2qqbar_at_dipole_q_end(int iqbar, int idipole) {
    // grab hold of the current dipole
    Dipole & existing_dipole = dipoles()[idipole];

    // make sure working assumptions are satisfied
    int pdgid_q    = _particles[existing_dipole.index_q].pdgid();
    int pdgid_qbar = _particles[iqbar].pdgid();
    //assert((pdgid_q>0) && (pdgid_q+pdgid_qbar==0));
    if (pdgid_q>0) assert(pdgid_q+pdgid_qbar==0);   // FSR
    else           assert(pdgid_q==pdgid_qbar);     // ISR
    
    // if the q end of a gluon is splitting, this implies there has 
    // to be a valid (>=0) index_previous for this dipole
    int idipole_previous = existing_dipole.index_previous;
    assert(idipole_previous >=0);
    Dipole & previous_dipole = dipoles()[idipole_previous];

    // create a non_splitting dipole
    unsigned non_splitting_dipole_index = add_non_splitting_dipole_return_index(Dipole());
    Dipole & non_splitting_dipole = non_splitting_dipoles()[non_splitting_dipole_index];
    int non_splitting_dipole_signed_index = -2-non_splitting_dipole_index;

    // ensure that the previous dipole knows about the new particle,
    // which corresponds to the qbar end of that dipole
    previous_dipole.index_qbar     = iqbar;
    // the previous dipole "next" points to the non-splitting dipole
    previous_dipole.index_next     = non_splitting_dipole_signed_index;
    // similarly for the previous dipole (3 end) of the existing dipole
    existing_dipole.index_previous = non_splitting_dipole_signed_index;

    // set the information for the non-splitting dipole
    //
    // has its 3/3bar end pointing to the qbar/q respectively
    non_splitting_dipole.index_q    = previous_dipole.index_qbar;
    non_splitting_dipole.index_qbar = existing_dipole.index_q;
    // and has its next/previous dipoles pointing to the
    // previous/existing dipoles
    non_splitting_dipole.index_previous = idipole_previous;
    non_splitting_dipole.index_next     = idipole;

    // reset the list of dipoles whose kinematics (may) have changed,
    // keeping in mind that recoil affects the emitter and spectator
    // so next_dipole (i.e. other dipole associated with a gluon at the
    // qbar end of existing dipole) needs updating as well.
    _touched_dipole_indices.clear();
    _touched_dipole_indices.push_back(idipole);
    _touched_dipole_indices.push_back(idipole_previous);
    if (existing_dipole.index_next >=0) _touched_dipole_indices.push_back(existing_dipole.index_next);

    // for use with update_event_gen
    return non_splitting_dipole_signed_index;
  }

  
  //----------------------------------------------------------------------
  // Arrange internal indices for case where a gluon at the qbar-like
  // end of a dipole splits to qqbar. 
  //
  // - the dipole that is splitting is idipole
  // - the splitting gluon has its qbar end in the idipole dipole,
  //   its quark end in the "next" dipole in the chain
  // - iq is the index of a new particle which will be the
  //   quark produced in the splitting (goes w "next" dipole)
  // - the qbar particle from the splitting is assumed to be
  //   dipoles[idipole].index_qbar
  // - we assume that PDG IDs have been correctly set (by the calling
  //   routine) for the q and qbar
  //
  // A "non_splitting_dipole" is created (at the end of the
  // event.non_splitting_dipole list). It is such that:
  // - its q and qbar end correspond respectively to the qbar and q
  //   products of the splitting
  // - the existing dipole "index_next" points to the non_splitting dipole
  // - the previous dipole "index_previous" points to the non_splitting dipole
  // - the non_splitting dipole "index_next" points to the previous dipole
  // - the non_splitting dipole "index_previous" points to idipole
  //
  // it returns the index of the "next" dipole
  int Event::split_gluon2qqbar_at_dipole_qbar_end(int iq, int idipole) {
    Dipole & existing_dipole = dipoles()[idipole];

    // make sure working assumptions are satisfied
    int pdgid_qbar = _particles[existing_dipole.index_qbar].pdgid();
    int pdgid_q    = _particles[iq].pdgid();
    if (pdgid_qbar<0) assert(pdgid_q+pdgid_qbar==0); // FSR
    else              assert(pdgid_q==pdgid_qbar);   // ISR

    // if the qbar end of a gluon is splitting, this implies there has 
    // to be a valid (>=0) index_next for this dipole
    int idipole_next = existing_dipole.index_next;
    assert(idipole_next >=0);
    Dipole & next_dipole = dipoles()[idipole_next];

     // create a non_splitting dipole
    unsigned non_splitting_dipole_index = add_non_splitting_dipole_return_index(Dipole());
    Dipole & non_splitting_dipole = non_splitting_dipoles()[non_splitting_dipole_index];
    int non_splitting_dipole_signed_index = -2-non_splitting_dipole_index;
    
    // ensure that the next dipole knows about the new particle,
    // which corresponds to the q end of that dipole
    next_dipole.index_q        = iq;
    // the next dipole "previous" should point to the non-splitting diopole
    next_dipole.index_previous = non_splitting_dipole_signed_index;
    // similarly for the next dipole (3bar end) of the existing dipole
    existing_dipole.index_next = non_splitting_dipole_signed_index;

    // set the information for the non-splitting dipole
    //
    // has its 3/3bar end pointing to the qbar/q respectively
    non_splitting_dipole.index_q    = existing_dipole.index_qbar;
    non_splitting_dipole.index_qbar = next_dipole.index_q;
    // and has its next/previous dipoles pointing to the
    // previous/existing dipoles
    non_splitting_dipole.index_previous = idipole;
    non_splitting_dipole.index_next     = idipole_next;

    // reset the list of dipoles whose kinematics (may) have changed,
    // keeping in mind that recoil affects the emitter and spectator
    // so next_dipole (i.e. other dipole associated with a gluon at the
    // qbar end of existing dipole) needs updating as well.
    _touched_dipole_indices.clear();
    _touched_dipole_indices.push_back(idipole);
    _touched_dipole_indices.push_back(idipole_next);
    if (existing_dipole.index_previous >=0) _touched_dipole_indices.push_back(existing_dipole.index_previous);

    // for use with update_event_gen
    return non_splitting_dipole_signed_index;
  }

  // Arrange internal indices for cases where a quark in the initial
  // state (i.e. at the 3bar end of a dipole) evolves backwards so
  // that the new incoming parton is a gluon, with emission of an
  // anti-quark in the final state, i.e. we have a splitting
  //   g_in -> q_in + qbar_out
  //
  // This does several things:
  //   - the 3bar end of the existing dipole becomes the IS gluon
  //   - a new dipole is created with the IS gluon as its 3 end and
  //     the emitted qbar at its 3bar end
  //   - the existing "next" dipole is set to the newly created dipole
  //   - the new dipole "previous" diopole is the existing one
  //   - if the existing dipole had a (non-splitting) "next" dipole:
  //     . it becomes the "next" of the new dipole
  //     . its "previous" points to the new dipole
  int Event::split_backwards_q2gluon(int iqbar, int idipole) {
    // create a new dipole
    unsigned new_dipole_index = add_dipole_return_index(Dipole());

    Dipole & existing_dipole = dipoles()[idipole];
    Dipole & new_dipole      = dipoles()[new_dipole_index];
    Dipole * next_dipole     = existing_dipole.dipole_at_3bar_end(true);

    // update the dipole legs (particles)
    new_dipole.index_q    = existing_dipole.index_qbar;
    new_dipole.index_qbar = iqbar;
    if (next_dipole){ next_dipole->index_q = iqbar; }

    // now update the dipole connections
    new_dipole.index_previous  = idipole;
    new_dipole.index_next      = existing_dipole.index_next;
    existing_dipole.index_next = new_dipole_index;
    if (next_dipole) { next_dipole->index_previous = new_dipole_index; }

    // reset the list of dipoles whose kinematics (may) have changed,
    // Note that the next dipole, if any, is a non-splitting dipole
    // and so does not need to be updated.
    _touched_dipole_indices.clear();
    _touched_dipole_indices.push_back(new_dipole_index);
    _touched_dipole_indices.push_back(idipole);
    if (existing_dipole.index_previous >=0) _touched_dipole_indices.push_back(existing_dipole.index_previous);

    return new_dipole_index;
  }
  
  // Arrange internal indices for cases where an anti quark in the
  // initial state (i.e. at the 3 end of a dipole) evolves backwards
  // so that the new incomimg parton is a gluon, with emission of an
  // quark in the final state, i.e. we have a splitting
  //   g_in -> qbar_in + q_out
  //
  // This does several things:
  //   - the 3 end of the existing dipole becomes the IS gluon
  //   - a new dipole is created with the IS gluon as its 3bar end
  //     and the emitted q at its 3 end
  //   - the existing "previous" dipole is set to the new dipole
  //   - the new dipole "next" diopole is the existing one
  //   - if the existing dipole had a (non-splitting) "previous" dipole:
  //     . it becomes the "previous" of the new dipole
  //     . its "next" points to the new dipole
  int Event::split_backwards_qbar2gluon(int iq, int idipole) {
    // create a new dipole
    unsigned new_dipole_index = add_dipole_return_index(Dipole());

    Dipole & existing_dipole = dipoles()[idipole];
    Dipole & new_dipole      = dipoles()[new_dipole_index];
    Dipole * previous_dipole = existing_dipole.dipole_at_3_end(true);
    
    // update the dipole leges (particles)
    new_dipole.index_q    = iq;
    new_dipole.index_qbar = existing_dipole.index_q;
    if (previous_dipole){ previous_dipole->index_qbar = iq; }

    // now update the dipole connections
    new_dipole.index_previous  = existing_dipole.index_previous;
    new_dipole.index_next      = idipole;
    existing_dipole.index_previous = new_dipole_index;
    if (previous_dipole) { previous_dipole->index_next = new_dipole_index; }

    // reset the list of dipoles whose kinematics (may) have changed,
    // Note that the previous dipole, if any, is a non-splitting dipole
    // and so does not need to be updated.
    _touched_dipole_indices.clear();
    _touched_dipole_indices.push_back(new_dipole_index);
    _touched_dipole_indices.push_back(idipole);
    if (existing_dipole.index_next >=0) _touched_dipole_indices.push_back(existing_dipole.index_next);

    return new_dipole_index;
  }

  //----------------------------------------------------------------------
  // print the particles in the event following the dipole chains
  // for simplicity we start with the q end of the first dipole
  void Event::print_following_dipoles() const{
    std::cout << "EVENT RECORD" << std::endl;
    std::cout << "barcode part_index      px           py             pz            E" << std::endl;
    std::cout << "-------------------------------------------------------------------" << std::endl;
    std::vector<bool> handled(_dipoles.size(), false);
    std::vector<bool> particles_handled(_particles.size(), false);

    // we start from the 1st dipole
    for (unsigned int idipole_start=0; idipole_start<_dipoles.size(); ++idipole_start){
      // check if already dealt with
      if (handled[idipole_start]) continue;

      // start circling from here
      unsigned int idipole = idipole_start;
      do {
        const auto & dipole = _dipoles[idipole];
        const auto & q = _particles[dipole.index_q];

        // print the q end of the current dipole
        std::cout << std::setw(7) << q.barcode() 
                  << " " << std::setw(10) << dipole.index_q << " " << q << std::endl;
        particles_handled[dipole.index_q] = true;

        // print full-colour transitions
        std::cout << "     " << dipole.colour_transitions
                  << "_(" << dipole.index_qbar << "," << dipole.index_q << ")"
                  << std::endl;
        // mark the current dipole as handled
        handled[idipole] = true;

        // if possible, do to the next dipole
        //
        // if the next dipole is a non-splitting dipole, go through it
        // to get the next "real" dipole. If we are at the end of a
        // dipole chain, just terminate.
        if (dipole.index_next>=0){
          idipole = dipole.index_next;
        } else{
          // we've reached the end of a q...qbar series.
          // Print the qbar...
          const auto & qbar = _particles[dipole.index_qbar];
          std::cout << std::setw(7) << qbar.barcode() 
                    << " " << std::setw(10) << dipole.index_qbar << " " << qbar << std::endl;
          particles_handled[dipole.index_qbar] = true;

          // ... and search for the next q which is either taken from
          // a connected qqbar splitting, or somewhere else in the
          // event
          if (dipole.index_next<-1){
            // inext = -2-i_non_splitting  =>  i_non_splitting = -2-inext
            //
            // We simply skip through the non-splitting dipole
            // Note that we should print the particle associated with the end of the chain
            int index_non_splitting = -2 - dipole.index_next;
            assert(index_non_splitting>=0);
            idipole = _non_splitting_dipoles[index_non_splitting].index_next;
            assert(idipole>=0);
          } else {          
            break;
          }
        }
      } while (!handled[idipole]);
    }
    // next handle the non-dipole particles (e.g. electroweak particles)
    for (unsigned i = 0; i < _particles.size(); i++) {
      if (!particles_handled[i]) {
        std::cout << "ndp" << std::setw(4) 
                  << _particles[i].barcode() 
                  << " " << std::setw(10) << i << " "
                  << _particles[i] << std::endl;
      }
    }
    // print the PDF fractions if pp
    if (is_pp()){
      std::cout << "PDF fractions      (" << pdf_x_beam1() << ", " << pdf_x_beam2() << ")" << std::endl;
    }
    // finally give a summary of momenta in / out
    std::cout << "sum partons in     " << momentum_partons_in() << std::endl;
    std::cout << "sum p out          " << momentum_sum()        << std::endl;
    std::cout << std::endl;
  }

  // run a bunch of consistency tests on the event
  bool Event::check_dipoles_pdgids() const{
    // check that 3 and 3bar ends of dipoles have an acceptable PDGID
    for (const auto & dipole : _dipoles){
      const auto & p3  = dipole.particle_3();
      const auto & id3 = p3.pdgid();
      if (id3 == 21){
        // if the particle is a gluon, we should have a "previous" dipole
        if (dipole.index_previous<0){
          std::cerr << "Event::check_dipoles_pdgids: found a gluon at the dipole 3 end (dipole [3bar, 3]=["<<dipole.index_qbar << "," <<dipole.index_q<<"]) with no previous dipole" << std::endl;
          return false;
        }
      } else if (id3 < 0){
        // if the particle is an anti-quark we should (i) be in the
        // initial-state and (ii) have no splitting previous dipole
        if (! p3.is_initial_state()){
          std::cerr << "Event::check_dipoles_pdgids: found an anti-quark at the dipole 3 end  (dipole [3bar, 3]=["<<dipole.index_qbar << "," <<dipole.index_q<<"]) in the final state" << std::endl;
          return false;
        }
        if (dipole.index_previous>=0){
          std::cerr << "Event::check_dipoles_pdgids: found an anti-quark at the dipole 3 end  (dipole [3bar, 3]=["<<dipole.index_qbar << "," <<dipole.index_q<<"]) with a splitting previous dipole" << std::endl;
          return false;
        }
      } else { // id3>0, quark
        // if the particle is a quark we should (i) be in the
        // final-state and (ii) have no splitting previous dipole
        if (p3.is_initial_state()){
          std::cerr << "Event::check_dipoles_pdgids: found a quark at the dipole 3 end  (dipole [3bar, 3]=["<<dipole.index_qbar << "," <<dipole.index_q<<"]) in the initial state" << std::endl;
          return false;
        }
        if (dipole.index_previous>=0){
          std::cerr << "Event::check_dipoles_pdgids: found a quark at the dipole 3 end  (dipole [3bar, 3]=["<<dipole.index_qbar << "," <<dipole.index_q<<"]) with a splitting previous dipole" << std::endl;
          return false;
        }
      }
    
      // now repeat the same set of tests at the 3bar end of the dipole        
      const auto & p3b  = dipole.particle_3bar();
      const auto & id3b = p3b.pdgid();
      if (id3b == 21){
        // if the particle is a gluon, we should have a "next" dipole
        if (dipole.index_next<0){
          std::cerr << "Event::check_dipoles_pdgids: found a gluon at the dipole 3bar end (dipole [3bar, 3]=["<<dipole.index_qbar << "," <<dipole.index_q<<"]) with no next dipole" << std::endl;
          return false;
        }
      } else if (id3b < 0){
        // if the particle is a quark we should (i) be in the
        // final-state and (ii) have no splitting next dipole
        if (p3b.is_initial_state()){
          std::cerr << "Event::check_dipoles_pdgids: found a quark at the dipole 3bar end (dipole [3bar, 3]=["<<dipole.index_qbar << "," <<dipole.index_q<<"]) in the initial state" << std::endl;
          return false;
        }
        if (dipole.index_next>=0){
          std::cerr << "Event::check_dipoles_pdgids: found a quark at the dipole 3bar end (dipole [3bar, 3]=["<<dipole.index_qbar << "," <<dipole.index_q<<"]) with a splitting next dipole" << std::endl;
          return false;
        }
      } else { // id3b>0, quark
        // if the particle is a quark we should (i) be in the
        // initial-state and (ii) have no splitting previous dipole
        if (! p3b.is_initial_state()){
          std::cerr << "Event::check_dipoles_pdgids: found a quark at the dipole 3bar end (dipole [3bar, 3]=["<<dipole.index_qbar << "," <<dipole.index_q<<"]) in the final state" << std::endl;
          return false;
        }
        if (dipole.index_next>=0){
          std::cerr << "Event::check_dipoles_pdgids: found a quark at the dipole 3bar end (dipole [3bar, 3]=["<<dipole.index_qbar << "," <<dipole.index_q<<"])  with a splitting next dipole" << std::endl;
          return false;
        }
      }
    }      

    return true;
  }

// create an ee event
//
// for now assumes that p1 is quark-like and p2 is anti-quark like
// and create a dipole associated with that pair; or alternatively that
// p1 and p2 are both gluon-like, in which case it creates two
// dipoles.
Event create_ee2X_event(const Particle & beam1, const Particle & beam2,
                        const Particle & p1_out, const Particle &p2_out,
                        const vector<Particle> &additions) {

  std::vector<Dipole> dipoles;

  if (p1_out.pdgid() == 21 && p2_out.pdgid() == 21) {
    // Handle net colour singlet gluon-gluon system here
    Dipole dipole0, dipole1;
    dipole0.index_q    = 0;
    dipole0.index_qbar = 1;
    dipole0.index_previous = 1;
    dipole0.index_next     = 1;
    dipole0.colour_transitions = ColourTransitionVector(false);
    
    dipole1.index_q    = 1;
    dipole1.index_qbar = 0;
    dipole1.index_previous = 0;
    dipole1.index_next     = 0;
    dipole1.colour_transitions = ColourTransitionVector(false);

    dipoles.push_back(dipole0);
    dipoles.push_back(dipole1);
    
  } else if (1 <= abs(p1_out.pdgid()) &&  abs(p1_out.pdgid()) <= 6 && p1_out.pdgid() + p2_out.pdgid() == 0) {
    // Handle colour & flavour singlet qqbar system here (W's will come later...)

    // Create a dipole 
    Dipole dipole;
    // arrange the q-like and anti-q-like partons properly in the dipole
    if (p1_out.pdgid() > 0) {
      dipole.index_q    = 0;
      dipole.index_qbar = 1;
    } else {
      dipole.index_q    = 1;
      dipole.index_qbar = 0;
    }
    dipole.index_previous = -1;
    dipole.index_next     = -1;
    dipole.colour_transitions = ColourTransitionVector(true);

    dipoles.push_back(dipole);
  } else {
    std::cerr << "Could not handle structure of initial event" << std::endl;
    std::cerr << p1_out << std::endl;
    std::cerr << p2_out << std::endl;
    exit(-1);
  }

  // finally return the event
  vector<Particle> particles{p1_out, p2_out};
  std::copy(additions.begin(), additions.end(), std::back_inserter(particles));
  return Event(particles, dipoles, beam1, beam2);
}

// create a pp event
//
// supports qq > Z and gg > h
Event create_pp2X_event(const Particle & beam1, const Particle & beam2, 
                        const Particle & p1_in, const Particle & p2_in) {

  // if the pdgids sum to the magic number 42 we have
  // two gluons in the initial state
  bool gluon_is = (p1_in.pdgid() + p2_in.pdgid() == 42);
  
  std::vector<Particle> particles{p1_in, p2_in};

  /// make sure these properties are set
  particles[0].set_initial_state(1);  /// from beam1
  particles[1].set_initial_state(2);  /// from beam2
  // explicitly set the initial state particles 
  // not as part of the hard system
  particles[0].set_hard_system(false);  
  particles[1].set_hard_system(false); 

  // first check whether X is a Z boson or a H
  // default is qqbar, this case it is a Z-boson
  int pdgid_boson = 23;
  // if gg we put the pdgid_boson to 25 (Higgs boson)
  if(gluon_is){
    pdgid_boson = 25;
  }
  
  // add the X, which is part of the hard system
  particles.push_back(Particle(p1_in + p2_in, pdgid_boson));
  particles[2].set_hard_system(true);
  
  // now we need to add the initial dipoles
  // there are two in the case of gg scattering
  // first create the common [0, 1] 
  Dipole dipole;
  // our assumption is that particle 1 is the antiquark
  // (i.e. 3-end of dipole, in our labelling of outgoing colour)
  // if (!gluon_is) assert(p1_in.pdgid() < 0);
  // we swap the indices below if p1_in.pdgid() > 0
  dipole.index_q    = 0;
  dipole.index_qbar = 1;
  dipole.index_previous = -1;
  dipole.index_next     = -1;
  dipole.colour_transitions = ColourTransitionVector(true);
  if(gluon_is){
    dipole.index_previous = 1;
    dipole.index_next     = 1;
    dipole.colour_transitions = ColourTransitionVector(false);
  } else if (p1_in.pdgid() > 0){
    // recall that an incoming quark corresponds to the qbar end of a dipole  
    // but only for a qqbar system (otherwise no need to swap)
    std::swap(dipole.index_q, dipole.index_qbar);
  }
  
  std::vector<Dipole> dipoles{dipole};
  
  // now we add the second dipole in case of a gg initial state
  if(gluon_is){
    Dipole dipole1;
    dipole1.index_q    = 1;
    dipole1.index_qbar = 0;
    dipole1.index_previous = 0;
    dipole1.index_next     = 0;
    dipole1.colour_transitions = ColourTransitionVector(false);
    dipoles.push_back(dipole1);
  }

  // finally return the event
  return Event(particles, dipoles, beam1, beam2);

}

// create DIS event
Event create_DIS_event(const Particle & beam1, const Particle & beam2, 
                        const Particle & p1_in, const Particle & p2_out) {

  // beam1 = photon
  // beam2 = proton
  // p1_in = incoming (anti-)quark
  // p2_in = outgoing (anti-)quark

  std::vector<Particle> particles{beam1, p1_in, p2_out};

  /// make sure these properties are set
  particles[0].set_initial_state(1);  /// this is the photon
  particles[1].set_initial_state(2);  /// this is the quark 
  particles[2].set_hard_system(true);
  
  
  // // now we need to add the initial dipoles
  // // there are two in the case of gg scattering
  // // first create the common [0, 1] 
  Dipole dipole;
  // our assumption is that particle 1 is the quark
  // (i.e. 3-end of dipole, in our labelling of outgoing colour)
  // we swap the indices below if p1_in.pdgid() < 0
  dipole.index_q    = 2; // = outgoing particle
  dipole.index_qbar = 1; // = incoming particle
  dipole.index_previous = -1;
  dipole.index_next     = -1;
  dipole.colour_transitions = ColourTransitionVector(true);
  // this sets the first DIS chain - store that in the particle information
  particles[dipole.index_q]   .set_DIS_chain(1);
  particles[dipole.index_qbar].set_DIS_chain(1);

  if (p1_in.pdgid() < 0){
    // recall that an incoming antiquark corresponds to the q end of a dipole 
    std::swap(dipole.index_q, dipole.index_qbar);
  }
  
  std::vector<Dipole> dipoles{dipole};
  
  // finally return the event
  return Event(particles, dipoles, beam1, beam2);

}


// create VBF event
Event create_VBF_event(const Particle & beam1, const Particle & beam2, 
                         const Particle & p1_in, const Particle & p2_in,
                         const Particle & pH,    
                         const Particle & pj1, const Particle & pj2) {

  std::vector<Particle> particles{p1_in, p2_in, pH, pj1, pj2};

  /// make sure these properties are set
  particles[0].set_initial_state(1);  
  particles[1].set_initial_state(2);  
  // higgs, j1 and j2 are all part of the hard system
  particles[2].set_hard_system(true);
  particles[3].set_hard_system(true);
  particles[4].set_hard_system(true);
  
  
  // now we need to add the initial dipoles - there are two
  Dipole dipole1;
  // this corresponds to the case that we have incoming quarks
  dipole1.index_q    = ((pj1.pdgid() < 0) == (p1_in.pdgid() < 0)) ? 3 : 4; // = outgoing particle
  dipole1.index_qbar = 0; // = incoming particle
  dipole1.index_previous = -1;
  dipole1.index_next     = -1;
  dipole1.colour_transitions = ColourTransitionVector(true);
  // this sets the first DIS chain - store that in the particle information
  particles[dipole1.index_q]   .set_DIS_chain(1);
  particles[dipole1.index_qbar].set_DIS_chain(1);
  // if this is not the case, swap
  if (p1_in.pdgid() < 0){
    // recall that an incoming antiquark corresponds to the q end of a dipole 
    std::swap(dipole1.index_q, dipole1.index_qbar);
  }
  Dipole dipole2;
  // this corresponds to the case that we have incoming quarks
  dipole2.index_q    = ((pj2.pdgid() < 0) == (p2_in.pdgid() < 0)) ? 4 : 3; // = outgoing particle
  dipole2.index_qbar = 1; // = incoming particle
  dipole2.index_previous = -1;
  dipole2.index_next     = -1;
  dipole2.colour_transitions = ColourTransitionVector(true);
  particles[dipole2.index_q]   .set_DIS_chain(2);
  particles[dipole2.index_qbar].set_DIS_chain(2);
  // if this is not the case, swap
  if (p2_in.pdgid() < 0){
    // recall that an incoming antiquark corresponds to the q end of a dipole 
    std::swap(dipole1.index_q, dipole1.index_qbar);
  }
  
  std::vector<Dipole> dipoles{dipole1, dipole2};
  
  // set up the event
  auto event = Event(particles, dipoles, beam1, beam2);
  // and also the reference vectors for the two DIS chains
  Momentum & p1 = (particles[dipole1.index_q].is_initial_state()) ? particles[dipole1.index_q]    : particles[dipole1.index_qbar];
  Momentum & p2 = (particles[dipole1.index_q].is_initial_state()) ? particles[dipole1.index_qbar] : particles[dipole1.index_q];
  event.set_incoming_ref_momenta(p1, p2, 1);

  p1 = (particles[dipole2.index_q].is_initial_state()) ? particles[dipole2.index_q]    : particles[dipole2.index_qbar];
  p2 = (particles[dipole2.index_q].is_initial_state()) ? particles[dipole2.index_qbar] : particles[dipole2.index_q];
  event.set_incoming_ref_momenta(p1, p2, 2);

  return event;

}


// create a pp->Zj(hj) event [under development]
Event create_pp2Xj_event(const Particle & beam1, const Particle & beam2, 
                         const Particle & p1_in, const Particle & p2_in,
                         const Particle & pX,    const Particle & pj_out) {
  
  PRINT_EXPERIMENTAL("create_pp2Xj_event");
  
  // if the pdgids sum to the magic number 42 we have
  // two gluons in the initial state
  bool gluon_is = (p1_in.pdgid() + p2_in.pdgid() == 42); 
  // also check whether a gluon exists in the final state
  bool gluon_fs = (pj_out.pdgid() == 21); 
  // we then have:
  // 1. !gluon_is && gluon_fs: qqbar -> Xg event
  // 2. !gluon_is && !gluon_fs: qg -> Xq or qbarg->Xqbar event
  // 3. gluon_is && gluon_fs: gg -> Xg event
  //assert(!gluon_is && "Cannot handle the dipole setup of these gg->hg events");
  // 4. gluon_is && !gluon_fs -> cannot exist with 3 coloured particles
  assert(!(gluon_is && !gluon_fs) && "Wrong event setup!");
  
  std::vector<Particle> particles{p1_in, p2_in};

  /// make sure these properties are set
  particles[0].set_initial_state(1);  /// from beam1
  particles[1].set_initial_state(2);  /// from beam2
  // explicitly set the initial state particles 
  // not as part of the hard system
  particles[0].set_hard_system(false);  
  particles[1].set_hard_system(false); 
  // add the X, which is part of the hard system
  particles.push_back(pX);
  particles[2].set_hard_system(true);
  particles.push_back(pj_out);
  particles[3].set_hard_system(true); 

  // now we need to add the initial dipoles
  // the latter dipole is only used if we are dealing with an all-gluonic partonic state
  Dipole dipole1, dipole2, dipole3;
  // needed for the dipole transition points
  Momentum Q = (particles[2] + particles[3]).p4();
  precision_type Q2 = Q.m2();
  // and also the angles (= Ei Ej (1-cos theta_ij) assuming massless momenta)
  precision_type p1_dot_Q  = dot_product(Q, p1_in);      
  precision_type p2_dot_Q  = dot_product(Q, p2_in);     
  precision_type pj_dot_Q  = dot_product(Q, pj_out);    
  //precision_type p1_dot_p2 = dot_product(p1_in, p2_in); 
  precision_type p1_dot_pj = dot_product(pj_out, p1_in); 
  precision_type p2_dot_pj = dot_product(pj_out, p2_in); 

  // setting up the dipoles and transitions depends on the partonic structure
  if(!gluon_is && gluon_fs){  // qqbar -> Xg event
    // this means that for the first dipole we get
    dipole1.index_q    = 0; // initial-state anti-quark
    dipole1.index_qbar = 3; // gluon
    dipole1.index_previous = -1; // anti-quark end
    dipole1.index_next     = 1; // this is the next dipole, and starts with the gluon
    dipole1.colour_transitions = ColourTransitionVector(false); //qbar end is not CF
    // for the second dipole
    dipole2.index_q        = 3; // gluon
    dipole2.index_qbar     = 1; // initial-state quark
    dipole2.index_previous = 0;
    dipole2.index_next     = -1;
    dipole2.colour_transitions = ColourTransitionVector(true); //qbar end is CF

    // swap q and qbar if this is the case
    if (p1_in.pdgid() > 0){
      dipole1.index_q        = 1; // anti-quark
      dipole2.index_qbar     = 0; // quark
    }
    
    // now we are going to insert the segments 
    // the current set of dipoles is
    // d1 = [-inf, CA, inf]_(gqbar)
    // d2 = [-inf, CF, inf]_(qg)
    // we need to get two dipoles with 
    // d1 = [-inf, CA, etag^d1, CF, inf]_(gqbar)  and 
    // d2 = [-inf, CF, etag^d2, CA, inf]_(qg) 
    // where remember the 3bar-end is at -inf
    // we need to determine etag^d1 and etag^d2
    // first we need the angles theta_qg and theta_qbarg
    // p1_dot_Q  = pqbar.Q
    // p2_dot_Q  = pq.Q
    // pj_dot_Q  = pg.Q
    // p1_dot_pj = pqbar.pg
    // p2_dot_pj = pq.pg
    if (p1_in.pdgid() > 0){ // turn around the dot products if this is the case
      std::swap(p1_dot_Q , p2_dot_Q);
      std::swap(p1_dot_pj, p2_dot_pj);
    }
    // compute angles w.r.t. the 3 (=qbar) and 3bar (q) end of the dipoles
    precision_type omcos_theta3g    = Q2 / p1_dot_Q / pj_dot_Q * p1_dot_pj;
    precision_type omcos_theta3barg = Q2 / p2_dot_Q / pj_dot_Q * p2_dot_pj;

    // now get the rapidity
    double etag;
    // if theta3g < theta3barg -> omcos_theta3g > omcos_theta3barg
    if(omcos_theta3g > omcos_theta3barg){
      // closer to 3-end of dipole (theta3g < theta3barg)
      etag = to_double(- 0.5 * abs(log(omcos_theta3g/(2-omcos_theta3g))));
    } else{
      // closer to 3bar-end of dipole (theta3g > theta3barg)
      etag = to_double(0.5 * abs(log(omcos_theta3barg/(2-omcos_theta3barg))));
    }

    // determine the transition points
    double etagd1 = std::min(0.0, etag); // right of CA 
    double etagd2 = std::max(0.0, etag); // left of CA

    // finally add them to the pre-existing dipoles
    // ColourTransitionPoint(double eta_in,
    //                           const Momentum3<precision_type> &dirdiff_in,
    //                           bool is_dirdiff_wrt_3bar_in)
    dipole1.colour_transitions.push_back(ColourTransitionPoint(etagd1));
    dipole2.colour_transitions.push_back(ColourTransitionPoint(etagd2));
  } else if(!gluon_is && !gluon_fs){ // qg -> Xq or qbarg -> Xqbar event 
    // first set up the dipoles
    if(p1_in.pdgid() > 0 && p2_in.pdgid() > 0){ // qg event
      // first dipole stretches between two initial-states
      dipole1.index_q    = 0; // gluon
      dipole1.index_qbar = 1; // initial-state quark
      dipole1.index_previous = 1; // the gluon end
      dipole1.index_next     = -1; 
      dipole1.colour_transitions = ColourTransitionVector(true); //qbar end is CF
      // for the second dipole
      dipole2.index_q        = 3; // final-state quark
      dipole2.index_qbar     = 0; // gluon
      dipole2.index_previous = -1;
      dipole2.index_next     = 0; // gluon end
      dipole2.colour_transitions = ColourTransitionVector(false); //qbar end is not CF
      if (p2_in.pdgid() == 21){
        // swap the relevant indices when p2 is the gluon instead of p1
        std::swap(dipole1.index_q, dipole1.index_qbar);
        dipole2.index_qbar     = 1; // gluon
      }

      // now insert the segments

      // now we are going to insert the segments 
      // the current set of dipoles is
      // d1 = [-inf, CF, inf]_(qIg)
      // d2 = [-inf, CA, inf]_(gqF)
      // we need to get two dipoles with 
      // d1 = [-inf, CF, etaq^d1, CA, inf]_(qIg)  and 
      // d2 = [-inf, CA, etaq^d2, CF, inf]_(gqF) 
      // determine the inner products
      // p1_dot_Q  = pg.Q
      // p2_dot_Q  = pqI.Q
      // pj_dot_Q  = pqF.Q
      // p1_dot_pj = pg.pqF
      // p2_dot_pj = pqI.pqF

      if (p2_in.pdgid() == 21){ // turn around the dot products if this is the case
      std::swap(p1_dot_Q , p2_dot_Q);
      std::swap(p1_dot_pj, p2_dot_pj);
      }

      // compute angles w.r.t. the 3 (=g) and 3bar (q) end of the dipoles
      precision_type omcos_theta3qF    = Q2 / p1_dot_Q / pj_dot_Q * p1_dot_pj;
      precision_type omcos_theta3barqF = Q2 / p2_dot_Q / pj_dot_Q * p2_dot_pj;

      // now get the rapidity
      double etaq;
      // if theta3g < theta3barg -> omcos_theta3g > omcos_theta3barg
      if(omcos_theta3qF > omcos_theta3barqF){
        // closer to 3-end of dipole (theta3g < theta3barg)
        etaq = to_double(- 0.5 * abs(log(omcos_theta3qF/(2-omcos_theta3qF))));
      } else{
        // closer to 3bar-end of dipole (theta3g > theta3barg)
        etaq = to_double(0.5 * abs(log(omcos_theta3barqF/(2-omcos_theta3barqF))));
      }

      // determine the transition points
      double etaqd1 = std::max(0.0, etaq); // left of CA 
      double etaqd2 = std::min(0.0, etaq); // right of CA

      // finally add them to the pre-existing dipoles
      dipole1.colour_transitions.push_back(ColourTransitionPoint(etaqd1));
      dipole2.colour_transitions.push_back(ColourTransitionPoint(etaqd2));
    } else { //qbar g event

      // first dipole stretches between two initial-states
      dipole1.index_q    = 1; // qbar
      dipole1.index_qbar = 0; // gluon
      dipole1.index_previous = -1; 
      dipole1.index_next     = 1; // the gluon end
      dipole1.colour_transitions = ColourTransitionVector(false); //qbar end is not CF
      // for the second dipole
      dipole2.index_q        = 0; // gluon
      dipole2.index_qbar     = 3; // final-state anti-quark
      dipole2.index_previous = 0;
      dipole2.index_next     = -1; // gluon end
      dipole2.colour_transitions = ColourTransitionVector(true); //qbar end is CF

      if (p2_in.pdgid() == 21){
        // swap the relevant indices when p2 is the gluon instead of p1
        std::swap(dipole1.index_q, dipole1.index_qbar);
        dipole2.index_q     = 1; // gluon
      }

      // now insert the segments

      // now we are going to insert the segments 
      // the current set of dipoles is
      // d1 = [-inf, CA, inf]_(gqbarI)
      // d2 = [-inf, CF, inf]_(qbarFg)
      // we need to get two dipoles with 
      // d1 = [-inf, CA, etaq^d1, CF, inf]_(gqbarI)  and 
      // d2 = [-inf, CF, etaq^d2, CA, inf]_(qbarFg) 
      // determine the inner products
      // p1_dot_Q  = pg.Q
      // p2_dot_Q  = pqbarI.Q
      // pj_dot_Q  = pqbarF.Q
      // p1_dot_pj = pg.pqbarF
      // p2_dot_pj = pqI.pqbarF

      if (p2_in.pdgid() == 21){ // turn around the dot products if this is the case
      std::swap(p1_dot_Q , p2_dot_Q);
      std::swap(p1_dot_pj, p2_dot_pj);
      }

      // compute angles w.r.t. the 3 (=g) and 3bar (q) end of the dipoles
      precision_type omcos_theta3qbarF    = Q2 / p1_dot_Q / pj_dot_Q * p1_dot_pj;
      precision_type omcos_theta3barqbarF = Q2 / p2_dot_Q / pj_dot_Q * p2_dot_pj;

      // now get the rapidity
      double etaq;
      if(omcos_theta3qbarF > omcos_theta3barqbarF){
        // closer to 3-end of dipole
        etaq = to_double(- 0.5 * abs(log(omcos_theta3qbarF/(2-omcos_theta3qbarF))));
      } else{
        // closer to 3bar-end of dipole 
        etaq = to_double(0.5 * abs(log(omcos_theta3barqbarF/(2-omcos_theta3barqbarF))));
      }

      // determine the transition points
      double etaqd1 = std::min(0.0, etaq); // right of CA 
      double etaqd2 = std::max(0.0, etaq); // left of CA

      // finally add them to the pre-existing dipoles
      dipole1.colour_transitions.push_back(ColourTransitionPoint(etaqd1));
      dipole2.colour_transitions.push_back(ColourTransitionPoint(etaqd2));
    }
  } else if (gluon_is && gluon_fs){ // gg -> Xg event
    // much easer event setup than above, don't have to worry about colour transitions
    // we have however 3 dipole connections
    //             3bar   3   <- particle 3 
    //                 ||
    //                 ||
    //          dip1   ||  dip2
    // 3    0..........||.........1 3bar
    // 3bar 0.....................1 3
    //                dip3
    // this picture leads to
    dipole1.index_q    = 0; // gluon
    dipole1.index_qbar = 3; // gluon
    dipole1.index_previous = 2; //< dip3 
    dipole1.index_next     = 1; //< dip2
    dipole1.colour_transitions = ColourTransitionVector(false); //qbar end is not CF

    dipole2.index_q    = 3; // gluon
    dipole2.index_qbar = 1; // gluon
    dipole2.index_previous = 0; //< dip1 
    dipole2.index_next     = 2; //< dip3
    dipole2.colour_transitions = ColourTransitionVector(false); //qbar end is not CF

    dipole3.index_q    = 1; // gluon
    dipole3.index_qbar = 0; // gluon
    dipole3.index_previous = 1; //< dip2 
    dipole3.index_next     = 0; //< dip1
    dipole3.colour_transitions = ColourTransitionVector(false); //qbar end is not CF
  } else{
    assert(false && "Should not be able to end up here");
  }
  // construct the dipole list
  std::vector<Dipole> dipoles{dipole1, dipole2};
  if (gluon_is && gluon_fs) dipoles.push_back(dipole3);

  // finally return the event
  return Event(particles, dipoles, beam1, beam2);

}

// create a pp->dijet event
Event create_pp2jj_event(const Particle & beam1, const Particle & beam2,
                         const Particle & p1_in, const Particle & p2_in,
                         const Particle & pj_out,const Particle & pr_out) {

  PRINT_EXPERIMENTAL("create_pp2jj_event");
  
  // if the pdgids sum to the magic number 42 we have
  // two gluons in the intial state
  bool gluon_is = (p1_in.pdgid() + p2_in.pdgid() == 42);
  assert(!gluon_is && "Cannot handle the dipole setup of these gg->jj events");
  
  std::vector<Particle> particles{p1_in, p2_in};

  /// make sure these properties are set
  particles[0].set_initial_state(1);  /// from beam1
  particles[1].set_initial_state(2);  /// from beam2
  // explicitly set the initial state particles
  // not as part of the hard system
  particles[0].set_hard_system(false);
  particles[1].set_hard_system(false);

  // find whether we have a qqbar or a qq' in the inital state 
  bool is_qqbar = (p1_in.pdgid()*p2_in.pdgid() == -1);
  // add the jet
  particles.push_back(pj_out);
  particles[2].set_hard_system(true);
  // and the recoiling jet
  particles.push_back(pr_out);
  particles[3].set_hard_system(true);
  
  // now we need to add the initial dipoles
  Dipole dipole1, dipole2;
  // we consider one partonic channel:
  // 1. q(0)q'(1) -> q(2)q'(3) 
  // and the colour flow is fixed to 2 IF dipoles

  //                 | 2 (3-end)
  //                 | 
  // 0 (3bar-end)----<--X--<--- 1 (3bar-end)
  //                 |
  //                 | 3 (3-end)

  if(is_qqbar){
    // initial-initial dipole
    dipole1.index_q    = 1; // initial-state anti-quark
    dipole1.index_qbar = 0; // initial-state quark
    dipole1.index_previous = -1; // anti-quark end
    dipole1.index_next     = -1; // this is the next dipole, and starts with the gluon
    dipole1.colour_transitions = ColourTransitionVector(true); //qbar end is CF
    // final-final dipole
    dipole2.index_q        = 2; // final-state quark
    dipole2.index_qbar     = 3; // final-state anti-quark
    dipole2.index_previous = -1;
    dipole2.index_next     = -1;
    dipole2.colour_transitions = ColourTransitionVector(true); //qbar end is CF
  }
  else{
  // this means that for the first dipole we get
  dipole1.index_q    = 2; // final-state quark
  dipole1.index_qbar = 0; // initial-state quark
  dipole1.index_previous = -1; // anti-quark end
  dipole1.index_next     = -1; // this is the next dipole, and starts with the gluon
  dipole1.colour_transitions = ColourTransitionVector(true); //qbar end is CF
  // for the second IF dipole
  dipole2.index_q        = 3; // final-state quark
  dipole2.index_qbar     = 1; // initial-state quark
  dipole2.index_previous = -1;
  dipole2.index_next     = -1;
  dipole2.colour_transitions = ColourTransitionVector(true); //qbar end is CF
  }
  // construct the dipole list
  std::vector<Dipole> dipoles{dipole1, dipole2};

  // finally return the event
  return Event(particles, dipoles, beam1, beam2);

}

} // namespace panscales

