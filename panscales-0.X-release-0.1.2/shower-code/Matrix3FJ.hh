//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __MATRX3FJ_HH__
#define __MATRX3FJ_HH__

#include "fjcore_local.hh"
#include "Matrix3.hh"

FJCORE_BEGIN_NAMESPACE

typedef panscales::Matrix3 Matrix3;


/// returns the product of this matrix with the PseudoJet,
/// maintaining the 4th component of the PseudoJet unchanged
inline PseudoJet operator*(const Matrix3 & mat, const PseudoJet & p) {
  // r_{i} = m_{ij} p_j
  std::array<precision_type,3> res3{{0,0,0}};
  for (unsigned i = 0; i < 3; i++) {
    for (unsigned j = 0; j < 3; j++) {
      res3[i] += mat(i,j) * p[j];
    }
  }
  // return a jet that maintains all internal pointers by
  // initialising the result from the input jet and
  // then resetting the momentum.
  PseudoJet result(p);
  // maintain the energy component as it was
  result.reset_momentum(res3[0], res3[1], res3[2], p[3]);
  return result;
}

FJCORE_END_NAMESPACE

#endif // __MATRX3FJ_HH__