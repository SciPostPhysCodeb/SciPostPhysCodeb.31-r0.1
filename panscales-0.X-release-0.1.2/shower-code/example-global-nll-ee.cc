//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "AnalysisFramework.hh"
#include "FastY3.hh"
#include "LundEEGenerator.hh"
#include <sstream>
#include <cmath>

using namespace std;
using namespace fjcore;
using namespace fjcore::contrib;
using namespace panscales;

//----------------------------------------------------------------------
/// Code for quick tests of NLL accuracy for Lund observables
///
/// For simplicity, this code is meant to be run with the same beta
/// power for the shower and the observable
///
/// In order to do NLL event-shape tests, (see and) run the
/// example-global-nll-ee.py script.
/// 
/// An example of a command line is
/**
    ./build-double/example-global-nll-ee -process ee2qq -Q 1.0 -shower panglobal -beta 0.0 \
      -spin-corr off -nloops 2 -alphas 0.016 -lnkt-cutoff -46.25 \
      -weighted-generation -nev-cache 50000 -nev 20000 \
      -dynamic-lncutoff -15 -lambda-obs-min -0.5  \
      -out example-results/example-global-nll-ee.dat
*/
/// This should run in O(1 minute)
///
class GlobalObs : public AnalysisFramework {
public:
  /// default ctor, initialising from command-line arguments  
  GlobalObs(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in),
                                    _lund_ee_gen(0,false) {
    // get info from the command line
    cmdline_in->start_section("Analysis-specific options");
    _lambda_obs_min = cmdline->value("-lambda-obs-min", -0.5)
      .help("minimal value of alphas ln(v_obs) that we want to reach (negative)");
    cmdline_in->end_section("Analysis-specific options");

    // size of the buffer zone in ln(v_obs)
    auto veto_buffer_opt = cmdline->value("-dynamic-lncutoff",0.0).help("Size of the buffer zone (in ln(vobs))");
    if(!cmdline_in->help_requested())
      assert((veto_buffer_opt.present() && veto_buffer_opt() < 0) && "The argument -dynamic-lncutoff should be present and with a negative value." );
    _veto_buffer = veto_buffer_opt();

    // observable and shower rapidity scaling
    //
    // currently, we only support beta==0 because the driving python
    // script has hard-coded analytic expressions for beta=0
    // We'll make sure later that the shower one uses does indeed have beta==0.
    _beta_obs = _beta_ps = 0.0;
    //_beta_obs = cmdline->value("-beta-obs", 0.0);
    //_beta_ps  = cmdline->value("-beta",     0.0);
    assert((_beta_obs == _beta_ps) && "This code assumes beta_ps and beta_obs to be the same. One should use an FAPX veto otherwise.");
  }

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {
    if (f_shower_runner->shower()->beta() != 0){
      cerr << "Currently the example-global-ee code is forced to use a shower with beta==0 while you have requested a shower with beta==" << f_shower_runner->shower()->beta() << ". Aborting." << endl;
      exit(1);
    }
    
    // get the value of alphas at the reference scale
    _alphasQ = f_shower_runner->shower()->qcd().alphasMSbarQ();

    // get Q (for normalisation of the Lund shapes)
    double lnQ = f_shower_runner->shower()->qcd().lnQ();
    _Q = exp(precision_type(lnQ));

    // adjust the shower range: this is set by the range we want to
    // cover in lambda as well as the various buffers we impose
    //
    // for beta_ps > beta_obs  the stopping condition is imposed in the collinear limit
    // otherwise it is set at eta=0
    double factor = (_beta_ps > _beta_obs) ? ((1+_beta_ps)/(1+_beta_obs)) : 1;
    f_lnvmin = factor*(_lambda_obs_min/_alphasQ+_veto_buffer);

    // make sure we are running with a small-enough lnkt cut on alphas
    if (f_shower_runner->shower()->qcd().lnkt_cutoff() > f_lnvmin){
      ostringstream message;
      message << "# WARNING: alphas is cutoff at a kt scale above the low end of the buffer region: "
             << f_shower_runner->shower()->qcd().lnkt_cutoff() << " is larger than "
             << factor*(_lambda_obs_min/_alphasQ+_veto_buffer) << endl;
      header << message.str();
      cerr   << message.str();
    }

    // start the evolution at the Q scale
    f_lnvmax = lnQ;

    //--------------------------------------------------
    // observable histograms
    const double dasL  = 0.01;
    const double asLmin=-0.6;
    const double asLmax= 0.0;
    
    if (_beta_obs==0)
      cumul_hists_err["sqrt_y3"].declare(asLmin, asLmax, dasL);
    cumul_hists_err["Sj"].declare(asLmin, asLmax, dasL);
    cumul_hists_err["Mj"].declare(asLmin, asLmax, dasL);
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    // Use also the histogram because it is easier to read from python
    double evwgt = event_weight();

    // For brevity
    unsigned int N = f_event.size();

    //-------------------------------------------------------------
    // get the (Cambridge/Aachen) y3 jet rate (watch out for the square root)
    _fasty3.analyse(f_event); // C/A clustering
    if (_beta_obs==0)
      cumul_hists_err["sqrt_y3"].add_entry(_lambda(_fasty3())/2., evwgt);

    //-------------------------------------------------------------
    // get the Lund declusterings and compute the max and scalar sum of
    //   ν_obs = kt exp(-β_obs|η|) / Q
    // from the primary declusterings.
    std::vector<LundEEDeclustering> declusterings;
    if(N>2) declusterings = _lund_ee_gen.result(_fasty3.cs());

    precision_type lund_sum=0.0, lund_max=0.0;
    for (const auto & d : declusterings){
      precision_type x = d.kt() * exp(-precision_type(_beta_obs*d.eta()));

      lund_sum += x;
      if (x>lund_max){ lund_max = x; }
    }
    cumul_hists_err["Sj"].add_entry(_lambda(lund_sum/_Q), evwgt);
    cumul_hists_err["Mj"].add_entry(_lambda(lund_max/_Q), evwgt);
  }

protected:
  //----------------------------------------------------------------------
  // helpers
  // log v -> lambda
  double _lambda(precision_type v) const{
    if (v<=0) return -std::numeric_limits<double>::max();
    return _alphasQ * to_double(log_T(v));
  }

  //----------------------------------------------------------------------
  // internal variables 
  precision_type _Q;           //< energy Q of the ee collision
  double _alphasQ;             //< strong coupling at the hard scale Q
  double _lambda_obs_min;      //< minimal target value of lambda = alphas log(v) [negative] 
  double _veto_buffer;         //< size of the "dynamical" lnv buffer
  double _beta_ps, _beta_obs;  //< shower nd observable beta values. SHOULD BE IDENTICAL

  FastY3 _fasty3;                        //< tool to cluster with Cambridge/Aachen and et y3
  RecursiveLundEEGenerator _lund_ee_gen; //< too to get the Lund declusterings
};
  
//------------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  GlobalObs driver(&cmdline);
  driver.run();
}
