//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerPanScaleGlobalDIS.hh"

using namespace std;

namespace panscales{

  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerPanScaleGlobalDIS::elements(Event & event, int dipole_index) const {
    // This is an antenna shower, so we need one element per dipole
    // Definition is that we have [3end, 3bar-end] for each dipole
    // For IF and FI, the definition is then that the first letter corresponds
    // to the 3end of the dipole, and the 2nd letter corresponds to the 3bar-end
    int i3    = event.dipoles()[dipole_index].index_3()   ;
    int i3bar = event.dipoles()[dipole_index].index_3bar();
    ShowerPanScaleGlobalDIS::Element *elm;
    if (event.particles()[i3].is_initial_state()){
      // 3 is I, 3bar is F
      elm = new ShowerPanScaleGlobalDIS::ElementIF(i3, i3bar, dipole_index, &event, this);
    } else {
      if (event.particles()[i3bar].is_initial_state()){
        // 3 is F, 3bar is I
        // we make this choice such that we do not copy the kinematics of IF for an FI element
        elm = new ShowerPanScaleGlobalDIS::ElementIF(i3bar, i3, dipole_index, &event, this);
      } else {
        // 3 and 3bar are F
        elm = new ShowerPanScaleGlobalDIS::ElementFF(i3, i3bar, dipole_index, &event, this);
      }
    }      
    std::unique_ptr<typename ShowerBase::Element> elms[] = {std::unique_ptr<typename ShowerBase::Element>(elm)};
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }

  ShowerBase::EmissionInfo* ShowerPanScaleGlobalDIS::create_emission_info() const{ return new ShowerPanScaleGlobalDIS::EmissionInfo(); }

  Range ShowerPanScaleGlobalDIS::Element::lnb_generation_range(double lnv) const {
    assert(false && "this should be reimplemented in the derived element classes");
    return Range(0,0);
  }
  
  Range ShowerPanScaleGlobalDIS::Element::lnb_exact_range(double lnv) const {
      std::cerr << "Error: We do not expect lnb_exact_range to be called " << std::endl;
      return Range(0,0);
  }

  // bit of acceptance probability that is common to all the elements
  bool ShowerPanScaleGlobalDIS::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{

    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleGlobalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalDIS::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;

    precision_type kappat = _rho*exp(precision_type(lnv + _shower->_beta*abs(lnb)));
    precision_type alphak = sqrt(_sjtilde/(_dipole_m2*_sitilde))*kappat*exp(precision_type(lnb));
    // now compute betak
    // betak  = sqrt(_sitilde/(_dipole_m2*_sjtilde))*kappat*exp(-lnb);
    // but we can use 1/alpha = sqrt(_sitilde/_sjtilde)*sqrt(_dipole_m2)*1/kappat*exp(-lnb)
    // so betak = kappat^2 / _dipole_m2 * 1/alphak
    precision_type betak = pow2(kappat)/_dipole_m2/alphak;

    emission_info.kappat = kappat;
    emission_info.alphak = alphak;
    emission_info.betak  = betak;

    emission_info.emitter_weight_rad_gluon   = 0.0;
    emission_info.emitter_weight_rad_quark   = 0.0;
    emission_info.spectator_weight_rad_gluon = 0.0;
    emission_info.spectator_weight_rad_quark = 0.0;
    
    return true;
  }


  // do_kinematics should be reimplemented in the derived classes
  bool ShowerPanScaleGlobalDIS::Element::do_kinematics(
    typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const {
        assert(false && "this should be reimplemented in the derived element classes");
        return false; 
  }

  // These go into the derived classes
  void ShowerPanScaleGlobalDIS::Element::update_event(const ColourTransitionRunnerBase *transition_runner,
                                                   typename ShowerBase::EmissionInfo * emission_info) {
    // Call update event of base shower class
    assert(false && "this should be reimplemented in the derived element classes");
  }

  /// Boost particles in a DIS chain
  void ShowerPanScaleGlobalDIS::Element::boost_all_in_chain(typename ShowerBase::EmissionInfo * emission_info_base, int dis_chain) const {
    // retrieve information of emission info
    const typename ShowerPanScaleGlobalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalDIS::EmissionInfo*>(emission_info_base));
    // retrieve the reference directions and Q_DIS^2
    const Momentum & p_ref_in  = _event->ref_in(dis_chain);
    const Momentum & p_ref_out = _event->ref_out(dis_chain);
    const precision_type &Q2_DIS      = _event->Q2_dis(dis_chain);

    // build the Lorentz boost
    LorentzBoostDIS<MomentumM2<precision_type>> boost(p_ref_in, p_ref_out, emission_info.pf, Q2_DIS, emission_info.af, emission_info.bf);
    if(use_diffs()){
      std::vector<precision_type> Ei(_event->size());
      for(unsigned i = 0; i < _event->size(); ++i) {
        Particle & p = (*_event)[i];
        if(p.get_DIS_chain() != dis_chain) continue; // < skip the wrong chains
        Ei[i] = p.E();
        if (p.is_initial_state() && !p.is_parton()) continue; // skip the photon
        if (p.is_initial_state()) (*_event)[i].reset_momentum(Momentum::fromP3M2((emission_info.bf*p).p3(), p.m2()));
        else	                  (*_event)[i].reset_momentum(Momentum::fromP3M2((boost*p).p3(), p.m2()));
      }
      // then handle the dipole differences
      for (auto & dipole: _event->dipoles()) {
        int i3bar = dipole.index_3bar();
        int i3    = dipole.index_3();

        if((*_event)[i3].get_DIS_chain() != dis_chain) continue; // < skip the wrong chains
        dipole.dirdiff_3_minus_3bar = boost.apply_linear_transform_to_dirdiff(Ei[i3bar], (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);
      }

      // then handle the dipole differences for non-splitting dipoles as well
      for (auto & dipole: _event->non_splitting_dipoles()) {
        int i3bar = dipole.index_3bar();
        int i3    = dipole.index_3();
        if((*_event)[i3].get_DIS_chain() != dis_chain) continue; // < skip the wrong chains
        dipole.dirdiff_3_minus_3bar = boost.apply_linear_transform_to_dirdiff(Ei[i3bar], (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);   
           
      }
    } else{
      // boost only done on the final-state particles
      for(unsigned i = 0; i < _event->size(); ++i) {
        Particle & p = (*_event)[i];
        
        if (p.get_DIS_chain() != dis_chain) continue; // < skip the wrong chains
        if (p.is_initial_state() && !p.is_parton()) continue; // skip the photon
        if (p.is_initial_state()) (*_event)[i].reset_momentum(Momentum::fromP3M2((emission_info.bf*p).p3(), p.m2()));
        else {	  
	        (*_event)[i].reset_momentum(Momentum::fromP3M2((boost*p).p3(), p.m2()));
	      }
      }
    }
  } 
  //----------------------------------------------------------------------
  // computes the Sudakov components of the final state
  // and returns the rescaling factor for the initial state
  void ShowerPanScaleGlobalDIS::Element::get_sudakov_and_rescaling(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalDIS::EmissionInfo*>(emission_info_base));
    
    // for the rescaling factors we need the sudakov components of the final-state momentum sum
    // kperp_f and bf needed for the boost 
    // rescaling factor for initial-state parton can now be built (=invariant)
    precision_type r = (1 + emission_info.pf.m2() / _event->Q2_dis(emitter().get_DIS_chain()));
    emission_info.r  = r;

    // obtain the Sudakov components (invariants) and kperp_f 
    precision_type af = 2.*dot_product(_event->ref_out(emitter().get_DIS_chain()) ,emission_info.pf)/(_event->Q2_dis(emitter().get_DIS_chain()));
    precision_type bf = 2.*dot_product(_event->ref_in(emitter().get_DIS_chain())  ,emission_info.pf)/(_event->Q2_dis(emitter().get_DIS_chain()));
    // store information for later
    emission_info.af      = af;
    emission_info.bf      = bf;
  }

  //======================================================================
  // ShowerPanScaleGlobalDIS::ElementIF implementation
  //======================================================================
  double ShowerPanScaleGlobalDIS::ElementIF::lnb_extent_const() const {
    double xi      = to_double(event().pdf_x(emitter()));
    double xi_over_omxi = xi / (1 - xi);
    double lnxi_over_omxi = log(xi_over_omxi);
    return to_double(_log_dipole_m2 - 2.*_log_rho - lnxi_over_omxi)/(1. + _shower->_beta);
  }

  Range ShowerPanScaleGlobalDIS::ElementIF::lnb_generation_range(double lnv) const{
    // We assume here that alphak and betak are small
    double xi      = to_double(event().pdf_x(emitter()));
    double xi_over_omxi = xi / (1 - xi);
    double lnxi_over_omxi = log(xi_over_omxi);

    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv                  ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv - lnxi_over_omxi ) / (1+_shower->_beta));
  }
  
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleGlobalDIS::ElementIF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    //construct alphak, betak which is the same for every element
    ShowerPanScaleGlobalDIS::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalDIS::EmissionInfo*>(emission_info_base));
    // bk needs to be smaller then 1 to prevent negative energies for the spectator
    if(emission_info.betak >= 1) return false;

    // determine the z factors that go into the splitting kernels
    precision_type za   = emission_info.alphak / (1. + emission_info.alphak);
    precision_type zb   = emission_info.betak;
    
    // pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), za,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    _shower->fill_dglap_splitting_weights(spectator(), zb,
                                          emission_info.spectator_weight_rad_gluon,
                                          emission_info.spectator_weight_rad_quark);

    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    // we will get in the end pi = r * pitilde
    // x_new / x_old = (Q2_DIS + pf2_new)/(Q2_DIS + pf2)
    // with
    // pf2_new = (pf - pjtilde + pj + pk)^2
    //         = pf^2 + 2 ak pitilde.pf + 2 kT.pf + kT2
    // x_new / x_old = 1 + (2 ak pitilde.pf + 2 kT.pf + kT2)/(Q2_DIS + pf2)
    // x_new / x_old can therefore be larger or smaller than 1
    // for ak ->0, bk ->0 we have x_new / x_old = 1
    // in collinear limit with ak = 1, bk = 0 we have (1+ak) 
    // see DIS logbook
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = (1. + emission_info.alphak);   
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = (1. + emission_info.alphak);  
    }
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter   = za;
    emission_info.z_radiation_wrt_spectator = zb;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();

    // this is an antenna shower
    // either the emitter can split or the spectator
    double f, omf;
    f_fcn(emission_info.lnb, f, omf);
    //emission_info.emitter_partition_fraction = f_fcn(lnb);  
    // spectator splitting weight = 1-f(lnb)
    
    emission_info.emitter_weight_rad_gluon   *=   f * normalisation_factor;
    emission_info.emitter_weight_rad_quark   *=   f * normalisation_factor;
    emission_info.spectator_weight_rad_gluon *= omf * normalisation_factor;
    emission_info.spectator_weight_rad_quark *= omf * normalisation_factor;
    
    return true;
    
  }
  
  /// carry out the splitting and update the event
  bool ShowerPanScaleGlobalDIS::ElementIF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{

    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalDIS::EmissionInfo*>(emission_info_base));
    
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    // build the radiated mapping coefficients
    precision_type ak = emission_info.alphak;
    precision_type bk = emission_info.betak;
    precision_type norm_perp = emission_info.kappat;
    // ai is not needed, will be replaced with a rescaling coefficient
    // bj will never be smaller than 0 because bk cannot be larger than one
    precision_type bj = 1. - bk;

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);

    // build the momenta of the dipole (but just the radiation and final-state spectator)
    // get the dis chain
    int dis_chain = emitter().get_DIS_chain();
    assert(dis_chain == spectator().get_DIS_chain() && "Error - emitter and spectator should belong to same DIS chain");
    // rotate p_perp to event frame, needed for dot product below
    Momentum3<precision_type> p_perp_unrotated3 = rp.rotn_from_z*perp;
    Momentum  p_perp_unrotated  = Momentum4<precision_type>(p_perp_unrotated3.px(), p_perp_unrotated3.py(), p_perp_unrotated3.pz(), perp.E());

    // build the momenta
    Momentum3<precision_type> radiation3, spectator_out3;
    radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3();
    spectator_out3 =                        bj * rp.spectator.p3(); 
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
    // now we can construct the new final-state momentum
    // const Momentum pf_event_frame = _event->momentum_partons_in();
    // need only the momenta that belongs to the current chain
    const Momentum pf_event_frame = _event->sum_momentum_out_in_chain(dis_chain);

    // pf = pfTilde + (ak piTilde + kperp)
    // pf^2 = pfTilde^2 + kperp^2 + 2 pfTilde* (r (ak piTilde + kperp) )
    // 2pfTilde * piTilde = pfTilde2+Q^2   {piTilde= (1+pfTilde2/Q2) n1,    pfTilde = n2 + pfTilde2/Q2 n1}
    precision_type pftilde2       = pf_event_frame.m2();
    precision_type pf_dot_pitilde = pftilde2 + _event->Q2_dis(dis_chain);
    precision_type pf_dot_kperp   = 2.*dot_product(p_perp_unrotated, pf_event_frame);
    // pf   = pf_tilde + perp
    // pf^2 = pf_tilde^2 + pf_dot_kperp  + norm_perp^2
    precision_type pfbar2 = pftilde2 + (ak * pf_dot_pitilde + pf_dot_kperp) -  pow2(norm_perp);
    
    emission_info.pf = Momentum::fromP3M2(pf_event_frame.p3() + ak * emitter().p3() + p_perp_unrotated3, pfbar2);

    // calculate af, bf and r
    get_sudakov_and_rescaling(emission_info_base, rp);
    
    // update the PDF x fractions
    // original x   = dot_product(emitter(), _event->ref_out())/dot_product(_event->beam1(), _event->beam2())
    // new x        = dot_product(emission_info.emitter_out, _event->ref_out())/dot_product(_event->beam1(), _event->beam2())
    // new over old = dot_product(emission_info.emitter_out, _event->ref_out())/dot_product(emitter(), _event->ref_out())
    //              = emission_info.r * Q2_DIS / (2 * dot_product(emitter(), _event->ref_out()))
    //              = (Q2_DIS + emission_info.pf.m2()) / (Q2_DIS + _event->momentum_sum().m2())
    if(emitter().initial_state()==1){
      emission_info.beam1_pdf_new_x_over_old = emission_info.r * _event->Q2_dis(dis_chain)/(2.*dot_product(_event->ref_out(dis_chain), (*_event)[0]));
      emission_info.emitter_out              = emission_info.beam1_pdf_new_x_over_old/emission_info.bf * rp.emitter;
      // alternatively: (_event->Q2_dis() + emission_info.pf.m2()) / (_event->Q2_dis() + _event->momentum_sum().m2())
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                       // the parton is from beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = emission_info.r * _event->Q2_dis(dis_chain)/(2.*dot_product(_event->ref_out(dis_chain), (*_event)[1]));
      emission_info.emitter_out              = emission_info.beam2_pdf_new_x_over_old/emission_info.bf * rp.emitter;
    }

   return true;
  }
  
  // do the final boost
  void ShowerPanScaleGlobalDIS::ElementIF::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {

    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    // Do the boost that restores momentum conservation
    ShowerPanScaleGlobalDIS::Element::boost_all_in_chain(emission_info_base, emitter().get_DIS_chain());
  }
  
  //======================================================================
  // ShowerPanScaleGlobalDIS::ElementFF implementation
  //======================================================================
  double ShowerPanScaleGlobalDIS::ElementFF::lnb_extent_const() const {
    return to_double(_log_dipole_m2 - 2.*_log_rho)/(1. + _shower->_beta);
  }

  Range ShowerPanScaleGlobalDIS::ElementFF::lnb_generation_range(double lnv) const{  
    // this corresponds to Eq. (52 - 53) of 2018-07-notes.pdf (and to the soft limit used in the Sudakov)
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv ) / (1+_shower->_beta));
  }

  // return the acceptance probability for the given kinematic point
  bool ShowerPanScaleGlobalDIS::ElementFF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // construct alphak, betak which is the same for every element
    ShowerPanScaleGlobalDIS::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalDIS::EmissionInfo*>(emission_info_base));

    // impose that ak and bk are smaller then 1
    // if they are larger, they will result in negative energies of our emitter/spectator
    if((emission_info.alphak >= 1) || (emission_info.betak >= 1)){
      return false;
    }
    // both are final state so za = ak, zb = bk
    precision_type za   = emission_info.alphak;
    precision_type zb   = emission_info.betak;
    
    // here we should make a decision based on flavour
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), za,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    _shower->fill_dglap_splitting_weights(spectator(), zb,
                                          emission_info.spectator_weight_rad_gluon,
                                          emission_info.spectator_weight_rad_quark);

    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    // initial-state gets rescaling that differs from 1.0 in the hard and/or collinear limit
    emission_info.beam1_pdf_new_x_over_old = 1.0;
    emission_info.beam2_pdf_new_x_over_old = 1.0; 
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter   = za;
    emission_info.z_radiation_wrt_spectator = zb;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();

    // antenna: the splitter is either the emitter or the spectator
    double f, omf;
    f_fcn(emission_info.lnb, f, omf);
    
    emission_info.emitter_weight_rad_gluon   *=   f * normalisation_factor;
    emission_info.emitter_weight_rad_quark   *=   f * normalisation_factor;
    emission_info.spectator_weight_rad_gluon *= omf * normalisation_factor;
    emission_info.spectator_weight_rad_quark *= omf * normalisation_factor;
    
    return true;
  }
  
  /// carry out the splitting and update the event
  bool ShowerPanScaleGlobalDIS::ElementFF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    
    // retrieve emission_info
    typename ShowerPanScaleGlobalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalDIS::EmissionInfo*>(emission_info_base));
    assert(emitter().get_DIS_chain() == spectator().get_DIS_chain() && "Error - chains of emitter and spectator are different");
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    // build the radiated mapping coefficients
    // that can be found in
    // Eq. (4.101) in logbook/2020-12-09-pp-schemes.pdf
    precision_type ak = emission_info.alphak;
    precision_type bk = emission_info.betak;
    precision_type norm_perp = emission_info.kappat;

    // rescaling coefficients for the emitter (ai) and spectator (bj)
    precision_type ai = 1. - ak;
    precision_type bj = 1. - bk;

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    // rotate p_perp to event frame, needed for dot product below
    Momentum3<precision_type> p_perp_unrotated3 = rp.rotn_from_z*perp;
    Momentum  p_perp_unrotated  = Momentum4<precision_type>(p_perp_unrotated3.px(), p_perp_unrotated3.py(), p_perp_unrotated3.pz(), perp.E());
    
    // we now impose that (n1+n2).(pitilde + pjtilde) = Q.(pi + pj + pk) = rl*Q.(pitilde + pjtilde + kT)
    // this leads to a local rescaling coefficient defined as pi = rl*pibar, pj = rl*pjbar, pk = rl*pkbar
    // solving the eqn above gives us
    int dis_chain = emitter().get_DIS_chain();
    precision_type rl, rl_minus_one;
    Momentum3<precision_type> emitter_out3, radiation3, spectator_out3;
    if(_shower->rescale_option() == PanGlobalRescaleOption::LocalRescaling){
      precision_type kTQ          = 2.*dot_product(p_perp_unrotated, _event->Q_dis(dis_chain));
      rl           = (_sitilde + _sjtilde)/(_sitilde + _sjtilde + kTQ);
      // ((si+sj) - (si+sj+kTQ))/(si+sj+kTQ) 
      rl_minus_one = -(kTQ)/(_sitilde + _sjtilde + kTQ);
    } else if (_shower->rescale_option() == PanGlobalRescaleOption::GlobalRescalingDeprecated){
      rl           = 1.;
      rl_minus_one = 0.;
    } else{
      throw std::runtime_error("Unknown rescaling option asked for");
    }
    // sum: rl*perp imbalance 
    
    // build the momenta
    emitter_out3   = rl*(ai * rp.emitter.p3());
    radiation3     = rl*(ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3());
    spectator_out3 = rl*(                       bj * rp.spectator.p3()); 
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    // now construct the total final-state sum
    const Momentum pf_event_frame = _event->sum_momentum_out_in_chain(dis_chain);
    // get the required dot products 
    precision_type pf_dot_pitilde = 2.*dot_product(emitter(), pf_event_frame);
    precision_type pf_dot_pjtilde = 2.*dot_product(spectator(), pf_event_frame);
    precision_type pf_dot_kperp   = 2.*dot_product(p_perp_unrotated, pf_event_frame);
    // pf   = pf_tilde + (rl-1)(pitilde + pjtilde) + rl * perp
    // pf^2 = pf_tilde^2 + (rl-1)*(pf_dot_pitilde + pf_dot_pjtilde) + rl * pf_dot_kperp + (rl-1)^2 _dipole_m2 + rl^2 * norm_perp^2
    precision_type pfbar2 = pf_event_frame.m2() + rl_minus_one*(pf_dot_pitilde + pf_dot_pjtilde) + rl * pf_dot_kperp  \
                          + pow2(rl_minus_one)*_dipole_m2 - pow2(rl) * pow2(norm_perp);
    
    emission_info.pf = Momentum::fromP3M2(pf_event_frame.p3() 
                                    + rl_minus_one * (emitter().p3() +spectator().p3()) + rl * p_perp_unrotated3, pfbar2);
                                    
    // calculate af, bf and r
    get_sudakov_and_rescaling(emission_info_base, rp);
    
    // update the PDF x fractions
    // original x   = dot_product((*_event)[1], _event->ref_out())/dot_product(_event->beam1(), _event->beam2())
    // new x        = dot_product(emission_info.r*_event->ref_in(), _event->ref_out())/dot_product(_event->beam1(), _event->beam2())
    // new over old = dot_product(emission_info.r*_event->ref_in(), _event->ref_out())/dot_product((*_event)[1], _event->ref_out())
    if ((*_event)[0].get_DIS_chain() == dis_chain){ // parton 0 is in the same DIS chain as the current emitting dipole
      emission_info.beam1_pdf_new_x_over_old = emission_info.r * _event->Q2_dis(dis_chain)/(2.*dot_product(_event->ref_out(dis_chain), (*_event)[0]));
      // alternatively: (_event->Q2_dis() + emission_info.pf.m2()) / (_event->Q2_dis() + _event->momentum_sum().m2())
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                       // the parton is from beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = emission_info.r * _event->Q2_dis(dis_chain)/(2.*dot_product(_event->ref_out(dis_chain), (*_event)[1]));
    }

    return true;
   
  }  

  /// do the final boost
  void ShowerPanScaleGlobalDIS::ElementFF::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobalDIS!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    const typename ShowerPanScaleGlobalDIS::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobalDIS::EmissionInfo*>(emission_info_base));

    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    // reset the momentum of the initial-state parton
    if ((*_event)[0].get_DIS_chain() == emitter().get_DIS_chain()){ // the parton is from beam1
      (*_event)[0].reset_momentum(Momentum::fromP3M2(emission_info.r/emission_info.bf * _event->ref_in(emitter().get_DIS_chain()).p3(), _event->ref_in(emitter().get_DIS_chain()).m2()));
    } else {                       // the parton is from beam2 (DIS falls in this class because [0] is not a photon) 
      (*_event)[1].reset_momentum(Momentum::fromP3M2(emission_info.r/emission_info.bf * _event->ref_in(emitter().get_DIS_chain()).p3(), _event->ref_in(emitter().get_DIS_chain()).m2()));
    }
    // Do the boost that restores momentum conservation on final-state partons only
    ShowerPanScaleGlobalDIS::Element::boost_all_in_chain(emission_info_base, emitter().get_DIS_chain());
  }
} // namespace panscales
