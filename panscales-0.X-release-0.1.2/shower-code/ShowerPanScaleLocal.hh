//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __SHOWERPANSCALELOCAL_HH__
#define __SHOWERPANSCALELOCAL_HH__

#include "ShowerPanScaleBase.hh"

namespace panscales{
  
//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerPanScaleLocal
/// the PanScales shower w dipole-local recoil (dipole variant) for e+e- collisions  
/// introduced in arXiv:2002.11114.

class ShowerPanScaleLocal : public ShowerPanScaleBase {
public:
  /// default ctor
  ShowerPanScaleLocal(const QCDinstance & qcd, double beta = 0.5) 
    : ShowerPanScaleBase(qcd, beta) {

    // disable double-soft by default
    // this involves virtiual methods so should be alled here again
    enable_double_soft(false);
  }

  /// virtual dtor
  virtual ~ShowerPanScaleLocal() {}

  /// implements the element as a subclass (EmissionInfo is common to
  /// all PanScales showers and defined in the ShowerPanScaleBase base
  /// class)
  class Element;
  
  /// description of the class
  std::string description() const override {
    std::ostringstream ostr;
    ostr << name() << " shower with beta = " << _beta;
    return ostr.str();
  }
  /// name of the shower
  std::string name() const override{return "PanScaleLocal-ee";}
  
  /// this is a dipole shower (2 elements per dipole)
  virtual unsigned int n_elements_per_dipole() const override{ return 2;}

  /// returns true if an emission happens at a phase-space point that has
  /// already been covered by the matching shower (used in "truncated" showers)
  virtual bool veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const override;

  /// element store
  virtual std::vector<std::unique_ptr<ShowerBase::Element> > elements(Event & event, int dipole_index) const override;

  /// this is a dipole-like shower, so only the emitter splits
  bool only_emitter_splits() const override {return true;}
 
  virtual void init_double_soft() override {
  // Using PanGlobal(beta=1/2) overhead. Needs to be updated    
    _double_soft_overhead = double_soft() ? 15.0 : 1.0;
  }
  double double_soft_overhead() const {return _double_soft_overhead;}
  
  /// given kinematics from an event, returns the lnv, lnb values
  /// that correspond to (ĩ,j̃) -> (i,j,k)
  virtual bool find_lnv_lnb_from_kinematics(const Momentum &p_emit, 
                                     const Momentum &p_spec, 
                                     const Momentum &p_rad, 
                                     const precision_type &p_emit_dot_p_spec,
                                     const precision_type &p_emit_dot_p_rad,
                                     const precision_type &p_spec_dot_p_rad,
                                     const Event &event,
                                     double &lnv, double &lnb) const override;
private:
  double _double_soft_overhead = 1.0; ///< Overhead factor needed for double soft acceptance prob  
};


//--------------------------------------------------------------
/// \class ShowerPanScaleLocal::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs for the PanScaleLocal shower w dipole-local recoil (dipole variant)
class ShowerPanScaleLocal::Element : public ShowerPanScaleBase::Element {

public:
   /// dummy ctor  
  Element() {}

  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleLocal * shower) :
    ShowerPanScaleBase::Element(emitter_index, spectator_index, dipole_index, event, shower),  _shower(shower) {}

  /// given that we have virtual functions in a number of places,
  /// we need a virtual destructor to avoid compiler warnings
  virtual ~Element() {}

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------
  /// Approximate calculation of the log of the kT of the emission relative
  /// to its parents in their rest frame, (eq. 57 of logbook 2019-07-02).
  const double alphas_lnkt(double lnv, double lnb) const override {
    precision_type lnkt = log_T(_rho) + lnv + _shower->_beta*fabs(lnb);
    return to_double(lnkt);
  }

  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  /// the lnb total extent, as a function of lnv, is parametrised
  /// as  const + lnv_deriv * lnv.
  ///
  double lnb_extent_const()     const override {
    return to_double(log_T(_dipole_m2) - 2.*log_T(_rho))/(1. + _shower->_beta);
  }
  double lnb_extent_lnv_coeff() const override {return -2.0/(1. + _shower->_beta);}

  Range lnb_generation_range(double lnv) const override;
  /// returns the jacobian for the first emission
  precision_type dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const override;
  /// returns the partitioning factor for the matching probability
  precision_type get_partitioning_factor_matching_probability(typename ShowerBase::EmissionInfo * emission_info) const override;


  // #TRK_ISSUE-416 The exact range is actually known (Eq. (42) of 2018-07-notes.pdf)
  // but it depends both on lnv and lnb
  bool has_exact_lnb_range() const override {return false;}

  Range lnb_exact_range(double lnv) const override { return Range(0., 0.); }

  double lnv_lnb_max_density() const override;
  
  //--------------------------------------------------
  // important definitions to be implemented in all derived classed
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const override;

  /// carry out the splitting and update the event; return true if
  /// successful; the RotatedPieces pass on a potentially rotated version
  /// of the emitter, spectator, and perpendicular vectors, and these
  /// are the ones that should be used, not those from emission_info; 
  /// the output is stored back in emission_info (possibly still rotated)
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const override;
  
  /// Acceptance probability associated with correct double soft behaviour of the shower
  virtual double double_soft_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;
  
  // LuSi: the block below might be common with PanGlobal, we can put it in
  // ShowerPanScaleBase later on
  class DoubleSoftInfo{
  public:
    // momenta of the 4 particles involved
    Momentum p_new;       ///< new emission
    Momentum p_first;     ///< partner soft emission (emitted first)
    Momentum p_aux_new;   ///< auxiliary colour-connected w p_new
    Momentum p_aux_first; ///< auxiliary colour-connected w p_first

    // flavour information
    int flav_init;   ///< flavour as assigned initially by the shower

    // cached distances
    // dcross = dna d1b - d1a dnb
    precision_type dn1, dna, dnb, d1a, d1b, dab, dcross;
    
    // weights generated by the shower
    //
    // these weights sum over all the histories yieldins the same
    // kinematic configuration, i.e. over the 2 options for the
    // particle which is emitted first.
    //
    //GS-NOTE-DS: in principles we do not need to cache the "swapped"
    //versions as only the "same" are used in the probabilities. Also,
    //we may want to use the fact that the same and swapped exact qq
    //are the same.
    //
    // We also cache partial sums
    precision_type shower_weight_gg_same;    ///< g->gg:    same colour flow as the shower
    precision_type shower_weight_gg_swapped; ///< g->gg:    opposite colour flow as the shower
    precision_type shower_weight_qq_same;    ///< g->qqbar: same colour flow as the shower
    precision_type shower_weight_qq_swapped; ///< g->qqbar: opposite colour flow as the shower

    precision_type shower_weight_gg;   ///< g->gg
    precision_type shower_weight_qq;   ///< g->qqbar
    precision_type shower_weight;      ///< g->gg + g->qqbar
    
    // exact weights in the various channels
    precision_type exact_weight_gg_same;    ///< g->gg:    same colour flow as the shower
    precision_type exact_weight_gg_swapped; ///< g->gg:    opposite colour flow as the shower
    precision_type exact_weight_qq_same;    ///< g->qqbar: same colour flow as the shower
    precision_type exact_weight_qq_swapped; ///< g->qqbar: opposite colour flow as the shower

    precision_type exact_weight_gg;   ///< g->gg
    precision_type exact_weight_qq;   ///< g->qqbar
    precision_type exact_weight;      ///< g->gg + g->qqbar
  };

  double double_soft_overhead() const {return _shower->double_soft_overhead();}
private:
  //----------------------------------------------------------------------
  // material to handle double-soft corrections
  //----------------------------------------------------------------------
  virtual bool _double_soft_fill_momenta(typename ShowerBase::EmissionInfo * emission_info,
                                         DoubleSoftInfo * double_soft_info) const;

  /// analytic double soft approximation (both shower and exact weights)
  virtual void _double_soft_fill_weights(DoubleSoftInfo * double_soft_info) const;

  /// analytic double soft approximation for this shower.
  ///   dij = 2*1-cos(theta_ij)
  ///   diQ = 2*pi.Q/(Ei EQ)
  ///   diQ = Q^2/EQ^2
  ///    z2 = E2/(E1+E2)
  virtual void _double_soft_set_weight_shower(precision_type &d12, precision_type &d1a,
                                              precision_type &d1b, precision_type &d2a,
                                              precision_type &d2b, precision_type &dab,
                                              precision_type &d1Q, precision_type &d2Q,
                                              precision_type &daQ, precision_type &dbQ,
                                              precision_type &dQQ,
                                              precision_type &z1, precision_type &z2, 
                                              precision_type * weight_gg,
                                              precision_type * weight_qq) const;            
  ///
  ///GS-NOTE-DS: we could think of filling em_info wo returnig the probability
  virtual double _double_soft_swap_flavours_prob(typename ShowerBase::EmissionInfo * emission_info,
                                                 DoubleSoftInfo * double_soft_info) const;

  /// compute the probabilities for a colour swap
  /// We have 2 probabilities depending on the flavour of the splitting
  virtual void _double_soft_swap_colour_connections(typename ShowerBase::EmissionInfo * emission_info,
                                                    DoubleSoftInfo * double_soft_info,
                                                    double &colour_swap_prob_gg, 
                                                    double &colour_swap_prob_qq) const;

  const ShowerPanScaleLocal * _shower;
};
  
} // namespace panscales

#endif // __SHOWERPANSCALELOCAL_HH__
