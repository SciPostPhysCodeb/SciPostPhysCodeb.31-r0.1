Basic build instructions
========================

Before building the code, you first need to initialise the submodules

     git submodule init
     git submoudle update

Using the build script
----------------------

We also provide a script to automate the above. From the `shower-code/`
directory call

    ../scripts/build.py [--builds X] -j

where the optional `--builds` argument allows for builds other than in
double precision, and `X` is a space-separated list that contains one or
more of the following options: *double, ddreal, qdreal, doubleexp,
mpfr4096*. The `-j` argument indicates a parallel build.

This will create a separate directory for each precision type, and
build the code therein.

The `build.py` can also be used directly with other sub-directories of
the project. E.g. from `analyses/analysis-name/` run

     ../scripts/build.py [--builds X] [-j] [--build-lib]

If `--build-lib` is present the script will first check if the
PanScales library needs rebuilding, and if so, rebuild it.

At the moment events are most easily analysed using the internal
analysis framework. A few example analyses are described in the
`EXAMPLES.md` file. If event output is needed in standard formats (e.g.
HepMC), we currently recommend use of the Pythia interface.

We also provide a small script that sets up a skeleton analysis. From
the `analyses/` directory (located in the main directory) call

     ../scripts/new-analysis.py analysis-name

This will create a directory called `analysis-name`. 

Using CMake directly
--------------------

The `build.py` script relies on CMake. If using CMake directly, we
strongly recommend an out-of-directory build. Typically, from within the
`shower-code/` directory, one would do
  
    mkdir build-double
    cd build-double
    cmake ..
    make -j

for a build in double precision. This will create a library
`libpanscales.so` which can be linked and used with your favourite
analysis. 

Build options with CMake:
-------------------------

The main CMake options are documented below. Note that you cannot have
in-directory and out-of-source builds at the same time.

To view all options do

    cmake -L ..

(or `-LA` to include advanced options)

The main options include:

* `-DGSL_ROOT_DIR=<value>`  base path for the GSL package (without this option cmake will attempt to deduce a location)

* `-DHAS_LHAPDF=<ON|OFF>`   if on, it will try to include LHAPDF (default off)

* `-DPSVERBOSE=<ON|OFF>`    turn on (very) verbose output for shower debugging

* `-DGIT_WATCHER=<ON|OFF>`  turn on compilation of git version info into library (default on)

* `-DPSDDREAL=<ON|OFF>`     use QD's dd_real as precision_type (requires QD)
* `-DPSQDREAL=<ON|OFF>`     use QD's qd_real as precision_type (requires QD)
* `-DQD_ROOT_DIR=<value>`   base path for the QD package (without this option cmake will attempt to deduce a location)
* `-DPSQUAD=<ON|OFF>`       use Boosts's float128 as precision_type (requires Boost)

* `-DPANSCALES_MOMENTUM_CHECKS=<ON|OFF>`
    activate momentum checks (default on, recommended)
                                  

Cleaning up:
------------

Simply remove the `build-double` directory (or whatever directory you
used for the build).
