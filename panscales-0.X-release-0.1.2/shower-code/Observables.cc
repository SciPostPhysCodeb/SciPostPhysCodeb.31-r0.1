//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "Observables.hh"
#include "Type.hh"
#include "Momentum.hh"
#include <algorithm>
#include <chrono>

using namespace panscales;

//--------------------------------------------------------------------------
// Constants: could be changed here if desired, but normally should not.
// These are of technical nature, as described for each.

// Minimum number of particles to perform study.
const int Observable::NSTUDYMIN     = 2;

// Maximum number of times that an error warning will be printed.
const int Observable::TIMESTOPRINT  = 1;

//==========================================================================
// ThrustBase class, which has a thrust axis
bool Thrust::set_thrust_axis(const Event& event) {
  
  // #TRK_ISSUE-208  ~~~~ TO DO ~~~~
  // Run the FPE trap on all of this, in particular with rts=1e152 (for 
  // denormal issues) and make all relevant changes it reveals a need for.
  // Easier to do if set_thrust_axis_v2 is disabled.

  // Initial values and counters zero.
  _thrust = 0.; // Thrust value
  _thrust_axis = Momentum(0., 0., 0., 0.); // Thrust axis
  int nStudy = 0;
  std::vector<Momentum> pOrder;
  Momentum pSum;
  precision_type pAbs;
  precision_type two = 2.0;
  
  // Loop over desired particles in the event.
  for (unsigned i = 0; i < event.size(); ++i) {
    // Exclude initial-state particles
    if(event[i].is_initial_state()) continue;
    ++nStudy;
  
    // Store momenta. Use energy component for absolute momentum.
    Momentum pNow = event[i].momentum();
    pAbs = sqrt(pNow.px()*pNow.px() + pNow.py()*pNow.py() + pNow.pz()*pNow.pz());
    pNow = Momentum(pNow.px(), pNow.py(), pNow.pz(), pAbs);
    pSum += pNow;
    pOrder.push_back(pNow);
  }

  //
  // #TRK_ISSUE-209  KH-31-05-19 : TO DO: the following fix should maybe be thought more/better on.
  //
  // KH-30-05-19 : Added in the following two lines after finding that
  // for rts=1e152, the set_thrust_axis_v2 and this function disagreed
  // occasionally on 1-T values that, in dd_real, should really be very
  // much safely in agreement e.g.
  // old =  5.33991696e-09	new =  1.04919856e-15 (multiplicity = 20)
  // After inserting the 2 lines below these two values (and many other
  // similar ones) went to, for example,
  // old =  1.04919856e-15	new =  1.04919856e-15 (multiplicity = 20)
  // I am not clear what is at the bottom of this issue, but suspect
  // FPE issues re. denormals. Investigations with the FPE trap so far
  // were unproductive (set_thrust_axis_v2, also has FPE issues, although,
  // the ones I've seen so far are harmless, e.g., occuring in computing
  // m^2 automatically on declaring a MomentumM2, where the m^2 is never
  // used for anything). Also, FPE issues aside, it looks as though the
  // absence of the two lines below would yield wrong answers for special
  // case events, with 2 or 3 particles, for rts ! = 1 .
  // for (unsigned i = 0; i < event.size(); ++i) pOrder[i] /= pSum.E();
  // pSum /= pSum.E();

  // Account for skipped particles in the event
  for (unsigned i = 0; i < pOrder.size(); ++i) pOrder[i] /= pSum.E();
  pSum /= pSum.E();

  // Very low multiplicities (0 or 1) not considered.
  if (nStudy < Observable::NSTUDYMIN) {
    if (Observable::n_few < Observable::TIMESTOPRINT) std::cout << " Error in "
				       << "Thrust::analyse: too few particles" << std::endl;
    ++Observable::n_few;
    return false;
  }

  // Now get the thrust axis
  // Trivial cases first
  if (nStudy == 2){
    _thrust = 1.0;
    _thrust_axis = two*pOrder[0];
    _thrust_axis = Momentum(_thrust_axis.px(), _thrust_axis.py(), _thrust_axis.pz(), 0.);
    return true;
  } else if (nStudy == 3){
    // compute the energy fractions and obtain the maximum
    _thrust = two*pOrder[0].E()/pSum.E();
    _thrust_axis = two*pOrder[0];
    _thrust_axis = Momentum(_thrust_axis.px(), _thrust_axis.py(), _thrust_axis.pz(), 0.);
    for (int i = 1; i < nStudy; i++){
      if (2.*pOrder[i].E()/pSum.E() > _thrust) {
	_thrust = two*pOrder[i].E()/pSum.E();
	_thrust_axis = two*pOrder[i];
	_thrust_axis = Momentum(_thrust_axis.px(), _thrust_axis.py(), _thrust_axis.pz(), 0.);
      }
    }
    return true;
  } else {
    Momentum ta, pt, pc;
    _thrust = 0.;
    for (int i = 1; i < nStudy; i++){
      for (int j = 0; j < i; j++){
	ta = cross(pOrder[i],pOrder[j]);
	// initialise pt
	pt = Momentum(0., 0., 0., 0.);
	for (int k = 0; k < nStudy; k++) {
	  if ((k != j) && (k != i)) {
	    if (dot3(ta, pOrder[k]) >= 0.) {
	      pt += pOrder[k];
	    } else {
	      pt -= pOrder[k];
	    }
	    pt = Momentum(pt.px(), pt.py(), pt.pz(), 0.);
	  }
	}
	
	for (int k = 0; k < 4; k++) {
	  if (k == 0) pc = pt + pOrder[j] + pOrder[i];
	  if (k == 1) pc = pt + pOrder[j] - pOrder[i];
	  if (k == 2) pc = pt - pOrder[j] + pOrder[i];
	  if (k == 3) pc = pt - pOrder[j] - pOrder[i];
	  pAbs = sqrt(pc.px()*pc.px() + pc.py()*pc.py() + pc.pz()*pc.pz());
	  pc = Momentum(pc.px(), pc.py(), pc.pz(), pAbs);
	  if (pc.E() > _thrust) {
	    _thrust = pc.E();
	    _thrust_axis = pc/pc.E();
	    _thrust_axis = Momentum(_thrust_axis.px(), _thrust_axis.py(), _thrust_axis.pz(), 0.);
	  }
	}
      }
    }
    _thrust = _thrust/pSum.E();
    return true;
  }
}

//==========================================================================
// ThrustBase class, which has a thrust axis
bool Thrust::set_thrust_axis_v2(const Event& event) {

  // Get event momenta to pass to main T-finding code
  std::vector<Momentum> mom;
  for(unsigned ixx = 0; ixx < event.size(); ixx++){
    assert(!event[ixx].is_initial_state() && "Routine does not work for particles in the initial state" );
    mom.push_back(event[ixx].momentum());
  }

  // Total energy in event must be in COM.
  assert(event.Q().px()==0. && event.Q().py()==0. && event.Q().pz()==0.);
  //
  return set_thrust_axis_v2_mom( mom, event.Q().E() );
  //
}

//==========================================================================
// ThrustBase class, which has a thrust axis
bool Thrust::set_thrust_axis_v2_mom(std::vector<Momentum>& p, const precision_type& E_Tot) {

  // ~~~~ TO DO ~~~~
  // * Run the FPE trap on all of this, in particular with rts=1e152 (for 
  //   denormal issues) and make all relevant changes it reveals a need for.
  //   Same comment applies also to the old thrust computation above.
  //   N.B. It might be the case that below we have the FPE trap firing
  //   for innocent and harmless reasons, owing to the fact the momenta
  //   are by default often MomentumM2's for which apparently simple operations
  //   like +, +=, /= etc can sometimes involve a fair bit of extra behind
  //   the scenes gymnastics. It's possible that we inadvertently run
  //   into sqrt's of negative masses along the way, because in computing
  //   things below for getting 1-T and the thrust axis we add up four-vectors
  //   with `weird' signs in front of them. Maybe not.
  // * See comment below about going, very carefully, to using less seeds
  //   in the iterative T-axis finding part. 

  // Initialise thrust and thrust axis
  _thrust = 0.;
  _one_minus_thrust = numeric_limit_extras<precision_type>::max();
  _thrust_axis = Momentum(0., 0., 0., 0., 0.);
  _local_search_failed = false;
  const precision_type one =  1.;
  const precision_type two =  2.;

  // Number of particles in the event
  unsigned int nParticles = p.size();

  // Special cases first
  if(nParticles == 2) {
    _thrust = 1.;
    _thrust_axis = Momentum(2.*p[0].px(), 2.*p[0].py(), 2.*p[0].pz(), 0.);
    _thrust_axis /= (p[0].E()+p[1].E());
    _one_minus_thrust = 0.;
    return true;
    //
  } else {
    //
    // Strategy:
    // ~~~~~~~~~
    // First attempt an approximate thrust calculation, based on an
    // iterative maximisation of T, starting from a suitably chosen `seed'
    // thrust axis. While relatively very fast, this approach does not
    // always yield an axis which truly maximises T, but rather finds only
    // a local maximum in the vicinity of the seed axis. We will mitigate
    // this issue by repeating the calculation with multiple seed axes (8).
    //
    // If all of the calculations, or a subset, agree with one another to
    // some high precision, we accept the result and exit the function. If
    // the result is otherwise considered `suspect' by the latter criteria
    // the full-monty thrust computation is then run.
    //
    //
    // The number of n' seed thrust axis vectors we want to use. 
    unsigned int NnPr = nParticles != 3 ? 8 : 3;
    //
    // Try the iterative thrust axis finder first (HIGHLY recommended), or,
    // skip and go straight to the exact computation.
    bool do_exhaustive_search = true;
    //
    // Inside this `if' is where the magic happens that is the 
    if (_try_local_search) {
      //
      // Get seed axes for the iterative approach T-axis finding. These
      // are constructed from the four hardest momenta which we now get ...
      std::vector<Momentum> fourHardest(4,Momentum(0., 0., 0., 0., 0.));
      unsigned int nMaxHardest = nParticles >= 4 ? 4 : nParticles;
      std::partial_sort( p.begin(), p.begin()+nMaxHardest, p.end(),
                         [](const Momentum& a, const Momentum& b) 
                           { return a.E() > b.E(); } );
      fourHardest.assign( p.begin(), p.begin()+nMaxHardest );
      //
      //
      // #TRK_ISSUE-212  KH-31-05-19 : TO DO : _carefully_ investigate using fewer and fewer
      //                       seed axes in here.
      //
      // Compute starting seed thrust axes vectors, n', from all possible
      // combinations (+ & -) of the four hardest momenta. Each of the
      // four momenta can enter the combs with a +/- sign, leading to 2^4
      // possible sums. However, 8 of the 16 are ultimately reflections
      // of the other 8, given T is n -> -n (and n' -> -n')invariant.
      std::vector<Momentum> nPrime(NnPr,Momentum(0., 0., 0., 0., 0.));
      if( NnPr==8 ) {
        for( unsigned int ixx = 0; ixx < NnPr ; ixx++ ) {
          Momentum3<precision_type> np3 
            =                          ((Momentum3<precision_type>&)fourHardest[0])   
            + (ixx%8 > 3 ? -one : one)*((Momentum3<precision_type>&)fourHardest[1])   
            + (ixx%4 > 1 ? -one : one)*((Momentum3<precision_type>&)fourHardest[2])   
            + (ixx%2 > 0 ? -one : one)*((Momentum3<precision_type>&)fourHardest[3]);
          if (np3.pz()<0) np3 = -one*np3;
          nPrime[ixx] = Momentum::fromP3M2(np3,0.0);
        }
      } else nPrime.assign( fourHardest.begin(), fourHardest.begin()+nMaxHardest );
      // nPrime[0] is equal to the straightforward sum of the four
      // hardest momenta in the event. If there are only four particles
      // in the event then this sum is (0,0,0,Q)  up to rounding errors.
      if(nParticles==4) nPrime[0] = fourHardest[3];
      //
      // Sort the constructed seed thrust axis vectors by their |n|.
      std::sort( nPrime.begin(), nPrime.end(), [](const Momentum& a, const Momentum& b) 
                 { return a.E() > b.E(); } );
      //
      // Normalise the seed n's to |n| = 1.
      for( auto & nPr : nPrime ) nPr /= nPr.E();
      //
      // Prep for iterations to find max T starting from each seed n'.
      unsigned int nMaxIterations = 100;
      std::vector<precision_type> one_minus_thrustCandidates = std::vector<precision_type>(NnPr,numeric_limit_extras<precision_type>::max());
      std::vector<Momentum> thrustAxisCandidates;
      //
      // Loop over the initial n' vectors.
      for( unsigned ixx=0; ixx<NnPr; ixx++ ) {
        //
        // Initialisation for the current considered n' vector.
        Momentum & nPr = nPrime[ixx];
        precision_type one_minus_thrustNow    = numeric_limit_extras<precision_type>::max();
        precision_type one_minus_thrustBefore = one_minus_thrustNow;
        //
        std::vector<bool> signatureNow(nParticles,false);
        std::vector<bool> signatureBefore = signatureNow;
        //
        // The following loop is used to do two things:
        //   i)  use recursion relation for n to get n^(j+1) from current n^(j)
        //  ii) compute 1-T using n^(j) to see if we converged already or not
        unsigned jxx;
        for( jxx = 0; jxx < nMaxIterations; jxx++ ) {
          //
          // nPrNext is the thrust axis as it comes out after one iteration.
          Momentum nPrNext = Momentum(0.,0.,0.,0.);
          //
          // List of bits indicating which side of the dividing plane the 
          // particles lie on (i.e. the sign of three-vector n.p).
          signatureBefore = signatureNow;
          signatureNow.clear();
          //
          // one_minus_thrustBefore is 1-T from previous iteration (jxx->jxx-1)
          // for checking if 1-T has converged.
          one_minus_thrustBefore = one_minus_thrustNow;
          one_minus_thrustNow = 0.;
          //
          // one_minus_thrustNow = Sum_i [ E_p_i - |n^(j).p_i| ] (gets /= Sum_i E_p_i below).
          // n^(j+1) = Sum_i eps(n^(j).p_i) p_i ; eps(n^(j).p_i) = +1/-1 for n^(j).p_i > / < 0.
          for( auto & q : p ) {
            precision_type np_3D = dot3(nPr,q);
            precision_type one_minus_thrustIncrement = np_3D > 0. ?
                dot_product(nPr,q) :
                dot_product(nPr,Momentum(-q.px(),-q.py(),-q.pz(),q.E(),q.m2()));
            one_minus_thrustNow += one_minus_thrustIncrement;
            nPrNext += np_3D > 0. ? q : -one*q;
            signatureNow.push_back( np_3D > 0. ? true : false );
          }
          //
          // Now one_minus_thrustNow is really 1-T, at least for n = n^(j).
          one_minus_thrustNow/=E_Tot;
          //
          // Complete computation of n^(j+1): n.b. nPr is a reference to nPrime[ixx].
          nPr = nPrNext/nPrNext.modp();
          nPr = Momentum::fromP3M2(nPr, 0);
          //
          // To check for convergence we compare (1-T)_now to (1-T)_before.
          // The maximum resolution we can have on that difference is 
          // numeric_limit_extras<precision_type>::epsilon() times the larger of (1-T)_now
          // and (1-T)_before. Since the iterative 1-T search is/should-be
          // always monotonically decreasing, the greater of the two is then
          // (1-T)_before. Hence we set the convergence check as you see it
          // below. We also check that the sign of three-vector n.p agrees
          // between the current and previous iterations, as per Gavin's 
          // suggestion for a robust convergence test (see comment at start
          // of ixx loop).
          if( fabs(   (one_minus_thrustNow-one_minus_thrustBefore)
                    / (_eps*one_minus_thrustBefore)              ) <= 1. 
              && signatureBefore == signatureNow ) {
            one_minus_thrustCandidates[ixx] = one_minus_thrustNow;
            if( one_minus_thrustNow<_one_minus_thrust ) {
              _one_minus_thrust = one_minus_thrustNow;
              _thrust = 1. - _one_minus_thrust;
              _thrust_axis = nPr;
            }
            break;
          }
        // End of loop, index jxx, using recursion relation for n^(j+1)
        }

        // Note: at thispoint, we have checked extensively that the
        // max number of iterations is never reached.
        
      // End of loop, index, ixx, over NnPr different starting seed axes (n')
      }
      //
      // Now we make a decision whether or not to accept the above iterative
      // thrust axis search. If so we return from the function, otherwise we
      // go on with a computation of thrust based on an exhaustive search.
      precision_type maxOneMinusThrust = *max_element(one_minus_thrustCandidates.begin(), one_minus_thrustCandidates.end());
      //
      // Now decide if the local search result is to be trusted or not.
      // The only data one has to make a criterion here with are the 
      // 1-T values from each starting seed and the respective axes (and
      // one could also consider information relating to the history of
      // how those values were reached). For now we will make what we
      // consider is a very tight requirement that all 1-T's from each
      // starting seed agree exactly to within available numerical precision
      // (scaling with 1-T). We also make a cut on events with 1-T > 1e-4
      // based, so far, on just experience of running the code and looking
      // for discrepancies between exact and local-search based methods.
      // #TRK_ISSUE-217  KH TO DO : think to improve the condition below, the first part
      // of it is perhaps too tight.
      if( fabs((maxOneMinusThrust-_one_minus_thrust)/(_eps*_one_minus_thrust)) <= 1.
          && _one_minus_thrust < _max_approx_omthrust ) {
        do_exhaustive_search = false;
        _local_search_failed   = false;
      } else {
        _local_search_failed   = true;
      }
    // End of if { do the approximate thrust axis finding stuff } 
    }
    //
    // Below here is the code for the exhaustive thrust calculation:
    //
    if(do_exhaustive_search) {
      if(nParticles == 3) {
        precision_type E_tot = p[0].E()+p[1].E()+p[2].E();
        _thrust = -numeric_limit_extras<precision_type>::max();
        unsigned int ixx_thrust = 0;
        for (unsigned int ixx = 0; ixx <= 2; ixx++) {
          precision_type thrust_candidate = two*p[ixx].E()/E_tot;
          if (thrust_candidate > _thrust) {
            ixx_thrust = ixx;
	          _thrust = thrust_candidate;
	          _thrust_axis = two*p[ixx];
            _thrust_axis = Momentum::fromP3M2(_thrust_axis, 0);
            _thrust_axis = _thrust_axis/_thrust_axis.E();
	          _thrust_axis = Momentum(_thrust_axis.px(), _thrust_axis.py(), _thrust_axis.pz(), 0.);
          }
        }
        if(ixx_thrust==0)      { _one_minus_thrust = two*dot_product(p[1],p[2]); }
        else if(ixx_thrust==1) { _one_minus_thrust = two*dot_product(p[0],p[2]); }
        else if(ixx_thrust==2) { _one_minus_thrust = two*dot_product(p[0],p[1]); }
        _one_minus_thrust = _one_minus_thrust / (E_tot*E_tot);
        return true;
      }
      //
      //
      // For the exact computation below, i.e. with the iterative local search
      // switched off, the following combination of sorts and rotations gave 
      // a further factor x2 speed up (w.r.t not having them in) for average
      // unweighted multiplicity of ~1000 for a run with settings
      //
      // -shower dire -dynamic-lncutoff -18 -strategy RoulettePeriodic -thrustV2 
      // -weighted-generation -lambda-max 0.5 -alphas 0.10 -nev 1000
      //
      // The two hardest momenta are the first 2 elements after this partial_sort:
      std::partial_sort( p.begin(), p.begin()+2, p.end(),
                         [](const Momentum& a, const Momentum& b) 
                         { return a.E() > b.E(); } );
      Momentum hardest1 = p[0];
      Momentum hardest2 = p[1];
      //
      // Rotate array s.t. two hardest particles go to the end and remove them:
      std::rotate(p.begin(),p.begin()+2,p.end());
      p.pop_back();
      p.pop_back();
      //
      // Sort particles approximately in descending order of their contribution
      // to pt4.E() - pt4.modp() (see below for defn of pt4):
      std::sort( p.begin(), p.end(),
                 [](const Momentum& a, const Momentum& b)
                 { return a.E() - fabs(a.pz()) > b.E() - fabs(b.pz()); } );
      //
      // Put two hardest particles back in the back of the latter sorted vector
      // then move them to the front of it by rotating all elements two places:
      p.push_back(hardest1);
      p.push_back(hardest2);
      std::rotate(p.begin(),p.end()-2,p.end());
      //
      _one_minus_thrust = numeric_limit_extras<precision_type>::max();
      Momentum pc;
      for( unsigned i = 1; i < nParticles; i++ ) {
        new_i:
        for( unsigned j = 0; j < i; j++ ) {
          new_j:
          Momentum ta = cross(p[i],p[j]);
          // initialise pt
          Momentum4<precision_type> pt4 = Momentum4<precision_type>(0., 0., 0., 0., 0.);
          for( unsigned k = 0; k < nParticles; k++ ) {
            if( (k != j) && (k != i) ) {
              const Momentum4<precision_type> & pk = p[k];
              precision_type pm = dot3(ta, pk) >= 0 ? 1 : -1;
              pt4 += pm > 0 ? pk : Momentum4<precision_type>(pm*pk.px(),pm*pk.py(),pm*pk.pz(),pk.E(),0);
              //
              // #TRK_ISSUE-218  KH-31-05-19:
              // Maximum resolution on nominal RHS of inequality, i.e.
              // pt4.E() - pt4.modp() is _eps * pt4.E() (TO DO: check more
              // carefully). The thing on the RHS IS an overestimate afterall,
              // and it should be a big one which _should_ imply not having
              // to worry too badly about precision issues here.
              // Perhaps consider replacing pt4 by pt, a MomentumM2, which will 
              // mean that the += in pt += ... updates pt.m2 by the `accurate'
              // internal_set_m2_from_pair(m2_, pk.m2(), dot) , and then one
              // could use in the 'if' below pt.m2()/(pt.E()+pt.modp()), which
              // is maybe more accurate (???), by replacing the potentially
              // nasty E-|p| by a possibly less nasty m2/(E+|p|). Note, however,
              // call to internal_set_m2_from_pair which would happen if pt4 is
              // replaced by a MomentumM2, is going to add a *possibly* expensive
              // time penalty. So if considering this, one should bear in mind
              // also simpler more crude options at the same time along the lines
              // of just making _intermediate_bound_margin bigger ...
              if(  E_Tot * _one_minus_thrust
                 < pt4.E()*(1.-_intermediate_bound_margin*_eps) - pt4.modp() ) {
                if( j<i-1 ) { j++; goto new_j; }
                if( j==i-1 && i<nParticles-1 ) { i++; goto new_i; }
                if( j==i-1 && i==nParticles-1) { goto theEnd; }
              }
            }
          }
          Momentum4<precision_type> pt;// = Momentum(pt4.px(),pt4.py(),pt4.pz(),pt4.E(),0.);
          for( unsigned k = 0; k < 4; k++ ) {
            if( k == 0 )      pt = pt4 + p[j] + p[i];
            else if( k == 1 ) pt = pt4 + p[j] - p[i];
            else if( k == 2 ) pt = pt4 - p[j] + p[i];
            else if( k == 3 ) pt = pt4 - p[j] - p[i];
            // Flip thrust axis s.t. it always points in +z hemisphere? (Perhaps
            // can help enable ideas for making the code simpler/faster).
            // if(pc.pz()<0) pc = Momentum(-pc.px(),-pc.py(),-pc.pz(),0,0);
            pc = Momentum::fromP3M2(pt, 0);
            // Handling exception.
            // If the pc.E() is zero at this point, it means all (x,y,z) components of pc are
            // are zero too. I.e. pc has no direction. It is not then sensible to be able to 
            // go on and select this pc as thrust axis, as can happen in the block starting
            // if( one_minus_thrustRunningTotal < _one_minus_thrust ) { ... }
            // just below. Since nothing else is computed below that is relevant to the further
            // running of the algorithm we therefore skip around the loop to the next k here.
            // This situation may arise due to numerical precision loss.
            if(pc.E()==0) continue;
            pc /= pc.E();
            precision_type one_minus_thrustRunningTotal = 0;
            for( auto q : p ) {
              q = dot3(pc,q) > 0 ? q : Momentum(-q.px(),-q.py(),-q.pz(),q.E(),0);
              one_minus_thrustRunningTotal += dot_product(pc,q);
              // #TRK_ISSUE-220 GPS 2019-05-23: this works well perhaps in part
              // because the particles come in a very special order (the
              // q, qbar always first in the list). More generally I
              // wonder if it would make sense to order the particles in
              // decreasing energy. 
              // 
              // Another ordering question applies to the particles used
              // to set the hemisphere: might it make sense to order
              // them according to cos(theta) to some approx axis
              // and then work one's way out from the pair with the
              // smallest |cos theta|? (starting with a pair where
              // one has positive cos theta, the other negative?)
              // Not entirely sure about this...
              if( E_Tot * _one_minus_thrust < one_minus_thrustRunningTotal ) break;
            }
            one_minus_thrustRunningTotal /= E_Tot;
            if( one_minus_thrustRunningTotal < _one_minus_thrust ) {
               _one_minus_thrust = one_minus_thrustRunningTotal;
               _thrust = 1 - _one_minus_thrust;
               _thrust_axis = Momentum(pc.px(),pc.py(),pc.pz(), 0.);
               //
            }
          }
        }
      }
    }
    theEnd:
    //
    // Finally, for consistency with original set_thrust_axis function
    _thrust_axis = Momentum(_thrust_axis.px(),_thrust_axis.py(),_thrust_axis.pz(), 0.);
    //
    return true;
  }
}

//--------------------------------------------------------------------------
// Provide a listing of the info.
void Thrust::list() const {

  // Header.
  std::cout << "\n ---------------  Thrust  --------------- \n"
       << "\n          value      e_x       e_y       e_z \n";

  // The thrust, major and minor values and related event axes.
  std::cout << " Thr = " << _thrust << " - (" << _thrust_axis.px()
       << "," << _thrust_axis.py() << "," << _thrust_axis.pz() << ")\n";
  std::cout << "\n -------------------------------------------" << std::endl;

}

//======================================================================
// FCx class - fractional moments of energy-energy correlation
//----------------------------------------------------------------------
bool FCx::analyse(const Event& event, const Momentum& thrust_axis) {
  // initialize
  int nStudy = 0;
  std::vector< Momentum > pOrder;
  Momentum pSum;
  precision_type pAbs;
  
  _fcx = 0.0;

  // Loop over desired particles in the event.
  for (unsigned i = 0; i < event.size(); ++i) {
    // Exclude initial-state emissions
    if(event[i].is_initial_state()) continue;
    ++nStudy;

    // Store momenta. Use energy component for absolute momentum.
    Momentum pNow = event[i].momentum();
    pAbs = sqrt(pNow.px()*pNow.px() + pNow.py()*pNow.py() + pNow.pz()*pNow.pz());
    pNow = Momentum(pNow.px(), pNow.py(), pNow.pz(), pAbs);
    pSum += pNow;
    pOrder.push_back(pNow);
  }

  // Very low multiplicities (0 or 1) not considered.
  if (nStudy < Observable::NSTUDYMIN) {
    if (Observable::n_few < Observable::TIMESTOPRINT) std::cout << " Error in "
				  << "FCx::analyse: too few particles" << std::endl;
    ++Observable::n_few;
    return false;
  }

  // Now compute FCx - implementation inspired to EvShpLib
  for (int i = 1; i < nStudy; i++){
    for (int j = 0; j < i; j++){
      precision_type hemisphere_i = dot3(pOrder[i], thrust_axis);
      precision_type hemisphere_j = dot3(pOrder[j], thrust_axis);      
      if (hemisphere_i*hemisphere_j > 0.0) {
	precision_type cost  = dot3(pOrder[i],pOrder[j])/pOrder[i].E()/pOrder[j].E();
	precision_type sint2 = 1.0 - cost*cost; 
	_fcx = _fcx + pOrder[i].E()*pOrder[j].E() * pow(sint2,_x/2.0) * pow((1.0 - cost),1.0-_x);
      }
    }
  }
  _fcx = _fcx*2.0/pow(pSum.E(),2.0);
  return true;
}

/// ---------------------------------------------------------------
/// FCx class again, as above, but aiming for better numerical
/// stability making use of dot and cross product operations.
/// This function assumes: 1) ALL input momenta are MASSLESS
///                        2) we are in the EVENT COM
///                        3) ALL FS particles contribute
/// ---------------------------------------------------------------
bool FCx::analyse(const Event& event, const Momentum& n,
                     bool all_massless) {

  /// Make sure no-one tries feeding in massive particles ...
  /// It is the user's responsibility to flag they understood
  /// the scope of this function by setting all_massless properly
  /// as we can't afford to check whether every particle here.
  assert(all_massless && "FCx::analyse:"
         "\n this version of analyse is only intended for use with"
         "\n events comprising of just massless particles. To use "
         "\n the original version of FCx::analyse that should work"
         "\n with massive particles, use the two-argument version "
         "\n of this function");

  MomentumM2<precision_type> massless_axis = MomentumM2<precision_type>::fromP3M2(_thrust_axis.p3(),0.0);
  MomentumM2<precision_type> minus_axis    = MomentumM2<precision_type>::fromP3M2(-_thrust_axis.p3(),0.0);

  /// Get event momenta
  std::vector<Momentum> p;
  for(auto & q : event.particles()){
    if (q.is_initial_state()) continue;
    p.push_back(q.momentum());
  }

  /// Since the thrust axis is fixed we can limit the number of
  /// times we compute pi.n / pj.n by making a vector of bools.
  std::vector<bool> pn;
  for(auto & q : p) pn.push_back( dot3(q,n) >= 0 ? true : false );

  /// Initialise running total
  _fcx  = 0.0;
  _taux = 0.0;

  /// Event energy squared 
  precision_type Q2 = event.Q2();

  // Now compute FCx
  ///
  /// Special code is executed for x = 0, ½, 1. For x=0 and x=1
  /// one only needs either the dot- or cross-product computed
  /// at each cycle, rather than both. Also in cases x = 0, ½, 1
  /// the costly pow(...) function can be completely avoided.
  /// The special cases could have instead been implemented by
  /// using if's within the nested loop, but I worried this might
  /// have a cpu cost that is not balanced by the slightly more
  /// compact code that results. 
  ///
  if(_x==1.0) {
    for(unsigned i = 1; i < p.size(); i++) {
      // FCx
      for(unsigned j = 0; j < i; j++) {
        if(!(pn[i]==pn[j])) continue;                         /// ϴ[(pi.n)(pj.n)]
        precision_type pi_x_pj = cross(p[i],p[j],true).E();   /// Ei Ej sinϴij
        _fcx += pi_x_pj;
      }
      // taux
      precision_type pi_x_n = cross(p[i],massless_axis,true).E();   /// Ei sinϴij
      _taux += pi_x_n;
    }
  } else if(_x==0.5) {
    for(unsigned i = 1; i < p.size(); i++) {
      // FCx
      for(unsigned j = 0; j < i; j++) {
        if(!(pn[i]==pn[j])) continue;                         /// ϴ[(pi.n)(pj.n)]
        precision_type pi_dot_pj = dot_product(p[i],p[j]);    /// Ei Ej (1-cosϴij)
        precision_type Ei_Ej = p[i].E() * p[j].E();           /// Ei Ej
        if(pi_dot_pj>Ei_Ej) pi_dot_pj = 2*Ei_Ej-pi_dot_pj;    /// Ei Ej (1-|cosϴij|)
        precision_type pi_x_pj   = cross(p[i],p[j],true).E(); /// Ei Ej sinϴij
        _fcx += sqrt(pi_x_pj*pi_dot_pj);
      }
      // taux
      precision_type pi_dot_n = dot_product(p[i],massless_axis);     // Ei Ej (1-cosϴij)
      if(pi_dot_n>p[i].E()) pi_dot_n = dot_product(p[i],minus_axis); // Ei Ej (1-|cosϴij|)
      precision_type pi_x_n = cross(p[i],massless_axis,true).E();    // Ei sinϴij
      _taux += sqrt(pi_x_n*pi_dot_n);
    }
  } else if(_x==0.0) {
    for(unsigned i = 1; i < p.size(); i++) {
      // FCx
      for(unsigned j = 0; j < i; j++) {
        if(!(pn[i]==pn[j])) continue;                         /// ϴ[(pi.n)(pj.n)]
        precision_type pi_dot_pj = dot_product(p[i],p[j]);    /// Ei Ej (1-cosϴij)
        precision_type Ei_Ej = p[i].E() * p[j].E();           /// Ei Ej
        if(pi_dot_pj>Ei_Ej) pi_dot_pj = 2*Ei_Ej-pi_dot_pj;    /// Ei Ej (1-|cosϴij|)
        _fcx += pi_dot_pj;
      }
      // taux
      precision_type pi_dot_n = dot_product(p[i],massless_axis);     // Ei Ej (1-cosϴij)
      if(pi_dot_n>p[i].E()) pi_dot_n = dot_product(p[i],minus_axis); // Ei Ej (1-|cosϴij|)
      _taux += pi_dot_n;
    }
  } else {
    for(unsigned i = 1; i < p.size(); i++) {
      for(unsigned j = 0; j < i; j++) {
        if(!(pn[i]==pn[j])) continue;                         /// ϴ[(pi.n)(pj.n)]
        precision_type pi_dot_pj = dot_product(p[i],p[j]);    /// Ei Ej (1-cosϴij)
        precision_type Ei_Ej = p[i].E() * p[j].E();           /// Ei Ej
        if(pi_dot_pj>Ei_Ej) pi_dot_pj = 2*Ei_Ej-pi_dot_pj;    /// Ei Ej (1-|cosϴij|)
        precision_type pi_x_pj   = cross(p[i],p[j],true).E(); /// Ei Ej sinϴij
        _fcx += pi_dot_pj*pow(pi_x_pj/pi_dot_pj,_x);
      }
      // taux
      precision_type pi_dot_n = dot_product(p[i],massless_axis);     // Ei Ej (1-cosϴij)
      if(pi_dot_n>p[i].E()) pi_dot_n = dot_product(p[i],minus_axis); // Ei Ej (1-|cosϴij|)
      precision_type pi_x_n = cross(p[i],massless_axis,true).E();    // Ei sinϴij
      _taux += pi_dot_n * pow(pi_x_n/pi_dot_n, _x);
    }
  }
  _fcx  *= 2.0/Q2;
  _taux /= sqrt(Q2);
  return true;
}

//--------------------------------------------------------------------------

// Provide a listing of the info.
void FCx::list() const {
  // Header.
  std::cout << "\n -----  Fcx  ------ \n";
  std::cout << " FCx = " << _fcx  << "\n";
  std::cout << " taux = " << _taux  << "\n";
  std::cout << "\n ----------------------" << std::endl;
}

/// ---------------------------------------------------------------
/// Broadening computation.
/// Trying to be as tight as possible to the EvShpLib computation
/// in Event2/EvShpLib/dpevent_shapes.f90 in this repository.
/// This function is hopefully temporary and will be deleted in
/// favour of ::analyse beneath it once all checks out well enough.
/// ---------------------------------------------------------------
bool Broadening::analyseEvShpLib(const Event& event, const Momentum& n) {

  precision_type rts = sqrt(event.Q2());

  precision_type Bt, Bw, Bs;
  precision_type tmp = n.modp();
  tmp *= tmp;

  precision_type Bl, Br;

  Bl = Br = 0.0;

  for(unsigned ixx=0; ixx<event.size(); ixx++) {
    Bt = dot3(event[ixx].momentum(),n);

    Momentum pt = event[ixx].momentum() - Bt*n/tmp;
    Bw = pt.modp();

    Bt > 0. ? Bl += Bw : Br += Bw;
  }

  Bl /= rts;
  Br /= rts;
  Bl /= 2.0;
  Br /= 2.0;
  Bt = Bl + Br;
  Bw = std::max(Bl,Br);

  n.pz()<0. ? Bs = Br : Bs = Bl;

  _total = Bt;
  _wide_jet = Bw;
  _single_jet = Bs;

  return true;
}

/// ---------------------------------------------------------------
/// Broadening computations.
/// This function assumes: 1) ALL input momenta are MASSLESS
///                        2) we are in the EVENT COM
///                        3) ALL FS particles contribute
/// ---------------------------------------------------------------
bool Broadening::analyse(const Event& event, const Momentum& n) {

  /// Annoying computation of event COM energy every time. (But not
  /// a big cost in the overall context of this function).
  precision_type rts = sqrt(event.Q2());

  /// Initialise running totals
  _total = _wide_jet = _single_jet = 0.;

  /// Get separate left & right jet broadening.
  precision_type b, bl, br;
  bl = br = 0.0;
  for(auto & p : event.particles()) {
    // Exclude initial-state particles
    if(p.is_initial_state()) continue;
    Momentum q = p.momentum();
    b = cross(q,n,true).E();
    dot3(q,n) >= 0. ? bl += b : br += b;
  }

  /// Normalise
  bl /= 2.*rts;
  br /= 2.*rts;

  /// Final answers
  _total = bl + br;
  _wide_jet = std::max(bl, br);
  _single_jet = n.pz() < 0.0 ? br : bl;
               
  return true;
}

//--------------------------------------------------------------------------

// Provide a listing of the info.
void Broadening::list() const {
  // Header.
  std::cout << "\n -----------  Broadening  ------------ \n";
  std::cout << " Total      = " << _total      << "\n";
  std::cout << " Wide jet   = " << _wide_jet   << "\n";
  std::cout << " Single jet = " << _single_jet << "\n";
  std::cout << "\n ------------------------------------- \n";
}
 

//======================================================================
// η-φ patch class
// Particles entering here are strictly assumed to be massless. Thrust
// axis is assumed to have |n|=1.  
//----------------------------------------------------------------------
bool Patch::analyse(const Event& event, const Momentum& ref_axis) {

  // Initialization.
  Momentum p,p_tot;
  //
  _patch_E  = _patch_Et = 0.;

  // Very low multiplicities (0 or 1) not considered.
  if(event.size() < 2) {
    std::cout << "\nError in Patch::analyse.\n";
    std::cout << "\nEvent only has " << event.size() << " particles!\n";
    return false;
  }

  // If the event has two particles there is no E,tot & E_t,tot in the η-φ patch.
  if(event.size() == 2) return true;

  // choice of reference vector for the phi computation
  //
  // Note: perp1 = axisdir x perp2 (in general, perp2 is the "in",
  // perp1 the "out"). If "axis" is along z, this puts "perp2" along x
  // and perp1 along y
  //
  // By default we measure angles from perp2 (unless randomised in
  // whih case perp2 corresponds to phi=0), i.e.
  //   perp_xx = cos(random_phi) perp2 + sin(random_phi) perp1 
  // 
  //
  // In practice, for a momentum p, dphi is the angle between the
  // (axis, perp2) plane and the (axis,p) plane, i.e. the angle between
  //   unit(axis x perp_xx)    and unit(axis x p)
  // where unit(v) goes in the direction of v and has norm 1
  // We have
  //   unit(axis x perp_xx) = cos(random_phi) perp1 - sin(random_phi) perp2
  //(defined as a massless 4-vector)
  // Note that we'll take 1-cos(theta) so the "unit" norm can be forgotten
  //
  // Finally, we have to compute unit(axis x p) which is OK as long as
  // p is not along the axis (in which case we take phi="random_phi")
  
  Momentum perp1, perp2;
  auto axis = (ref_axis.pz()<0) ? ref_axis.reversed() : ref_axis;
  two_perp(axis, axis.reversed(), perp1, perp2);
  Momentum perp_yy;
  precision_type random_phi = 0.0;
  if (_use_random_phi){
    random_phi = _gsl_ptr->uniform(0, 2*M_PI);
    // this is the cross [product between thrust
    // and cos(random_phi) * perp2 + sin(random_phi) * perp1
    perp_yy = cos(random_phi) * perp1 - sin(random_phi) * perp2;
  } else {
    perp_yy = perp1;
  }
  Momentum perp = Momentum::fromP3M2(perp_yy, 0.0);
  precision_type one_minus_cos_phirange = 1-cos(_phi_range);
  
  // Compute E,tot & E_t,tot in the η-φ patch.
  for (unsigned i = 0; i < event.size(); i++) {
    // skip the initial-state partons
    if(event[i].is_initial_state()) continue;
    //
    p = event[i].momentum();
    p_tot += p;
    //
    // Rapidity of emission w.r.t input axis: y = 0.5 ln ((E + k.T)/(E - k.T))
    precision_type rap = log_T( (p.E() + dot3(p, axis))
                               /(p.E() - dot3(p, axis)) ) / 2;
    //
    // The treatment of the phi angle depends on whether we use the
    // old or new version
    bool passphi = false;
    if (_use_old_phi){
      //TODO: Add support for the random phi option.
      precision_type dphi = p.phi() - axis.phi();
      if(dphi >  M_PI) dphi -= 2*M_PI;
      if(dphi < -M_PI) dphi += 2*M_PI;
      passphi = (std::abs(dphi - _phi_center) < _phi_range);
    } else {                          // lightlike
      Momentum axp = cross(axis, p, true);
      passphi = (one_minus_costheta(perp, axp) < one_minus_cos_phirange);
    }
      
    //
    // If the emission is in the patch update E_tot and E_t,tot.
    if( passphi && (std::abs(rap  - _rap_center) < _rap_range) ) {
      _patch_E  += p.E();
      _patch_Et += cross(p, axis, true).E();
    }
  }
  _patch_E  /= p_tot.E();
  _patch_Et /= p_tot.E();
  return true;
}


//======================================================================
// IsolationConeEnergy
//----------------------------------------------------------------------
bool IsolationConeEnergy::analyse(const Event& event, const Momentum& thrust_axis) {

  // Initialization.
  Momentum p,p_tot;
  _cone_E  = 0.;

  // Very low multiplicities (0 or 1) not considered.
  if(event.size() < 2) {
    std::cout << "\nError in IsolationConeEnergy::analyse.\n";
    std::cout << "\nEvent only has " << event.size() << " particles!\n";
    return false;
  }

  // If the event has two particles there is no E,tot & E_t,tot in the η-φ patch.
  if(event.size() == 2) return true;

  // choice of reference vector for the phi computation
  // See the "Patch" comments above
  
  Momentum perp1, perp2;
  two_perp(thrust_axis, thrust_axis.reversed(), perp1, perp2);
  Momentum perp_yy;
  precision_type random_phi = 0.0;
  if (_use_random_phi){
    random_phi = _gsl_ptr->uniform(0, 2*M_PI);
    perp_yy = cos(random_phi) * perp1 - sin(random_phi) * perp2;
  } else {
    perp_yy = perp1;
  }
  Momentum perp = Momentum::fromP3M2(perp_yy, 0.0);
  precision_type one_minus_cosR = 1-cos(_radius);

  // compute hte energy in the cone
  precision_type Etot = 0;
  for (const auto & p_local: event.particles()) {
    Etot += p_local.E();
    if (one_minus_costheta(perp, p_local) < one_minus_cosR) {
      _cone_E += p_local.E();
    }
  }

  _cone_E /= Etot;
  return true;
}



//==========================================================================
// y3 Durham class, angular ordered algorithm for tests
bool Y3::analyse(const Event& event) {

  // Initial values and counters
  _y3 = 0.0;
  int clust_1 = 0;
  int clust_2 = 0;
  std::vector< Momentum > pjet;

  for (unsigned i = 0; i < event.size(); i++) {
    pjet.push_back(event[i].momentum());
  }

  int njet = event.size();

  if (njet < 2) {
    std::cout << " Error in "
          << "Y3::analyse: too few particles" << std::endl;
    return false;
  }
  if (njet == 2){
    _y3 = 0.0;
    return true;
  }

  while (true) {
    precision_type vmax = 1e300;
    for (int i = 0; i < njet - 1; i++) {
      for (int j = i+1; j < njet; j++) {

        precision_type vij = one_minus_costheta(pjet[i],pjet[j]);

        if (vij < vmax) {
          clust_1 = i;
          clust_2 = j;
          vmax = vij;
        }
      }
    }

    precision_type Emin = pjet[clust_1].E();
    if (pjet[clust_1].E() > pjet[clust_2].E()) {
      Emin = pjet[clust_2].E();
    }

    precision_type yij = 2.0 * pow(Emin,2)/pow(_rts,2) * vmax;

    if (yij > _y3) {
      _y3 = yij;
    }

    // now recombine in the E-scheme
    pjet[clust_1] += pjet[clust_2];
    if (clust_2 != njet-1){
      pjet[clust_2] = pjet[njet-1];
    }
    njet -= 1;

    if (njet == 2) {
      return true;
    }
  } 
}

//--------------------------------------------------------------------------

// Provide a listing of the info.
void Y3::list() const {
  // Header.
  std::cout << "\n -----  Y3 Durham (Ang. Ord.)  ------ \n";
  std::cout << " y3 = " << _y3  << "\n";
  std::cout << "\n ----------------------" << std::endl;
}

bool CParam::analyse(const Event& event) {
  // NB: this implementation takes O(N^2) time for N particles
  //     and is not yet set up to use direction differences
  //     It requires particles to be massless.
  unsigned n = event.size();
  _Cparam = 0.0;
  for (unsigned i = 1; i < n; i++) {
    // currently implemented only for massless particles
    // (which allows us to assume E == modp)
    assert(event[i].m2() == 0); 
    for (unsigned j = 0; j < i; j++) {
      // The squared cross product is Ei^2 Ej^2 sin^2 thetaij.
      //
      // When events are aligned along the z axis, it should be fairly
      // resilient to rounding errors because same-side and
      // opposite-side cross products will always involve the
      // multiplication of a small transverse component with a large z
      // component, but there should never cancellations between
      // two large numbers
      //
      precision_type cross_sq = cross(event[i],event[j]).modpsq();
      // check it's non-zero to avoid division by zero when one or other
      // energy is zero
      if (cross_sq != 0) _Cparam += cross_sq / (event[i].E() * event[j].E());

      // // earlier formulations
      // precision_type sinsq_theta_2;
      // if (!use_dirdiffs_) {
      //   precision_type omct = one_minus_costheta(event[i], event[j]);
      //   sinsq_theta_2 = omct / 2;
      // }
      // else {
      //   // 1-costheta = 2*sin^2(theta/2) = delta^2/2
      //   sinsq_theta_2 = event[i].direction_diff(event[j]).modpsq() / 4;
      // }
      // precision_type cossq_theta_2 = 1 - sinsq_theta_2;
      // precision_type sinsq_theta = 4 * cossq_theta_2 * sinsq_theta_2;
      // _Cparam += event[i].E() * event[j].E() * sinsq_theta; 
    }
  }
  _Cparam *= 3.0 / event.Q2();
  return true;
}

