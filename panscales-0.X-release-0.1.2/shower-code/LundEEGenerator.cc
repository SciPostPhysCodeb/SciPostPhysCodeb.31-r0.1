
// $Id: LundEEGenerator.cc 1153 2018-08-22 11:56:35Z frdreyer $
//
// Copyright (c) -, Frederic A. Dreyer, Gavin P. Salam, Gregory Soyez
//
//----------------------------------------------------------------------
// This file is distributed with PanScales but it is 
// part of FastJet contrib.
//
// It is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License, or (at
// your option) any later version.
//
// It is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this code. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

#include <sstream>
#include "LundEEGenerator.hh"
#include "EECambridgeOneMinusCosTheta.hh"
#include "EECambridgeDirDiff.hh"
#include "Type.hh"

using namespace std;
using namespace panscales;



FJCORE_BEGIN_NAMESPACE      // defined in fastjet/internal/base.hh

//----------------------------------------------------------------------
/// Returns the 3-vector cross-product of p1 and p2. If lightlike is false
/// then the energy component is zero; if it's true the the energy component
/// is arranged so that the vector is lighlike
PseudoJet cross_product(const PseudoJet & p1, const PseudoJet & p2, bool lightlike) {
  precision_type px = p1.py() * p2.pz() - p2.py() * p1.pz();
  precision_type py = p1.pz() * p2.px() - p2.pz() * p1.px();
  precision_type pz = p1.px() * p2.py() - p2.px() * p1.py();

  precision_type E;
  if (lightlike) {
    E = sqrt(px*px + py*py + pz*pz);
  } else {
    E = 0.0;
  }
  return PseudoJet(px, py, pz, E);
}

namespace contrib{

LimitedWarning RecursiveLundEEGenerator::_psibar_not_finite;



  
LundEEDeclustering::LundEEDeclustering(const PseudoJet& pair,
				       const PseudoJet& j1, const PseudoJet& j2,
				       int iplane, double psi, double psibar,
               int depth, int leaf_iplane, int sign_s)
  : iplane_(iplane), psi_(psi), psibar_(psibar), m_(pair.m()), pair_(pair), depth_(depth), leaf_iplane_(leaf_iplane), sign_s_(sign_s) {

  precision_type one_minus_costheta;
  if (j1.has_user_info<DirDiffUserInfo>()) {
    one_minus_costheta = j1.user_info<DirDiffUserInfo>().omc_to(j2);
  } else {
    // to work out angles, etc. reliably, we're going to use the
    // EECamBriefJet
    EECamBriefJet j1_brief, j2_brief;
    j1_brief.init(j1); j2_brief.init(j2);
    one_minus_costheta = j1_brief.distance(&j2_brief);
  }
  if (one_minus_costheta > numeric_limit_extras<precision_type>::sqrt_epsilon()) {
    precision_type cos_theta = 1.0 - one_minus_costheta;
    precision_type theta     = acos(cos_theta);
    sin_theta_ = sin(theta);
    eta_       = to_double(-log(tan(theta/2.0)));
  } else {
    // we are at small angles, so use small-angle formulas
    precision_type theta = sqrt(2. * one_minus_costheta);
    sin_theta_ = theta;
    //eta_       = -0.5*log(0.5*one_minus_costheta);  // this would avoid the square root but we favour readability
    eta_       = to_double(-log(theta/2));
  }

  // establish which of j1 and j2 is softer
  if (j1.modp2() > j2.modp2()) {
    harder_ = j1;
    softer_ = j2;
  } else {
    harder_ = j2;
    softer_ = j1;
  }

  // now work out the various LundEE declustering variables
  precision_type softer_modp = softer_.modp();
  z_   = softer_modp / (softer_modp + harder_.modp());
  kt_  = softer_modp * sin_theta_;
  lnkt_ = to_double(log(kt_));
  kappa_ = z_ * sin_theta_;
}
  
//----------------------------------------------------------------------
/// return a vector of the declusterings of the primary plane of the
/// jet, registering information about the plane from which this
/// comes
std::vector<LundEEDeclustering> LundEEGenerator::result_iplane(const PseudoJet& jet, int iplane) const {
  std::vector<LundEEDeclustering> result;
  PseudoJet j = jet;
  PseudoJet pair, j1, j2, cross;
  pair = j;
  while (pair.has_parents(j1, j2)) {
    // order the jets in their 3-mom norm
    if (j1.modp2() < j2.modp2()) std::swap(j1,j2);
    // on the first invocation cross has its default value of 0; 
    result.push_back(single_declustering(pair, j1, j2, cross, iplane));
    pair = j1;
  }
  return result;
}

//----------------------------------------------------------------------
/// description
std::string LundEEGenerator::description() const {
  std::ostringstream oss;
  oss << "LundEEGenerator "; 
  return oss.str();
}


  
//----------------------------------------------------------------------
/// get declustering corresponding to a given pseudojet and its parents.
///
/// If cross is a null vector, then the psi value for the declustering
/// will be set to zero and the cross vector will be defined as
/// j1 x j2.
///
/// If cross is non-zero, then the psi value is determined as the
/// angle between this j1 x j2 and the existing cross.  
LundEEDeclustering LundEEGenerator::single_declustering(const PseudoJet& pair,
							const PseudoJet& j1,
							const PseudoJet& j2,
							PseudoJet& cross,
							int iplane) const {
  // get the angle between current declustering plane and previous one
  PseudoJet j1xj2 = cross_product(j1,j2,false);
  // the first time one encounters the next line cross == 0
  // (default PJ initialisation), so one simply defines the direction 
  // meant by psi=0
  double psi;
  if (cross == precision_type(0)) {
    psi = 0;
    cross = j1xj2;
    // normalise it 
    cross /= cross.modp();
  } else {
    psi = to_double(theta(cross, j1xj2));
  }

  // prepare for next iteration
  // return declustering element
  return LundEEDeclustering(pair, j1, j2, iplane, psi);
}

//----------------------------------------------------------------------
/// create an ordered declustering sequence for one jet
std::vector<LundEEDeclustering> LundEEGeneratorOrdered::result_iplane(const PseudoJet& jet,
								      int iplane) const {
  std::priority_queue< std::pair<double, LundEEDeclustering> > pq_declusts;
  this->fill_pq(jet, iplane, pq_declusts);
  return this->pq_to_vector(pq_declusts);
}

//----------------------------------------------------------------------
/// create an ordered declustering sequence for two jets
std::vector<LundEEDeclustering> LundEEGeneratorOrdered::result_twojets(const PseudoJet& jet1,
								       const PseudoJet& jet2) const {
  std::priority_queue< std::pair<double, LundEEDeclustering> > pq_declusts;
  this->fill_pq(jet1, 0, pq_declusts);
  this->fill_pq(jet2, 1, pq_declusts);
  return this->pq_to_vector(pq_declusts);
}

//----------------------------------------------------------------------
/// create an ordered declustering sequence for multiple jets, along with n secondary planes
std::vector<LundEEDeclustering> LundEEGeneratorOrdered::result_jets(const std::vector<PseudoJet>& jets,
								    int n) const{
  std::priority_queue< std::pair<double, LundEEDeclustering> > pq_declusts;
  unsigned int i=0;
  for(; i < jets.size(); ++i)
    this->fill_pq(jets[i], i, pq_declusts);
  std::vector< std::pair<double, LundEEDeclustering> >  tmp;
  // get the n elements from which we want secondary planes
  for(int isec=0; isec < n; ++isec) {
    tmp.push_back(pq_declusts.top());
    pq_declusts.pop();
  }
  // get the n secondary declustering sequences and insert them into priority queue
  for(int isec=0; isec < n; ++isec) {
    this->fill_pq(tmp[isec].second.softer(), isec+i, pq_declusts);
    pq_declusts.push(tmp[isec]);
  }
  // return sorted vector of all the declusterings
  return this->pq_to_vector(pq_declusts);
}
  
//----------------------------------------------------------------------
/// fill a priority queue with ordered declusterings
void LundEEGeneratorOrdered::fill_pq(const PseudoJet& jet, int iplane,
               std::priority_queue< std::pair<double,LundEEDeclustering> >& pq) const {
  
  PseudoJet j = jet;
  PseudoJet pair, j1, j2, cross;
  pair = j;
  while (pair.has_parents(j1, j2)) {
    // order the jets in their 3-mom norm
    if (j1.modp2() < j2.modp2()) std::swap(j1,j2);
    LundEEDeclustering d = single_declustering(pair, j1, j2, cross, iplane);
    pq.push(std::make_pair(this->ordering_measure(d), d));
    pair = j1;
  }
}

//----------------------------------------------------------------------
/// turn a priority queue to an ordered vector
std::vector<LundEEDeclustering> LundEEGeneratorOrdered::pq_to_vector(std::priority_queue< std::pair<double,
								     LundEEDeclustering> >& pq) const {
  std::vector<LundEEDeclustering> result;
  PseudoJet cross;
  while (!pq.empty()) {
    LundEEDeclustering d = pq.top().second;
    // update the psi angle according to the new ordering
    PseudoJet newcross = PseudoJet(d.harder().py()*d.softer().pz()
				   - d.harder().pz()*d.softer().py(),
				   d.harder().pz()*d.softer().px()
				   - d.harder().px()*d.softer().pz(),
				   d.harder().px()*d.softer().py()
				   - d.harder().py()*d.softer().px(), 0.0);
    if (cross != precision_type(0)) d.set_psi(to_double(theta(cross, newcross)));
    cross = newcross;
    // add declustering to result vector
    result.push_back(d);
    pq.pop();
  }
  return result;
}

} // namespace contrib


FJCORE_END_NAMESPACE

std::ostream & operator<<(std::ostream & ostr, const fjcore::contrib::LundEEDeclustering & d) {
  ostr << "kt = "   << d.kt()
       << " z = "   << d.z()
       << " eta = " << d.eta()
       << " psi = " << d.psi()
       << " psibar = " << d.psibar()
       << " m = "   << d.m()
       << " iplane = " << d.iplane()
       << " depth = " << d.depth()
       << " leaf_iplane = " << d.leaf_iplane();
  return ostr;
}

