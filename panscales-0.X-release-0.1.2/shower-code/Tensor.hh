//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
/// \file Tensor.hh
/// this handles multiple operations with tensors. 
#ifndef __PANSCALES_TENSOR_HH__
#define __PANSCALES_TENSOR_HH__
#include <array>
#include <cmath>
#include <iostream>
#include <complex>
#include <vector>

namespace panscales {

// Returns the binary number ...00011...11, where x is the number of 1's
inline size_t n_bits(size_t x) {
  return (1 << x) - 1;
}

// Empty index object for index contraction
struct Index {};

// Tensor of arbitrary precision types with number of indices equal to dim
// Currently, all indices only take values 0 and 1
template <size_t dim>
struct Tensor {

    Tensor() {
      for (size_t i=0; i<dim; i++) {
        _indices[i] = nullptr;
      }
    }

    Tensor(precision_type val) {
      for (size_t i=0; i<_size; i++) {
        _data[i] = val;
      }
    }

    // Accessors for the data
    PSCOMPLEX& operator()(std::array<size_t, dim> &arr) {
      assert(arr[0] < 2);
      size_t ind = arr[0];
      for (size_t i=1; i < arr.size(); i++) {
        assert(arr[i] < 2);
        ind = ind*2 + arr[i];
      }
      return _data.at(ind);
    }
    // First argument is explicitly size_t to distinguish from the index setter
    // The parameter pack is unpacked into an array
    template<typename... indices>
    PSCOMPLEX& operator()(size_t first_ind, indices... inds) {    
      assert(sizeof...(inds) == dim-1);
      std::array<size_t, dim> arr = {{first_ind, (size_t)inds... } };
      return (*this)(arr);
    }

    // Setters for the indices
    Tensor<dim>& operator()(std::array<Index*, dim> &arr) {
      _indices = arr;
      return *this;
    }
    // First argument is explicitly Index* to distinguish from the accessor
    // The && notation implies reference forwarding
    // The parameter pack is unpacked into an array
    template<typename... indices>
    Tensor<dim>& operator()(Index &first_ind, indices&&... inds) {
      _indices = {{&first_ind, &inds...}};
      return *this;
    }

    // Add tensors
    Tensor<dim>& operator+=(Tensor<dim> &o) {
      for (size_t i=0; i<_size; i++) {
        _data[i] += o._data[i];
      }      
      return *this;
    }

    Tensor<dim> operator+(Tensor<dim> &o) const {
      auto out = (*this);
      out += o;
      return out;
    }

    // Subtract tensors
    Tensor<dim>& operator-=(Tensor<dim> &o) {
      for (size_t i=0; i<_size; i++) {
        _data[i] -= o._data[i];
      }      
      return *this;
    }

    Tensor<dim> operator-(Tensor<dim> &o) const {
      auto out = (*this);
      out -= o;
      return out;
    }

    // Multiply with a complex constant
    Tensor<dim>& operator*=(PSCOMPLEX &o) {
      for (size_t i=0; i<_size; i++) {
        _data[i] *= o;
      }
      return *this;
    }

    Tensor<dim> operator*(PSCOMPLEX &o) const {
      auto out = (*this);
      out *= o;
      return out;
    }

    // Multiply with a constant
    Tensor<dim>& operator*=(precision_type &o) {
      for (size_t i=0; i<_size; i++) {
        _data[i] *= o;
      }
      return *this;
    }

    Tensor<dim> operator*(precision_type &o) const {
      auto out = (*this);
      out *= o;
      return out;
    }

    // Divide by a constant
    Tensor<dim>& operator/=(PSCOMPLEX &o) {
      for (size_t i=0; i<_size; i++) {
        _data[i] /= o;
      }
      return *this;
    }

    Tensor<dim> operator/(PSCOMPLEX &o) const {
      auto out = (*this);
      out /= o;
      return out;
    }

    void fill() {
      for (size_t i=0; i<_size; i++) {
        _data[i] = i;
      }
    }

    void print(std::ostream &ostr = std::cout) const {
      ostr << "Tensor<" << dim << ">[";
      for (size_t i=0; i<_size; i++) {
        ostr << _data[i];
        if (i != _size-1) ostr << ",";
      }
      ostr << "]";
    }

    // Compute a trace if dim=2
    PSCOMPLEX tr() const {
      assert(dim == 2);
      return _data[0] + _data[3];
    }

    // Normalize by trace if dim=2
    Tensor<dim>& normalize() {
      assert(dim == 2);
      PSCOMPLEX tr = (*this).tr();
      (*this) /= tr;
      return *this;
    }

    // Complex conjugate
    Tensor<dim> conj() const {
      Tensor<dim> out;
      for (size_t i=0; i<_data.size(); i++) {
        out._data[i] = std::conj(_data[i]);
      }
      return out;
    }

    // Return the scalar in the case dim=0
    PSCOMPLEX& scalar() {
      assert(dim == 0);
      return _data[0];
    }

    // Single index contraction
    Tensor<dim-2> contract_single_index(size_t i_left, size_t i_right) {
      assert(dim > 2);

      // Let's say that, in a tensor A, we're doing the contraction A(a,x,b,c,x,d)
      // where x are the repeated indices and {a,b,c,d,x} \in {0,1}
      // We want to find the new tensor B(a,b,c,d) = A(a,0,b,c,0,d) + A(a,1,b,c,1,d)  
      // We do this by interpreting the new set of indices as a binary number (abcd)
      // The masks project to the numbers (000d), (0bc0), (a000)
      // We then bit shift and add to get to the numbers (a0bc0d) and (a1bc1d)
      // The procedure is super efficient because the masks only need to be computed once
      Tensor<dim-2> out;
      size_t mask_right  = (1 << (dim-i_right-1))-1;
      size_t mask_middle = (1 << (dim-i_left-2)) - mask_right - 1;
      size_t mask_left   = (1 << (dim-2)) - mask_right - mask_middle - 1;
      size_t index_diff  = (1 << (dim-i_right-1)) + (1 << (dim-i_left-1));

      for (size_t j=0; j<out._size; j++) {
        size_t j0 = (j & mask_right) + ((j & mask_middle) << 1) + ((j & mask_left) << 2);
        size_t j1 = j0 + index_diff;

        out._data[j] = _data[j0] + _data[j1];
      }

      return out;
    }

    // Single index contraction with index pointers
    Tensor<dim-2> contract_single_index() {
      // Need at least 3 dimensions
      assert(dim > 2);

      Tensor<dim-2> out;
      for (size_t i_right=0; i_right<dim; i_right++) {
        for (size_t i_left=0; i_left<i_right; i_left++) {
          if (_indices[i_left] == _indices[i_right]) {
            // Found the contracted indices
            out = contract_single_index(i_left, i_right);

            // Fix outgoing indicides
            size_t j0 = 0;
            for (size_t j=0; j<_indices.size(); j++) {
              if (_indices[j] != _indices[i_left] && _indices[j] != _indices[i_right]) {
                out._indices[j0] = _indices[j];
                j0++;
              }
            }

            return out;
          }
        }
      }

      // To prevent warnings
      assert(false && "Didn't find contracted indices");
      return out;
    }
    
    // The number of precision_types stored in the Tensor = 2^dim
    static constexpr size_t _size = 1 << dim;

    // Index storage
    std::array<Index*, dim> _indices;

    // Data structure
    std::array<PSCOMPLEX, Tensor<dim>::_size> _data;
};

// ---------------------------------------------------------------------------------------------
// ---------------------- Methods for contractions of indices in products ----------------------
// ---------------------------------------------------------------------------------------------

// Contract n indices in a product of tensors
// Currently assumes the contracted indices follow the same order on both sides
template<size_t n, size_t dim_left, size_t dim_right>
Tensor<dim_left+dim_right-2*n> product_contract_n_index(Tensor<dim_left> &left, Tensor<dim_right> &right) {
  Tensor<dim_left+dim_right-2*n> out;

  // Example: (a,b,x,y,c,z,d) x (x,y,e,f,g,z);

  // Figure out which indices we are contracting over
  // NOTE: This assumes the indices are ordered 
  // Example: i_left = (2,3,5), i_right = (0,1,5)
  std::array<size_t, n> i_left, i_right;
  size_t i_count = 0;
  for (size_t k_left=0; k_left<dim_left; k_left++) {
    for (size_t k_right=0; k_right<dim_right; k_right++) {
      if (left._indices[k_left] == right._indices[k_right]) {
        i_left[i_count]  = k_left;
        i_right[i_count] = k_right;
        i_count++;
      }
    }
  }
  assert(i_count == n);

  // Masks to split the number into the left and right sides
  // Example: (a,b,c,d,e,f,g) to (a,b,c,d) and (e,f,g)
  size_t mask_right = n_bits(dim_right - n);
  size_t mask_left = n_bits(dim_left + dim_right - 2*n) - mask_right;

  // Figure out the insertion positions starting from the right-hand side
  // Example: We want to loop over all tensors (a,b,c,d) x (e,f,g) and find the 
  // insertion positions of the contracted indices x, y, z
  // For instance, to get to (a,b,x,y,c,z,d) one needs to insert 
  // at positions (1,2,2)
  
  std::array<size_t, 2*n> ins_pos_left, ins_pos_right;
  for (size_t k=0; k<n; k++) {
    ins_pos_left[k] = dim_left  - i_left[n-k-1]  - k - 1;
    ins_pos_right[k] = dim_right - i_right[n-k-1] - k - 1;
  }

  // Construct masks to split tensors
  // Example: Split the tensor (a,b,c,d) into (a,b,0,0), (0,0,c,0), (0,0,0,d)
  std::array<size_t, n+1> split_masks_left, split_masks_right;
  split_masks_left[0]  = n_bits(ins_pos_left[0]);
  split_masks_right[0] = n_bits(ins_pos_right[0]);
  for (size_t k=1; k<n; k++) {
    split_masks_left[k]  = n_bits(ins_pos_left[k])  - n_bits(ins_pos_left[k-1]);
    split_masks_right[k] = n_bits(ins_pos_right[k]) - n_bits(ins_pos_right[k-1]);
  }
  split_masks_left[n]  = n_bits(dim_left - n)  - n_bits(ins_pos_left[n-1]);
  split_masks_right[n] = n_bits(dim_right - n) - n_bits(ins_pos_right[n-1]);

  // Incrementors to go from a 0 to a 1
  // Example: After masking, we will end up with the tensor 
  // (a,b,0,0,c,0,d). We need the numbers 
  // (0,0,0,0,0,1,0)
  // (0,0,0,1,0,0,0)
  // (0,0,0,1,0,1,0) etc to sum over the contracted indices
  std::array<size_t, (1 << n)> incrementors_left = {}, incrementors_right = {};
  for (size_t k=0; k < (1 << n); k++) {
    size_t temp = k;
    for (size_t j=0; j<n; j++) {
      if (temp % 2 == 1) {
        incrementors_left[k]  += 1 << (dim_left  - i_left[j]  - 1);
        incrementors_right[k] += 1 << (dim_right - i_right[j] - 1);
      }
      temp = temp >> 1;
    }
  }

  for (size_t i=0; i<out._size; i++) {
    // Split the index into the components of the lh and rh
    // Example: (a,b,c,d,e,f,g) into (a,b,c,d) and (e,f,g)
    size_t base_left_pre_expand  = (mask_left & i) >> (dim_right - n); 
    size_t base_right_pre_expand = mask_right & i;

    // Expand the bases by inserting zeroes at the positions of contracted indices
    // Example (a,b,c,d) into (a,b,0,0,c,0,d)
    size_t base_left = 0, base_right = 0;
    for (size_t k=0; k<n+1; k++) {
      base_left  += (split_masks_left[k]  & base_left_pre_expand)  << k;
      base_right += (split_masks_right[k] & base_right_pre_expand) << k;
    }

    // Sum over the contracted indices
    out._data[i] = 0;
    for (size_t k=0; k<(1 << n); k++) {
      out._data[i] += left._data[base_left + incrementors_left[k]]*right._data[base_right + incrementors_right[k]];
    }
  }

  // Fix the outgoing indices
  size_t k_out = 0, k_left = 0, k_right = 0;
  for (size_t k=0; k<dim_left; k++) {
    if ((k_left<n) && (k == i_left[k_left])) {
      k_left++;
    } else {
      out._indices[k_out] = left._indices[k];
      k_out++;
    }
  }

  for (size_t k=0; k<dim_right; k++) {
    if ((k_right<n) && (k == i_right[k_right])) {
      k_right++;
    } else {
      out._indices[k_out] = right._indices[k];
      k_out++;
    }
  }

  return out;
}

// Contract 1 index in a product of tensors
// Specialization of the previous function for efficiency
template<size_t dim_left, size_t dim_right>
Tensor<dim_left+dim_right-2> product_contract_single_index(Tensor<dim_left> &left, Tensor<dim_right> &right) {
  size_t found_matches = 0;
  // #TRK_ISSUE-698  KH modified next 2 lines as agreed with RV on Slack 2021 May 27, 7:42
  size_t i_left  = 0;
  size_t i_right = 0;
  for (size_t j_left=0; j_left<dim_left; j_left++) {
    for (size_t j_right=0; j_right<dim_right; j_right++) {
      if (left._indices[j_left] == right._indices[j_right]) {
        if (found_matches == 0) {
          i_left = j_left;
          i_right = j_right;
        }
        found_matches++;
      }
    }
  }

  assert (found_matches == 1);

  Tensor<dim_left+dim_right-2> out;
  // Let's say that we're doing the contraction A(a,x,b)*B(c,d,x,e)
  // where x are the repeated indices and {a,b,c,d,e,x} \in {0,1}
  // We want to find the new tensor 
  // C(a,b,c,d,e) = A(a,0,b)*B(c,d,0,e) + A(a,1,b)*B(c,d,1,e)

  // First set of masks facilitate the reduction of (a,b,c,d,e) 
  // to the two numbers (a,b) and (c,d,e)
  size_t mask_right = (1 << (dim_right - 1)) - 1;
  size_t mask_left  = (1 << (dim_left + dim_right - 2)) - 1 - mask_right;

  // Second set of masks facilitate the steps 
  // (a,b) -> (a,0,b) and (c,d,e) -> (c,d,0,e)
  size_t mask_left_right  = (1 << (dim_left  - i_left  - 1)) - 1;
  size_t mask_left_left   = (1 << (dim_left  - 1)) - 1 - mask_left_right;
  size_t mask_right_right = (1 << (dim_right - i_right - 1)) - 1;
  size_t mask_right_left  = (1 << (dim_right - 1)) - 1 - mask_right_right; 

  // The numbers to add to go from (a,0,b) -> (a,1,b)
  // and (c,d,0,e) -> (c,d,1,e)
  size_t diff_left  = 1 << (dim_left - i_left - 1);
  size_t diff_right = 1 << (dim_right - i_right - 1);

  for (size_t j=0; j<out._size; j++) {
    // These are the numbers (a,b) and (c,d,e)
    size_t split_left = (j & mask_left) >> (dim_right - 1);
    size_t split_right = j & mask_right;
    // These are the numbers (a,0,b) and (c,d,0,e)
    size_t j_left  = (split_left  & mask_left_right)  + ((split_left  & mask_left_left)  << 1);
    size_t j_right = (split_right & mask_right_right) + ((split_right & mask_right_left) << 1);

    // Do the actual contraction
    out._data[j] = left._data[j_left]*right._data[j_right] + left._data[j_left+diff_left]*right._data[j_right+diff_right];
  }

  // Fix the outgoing indices
  size_t j0 = 0;
  for (size_t j=0; j<dim_left; j++) {
    if (j != i_left) {
      out._indices[j0] = left._indices[j];
      j0++;
    }
  }
  for (size_t j=0; j<dim_right; j++) {
    if (j != i_right) {
      out._indices[j0] = right._indices[j];
      j0++;
    }
  }
  
  return out;
}

// Contract 2 indices in a product of tensors
// Specialization of the previous function for efficiency
// Currently assumes the contracted indices follow the same order on both sides
template<size_t dim_left, size_t dim_right>
Tensor<dim_left+dim_right-4> product_contract_double_index(Tensor<dim_left> &left, Tensor<dim_right> &right) {
  size_t found_matches = 0;
  // #TRK_ISSUE-699  KH modified next 4 lines as agreed with RV on Slack 2021 May 27, 7:42
  size_t i_left  = 0;
  size_t i_right = 0;
  size_t j_left  = 0;
  size_t j_right = 0;
  for (long unsigned int k_left=0; k_left<dim_left; k_left++) {
    for (long unsigned int k_right=0; k_right<dim_right; k_right++) {
      if (left._indices[k_left] == right._indices[k_right]) {
        // The first pair of identical indices
        if (found_matches == 0) {
          i_left  = k_left;
          i_right = k_right;
        }
        // The second pair of identical indices
        else {
          j_left  = k_left;
          j_right = k_right;
        }
        found_matches++;
      }
    }
  }

  assert(found_matches == 2);

  // Make sure the indices are ordered
  assert(i_left < j_left && i_right < j_right);

  Tensor<dim_left+dim_right-4> out;

  // Let's say that we're doing the contraction A(x,y,a,b)*B(c,x,d,y)
  // where x,y are the repeated indices and {a,b,c,d,x,y} \in {0,1}
  // We want to find the new tensor 
  // C(a,b,c,d) = A(0,0,a,b)*B(c,0,d,0) + A(1,0,a,b)*B(c,1,d,0) + A(0,1,a,b)*B(c,0,d,1) + A(1,1,a,b)*B(c,1,d,1) 

  // First set of masks facilitate the reduction of (a,b,c,d) 
  // to the two numbers (a,b) and (c,d)
  size_t mask_right = (1 << (dim_right - 2)) - 1;
  size_t mask_left  = (1 << (dim_left + dim_right - 4)) - 1 - mask_right;

  // Second set of masks facilitate the steps 
  // (a,b) -> (0,0,a,b) and (c,d) -> (c,0,d,0)
  size_t mask_left_right   = (1 << (dim_left-j_left-1))-1;
  size_t mask_left_middle  = (1 << (dim_left-i_left-2)) - mask_left_right - 1;
  size_t mask_left_left    = (1 << (dim_left-2)) - mask_left_right - mask_left_middle - 1;

  size_t mask_right_right  = (1 << (dim_right-j_right-1))-1;
  size_t mask_right_middle = (1 << (dim_right-i_right-2)) - mask_right_right - 1;
  size_t mask_right_left   = (1 << (dim_right-2)) - mask_right_right - mask_right_middle - 1;

  // The numbers to add to go 
  // (0,0,a,b) -> (1,0,a,b) or (0,1,a,b)
  // (c,0,d,0) -> (c,1,d,0) or (c,0,d,1)
  size_t diff_left_i  = 1 << (dim_left - i_left - 1);
  size_t diff_left_j  = 1 << (dim_left - j_left - 1);
  size_t diff_right_i = 1 << (dim_right - i_right - 1);
  size_t diff_right_j = 1 << (dim_right - j_right - 1);

  for (size_t k=0; k<out._size; k++) {
    // These are the numbers (a,b) and (c,d)
    size_t split_left = (k & mask_left) >> (dim_right - 2);
    size_t split_right = k & mask_right;

    // These are the numbers (0,0,a,b) and (c,0,d,0)
    size_t k_left  = (split_left  & mask_left_right)  + ((split_left  & mask_left_middle)  << 1) + ((split_left  & mask_left_left)  << 2);
    size_t k_right = (split_right & mask_right_right) + ((split_right & mask_right_middle) << 1) + ((split_right & mask_right_left) << 2);

    // Do the actual contraction
    out._data[k] =  left._data[k_left]                         * right._data[k_right]              + 
                    left._data[k_left+diff_left_i]             * right._data[k_right+diff_right_i] +
                    left._data[k_left+diff_left_j]             * right._data[k_right+diff_right_j] + 
                    left._data[k_left+diff_left_i+diff_left_j] * right._data[k_right+diff_right_i+diff_right_j];

  }

  // Fix the outgoing indices
  size_t k0 = 0;
  for (size_t k=0; k<dim_left; k++) {
    if (k != i_left && k != j_left) {
      out._indices[k0] = left._indices[k];
      k0++;
    }
  }
  for (size_t k=0; k<dim_right; k++) {
    if (k != i_right && k != j_right) {
      out._indices[k0] = right._indices[k];
      k0++;
    }
  }

  return out;
}

template <size_t dim>
inline std::ostream & operator<<(std::ostream & ostr, const Tensor<dim> & t) {
  t.print(ostr);
  return ostr;
}

} // end namespace panscales


#endif
