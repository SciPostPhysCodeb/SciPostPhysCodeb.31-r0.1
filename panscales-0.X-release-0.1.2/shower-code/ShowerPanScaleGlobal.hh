//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __SHOWERPANSCALEGLOBAL_HH__
#define __SHOWERPANSCALEGLOBAL_HH__

#include "ShowerPanScaleBase.hh"
#include <math.h>
#include <array>
#include <cassert>
#include <limits>
#include <sstream>
#include "LimitedWarning.hh"
#include "ColourTransitionRunner.hh"
#include "DKCMWInterpolation.hh"
#include "config.hh"

namespace panscales{

/// \enum PanGlobalDeltaKRegularisationOption
/// Various prescriptions to regularise 1+x with x=as/(2pi) deltaKCMW
enum PanGlobalDeltaKRegularisationOption{
  Linear        = 0,   ///< max(min(1+x,2),0)             [overhead = 2]
  Tanh          = 1,   ///< 1+tanh(x)                     [overhead = 2]
  Tanh3         = 2,   ///< 1+a tanh(x/a) (a=x<0 ? 1 : 3) [overhead = 4]
  PiecewiseTanh = 3,   ///< piecewise tanh                [overhead = 4] [a=-0.5,b=2,m=3] (x>-0.5) ? ((x<2) ? 1+x : tanh(x-2)+3) : 0.5*(tanh(2*x+1)+1)
}; 

  
//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerPanScaleGlobal
/// the PanScales shower w global recoil for e+e- collisions 
/// introduced in arXiv:2002.11114. Note that, as discussedd in arXiv:2307.11142 
/// the original recoil strategy has been deprecated. We instead consider three
/// alternative rescaling options:
///
///   - DoubleRescaling: 1) dipole-local rescaling imposing 
///         r_L Q.(pi+pj+pk) = Q.(pitilde+pjtilde)
///     and 2) global rescaling to restore the event invariant mass.
///          r_G =  sqrt(Q^2/Qbar^2)
///   - LocalRescaling: dipole-local rescaling so as to restore
///     the invariant mass. [default]
///   - LocalijRescaling: rescaling of emitter and spectator so
///     as to restore the invariant mass. 
///
/// All three options satisfy the PanScales NLL criteria. 

class ShowerPanScaleGlobal : public ShowerPanScaleBase {
public:
  /// default ctor
  ShowerPanScaleGlobal(const QCDinstance & qcd, double beta = 0.0,
		       bool additive_branching = true, double additive_suppression_power = 0.0,
                       double boost_rescaling_cutoff = 0.0,
                       PanGlobalRescaleOption rescaling_option=PanGlobalRescaleOption::GlobalRescalingDeprecated,
                       bool split_dipole_frame=false)
    : ShowerPanScaleBase(qcd, beta),
      _additive_branching(additive_branching),
      _additive_suppression_power(additive_suppression_power),
      _boost_rescaling_cutoff(boost_rescaling_cutoff),
      _rescaling_option(rescaling_option),
      _split_dipole_frame(split_dipole_frame),
      _deltaK_regularisation(PanGlobalDeltaKRegularisationOption::Tanh){
        
    _update_event_changes_dot_products_emit_spec_rad =
      (_rescaling_option == PanGlobalRescaleOption::LocalRescaling)
      ? false : true;
    // with multiplicative branching, we currently assume that all
    // structure functions go to CA/z in the soft limit.
    // Print a warning if 2CA is different from CF
    if ((!_additive_branching) &&
        (std::abs(qcd.CA() - 2*qcd.CF())>numeric_limit_extras<precision_type>::sqrt_epsilon())){
      _PanGlobalMultiplicativeCAnot2CF.warn("PanGlobal: the current implementation of the multiplicative shower assumes CA=2CF which is not respected in practice");
    }

    // disable double-soft by default
    // this involves virtual methods so should be called here again
    enable_double_soft(false);
  }
  
  /// virtual dtor
  virtual ~ShowerPanScaleGlobal() {}

  /// implements the element as a subclass (EmissionInfo is common to
  /// all PanScales showers and defined in the ShowerPanScaleBase base
  /// class)
  class Element;
  class EmissionInfo;
  virtual ShowerBase::EmissionInfo* create_emission_info() const override;
  
  /// description of the class
  std::string description() const override {
    std::ostringstream ostr;
    ostr << name() << " shower with beta = " << _beta;
    ostr << ", boost/rescaling cutoff = " << _boost_rescaling_cutoff;
    if (_additive_branching) {
      ostr << ", sum of splitting functions";
      ostr << " with suppression power p = " << _additive_suppression_power;
    } else {
      ostr << ", product of splitting functions";
    } 
    if (_split_dipole_frame)
      ostr << ", with splitter decided in the dipole frame";
    ostr << ", rescaling according to scheme: " << _rescaling_option;
    return ostr.str();
  }
  
  /// name of the shower
  std::string name() const override{return "PanScaleGlobal-ee";}

  /// returns true if the shower is global 
  bool is_global() const override { return true; }
  
  /// this is a dipole shower (1 element per dipole)
  virtual unsigned int n_elements_per_dipole() const override{ return 1;}

  /// do_kinematics always returns true
  virtual bool do_kinematics_always_true_if_accept() const override {return true;} 

  /// returns true if an emission happens at a phase-space point that has
  /// already been covered by the matching shower (used in "truncated" showers)
  virtual bool veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const override;
  
  /// element store
  virtual std::vector<std::unique_ptr<ShowerBase::Element> > elements(Event & event, int dipole_index) const override;

  /// this is an antenna-like shower, so both the emitter and spectator
  /// can split
  bool only_emitter_splits() const override {return false;}

  /// shower-specific double-soft initialisation
  /// This is only called by "enable_double_soft" in ShowerBase
  virtual void init_double_soft() override {
    // no need for an overhead if double-soft corrections are disabled
    if (! double_soft()){
      _double_soft_overhead = 1.0;
      _dKCMW_overhead       = 1.0;
      _dKCMW_error_offset_factor = 0.0;
      return;
    }
    
    // the overhead factor is beta-dependent (tested for 0 and 1/2)
    assert(((abs(beta())<1e-6) || (abs(beta()-0.5)<1e-6)) && "only beta=0 and 1/2 currently supported for double-soft corrections");

    if(beta() > 0.0){
      //_double_soft_overhead = 20.0;
      update_double_soft_overhead(0.0);
    } else {
      _double_soft_overhead = (_additive_branching && (!_split_dipole_frame)) ? 3.1 : 4.0;
    }

    if (double_soft_dKCMW()){
      if(beta() > 0.0){
        _dKCMW_interp = DeltaKCMWInterpolationTable(std::string(PANSCALES_SOURCE_DIR)+"/dKCMW-tables/pg05-CF.res", 
						    std::string(PANSCALES_SOURCE_DIR)+"/dKCMW-tables/pg05-CA.res", 
						    std::string(PANSCALES_SOURCE_DIR)+"/dKCMW-tables/pg05-TR.res", 
						    _dKCMW_error_offset_factor, _dKCMW_subtract_fit);
      } else {
        _dKCMW_interp = DeltaKCMWInterpolationTable(std::string(PANSCALES_SOURCE_DIR)+"/dKCMW-tables/pg00-CF.res", 
						    std::string(PANSCALES_SOURCE_DIR)+"/dKCMW-tables/pg00-CA.res", 
						    std::string(PANSCALES_SOURCE_DIR)+"/dKCMW-tables/pg00-TR.res", 
						    _dKCMW_error_offset_factor, _dKCMW_subtract_fit);
        //_dKCMW_interp = DeltaKCMWInterpolationTable(std::string(PANSCALES_SOURCE_DIR)+"/../analyses/double-soft-second-order/gregory-tests/res/acceptance-integral/pg00-all.res", _dKCMW_error_offset_factor, _dKCMW_subtract_fit);
      }
      if ((_deltaK_regularisation==panscales::PanGlobalDeltaKRegularisationOption::Tanh) || 
          (_deltaK_regularisation==panscales::PanGlobalDeltaKRegularisationOption::Linear)){
        _dKCMW_overhead = 2.0;
      } else {
        _dKCMW_overhead = 4.0;
      }        
    } else {
      _dKCMW_overhead = 1.0;
    }

  }

  void set_deltaK_regularisation(PanGlobalDeltaKRegularisationOption regul) {_deltaK_regularisation = regul;}

  PanGlobalDeltaKRegularisationOption deltaK_regularisation() {return _deltaK_regularisation;}
  
  /// returns the overhead factor associated with double-soft
  /// corrections
  double double_soft_overhead() const {return _double_soft_overhead;}
  
  double dKCMW_overhead() const {return _dKCMW_overhead;}
  
  virtual void update_double_soft_overhead(double lnv_minus_lnvlast) override{
    if (! double_soft()) return;
    if (beta() == 0.0)   return;
    _double_soft_overhead = 20.0; return;
    // if (lnv_minus_lnvlast>-0.7){
    //   _double_soft_overhead = 22.0;
    // } else {
    //   _double_soft_overhead = 3.0 + 19.0/pow2(pow2(0.3-lnv_minus_lnvlast));
    // }
      
    // if (lnv_minus_lnvlast<-4){
    //   _double_soft_overhead = 1.25;
    // } else if  (lnv_minus_lnvlast<-1){
    //   double delta2 = pow2(lnv_minus_lnvlast);
    //   double delta4 = pow2(pow2(lnv_minus_lnvlast+0.2));
    //   _double_soft_overhead = 1.2+4/(delta2+delta4);
    // } else if (lnv_minus_lnvlast<-0.6) {
    //   double delta3 = -pow3(lnv_minus_lnvlast);
    //   _double_soft_overhead = 1/delta3 * (2.8 - 1.2/lnv_minus_lnvlast);
    // } else {
    //   _double_soft_overhead = 4.8/0.216;
    // }
  }

  /// double-soft-related KCMW correction (applied as an acceptance
  /// probability, potentially w an overhead factor to be handled by
  /// individual showers)
  virtual double delta_K_CMW_acceptance(typename ShowerBase::EmissionInfo * emission_info_base) const override;

  /// given kinematics from an event, returns the lnv, lnb values
  /// that correspond to (ĩ,j̃) -> (i,j,k)
  virtual bool find_lnv_lnb_from_kinematics(const Momentum &p_emit, 
                                     const Momentum &p_spec, 
                                     const Momentum &p_rad, 
                                     const precision_type &p_emit_dot_p_spec,
                                     const precision_type &p_emit_dot_p_rad,
                                     const precision_type &p_spec_dot_p_rad,
                                     const Event &event,
                                     double &lnv, double &lnb) const override;
  /// access to the rescaling option
  PanGlobalRescaleOption rescaling_option() const {return _rescaling_option;}

  /// if true - we know that the update event changes dot products because of a rescaling factor
  bool update_event_changes_dot_products_emit_spec_rad() const {return _update_event_changes_dot_products_emit_spec_rad;}

protected:
  static LimitedWarning _PanGlobalNegativeQbar, _PanGlobalMultiplicativeCAnot2CF;

private:
  bool _additive_branching;           ///< true:  additive branching (default)
                                      ///< false: mutliplicative branching
  double _additive_suppression_power; ///< extra parameter to tentatively reduce non-Sudakov routes to 0 (inactive)

  double _boost_rescaling_cutoff;     ///< If the boost |beta| is beneath this value the boost will
                                      ///  be skipped on the grounds that it's null from a numerical
                                      ///  point of view. Similarly if |r-1| is less than this value
                                      ///  the momentum recoil rescaling factor is not applied, on
                                      ///  the same basis.
  
  bool _update_event_changes_dot_products_emit_spec_rad; ///< depending on the rescaling option the 
                                                         ///  update_event will change dot products

  PanGlobalRescaleOption _rescaling_option;
  bool _split_dipole_frame; ///< together with multiplicative branching, 
                            ///  set the transition between the emitter 
                            ///  and spectator in the dipole frame

  double _double_soft_overhead = 1.0; ///< Overhead factor needed for double soft acceptance prob
  double _dKCMW_overhead = 1.0;       ///< Overhead factor needed for deltaKCMW with  double soft enabled
  DeltaKCMWInterpolationTable _dKCMW_interp;
  PanGlobalDeltaKRegularisationOption _deltaK_regularisation;
};


//--------------------------------------------------------------
/// \class ShowerPanScaleGlobal::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs for the PanScaleGlobal shower w global recoil
class ShowerPanScaleGlobal::Element : public ShowerPanScaleBase::Element {

public:
  /// dummy ctor
  Element() {}

  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleGlobal * shower,
          bool additive_branching = true) :
    ShowerPanScaleBase::Element(emitter_index, spectator_index, dipole_index, event, shower),  _shower(shower) {}

  /// given that we have virtual functions in a number of places,
  /// we need a virtual destructor to avoid compiler warnings
  virtual ~Element() {}

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------
  /// Approximate calculation of the log of the kT of the emission relative
  /// to its parents in their rest frame, (eq. 57 of logbook 2019-07-02).
  const double alphas_lnkt(double lnv, double lnb) const override {
    precision_type lnkt = log_T(_rho) + lnv + _shower->_beta*fabs(lnb);
    return to_double(lnkt);
  }

  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  /// the lnb total extent, as a function of lnv, is parametrised
  /// as  const + lnv_deriv * lnv.
  ///
  double lnb_extent_const()     const override {
    return to_double(log_T(_dipole_m2) - 2.*log_T(_rho))/(1. + _shower->_beta);
  }
  double lnb_extent_lnv_coeff() const override {return -2.0/(1. + _shower->_beta);}
  
  Range lnb_generation_range(double lnv) const override;
  
  bool has_exact_lnb_range() const override {return true;}
  /// jacobian for this shower is not computed
  precision_type dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const override;
  /// update the kinematics if needed
  void get_emit_spec_rad_kinematics_for_matching(typename ShowerBase::EmissionInfo * emission_info, Momentum & emit, Momentum & spec, Momentum & rad, bool include_boost = false) const override;

  /// returns the exact range of lnb that is allowed; returns a a
  /// zero-extent range in cases where there in no range of lnb
  /// allowed. (Note that the zero extent range may take various
  /// forms, e.g. two identical points, or inverted points)
  Range lnb_exact_range(double lnv) const override;

  double lnv_lnb_max_density() const override;
  
  //--------------------------------------------------
  // important definitions to be implemented in all derived classed
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const override;

  /// carry out the splitting and update the event; return true if
  /// successful; the RotatedPieces pass on a potentially rotated version
  /// of the emitter, spectator, and perpendicular vectors, and these
  /// are the ones that should be used, not those from emission_info; 
  /// the output is stored back in emission_info (possibly still rotated)
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const override;
  
  /// Overridden update event function to apply boost and rescaling
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;
  
  /// Acceptance probability associated with correct double soft behaviour of the shower
  virtual double double_soft_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  //----------------------------------------------------
  // caching info to facilitate the work of ShowerRunner
  //----------------------------------------------------
  // all defined in ShowerPanScaleBase

  //--------------------------------------------------------
  // Things useful for observable-dependent dynamic cut-offs 
  //--------------------------------------------------------
  // all defined in ShowerPanScaleBase

  //--------------------------------------------------
  // Handling of double-soft corrections
  //
  // Note: for now, only PanGlobal has double-soft support, so most of
  // the code is implemented here. Once we have a cleaner view of what
  // needs to be done for other showers, we'll probably have to move
  // some of the code below to ShowerBase
  //--------------------------------------------------

  /// structure that holds double-soft information
  ///
  /// This is structured so that:
  /// - p_new       is the new emission
  /// - p_first     is the previous emission at commensurate angle and kt
  /// - p_aux_new   is the auxiliary momentum colour-connected with p_new 
  /// - p_aux_first is the auxiliary momentum colour-connected with p_first
  /// where the colour connections are as provided by the shower
  /// (i.e. before any potential swap)
  ///
  /// For gluons, we have
  /// 
  ///                  first    
  ///                   ||   
  ///                   ||      new
  ///                   ||     //    
  ///  aux_first  -----/  \---------- aux_new
  ///
  /// so that (aux_new, first) is the splitting dipole 
  ///
  /// For g->qqbar, we have
  ///
  ///              first   new
  ///                  \  /    
  ///                   || 
  ///                   || 
  ///  aux_first  -----/  \---------- aux_new
  ///
  /// so that (aux_first, first) is the splitting dipole 
  ///
  ///
  ///GS-NOTE-DS: Below a series of thoughs:
  /// - Can we use pointers to the momenta?
  /// - Do we want to put the methods in here as well?
  ///    double_soft_weight_shower
  ///    double_soft_find_pi_pj_pk_pl
  ///    double_soft_swap_colour_connections
  ///    double_soft_swap_flavours_prob
  ///   (at the very least, they should be private in the shower class)
  ///   Conceptually, we'd need 2 classes: one w the info and one
  ///   doing the work. Hence, leave the functions in the shower for now
  /// - swap_flavour || flavour_swap?
  class DoubleSoftInfo{
  public:
    // momenta of the 4 particles involved
    Momentum p_new;       ///< new emission
    Momentum p_first;     ///< partner soft emission (emitted first)
    Momentum p_aux_new;   ///< auxiliary colour-connected w p_new
    Momentum p_aux_first; ///< auxiliary colour-connected w p_first

    // flavour information
    int flav_init;   ///< flavour as assigned initially by the shower

    // cached distances
    // dcross = dna d1b - d1a dnb
    precision_type dn1, dna, dnb, d1a, d1b, dab, dcross;
    
    // weights generated by the shower
    //
    // these weights sum over all the histories yieldins the same
    // kinematic configuration, i.e. over the 2 options for the
    // particle which is emitted first.
    //
    //GS-NOTE-DS: in principles we do not need to cache the "a1nb"
    //versions as only the "an1b" are used in the probabilities. Also,
    //we may want to use the fact that the an1b and a1nb exact qq
    //are the an1b.
    //
    // We also cache partial sums
    precision_type shower_weight_gg_an1b; ///< g->gg:    an1b colour flow as the shower
    precision_type shower_weight_gg_a1nb; ///< g->gg:    opposite colour flow as the shower
    precision_type shower_weight_qq_an1b; ///< g->qqbar: an1b colour flow as the shower
    precision_type shower_weight_qq_a1nb; ///< g->qqbar: opposite colour flow as the shower

    precision_type shower_weight_gg;   ///< g->gg
    precision_type shower_weight_qq;   ///< g->qqbar
    precision_type shower_weight;      ///< g->gg + g->qqbar
    
    // exact weights in the various channels
    precision_type exact_weight_gg_an1b; ///< g->gg:    an1b colour flow as the shower
    precision_type exact_weight_gg_a1nb; ///< g->gg:    opposite colour flow as the shower
    precision_type exact_weight_qq_an1b; ///< g->qqbar: an1b colour flow as the shower
    precision_type exact_weight_qq_a1nb; ///< g->qqbar: opposite colour flow as the shower

    precision_type exact_weight_gg;   ///< g->gg
    precision_type exact_weight_qq;   ///< g->qqbar
    precision_type exact_weight;      ///< g->gg + g->qqbar
  };


  double double_soft_overhead() const {return _shower->double_soft_overhead();}
  double dKCMW_overhead() const {return _shower->dKCMW_overhead();}

private:

  Momentum3<precision_type> _lorentzBoost(const Momentum& p,
                                          const Momentum& Qp) const;

  //----------------------------------------------------------------------
  // material to handle double-soft corrections
  //----------------------------------------------------------------------

  /// fill the 4 momenta associated w the double-soft correction
  ///
  /// mainly, this function decides at which end of the splitting
  /// dipole we have to take the partner gluon and the 2 auxiliaries
  ///
  /// WATCH OUT: If needed, this method overwrites
  /// emission_info->do_split_emitter so that the splitter points to
  /// p_first
  ///
  /// @return false if this element should not be associated 
  ///         with a double soft correction (in particular because 
  ///         it is the Born hard dipole), otherwise true
  virtual bool _double_soft_fill_momenta(typename ShowerBase::EmissionInfo * emission_info,
                                         DoubleSoftInfo * double_soft_info) const;

  /// analytic double soft approximation (both shower and exact weights)
  virtual bool _double_soft_fill_weights(DoubleSoftInfo * double_soft_info) const;
  
  /// analytic double soft approximation for this shower.
  ///   dij = 2*1-cos(theta_ij)
  ///   diQ = 2*pi.Q/(Ei EQ)
  ///   diQ = Q^2/EQ^2
  ///    z2 = E2/(E1+E2)
  virtual void _double_soft_set_weight_shower(precision_type &d12, precision_type &d1a,
                                              precision_type &d1b, precision_type &d2a,
                                              precision_type &d2b, precision_type &dab,
                                              precision_type &d1Q, precision_type &d2Q,
                                              precision_type &daQ, precision_type &dbQ,
                                              precision_type &dQQ,
                                              precision_type &z1, precision_type &z2, 
                                              precision_type * weight_gg,
                                              precision_type * weight_qq) const;

  /// compute the probability for a flavour swap
  ///
  ///GS-NOTE-DS: we could think of filling em_info wo returning the probability
  virtual double _double_soft_swap_flavours_prob(typename ShowerBase::EmissionInfo * emission_info,
                                                 DoubleSoftInfo * double_soft_info) const;

  /// compute the probabilities for a colour swap
  /// We have 2 probabilities depending on the flavour of the splitting
  ///
  ///GS-NOTE-DS: we could think of filling em_info wo passing the last 2 arguments
  virtual void _double_soft_swap_colour_connections(typename ShowerBase::EmissionInfo * emission_info,
                                                    DoubleSoftInfo * double_soft_info,
                                                    double &colour_swap_prob_gg, 
                                                    double &colour_swap_prob_qq) const;

  
  const ShowerPanScaleGlobal * _shower;

};

//--------------------------------------------------------------
/// \class ShowerPanScaleGlobal::EmissionInfo
/// emission information specific to the PanGlobal shower
class ShowerPanScaleGlobal::EmissionInfo : public ShowerPanScaleBase::EmissionInfo {
public:

  ShowerBase::EmissionInfo * clone() const override {return new EmissionInfo(*this);}

  Momentum Qbar;
  precision_type r; // rescaling factor
};

} // namespace panscales

#include "autogen/auto_ShowerPanScaleGlobal_global-hh.hh"
#endif // __SHOWERPANSCALEGLOBAL_HH__
