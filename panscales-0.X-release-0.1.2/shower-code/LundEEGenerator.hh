// $Id: LundGenerator.hh 1153 2018-08-22 11:56:35Z frdreyer $
//
// Copyright (c) 2018-, Frederic A. Dreyer, Gavin P. Salam, Gregory Soyez
//
//----------------------------------------------------------------------
// This file is distributed with PanScales but it is 
// part of FastJet contrib.
//
// It is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License, or (at
// your option) any later version.
//
// It is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this code. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

#ifndef __FASTJET_CONTRIB_LUNDEEGENERATOR_HH__
#define __FASTJET_CONTRIB_LUNDEEGENERATOR_HH__

#include "Type.hh"
#include "fjcore_local.hh"
#include "Matrix3FJ.hh"
// for handling things with direction differences
#include "EECambridgeDirDiff.hh"
#include "LimitedWarning.hh"

#include <string>
#include <vector>
#include <utility>
#include <queue>

// TODO:
// - add interface to write declusterings to json files
//   [possibly as a separate header, in order to factorise the json.hh dependence]
// - something for pileup subtraction?
// - do we want to update json.hh to latest? And handle
//   the precision issue more elegantly than the current 
//   hack of editing json.hh
// - what do we do about the fact that json.hh is c++11?

FJCORE_BEGIN_NAMESPACE


PseudoJet cross_product(const PseudoJet & p1, const PseudoJet & p2, bool lightlike = false);


namespace contrib{

LimitedWarning _warning_infinity_in_n1_n2;
LimitedWarning _deprecated_psi;
class LundEEGenerator;



//----------------------------------------------------------------------
/// \class LundEEDeclustering
/// Contains the declustering variables associated with a single node
/// on the LundEE plane
class LundEEDeclustering {
public:

  LundEEDeclustering() {};

  /// return the pair PseudoJet, i.e. sum of the two subjets
  const PseudoJet & pair()  const {return pair_;}
  /// returns the subjet with larger transverse momentum
  const PseudoJet & harder() const {return harder_;}
  /// returns the subjet with smaller transverse momentum
  const PseudoJet & softer() const {return softer_;}


  /// returns pair().m() [cached]
  precision_type m()         const {return m_;}

  /// returns the effective pseudorapidity of the emission [cached]
  double eta()       const {return eta_;}

  /// returns sin(theta) of the branching [cached]
  precision_type sin_theta() const {return sin_theta_;}

  /// returns softer().modp() / (softer().modp() + harder().modp()) [cached]
  precision_type z()         const {return z_;}

  /// returns softer().modp() * sin(theta()) [cached]
  precision_type kt()        const {return kt_;}

  /// returns ln(softer().modp() * sin(theta())) [cached]
  double lnkt()      const {return lnkt_;}

  /// returns z() * Delta() [cached]
  precision_type kappa()     const {return kappa_;}

  /// returns the index of the plane to which this branching belongs
  int iplane() const {return iplane_;}

  /// returns the depth of the plane on which this declustering
  /// occurred. 0 is the primary plane, 1 is the first set of leaves, etc. 
  int depth() const {return depth_;}
  
  /// returns iplane (plane index) of the leaf associated with the
  /// potential further declustering of the softer of the objects in
  /// this splitting
  int leaf_iplane() const {return leaf_iplane_;}

  /// Returns sign_s, indicating the initial parent jet index of this splitting
  int sign_s() const {return sign_s_;}
  
  /// (DEPRECATED)
  /// returns an azimuthal type angle between this declustering plane and the previous one
  /// Note: one should use psibar() instead, since we found that this definition of psi is
  /// not invariant under rotations of the event
  double psi()       const {
    _deprecated_psi.warn("LundEEDeclustering::psi() is deprecated. Use LundEEDeclustering::psibar() instead.");
    return psi_;
  }

  /// update the azimuthal angle (deprecated)
  void set_psi(double psi) {psi_ = psi;}

  /// returns an azimuthal angle psibar associated with this
  /// declustering. The actual value of psibar is arbitrary and IR
  /// unsafe (but see below), while differences in psibar values between
  /// different clusterings are meaningful.
  ///
  /// The absolute value of psibar should in general not be used,  but
  /// it has the following properties. (1) the first (largest-angle) splitting
  /// ("ref") in the positive-z hemisphere defines psibar=0 (if there is
  /// no splitting in that hemisphere then the first splitting in
  /// the negative-z hemisphere does). If the +z jet aligns along the z
  /// axis, then psibar of collinear splitting i in that jet is 
  ///
  ///   psibar_i = phi_i - phi_ref
  ///
  /// For the negative-z jet, the psibar of collinear splitting i is
  ///
  ///   psibar_i = phi_i - pi - phi_ref
  ///
  double psibar()    const {return psibar_;}

  
  /// returns the x,y coordinates that are used in the LundEE-plane plots
  /// of arXiv:1807.04758: ln(1/Delta()), and ln(kt()) respectively
  std::pair<double,double> const lund_coordinates() const {
    return std::pair<double,double>(eta_,lnkt_);
  }

  virtual ~LundEEDeclustering() {}

private:
  int iplane_;
  double psi_, psibar_, lnkt_, eta_;
  precision_type m_, z_, kt_, kappa_, sin_theta_;
  PseudoJet pair_, harder_, softer_;
  int  depth_ = -1, leaf_iplane_ = -1;
  int sign_s_;

protected:

  /// the constructor is protected, because users will not generally be
  /// constructing a LundEEDeclustering element themselves.
  LundEEDeclustering(const PseudoJet& pair,
		     const PseudoJet& j1, const PseudoJet& j2,
		     int iplane = -1, double psi = 0.0, double psibar = 0.0, int depth = -1, int leaf_iplane = -1, int sign_s = 1);

  friend class LundEEGenerator;
  friend class RecursiveLundEEGenerator;

};


/// Default comparison operator for LundEEDeclustering, using kt as the ordering.
/// Useful when including declusterings in structures like priority queues
inline bool operator<(const LundEEDeclustering& d1, const LundEEDeclustering& d2) {
  return d1.kt() < d2.kt();
}

/// Map angles to [-pi, pi]
inline const double map_to_pi(const double &phi) {
  if (phi < -M_PI)     return phi + 2 * M_PI;
  else if (phi > M_PI) return phi - 2 * M_PI;
  else                 return phi;
}

inline precision_type dot_product_3d(const PseudoJet & a, const PseudoJet & b) {
  return a.px()*b.px() + a.py()*b.py() + a.pz()*b.pz();
}

/// Returns (1-cos theta) where theta is the angle between p1 and p2
inline precision_type one_minus_costheta(const PseudoJet & p1, const PseudoJet & p2) {

  if (p1.m2() == 0 && p2.m2() == 0) {
    // use the 4-vector dot product. 
    // For massless particles it gives us E1*E2*(1-cos theta)
    return dot_product(p1,p2) / (p1.E() * p2.E());
  } else {
    precision_type p1mod = p1.modp(); // sqrt(p1.px()*p1.px() + p1.py()*p1.py() + p1.pz()*p1.pz());
    precision_type p2mod = p2.modp(); // sqrt(p2.px()*p2.px() + p2.py()*p2.py() + p2.pz()*p2.pz());
    precision_type p1p2mod = p1mod*p2mod;
    precision_type dot = dot_product_3d(p1,p2);

    if (dot > (1-numeric_limit_extras<double>::sqrt_epsilon()) * p1p2mod) {
      // use the same trick that we had for the dot_product function,
      // but now with pimod instead of Ei
      PseudoJet cross_result = cross_product(p1, p2, false);
      // the mass^2 of cross_result is equal to 
      // -(px^2 + py^2 + pz^2) = (p1mod*p2mod*sintheta_ab)^2
      // so we can get
      return -cross_result.m2()/(p1p2mod * (p1p2mod+dot));
    }

    return 1.0 - dot/p1p2mod;
    
  }
}

// Get the angle between two planes defined by normalized vectors
// n1, n2. The sign is decided by the direction of a vector n.
inline precision_type signed_angle_between_planes(const PseudoJet& n1,
      const PseudoJet& n2, const PseudoJet& n) {

  if(abs(n1.modp()-1) > 1e-12 || abs(n2.modp()-1) > 1e-12) {
    _warning_infinity_in_n1_n2.warn("LundEEGenerator.hh::psibar() n1.modp() = " + std::to_string(to_double(n1.modp())) + ", n2.modp() = " + std::to_string(to_double(n2.modp())));
    return 0;
  }

  precision_type omcost = one_minus_costheta(n1,n2);
  precision_type theta;

  // Sometimes when theta ~ pi, that is -n1 || n2, the function returns something
  // that is slightly smaller than -1, i.e. 1-costheta >~ 2 (within sqrt(epsilon))
  // Then we return pi.
  if(abs(omcost-2) < numeric_limit_extras<precision_type>::sqrt_epsilon()) {
    theta = M_PI;
  } else if (omcost > numeric_limit_extras<precision_type>::sqrt_epsilon()) {
    precision_type cos_theta = 1.0 - omcost;
    theta = acos(cos_theta);
  } else {
    // we are at small angles, so use small-angle formulas
    theta = sqrt(2. * omcost);
  }

  PseudoJet cp = cross_product(n1,n2);
  precision_type sign = dot_product_3d(cp,n);

  if (sign > 0) return theta;
  else          return -theta;
}

// function that safely normalises a PseudoJet by first dividing all
// components by the largest of all components, normalising,
// and then multiplying back the above factor
inline PseudoJet safe_normalise(const PseudoJet & p) {
  precision_type px = p.px(), py = p.py(), pz = p.pz(), E = p.E();
  precision_type max = std::max(std::max(std::max(std::abs(px),std::abs(py)),std::abs(pz)),std::abs(E));
  PseudoJet pnorm = PseudoJet(px/max, py/max, pz/max, E/max);
  return pnorm / pnorm.modp();
}

//----------------------------------------------------------------------  
/// \class RecursiveLundEEGenerator
/// Class to carry out Lund declustering to get anything from the
/// primary Lund plane declusterings to the full Lund diagram with all
/// its leaves, etc.
class RecursiveLundEEGenerator {
 public:
  /// constructs a RecursiveLundEEGenerator with the specified depth.
  /// - depth = 0 means only primary declusterings are registered
  /// - depth = 1 means the first set of leaves are declustered
  /// - ...
  /// - depth < 0 means no limit, i.e. recurse through all leaves
  ///
  /// The psibar values that are set in the result Lund tree have the
  /// following property:
  ///
  /// - if the jet with the larger pz has splittings, then its
  ///   first splitting has psibar = 0
  /// - otherwise the first splitting of the other jet has psibar = 0
  ///
  /// Note that this makes psibar IR unsafe (because an arbitrarily soft
  /// splitting can be the one that gets the reference psibar=0 value), 
  /// but differences between psibar values are IR safe.
  /// 
  /// NB: The dynamical_psi_ref option relates to the deprecated definition of psi
  /// New code should use the psibar() function and dynamical_psi_ref
  /// is irrelevant.
  RecursiveLundEEGenerator(int max_depth = 0, bool dynamical_psi_ref = false) :
    max_depth_(max_depth), nx_(1,0,0,0), ny_(0,1,0,0), dynamical_psi_ref_(dynamical_psi_ref)
  {}

  /// destructor
  virtual ~RecursiveLundEEGenerator() {}

  /// enables/disables the computation of psibar
  void do_compute_psibar(bool value) {do_compute_psibar_ = value;}

  /// This takes a cluster sequence with an e+e- C/A style algorithm, e.g.
  /// fjcore::EECambridgeFastPlugin(ycut=1.0).
  ///
  /// The output is a vector of LundEEDeclustering objects, ordered
  /// according to kt [THIS MAY BECOME MORE FLEXIBLE LATER]
  virtual std::vector<LundEEDeclustering> result(const ClusterSequence & cs) const {
    std::vector<fjcore::PseudoJet> exclusive_jets = cs.exclusive_jets(2);
    assert(exclusive_jets.size() == 2);
    
    // order the two jets according to momentum along z axis
    if (exclusive_jets[0].pz() < exclusive_jets[1].pz()) {
      std::swap(exclusive_jets[0],exclusive_jets[1]);
    }

    // the start of the material needed for the psi calculation
    // as described in 2019-02-27-ee-phi-definition logbook
    PseudoJet d_ev = exclusive_jets[0] - exclusive_jets[1];
    Matrix3 rotmat = Matrix3::from_direction(d_ev);
    
    std::vector<LundEEDeclustering> declusterings;
    int depth = 0;
    int max_iplane_sofar = 1;

    // 2024-01 -- attempt at new definition of psibar
    PseudoJet ref_plane;
    double last_psibar = 0.;
    bool first_time = true;

    for (unsigned ijet = 0; ijet < exclusive_jets.size(); ijet++) {
      int sign_s = ijet == 0? +1 : -1;
      append_to_vector(declusterings, exclusive_jets[ijet], depth, ijet, max_iplane_sofar,
                        rotmat, sign_s, ref_plane, last_psibar, first_time);
    }
    
    // a typedef to save typing below
    typedef LundEEDeclustering LD;
    // sort so that declusterings are returned in order of decreasing
    // kt (if result of the lambda is true, then first object appears
    // before the second one in the final sorted list)
    sort(declusterings.begin(), declusterings.end(),
         [](const LD & d1, LD & d2){return d1.kt() > d2.kt();});

    return declusterings;
  }
  
 private:

  /// internal routine to recursively carry out the declusterings,
  /// adding each one to the declusterings vector; the primary
  /// ones are dealt with first (from large to small angle),
  /// and then secondary ones take place.
  void append_to_vector(std::vector<LundEEDeclustering> & declusterings,
                        const PseudoJet & jet, int depth,
                        int iplane, int & max_iplane_sofar,
                        const Matrix3 & rotmat, int sign_s,
                        PseudoJet & psibar_ref_plane,
                        const double & last_psibar, bool & first_time) const {
    PseudoJet j1, j2;
    if (!jet.has_parents(j1, j2)) return;
    if (j1.modp2() < j2.modp2()) std::swap(j1,j2);

    Matrix3 new_rotmat;
    if (dynamical_psi_ref_) {
      // of Eq.(3) 2019-02-27-ee-phi-definition
      new_rotmat = Matrix3::from_direction(rotmat.transpose()*(sign_s*jet)) * rotmat;
    } else {
      new_rotmat = rotmat;
    }
    // main part of psi calculation as described in
    // 2019-02-27-ee-phi-definition logbook
    // (for now this is the fixed reference)
    fjcore::PseudoJet rx = new_rotmat * nx_;
    fjcore::PseudoJet ry = new_rotmat * ny_;
    fjcore::PseudoJet u1 = j1/j1.modp(), u2 = j2/j2.modp();
    fjcore::PseudoJet du = u2 - u1;
    precision_type x = du.px() * rx.px() + du.py() * rx.py() + du.pz() * rx.pz();
    precision_type y = du.px() * ry.px() + du.py() * ry.py() + du.pz() * ry.pz();
    double psi = to_double(atan2(y,x));

    // calculation of psibar
    double psibar = 0.;
    PseudoJet n1, n2;

    // The reference direction (the choice of what psibar = 0 means)
    // is set the first time we compute psibar.
    if (first_time) {
      assert(last_psibar == 0.0);
      psibar = 0.0;
      if (j1.has_user_info<panscales::DirDiffUserInfo>()) {
        PseudoJet j2dir_minus_j1dir = j1.user_info<panscales::DirDiffUserInfo>().dirdiff_to(j2);
        n2 = cross_product(j1,j2dir_minus_j1dir);
      } else {
        n2 = cross_product(j1,j2);
      }
      n2 /= n2.modp();
      psibar_ref_plane = n2;
      first_time = false;
    }
    // Else take the value of psibar_i and the plane from the last splitting to define psibar_{i+1}
    // The signed angle is multiplied by sign_s (+1 for the +z hemisphere, -1 for the -z hemisphere)
    // to take the correct orientation into account.
    else {
      if (j1.has_user_info<panscales::DirDiffUserInfo>()) {
        PseudoJet j2dir_minus_j1dir = j1.user_info<panscales::DirDiffUserInfo>().dirdiff_to(j2);
        n2 = cross_product(j1,j2dir_minus_j1dir);
      } else {
        n2 = cross_product(j1,j2);
      }
      n2 /= n2.modp();
      psibar = map_to_pi(to_double(last_psibar + sign_s*signed_angle_between_planes(psibar_ref_plane, n2, j1)));
    }

    if (!std::isfinite(psibar) && _psibar_not_finite.n_warn_so_far() <= _psibar_not_finite.max_warn()) {
      std::ostringstream ostr;
      ostr      << "-------------------------------------" << std::endl
                << "Warning: LundEEGenerator.hh::psibar() returns an invalid psibar." << std::endl
                << "  harder = " << j1.px() << " " << j1.py() << " " << j1.pz() << " " << j1.E() << " " << j1.m2() << std::endl
                << "  softer = " << j2.px() << " " << j2.py() << " " << j2.pz() << " " << j2.E() << " " << j2.m2() << std::endl
                << " -> plane= " << n2.px() << " " << n2.py() << " " << n2.pz() << " " << n2.E() << " " << n2.m2() << std::endl;
      _psibar_not_finite.warn(ostr.str());
    }

    int leaf_iplane = -1;
    // we will recurse into the softer "parent" only if the depth is
    // not yet at its limit or if there is no limit on the depth (max_depth<0)
    bool recurse_into_softer = (depth < max_depth_ || max_depth_ < 0);
    if (recurse_into_softer) {
      max_iplane_sofar += 1;
      leaf_iplane = max_iplane_sofar;
    }
    
    LundEEDeclustering declust(jet, j1, j2, iplane, psi, psibar, depth, leaf_iplane, sign_s);
    declusterings.push_back(declust);

    // now recurse
    // for the definition of psibar, we recursively pass the last splitting plane (normal to n2) and the last value
    // of psibar
    bool lcl_first_time = false;
    append_to_vector(declusterings, j1, depth, iplane, max_iplane_sofar, new_rotmat, sign_s, n2, psibar, lcl_first_time);
    if (recurse_into_softer) {
      append_to_vector(declusterings, j2, depth+1, leaf_iplane, max_iplane_sofar, new_rotmat, sign_s, n2, psibar, lcl_first_time);
    }
  }
  
  int max_depth_ = 0;
  /// vectors used to help define psi (2019-02-27-ee-phi-definition)
  PseudoJet nx_;
  PseudoJet ny_;
  bool dynamical_psi_ref_;
  bool do_compute_psibar_ = false;

  static LimitedWarning _psibar_not_finite;

};

//----------------------------------------------------------------------
/// \class LundEEGenerator
/// Generates vector of LundEEDeclustering for a given jet
/// corresponding to its LundEE primary plane.
class LundEEGenerator : public FunctionOfPseudoJet< std::vector<LundEEDeclustering> > {
public:
  /// LundEEGenerator constructor
  LundEEGenerator() {}

  /// destructor
  virtual ~LundEEGenerator() {}

  // definitions needed for comparison of declusterings
  struct CompareDeclustWithKt {
    bool operator ()(const LundEEDeclustering& d1, const LundEEDeclustering& d2) const {
      return d1.kt() < d2.kt();
    }
  };
    
  /// return a vector of the declusterings of the primary plane of the
  /// jet, registering the supplied information (iplane) about how to label
  /// the plane
  virtual std::vector<LundEEDeclustering> result_iplane(const PseudoJet& jet, 
							int iplane) const;

  /// create the primary declustering sequence of a jet (with a default plane
  /// label of -1)
  virtual std::vector<LundEEDeclustering> result(const PseudoJet& jet) const override{
    return this->result_iplane(jet, -1);
  }

  /// description of the class
  virtual std::string description() const override;
  
protected:
  /// create a declustering from a pseudojet and its parents
  LundEEDeclustering single_declustering(const PseudoJet& pair,
					 const PseudoJet& j1, const PseudoJet& j2,
					 PseudoJet& cross, int iplane) const;
};

//----------------------------------------------------------------------
/// \class LundEEGeneratorOrdered
/// return declusterings ordered according to the defined measure
class LundEEGeneratorOrdered : public LundEEGenerator {
public:
  LundEEGeneratorOrdered() {}
  virtual ~LundEEGeneratorOrdered() {}

  /// return vector of declusterings ordered according to the defined measure
  virtual std::vector<LundEEDeclustering> result_iplane(const PseudoJet& jet, 
							int iplane) const override;

  /// create an ordered declustering sequence of two jets
  virtual std::vector<LundEEDeclustering> result_twojets(const PseudoJet& jet1,
							 const PseudoJet& jet2) const;
  
  /// create an ordered declustering sequence of multiple jets and n secondary planes
  virtual std::vector<LundEEDeclustering> result_jets(const std::vector<PseudoJet>& jets,
						      int n = 0) const;

protected:
  /// measure to use for ordering
  virtual double ordering_measure(const LundEEDeclustering& d) const = 0;

  /// fill a priority queue with ordered declusterings
  virtual void fill_pq(const PseudoJet& jet, int iplane,
		       std::priority_queue< std::pair<double,LundEEDeclustering> >& pq) const;

  virtual std::vector<LundEEDeclustering> pq_to_vector(std::priority_queue< std::pair<double,
						       LundEEDeclustering> >& pq) const;

};

//----------------------------------------------------------------------
/// \class LundEEGeneratorKtOrdered
/// return declusterings ordered in kt
class LundEEGeneratorKtOrdered : public LundEEGeneratorOrdered {
protected:
  /// measure to use for ordering
  //
  virtual double ordering_measure(const LundEEDeclustering& d) const override {return d.lnkt();};
};
  
} // namespace contrib

FJCORE_END_NAMESPACE

/// for output of declustering information
std::ostream & operator<<(std::ostream & ostr, const fjcore::contrib::LundEEDeclustering & d);



#endif  // __FASTJET_CONTRIB_LUNDEEGENERATOR_HH__

