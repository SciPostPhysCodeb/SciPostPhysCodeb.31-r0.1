//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __EnhancedRoulette_HH__
#define __EnhancedRoulette_HH__

#include <vector>
#include <cmath>

//----------------------------------------------------------------------
/// \class EnhancedRoulette
/// class that implements an efficient version of the veto algorithm 
/// combining a roulette-wheel selection of the element (arXiv:1109.3627)
/// with an enhancement factor (arXiv:1211.7204).

class EnhancedRoulette{
public:

  EnhancedRoulette() {}

  // Constructor
  EnhancedRoulette(double EF) {

    // We arrange elements in bins of enhancement_factor as follows.
    //  bin 0 :            EF <= 0
    //  bin 1 :        0 < EF <= bins_min ( = 1.0)
    //  bin 2 : bins_min < EF <= bins_min + 1*δEF
    //  ...
    //  bin i : bins_min + (i-2)*δEF < EF <= bins_min + (i-1)*δEF
    //  ...
    //  bin n_bins + 1 : bins_min + (n_bins-1)*δEF < EF <= bins_min + (n_bins)*δEF
    //  bin n_bins + 2 : bins_min +   (n_bins)*δEF < EF
    // Where bins_max = bins_min + (n_bins)*δEF.
    // I.e. δEF = (bins_max - bins_min) / n_bins.
    double bins_min = 1.0;
    double bins_max = EF;
    unsigned int n_bins = bins_min == bins_max ? 0 : 1;

    // Total no. bins is: i) uflow (-∞,0], ii) (0,bins_min], iii) n_bins in
    /// (bins_min=1,bins_max], iv) oflow (bins_max,∞).
    _n_bins    = n_bins+3;
    _bins_min  = bins_min;
    _bins_max  = bins_max;
    _dbin      = (_bins_max-_bins_min)/n_bins;
    assert(_bins_max >= _bins_min && n_bins >= 0);

    // Set upper EF bin edge values once and for all.
    _upper_bin_edges.resize(_n_bins);
    _upper_bin_edges[0] = 0.;
    _upper_bin_edges[1] = _bins_min;
    _upper_bin_edges[_n_bins-1] = std::numeric_limits<double>::max();
    if(_n_bins==4) {
      _upper_bin_edges[2] = EF;
    } else if(_n_bins>4) {
      for(unsigned int ixx=2; ixx<_n_bins-1; ixx++)
        _upper_bin_edges[ixx] = _bins_min + (ixx-1)*_dbin;
    }

  // If the following variable is non-zero it means that we are
  // generating lnv according to
  //
  //   dP_trial = Σ_elements dln(v) [ b ln(v) + a - b C ],
  //
  // which differs from the usual dP_trial by the '-b C' term.
  // The following variable is 'C' in the above.
  _find_element_alg_bias = 0.;

  }

  // Set function for 'C' term/factor in dP_trial. See init(...)
  // function above for more info.
  void set_find_element_alg_bias(double find_element_alg_bias) {
    _find_element_alg_bias = find_element_alg_bias;
  }

  // Book-keeping to be initialised on event-by-event basis.
  void init(double EF, double lnv){

    // Total of all astars & bs in the event, and again in bins of EF.
    _tot_astar     = _tot_b     = 0;
    _tot_astar_bin = _tot_b_bin = std::vector<double>(_n_bins);

    // Does what it says on the tin.
    _lnv_of_last_exhaustive_max_min_mod_EF_search = lnv;

    // Index of lowest EF bin that has ever been found to be populated.
    _min_live_bin = _n_bins-1;

    // Vecs of EF, astar, a and b values for every element.
    _EF.clear();   _astar.clear();   _a.clear();   _b.clear();

    // Maximum EF, maximum astar/EF, minimum b/EF of any element in each bin so far.
    _max_EF_bin           = std::vector<double>(_n_bins,-std::numeric_limits<double>::max());
    _max_astar_mod_EF_bin = std::vector<double>(_n_bins,-std::numeric_limits<double>::max());
    _min_b_mod_EF_bin     = std::vector<double>(_n_bins, std::numeric_limits<double>::max());

    // For each bin of EF, a list of indices of ShowerRunner::_elements in that bin.
    _elements_in_bin = std::vector<std::vector<unsigned int> >(_n_bins, std::vector<unsigned int>());

    // Position of each _element's index, eleIdx, in its EF bin vector, iBin.
    // I.e. eleIdx = _elements_in_bin[ iBin ].[ _element_place_in_bin[ eleIdx ] ].
    _element_place_in_bin.clear();

    // Vec of EF bin indices associated to each element.
    _bin_of_element.clear();

  }

  // Return bin index for a given value of EF
  inline unsigned int _get_bin(double EF) {
    if(EF> _bins_max)      return _n_bins-1; // oflow (_bins_max,∞)
    else if(EF==_bins_max) return _n_bins-2; // one bin below oflow
    else if(EF<=0.       ) return 0;         // uflow (-∞,0]
    else if(EF<=_bins_min) return 1;         // (0,_bins_min]
    // We don't use std::floor here, relying instead on conversion from float 
    // to unsigned int. We can get away with it as the conversion to uint is
    // happening on a +ve definite quantity: _dbin is always +ve and 
    // EF -_bins_min is > 0, having handled the various special cases above.
    // #TRK_ISSUE-108  (Valgrind showed an annoying small-ish box saying when using floor.)
    else return (unsigned int)((EF-_bins_min)/_dbin) + 2;
  }

  // Updates following addition of a new element to _elements.
  inline void update(unsigned int eleIdx, double new_EF, double new_astar, double new_a, double new_b) {

    // Find the EF bin this new element is associated with 
    unsigned int iBin = _get_bin(new_EF);

    // Update running total astars and bs for the event and for bin iBin
    _tot_astar += new_astar;
    _tot_b     += new_b;
    _tot_astar_bin[iBin] += new_astar;
    _tot_b_bin[iBin]     += new_b;

    // Record eleIdx's astar, a, b and EF values
    _astar.push_back(new_astar);   _a.push_back(new_a);   _b.push_back(new_b);
    _EF.push_back(new_EF);

    // Check for new lowest b/EF, highest astar/EF, or highest EF so far in 
    // this bin; essential to select an element from within EF bins.
    if(_min_b_mod_EF_bin[iBin]>(new_b/new_EF)) _min_b_mod_EF_bin[iBin] = new_b/new_EF;
    if(_max_astar_mod_EF_bin[iBin]<(new_astar/new_EF)) _max_astar_mod_EF_bin[iBin] = new_astar/new_EF;
    if(_max_EF_bin[iBin]<new_EF) _max_EF_bin[iBin] = new_EF;

    // Check if eleIdx went in the lowest bin so far
    if(iBin<_min_live_bin) _min_live_bin = iBin;

    // Record, in _element_place_in_bin, eleIdx is currently last in
    // _elements_in_bin[iBin], and make it so in _elements_in_bin[iBin].
    _element_place_in_bin.push_back(_elements_in_bin[iBin].size());
    _elements_in_bin[iBin].push_back(eleIdx);

    // Log eleIdx's EF bin
    _bin_of_element.push_back(iBin);

    return;
  }

  // Update for an existing element in _elements disturbed in some way or other.
  inline void update(unsigned int eleIdx, double old_EF, double new_EF,
                     double old_astar, double new_astar, double new_a,
                     double old_b, double new_b) {

    // Update total astars and bs for the whole event; only for generating lnv.
    _tot_astar += new_astar - old_astar;
    _tot_b     += new_b     - old_b;

    // Update EF bin index of the element (may not change).
    unsigned int old_iBin     = _bin_of_element[eleIdx];
    unsigned int new_iBin     = _get_bin(new_EF);

    // Update total astars & bs for EF bins of the element; only for selecting
    // EF bin after lnv. Must be done even if element remained in the same bin.
    _tot_astar_bin[old_iBin] -= old_astar;
    _tot_astar_bin[new_iBin] += new_astar;
    _tot_b_bin[old_iBin]     -= old_b;
    _tot_b_bin[new_iBin]     += new_b;

    // Record eleIdx's potentially updated astar, a, b, EF values; only needed
    // for selecting an element from within its EF bin. 
    _astar[eleIdx] = new_astar;   _a[eleIdx] = new_a;   _b[eleIdx] = new_b;
    _EF[eleIdx] = new_EF;

    // Check for new lowest b/EF, highest astar/EF, or highest EF so far in 
    // this bin; essential to select an element from within EF bins.
    if(_min_b_mod_EF_bin[new_iBin]>(new_b/new_EF)) _min_b_mod_EF_bin[new_iBin] = new_b/new_EF;
    if(_max_astar_mod_EF_bin[new_iBin]<(new_astar/new_EF)) _max_astar_mod_EF_bin[new_iBin] = new_astar/new_EF;
    if(_max_EF_bin[new_iBin]<new_EF) _max_EF_bin[new_iBin] = new_EF;

    // If the element didn't hop to a different EF bin, no further book-keeping
    if(old_iBin==new_iBin) return;

    // Check if eleIdx went in the lowest bin so far
    if(_min_live_bin>new_iBin) _min_live_bin = new_iBin;

    // If eleIdx is to be deleted from list of elements in old_iBin isn't already at end
    if(eleIdx!=_elements_in_bin[old_iBin].back()) {
      // Reset position of element at back of elements_in_bin[old_iBin] to that of eleIdx
      _element_place_in_bin[_elements_in_bin[old_iBin].back()]
        = _element_place_in_bin[eleIdx];
      // Swap eleIdx with the element at back of _elements_in_bin[old_iBin]
      std::swap(_elements_in_bin[old_iBin][_element_place_in_bin[eleIdx]],
                _elements_in_bin[old_iBin].back());
    }
    // Delete the eleIdx entry from the list of elements in old_iBin
    _elements_in_bin[old_iBin].pop_back();

    // Record, in _element_place_in_bin, eleIdx is currently last in
    // _elements_in_bin[new_iBin], and make it so in _elements_in_bin[new_iBin].
    _element_place_in_bin[eleIdx] = _elements_in_bin[new_iBin].size();
    _elements_in_bin[new_iBin].push_back(eleIdx);

    // Record ShowerRunner::_elements[eleIdx] now has EF bin index new_iBin
    _bin_of_element[eleIdx] = new_iBin;

    // Check if all elements are now in the EF <= 0 (uflow) bin.
    if(_elements_in_bin[0].size()==_EF.size()) {
      _tot_astar = 0.;
      _tot_b     = 0.;
    }

    return;
  }

  // Update max EF, aStar, min b, in each bin and tag associated lnv. 
  void update_max_min_mod_EF(double lnv) {
    for (unsigned int iBin = 0; iBin < _n_bins; iBin++) {
      _max_EF_bin[iBin]           = -std::numeric_limits<double>::max();
      _max_astar_mod_EF_bin[iBin] = -std::numeric_limits<double>::max();
      _min_b_mod_EF_bin[iBin]     =  std::numeric_limits<double>::max();
      for (unsigned int iEle_bin = 0; iEle_bin < _elements_in_bin[iBin].size(); iEle_bin++) {
        unsigned int iEle = _elements_in_bin[iBin][iEle_bin];
        _max_EF_bin[iBin] = std::max(_max_EF_bin[iBin],_EF[iEle]);
        _max_astar_mod_EF_bin[iBin] = std::max(_max_astar_mod_EF_bin[iBin],_astar[iEle]/_EF[iEle]);
        _min_b_mod_EF_bin[iBin] = std::min(_min_b_mod_EF_bin[iBin],_b[iEle]/_EF[iEle]);
      }
    }
    _lnv_of_last_exhaustive_max_min_mod_EF_search = lnv;
  }

  // Generates an lnv and element index. First lnv generation is done based
  // on the total Sudakov density (approximately) across the whole event.
  // Then a bin of enhancement factor is selected based on inverse cumulative
  // sampling the contribution each EF bin makes to the total Sudakov density.
  // Then element selection is done by applying stochastic roulette within the
  // chosen bin of EF, using an overestimated density for all elements,
  // min(b)*lnv+max(aStar), where min(b) and max(aStar) are determined
  // periodically, i) every time the event evolves ??? units of lnv, and
  // ii) every time there is a full-fledged branching. The biasing factor
  // _find_element_alg_bias, is set to zero by default, and so has no effect.
  // _find_element_alg_bias can be turned on (+ve) to facilitate throwing
  // points in regions that would otherwise not be populated, in particular
  // lnv = lnQ. This has been necessary for ME corrections, where the density
  // was found to be zero in certain PS, in certain regions of phase space.
  // __ N.B. __ if _find_element_alg_bias is non-zero the PS acceptance prob
  // needs to be modified to account for it: turning _find_element_alg_bias
  // on effectively changes each trial emission probability like so,
  //    dP_trial → dP_trial_new
  //             = dP_trial * ( b ln(v) + a - b C ) / ( b ln(v) + a ),
  // with C here short for _find_element_alg_bias. The latter factor above
  // needs to be accounted for in the PS acceptance probability: something
  // which is currently not in place, but not needed either, since we can
  // keep C = 0 for general purposes. For ME corrections, we are using C > 0,
  // but in that case the ME correction acceptance probability explicitly
  // takes C into account.
  unsigned int find_element(GSLRandom & gsl, double & lnv, double overhead_factor) {
    if(_tot_b == 0 && _tot_astar == 0) lnv = - std::numeric_limits<double>::max();
    if(lnv <= - std::numeric_limits<double>::max()) return 0;

    // overhead_factor multiplies all a's and b's. It explicitly
    // cancels in the tot_astar_o_tot_b ratio and when comparing two
    // densities (effectively a ratio)
    double two_o_tot_b = 2/(overhead_factor*_tot_b);
    double tot_astar_o_tot_b = _tot_astar/_tot_b - _find_element_alg_bias;

    while(true) {

      // Select lnv using overestimated Sudakov (with a -> astar).
      double r = gsl.uniform();
      lnv = -tot_astar_o_tot_b - sqrt(pow2(tot_astar_o_tot_b + lnv) + two_o_tot_b*log1p(-r));

      // _____Validation_____
      // #TRK_ISSUE-109  If seeking binary identical agreement with RoulettePeriodic you should
      // comment out line r = gsl.uniform() ... and the block while(iBin<_n_bins)
      // just below, and set iBin = 1 instead by-hand. See also the next comment
      // marked 'Validation' below too!

      // #TRK_ISSUE-110  TO DO : 
      // Currently the bin selection is done by inverse cumulative sampling,
      // complexity O(N_bins). This has so far been more than fine as slowness
      // in other parts of the code, combined with the fact that we so far 
      // haven't really considered N_bins >~ a few, makes better bin selection
      // here just a micro-optimisation ... for cases studied so far ... 

      // Select iBin by inverse cumulative sampling.
      unsigned int iBin = _min_live_bin;
      r = gsl.uniform() * ( _tot_b * ( lnv - _find_element_alg_bias ) + _tot_astar );
      while(iBin<_n_bins) {
        r -= _tot_b_bin[iBin] * ( lnv - _find_element_alg_bias ) + _tot_astar_bin[iBin];
        if(r<=0) break;
        iBin++;
      }

      // No. elements in iBin'th bin of EF.
      unsigned int N = _elements_in_bin[iBin].size();

      // _____Validation_____
      // If seeking binary identical agreement with RoulettePeriodic you should
      // comment out the next both if(N==1) {...} and if(N==2) {...} blocks below.
      // The final edit needed to obtain binary agreement with RoulettePeriodic
      // is to insert "if(norm_astar>this->_max_aStar) { this->_max_aStar = norm_astar;
      // _max_aStar_o_min_b = _max_aStar/_min_b; }" into ShowerRunner<Shower>::
      // _add_dipole_elements_to_store. Apparently, there is a bug in the other
      // algorithms (RoulettePeriodic, RouletteOneShot) relying on a properly
      // maintained _max_aStar, in that one can seemingly generate a new max
      // aStar value by adding new elements to the event. This seems very weird
      // since one naturally assumes that the dipoles are always decaying to lower
      // mass dipoles. We have long-ago noted that neighbouring dipoles w.r.t a
      // branching dipole can increase their mass through recoil transmitted along
      // a shared gluon, and we check for new max_aStars resulting from that in
      // ShowerRunner<Shower>::_update_element_in_store (since long ago).

      // Efficiency boost for N = 1 element in the bin.
      if(N==1) {
        unsigned int iEle = _elements_in_bin[iBin][0];
        if(_astar[iEle]==_a[iEle]) return iEle;
        double true_density = std::max(_b[iEle] * lnv + _a[iEle],0.0)  - _b[iEle] * _find_element_alg_bias;
        if( ( _b[iEle] * ( lnv - _find_element_alg_bias ) + _astar[iEle] )
            * gsl.uniform() < true_density ) return iEle;
        else continue;
      }

      // Efficiency boost for N = 2 elements in the bin.
      if(N==2) {
        unsigned int iEle0 = _elements_in_bin[iBin][0];
        unsigned int iEle1 = _elements_in_bin[iBin][1];
        double P = ( _b[iEle0] * lnv + _astar[iEle0] )
                 / ( _tot_b_bin[iBin] * lnv + _tot_astar_bin[iBin] );
        unsigned int iEle = gsl.uniform()<P ? iEle0 : iEle1;
        if(_astar[iEle]==_a[iEle]) return iEle;
        double true_density = std::max(_b[iEle] * lnv + _a[iEle],0.0)  - _b[iEle] * _find_element_alg_bias;
        if( ( _b[iEle] * ( lnv - _find_element_alg_bias ) + _astar[iEle] )
            * gsl.uniform() < true_density ) return iEle;
        else continue;
      }

      // Overestimate max density contribution from any element in this EF bin.
      double max_density = _max_EF_bin[iBin]
                         * ( _min_b_mod_EF_bin[iBin] * ( lnv - _find_element_alg_bias )
                           + _max_astar_mod_EF_bin[iBin] );

      // Select element from N elements in iBin with stochastic roulette.
      double over_density = 0;
      unsigned int iEle = 0;
      while(true) {
        iEle = _elements_in_bin[iBin][gsl.uniform_int(N)];
        over_density = _b[iEle] * ( lnv - _find_element_alg_bias ) + _astar[iEle];
        if(max_density==0) break; // Handle potential freak r = 0 at initial lnv = 0.
        if(gsl.uniform()*max_density<over_density) {
          if(_astar[iEle]==_a[iEle]) return iEle;
          double true_density = std::max(_b[iEle] * lnv + _a[iEle],0.0) - _b[iEle] * _find_element_alg_bias;
          if(over_density*gsl.uniform()<true_density) return iEle;
          else break;
        }
      }

    }

    assert("EnhancedRoulette::find_element failed." && false);
    return 0;
  }

protected:

  // No. bins of enhancement factor (EF) in (bins_min,bins_max). N.B.
  // unusually we take, underflow <= bins_min, overflow  >= bins_max,
  // s.t. with n_bins = 1, EF = 0 goes in underflow, EF = 1 goes in the
  // one bin, and EF = default EF goes in overflow. See also _get_bin fn.
  unsigned int _n_bins;
  double _bins_min, _bins_max;

  // Bin width.
  double _dbin;  

  // The upper EF value where each bin ends.
  std::vector<double> _upper_bin_edges;

  // Total of all astars & bs in the event, and again in bins of EF.
  double _tot_astar, _tot_b;
  std::vector<double> _tot_astar_bin, _tot_b_bin;

  // Does what it says on the tin.
  double _lnv_of_last_exhaustive_max_min_mod_EF_search;

  // Index of lowest EF bin that has ever been found to be populated.
  unsigned int _min_live_bin;

  // Vecs of EF, astar, a and b values for every element.
  std::vector<double> _EF,   _astar,   _a,   _b;

  // Maximum EF, maximum astar/EF, minimum b/EF of any element in each bin so far.
  std::vector<double> _max_EF_bin, _max_astar_mod_EF_bin, _min_b_mod_EF_bin;

  // For each bin of EF, a list of indices of ShowerRunner::_elements in that bin.
  std::vector<std::vector<unsigned int>> _elements_in_bin;

  // Position of each _element's index, eleIdx, in its EF bin vector, iBin.
  // I.e. eleIdx = _elements_in_bin[ iBin ].[ _element_place_in_bin[ eleIdx ] ].
  std::vector<unsigned int> _element_place_in_bin;

  // Vec of EF bin indices associated to each element.
  std::vector<unsigned int> _bin_of_element;

  // Represents 'C' in dP_trial = Σ_elements dln(v) [ b ln(v) + a - b C ].
  // See init function definition at the top of the file for more info.
  double _find_element_alg_bias;
};

#endif // __EnhancedRoulette_HH__
