//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
// File generated automatically from ['GenKtHiPrecPlugin.hh'] by /Users/gsalam/work/2020-flavour-jet-alg/IRSafety-prec/convert-flav.py
// start of GenKtHiPrecPlugin.hh
#include "Type.hh"
#ifndef __GENKTHIPRECPLUGIN_HH__
#define __GENKTHIPRECPLUGIN_HH__

// to facilitate use with fjcore
// #ifndef __FJC_FLAVINFO_USEFJCORE__
// #include "fastjet/NNFJN2Plain.hh"
// #else
#define fastjet fjcore
#include "fjcore_local.hh"
#define FASTJET_BEGIN_NAMESPACE namespace fjcore {
#define FASTJET_OVERRIDE override
#define FASTJET_END_NAMESPACE }
// #endif

FASTJET_BEGIN_NAMESPACE  // defined in fastjet/internal/base.hh

//--------------------------------------------------------------------
/// @ingroup plugins
/// \class GenKtHiPrecPlugin
/// C++ implementation of Flavour-kt algorithm. MLH 2021-10-01.
class GenKtHiPrecPlugin : public JetDefinition::Plugin {
public:

  /// distance measure
  enum DistanceMeasure { sinh_delta_R, delta_R, cosphi_coshy };

  /// Main constructor for the class
  GenKtHiPrecPlugin(precision_type R, precision_type p) : _R(R), _p(p) {}

  /// copy constructor
  GenKtHiPrecPlugin(const GenKtHiPrecPlugin &plugin) { *this = plugin; }

  /// Jet radius of kt distance
  precision_type R() const { return _R; }
  precision_type p() const { return _p; }

  /// whether to use the e+e- version of the flavour-kt algo
  bool is_spherical() const { return false; }

  // Required by base class:
  virtual std::string description() const;

  virtual void run_clustering(ClusterSequence &) const;

private:
  precision_type _R, _p;

};

FASTJET_END_NAMESPACE

#endif  // __GENKTHIPRECPLUGIN_HH__
// end of GenKtHiPrecPlugin.hh
