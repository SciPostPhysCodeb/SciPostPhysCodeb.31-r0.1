//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __HELPERS_HH__
#define __HELPERS_HH__

#include "SimpleHist.hh"

inline void output_cumulative(const SimpleHist & hist0, 
                   std::ostream * ostr = (&std::cout),
                   double norm = 1.0) {

//  double count_events = hist0.outflow();
//  for (unsigned i = 0; i < hist0.size(); i++) {
//    count_events += hist0[i];
//  }
//  if (count_events != 1.0/norm){
//    std::cout << "ERROR: fraction of lost events = " << count_events*norm << std::endl;    
//    exit(-1);
//  }

  double weight_cumul = hist0.outflow();   
  for (unsigned i = 0; i < hist0.size(); i++) {
    weight_cumul += hist0[i];
    *ostr << hist0.binlo(i)  << " " 
          << hist0.binmid(i) << " "
          << hist0.binhi(i) << " "
          << weight_cumul*norm << std::endl;
  }
}


// returns the integral between v and the upper edge of the histogram
// This function might be useful for tests, e.g. fitting the O(as) constant term
inline void output_inverse_cumulative(const SimpleHist & hist0, 
                   std::ostream * ostr = (&std::cout),
                   double norm = 1.0) {
  // assume there is no outflow bin above the upper edge of the histogram [this might be dangerous]
  double weight_inverse_cumul = 0.;
  
  for (int i = hist0.size() - 1; i >= 0; i--) {
    weight_inverse_cumul += hist0[i];
    *ostr << hist0.binlo(i)  << " " 
          << hist0.binmid(i) << " "
          << hist0.binhi(i) << " "
          << weight_inverse_cumul*norm << std::endl;
  }
}

#endif // __HELPERS_HH__
