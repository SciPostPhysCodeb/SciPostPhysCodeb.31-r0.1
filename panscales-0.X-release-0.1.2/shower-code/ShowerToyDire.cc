//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerToyDire.hh"

namespace panscales{
  // read-in the splitting weights 
  void ShowerToyDire::Element::fill_dglap_splitting_weights(const Particle &p,
                                                const precision_type &z,
                                                const precision_type &omz,
                                                const precision_type &k2,
                                                double & weight_rad_g,
                                                double & weight_rad_q) const{
    precision_type pq, pg;
    // treat differently cases where p is a quark or a gluon
    if (p.pdgid() == 21){
      pg = halfPg2gg_normalised(z, omz, k2);
      pq = halfPg2qqbar_normalised(z, omz, k2);
    } else {
      pg = Pq2qg_normalised(z, omz, k2);
      pq = 0.0;
    }

    // note that the jacobian factor of omz
    // is included here
    // the reason is that otherwise we loose
    // precision in converting the precision_type
    // to double in the step below, causing
    // issues when running in higher-precision (i.e. doubleexp)
    weight_rad_g = to_double(pg * omz);
    weight_rad_q = to_double(pq * omz);
  }

  //----------------------------------------------------------------------
  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerToyDire::elements(
    Event & event, int dipole_index) const {
    int i3    = event.dipoles()[dipole_index].index_3()   ;
    int i3bar = event.dipoles()[dipole_index].index_3bar();

    assert(!event.particles()[i3].is_initial_state() && !event.particles()[i3bar].is_initial_state() && 
      "Our Dire implementation only handles final-state showers");

    std::unique_ptr<typename ShowerBase::Element> elms[] = {
      std::unique_ptr<typename ShowerBase::Element>(new ShowerToyDire::Element(i3, i3bar, dipole_index, &event, this)),
      std::unique_ptr<typename ShowerBase::Element>(new ShowerToyDire::Element(i3bar, i3, dipole_index, &event, this))
    };
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }
  ShowerBase::EmissionInfo* ShowerToyDire::create_emission_info() const{ return new ShowerToyDire::EmissionInfo(); }

  //----------------------------------------------------------------------
  Range ShowerToyDire::Element::lnb_generation_range(double lnv) const {
    // return something that corresponds to log(kappa^2) < ln(1-z) < 0
    // or equivalently 0 < z < 1-kappa^2
    return Range(to_double(log_T(kappa2(precision_type(lnv)))), 0.0);
  }

  //Range ShowerToyDire::Element::lnb_exact_range(double lnv) const {
  //  std::cerr << "Error: We do not expect lnb_exact_range to be called " << std::endl;
  //  double k2 = to_double(kappa2(precision_type(lnv)));
  //  double rt = k2 < 0.25 ? sqrt(0.25 - k2) : 0;
  //  return Range(log(0.5-rt), log(0.5+rt));
  //}

  double ShowerToyDire::Element::lnv_lnb_max_density() const {
    // For now we work in a large-NC limit, with CF defined to be equal
    // to CA/2; the density that is returned here corresponds to that
    // for a quark or for half a gluon
    return _shower->max_alphas() * _shower->qcd().CA() / M_PI;
  }

  //----------------------------------------------------------------------
  bool ShowerToyDire::veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const {
    assert(false && "cannot use Dire truncated.");
    return false;
  }

  //----------------------------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  bool ShowerToyDire::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerToyDire
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerToyDire::EmissionInfo & emission_info = *(static_cast<typename ShowerToyDire::EmissionInfo*>(emission_info_base));

    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;

    // We will often be working with 1-z close to 0, because (1-z) is
    // the gluon momentum fraction.  To retain precision, we explicitly
    // introduce a 1-z variable called omz, and use that wherever
    // we would otherwise call 1-z
    precision_type omz = exp(precision_type(lnb));
    precision_type z = 1.0 - omz;

    precision_type k2 = kappa2(precision_type(lnv));
    // kappa2 = e^(2 lnv)/m^2
  
    // cache this info for future usage
    emission_info.omz = omz;
    emission_info.z   = z;
    emission_info.k2  = k2;

    // this is a dipole shower: only the emitter can split
    emission_info.emitter_weight_rad_gluon   = 0.0;
    emission_info.emitter_weight_rad_quark   = 0.0;
    emission_info.spectator_weight_rad_gluon = 0.0;
    emission_info.spectator_weight_rad_quark = 0.0;

    // To avoid nans later
    precision_type omz_minus_k2 = omz-k2;
    if (omz_minus_k2 <= 0) return 0;
    
    // original formulation
    // kt2_orig = omz * (z*omz - k2) / pow2(omz_minus_k2) * exp(precision_type(2.0*lnv));
    // had problems because pow2(omz_minus_k2) could be of order k^4
    // Instead, write it in stages, so as to reduce chance of 
    // getting underflows or overflows along the way
    precision_type kt2 = (omz / omz_minus_k2) * ((z*omz - k2)/omz_minus_k2) * exp(precision_type(2.0*lnv));
    if (kt2 < 0 || kt2 != kt2) {return 0;}

    // here we should make a decision based on flavour
    fill_dglap_splitting_weights(emitter(), z, omz, k2, 
                                 emission_info.emitter_weight_rad_gluon,
                                 emission_info.emitter_weight_rad_quark);
    // we have dln(1-z) as our integration measure, this was included in the 
    // emitter weights above


    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = omz;
    emission_info.z_radiation_wrt_spectator = 0.;

    // Normalisation: 3 factors to take into account
    //  - colour factors are treated elsewhere so that this code can
    //    assume the large-Nc limit and use CA as an overall
    //    factor. This gives a factor alphas CA/pi
    //  - we have dln(1-z) as our integration measure so need to
    //    include a dz/dln(1-z) = (1-z) factor here. 
    //    Note that this factor is already included in the 
    //    emitter_weight_rad_gluon/quark to prevent
    //    issues when running at higher precision. 
    //    See also the comment in fill_dglap_splitting_weights
    //  - we use lnv = 0.5*ln(t) as our evolution variable, so include
    //    a factor of two in the density

    // colour factors are treated elsewhere so that this code can assume
    // the large-Nc limit and use CA as an overall factor
    precision_type normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
  
    // we use lnv = 0.5*ln(t) as our evolution variable, so
    // include a factor of two in the density
    normalisation_factor *= 2.0;

    // then normalise by the max density to get the acceptance
    double normalisation_factor_double = to_double(normalisation_factor);
    emission_info.emitter_weight_rad_gluon *= normalisation_factor_double;
    emission_info.emitter_weight_rad_quark *= normalisation_factor_double;

    return true;
  }

  //----------------------------------------------------------------------
  bool ShowerToyDire::Element::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, 
        const RotatedPieces & rp) const {
    typename ShowerToyDire::EmissionInfo & emission_info = *(static_cast<typename ShowerToyDire::EmissionInfo*>(emission_info_base));

    // retrieve cached info
    const double & phi = emission_info.phi;

    const precision_type & omz = emission_info.omz;
    const precision_type & z   = emission_info.z;
    const precision_type & k2  = emission_info.k2;

    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);

    // the eqs. after (A.4) in the Dire paper
    precision_type yijk   = k2/omz;

    // #TRK_ISSUE-324  make sure our kinematics are sensible: how do we tie this 
    // with our acceptance cut [FORLATER]
    if (yijk > z) return false;

    // this is the formula we are looking for: ztilde = (z - yijk)/(1 - yijk);
    // but we want something that will retain precision when 1-z is small
    precision_type ztilde = (k2 - z*omz)/(k2-omz);
    precision_type omztilde = pow2(omz)/(omz-k2);

    // the transverse normalisation is given by masslessness condition
    // for (A.4a)
    precision_type norm_perp = sqrt(2 * dot_product(rp.spectator,rp.emitter)
                                   * ztilde * omztilde * yijk);

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp * (sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    emission_info.k_perp = perp;

    // A.4 itself
    // pi = emitter
    // pj = radiation
    // pk = spectator
    // Note: we use p3 in the operations here to avoid going via special "mass preserving" operations
    // (and mass tests) which bring no benefit in a case where the final mass should be zero.
    Momentum3<precision_type> emitter_out3   = ztilde     * rp.emitter.p3() + (yijk * omztilde) * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> radiation3     = omztilde   * rp.emitter.p3() + (yijk * ztilde  ) * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> spectator_out3 = (1 - yijk) * rp.spectator.p3();

    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
    return true;
  }


  //----------------------------------------------------------------------
  // update the element kinematics (called after an emission)
  void ShowerToyDire::Element::update_kinematics() {
    if (use_diffs()) {
      _dipole_m2 = 2*dot_product_with_dirdiff(emitter(), spectator(), dipole().dirdiff_3_minus_3bar); 
    } else {
      _dipole_m2 = (emitter() + spectator()).m2();
    }
    if (_dipole_m2 <= 0) {
      std::cerr << "ShowerToyDire::Element::update_kinematics: zero mass element with dipole_m2 = " << _dipole_m2 << std::endl;
      std::cerr << "   emitter  : " << emitter()   << std::endl;
      std::cerr << "   spectator: " << spectator() << std::endl;
      std::cerr << "   dipole   : " << emitter()+spectator() << std::endl;
      std::cerr << "   1-cos(th): " << one_minus_costheta(emitter(),spectator()) << std::endl;
      if (use_diffs()) {
        std::cerr << "   dirdiff  : " << dipole().dirdiff_3_minus_3bar << std::endl;
      }
      throw ErrorZeroMassElement();
    }
  }

  // update the element indices and kinematics
  void ShowerToyDire::Element::update_indices(unsigned emitter_index, unsigned spectator_index) {
    _emitter_index   = emitter_index;
    _spectator_index = spectator_index;
    update_kinematics();
  }

  //----------------------------------------------------------------------
  // for a given lnv,lnb, returns the pseudorapidity with respect to
  // the closer of the emitter/spectator, defined in the event
  // centre-of-mass frame. This eta will be correct in the
  // soft+collinear limit but may differ from the true eta in the
  // soft large-angle limit and the hard-collinear limit.
  //
  // See tests in 2019-07-approx-lnkt-eta/ and
  // logbook/2019-07-27--approx-lnkt-eta
  double ShowerToyDire::Element::eta_approx(double lnv, double lnb) const {
    // work out eta wrt the emitter, keeping in mind that lnb is 0 for a hard
    // collinear emission, so eta there should be given by log(2E/kt) where
    // E is the emitter energy in the event centre-of-mass frame.
    // Decreasing lnb (i.e. going to negative lnb) decreases the rapidity.
    double eta_emitter = -lnv + to_double(log_T(2.0*dot_product(emitter(), _event->Q())/_event->Q().m())) + lnb;
    // similar formula wrt spectator, keeping in mind a map lnb ->
    // log(kappa2) - lnb gives us the symmetric point in lnb (swapping
    // spectator/emitter)
    double eta_spectator = -lnv + to_double(log_T(2.0*dot_product(spectator(), _event->Q())/_event->Q().m()))
                                + (to_double(log_T(kappa2(precision_type(lnv))))-lnb);
    // now decide which of the rapidities is smaller and return that
    // one, signing things such that we're positive if closer to the
    // emitter negative otherwise.
    if (eta_emitter > eta_spectator) return  eta_emitter;
    else                             return -eta_spectator;
  }  

  double ShowerToyDire::Element::lnb_for_min_abseta(double lnv) const {
    // we need to work out the lnb value for which eta_emitter and
    // eta_spectator are equal in the eta_approx function
    return 0.5*(to_double(log_T(2.0*dot_product(spectator(), _event->Q())/_event->Q().m()))
                + to_double(log_T(kappa2(lnv)))
                - to_double(log_T(2.0*dot_product(emitter(), _event->Q())/_event->Q().m())));
  }
  
  //----------------------------------------------------------------------
  // splitting functions
  //----------------------------------------------------------------------
  
  /// Return the value of the splitting function for a quark splitting
  /// into a quark and a gluon, including coupling and colour factors.
  ///
  /// z and k2 correspond to z and kappa2 in Eq.(2.20a) of our
  /// arxiv:1805.09327; omz = 1-z, but intended to be precise
  ///
  /// This implementation is normalised by 1/2 CF to match our treatment
  /// of colour factors.
  precision_type ShowerToyDire::Element::Pq2qg_normalised(precision_type z, precision_type omz, precision_type k2) const {
    // the brackets in Eq.(2.20a)
    precision_type p;
    if (_shower->qcd().soft_limit()){
      p = 2.*omz/(pow2(omz) + k2);
    } else {
      p = 2.*omz/(pow2(omz) + k2) - (1.+z);
    }
    // // A line for testing: removing the k2 in the splitting fn
    // // to see how we approach the asymptotic soft region.
    // precision_type p = 2*(1-z)/(pow2(1-z)) - (1+z);
    // // the prefactor, where we have used
    // p *= _shower->qcd().alphas() * _shower->qcd().CF() / (2.*M_PI);

    // Note: this is used for the kinematic acceptance only (the running
    // coupling is handled elsewhere. Hence we used the max alphas)
    // [otherwise, it's not 100% clear how one could get lnkt]
    p *= 0.5 / 2.;
    return p;
  }

  /// Return the value of the splitting function, for a gluon splitting
  /// into 2 gluons, including coupling and colour factors.
  ///
  /// z and k2 correspond to z and kappa2 in Eq.(2.20b) of our
  /// arxiv:1805.09327; omz = 1-z, but intended to be precise
  ///
  /// Note that we include the option, implemented in QCDinstance, to
  /// have the (half) g->gg splitting function behave as the q->qg one.
  ///
  /// This implementation is normalised by 1/CA to match our treatment
  /// of colour factors
  precision_type ShowerToyDire::Element::halfPg2gg_normalised(precision_type z, precision_type omz, precision_type k2) const {
    if (_shower->qcd().gluon_uses_q2qg()) return Pq2qg_normalised(z,omz,k2);
  
    // the brackets in Eq.(2.20b)
    precision_type p;
    if (_shower->qcd().soft_limit()){
      p = 2.*omz/(pow2(omz) + k2);
    } else {
      p = 2.*omz/(pow2(omz) + k2) - 2 + z*omz;
    }

    p *= 0.5 / 2.;
    return p;
  }

  /// Return the value of the splitting function, for a gluon splitting
  /// to a qqbar pair, including coupling and colour factors.
  ///
  /// z and k2 correspond to z and kappa2 in Eq.(2.20c) of our
  /// arxiv:1805.09327; omz = 1-z, but intended to be precise
  ///
  /// Note that we include the option, implemented in QCDinstance, to
  /// disable g->qqbar splittings
  ///
  /// This implementation is normalised by 1/CA to match our treatment
  /// of colour factors
  precision_type ShowerToyDire::Element::halfPg2qqbar_normalised(precision_type z, precision_type omz, precision_type k2) const {
    // the brackets in Eq.(2.20c)
    precision_type p;
    if ((_shower->qcd().soft_limit()) || (!_shower->qcd().gluon_uses_qqbar())){
      p = 0.0;
    } else {
      p = 1-2*z*omz;
    }

    p *= 0.5*_shower->qcd().nf()*_shower->qcd().TR() / (2.*_shower->qcd().CA());
    return p;
  }

  /// Return the fraction of g2qq splittings (in total gluon splittings)
  precision_type ShowerToyDire::Element::g2qqbar_fraction(precision_type z, precision_type omz, precision_type k2) const {
    precision_type Pq = halfPg2qqbar_normalised(z,omz,k2);
    precision_type Pg = halfPg2gg_normalised   (z,omz,k2);
    return Pq/(Pg+Pq);
  }
} // namespace panscales
