
# - Try to find LHAPDF
# Defines:
#
#  LHAPDF_FOUND
#  LHAPDF_INCLUDE_DIR
#  LHAPDF_INCLUDE_DIRS (not cached)
#  LHAPDF_LIBRARY
#  LHAPDF_LIBRARIES (not cached)
#  LHAPDF_LIBRARY_DIRS (not cached)
#
# This file has been taken from
#   https://github.com/HSF/cmaketools/blob/master/modules/FindLHAPDF.cmake
# as of 2022-11-24
#
#
# We have added a first part that tries to add support to cases where
# we want to find LHAPDF through lhapdf-config

#----------------------------------------------------------------------
# added part

set(LHAPDF_CONFIG_ROOT_HINT)

# Check for the lhapdf-config script
find_program(LHAPDF_CONFIG NAMES lhapdf-config PATHS $ENV{LHAPDF_ROOT_DIR}/bin ${LHAPDF_ROOT_DIR}/bin)
if (LHAPDF_CONFIG)
   execute_process(COMMAND ${LHAPDF_CONFIG} --prefix OUTPUT_VARIABLE LHAPDF_CONFIG_ROOT_HINT ERROR_VARIABLE error OUTPUT_STRIP_TRAILING_WHITESPACE)
   message(STATUS "Will add ${LHAPDF_CONFIG_ROOT_HINT} to LHAPDF Search (from lhapdf-config)")
endif()

# end of added part
#----------------------------------------------------------------------
# part borrowed from
#  https://github.com/HSF/cmaketools/blob/master/modules/FindLHAPDF.cmake



find_library(LHAPDF_LIBRARY NAMES LHAPDF
             HINTS $ENV{LHAPDF_ROOT_DIR}/lib ${LHAPDF_ROOT_DIR}/lib ${LHAPDF_CONFIG_ROOT_HINT}/lib)

find_path(LHAPDF_INCLUDE_DIR LHAPDF/LHAPDF.h
          HINTS $ENV{LHAPDF_ROOT_DIR}/include ${LHAPDF_ROOT_DIR}/include ${LHAPDF_CONFIG_ROOT_HINT}/include)

mark_as_advanced(LHAPDF_INCLUDE_DIR LHAPDF_LIBRARY)

# handle the QUIETLY and REQUIRED arguments and set LHAPDF_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LHAPDF DEFAULT_MSG LHAPDF_INCLUDE_DIR LHAPDF_LIBRARY)

set(LHAPDF_LIBRARIES ${LHAPDF_LIBRARY})
get_filename_component(LHAPDF_LIBRARY_DIRS ${LHAPDF_LIBRARY} PATH)

set(LHAPDF_INCLUDE_DIRS ${LHAPDF_INCLUDE_DIR})

mark_as_advanced(LHAPDF_FOUND)

