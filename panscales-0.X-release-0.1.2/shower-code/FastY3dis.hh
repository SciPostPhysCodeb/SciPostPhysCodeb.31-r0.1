//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
//
#ifndef __FASTY3DIS_HH__
#define __FASTY3DIS_HH__
//
#include <memory>
#include "Observables.hh"
#include "EECambridgeFast.hh"
#include "EECambridgeDirDiff.hh"

//----------------------------------------------------------------------
/// @ingroup observable_classes
/// \class FastY3dis 
/// class that calculates a fast version of Durham Y3 for DIS events
class FastY3dis : public Observable {
  
public:
  // Constructor select_in=1 : keep all particles in event (unused here ...)
  FastY3dis(bool use_WTA = true, bool use_dirdiffs = false, int select_in = 1) : 
    Observable(select_in), _use_WTA(use_WTA), _use_dirdiffs(use_dirdiffs) {
    //fjcore::JetDefinition jet_def(new EECambridgeOneMinusCosTheta(1.0));
    assert(_use_WTA && "for dis, only wta -- otherwise we need to fix ISR");

    const double cambridge_ycut = 1.0;
    if (_use_dirdiffs) {
      assert(_use_WTA);
      _jet_def = fjcore::JetDefinition(new panscales::EECambridgeDirDiff(cambridge_ycut));
    } else {
      _jet_def = fjcore::JetDefinition(new fjcore::EECambridgeFastPlugin(cambridge_ycut));
    }
    if (_use_WTA) _jet_def.set_recombination_scheme(fjcore::WTA_modp_scheme);
    _jet_def.delete_plugin_when_unused();
  }
  
  // Analyse event for FastY3dis
  virtual bool analyse(const panscales::Event& event) override {
    //
    // Initization as per Observables.cc Y3 function ...
    _y3 = 0.0;
    //
    // Also as per Observables.cc Y3 function ...
    if(event.size() < 3) {
      std::cout << "\nError in FastY3dis::analyse.\n";
      std::cout << "\nEvent only has " << event.size() << " particles!\n";
      // ensure that an old _cs cannot accidentally be re-used
      _cs.reset(nullptr);
      return false;
    }

    //
    // Set up jet definition etc ...
    //
    // Run clustering, get y3 ...
    // (NB: particles here are automatically converted to FJ::PseudoJet,
    // and we can make this even more sophisticated if need be)
    if (_use_dirdiffs) {
      // store the object with pairwise distances
      _pairwise_distances.reset(new panscales::EventPairwiseDistances(event));
    }
      
    // extract the particles and associated them with a UserInfo type
    // that contains a pointer to the pairwise distances 
    vector<fjcore::PseudoJet> particles(event.particles().size()-1);
    for (unsigned i = 0; i < event.particles().size(); i++) {
      if(event.particles()[i].is_parton()){
	particles.push_back(event.particles()[i]);
	assert(!_use_dirdiffs &&"\n \t Silvia has not checked the following\n");
	if (_use_dirdiffs)  particles[particles.size()-1].set_user_info(new panscales::DirDiffUserInfo(_pairwise_distances.get(), i));
      }
    }
    _cs.reset(new fjcore::ClusterSequence(particles, _jet_def));

    // this will return zero if the event had exactly two particles
    _y3 = _cs->exclusive_dmerge_max(2) / event.Q2();
  
    // compute the jet axis from the exclusive 2-jet configuration
    std::vector<fjcore::PseudoJet> exclusive_jets = _cs->exclusive_jets(2);
    _jet_axis = panscales::Momentum(exclusive_jets[0].px(), exclusive_jets[0].py(), exclusive_jets[0].pz(), 0.)/exclusive_jets[0].modp();
    //

    _reference_vector[0] = panscales::Momentum(exclusive_jets[0].px(), exclusive_jets[0].py(), exclusive_jets[0].pz(), exclusive_jets[0].modp() )/exclusive_jets[0].modp();
    _reference_vector[1] = panscales::Momentum(exclusive_jets[1].px(), exclusive_jets[1].py(), exclusive_jets[1].pz(), exclusive_jets[1].modp() )/exclusive_jets[1].modp();
    
    return true;
  }

  /// returns a constant reference to the ClusterSequence that was generated
  /// for this event
  const fjcore::ClusterSequence & cs() const {return *_cs;}
  
  // Provide a listing of the info.
  virtual void list() const override {
    std::cout << "\n ----- FastJet Y3 Durham (Ang. Ord.)  ------ \n";
    std::cout << " y3 = " << _y3  << "\n";
    std::cout << " jet_axis = " << _jet_axis  << "\n";
    std::cout << "\n ------------------------------------------- \n";
  }
  
  // Return info on results of analysis
  virtual precision_type operator()() const override { return y3(); }
  precision_type y3() const { return _y3; }

  // returns the jet axis as it stands after 2->2 clustering. 
  panscales::Momentum jet_axis() const { 
    // Watch out: if the class with created with use_WTA=true, the the
    // axis is deduced from just one of the two jets, which may be
    // random; until we have a good solution to this, crash out
    // if the jet axis is requested.
    assert(!_use_WTA);
    return _jet_axis; 
  }

  panscales::Momentum reference1() const{
    return _reference_vector[0];
  }
  
  panscales::Momentum reference2() const{
    return _reference_vector[1];
  }

  

private:
  // Outcome of analysis
  bool _use_WTA, _use_dirdiffs;
  precision_type _y3;
  panscales::Momentum _jet_axis;
  panscales::Momentum _reference_vector[2];
  fjcore::JetDefinition _jet_def;
  std::unique_ptr<fjcore::ClusterSequence> _cs;
  std::unique_ptr<panscales::EventPairwiseDistances> _pairwise_distances;
  
};
//
#endif // end __FASTY3DIS_HH__
//

