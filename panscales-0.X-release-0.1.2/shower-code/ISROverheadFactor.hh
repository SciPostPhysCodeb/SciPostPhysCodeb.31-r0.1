//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __ISROVERHEADFACTOR_HH__
#define __ISROVERHEADFACTOR_HH__

#include "QCD.hh"
#include "HoppetRunner.hh"
#include "Optional.hh"

namespace panscales{

//-----------------------------------------------------------------
/// \class ISROverheadFactor
/// helper class to keep track of the overhead factor for ISR
///
/// We store the overhead factor in 2 vectors:
///   overhead_smallx (x<1/2) with log(1/2x)=0...Lsmall_max
///   overhead_largex (x>1/2) with log(1/2(1-x))=0...Llarge_max
/// In both cases, we take steps of size dLsmall (fixed to 1) and dLlarge(fixed to 0.2.
/// Initially, we go to Lsmall_max=10, Llarge_max=2
/// If we make a query outside the range, the range is dynamically
/// extended
class ISROverheadFactor{
public:
  /// default ctor
  ISROverheadFactor(const QCDinstance &qcd, HoppetRunner *hoppet_runner);

  /// query the overhead factor for 2 partons of x fractions x1 and x2
  /// and flavours id1 and id2.  lnktmax is the current PDF lnkt in
  /// the shower (used as an upper bound when finding the max)
  ///
  /// Note that this automatically updates the cache if x1|x2 go
  /// outside the already cached range (hence this method being
  /// non-const)
  ///
  /// #TRK_ISSUE-660  GS-NOTE: lnktmax currently not implemented
  double operator()(double x1, double x2,
                    int pdgid1, int pdgid2);
  // , double lnktmax);    
  // overload for when we have DIS
  double operator()(double x, 
                    int pdgid);

  /// for situations where ISR overhead is coming out as insufficiently large, 
  /// increase the safety factor by_this_factor
  void increase_safety_factor(double by_this_factor) {_safety_factor *= by_this_factor;}
  double safety_factor() const {return _safety_factor;}

  /// output the ISROverheadFactor to the stream
  std::ostream & output(std::ostream & ostr); 

protected:
  const QCDinstance &_qcd;
  HoppetRunner *_hoppet_runner;
  double _dLsmall, _dLlarge;
  double _safety_factor;
  double _Lsmall_max, _Llarge_max, _transition_x, _transition_logx;
  std::vector<std::vector<double> > _Csmall, _Clarge; //< one per flavour
  unsigned int _points_per_grid, _grid_iterations;

  /// fill the initial grids
  void _initialise();

  /// return the max overhead factor for a parton of a given x and
  /// pdgid, knowing that one probes ktscales below lnktmax
  double _search_and_extend(double x, int pdgid);
  //, double lnktmax);
  

  /// compute the maximal probability for a given x0, pdgid0
  double _find_max_probability(double lnx0, int pdgid0) const;
  //, double lnktmax) const;

  /// returns the weight for a given initial particle (flavour pdgid0,
  /// etc.) branching into a given final particle at x, and factorisation
  /// scale lnkt.
  ///
  /// \param pdgid0 is the flavour of the pre-branching particle
  /// \param x0 is the x of the pre-branching particle
  /// \param pdf0 is the pdf at x0,pdgid0 (optional, if not supplied then calculated)
  /// \param x is the x of the post-branching particle
  /// \param lnkt is the factorisation scale of the branching (should already include any massive adjustment)
  /// \param massive_quark_damping_factor should be 1 for massless quarks, and 
  ///        typically (muF^2/(m^2+muF^2) otherwise
  double _get_weight(double pdgid0, double x0, Optional<double> pdf0, double x, double lnkt,
                     double massive_quark_damping_factor) const;

  friend class ShowerRunner;

}; // end class ISROverheadFactor
  
inline std::ostream & operator<<(std::ostream & ostr, ISROverheadFactor & isr_overhead) {
  return isr_overhead.output(ostr);
}
  

} // namespace panscales

#endif // __ISROVERHEADFACTOR_HH__
