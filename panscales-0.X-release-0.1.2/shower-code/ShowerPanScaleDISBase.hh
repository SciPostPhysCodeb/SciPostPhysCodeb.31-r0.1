//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __ShowerPanScaleDISBase_HH__
#define __ShowerPanScaleDISBase_HH__
#include "ShowerPanScaleBase.hh"

namespace panscales{
  
//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerPanScaleDISBase
/// base class for the two DIS showers in PanScales
class ShowerPanScaleDISBase : public ShowerPanScaleBase {
public:

  /// default ctor for the Panscale Showers
  ShowerPanScaleDISBase(const QCDinstance & qcd, double beta = 0.5)
    : ShowerPanScaleBase(qcd, beta) {}

  /// virtual dtor
  virtual ~ShowerPanScaleDISBase() {}

  /// meaning of kinematic variables
  std::string name_of_lnv() const override {return "ln(v)";}
  std::string name_of_lnb() const override {return "eta";}

};


/// \class LorentzBoostDIS
/// implementation of Lorentz boost for DIS events (see arXiv:2305.08645)
template <class M>
class LorentzBoostDIS {

public:
  typedef typename M::prec_type T;
  typedef typename M::prec_type prec_type;

  /// constructor
  LorentzBoostDIS(const Momentum & p_ref_in, const Momentum & p_ref_out, 
                  const Momentum & pf, const precision_type & Q2_DIS,  const precision_type & af, const precision_type & bf):
    _p_ref_in(p_ref_in), Q2_DIS_(Q2_DIS){   
    _mult_B = pf.p4() - p_ref_out.p4();
    // multiplies pout - pf = - _mult_B
    _Bmu    = 2 / bf/Q2_DIS_ * p_ref_in.p4();
    assert((p_ref_in.px() == 0 && p_ref_in.py() == 0) && "Current structure can only be used when p_ref_in is aligned with z axis");
    _mult_B_dirdiff = _Bmu.pz() * _mult_B;
    // multiplies pin
    _Amu    = 2 / Q2_DIS_ * _mult_B  - 2 * (pf.m2() - af*Q2_DIS_)/ (bf * pow2(Q2_DIS_)) * _p_ref_in.p4();
  }

  /// returns the application of the boost to the momentum
  M operator*(const M & p) const { 
    return M(p - lambda(p));
  }

  //Lambdamunu = gmunu - lambdamunu
  M lambda(const M & p ) const {
    return - dot_product(p, _Amu) * _p_ref_in.p4() + dot_product(p, _Bmu) * _mult_B;
  }  

  // alternatively the boost can be computed as follows
  // we have p^mu = alpha*p_ref_in + beta*p_ref_out + pT
  // Lambda^munu p_nu = p^mu + p_ref_in * (A.p) + p_ref_out * (B.p) + kperp_f * (C.p)
  // where
  // (A.p) = ( - 2 * kperp_f.m2() / (bf * Q2^2) * p_ref_in + 2 * (bf - 1)/Q2 * p_ref_out + 2/Q2 * kperp_f).(alpha*p_ref_in + beta*p_ref_out + pT)
  //       = ( alpha * (bf - 1) - beta * kperp_f.m2() / (bf * Q2) + 2/Q2 * (kperp_f.pT))
  // (B.p) = beta * (1-bf)/bf 
  // (C.p) = - beta /bf
  // but this needs more operations
  // (alpha, beta, kperp_f.pT and in addition we need to obtain pT from p - alpha*p_ref_in - beta*p_ref_out) 

  /// this function is needed when applying the boost on directional differences
  Momentum3<T> apply_linear_transform_to_dirdiff(const T & mom1_E,
                                                 const MomentumM2<T> & lt_mom1,
                                                 const MomentumM2<T> & lt_mom2,
                                                 const Momentum3<T> & dirdiff_2_minus_1) {
    assert(lt_mom1.m2() == 0);
    assert(lt_mom2.m2() == 0);

    // energy component of the 4-vector diff is zero, because this is the
    // difference between two light-like vectors each of energy 1
    const MomentumM2<T> dirdiff_2_minus_1_4vec(dirdiff_2_minus_1.px(), dirdiff_2_minus_1.py(), dirdiff_2_minus_1.pz(), 0.0);
    MomentumM2<T> lt_dirdiff_2_minus_1_4vec = dirdiff_2_minus_1_4vec
                                              + dot_product(dirdiff_2_minus_1_4vec, _Amu) * _p_ref_in.p4() 
                                              + dirdiff_2_minus_1.pz() * _mult_B_dirdiff;

    // Imagine that we had applied the linear transform to each of 1 & 2, 
    // normalised such that the original lengths are equal to one.
    //
    //     / 2
    //    /
    //    -------- 1
    //
    // after the boost, the energy components of the two would no longer
    // be equal to 1 and would also no longer be equal to each other.
    //
    // to get the direction different we need to rescale such that one of
    // the lengths is equal to 1, and then adjust for the remaining
    // difference in length
    //
    // For now, take dir1 as the reference (it shouldn't matter which one
    // we use, unless one happens to be unusually close to zero)
    
    T lt_E1_norm = lt_mom1.E()/mom1_E;
    // this operation takes us to a 4-mom difference in which 
    // "1" has unit length
    lt_dirdiff_2_minus_1_4vec /= lt_E1_norm;

    // after rescaling, we still have a difference in length between 2 &
    // 1, and that difference in length is equal to the energy component
    // of 2_minus_1.
    T E2_minus_E1 = lt_dirdiff_2_minus_1_4vec.E();

    // the next step is to add something to the 4-vector difference, in the
    // direction of 2 (post branching), such that energy difference goes
    // to zero; so we remove something of length E2_minus_E1, in the
    // direction of lt_mom2
    lt_dirdiff_2_minus_1_4vec -= (E2_minus_E1/lt_mom2.E()) * lt_mom2;

    return Momentum3<T>(lt_dirdiff_2_minus_1_4vec);
  }

private:
  const M _p_ref_in;
  M _mult_B, _mult_B_dirdiff;
  M _Amu, _Bmu;
  T Q2_DIS_;
};

} // namespace panscales

#endif // __ShowerPanScaleDISBase_HH__
