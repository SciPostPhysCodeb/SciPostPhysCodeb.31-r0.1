//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __SHOWERDIPOLEKT_HH__
#define __SHOWERDIPOLEKT_HH__

#include "ShowerBase.hh"

namespace panscales{
/// forward declaration

template <class M> class LorentzBoostII;
//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerDipoleKt
/// a generic kt-ordered dipole shower algorithm with recoil in the dipole centre-of-mass
///
/// This is meant to be a generic kt-ordered dipole shower. This is
/// inspired by Dire (for the variables, maps, recoil-schemes, ...)
/// with tweaks to facilitate its implementation in our generic framework
/// (like using the "standard" splitting functions with a
/// g(eta_dipole), sampling logarithmically the hard-splitting region
/// for ISR, ...)
///
/// The shower variables are
///
///   lnv = ln(kt)       [Dire's log(t)/2]
///   lnb = log( 1-z)    for FSR
///       = log((1-z)/z) for ISR
///
/// with z the longitudinal momentum fraction of the emitter after the
/// emission, i.e. the transmitted momentum fraction. The longitudinal
/// momentum fraction of the radiated particle is therefore 1-z [z->1
/// is the soft limit]
///
/// The kinematic maps are those detailed in arXiv:1506.05057 (DireV1)
/// However, the implementation of the splitting functions is different!
/// 
/// See more details in 2205.02237, 2207.09467 
class ShowerDipoleKt : public ShowerBase {
public:
  /// default ctor
  ///  @param qcd               info about QCD
  ///  @param if_is_global      if true (the default): use global recoil for IF
  ///                           dipoles, otherwise, use a local recoil
  ///  @param compensate_lxmuR  when true, include a compensation term for xmuR
  ///                           scale variations [default: false]
  ShowerDipoleKt(const QCDinstance & qcd, bool if_is_global = true, bool compensate_lxmuR = false)
    : ShowerBase(qcd), _if_is_global(if_is_global), _compensate_lnxmuR(compensate_lxmuR) {}

  /// virtual dtor
  virtual ~ShowerDipoleKt() {}

  // implements the element and splitting info as subclasses (defined later)
  class Element;         //< base class
  class ElementII;       //< element w I emitter and I spectator
  class ElementIF;       //< element w I emitter and F spectator (base class)
  class ElementIFGlobal; //< element w I emitter and F spectator (global recoil)
  class ElementIFLocal;  //< element w I emitter and F spectator (local recoil)
  class ElementFI;       //< element w F emitter and I spectator
  class ElementFF;       //< element w F emitter and F spectator
  
  class EmissionInfo;    //< information about emissions
  /// helper to allocate (and return) a pointer of ShowerDipoleKt::EmissionInfo type
  ShowerBase::EmissionInfo* create_emission_info() const override;

  /// description of the shower
  std::string description() const override {
    if(_if_is_global) {
      return name()+" (generic) shower (PanScales implementation) with global IF recoil";
    } else {
      return name()+" (generic) shower (PanScales implementation) with local IF recoil";
    }
  }
  /// name of the shower
  std::string name() const override {
    return "DipoleKt";
  }
  /// meaning of kinematic variables
  std::string name_of_lnv() const override {return "ln(kt)";}
  std::string name_of_lnb() const override {return "ln(1-z) [FSR] or ln((1-z)/z) [ISR]";}

  /// returns true if the coupling depends not just on kt, but also
  /// on z; for the standard NLL showers, the scale prescription
  /// of 2022-09-11 will introduce a z dependence, because the
  /// the scale cancellation will be made exact only for small z
  bool coupling_depends_on_z() const override {return (qcd().lnxmuR() != 0.0) && _compensate_lnxmuR; }

  /// returns true if the shower involves a second-order piece
  bool has_alphas2_coeff() const override {return (qcd().lnxmuR() != 0.0)  && _compensate_lnxmuR;}
 
  /// this is a dipole shower (2 elements per dipole)
  unsigned int n_elements_per_dipole() const override { return 2; }

  /// allocates unique_ptrs for for the "elements" associated with dipole
  /// index i in the event and returns them as a vector of unique_ptrs
  std::vector<std::unique_ptr<ShowerBase::Element> > elements(Event & event, int dipole_index) const override;

  /// certain simplifications hold if one expects
  /// Element::do_kinematics() to always return true when
  /// accept_prob > 0
  bool do_kinematics_always_true_if_accept() const override {return true;}

  /// the DipoleKt shower is kt-ordered (beta=0)
  double beta() const override {return 0.0;}

  /// this is a dipole-like shower, so only the emitter splits
  bool only_emitter_splits() const override {return true;}

private:
  bool _if_is_global;
  bool _compensate_lnxmuR = false;
};


//--------------------------------------------------------------
/// \class ShowerDipoleKt::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs for the DipoleKt shower algorithm: base class
class ShowerDipoleKt::Element : public ShowerBase::Element {
public:
  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerDipoleKt * shower) :
    ShowerBase::Element(emitter_index, spectator_index, dipole_index, event),
    _shower(shower){
    update_kinematics();
  }

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------
  /// Approximate calculation of the log of the kT of the emission relative
  /// to its parents in their rest frame, in the kappa2->0 & z->1 limits
  /// (eq. 11 of logbook 2019-07-02).
  const double alphas_lnkt(double lnv, double lnb) const override {
    return lnv;
  }

  const double betaPS() const override { return 0.; }

  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  /// the lnb total extent, as a function of lnv, is parametrised
  /// as  const + lnv_deriv * lnv.
  ///
  /// These are defined in derived classes
  /// should be == lnb_generation_range(0).extent()
  virtual double lnb_extent_const() const override = 0;
  virtual double lnb_extent_lnv_coeff() const override {return -2.0;}
  virtual Range lnb_generation_range(double lnv) const override = 0;
  virtual bool has_exact_lnb_range() const override {return false;}
  virtual double lnv_lnb_max_density() const override;
  
  /// The jacobian at a given lnv and lnb [not computed for this shower]
  precision_type dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const override {
    throw std::runtime_error("Tried to access the exact jacobian an element of shower " + _shower->name() + ", which is not implemented");
    return 0.;
  }
  
  /// Checks if the update event step of the shower impacts
  /// the kinematics of the dipole.
  ///
  /// This should never be called since matchign is not implemented
  /// for this shower
  void get_emit_spec_rad_kinematics_for_matching(typename ShowerBase::EmissionInfo * emission_info, Momentum & emit, Momentum & spec, Momentum & rad, bool include_boost = false) const override{
    throw std::runtime_error("Tried to access kinematics for matching of the shower " + _shower->name() + ", for which matching is not implemented");
  }

  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  ///
  /// This fills the channel information as well as cached kinematic
  /// variables in emission_info
  ///
  /// These are implemented in subclasses for each element type
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override=0;

  /// carry out the splitting and update the event; return true if
  /// successful; the RotatedPieces pass on a potentially rotated version
  /// of the emitter, spectator, and perpendicular vectors, and these
  /// are the ones that should be used, not those from emission_info; 
  /// the output is stored back in emission_info (possibly still rotated)
  ///
  /// These are implemented in derived classes
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override = 0;
  
  //----------------------------------------------------
  // caching info to facilitate the work of ShowerRunner
  //----------------------------------------------------
  /// update the element kinematics (called after an emission)
  virtual void update_kinematics() override;
  
  /// update the element indices and kinematics
  void update_indices(unsigned emitter_index, unsigned spectator_index);

  /// for a given lnv,lnb, returns approximate lnkt in the
  /// emitter-spectator dipole rest frame, which is equivalent to the
  /// frame-independent lnkt wrt to the emitter-spectator system.
  /// This lnkt will be correct in the soft+collinear limit but may
  /// differ from the true lnkt in the soft large-angle limit and the
  /// hard-collinear limit
  /// Note that this may not be correct in the small-x region
  virtual double lnkt_approx(double lnv, double lnb) const override {
    return lnv;
  }

  /// for a given lnv,lnb, returns the pseudorapidity with respect to
  /// the closer of the emitter/spectator, defined in the event
  /// centre-of-mass frame. This eta will be correct in the
  /// soft+collinear limit but may differ from the true eta in the
  /// soft large-angle limit and the hard-collinear limit.
  ///
  /// See tests in 2019-07-approx-lnkt-eta/ and
  /// logbook/2019-07-27--approx-lnkt-eta
  virtual double eta_approx(double lnv, double lnb) const override;

  /// returns the lnb for which the eta_approx yields the smallest
  /// value of |eta| --- this corresponds roughly to emissions that
  /// bisect the emitter-spectator pair in the event centre-of-mass
  virtual double lnb_for_min_abseta(double lnv) const override;

  /// returns the coefficient of the term multiplying alphas_MSbar^2,
  /// which is to be added to alphas
  double alphas2_coeff(const typename ShowerBase::EmissionInfo * emission_info) const override;

  /// returns true if using direction differences
  bool use_diffs() const override {return _shower->use_diffs();}

  /// check whether this element is the same as the one passed as
  /// argument. This is helper to debug potential issues.
  virtual bool check_equal_to(const ShowerBase::Element *other_base) const override{
    const typename ShowerDipoleKt::Element & other = *(static_cast<const typename ShowerDipoleKt::Element*>(other_base));
    return (std::abs(other._dipole_m2 - _dipole_m2) < 1e-10);
  }
  
protected:
  precision_type kappa2(precision_type lnv) const {return exp(2*lnv) / _dipole_m2;} 

  // smooth weight function in eta 
  double g(double eta) const;

  const ShowerDipoleKt * _shower;
  precision_type _dipole_m2;
};


//--------------------------------------------------------------
/// \class ShowerDipoleKt::ElementII
/// implementation of the elements for II splittings
class ShowerDipoleKt::ElementII : public ShowerDipoleKt::Element {
public:
  /// full ctor w initialisation
  ElementII(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerDipoleKt * shower) :
    ShowerDipoleKt::Element(emitter_index, spectator_index, dipole_index, event, shower){}

  //--------------------------------------------------
  // generation ranges
  //--------------------------------------------------
  virtual double lnb_extent_const()     const override {
    precision_type xtilde = event().pdf_x(emitter());
    return to_double(log_T(_dipole_m2/xtilde));
  }
  virtual Range lnb_generation_range(double lnv) const override;

  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// carry out the splitting and update the event
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;
  
  /// do the final boost
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

};

//--------------------------------------------------------------
/// \class ShowerDipoleKt::ElementIF
/// implementation of a virtual base for ElementIFGlobal and ElementIFLocal
class ShowerDipoleKt::ElementIF : public ShowerDipoleKt::Element {
public:
  /// full ctor w initialisation
  ElementIF(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerDipoleKt * shower) :
    ShowerDipoleKt::Element(emitter_index, spectator_index, dipole_index, event, shower){}

  //--------------------------------------------------
  // generation ranges 
  //--------------------------------------------------
  virtual double lnb_extent_const()     const override {
    precision_type xtilde = event().pdf_x(emitter());
    return to_double(log_T(_dipole_m2/xtilde));
  }
  virtual Range lnb_generation_range(double lnv) const override;

  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// this needs to be overwritten
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override = 0;

};

//--------------------------------------------------------------
/// \class ShowerDipoleKt::ElementIFGlobal
/// implementation of the elements for IF splittings with global recoil
class ShowerDipoleKt::ElementIFGlobal : public ShowerDipoleKt::ElementIF {
public:
  /// full ctor w initialisation
  ElementIFGlobal(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerDipoleKt * shower) :
    ShowerDipoleKt::ElementIF(emitter_index, spectator_index, dipole_index, event, shower){}

  /// carry out the splitting and update the event
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;

  /// do the final boost
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

};

//--------------------------------------------------------------
/// \class ShowerDipoleKt::ElementIFLocal
/// implementation of the elements for IF splittings with global recoil
class ShowerDipoleKt::ElementIFLocal : public ShowerDipoleKt::ElementIF {
public:
  /// full ctor w initialisation
  ElementIFLocal(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerDipoleKt * shower) :
    ShowerDipoleKt::ElementIF(emitter_index, spectator_index, dipole_index, event, shower){}

  /// carry out the splitting and update the event
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;

  /// do the final boost
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

};

//--------------------------------------------------------------
/// \class ShowerDipoleKt::ElementFI
/// implementation of the elements for FI splittings    
class ShowerDipoleKt::ElementFI : public ShowerDipoleKt::Element {
public:
  /// full ctor w initialisation
  ElementFI(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerDipoleKt * shower) :
    ShowerDipoleKt::Element(emitter_index, spectator_index, dipole_index, event, shower){}

  //--------------------------------------------------
  // generation ranges
  //--------------------------------------------------
  virtual double lnb_extent_const()     const override {
    return to_double(log_T(_dipole_m2));
  }
  virtual Range lnb_generation_range(double lnv) const override;

  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// carry out the splitting and update the event
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;

  /// do the final boost
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

};

//--------------------------------------------------------------
/// \class ShowerDipoleKt::ElementFF
/// implementation of the elements for FF splittings   
class ShowerDipoleKt::ElementFF : public ShowerDipoleKt::Element {
public:
  /// full ctor w initialisation
  ElementFF(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerDipoleKt * shower) :
    ShowerDipoleKt::Element(emitter_index, spectator_index, dipole_index, event, shower){}

  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  /// the lnb total extent, as a function of lnv, is parameterised
  /// as  const + lnv_deriv * lnv.
  virtual double lnb_extent_const()     const override {
    return to_double(log_T(_dipole_m2));  // should be == lnb_generation_range(0).extent()
  }

  virtual Range lnb_generation_range(double lnv) const override;

  virtual bool has_exact_lnb_range() const override {return true;}

  /// exact rage available for the FF case
  virtual Range lnb_exact_range(double lnv) const override;

  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// carry out the splitting and update the event
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;
 
  /// update the event (call to base class)
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

};


//--------------------------------------------------------------
/// \class ShowerDipoleKt::EmissionInfo
/// emission information specific to the DipoleKt shower
class ShowerDipoleKt::EmissionInfo : public ShowerBase::EmissionInfo {
public:
  /// kinematic map variables
  precision_type omz, z, k2;
  // for boost
  Momentum Qout, Qsum, Qin, Qout_minus_Qin; 
  precision_type Qsum2, Qout_minus_Qin_dot_Qout;
};

//--------------------------------------------------------------
/// \class LorentzBoostII
// class to implement a Lorentz Boost on II dipoles.
// helper to easily implement the action
// of the boost on the direction differences
template <class M>
class LorentzBoostII {
public:

  typedef typename M::prec_type T;
  typedef typename M::prec_type prec_type;

  /// constructor
  LorentzBoostII(const typename ShowerDipoleKt::EmissionInfo & emission_info, const T & dipole_m2, const M & Qin): 
    Qout_minus_Qin_(emission_info.Qout_minus_Qin), 
    Qin_(Qin), 
    Qout_(emission_info.Qout), 
    Qsum_(emission_info.Qsum), 
    Qin2_(dipole_m2), 
    Qsum2_(emission_info.Qsum2), 
    Qout_minus_Qin_dot_Qout_(emission_info.Qout_minus_Qin_dot_Qout)
    {}

  /// returns the application of the boost to the momentum
  M operator*(const M & p) const { 
    return M(p - lambda(p));
  }

  /// returns Lorentz trasformation: Lambdamunu = gmunu - lambdamunu. Eq. (B7) in arXiv:2205.02237
  M lambda(const M & p ) const {
    // see logbook dir_diffs on the derivation of this
    precision_type fac1 = 4 / Qin2_ / Qsum2_ * (dot_product(Qin_, p) * Qout_minus_Qin_dot_Qout_ + dot_product(Qout_minus_Qin_, p) * Qin2_); 
    precision_type fac2 = - 2*dot_product(Qsum_, p)/ Qsum2_;

    return M(fac1 * Qout_ + fac2 * Qout_minus_Qin_);
  }
  
private:
  M Qout_minus_Qin_, Qin_, Qout_, Qsum_;
  T Qin2_, Qsum2_, Qout_minus_Qin_dot_Qout_;

};

//--------------------------------------------------------------
/// \class LorentzBoostIF
/// class to implement a Lorentz Boost on IF dipoles when the global
/// recoil scheme is selected. 
template <class M>
class LorentzBoostIF { 
public:

  typedef typename M::prec_type T;
  typedef typename M::prec_type prec_type;

  /// constructor
  LorentzBoostIF(const M & pa, const M & pA, const M & pB) 
  : _pB(pB) {
    _daB     = dot_product(pa, pB);
    _pa_perp = M(pa.px(),pa.py(),0,0);
    _paT2    = _pa_perp.m2();
  }

  /// returns the application of the boost to the momentum
  M operator*(const M & p) const {
    precision_type dB = dot_product(p, _pB);
    precision_type dperp = dot_product(p, _pa_perp);
    return M(p - dB/_daB * _pa_perp + (dperp - dB*_paT2/2./_daB)/_daB * _pB);
  }

  /// returns Lorentz trasformation: Lambdamunu = gmunu - lambdamunu. Eq. (B15) in arXiv:2205.02237
  M lambda(const M & p ) const {
    precision_type dB = dot_product(p, _pB);
    precision_type dperp = dot_product(p, _pa_perp);
    return M( dB/_daB * _pa_perp - (dperp - dB*_paT2/2./_daB)/_daB * _pB);
  }

private:
  M _pB, _pa_perp;
  T _daB, _paT2;
};
  
} // namespace panscales

#endif // __SHOWERDIPOLEKT_HH__

