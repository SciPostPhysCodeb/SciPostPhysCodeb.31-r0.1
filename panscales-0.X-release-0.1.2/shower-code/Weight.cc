#include <exception>
#include <limits>
#include "Weight.hh"

namespace panscales{

  // ctor for exact cases
  Weight::Weight(Exact exact) : _exact(exact) {
    if      (exact == exact_one)  _value = 1.0;
    else if (exact == exact_zero) _value = 0.0;
    else
      throw std::runtime_error("Weight::Weight(Exact) : can only take exact_one or exact_zero");
  }

  // multiplication by another weight
  Weight& Weight::operator*=(const Weight& other) {
    // if either weight is exact 0, return an exact 0
    if (_exact == exact_zero || other._exact == exact_zero)
      return (*this) = Weight(exact_zero);

    // if a weight is an exact one, return the other
    if (      _exact == exact_one) return (*this) = other;
    if (other._exact == exact_one) return  *this;

    // otherwise, take a product of the weights
    return (*this) = Weight(_value * other._value);
  }
  Weight Weight::operator*(const Weight& other) const {
    Weight w = (*this);
    return w*=other;
  }
  
  // multiplication by a double
  Weight& Weight::operator*=(double other){
    // if the local weight is an exact 0, return an exact 0
    if (_exact == exact_zero){ return (*this) = Weight(exact_zero); }

    // otherwise, return a standard product (no notion of an exact 1 here)
    return (*this) = Weight(_value * other);
  }
   
  Weight Weight::operator*(double other) const {
    Weight w = (*this);
    return w*=other;
  }

  // division by another weight
  Weight Weight::operator/(const Weight & other) const {
    // first treat the case where the the local value is an exact 0
    if (_exact == exact_zero){
      if (other._exact == exact_zero)
        return Weight(std::numeric_limits<double>::quiet_NaN());        
      return Weight(exact_zero);
    }
    
    // then the case where the other value is an exact 1
    if (other._exact == exact_one) return *this;

    // otherwise, take a standard division (1/0 should return inf)
    return Weight(_value / other._value);
  }

  // division by a double
  Weight Weight::operator/(double other) const {
    // if the local weight is an exact 0, return an exact 0
    if (_exact == exact_zero){
      if (other == 0)
        return Weight(std::numeric_limits<double>::quiet_NaN());        
      return Weight(exact_zero);
    }

    // otherwise, return a standard division (no notion of an exact 1 here)
    return Weight(_value / other);
  }


  Weight operator*(double a, const Weight &w){
    return w*a;
  }
}
#include "autogen/auto_Weight_global-cc.hh"
