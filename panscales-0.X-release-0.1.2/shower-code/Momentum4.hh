//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __MOMENTUM4_HH__
#define __MOMENTUM4_HH__
#include <cassert>
#include "Momentum3.hh"

namespace panscales{

//----------------------------------------------------------------------
/// \class Momentum4
/// Class containing a four vector. Heavily drawn from FastJet's PseudoJet

template <class T>
class Momentum4 : public Momentum3<T> {
public:

  static bool verbose_;
  static void set_verbose (bool v);
  static bool verbose();

  // the following lines are needed so that we can use the corresponding
  // parent class's members (specific to templated derived classes)
  using Momentum3<T>::px_;
  using Momentum3<T>::py_;
  using Momentum3<T>::pz_;
  using Momentum3<T>::px;
  using Momentum3<T>::py;
  using Momentum3<T>::pz;
  using Momentum3<T>::pt;
  using Momentum3<T>::pt2;
  using Momentum3<T>::modpsq;
  using Momentum3<T>::modp;

  /// default constructor
  Momentum4() : Momentum3<T>(), E_(0.0) {}
  /// constructor from individual components
  Momentum4(T px_in, T py_in, T pz_in, T E_in)
    : Momentum3<T>(px_in,py_in,pz_in), E_(E_in) {}

  /// constructor from individual 3-momentum and energy
  Momentum4(const Momentum3<T> & p3_in, T E_in) : Momentum3<T>(p3_in), E_(E_in) {}

  /// construct the 4-momentum and, if PANSCALES_MOMENTUM_CHECKS is
  /// defined, verify the consistency with the supplied m2.
  Momentum4(T px_in, T py_in, T pz_in, T E_in, T m2_in)
    : Momentum4(px_in, py_in, pz_in, E_in) {
    register_true_m2(m2_in);
  }
  /// construct a 4-momentum from a 3-vector and a mass
  static Momentum4 fromP3M2(const Momentum3<T> & p3_in, T m2_in) {
    return Momentum4(p3_in.px(), p3_in.py(), p3_in.pz(), sqrt(p3_in.modpsq()+m2_in));
  }
  
  const Momentum4 & p4() const {return *this;}
  
  /// mass squared
  T m2() const {return E_*E_ - px_*px_ - py_*py_ - pz_*pz_;}

  /// mass
  T m() const {
    T mm = m2();
    return mm < 0.0 ? -sqrt(-mm) : sqrt(mm);
  }

  /// rapidity
  T rap() const;

  /// returns azimuthal angle phi in the range 0 <= phi < 2pi
  T phi() const;

  /// energy
  T E () const {return E_;}

  /// light-cone variables
  T p_plus()  const {return E_ + pz_;}
  T p_minus() const {return E_ - pz_;}

  /// return a reference to the underlying 3-momentum
  const Momentum3<T> & mom3() const {return *this;}

  /// multiplication by T
  Momentum4<T> & operator*=(T);

  /// division by T
  Momentum4<T> & operator/=(T);

  /// addition with momentum
  Momentum4<T> & operator+=(const Momentum4<T> &);

  /// subtraction with momentum
  Momentum4<T> & operator-=(const Momentum4<T> &);

  /// reset momentum
  void reset_momentum(const Momentum4<T> &);
  
  /// reset momentum
  void reset_momentum(T, T, T, T);

  /// for internal use: checks that the actual squared mass from
  /// the 4-vector components is consistent with the supplied m2_in
  inline void internal_m2_check(T m2_in) const {
#ifdef PANSCALES_MOMENTUM_CHECKS
    T modp2E2 = pow2(px_) + pow2(py_) + pow2(pz_) + pow2(E_);
    T diff = m2() - m2_in;
    bool internal_m2_check_result = fabs(diff) 
            <= numeric_limit_extras<T>::sqrt_epsilon() * modp2E2;
    if (!internal_m2_check_result){
      std::cout << "  m2 internal function (from 4-vector)= " << m2() 
                << "  m2_in for check = " << m2_in 
                << "  diff = " << diff 
                << "  threshold = " << numeric_limit_extras<T>::sqrt_epsilon() * modp2E2
                << "  4vector = " << *this
                << std::endl;
      std::cout << "About to abort" << std::endl;
    }
    assert(internal_m2_check_result);
#endif  
  }


  /// this function is a dummy, unless PANSCALES_MOMENTUM_CHECKS is
  /// defined, in which case it checks that the m2 value being
  /// registered coincides (to within accuracy) with the
  /// true squared mass. 
  inline void register_true_m2(T m2_in) {
    internal_m2_check(m2_in);
  }

  /// Registers the true value of the mass and fixes the energy
  /// accordingly to avoid accumulated rounding errors.
  /// Should only be used for m2 >= 0. 
  void register_true_m2_fix_E(T m2) {
    assert (m2>= 0);
    internal_m2_check(m2);
    // question of whether to place this before / after test depends
    // on whether you want to test user input, or our algebra.
    E_ = sqrt(modpsq() + m2);
  }


  /// transform this jet (given in the rest frame of prest) into a jet
  /// in the lab frame
  Momentum4<T> & boost(const Momentum4<T> & prest);

  /// transform this jet (given in lab) into a jet in the rest
  /// frame of prest
  Momentum4<T> & unboost(const Momentum4<T> & prest);

  T operator[](unsigned i) const {
    if      (i == 0) {return px_;}
    else if (i == 1) {return py_;}
    else if (i == 2) {return pz_;}
    else if (i == 3) {return E_;}
    else {assert(false && "i must be in range 0..3");}
  }

  /// returns a copy of the momentum with reversed spatial components
  Momentum4<T> reversed() const {
    return Momentum4<T>(-px_, -py_, -pz_, E_);
  }


protected:
  /// four momentum components
  T E_;

  /// maximum rapidity value
  // #TRK_ISSUE-800
#if defined(PSNOCONSTEXPR)
  static constexpr double maxrap_ = 1e5;
#else
  static constexpr T maxrap_ = 1e5;
#endif
};

template <class T>
Momentum4<T> operator*(T, const Momentum4<T> &);

template <class T>
Momentum4<T> operator/(const Momentum4<T> &, T);

template <class T>
Momentum4<T> operator+(const Momentum4<T> &, const Momentum4<T> &);

template <class T>
Momentum4<T> operator-(const Momentum4<T> &, const Momentum4<T> &);


/// returns the 4-vector dot product of a and b
template <class T>
inline T dot_product(const Momentum4<T> & a, const Momentum4<T> & b) {
  return a.E()*b.E() - a.px()*b.px() - a.py()*b.py() - a.pz()*b.pz();
}

//----------------------------------------------------------------------
/// output a momentum
template <class T>
inline std::ostream & operator<<(std::ostream & ostr, const Momentum4<T> & p) {
  ostr << std::setw(13) << p.px() << " "
       << std::setw(13) << p.py() << " "
       << std::setw(13) << p.pz() << " "
       << std::setw(13) << p.E()  << " "
       << " pt = " << std::setw(11) << p.pt()
       << " y  = " << std::setw(11) << p.rap()
       << " phi= " << std::setw(11) << p.phi()
       << " m2 = " << std::setw(11) << p.m2();
  return ostr;
}

//----------------------------------------------------------------------
/// Returns the 3-vector dot-product of p1 and p2.
template <class T>
T dot3(const Momentum4<T> & p1, const Momentum4<T> & p2);

//----------------------------------------------------------------------
/// Returns the 3-vector cross-product of p1 and p2. If lightlike is false
/// then the energy component is zero; if it's true the the energy component
/// is arranged so that the vector is lightlike
template <class T>
Momentum4<T> cross(const Momentum4<T> & p1, const Momentum4<T> & p2, bool lightlike = false);

//----------------------------------------------------------------------
/// Returns two 4-vectors, each with square = -1, that have a zero
/// (4-vector) dot-product with dir1 and dir2.
///
/// When dir1 and dir2 form a plane, then perp1 will be out of that
/// plane and perp2 will be in the plane.
template <class T>
[[deprecated("use MomentumM2's two_perp instead")]]
void twoPerp(const Momentum4<T> & dir1, const Momentum4<T> & dir2,
             Momentum4<T> & perp1, Momentum4<T> & perp2);

// angular distance for Durham algorithm in e+e-
template <class T>
T one_minus_costheta(const Momentum4<T> & p1, const Momentum4<T> & p2);

} // namespace panscales
  
#endif //  __MOMENTUM4_HH__
