//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __SHOWERTOYDIRE_HH__
#define __SHOWERTOYDIRE_HH__

#include "ShowerBase.hh"

namespace panscales{
  
//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerToyDire
/// Implementation of the final-state Dire shower presented in arXiv:1506.05057.
/// The splitting functions, kinematic map and evolution variable are as in 
/// the original paper. This shower shares the kinematic map and evolution
/// variable with ShowerDipoleKt::ElementFF. However, two differences exist:
///   - ShowerToyDire uses Dire splitting functions 
///   - ShowerToyDire does not include a g(eta_dipole) in the acceptance probability
/// thus, the final-state evolution in both showers is not binary identical. 

class ShowerToyDire : public ShowerBase {
public:
  /// default ctor
  ShowerToyDire(const QCDinstance & qcd) : ShowerBase(qcd) {}

  /// virtual dtor
  virtual ~ShowerToyDire() {}

  /// implements the element and splitting info as subclasses
  class Element;    //< base class
  class EmissionInfo;
  virtual ShowerBase::EmissionInfo* create_emission_info() const override;

  /// description of the shower
  std::string description() const override {return name() + " shower (PanScales implementation)";}
  /// name of the shower
  std::string name() const override{return "toydire-ee";}

  /// meaning of kinematic variables
  std::string name_of_lnv() const override {return "0.5*ln(t)";}
  std::string name_of_lnb() const override {return "ln(1-z)";}
 
  /// this is a dipole shower (2 elements per dipole)
  unsigned int n_elements_per_dipole() const override { return 2; }

  /// returns true if an emission happens at a phase-space point that has
  /// already been covered by the matching shower (used in "truncated" showers)
  virtual bool veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const override;

  /// element store
  virtual std::vector<std::unique_ptr<ShowerBase::Element> > elements(Event & event, int dipole_index) const override;

  /// the Dire shower is kt-ordered (beta=0)
  double beta() const override {return 0.0;}

  /// this is a dipole-like shower, so only the emitter splits
  bool only_emitter_splits() const override {return true;}

};


//--------------------------------------------------------------
/// \class ShowerToyDire::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs for the Dire shower algorithm
class ShowerToyDire::Element : public ShowerBase::Element {
public:
  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerToyDire * shower) :
    ShowerBase::Element(emitter_index, spectator_index, dipole_index, event),
    _shower(shower){
    update_kinematics();
  }

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------
  /// Approximate calculation of the log of the kT of the emission relative
  /// to its parents in their rest frame, in the kappa2->0 & z->1 limits
  /// (eq. 11 of logbook 2019-07-02).
  const double alphas_lnkt(double lnv, double lnb) const override {
    return lnv;
  }

  const double betaPS() const override { return 0.; }

  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  /// the lnb total extent, as a function of lnv, is parametrised
  /// as  const + lnv_deriv * lnv.
  virtual double lnb_extent_const()     const override {
    return to_double(log_T(_dipole_m2));
  }

  virtual double lnb_extent_lnv_coeff() const override {return -2.0;}
  virtual Range lnb_generation_range(double lnv) const override;
  //GS-NOTE: shoud the bootom one be false? [w Ludo & Rob]
  virtual bool has_exact_lnb_range() const override {return true;}
  virtual double lnv_lnb_max_density() const override;
  /// jacobian for this shower is not computed
  precision_type dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const override {
    throw std::runtime_error("Tried to access the exact jacobian an element of shower " + _shower->name() + ", which is not implemented");
    return 0.;
  }


  //--------------------------------------------------
  // important definitions to be implemented in all derived classes
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  ///
  /// This fills the channel information as well as cached kinematic
  /// variables in emission_info
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// carry out the splitting and update the event; return true if
  /// successful; the RotatePieces pass on a potentially rotated version
  /// of the emitter, spectator, and perpendicular vectors, and these
  /// are the ones that should be used, not those from emission_info; 
  /// the output is stored back in emission_info (possibly still rotated)
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;
  
  //----------------------------------------------------
  // caching info to facilitate the work of ShowerRunner
  //----------------------------------------------------
  /// update the element kinematics (called after an emission)
  virtual void update_kinematics() override;
  
  /// update the element indices and kinematics
  void update_indices(unsigned emitter_index, unsigned spectator_index);

  /// for a given lnv,lnb, returns approximate lnkt in the
  /// emitter-spectator dipole rest frame, which is equivalent to the
  /// frame-independent lnkt wrt to the emitter-spectator system.
  /// This lnkt will be correct in the soft+collinear limit but may
  /// differ from the true lnkt in the soft large-angle limit and the
  /// hard-collinear limit
  virtual double lnkt_approx(double lnv, double lnb) const override {
    return lnv;
  }

  /// for a given lnv,lnb, returns the pseudorapidity with respect to
  /// the closer of the emitter/spectator, defined in the event
  /// centre-of-mass frame. This eta will be correct in the
  /// soft+collinear limit but may differ from the true eta in the
  /// soft large-angle limit and the hard-collinear limit.
  ///
  /// See tests in 2019-07-approx-lnkt-eta/ and
  /// logbook/2019-07-27--approx-lnkt-eta
  virtual double eta_approx(double lnv, double lnb) const override;

  virtual double lnb_for_min_abseta(double lnv) const override;

  //--------------------------------------------------
  // Dire's implementation of splitting functions
  //--------------------------------------------------
  
  void fill_dglap_splitting_weights(const Particle &p,
                                    const precision_type &z,
                                    const precision_type &omz,
                                    const precision_type &k2,
                                    double & weight_rad_g,
                                    double & weight_rad_q) const;
  
  /// Return the value of the splitting function, including coupling
  /// and colour factors. 
  ///
  /// z and k2 correspond to z and kappa2 in Eq.(2.20a-c) of our arxiv:1805.09327
  ///
  /// Note that a quick check seems to indicate that the range of CA
  /// and nf values which is suited for the other showers (i.e. are
  /// such that the full splitting is smaller than the soft limit) is
  /// also valid for the Dire splittings below
  ///
  /// All the splitting functions are normalised by 1/CA since colour
  /// factors are handled separately in our code
  precision_type Pq2qg_normalised       (precision_type z, precision_type omz, precision_type k2) const;
  precision_type halfPg2gg_normalised   (precision_type z, precision_type omz, precision_type k2) const;
  precision_type halfPg2qqbar_normalised(precision_type z, precision_type omz, precision_type k2) const;
  precision_type g2qqbar_fraction(precision_type z, precision_type omz, precision_type k2) const;

  bool use_diffs() const override {return _shower->use_diffs();}

  bool double_soft() const override {return _shower-> double_soft();}

protected:
  precision_type kappa2(precision_type lnv) const {return exp(2*lnv) / _dipole_m2;}

  // smooth weight function in eta
  double g(double eta) const;

  const ShowerToyDire * _shower;
  precision_type _dipole_m2;
};

//--------------------------------------------------------------
/// \class ShowerToyDire::EmissionInfo
/// emission information specific to the Dire shower
class ShowerToyDire::EmissionInfo : public ShowerBase::EmissionInfo {
public:
  /// kinematic map variables
  precision_type omz, z, k2;
};
 

} // namespace panscales

#endif // __SHOWERTOYDIRE_HH__
