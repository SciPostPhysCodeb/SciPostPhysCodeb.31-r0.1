#!/usr/bin/env python3
"""
 Script to help validate showers against a set of reference results
 (specifically to make sure, e.g., that when refactoring code we haven't
 accidentally changed any results)
"""

# Current configurations
#
# Common to all showers: 
# - default alphas: 0.1 (lnvmin=-14 in ee, -13 in dis and pp),
#   alternative 0.02 (lnvmin=-22)
# - for dis and pp: defaule rts=5, alternative=100
# - colour: NODS by default
#           alt1: segment
#           alt2:

# Shower-dependent:
# - matching: (default in [])
#                 None  Mult   HEG     aMC@NLO
#   PG(sdf,0)      ok   [ok]  powheg     ok
#   PG(0)          ok   [ok]  powheg     ok
#   PG(0.5)        ok   [ok]  powheg     ok
#   PL(dip,0.5)    ok   [ok]  PG(0.5)    ok
#   PL(ant,0.5)    ok        [PG(0.5)]   ok
#   Toye          [ok]
#   ToyPythia8    [ok]

# command line arguments:
#  - values:
#     default ee:     -alphas 0.1  -lnvmin -14
#     default dis/pp: -alphas 0.1  -lnvmin -13 -rts 5
#     alt1:           -alphas 0.02 -lnvmin -22 -rts 5
#     alt2 (dis/pp):  -alphas 0.1  -lnvmin -13 -rts 100
#  - colour :
import subprocess
import os
import re
import argparse
import datetime
import sys
sys.path.append('../../../scripts/')
from JobRunner import *

lengths = {
  'short':  {'nev' : '4e3'},
  'medium': {'nev' : '1e5'},
  'long':   {'nev' : '4e6'}
}

# default tags for teh various colliders
#
#TODO: discuss which lnvmin/ktcuts
targs={'ee' : {'default'    : {'tag' : '-alphas_0.1_lnvmin_-14',         'args' : '-alphas 0.1  -lnvmin -13          -lnkt-cutoff  -7 -nloops 2 -do-frag'},
               'altalphas'  : {'tag' : '-alphas_0.02_lnvmin_-22',        'args' : '-alphas 0.02 -lnvmin -22                           -nloops 0 -do-frag'}},
       'dis' : {'default'   : {'tag' : '-alphas_0.1_lnvmin_-13_rts_5',   'args' : '-alphas 0.1  -lnvmin -13 -rts 5   -lnkt-cutoff  -7 -nloops 2 -pdf-lnQref 0 -hoppet-Qmax 1e7'},
                'altrts'    : {'tag' : '-alphas_0.1_lnvmin_-13_rts_100', 'args' : '-alphas 0.1  -lnvmin -13 -rts 100 -lnkt-cutoff  -7 -nloops 2 -pdf-lnQref 0 -hoppet-Qmax 1e7'}},
       'pp'  : {'default'   : {'tag' : '-alphas_0.1_lnvmin_-13_rts_5',   'args' : '-alphas 0.1  -lnvmin -13 -rts 5   -lnkt-cutoff  -7 -nloops 2 -pdf-lnQref 0 -hoppet-Qmax 1e7'},
                'altalphas' : {'tag' : '-alphas_0.02_lnvmin_-22',        'args' : '-alphas 0.02 -lnvmin -22 -rts 5                    -nloops 0 -pdf-lnQref 0 -hoppet-Qmax 1e7'},
                'altrts'    : {'tag' : '-alphas_0.1_lnvmin_-13_rts_100', 'args' : '-alphas 0.1  -lnvmin -13 -rts 100 -lnkt-cutoff  -7 -nloops 2 -pdf-lnQref 0 -hoppet-Qmax 1e7'}}}

# shower-specific configuartions (supported collider, matching schemes, physics features 
#   panglobal-beta0-sdf panglobal-beta0 panglobal-beta0.5 panlocal-beta0.5 panlocal-vincia-beta0.5 toydire toypythia8 dipole-kt dipole-kt-local
showers={'panglobal-beta0-sdf' : {'args-shower'     : '-shower panglobal -beta 0.0 -split-dipole-frame',
                                  'has-ee'          : True,
                                  'has-dis'         : False,
                                  'has-pp'          : False,
                                  'has-double-soft' : True,
                                  'matchings'       : {'-3jmult'       : '-3-jet-matching',
                                                       '-3jheg_powheg' : '-3-jet-matching -matching-shower powheg',
                                                       '-3jamcatnlo'   : '-3-jet-matching -mcatnlo-matching'},
                                  },
         'panglobal-beta0'     : {'args-shower'     : '-shower panglobal -beta 0.0 ',
                                  'has-ee'          : True,
                                  'has-dis'         : True,
                                  'has-pp'          : True,
                                  'has-double-soft' : True,
                                  'matchings'       : {'-3jmult'       : '-3-jet-matching',
                                                       '-3jheg_powheg' : '-3-jet-matching -matching-shower powheg',
                                                       '-3jamcatnlo'   : '-3-jet-matching -mcatnlo-matching'},
                                  },
         'panglobal-beta0.5'   : {'args-shower'     : '-shower panglobal -beta 0.5 ',
                                  'has-ee'          : True,
                                  'has-dis'         : True,
                                  'has-pp'          : True,
                                  'has-double-soft' : True,
                                  'matchings'       : {'-3jmult'       : '-3-jet-matching',
                                                       '-3jheg_powheg' : '-3-jet-matching -matching-shower powheg',
                                                       '-3jamcatnlo'   : '-3-jet-matching -mcatnlo-matching'},
                                  },
         'panlocal-beta0.5'    : {'args-shower'     : '-shower panlocal -beta 0.5 ',
                                  'has-ee'          : True,
                                  'has-dis'         : True,
                                  'has-pp'          : True,
                                  'has-double-soft' : False,
                                  'matchings'       : {'-3jmult'       : '-3-jet-matching',
                                                       '-3jheg_powheg' : '-3-jet-matching -matching-shower powheg',
                                                       '-3jamcatnlo'   : '-3-jet-matching -mcatnlo-matching'},
                                  },
         'panlocal-vincia-beta0.5' : {'args-shower'     : '-shower panlocal-vincia -beta 0.5 ',
                                      'has-ee'          : True,
                                      'has-dis'         : False,
                                      'has-pp'          : True,
                                      'has-double-soft' : False,
                                      'matchings'       : {'-3jheg_panglobal' : '-3-jet-matching -matching-shower panglobal',
                                                           '-3jamcatnlo'      : '-3-jet-matching -mcatnlo-matching'},
                                      },
         'toydire'             : {'args-shower'     : '-shower toydire ',
                                  'has-ee'          : True,
                                  'has-dis'         : False,
                                  'has-pp'          : False,
                                  'has-double-soft' : False,
                                  'matchings'       : {},
                                  'light'           : True,
                                  },
         'toypythia8'          : {'args-shower'     : '-shower toypythia8 ',
                                  'has-ee'          : True,
                                  'has-dis'         : False,
                                  'has-pp'          : False,
                                  'has-double-soft' : False,
                                  'matchings'       : {},
                                  'light'           : True,
                                  },
         'dipole-kt'           : {'args-shower'     : '-shower dipole-kt ',
                                  'has-ee'          : False,
                                  'has-dis'         : False,
                                  'has-pp'          : True,
                                  'has-double-soft' : False,
                                  'matchings'       : {},
                                  'light'           : True,
                                  },
         'dipole-kt-local'     : {'args-shower'     : '-shower dipole-kt -if-is-local ',
                                  'has-ee'          : False,
                                  'has-dis'         : True,
                                  'has-pp'          : True,
                                  'has-double-soft' : False,
                                  'matchings'       : {},
                                  'light'           : True,
                                  },
         }

# list of processes for each collider (some tests will only use the first)
processes={'ee'  : {'Z' : {'tag' : '-ee2Z', 'arg' : '-process ee2qq'     },
                    'H' : {'tag' : '-ee2H', 'arg' : '-process ee2gg'     }},
           'dis' : {'Z' : {'tag' : ''     , 'arg' : '-process DIS -Q2 1' }},
           'pp'  : {'Z' : {'tag' : '-pp2Z', 'arg' : '-process pp2Z -mZ 1'},
                    'H' : {'tag' : '-pp2H', 'arg' : '-process pp2H -mH 1'}}}

# specific command-line arguments
spin_on  = {'tag' : '-spin'  , 'args' : ""}
spin_off = {'tag' : '',        'args' : "-no-spin-corr"}

colour_largeNc = {'tag' : '-cfhalfca', 'args' : '-colour CFHalfCA'}
colour_segment = {'tag' : '-segment' , 'args' : '-colour Segment'}
colour_NODS    = {'tag' : '-nods'    , 'args' : '-colour NODS'}

double_soft_on  = {'tag' : '-ds', 'args' : "-double-soft"}   
#double_soft_off = {'tag' : ''   , 'args' : ""}

# number of events per run
#outdir='refruns'

#TODO: add fixed-order runs
#TODO: add collinear spin projection

#--------------------------------------------------------------------------------
def main():
    # get command line arguments
    parse_command_line()
    print(args)

    # make sure the code is built
    build_all()

    # generate the list of commands
    global commands
    commands = build_command_list()

    # run
    run_jobs()

    
#--------------------------------------------------------------------------------
def parse_command_line():
    # this script is meant to be run from the directory in which it
    # lives; so move into that directory if run from elsewhere
    script_path = os.path.dirname(os.path.abspath( __file__ ))
    os.chdir(script_path)
    
    # parse the command line
    parser = argparse.ArgumentParser()
    parser.add_argument('-l','--length',
                        choices=lengths.keys(), 
                        default='vshort',
                        help='vshort=4e3 events, short=4e4, medium=4e5, long=4e6')
    
    shower_choices = list(showers.keys())
    shower_choices.append('all')
    parser.add_argument('--shower',
                        choices=shower_choices,
                        default='all',
                        help='Determine the shower to generate reference files for')      
    
    parser.add_argument("-n",'--dry-run', action="store_true",
                        help="print commands but do not run them")
    
    parser.add_argument('--no-build', action='store_true',
                        help='do not try to build (helpful for batch runs where we build once and for all before but USE WITH CARE!!!)')
    
    
    # include the date in the default staging directory
    default_staging = 'staging/' + datetime.datetime.now().strftime('%Y-%m-%d') + "/"
    parser.add_argument("-d", '--staging-dir', default=default_staging, help="directory to stage output in")

    global runner
    runner = Runner(parser)
    
    global args
    args = parser.parse_args()

    


#----------------------------------------------------------------------
def build_command_list():
    global outdir
    outdir=f'{args.staging_dir}/refruns_{args.length}'
    nev   =lengths[args.length]["nev"]

    commands=[]
    
    included_showers = showers.keys() if args.shower=="all" else [args.shower]
        
    for collider in ['ee', 'pp']:
        procs = processes[collider]
        proc0 = next(iter(procs))
        
        base_collider_command=f'../build-double/example-framework -rseq 1 -nev {nev} '

        tgs = targs[collider]

        for showername in included_showers:
            info = showers[showername]
            if not info['has-'+collider]: continue

            base_command=f'{base_collider_command} {info["args-shower"]} '

            all_on_base=f'{base_command} {spin_on["args"]} {colour_NODS["args"]}'
            # for ee, add matching and double-soft
            if collider=='ee':
                if len(info['matchings'].keys())>0:
                    default_matching=next(iter(info['matchings'].keys()))
                    default_matching_args=info['matchings'][default_matching]
                    all_on_base=f'{all_on_base} {default_matching_args}'
                if info['has-double-soft'] and collider=='ee':
                    # WATCH OUT: for now, disable spin for double-soft
                    all_on_base=f'{all_on_base} {spin_off["args"]} {double_soft_on["args"]}'
                
            #  1. all-on: everything on [Z,H]
            for proc in procs:
                commands.append(f'{all_on_base} {tgs["default"]["args"]} {procs[proc]["arg"]} -o {outdir}/{collider}-{showername}{procs[proc]["tag"]}{tgs["default"]["tag"]}-allon.dat')

            if "light" in info.keys() and info["light"]:
                continue
    
            #  2. different alphas (all on) [Z]
            if "altalphas" in tgs.keys():
                commands.append(f'{all_on_base} {tgs["altalphas"]["args"]} {procs[proc0]["arg"]} -o {outdir}/{collider}-{showername}{procs[proc0]["tag"]}{tgs["altalphas"]["tag"]}-allon.dat')

            #  2b. For DIS and pp: different rts [Z]
            if "altrts" in tgs.keys():
                commands.append(f'{all_on_base} {tgs["altrts"]["args"]} {procs[proc0]["arg"]} -o {outdir}/{collider}-{showername}{procs[proc0]["tag"]}{tgs["altrts"]["tag"]}-allon.dat')
    
            #  3. all-off: everything off [Z,H]
            for proc in procs:
                commands.append(f'{base_command} {spin_off["args"]} {colour_largeNc["args"]} {tgs["default"]["args"]} {procs[proc]["arg"]} -o {outdir}/{collider}-{showername}{procs[proc]["tag"]}{tgs["default"]["tag"]}-alloff.dat')
            
            #  4. all off, spin on [Z]
            commands.append(f'{base_command} {spin_on["args"]} {colour_largeNc["args"]} {tgs["default"]["args"]} {procs[proc0]["arg"]} -o {outdir}/{collider}-{showername}{procs[proc0]["tag"]}{tgs["default"]["tag"]}{spin_on["tag"]}.dat')
    
            #  5. all off except colour: segment or NODS [Z]
            for colour in [colour_segment, colour_NODS]:
                commands.append(f'{base_command} {spin_off["args"]} {colour["args"]} {tgs["default"]["args"]} {procs[proc0]["arg"]} -o {outdir}/{collider}-{showername}{procs[proc0]["tag"]}{tgs["default"]["tag"]}{colour["tag"]}.dat')
    

            # matching and double-soft only for ee
            if collider=='ee':
                #  7. all off except matching [Z,H] (nomer depends on available matchings)
                for proc in procs:
                    for matching in info['matchings'].keys():
                        commands.append(f'{base_command} {spin_off["args"]} {colour_largeNc["args"]} {info["matchings"][matching]} {tgs["default"]["args"]} {procs[proc]["arg"]} -o {outdir}/{collider}-{showername}{procs[proc]["tag"]}{tgs["default"]["tag"]}{matching}.dat')

                #  8. all off except double-soft (if available) and mult matching [Z]
                if info['has-double-soft']:
                    commands.append(f'{base_command} {spin_off["args"]} {colour_largeNc["args"]} {double_soft_on["args"]} {tgs["default"]["args"]} {procs[proc0]["arg"]} -o {outdir}/{collider}-{showername}{procs[proc0]["tag"]}{tgs["default"]["tag"]}{double_soft_on["tag"]}.dat')

    
    #--------------------------------------------------------------------------------
    # treat DIS specifically 
    for collider in ['dis']:
        procs=processes[collider]
        proc0 = next(iter(procs))
        base_collider_command=f'../build-double/example-framework -rseq 1 -nev {nev} '
        tgs = targs[collider]
        for showername in included_showers:
            info = showers[showername]
            if not info['has-'+collider]: continue
    
            base_command=f'{base_collider_command} {info["args-shower"]} '
            
            # 1: NODS: spin
            commands.append(f'{base_command} {spin_on["args"]} {colour_NODS["args"]}    {tgs["default"]["args"]} {procs[proc0]["arg"]} -o {outdir}/{collider}-{showername}{tgs["default"]["tag"]}{spin_on["tag"]}{colour_NODS["tag"]}.dat')
    
            if "light" in info.keys() and info["light"]:
                continue
            
            # 2: CF=CA/2: spin
            commands.append(f'{base_command} {spin_on["args"]}  {colour_largeNc["args"]} {tgs["default"]["args"]} {procs[proc0]["arg"]} -o {outdir}/{collider}-{showername}{tgs["default"]["tag"]}{spin_on["tag"]}.dat')
            
            # 3: CF=CA/2: no spin, alt rts
            commands.append(f'{base_command} {spin_off["args"]} {colour_largeNc["args"]} {tgs["altrts"]["args"]} {procs[proc0]["arg"]}  -o {outdir}/{collider}-{showername}{tgs["altrts"]["tag"]}.dat')

    #--------------------------------------------------------------------------------
    # additional specific runs
    # Physical setup for pp
    if 'panglobal-beta0.5' in included_showers:
        commands.append(f'../build-double/example-framework -nev {nev} -shower panglobal -beta 0.5 -process pp2Z -mZ 91.2                      -rts 13600 -colour NODS -no-spin-corr -rseq 1 -physical-coupling -pdf-choice ToyVFNPhysical -o {outdir}/pp-panglobal-beta0.5-physical-pp2Z-ToyVFNPhysical-nods.dat')

    # scale variations (pp)
    if 'panglobal-beta0.5' in included_showers:
        commands.append(f'../build-double/example-framework -nev {nev} -shower panglobal -beta 0.5 -process pp2Z -mZ 91.2 -xmur 1.5  -xmuf 2.0 -rts 13600 -colour NODS -no-spin-corr -rseq 1 -physical-coupling -pdf-choice ToyVFNPhysical -o {outdir}/pp-panglobal-beta0.5-physical-pp2Z-ToyVFNPhysical-nods-scaleup.dat')
        commands.append(f'../build-double/example-framework -nev {nev} -shower panglobal -beta 0.5 -process pp2Z -mZ 91.2 -xmur 0.75 -xmuf 0.5 -rts 13600 -colour NODS -no-spin-corr -rseq 1 -physical-coupling -pdf-choice ToyVFNPhysical -o {outdir}/pp-panglobal-beta0.5-physical-pp2Z-ToyVFNPhysical-nods-scaledn.dat')

    # scale variations (ee)
    if 'panlocal-beta0.5' in included_showers:
        commands.append(f'../build-double/example-framework -nev {nev} -shower panlocal -beta 0.5 -Q 91.2 -xmur 1.5  -xhard 2.0 -colour NODS -no-spin-corr -rseq 1 -physical-coupling -o {outdir}/ee-panlocal-beta0.5-physical-ee2Z-nods-scaleup.dat')
        commands.append(f'../build-double/example-framework -nev {nev} -shower panlocal -beta 0.5 -Q 91.2 -xmur 0.75 -xhard 0.5 -colour NODS -no-spin-corr -rseq 1 -physical-coupling -o {outdir}/ee-panlocal-beta0.5-physical-ee2Z-nods-scaledn.dat')

    # tree level & weighted generation: do PanGlobal(0) with everything on and just try different matching schemes
    if 'panglobal-beta0' in included_showers:
        collider='ee'
        base_collider_command=f'../build-double/example-framework -rseq 1 -nev {nev} '
        procs = processes[collider]
        tgs = targs[collider]
        showername = 'panglobal-beta0'
        info = showers['panglobal-beta0']
        base_command=f'{base_collider_command} {info["args-shower"]} '
        all_on_base=f'{base_command} {spin_on["args"]} {colour_NODS["args"]}'
        
        # for ee, add matching and double-soft
        if len(info['matchings'].keys())>0:
            default_matching=next(iter(info['matchings'].keys()))
            default_matching_args=info['matchings'][default_matching]
            all_on_base=f'{all_on_base} {default_matching_args}'
        if info['has-double-soft']:
            # WATCH OUT: for now, disable spin for double-soft
            all_on_base=f'{all_on_base} {spin_off["args"]} {double_soft_on["args"]}'
                
        for proc in procs:
            commands.append(f'{all_on_base} {tgs["default"]["args"]} {procs[proc]["arg"]} -tree-level 2        -o {outdir}/{collider}-{showername}{procs[proc]["tag"]}{tgs["default"]["tag"]}-allon-tree2.dat')
            commands.append(f'{all_on_base} {tgs["default"]["args"]} {procs[proc]["arg"]} -weighted-generation -o {outdir}/{collider}-{showername}{procs[proc]["tag"]}{tgs["default"]["tag"]}-allon-wgt.dat')

    # weighted generation: do PanGlobal with everything on and just try different matching schemes
    
    
    
    return commands

#----------------------------------------------------------------------
def run_jobs():
    if args.dry_run:
        print ("Dry run: simply printing the list of commands")
        for command in commands:
            print (command)
        return

    # create the output dir
    print (f"saving to directory {outdir}")
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    # queue commands
    for command in commands:
        runner.run(command, groupname=f"new_refruns_{args.length}")

    # submission should happen automatically
    


#----------------------------------------------------------------------
def build_all():
    orig_dir=os.getcwd()
    os.chdir('..')
    # cmake will be needed if we make this part of a nightly check
    stdouterr_from(f"../scripts/build-multi-precisions.py --builds double -j")
    os.chdir(orig_dir)

#----------------------------------------------------------------------
def stdouterr_from(arguments):
    if args.dry_run:
        print(arguments)
        return
    
    result = subprocess.run(arguments, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if (result.returncode != 0):
        print(result)
        print("An error occurred")
        exit(-1)
    return result.stdout.decode('utf-8')


if __name__ == '__main__': main()
