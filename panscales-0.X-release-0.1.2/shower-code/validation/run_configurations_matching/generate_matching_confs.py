import os

matching_showers = [
    # multplicative scheme
    ['ee-panlocal_beta_0_mult',                       '-shower panlocal -beta 0'],
    ['ee-panlocal_beta_0.5_mult',                     '-shower panlocal -beta 0.5'],
    ['ee-panglobal_beta_0_mult',                      '-shower panglobal -beta 0'],
    ['ee-panglobal_beta_0.5_mult',                    '-shower panglobal -beta 0.5'],
    # MC@NLO
    ['ee-panlocal_beta_0_mcatnlo',                    '-shower panlocal -beta 0 -mcatnlo-matching'],
    ['ee-panlocal_beta_0.5_mcatnlo',                  '-shower panlocal -beta 0.5 -mcatnlo-matching'],
    ['ee-panlocal-vincia_beta_0_mcatnlo',             '-shower panlocal-vincia -beta 0 -mcatnlo-matching'],
    ['ee-panlocal-vincia_beta_0.5_mcatnlo',           '-shower panlocal-vincia -beta 0.5 -mcatnlo-matching'],
    ['ee-panglobal_beta_0_mcatnlo',                   '-shower panglobal -beta 0 -mcatnlo-matching'],
    ['ee-panglobal_beta_0.5_mcatnlo',                 '-shower panglobal -beta 0.5 -mcatnlo-matching'],
    # HEG matching
    ['ee-panlocal_beta_0_heg_powheg',                 '-shower panlocal -beta 0 -matching-shower powheg'],
    ['ee-panlocal_beta_0.5_heg_powheg',               '-shower panlocal -beta 0.5 -matching-shower powheg'],
    ['ee-panglobal_beta_0_heg_powheg',                '-shower panglobal -beta 0 -matching-shower powheg'],
    ['ee-panglobal_beta_0.5_heg_powheg',              '-shower panglobal -beta 0.5 -matching-shower powheg'],
    ['ee-panlocal-vincia_beta_0_heg_panglobal',       '-shower panlocal-vincia -beta 0 -matching-shower panglobal'],
    ['ee-panlocal-vincia_beta_0.5_heg_panglobal',     '-shower panlocal-vincia -beta 0.5 -matching-shower panglobal'],
]
proc = [
    ['ee2Z', '',       ['']],
    ['ee2Z', 'rts_mZ',  ['-rts 91.1876']],
    ['ee2H', '',        ['-h2gg']],
    ['ee2H', 'rts_mH',  ['-h2gg', '-rts 125.0']]
]

colour = [
    ['no_colour', '-CF-halfCA'],
    ['nods',      '-colour-factor-meas2']
]

spin = [
    ['no_spin', '-no-spin-corr']
]

alphas = [
    ['alphas_0.1_lnvmin_-14',   ['-lnvmin -14 ', '-lnkt-cutoff -5', '-nloops 2', '-alphas 0.1']],
    ['alphas_0.02_lnvmin_-22',  ['-lnvmin -22', '-lnkt-cutoff -14', '-nloops 2', '-alphas 0.020']]
]

for ptag1, ptag2, p in proc:

    # alphas/lnv options
    for atag, a in alphas:
        filename = ptag1 + '_' + atag
        if ptag2 != '': filename += '_' + ptag2
        filename += '.cmnd'

        with open(filename, 'w') as f:
            for cmnd in p:
                if (p != ''): f.write(cmnd + '\n')
            for cmnd in a: f.write(cmnd + '\n')
            f.write('-do-frag')

    # colour options
    for ctag, c in colour:
        filename = ptag1 + '_' + alphas[0][0] + '_' + ctag + '.cmnd'
        with open(filename, 'w') as f:
            for cmnd in p:
                if (p != ''): f.write(cmnd + '\n')
            for cmnd in alphas[0][1]: f.write(cmnd + '\n')
            f.write(c + '\n')
            f.write('-do-frag')
    

    # spin options
    for stag, s in spin:
        filename = ptag1 + '_' + alphas[0][0] + '_' + stag + '.cmnd'
        with open(filename, 'w') as f:
            for cmnd in p:
                if (p != ''): f.write(cmnd + '\n')
            for cmnd in alphas[0][1]: f.write(cmnd + '\n')
            f.write(s + '\n')
            f.write('-do-frag')