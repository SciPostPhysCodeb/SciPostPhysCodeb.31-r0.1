Directory to help with validation of shower code
================================================

Standard usage
--------------

Run 

     ./validate-showers [-l {vshort|short|...}] [-s SHOWER] [ -v  {double|doubleexp|...}] [...]

to get validation of showers. This is a check that results are identical
to a set of reference runs, to within a certain relative precision
(2e-5, to make sure we are not sensitive to rounding errors in histogram
outputs).




Advanced usage
--------------

To generate new validation runs, use the `generate-reference-runs.py`
script (preferable do not do things manually). 

Be careful, only use this if you are _absolutely_ sure that the code
you're using for generation is correct. Before doing that, one might
first want some checks:

- warn people on the general channel

- make sure everything is committed before starting a run (so that
  there's a record of exactly what was used)

- if it's just a switch of random number order, does the new code
  give results that are statistically identical to older code
  that did pass the checks?

- If not, then there's a physics change. Consult with everyone and make
  sure that log accuracy checks have been repeated.

To protect against accidentally overwriting "good" reference runs,
`generate-reference-runs.py` puts things into a `staging/YYYY-MM-DD`
directory. That provides opportunities for checking things before
copying them over to the main reference run directories.

`validate-showers` has a `-d` option to supply the staging directory for
running checks against it (e.g. across two different computers).

`check-equiv-physics.py -d staging/YYYY-MM-DD` does a brief check for
equivalence of the physics of the results (currently just mean multiplicity).


Design of validation setup
--------------------------

Here are some aims to keep in mind:

- we want to test as many features as possible, so that if something breaks we
  know about it

- at the same time, we don't want a huge number of files. We probably
  don't need all combinations of spin/colour/running-coupling for each
  shower -- rather we want to make sure we test the most complete
  combination for each shower, and perhaps have a few spot-checks
  with simpler combinations (e.g. so that they don't go untested). 
  