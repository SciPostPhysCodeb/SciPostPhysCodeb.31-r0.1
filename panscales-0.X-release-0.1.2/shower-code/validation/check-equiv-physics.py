#!/usr/bin/env python3
import os
import re
import argparse
import glob
import sys
import datetime

# add a search directory for the python modules
sys.path.insert(0,os.path.dirname(os.path.abspath( __file__ )) + '/../../submodules/AnalysisTools/python')
import hfile as h

go_red = '\033[91m'
go_green = '\033[92m'
go_end = '\033[0m'

def main():
    script_path = os.path.dirname(os.path.abspath( __file__ ))
    os.chdir(script_path)

    parser = argparse.ArgumentParser()

    refdir = 'refruns_long'
    parser.add_argument("-r", '--refdir', default=refdir, help='directory containing the reference runs')
    parser.add_argument("-d", '--newdir', required=True, help='directory containing the new runs')
    parser.add_argument('-s', '--shower', default=None,
                         help='if set, do only this shower, used as a normal glob (can be comma-separated list); ')
    parser.add_argument("-t",'--threshold', type=float, default=3.0,help='threshold for flagging a difference in sigma')

    args = parser.parse_args()

    # clean up the name for the refrun directory and newrun directories
    refdir = re.sub(f'/$','',args.refdir)
    newdir = re.sub(f'/$','',args.newdir)
    if 'refruns_long' not in refdir: refdir += f'/refruns_long'
    if 'refruns_long' not in newdir: newdir += f'/refruns_long'
    # further clean up for globbing/processing
    refdir = re.sub(f'//','/',args.refdir)    

    # Fish out the file names within the selected refrun directory to compare to.
    if args.shower is None:
        file_list = [file.replace(f'{refdir}/','') for file in glob.glob(f'{refdir}/*.dat')]
    else:
        file_list = []
        for shower in args.shower.split(','):
            file_list.extend([file.replace(f'{refdir}/','') for file in glob.glob(f'{refdir}/*{shower}*.dat')])
    file_list.sort()

    # document directories, host and state
    print(" ".join(sys.argv))
    print(f"Running from {script_path} on {os.uname()[1]} at {datetime.datetime.now()}\n")
    print("refdir (first file):")
    print_git_state(refdir,file_list[0])
    print("newdir (first file):")
    print_git_state(newdir,file_list[0])

    for file in file_list:
        ref_file = f"{refdir}/{file}"
        new_file = f"{newdir}/{file}"
        ref_mult = h.get_xsection(ref_file, 'multiplicity_unweighted', '').ve()
        new_mult = h.get_xsection(new_file, 'multiplicity_unweighted', '').ve()
        diff = new_mult - ref_mult
        nsigma = abs(diff.value / diff.error )
        good = nsigma < args.threshold 
        go = go_green if good else go_red
        if good:
            go = go_green
            extra = ""
        else:
            go = go_red
            extra = f"ref({git_state(ref_file)}): {ref_mult:9.5f}  new({git_state(new_file)}): {new_mult:9.5f}"
        print (f"{file.replace('.dat',''):84} {diff.value:9.5f} ± {diff.error:.5f} {go}({nsigma:.2f}σ){go_end} {extra}")

def print_git_state(dir,file):
    print(f"git state from {dir}/{file}")
    os.system(f"grep git {dir}/{file}")
    print()

def git_state(filename):
    with open(f"{filename}") as f:
        for line in f:
            m = re.match("# libpanscales compiled git info: ([^ ]*)", line)
            if m and len(m.group(1))>6: return m.group(1)[0:6]
    return "no-git"

if __name__ == '__main__': main()
