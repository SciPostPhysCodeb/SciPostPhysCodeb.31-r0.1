#!/usr/bin/env python3
"""
 Script to help validate showers against a set of reference results
 (specifically to make sure, e.g., that when refactoring code we haven't
 accidentally changed any results)
"""
import subprocess
import os
import re
import argparse
import glob
import shutil
import sys
from multiprocessing import Pool

tmpdir="tmp/"
build_dir = '../build-double/'
prog='example-framework'

variants = {
  'double': {'dir': '../build-double', 'build_opts': '', 'run_opts':''},
  'doubleexp': {'dir': '../build-doubleexp', 'build_opts': '-DPSDOUBLEEXP=on', 'run_opts':''},
  'ddreal': {'dir': '../build-ddreal', 'build_opts': '-DPSDDREAL=on', 'run_opts':''},
  'qdreal': {'dir': '../build-qdreal', 'build_opts': '-DPSQDREAL=on', 'run_opts':''},
  'mpfr4096': {'dir': '../build-mpfr4096', 'build_opts': '-DPSMPFR4096=on', 'run_opts':''},
  'double-diff': {'dir': '../build-double', 'build_opts': '', 'run_opts':'-use-diffs'},
  'doubleexp-diff': {'dir': '../build-doubleexp', 'build_opts': '-DPSDOUBLEEXP=on', 'run_opts':'-use-diffs'},
}
rundir = ''

red_cross='\033[91mFAIL\033[0m'
green_tick='\033[32mOK\033[0m'

found_trouble = False



def main():
    # this script is meant to be run from the directory in which it
    # lives; so move into that directory if run from elsewhere
    script_path = os.path.dirname(os.path.abspath( __file__ ))
    os.chdir(script_path)

    parser = argparse.ArgumentParser()
    parser.add_argument('-l','--length',
                        choices=['short','medium','long'], 
                        default='short',
                        help='short=4e3 events(~10s in double), medium=1e5,long=4e6. '
                        'The "lengths" with the xtrm_ prefix have large values of the log (1000), are '
                        'not run by default, and should be run either with mpfr4096 (v.slow) or doubleexp-diff (fast). '
                        '(they also only test multiplicity for now')
    parser.add_argument('-s','--shower',default=None,help='if set, do only this shower (can be comma-separated list); '
                                                        'currently needs "ee-", "pp-" or "dis-" prefix (e.g. ee-panglobal)')
    parser.add_argument('-d','--refdir',default='./',help='directory containing reference runs')
    parser.add_argument('-v', '--variant', default='double',choices=variants.keys())
    parser.add_argument("--list", action='store_true', help="if present just list the command but do not run them")
    parser.add_argument('--keep-tmp-files', action='store_true', help='if present temporary run files are always kept, even on success')
    parser.add_argument("--no-build", action='store_true', help="if present, don't build the code, just run the tests")
    parser.add_argument("-j", '--jobs', action="store", type=int, const=0, default=1, nargs='?', 
                        help="number of cores to use (default 1); if -j is given on its own, use all available cores")
    args = parser.parse_args()

    do_tests(args)

def do_tests(args):
    global found_trouble, rundir, tmpdir
    summary = ''
    print(f"Doing {args.length} validation, {args.variant} args.variant")
    rundir = f"{args.refdir}/refruns_{args.length}"
    rundir = rundir.replace('//','/') 

    if not args.no_build: build_all(args.variant)
    
    # Fish out the file names within the selected refrun directory to compare to.
    if args.shower is None:
        file_list = [file.replace(f'{rundir}/','') for file in glob.glob(f'{rundir}/*dat')]
    else:
        file_list = []
        for shower in args.shower.split(','):
            file_list.extend([file.replace(f'{rundir}/','') for file in glob.glob(f'{rundir}/*{shower}*dat')])

    file_list.sort()

    # Get the cmdline from within the header of each file.
    cmdline_list=[]
    for file in file_list:
        this_cmdline = open(f"{rundir}/{file}").readline().strip("^# ").strip()
        cmdline_list.append(this_cmdline)

    # if we're just asking the list of command, print it and bail out
    if args.list:
        print ("# List of command that would be run:")
        for cmd in cmdline_list:
            print (cmd)
        return

    # make sure we have the temporary directory for the output files
    if not os.path.isdir(tmpdir): os.mkdir(tmpdir)
    
    # Run each cmdline, either sequentially or in parallel
    njobs = args.jobs if args.jobs >= 1 else None
    print(f"njobs = {njobs if njobs else 'all available cores'}")
    # the pool will allow jobs to be run in parallel
    with Pool(njobs) as pool:        
        summaries = pool.starmap(run_cmdline, [[cmdline,rundir,args] for cmdline in cmdline_list])
        summary += ''.join(summaries)
    # if args.jobs == 1:
    #     for ixx in range(0,len(cmdline_list)) :
    #         cmdline=cmdline_list[ixx]
    #         summary += run_cmdline(cmdline, args)
    # else:



    print ('')
    print ('Summary')
    print ('')
    print (summary)
    print ('')
    nfail = summary.count("FAIL")
    
    if nfail > 0:
        print(f"{red_cross}: There were {nfail} failures out of {len(cmdline_list)} tests")
        exit(-1) 
    else:
        print(f"{green_tick}: There were {nfail} failures out of {len(cmdline_list)} tests")
        exit(0)

def run_cmdline(cmdline,rundir, args):
    '''Runs the given command-line and returns a line for the summary table
    ''' 
    # get the program name, with and without the directory
    prog = cmdline.split()[0]
    prog_stripdir = re.sub('.*/','',prog)

    # work out the output file (without its directory) and shower
    orig_outfile = re.search(r' -o +([^ ]+)',cmdline).group(1)
    orig_outfile = re.sub('.*/','',orig_outfile)
    tmp_outfile = f"{tmpdir}/" + orig_outfile
    shower = re.search(r' -shower +([^ ]+)',cmdline).group(1)
    
    #if (args.shower is not None and shower not in args.shower.split(',')): continue

    # get just the options without the outfile or the shower
    opts = " ".join(cmdline.split()[1:])
    opts = re.sub(r" -o +([^ ]+)"," ",opts)
    opts = re.sub(r" -shower +([^ ]+)"," ",opts)
    opts += f" {variants[args.variant]['run_opts']}"
    
    # then work out the new command
    new_command = f"{variants[args.variant]['dir']}/{prog_stripdir} {opts} -shower {shower} -output-interval 4e3 -o {tmp_outfile}"
    
    summary = f'{orig_outfile:84}  '
    shower_doc = f"\nRunning {shower}  -> {tmp_outfile}\n   ({opts})"

    # if running a single job, print the shower doc before it starts
    if args.jobs == 1: print (shower_doc)
    shower_output = stdouterr_from(new_command)

    if shower_output is None:
        if os.path.exists(rundir+'/'+orig_outfile):
            found_trouble = True
            summary += f'{red_cross} [test returned None]\n'
        else: 
            summary += f'{green_tick} [None]\n'
        # if running multiple jobs, print the shower doc after it finishes
        if args.jobs != 1: print(shower_doc.replace("Running","Finished") + "\n" + summary)
        else:              print(summary)
        return summary
    
    # We use numdiff.pl to compare the files, with two tolerances:
    # - 2e-5 for the relative differences
    # - but allowing absolute differences of up to 1e-14; the latter is needed
    #   because some bin edges are supposed to come out as zero, but have small
    #   rounding errors in their calculations and differences between systems 
    #   (M2 v. intel) were triggering failures.
    diff_cmd = '../../scripts/numdiff.pl -I "^#" {} {} 2e-5 r 1e-14'.format(tmp_outfile,rundir+'/'+orig_outfile)
    #print(diff_cmd)
    diff_output = stdouterr_from(diff_cmd)
    # print (diff_cmd,diff_output,sep="\n")
    if (diff_output is None or diff_output.strip() != ""):
        #print(red_cross)
        found_trouble = True        
        summary += f'{red_cross}\n'
    else:
        #print(green_tick)
        summary += f'{green_tick}\n'
        if not args.keep_tmp_files: os.remove(tmp_outfile)

    # if running multiple jobs, print the shower doc after it finishes
    if args.jobs != 1: print(shower_doc.replace("Running","Finished") + "\n" + summary)
    else:              print(summary)
    return summary

def build_all(variant):
    # Clean current build
    for f in ['../libpanscales.so', '../config.hh', '../CMakeCache.txt', '../cmake_install.cmake']:
        if os.path.exists(f): os.remove(f)
    if os.path.exists('../CMakeFiles'): shutil.rmtree('../CMakeFiles')

    build_dir = variants[variant]['dir']
    print(f"Building everything in {build_dir}")
    
    orig_dir=os.getcwd()
    if not os.path.isdir(build_dir):
        os.mkdir(build_dir)
    os.chdir(build_dir)

    return_code = os.system('cmake ' + variants[variant]['build_opts'] + ' ..')
    if return_code != 0:
        print (f'\nCMake failed (with code {return_code}). Exiting', file=sys.stderr)
        exit(-1)

    return_code = os.system(f'make -j')
    if return_code != 0:
        print (f'\nBuilding failed (with code {return_code}). Exiting', file=sys.stderr)
        exit(-1)
    
    os.chdir(orig_dir)


def stdouterr_from(args):
    result = subprocess.run(args, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if (result.returncode != 0):
        print(result)
        print("An error occurred")
        global found_trouble
        found_trouble = True
        return None
        #exit(-1)
    return result.stdout.decode('utf-8')

if __name__ == '__main__': main()
