//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerPanScaleGlobal.hh"

using namespace std;

namespace panscales{

  LimitedWarning ShowerPanScaleGlobal::_PanGlobalNegativeQbar;
  LimitedWarning ShowerPanScaleGlobal::_PanGlobalMultiplicativeCAnot2CF;
  

  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerPanScaleGlobal::elements(
    Event & event, int dipole_index) const {
    int iq    = event.dipoles()[dipole_index].index_q   ;
    int iqbar = event.dipoles()[dipole_index].index_qbar;
    std::unique_ptr<typename ShowerBase::Element> elms[] = {
      std::unique_ptr<typename ShowerBase::Element>(new ShowerPanScaleGlobal::Element(iq, iqbar, dipole_index, &event, this))
    };
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }
  ShowerBase::EmissionInfo* ShowerPanScaleGlobal::create_emission_info() const{ return new ShowerPanScaleGlobal::EmissionInfo(); }

  //----------------------------------------------------------------------
  Range ShowerPanScaleGlobal::Element::lnb_generation_range(double lnv) const {
    // this corresponds to Eq. (52 - 53) of 2018-07-notes.pdf (and to the soft limit used in the Sudakov)
    double lower_bound = to_double(log_T(_sitilde/_sjtilde*pow2(_rho)/_dipole_m2) + 2.*lnv)
      /2./(1. + _shower->_beta);
    double upper_bound = to_double(log_T(_sitilde/_sjtilde*_dipole_m2/pow2(_rho)) - 2.*lnv)
      /2./(1. + _shower->_beta);
    return Range(lower_bound,upper_bound);
  }
  
  /// returns the exact range of lnb that is allowed; returns a a
  /// zero-extent range in cases where there in no range of lnb
  /// allowed. (Note that the zero extent range may take various
  /// forms, e.g. two identical points, or inverted points)
  Range ShowerPanScaleGlobal::Element::lnb_exact_range(double lnv) const {
    double beta = _shower->_beta;
    assert(beta <= 1.0);
    // Recall expressions from nll-shower-notes/2018-07-notes.pdf Eqs.(70-73)
    // positive eta part (dropping all tildes for compactness):
    //   kt = rho v exp(beta * eta)
    //   ak = sqrt(sj/sij/si) kt exp(eta) = sqrt(sj/sij/si) rho v exp((beta+1)*eta)
    //   bk = sqrt(si/sij/sj) kt exp(eta) = sqrt(sj/sij/si) rho v exp((beta-1)*eta)
    // So we then have
    //   ak < 1  ---> eta < -log(sqrt(sj/sij/si) rho v)/(1+beta)
    //   bk < 1  ---> eta > +log(sqrt(si/sij/sj) rho v)/(1-beta)
    // Or if beta=1
    //   eta < +log(sqrt(sj/sij/si) rho v)/2
    //   log(sqrt(sj/sij/si) rho v) < 0
    precision_type ak_lnprefactor = (lnv + log(_rho) + this->_half_ln_sj_over_sij_si);
    precision_type bk_lnprefactor = ak_lnprefactor + log(_sitilde / _sjtilde);
    precision_type lnb_plus_min, lnb_plus_max;
    precision_type lnb_minus_min, lnb_minus_max;
    // then the outer ones (for +ve and -ve lnv) are
    // good for any 0 <= beta <= 1
    lnb_plus_max  = -ak_lnprefactor/(1+beta);
    lnb_minus_min =  bk_lnprefactor/(1+beta);
    // the formulas for the inner limits break down when
    // beta = 1, so treat beta=1 case separately
    if (beta < 1.0) {
      lnb_plus_min  =  bk_lnprefactor/(1-beta);
      lnb_minus_max = -ak_lnprefactor/(1-beta);
    } else if (beta == 1.0) {
      // for a positive eta solution to be possible, the bk_lnprefactor
      // (which gives bk directly, without additional factors), must be
      // negative to get b_k < 1; if this is satisfied, then provide
      // the full positive range of eta that is consistent with lnb_plus_max
      // otherwise set a zero range
      if (bk_lnprefactor < 0) lnb_plus_min = 0.0;
      else                    lnb_plus_min = lnb_plus_max;
      // analogously for the negative eta region
      if (ak_lnprefactor < 0) lnb_minus_max = 0.0;
      else                    lnb_minus_max = lnb_minus_min;
    } else {
      assert(false && "beta should be <= 1.0");
    }
    // now put the minus and plus ranges together
    if (lnb_plus_max > 0) {
      if (lnb_minus_min < 0) {
        // check that both ranges cover zero (they should always do so)
        assert(lnb_minus_max >= 0 && lnb_plus_min <= 0);
        return Range(to_double(lnb_minus_min),to_double(lnb_plus_max));
      } else {
        // otherwise return just the plus range (keeping in mind
        // that this may be a range with zero extent)
        return Range(to_double(lnb_plus_min),to_double(lnb_plus_max));
      }
    } else   {
      // or just the minus one (again, may have zero extent)
      return Range(to_double(lnb_minus_min),to_double(lnb_minus_max));
    }

  }
  
  //----------------------------------------------------------------------
  double ShowerPanScaleGlobal::Element::lnv_lnb_max_density() const {
    // For now we work in a large-NC limit, with CF defined to be equal
    // to CA/2; the density that is returned here corresponds to that
    // for a quark or for half a gluon
    // _double_soft_overhead is compensated in double_soft_acceptance_probability
    //std::cout << "dKCMW overhead = " << dKCMW_overhead() << std::endl;
    return _shower->max_alphas() * _shower->qcd().CA() / M_PI * double_soft_overhead() * dKCMW_overhead();
  }

  precision_type ShowerPanScaleGlobal::Element::dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const{
    double bps = _shower->beta();
    precision_type sqrtQ2 = to_double(sqrt(event().Q2()));
    double lnQ = to_double(log(sqrtQ2));
    //
    // Is there enough phase space for ak<1 and bk<1?
    if( lnb >= (lnQ-lnv)/(1+bps) || lnb <= (lnv-lnQ)/(1+bps) ) return 0;
    //
    precision_type ak       = exp(lnv + lnb + bps * fabs(lnb)) / sqrtQ2;
    precision_type bk       = exp(lnv - lnb + bps * fabs(lnb)) / sqrtQ2;
    precision_type kt_on_Q  = exp(lnv + bps * fabs(lnb)) / sqrtQ2;
    //
    precision_type jacobian = 2 * kt_on_Q * kt_on_Q * (1-ak) * (1-bk)
                    / pow(1-kt_on_Q*kt_on_Q,3);
    // If both a and b are within √ɛ of 1, we return 0
    if(fabs(1-ak) < numeric_limit_extras<double>::sqrt_epsilon() &&
        fabs(1-bk) < numeric_limit_extras<double>::sqrt_epsilon()) {
      jacobian = 0.;
    }
    //
    // If a is within √ɛ of 1, we use an expansion for the jacobian
    // about a = 1, to second order in (1-a).
    else if(fabs(1-ak) < numeric_limit_extras<double>::sqrt_epsilon()) {
      jacobian = 2 * (1 - ak) * bk * (ak - 3*bk + 2*ak*bk) / pow(1-bk,3);
    //
    // If b is within √ɛ of 1, we use an expansion for the jacobian
    // about b = 1, to second order in (1-b).
    } else if(fabs(1-bk) < numeric_limit_extras<double>::sqrt_epsilon()) {
      jacobian = 2 * (1 - bk) * ak * (bk - 3*ak + 2*ak*bk) / pow(1-ak,3);
    }
    //
    return jacobian;
  }

  //----------------------------------------------------------------------
  bool ShowerPanScaleGlobal::veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const {

    // The emission is to be vetoed if the point (lnkt, eta) is above the lnv contour.
    // PanGlobal has the property of leaving the angle unchanged to first order, i.e. eta = eta_bar
    // So, it's to be vetoed if lnkt > lnv_first + beta*|eta|
    if (lnkt > lnv_first + _beta*fabs(eta)) return true;
    else return false;
  }
  
  //----------------------------------------------------------------------

  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  bool ShowerPanScaleGlobal::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobal!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobal::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobal::EmissionInfo*>(emission_info_base));

    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;

    // Eq. (2) of arXiv:2002.11114
    precision_type kt = _rho * exp(precision_type(lnv + _shower->_beta * fabs(lnb)));

    // Eq. (3) of arXiv:2002.11114
    precision_type ak   = kt * exp(precision_type(lnb) + this->_half_ln_sj_over_sij_si);
    precision_type bk   = pow2(kt)/_dipole_m2/ak;

    // cache this info for future usage
    emission_info.kt = kt;
    emission_info.ak = ak;
    emission_info.bk = bk;
 
    // impose conservation of the light cone components
    if (ak >= 1.0 || bk >= 1.0) return false;

    // handle the splitting functions at both ends based on the flavour
    //
    // We use the splitting functions normalised so that we need a factor
    // CA in the end (the difference between CA and CF is handled
    // elsewhere
    //
    // Note that for the gluon case, there's an extra factor 1/2
    // accounting for the fact that a gluon is split over 2 dipoles
    //

    _shower->fill_dglap_splitting_weights(emitter(), ak,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    _shower->fill_dglap_splitting_weights(spectator(), bk,
                                          emission_info.spectator_weight_rad_gluon,
                                          emission_info.spectator_weight_rad_quark);
    
    // multiplicative matching:
    if (! _shower->_additive_branching) {
      // in the soft limit, these factors should go to 1 and have no effects
      //
      // Things are constructed as follows: ShowerRunner will combine
      // these things so as to give an overall probability
      //   P = P_emit + P_spec
      // with
      //   P_emit = f_emit * w_emit = f_emit * (w_emit_rad_gluon + w_emit_rad_quark)
      //   P_spec = f_spec * w_spec = f_spec * (w_spec_rad_gluon + w_spec_rad_quark)
      // with f_emit = f, f_spec = 1-f, and  an emitter splitting probability
      //   f_emit = P_emit/P
      // we want
      //   Pmult = (w_emit_rad_gluon + w_emit_rad_quark)(w_spec_rad_gluon + w_spec_rad_quark)
      //   Pmult_emit = f Pmult, Pmult_spec=(1-f) Pmult
      double wemit = emission_info.emitter_weight_rad_gluon   + emission_info.emitter_weight_rad_quark;
      double wspec = emission_info.spectator_weight_rad_gluon + emission_info.spectator_weight_rad_quark;
      emission_info.emitter_weight_rad_gluon   *= wspec;
      emission_info.emitter_weight_rad_quark   *= wspec;
      emission_info.spectator_weight_rad_gluon *= wemit;
      emission_info.spectator_weight_rad_quark *= wemit;
    }

    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = ak;
    emission_info.z_radiation_wrt_spectator = bk;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor *= (double_soft_overhead() * dKCMW_overhead() / lnv_lnb_max_density());
    
    // this is an antenna shower: the emitter splits w probability f(lnb), the spectator w 1-f(lnb)
    double f, omf;
    if (_shower->_split_dipole_frame){
      // 1/2 log(si/sj) = -1/2 log(sj/(sij si)) - 1/2 log(sij)
      double eta0_dip = to_double(-0.5*_log_dipole_m2 - _half_ln_sj_over_sij_si);
      //emission_info.emitter_partition_fraction = f_fcn(lnb-eta0_dip);
      f_fcn(lnb-eta0_dip, f, omf);
    } else {
      f_fcn(lnb, f, omf);
      //emission_info.emitter_partition_fraction = f_fcn(lnb);
    }
    emission_info.emitter_weight_rad_gluon   *=   f * normalisation_factor;
    emission_info.emitter_weight_rad_quark   *=   f * normalisation_factor;
    emission_info.spectator_weight_rad_gluon *= omf * normalisation_factor;
    emission_info.spectator_weight_rad_quark *= omf * normalisation_factor;
    
    return true;
  }

  //------------------------------------------------------------------------
  bool ShowerPanScaleGlobal::Element::do_kinematics(
    typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobal!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleGlobal::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobal::EmissionInfo*>(emission_info_base));

    // retrieve cached info
    const double & phi = emission_info.phi;

    const precision_type & kt = emission_info.kt;
    const precision_type & ak = emission_info.ak;
    const precision_type & bk = emission_info.bk;
    
    Momentum perp = kt * (sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    emission_info.k_perp = perp;

    // compute the local rescaling option in case for the double rescaling
    precision_type r_local = 1.0;
    if (_shower->rescaling_option() == PanGlobalRescaleOption::DoubleRescaling){
      // in this case, we proceed by doing 2 rescalings.
      //
      // The first one is dipole-local:
      //   pi = r_local (1-ak) pitilde
      //   pj = r_local (1-bk) pjtilde
      //   pk = r_local (ak pitilde + bk pjtilde - kperp)
      // and r_local is determined so as to satisfy
      //   Q.(pi+pj+pk) = Q.(pitilde+pjtilde)
      // We define r_local = 1 + delta
      precision_type ktQ   = 2*dot_product(_event->Q(), perp);
      precision_type delta = ktQ/(_sitilde+_sjtilde-ktQ);
      r_local = 1+delta; // Eq. (10) of arXiv:2307.11142
      precision_type Qbar2 = _Q2 + delta*delta*_dipole_m2 - pow2(r_local*kt);
      // define variables needed for boost 
      emission_info.Qbar = Momentum::fromP3M2(_event->Q().p3()
        + rp.rotn_from_z * (delta * (rp.emitter.p3() + rp.spectator.p3())
                            - r_local*perp.p3()), Qbar2);
      // compute global rescaling factor 
      emission_info.r = sqrt(_Q2/emission_info.Qbar.m2());
    } else if (_shower->rescaling_option() == PanGlobalRescaleOption::GlobalRescalingDeprecated) {  
      // define variables needed for boost in Eq. (16) of arXiv:2002.11114
      emission_info.Qbar = _event->Q() - rp.rotn_from_z * perp;
      // compute rescaling factor in Eq. (17) of arXiv:2002.11114
      emission_info.r = sqrt(_Q2/emission_info.Qbar.m2());
    }
  
  
    // Eqs. (72a-c) 
    // Use Momentum3 class to avoid issues with branchings at small invariant mass
    Momentum3<precision_type> emitter_out3, radiation3, spectator_out3;

    // Proceed with a fully local rescaling as defined by Eq.(9) of arXiv:2307.11142
    if(_shower->rescaling_option() == PanGlobalRescaleOption::LocalRescaling){
      precision_type ktQ = 2*dot_product(_event->Q(), perp);
      precision_type kt2 = pow2(kt);
      precision_type a = _dipole_m2 - kt2;
      precision_type b = _sitilde + _sjtilde - ktQ - 2*kt2;
      precision_type c = - ktQ - kt2;
      assert(b*b>4*a*c);
      // series expansion:
      //   [sqrt(b^2-4ac)-b]/(2a)
      //   = b/(2a) [sqrt(1-4ac/b^2)-1]
      //   ~ -c/b
      precision_type r_minus_one;
      if (std::abs(4*a*c)<numeric_limit_extras<precision_type>::sqrt_epsilon()*b*b){
        precision_type small = a*c/(b*b);
        r_minus_one = (-c/b)*(1+small*(1+small*(2+small*(5+small*14*(1+3*small)))));
      } else {
        r_minus_one = (sqrt(b*b-4*a*c)-b)/(2*a);
      }
      emission_info.r = 1 + r_minus_one;
      assert(emission_info.r>0);

      emitter_out3   = emission_info.r * (1-ak) * rp.emitter  .p3();
      spectator_out3 = emission_info.r * (1-bk) * rp.spectator.p3();
      radiation3     = emission_info.r * (ak * rp.emitter.p3() + bk * rp.spectator.p3() - perp.p3());
      
      emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
      emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
      emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
      
      emission_info.Qbar =
        Momentum::fromP3M2(_event->Q().p3()
                           + rp.rotn_from_z * (r_minus_one * (rp.emitter.p3() + rp.spectator.p3())
                                             - emission_info.r * perp.p3()), _Q2);

      // Note: alternative, untested, mapping options are documented at the end of this file

    // Rescale emitter and spectator imposing Q^2 = Qbar^2
    } else if (_shower->rescaling_option() == PanGlobalRescaleOption::LocalijRescaling){
      // build the a, b c coefficients
      precision_type ktQ = 2*dot_product(_event->Q(), perp);
      precision_type kt2 = pow2(kt);
      precision_type a = (1-ak)*(1-bk)*_dipole_m2;
      precision_type b = (1-ak)*_sitilde + (1-bk)*_sjtilde;
      precision_type c = - ktQ - kt2;
      assert(b*b>4*a*c); //< maybe needs to be replaced!
      // TRK_ISSUE-GEN-18 series expansion:
      //   [sqrt(b^2-4ac)-b]/(2a)
      //   = b/(2a) [sqrt(1-4ac/b^2)-1]
      //   ~ -c/b
      precision_type r_minus_one;
      if (std::abs(4*a*c)<numeric_limit_extras<precision_type>::sqrt_epsilon()*b*b){
        precision_type small = a*c/(b*b);
        r_minus_one = (-c/b)*(1+small*(1+small*(2+small*(5+small*14*(1+3*small)))));
      } else {
        r_minus_one = (sqrt(b*b-4*a*c)-b)/(2*a);
      }
      emission_info.r           = 1 + r_minus_one;
      if(emission_info.r < 0 ) return false;
      // r and r-1 already computed in check_after_acceptance
      emitter_out3   = emission_info.r * (1-ak) * rp.emitter  .p3();
      spectator_out3 = emission_info.r * (1-bk) * rp.spectator.p3();
      radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() - perp.p3();
      
      emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
      emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
      emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
      
      emission_info.Qbar =
        Momentum::fromP3M2(_event->Q().p3()
                           + rp.rotn_from_z * (r_minus_one * ((1-ak)*rp.emitter.p3() + (1-bk)*rp.spectator.p3())
                                             - perp.p3()), _Q2);

    } else if ((_shower->rescaling_option() == PanGlobalRescaleOption::GlobalRescalingDeprecated) || (_shower->rescaling_option() == PanGlobalRescaleOption::DoubleRescaling)) {
      emitter_out3   = r_local * (1.0 - ak) * rp.emitter.p3();
      radiation3     = r_local * (ak * rp.emitter.p3() + bk * rp.spectator.p3() - perp.p3());
      spectator_out3 = r_local * (1.0 - bk) * rp.spectator.p3();
    
      // Now construct the final 4-momenta by imposing the masslessness condition
      emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
      emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
      emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    } else{
      assert(false && "Shower rescaling option unknown");
    }

    // for safety, check that the resulting Q2 has a +ve virtuality
    if (emission_info.Qbar.m2() < 0.0) {
      _PanGlobalNegativeQbar.warn("PanGlobal: Found negative Qbar");
      cerr << "Found negative Qbar" << emission_info.Qbar.m2() << endl;
      return false;
    }

    return true;
  }

  //----------------------------------------------------------------------
  bool ShowerPanScaleGlobal::find_lnv_lnb_from_kinematics(const Momentum &p_emit,
                                     const Momentum &p_spec,
                                     const Momentum &p_rad,
                                     const precision_type &p_emit_dot_p_spec,
                                     const precision_type &p_emit_dot_p_rad,
                                     const precision_type &p_spec_dot_p_rad,
                                     const Event &event,
                                     double &lnv, double &lnb) const {

    // Note: the code below is only valid for the first emission (i.e. assumes rho = 0)
    precision_type a = p_spec_dot_p_rad/p_emit_dot_p_spec;
    precision_type b = p_emit_dot_p_rad/p_emit_dot_p_spec;
    precision_type ak = a/(1. + a);
    precision_type bk = b/(1. + b);

    // If ak or bk are close to 1, then we should just return lnv = 0
    if(fabs(1-ak) < numeric_limit_extras<precision_type>::sqrt_epsilon() ||
       fabs(1-bk) < numeric_limit_extras<precision_type>::sqrt_epsilon()) {
      lnv = 0.;
      lnb = 0.;
      return true;
    }

    precision_type sijtilde = (p_emit + p_spec + p_rad).m2();

    // Expressions for pitilde, pjtilde (assuming the emission is the first)
    Momentum pitilde = 1./(1-ak)*p_emit;
    Momentum pjtilde = 1./(1-bk)*p_spec;

    precision_type sitilde = 2*dot_product(pitilde, event.Q());
    precision_type sjtilde = 2*dot_product(pjtilde, event.Q());
    precision_type kt2 = ak*bk*sijtilde;

    lnb = to_double(log_T(ak/bk)*sitilde/sjtilde/2);
    lnv = to_double(log_T(kt2)/2 - _beta*fabs(lnb));

    return true;

  }

  void ShowerPanScaleGlobal::Element::get_emit_spec_rad_kinematics_for_matching(typename ShowerBase::EmissionInfo * emission_info, Momentum & emit, Momentum & spec, Momentum & rad, bool include_boost) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobal!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    // note this is not a const, we possibly change the information
    // when we need to supply a rescaling factor to the dot products
    // be extremely careful not to change anything else!
    typename ShowerPanScaleGlobal::EmissionInfo & emission_info_global = *(static_cast<typename ShowerPanScaleGlobal::EmissionInfo*>(emission_info));
    // compute the dot products
    set_dot_products(emission_info);
    // set the emitter, spectator and radiated momentum
    emit = emission_info_global.emitter_out;
    spec = emission_info_global.spectator_out;
    rad  = emission_info_global.radiation;

    // if the update event introduces a rescaling we need to multiply this in here
    if(_shower->update_event_changes_dot_products_emit_spec_rad()){
      // apply the rescaling factor to the momenta
      emit *= emission_info_global.r;
      spec *= emission_info_global.r;
      rad  *= emission_info_global.r;
      // the dot products have an additional factor of r here
      auto r2 = emission_info_global.r * emission_info_global.r;
      emission_info_global.emit_dot_rad  *= r2;
      emission_info_global.spec_dot_rad  *= r2;
      emission_info_global.emit_dot_spec *= r2;
      // Note that the reason for first computing the dot products above
      // and later supply them with a rescaling factor is that when
      // the code is run with directional differences, these dirdiffs
      // would also need to be updated taking into account the rescaling 
      // factor. To avoid this additional complication, we just
      // rescale the computed dot products (with or without dirdiff)
      // after the fact. 
    }

    // now check whether we need to include the boost
    if (include_boost) { 
      // this is functionality not yet implemented for dir diffs
      if(use_diffs()) throw std::runtime_error("Tried to get_emit_spec_rad_kinematics_for_matching with directional differences set to true. This is not implemented");
      // boost the momenta
      LorentzBoost boost(emission_info_global.Qbar.reversed());
      emit = emit.boost(boost);
      spec = spec.boost(boost);
      rad  = rad.boost(boost);
      // we do not have to recompute the dot products, they are invariant
    }
  }

  void ShowerPanScaleGlobal::Element::update_event(const ColourTransitionRunnerBase *transition_runner,
                                                   typename ShowerBase::EmissionInfo * emission_info) {
    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info);

    // now that the new emission has been inserted, rescale and boost
    // the event according to Eqs (78a-d)

    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleGlobal!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    const typename ShowerPanScaleGlobal::EmissionInfo & emission_info_global = *(static_cast<typename ShowerPanScaleGlobal::EmissionInfo*>(emission_info));

    // First, rescale the momenta
    // If the rescaling is sufficiently close to one its effect may 
    // be null to within numerical precision, in which case we skip it.
    PanGlobalRescaleOption rescale_option = _shower->rescaling_option();
    if ((!(rescale_option==PanGlobalRescaleOption::LocalijRescaling)) && (!(rescale_option==PanGlobalRescaleOption::LocalRescaling)) &&
        (fabs(emission_info_global.r-1) > _shower->_boost_rescaling_cutoff))
      for(unsigned i = 0; i < _event->size(); ++i) (*_event)[i] *= emission_info_global.r;

    // then handle the boost
    LorentzBoost boost(emission_info_global.Qbar.reversed());

    // If the boost has small enough beta, it can be null to within
    // numerical precision, and hence skipped.
    auto pboost = boost.pboost();
    if( fabs(pboost.modp()/pboost.E()) < _shower->_boost_rescaling_cutoff ) return;

    if (use_diffs()) {

      // record the energies and then boost the particles
      vector<precision_type> Ei(_event->size());
      for(unsigned i = 0; i < _event->size(); ++i) {
        Ei[i] = (*_event)[i].E();
        (*_event)[i].boost(boost);
      }

      // then handle the dipole differences
      for (auto & dipole: _event->dipoles()) {
        int i3bar = dipole.index_3bar();
        dipole.dirdiff_3_minus_3bar = boost.boost_dirdiff(dipole.dirdiff_3_minus_3bar, Ei[i3bar], (*_event)[i3bar]);
      }

      // then handle the dipole differences for non-splitting dipoles as well
      for (auto & dipole: _event->non_splitting_dipoles()) {
        int i3bar = dipole.index_3bar();
        dipole.dirdiff_3_minus_3bar = boost.boost_dirdiff(dipole.dirdiff_3_minus_3bar, Ei[i3bar], (*_event)[i3bar]);
      }

    } else {
      // not using differences, things are simple
      for(unsigned i = 0; i < _event->size(); ++i) {
        (*_event)[i].boost(boost);
      }
    }

  }

  // Eq. (77) of 2018-07-notes.pdf, or equivalently Eq. (5.144) in hep-ph/9605323
  //   p^mu_l = B^mu_nu r p~^nu_l
  // where B^mu_nu = g^mu_nu + 2 Q^mu Q'_nu / Q^2 - 2 (Q + Q')^mu (Q + Q')_nu / (Q + Q')^2
  Momentum3<precision_type> ShowerPanScaleGlobal::Element::_lorentzBoost(const Momentum& p,
                                                                         const Momentum& Qp) const {
    Momentum3<precision_type> result = p.p3();
    Momentum Q = _event->Q();
    result += (2*dot_product(Qp, p)/_Q2)*Q.p3();
    result -= (2*dot_product(Q + Qp, p)/dot_product(Q + Qp, Q + Qp))*(Q.p3() + Qp.p3());
    return result;
  }
  
  //----------------------------------------------------------------------
  // Double soft below
  //----------------------------------------------------------------------
  /// This function returns the double soft acceptance probability computed as the ratio
  /// acc_prob = (double soft approximation of shower)/(double soft approximation of full QCD)
  double ShowerPanScaleGlobal::Element::double_soft_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    typename ShowerPanScaleGlobal::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobal::EmissionInfo*>(emission_info_base));

    // make sure we start with decent defaults
    emission_info.flavour_swap_probability = 0.0;
    emission_info.colour_flow_swap_probability_gg = 0.0;
    emission_info.colour_flow_swap_probability_qq = 0.0;
    
    // only do something non-trivial if double soft corrections are
    // turned on
    if(!double_soft()) {
      assert(double_soft_overhead() == 1.0); 
      return 1.0;
    }

    // LuSi: we don't need this: the double_soft_fill_momenta() function should decide
    // this by itself based on neighbouring dipoles and born momenta.
    // If the base event only contains the two original particles then this is the first 
    // emission and there is no double soft correction
    //if(_event->size()==2)
    //  return 1.0/double_soft_overhead(); 

    DoubleSoftInfo ds_info;

    // first fill the momenta involved in the double-soft corrections
    if (!_double_soft_fill_momenta(emission_info_base, &ds_info)){
      return 1.0/double_soft_overhead();
    }

    // fill the shower histories
    if (!_double_soft_fill_weights(&ds_info)){
      return 1.0/double_soft_overhead();
    }

    // compute the flavour-swap probability
    emission_info.flavour_swap_probability = (_shower->qcd().gluon_uses_qqbar())
      ? _double_soft_swap_flavours_prob(emission_info_base, &ds_info) : 0.0;
    
    // Function that returns the acceptance probability for keeping the present colour connection.
    _double_soft_swap_colour_connections(emission_info_base, &ds_info,
                                         emission_info.colour_flow_swap_probability_gg,
                                         emission_info.colour_flow_swap_probability_qq);
    

    // compute the global acceptance probability
    double retval = to_double(ds_info.exact_weight/ds_info.shower_weight);

    // make sure that the value is in the expected range
    // AK Some points with extreme kinematics seem to violate the below bounds (they are NaN)
    if ((_shower->_double_soft_colour_channel == DoubleSoftColourAny) && ((retval > double_soft_overhead()) || (retval < 0.5))){
      cerr << "the computed double-soft probability (" << retval <<  ") is not between 0.5 and " << _shower->_double_soft_overhead << " as expected" << endl;
      cerr << "Event: " << endl;
      cerr << "p_new       = " << setprecision(40) << ds_info.p_new       << endl;
      cerr << "p_first     = " << setprecision(40) << ds_info.p_first     << endl;
      cerr << "p_aux_new   = " << setprecision(40) << ds_info.p_aux_new   << endl;
      cerr << "p_aux_first = " << setprecision(40) << ds_info.p_aux_first << endl;
      cerr << endl;
      cerr << "dn1    = " << ds_info.dn1 << endl;
      cerr << "dna    = " << ds_info.dna << endl;
      cerr << "dnb    = " << ds_info.dnb << endl;
      cerr << "d1a    = " << ds_info.d1a << endl;
      cerr << "d1b    = " << ds_info.d1b << endl;
      cerr << "dab    = " << ds_info.dab << endl;
      cerr << "dcross = " << ds_info.dcross;
      cerr << "dnQ    = " << 2.0*dot_product(ds_info.p_new,       _event->Q())/ds_info.p_new.E() << endl;
      cerr << "d1Q    = " << 2.0*dot_product(ds_info.p_first,     _event->Q())/ds_info.p_first.E() << endl;
      cerr << "daQ    = " << 2.0*dot_product(ds_info.p_aux_new,   _event->Q())/ds_info.p_aux_new.E() << endl;
      cerr << "dbQ    = " << 2.0*dot_product(ds_info.p_aux_first, _event->Q())/ds_info.p_aux_first.E() << endl;
      cerr << "dQQ    = " <<     dot_product(_event->Q(),     _event->Q()) << endl;
      cerr << "zn     = " << ds_info.p_new  .E()/(ds_info.p_first.E()+ds_info.p_new.E()) << endl;
      cerr << "z1     = " << ds_info.p_first.E()/(ds_info.p_first.E()+ds_info.p_new.E()) << endl;
 
      assert(false);
      return 0.0;
    }    

    return retval/double_soft_overhead();
  } 

  // double-soft-related KCMW correction (applied as an acceptance
  // probability, potentially w an overhead factor to be handled by
  // individual showers)
  double ShowerPanScaleGlobal::delta_K_CMW_acceptance(typename ShowerBase::EmissionInfo * emission_info_base) const{
    typename ShowerPanScaleGlobal::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobal::EmissionInfo*>(emission_info_base));
    if(!double_soft())       return 1.0;
    if(!double_soft_dKCMW()) return 1.0;

    auto element_ptr =  emission_info.element();

    double eta_skew;
    if (use_diffs()){
      // eta = -1/2 log(d^2/(4-d^2))
      precision_type d2 = emission_info.cached_dirdiff_3_minus_3bar.modpsq();
      precision_type four_minus_d2 = max(4-d2, 2*numeric_limit_extras<precision_type>::epsilon());
      eta_skew = to_double(-0.5*log_T(d2/four_minus_d2));
    } else {
      precision_type omc = one_minus_costheta(element_ptr->emitter(), element_ptr->spectator());
      precision_type two_minus_omc = max(2-omc, numeric_limit_extras<precision_type>::epsilon());
      eta_skew = to_double(-0.5*log_T(omc/two_minus_omc));
    }
    double eta1     = emission_info.lnb;
    double phi1     = emission_info.phi;
    // We damp the δKCMW far away from the soft region, where we anyways do not control anything.
    precision_type zemit    = emission_info.z_radiation_wrt_emitter;
    precision_type zspec    = emission_info.z_radiation_wrt_spectator;
    double damp     = to_double((1.0 - zemit) * (1.0 - zspec));

    double dk = damp * _dKCMW_interp(eta_skew, eta1, phi1);

    double as = qcd().alphasMSbar(element_ptr->alphas_lnkt(emission_info.lnv, emission_info.lnb));
    double x = dk*as/(2*M_PI);
    double p;
    switch (_deltaK_regularisation){
    case panscales::PanGlobalDeltaKRegularisationOption::Tanh:
      p = (1 + tanh(x))/_dKCMW_overhead;
      break;
    case panscales::PanGlobalDeltaKRegularisationOption::Linear:
      p = (x>-1) ? ((x<1) ? (1+x)/_dKCMW_overhead : 1) : 0;
      break;
    case panscales::PanGlobalDeltaKRegularisationOption::Tanh3:
      p = (x>=0)
        ? (1 + 3*tanh(x/3))/_dKCMW_overhead
        : (1 +   tanh(x  ))/_dKCMW_overhead;
      break;      
    case panscales::PanGlobalDeltaKRegularisationOption::PiecewiseTanh:
      p = ((x>-0.5) ? ((x<2) ? 1+x : tanh(x-2)+3) : 0.5*(tanh(2*x+1)+1))/_dKCMW_overhead;
      break;
    default:
      assert(false && "Unknown deltaK regularisation option");
    };
        
    assert((p<=1+numeric_limit_extras<double>::sqrt_epsilon()) && "Delta K_CMW acceptance propability not bounded by 1");
    return p;
  }


  //----------------------------------------------------------------------
  // material to handle double-soft corrections
  //----------------------------------------------------------------------

  /// fill the 4 momenta associated w the double-soft correction:
  ///   new        the newly radiated particles
  ///   first      the soft partner (new and first make the double-soft emissions)  
  ///   aux_new    the auxiliary momntum colour-connected to "new"
  ///   aux_first  the auxiliary momntum colour-connected to "first"
  ///
  /// mainly, this function decides at which end of the splitting
  /// dipole we have to take the partner gluon and the 2 auxiliarries
  ///
  /// WATCH OUT: If needed, this method overwrites
  /// emission_info->do_split_emitter so that the splitter points to
  /// p_first
  ///
  ///
  /// @return false if this element should not be associated 
  ///         with a double soft correction (in particular because 
  ///         it is the Born hard dipole), otherwise true
  bool ShowerPanScaleGlobal::Element::_double_soft_fill_momenta(
      typename ShowerBase::EmissionInfo * emission_info_base,
      DoubleSoftInfo * double_soft_info) const{
    typename ShowerPanScaleGlobal::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleGlobal::EmissionInfo*>(emission_info_base));

    // at this point, we have already handled the case with only 2
    // particles in the event.

    // double-soft corrections happen in the (phase-space) vicinity of
    // a soft gluon. We therefore have no corrections for a single qqbar
    // dipoles.
    if ((dipole().dipole_at_3_end()    == nullptr) &&
        (dipole().dipole_at_3bar_end() == nullptr)){
      return false;
    }

    // set the new emission
    double_soft_info->p_new = emission_info.radiation;
    double_soft_info->flav_init = emission_info.radiation_pdgid;

    // We want to make sure to never take hard Born partons as the
    // "first" partons in the double-soft correction: we check if we
    // need to invalidate either the 3 or the 3bar end of the current
    // emitting dipole
    int born1, born2; 
    _event->get_Born_indices(born1, born2);
    bool invalidate_3_end    = ((dipole().index_3()    == born1) || (dipole().index_3()    == born2));
    bool invalidate_3bar_end = ((dipole().index_3bar() == born1) || (dipole().index_3bar() == born2));

    // If we are splitting a "Born" colour line for the first time (i.e. if both ends
    // are unvalidated), we never correct for double-soft
    if (invalidate_3bar_end && invalidate_3_end) {
      return false;
    }

    // first treat the case of a gluon emission
    Momentum3<precision_type> vn1, vna, vnb, v1a, v1b, vab;
    if (emission_info.radiation_pdgid==21){
      // we decide whether we take the partner at the 3 or 3bar end of the dipole.
      //
      // If there are no "next" (splitting) dipole at either the 3 or
      // 3bar end of the current splitting dipole, there is a unique
      // assignment. Otherwise, we select the smallest of
      //    p_radiation.p_3_end    and    p_radiation.p_3bar_end
      // In the process, we make sure that we are not selecting an
      // invalidated (Born-level) parton.
      bool first_at_3_end = true;
      // Note that we can hae cases with only one neighbouring dipole
      // where this ed is being invalidated (e.g. a H->gg born-level
      // event where one of the ends undergoes a g->qqbar splitting,
      // or a qqbar event where the gluon connected to the q (or qbar)
      // splits into a qqbar pair). In this case we switch off the
      // double-soft correction.
      // 
      // Example of H->gg where one of the Born gluons has split to a
      // qqbar pair:
      // 
      //                                qbar = 3bar
      //                              /
      // (Born g) [INV]              /
      //  3     --------------------/
      //  3bar  --------------------\                                .
      //                             \                               .
      //                              \  q = 3 (Born) [INVALIDATED]
      // 
      if (dipole().dipole_at_3_end() == nullptr){
        // we should pick the particle at the 3bar end, unless it is invalidated
        if (invalidate_3bar_end) return false;
        first_at_3_end = false;
      } else if (dipole().dipole_at_3bar_end() == nullptr){
        // we should pick the particle at the 3 end, unless it is invalidated
        if (invalidate_3_end) return false;
        first_at_3_end = true; //< not strictly needed as already the default above
      } else {
        // if an end is invalidated, take the other one
        if (invalidate_3bar_end){ 
          first_at_3_end = true;
        } else if (invalidate_3_end){
          first_at_3_end = false;
        } else {
          // we have a (valid) dipole at both the 3 and 3bar ends
          //
          // compute the dot products at both ends
          //
          // AK: Another option would be to evaluate the ME and take
          // configuration that has maximal true ME.
          precision_type d3, d3bar;
          if (use_diffs()){
            d3    = dot_product_with_dirdiff(emission_info.radiation, emission_info.momentum_3_out(),
                                             emission_info.d_radiation_wrt_3()   -emission_info.d_3_end());
            d3bar = dot_product_with_dirdiff(emission_info.radiation, emission_info.momentum_3bar_out(),
                                             emission_info.d_radiation_wrt_3bar()-emission_info.d_3bar_end());
          } else {
            d3    = dot_product(emission_info.radiation, emission_info.momentum_3_out());
            d3bar = dot_product(emission_info.radiation, emission_info.momentum_3bar_out());
          }

          first_at_3_end = (d3<d3bar);
        }
      }

      //old version:
      //if ((dipole().dipole_at_3_end()    == nullptr && invalidate_3bar_end) ||
      //    (dipole().dipole_at_3bar_end() == nullptr && invalidate_3_end)) {
      //  return false;
      //}
      //else if (dipole().dipole_at_3_end() == nullptr || invalidate_3_end) {
      //  // we only have a dipole at the 3bar end
      //  first_at_3_end = false;
      //} else if (dipole().dipole_at_3bar_end() != nullptr && !invalidate_3bar_end){
      //  // we have a dipole at both the 3 and 3bar ends
      //  //
      //  // compute the dot products at both ends
      //  //
      //  // AK: Another option would be to evaluate the ME and take
      //  // configuration that has maximal true ME.
      //  precision_type d3    = dot_product(emission_info.radiation, emission_info.momentum_3_out());
      //  precision_type d3bar = dot_product(emission_info.radiation, emission_info.momentum_3bar_out());
      //  first_at_3_end = (d3<d3bar);
      //}  // otherwise, we only have a dipole at the 3 end and the "true"
      //   // default is fine

      if (first_at_3_end){
        // we have the following setup (in PanGlobal, "emitter = 3end"):
        //
        //                first
        //               3  3bar
        //                ||     
        //          new   ||
        //            \\  ||
        //    3bar  -----/  \----- 3
        // aux_new              aux_first
        //
        // The splitting dipole is (aux_new,first)
        assert(dipole().particle_3().pdgid()==21);
        double_soft_info->p_first     = emission_info.momentum_3_out();
        double_soft_info->p_aux_new   = emission_info.momentum_3bar_out();
        double_soft_info->p_aux_first = dipole().dipole_at_3_end()->particle_3();
        
        // We need to make sure that the "splitter" is set to use the 3 
        // end of the jk dipole. 
        // Recall that our convention is 3=emitter, 3bar=spectator.
        //
        //GS-NOTE-DS: do we want to postpone this? (I've put an
        //explicit note in the description)
        //
        // Update on 2022-12-12: faced w the following problem when
        // merging the master branch: an earlier call to
        // ShowerRunner::_select_channel_details fill in a
        // splitter_out_pdgid in emission_info. This is tight to the
        // dipole end that had initially beed selected by
        // _select_channel_details, so that resetting do_split_emitter
        // here is dangerous (we could have a qg dipole, selecting
        // initially the q as emitter, yielding
        // splitter_out_pdgid=1. If here we reset the emitter to be
        // the g, our call to update_event in the end would set the
        // pdgid of the gluon to 1 which is obviously wrong.
        //
        // My understanding is that we need the reset below this to
        // guarantee that double-soft flavour swaps are correctly
        // fixing the partner. Hence, instead of enforcing this here,
        // we'll do this in case of a flavour swap.
        //
        // Hence: commenting out the next line for now
        //
        //emission_info.do_split_emitter = true;
        emission_info.double_soft_partner_at_3_end = true;

        // cache angular distances w dirdiff
        if (use_diffs()){
          // Use 2*(1-cos(theta)) = ddiff^2
          // a few shortcuts (v_i_j = j-i; b==bout)
          const Momentum3<precision_type> &v_1_n    = emission_info.d_radiation_wrt_emitter();
          const Momentum3<precision_type> &v_a_n    = emission_info.d_radiation_wrt_spectator();
          const Momentum3<precision_type> &v_1_1out = emission_info.d_emitter_out;
          const Momentum3<precision_type> &v_a_aout = emission_info.d_spectator_out;
          const Momentum3<precision_type> &v_a_1    = dipole().dirdiff_3_minus_3bar;
          const Momentum3<precision_type> &v_1_b    = dipole().dipole_at_3_end()->dirdiff_3_minus_3bar;

          vn1 = v_1_1out-v_1_n;
          vna = v_a_aout-v_a_n;
          vnb = v_1_b-v_1_n;
          v1a = v_a_aout-v_a_1-v_1_1out;
          v1b = v_1_b-v_1_1out;
          vab = v_1_b+v_a_1-v_a_aout;
        }
      } else {
        // we have the following setup:
        //
        //                first
        //               3  3bar
        //                ||     
        //                ||   new
        //                ||  //    
        //    3bar  -----/  \----- 3
        // aux_first           aux_new
        //
        // The splitting dipole is (first,aux_new)
        assert(dipole().particle_3bar().pdgid()==21);
        double_soft_info->p_first     = emission_info.momentum_3bar_out();
        double_soft_info->p_aux_new   = emission_info.momentum_3_out();
        double_soft_info->p_aux_first = dipole().dipole_at_3bar_end()->particle_3bar();
        
        // We need to make sure that the "splitter" is set to use the 3bar 
        // end of the kj dipole. 
        // Recall that our convention is 3=emitter, 3bar=spectator.
        //
        // see comment above
        //
        //emission_info.do_split_emitter = false;
        emission_info.double_soft_partner_at_3_end = false;
        
        // cache angular distances w dirdiff
        if (use_diffs()){
          // Use 2*(1-cos(theta)) = ddiff^2
          // a few shortcuts (v_i_j = j-i; b==bout)
          const Momentum3<precision_type> &v_1_n    = emission_info.d_radiation_wrt_spectator();
          const Momentum3<precision_type> &v_a_n    = emission_info.d_radiation_wrt_emitter();
          const Momentum3<precision_type> &v_1_1out = emission_info.d_spectator_out;
          const Momentum3<precision_type> &v_a_aout = emission_info.d_emitter_out;
          const Momentum3<precision_type> &v_1_a    = dipole().dirdiff_3_minus_3bar;
          const Momentum3<precision_type> &v_b_1    = dipole().dipole_at_3bar_end()->dirdiff_3_minus_3bar;

          vn1 = v_1_1out-v_1_n;
          vna = v_a_aout-v_a_n;
          vnb = -(v_1_n+v_b_1);
          v1a = v_1_a+v_a_aout-v_1_1out;
          v1b = -(v_b_1+v_1_1out);
          vab = -(v_1_a+v_a_aout+v_b_1);
        }
      }
    } else {     //g->qqbar splitting
      // For a quark spitting the choice of partner is fixed
      //
      //      splitting at 3 end             splitting at 3bar end
      //
      //         first     new                    new     first
      //             3   3bar                        3   3bar        
      //    splitn    \  /                            \  /    splitn            
      //    dipole     ||                              ||     dipole 
      //               ||                              ||            
      //               ||                              ||              
      //    3bar -----/  \----- 3          3bar  -----/  \----- 3        
      // b=aux_first         a=aux_new    a=aux_new           b=aux_first

      // if the particle that splits into qqbar is one of the initial
      // hard gluons (in a H->gg event), do not set a double-soft correction
      if ((emission_info.splitter_index() == born1) || 
          (emission_info.splitter_index() == born2)) {
        return false;
      }

      // in all the other cases, we just need to know if the splitting
      // happens at the 3 or 3bar end of the dipole
      if(emission_info.splitter_is_3_end()){
        // we are emitting at the 3 end of the (aux_first,first)
        // dipole the emission should therefore be an anti-quark
        assert(emission_info.radiation_pdgid < 0);
        assert(dipole().particle_3().pdgid()==21);
        double_soft_info->p_first     = emission_info.momentum_3_out();
        double_soft_info->p_aux_new   = dipole().dipole_at_3_end()->particle_3();
        double_soft_info->p_aux_first = emission_info.momentum_3bar_out();
        //GS-NOTE-DS: this should always be true!
        //        emission_info.do_split_emitter = true;
        assert(emission_info.do_split_emitter);
        emission_info.double_soft_partner_at_3_end = true;

        // cache angular distances w dirdiff
        if (use_diffs()){
          // Use 2*(1-cos(theta)) = ddiff^2
          const Momentum3<precision_type> &v_1_n    = emission_info.d_radiation_wrt_emitter();
          const Momentum3<precision_type> &v_b_n    = emission_info.d_radiation_wrt_spectator();
          const Momentum3<precision_type> &v_1_1out = emission_info.d_emitter_out;
          const Momentum3<precision_type> &v_b_bout = emission_info.d_spectator_out;
          const Momentum3<precision_type> &v_b_1    = dipole().dirdiff_3_minus_3bar;
          const Momentum3<precision_type> &v_1_a    = dipole().dipole_at_3_end()->dirdiff_3_minus_3bar;

          vn1 = v_1_1out-v_1_n;
          vna = v_1_a-v_1_n;
          vnb = v_b_bout-v_b_n;
          v1a = v_1_a-v_1_1out;
          v1b = v_b_bout-v_b_1-v_1_1out;
          vab = v_b_bout-v_1_a-v_b_1; 
        }
      } else {
        // we are emitting at the 3bar end of the (first,aux_first)
        // dipole the emission should therefore be a quark
        assert(emission_info.radiation_pdgid > 0);
        assert(dipole().particle_3bar().pdgid()==21);
        double_soft_info->p_first     = emission_info.momentum_3bar_out();
        double_soft_info->p_aux_new   = dipole().dipole_at_3bar_end()->particle_3bar();
        double_soft_info->p_aux_first = emission_info.momentum_3_out();
        //GS-NOTE-DS: this should always be true!
        //        emission_info.do_split_emitter = false;
        assert(! emission_info.do_split_emitter);
        emission_info.double_soft_partner_at_3_end = false;
                
        if (use_diffs()){
          // Use 2*(1-cos(theta)) = ddiff^2
          const Momentum3<precision_type> &v_1_n    = emission_info.d_radiation_wrt_spectator();
          const Momentum3<precision_type> &v_b_n    = emission_info.d_radiation_wrt_emitter();
          const Momentum3<precision_type> &v_1_1out = emission_info.d_spectator_out;
          const Momentum3<precision_type> &v_b_bout = emission_info.d_emitter_out;
          const Momentum3<precision_type> &v_1_b    = dipole().dirdiff_3_minus_3bar;
          const Momentum3<precision_type> &v_a_1    = dipole().dipole_at_3bar_end()->dirdiff_3_minus_3bar;

          vn1 = v_1_1out-v_1_n;
          vna = -(v_a_1+v_1_n);
          vnb = v_b_bout-v_b_n;
          v1a = -(v_a_1+v_1_1out);
          v1b = v_1_b-v_1_1out+v_b_bout; 
          vab = v_1_b+v_b_bout+v_a_1;
        }
      }
    }

    // distance wo dirdiff
    if (use_diffs()) {
      double_soft_info->dn1 = vn1.modpsq(); 
      double_soft_info->dna = vna.modpsq();
      double_soft_info->dnb = vnb.modpsq(); 
      double_soft_info->d1a = v1a.modpsq();  
      double_soft_info->d1b = v1b.modpsq(); 
      double_soft_info->dab = vab.modpsq();
      
      // dcross = dot(d1b*(v1a+v2a) - d1a*(v1b+v2b)).v12 = dot(vcross,v12)
      double_soft_info->dcross = -dot_product_3(double_soft_info->d1b*(v1a+vna)-double_soft_info->d1a*(v1b+vnb),vn1);
    } else {
      double_soft_info->dn1 = 2.0*one_minus_costheta(double_soft_info->p_new,     double_soft_info->p_first);
      double_soft_info->dna = 2.0*one_minus_costheta(double_soft_info->p_new,     double_soft_info->p_aux_new);
      double_soft_info->dnb = 2.0*one_minus_costheta(double_soft_info->p_new,     double_soft_info->p_aux_first);
      double_soft_info->d1a = 2.0*one_minus_costheta(double_soft_info->p_first,   double_soft_info->p_aux_new);
      double_soft_info->d1b = 2.0*one_minus_costheta(double_soft_info->p_first,   double_soft_info->p_aux_first);
      double_soft_info->dab = 2.0*one_minus_costheta(double_soft_info->p_aux_new, double_soft_info->p_aux_first);
      double_soft_info->dcross = double_soft_info->dna*double_soft_info->d1b
                               - double_soft_info->d1a*double_soft_info->dnb;
    }
    
    return true;
  }

  // analytic double soft approximation for this shower (shower and exact weights)
  bool ShowerPanScaleGlobal::Element::_double_soft_fill_weights(
      DoubleSoftInfo * double_soft_info) const{
    // shorthand
    DoubleSoftInfo &dsi = *double_soft_info;
    
    // compute all the necessary dot products
    // 
    // short-hand notation:
    //  n = new
    //  1 = first
    //  a = aux_new
    //  b = aux_first
    //
    //GS-NOTE-DS: this will probably need to be adapted for dirdiff
    precision_type dn1 = dsi.dn1;
    precision_type dna = dsi.dna;
    precision_type dnb = dsi.dnb;
    precision_type d1a = dsi.d1a;
    precision_type d1b = dsi.d1b;
    precision_type dab = dsi.dab;

    // disable double-soft corrections in the collinear limit
    //
    //GS-NOTE-DS:
    // this interferes w spin corrections. We could take 2 approaches:
    //
    // 0. ignore spin (in which case the cut-off should have no effect
    //    after azimuthal average, i.e. for spin-independent
    //    quantities) [WHAT WE GO FOR NOW]
    //
    // 1. keep the current shower MEs (wo spin). Above the cut-off, we
    //    do not apply the spin acceptance (but still update the spin
    //    tree). Below the cut-off we do not apply DS corrections but
    //    treat the spin as at NLL [WHAT WE COULD DO CHEAPLY]
    //
    // 2. include the spin in the double-soft shpwer weights. This
    //    way, we keep the spin correlationas at all angles but only
    //    include DS corrections at angles above the cut-off (purely
    //    for numeroical stability reasons) [THIS IS WHAT WE SHOULD
    //    AIM FOR]


    // an alternative version of the cuts: we discard emissions which are 
    // - a rapidity deta=1/2 log(1/X) [X<<1] away from the 1st emission
    //   Say dmin1 = min(d1a,d1b)
    //       dmin2 = min(dna,dnb)
    //   We discard emissions for which
    //     dn1 < X dmin1                            [secondary]
    //     dmin2 < X dmin1  or  dmin2 > 1/X dmin1   [primary]
    // - a lnkt cutoff. Discard if
    //     (E2/E1)^2 min(d2a,d2b,dn1)/min(d1a,d1b) < X
    //
    // const precision_type double_soft_cut = 1e-16;
    // precision_type dmin1 = min(d1a,d1b);
    // if (dn1 < double_soft_cut * dmin1) return false;
    // precision_type dmin2 = min(dna,dnb);
    // if ((dmin2 < double_soft_cut * dmin1) ||
    //     (dmin1 < double_soft_cut * dmin2)) return false;
    // if (pow2(dsi.p_new.E()) * min(dn1,dmin2) < double_soft_cut * pow2(dsi.p_first.E()) * dmin1) return false;
    
    const double double_soft_max_angle_ratio = 1e-16;
    if (dn1/min(dna,dnb) < double_soft_max_angle_ratio) 
      return false;

    precision_type zn  = dsi.p_new  .E()/(dsi.p_first.E()+dsi.p_new.E());
    precision_type z1  = dsi.p_first.E()/(dsi.p_first.E()+dsi.p_new.E());

    // for numerical stability, we also impose a cut on z
    const double double_soft_max_z = 1e-16;
    if (min(z1,zn) < double_soft_max_z) 
      return false;    

    precision_type dcross = dsi.dcross;
    precision_type dnQ = 2.0*dot_product(dsi.p_new,       _event->Q())/dsi.p_new.E();
    precision_type d1Q = 2.0*dot_product(dsi.p_first,     _event->Q())/dsi.p_first.E();
    precision_type daQ = 2.0*dot_product(dsi.p_aux_new,   _event->Q())/dsi.p_aux_new.E();
    precision_type dbQ = 2.0*dot_product(dsi.p_aux_first, _event->Q())/dsi.p_aux_first.E();
    precision_type dQQ =     dot_product(_event->Q(),     _event->Q());

    
    
    // compute the shower weights
    //
    // we compute all flavour and colour channel
    // Each of them receives contributions from 2 histories:
    //  - the one where "first" is emitted before "new"
    //  - the one where "new"   is emitted before "first"
    //
    // We have a total of 8 configurations to consider
    //  0: g->gg, colour aux_new - new - first - aux_first, "first" then "new"
    //  1: g->gg, colour aux_new - new - first - aux_first, "new" then "first"
    //  2: g->gg, colour aux_new - first - new - aux_first, "first" then "new"
    //  3: g->gg, colour aux_new - first - new - aux_first, "new" then "first"
    //  4: g->qq, colour aux_new - new - first - aux_first, "first" then "new"
    //  5: g->qq, colour aux_new - new - first - aux_first, "new" then "first"
    //  6: g->qq, colour aux_new - first - new - aux_first, "first" then "new"
    //  7: g->qq, colour aux_new - first - new - aux_first, "new" then "first"
    //
    // 2i and 2i+1 are summed.
    // i and i+2 are the same up to a swap of "new" and "first" in the
    // ME we therefore introduce a function that computes a single
    // colour flow since g->gg and g->qq only differ by a weight
    // factor, both are computed at the same time
    // Default colour ordering for quarks is "a1nb"
    //
    // Comments:
    // - the second call only differs from the first one by the inversion 
    //   of "first" (1) and "new" (n)
    // - note that, in the quark case, the first call set the "a1nb" version. 
    //   In a nutshell, this is associated to our specific choice of dipole 
    //   structure where, for a quark (or antiquark) emission, the new emission 
    //   is associted with the previous or next dipole (instrad of the splitting 
    //   dipole)
    _double_soft_set_weight_shower(dn1, d1a, d1b, dna, dnb, dab,
                                   d1Q, dnQ, daQ, dbQ, dQQ, z1, zn,
                                   &dsi.shower_weight_gg_an1b,
                                   &dsi.shower_weight_qq_a1nb);
    _double_soft_set_weight_shower(dn1, dna, dnb, d1a, d1b, dab,
                                   dnQ, d1Q, daQ, dbQ, dQQ, zn, z1,
                                   &dsi.shower_weight_gg_a1nb,
                                   &dsi.shower_weight_qq_an1b);
    //&dsi.shower_weight_qq_same);
    
    if(! _shower->qcd().gluon_uses_qqbar()){
      dsi.shower_weight_qq_an1b = dsi.shower_weight_qq_a1nb = 0.0;
    }

    dsi.shower_weight_gg = dsi.shower_weight_gg_an1b + dsi.shower_weight_gg_a1nb;
    dsi.shower_weight_qq = dsi.shower_weight_qq_an1b + dsi.shower_weight_qq_a1nb;
    dsi.shower_weight = dsi.shower_weight_gg + dsi.shower_weight_qq;

    // compute the exact weights:  this is the full QCD double soft ME
    //
    // Note: taken out a factor 4 CA^2 (both here and in the exact ME)
    // compared to initial expressions
    dsi.exact_weight_qq_an1b = dsi.exact_weight_qq_a1nb = 0.0;

    if (_shower->_double_soft_colour_channel == DoubleSoftColourAny){
      // gg part
      // an1b-colour: a=a, b=n, c=1, d=b
      dsi.exact_weight_gg_an1b = _shower->qcd().ant_sbc(dna,d1a,dab,dn1,dnb,d1b, zn,z1, -dcross);
      // opposite colour: a=a, b=1, c=n, d=b
      dsi.exact_weight_gg_a1nb = _shower->qcd().ant_sbc(d1a,dna,dab,dn1,d1b,dnb, z1,zn, dcross);
      // qqbar part
      if (_shower->qcd().gluon_uses_qqbar()){
        dsi.exact_weight_qq_an1b = dsi.exact_weight_qq_a1nb = 
          4/pow2(_shower->qcd().CA()) * _shower->qcd().ant_qqbar_sqp_qbp(dab, d1a, dna, d1b, dnb, dn1, zn, z1, dcross);
      }
    } else if (_shower->_double_soft_colour_channel == DoubleSoftColourCF){
      dsi.exact_weight_gg_an1b = _shower->qcd().ant_sbc_fullCF(dna,d1a,dab,dn1,dnb,d1b, zn,z1, -dcross);
      dsi.exact_weight_gg_a1nb = _shower->qcd().ant_sbc_fullCF(d1a,dna,dab,dn1,d1b,dnb, z1,zn, dcross);
    } else if (_shower->_double_soft_colour_channel == DoubleSoftColourCA){
      dsi.exact_weight_gg_an1b = _shower->qcd().ant_sbc_fullCA(dna,d1a,dab,dn1,dnb,d1b, zn,z1, -dcross);
      dsi.exact_weight_gg_a1nb = _shower->qcd().ant_sbc_fullCA(d1a,dna,dab,dn1,d1b,dnb, z1,zn, dcross);
    } else {
      dsi.exact_weight_gg_an1b = dsi.exact_weight_gg_a1nb = 0.0;
      if (_shower->qcd().gluon_uses_qqbar()){
        dsi.exact_weight_qq_an1b = dsi.exact_weight_qq_a1nb = 
          4/pow2(_shower->qcd().CA()) * _shower->qcd().ant_qqbar_sqp_qbp(dab, d1a, dna, d1b, dnb, dn1, zn, z1, dcross);
      }
    }
    
    
    dsi.exact_weight_gg = dsi.exact_weight_gg_an1b + dsi.exact_weight_gg_a1nb;
    dsi.exact_weight_qq = dsi.exact_weight_qq_an1b + dsi.exact_weight_qq_a1nb;
    dsi.exact_weight = dsi.exact_weight_gg + dsi.exact_weight_qq;

    //std::cout << "DSratio: " << setprecision(15) << dsi.exact_weight/dsi.shower_weight << endl
    //          << "DSemissiona: " << dsi.p_aux_new << endl
    //          << "DSemissionb: " << dsi.p_aux_first << endl
    //          << "DSemission1: " << dsi.p_first << endl
    //          << "DSemissionn: " << dsi.p_new << endl
    //          << "DSinfo: "
    //          << dsi.exact_weight_gg_an1b   << " " << dsi.exact_weight_gg_a1nb << " "
    //          << dsi.exact_weight_qq_an1b   << " " << dsi.exact_weight_qq_a1nb << "    "
    //          << dsi.shower_weight_gg_an1b  << " " << dsi.shower_weight_gg_a1nb << " "
    //          << dsi.shower_weight_qq_an1b  << " " << dsi.shower_weight_qq_a1nb << endl;

    return true;
  }
  
  /// analytic double soft approximation for this shower.
  ///
  /// Note: taken out a factor 4 CA^2 (both here and in the exact ME)
  /// compared to initial expressions
  void ShowerPanScaleGlobal::Element::_double_soft_set_weight_shower(
      precision_type &d12, precision_type &d1a,
      precision_type &d1b, precision_type &d2a,
      precision_type &d2b, precision_type &dab,
      precision_type &d1Q, precision_type &d2Q,
      precision_type &daQ, precision_type &dbQ,
      precision_type &dQQ,
      precision_type &z1, precision_type &z2,
      precision_type * weight_gg,
      precision_type * weight_qq) const{
    (*weight_gg) = 0.0;
    (*weight_qq) = 0.0;
    //    assert(_shower->qcd().CA_is_2CF() && "Double soft code is currently only valid in large NC limit");
    // compared to the previous version: ilkj => a21b
    //const double CF = _shower->qcd().CF();
    //const double CA = _shower->qcd().CA();
    //const double nfTR = _shower->qcd().nf()*_shower->qcd().TR();
    const double beta = _shower->_beta;
    
    // history where 1 is emitted before 2
    precision_type lnb1     = 0.5*log((daQ/dbQ) * (d1b/d1a));
    precision_type kappa1sq = d1b * ((z1*d1a+z2*d2a)/d1a) * ((z1*d1a+z2*d2a)/dab);
    precision_type lnrho1   = 0.5*beta*log(daQ*dbQ/(dQQ*dab));
    precision_type lnv1 = 0.5*log(kappa1sq/dQQ) - lnrho1 - beta*fabs(lnb1);

    precision_type lnb2     = 0.5*log((daQ/d1Q) * (d12/d2a)); //the gluon(1) is the 3bar end
    precision_type kappa2sq = z2*z2*(d2a/d1a) * d12;
    precision_type lnrho2   = 0.5*beta*log(daQ*d1Q/(dQQ*d1a));
    precision_type lnv2 = 0.5*log(kappa2sq/dQQ) - lnrho2 - beta*fabs(lnb2); // factor log(E_{1+2}/Q) removed
    if(lnv2 < lnv1){
      precision_type jac = (dab/d1b)/(z2*d2a) * (d1a/(z1*d1a+z2*d2a));
      // when using "ant" for the exact QCD
      //precision_type jac = sab/(s1b * (s1a+s2a)) * s1a/(s2a*s12);
      // Compared to this version, we multiply by
      //    s12 (E1+E2)^2 
      // (as done in ant_sb compared to the "raw" ant) in order to deal
      // with dimensionless quantities

      // step 1: replace sij -> Ei Ej dij (everywhere)
      //                 s1  -> Ei diQ (where diQ has Energy dim)
      //                 Q2  -> dQQ (E^2 dim)
      // step 2: multiply the Jacobian by s12 (E1+E2)^2
      // step 3: one can neglect a common "constant" term in lnv1, lnv2 because we only need to know which is larger
      // step 4: Ea and Eb should drop out everywhere
      // step 5: E1 and E2 should only appear as Ei/(E1+E2) (which is zi)

      precision_type sum  = z1*d1a+z2*d2a;
      precision_type zg   = z2*d2a/sum;   // 1 -z = pl.piTilde/(piTilde.pkTilde) ~ pl.pi/(pi.(pk+pl))
      precision_type omzg = z1*d1a/sum;
      precision_type gp2 = f_fcn(to_double( lnb2));
      precision_type gm2 = f_fcn(to_double(-lnb2));

      // (*weight_gg) += jac * (gp2 + gm2 * 0.5*(1+omzg*omzg*omzg));
      // (*weight_qq) += jac * gm2 * nfTR/CA * zg*omzg*omzg;
      precision_type pg = _shower->qcd().splitting_zhalfPg2gg_normalised(zg);
      precision_type pq = _shower->qcd().splitting_zhalfPg2qqbar_normalised(zg);
      
      if (_shower->_split_dipole_frame){
        (*weight_gg) += jac * pg;
        (*weight_qq) += jac * pq;
      } else {
        if (_shower->_additive_branching) {
          (*weight_gg) += jac * (gp2 + gm2 * pg);
          (*weight_qq) += jac * gm2 * pq;
        } else {
          (*weight_gg) += jac * (pg + gp2 * pq);
          (*weight_qq) += jac * gm2 * pq;
        }
      }

      if (! isfinite(*weight_qq)){
        std::cout << "history1" << std::endl;
        std::cout << "  lnb1 = " << lnb1 << ", kappa1sq = " << kappa1sq << ", lnrho1 = " << lnrho1 << ", lnv1 = " << lnv1 << std::endl;
        std::cout << "  lnb2 = " << lnb2 << ", kappa2sq = " << kappa2sq << ", lnrho2 = " << lnrho2 << ", lnv2 = " << lnv2 << std::endl;
        std::cout << "  zg = " << zg << ", omzg = " << omzg << ", jac = " << jac << std::endl;
        std::cout << "  gp = " << gp2 << ", gm = " << gm2 << endl;
        std::cout << "  --> weight_qq = " << (*weight_qq) << std::endl;
      }
    }


    // history where 2 is emitted before 1
    lnb1     = 0.5*log((daQ/dbQ * d2b/d2a));
    kappa1sq = d2a * ((z2*d2b+z1*d1b)/d2b) * ((z2*d2b+z1*d1b)/dab);
    lnrho1   = 0.5*beta*log(daQ*dbQ/(dQQ*dab));
    lnv1     = 0.5*log(kappa1sq/dQQ) -lnrho1-beta*fabs(lnb1);
    
    lnb2     = -0.5*log( (dbQ/d2Q * d12/d1b)); //the gluon(1) is the 3 end
    kappa2sq = z1*z1 * (d1b/d2b) * d12;
    lnrho2   = 0.5*beta*log(dbQ*d2Q/(dQQ*d2b));
    lnv2     = 0.5*log(kappa2sq/dQQ) -lnrho2-beta*fabs(lnb2);

    if(lnv2 <= lnv1){
      precision_type jac = (dab/d2a)/(z1*d1b) * (d2b/(z1*d1b+z2*d2b));
      // when using "ant" for the exact QCD
      //precision_type jac = sab/(s2a*(s2b+s1b)) * s2b/(s1b*s12);

      precision_type sum  = z2*d2b+z1*d1b;
      precision_type zg   = z1*d1b/sum; // 1 -z = pk.pjTilde/(pjTilde.plTilde) ~ pk.pj/(pj.(pk+pl))
      precision_type omzg = z2*d2b/sum;

      precision_type gp2 = f_fcn(to_double( lnb2));
      precision_type gm2 = f_fcn(to_double(-lnb2));

      precision_type pg = _shower->qcd().splitting_zhalfPg2gg_normalised(zg);
      precision_type pq = _shower->qcd().splitting_zhalfPg2qqbar_normalised(zg);
      
      if (_shower->_split_dipole_frame){
        (*weight_gg) += jac * pg;
        (*weight_qq) += jac * pq;
      } else {
        if (_shower->_additive_branching) {
          (*weight_gg) += jac * (gm2 + gp2 * pg);
          (*weight_qq) += jac * gp2 * pq;
        } else {
          (*weight_gg) += jac * (pg + gm2 * pq);
          (*weight_qq) += jac * gp2 * pq;
        }
      }
      if (! isfinite(*weight_qq)){
        std::cout << "history2" << std::endl;
        std::cout << "  lnb1 = " << lnb1 << ", kappa1sq = " << kappa1sq << ", lnrho1 = " << lnrho1 << ", lnv1 = " << lnv1 << std::endl;
        std::cout << "  lnb2 = " << lnb2 << ", kappa2sq = " << kappa2sq << ", lnrho2 = " << lnrho2 << ", lnv2 = " << lnv2 << std::endl;
        std::cout << "  zg = " << zg << ", omzg = " << omzg << ", jac = " << jac << std::endl;
        std::cout << "  gp = " << gp2 << ", gm = " << gm2 << endl;
        std::cout << "  --> weight_qq = " << (*weight_qq) << std::endl;
      }
    }

    // insert the factor 64 that was previously in the callee (->16 given the 4CA factor taken out)
    (*weight_gg) *= 16;
    (*weight_qq) *= 16;
  }

  /// compute the probability for a flavour swap
  ///
  ///GS-NOTE-DS: we could think of filling em_info wo returnig the probability
  double ShowerPanScaleGlobal::Element::_double_soft_swap_flavours_prob(
      typename ShowerBase::EmissionInfo * emission_info,
      DoubleSoftInfo * double_soft_info) const{
    // shorthand
    DoubleSoftInfo &dsi = *double_soft_info;

    double w_exact, w_shower;
    if(emission_info->radiation_pdgid == 21){
      w_shower = to_double(dsi.shower_weight_gg/dsi.shower_weight);
      w_exact  = to_double(dsi.exact_weight_gg /dsi.exact_weight );
    } else {
      w_shower = to_double(dsi.shower_weight_qq/dsi.shower_weight);
      w_exact  = to_double(dsi.exact_weight_qq /dsi.exact_weight );
    }
    return (w_shower > w_exact) ? 1.0 - w_exact/w_shower : 0.0;
  }

  /// compute the probabilities for a colour swap
  /// We have 2 probabilities depending on the flavour of the splitting
  ///
  ///GS-NOTE-DS: we could think of filling em_info wo passing the last 2 arguments
  void ShowerPanScaleGlobal::Element::_double_soft_swap_colour_connections(
     typename ShowerBase::EmissionInfo * emission_info, DoubleSoftInfo * double_soft_info,
     double &colour_swap_prob_gg, double &colour_swap_prob_qq) const{

    // shorthand
    DoubleSoftInfo &dsi = *double_soft_info;
    
    // the shower weight involves the initial pdgid, i.e. the one
    // _before_ a potential flavour swap
    //
    // Note that the colour flow is inverted between the quark and
    // gluon channels. This means the following. The algorithm works
    // by (i) applying an overall double-soft reweighting (ii)
    // potentially flipping the flavour (iii) potentially flipping the
    // colour flow. If at step (ii) we have had a flavour swap, we
    // actually also flip the colour flow. So we need to take this
    // into account in the colour-flow swapping probabilty.  In this
    // case, onr convention is that the shower has (by construction)
    // given us a an1b colour flow, which after the flavour swap
    // becomes an a1nb colour flow, so we need to take the swapped
    // (i.e. a1nb) "exact" colour flow as a reference for a potential
    // colour-flow swap
    precision_type frac_shower, frac_exact;
    if(emission_info->radiation_pdgid == 21){ // shower initially give a gluon
      // initial gluon (an1b=a21b) -> gluon (an1b=a21b) after flavour assignment
      frac_shower = dsi.shower_weight_gg_an1b/dsi.shower_weight_gg;
      frac_exact  = dsi. exact_weight_gg_an1b/dsi. exact_weight_gg;
      colour_swap_prob_gg = max(0.0, to_double(1.0-frac_exact/frac_shower));
      // initial gluon (an1b=a21b) -> quark (a1nb=b21a) after flavour assignment
      frac_shower = dsi.shower_weight_gg_an1b   /dsi.shower_weight_gg;
      frac_exact  = dsi. exact_weight_qq_a1nb/dsi. exact_weight_qq;
      colour_swap_prob_qq = max(0.0, to_double(1.0-frac_exact/frac_shower));
    } else { 
      // initial quark (an1b=a21b) -> gluon (a1nb=a21b) after flavour assignment
      frac_shower = dsi.shower_weight_qq_an1b   /dsi.shower_weight_qq;
      frac_exact  = dsi. exact_weight_gg_a1nb/dsi. exact_weight_gg;
      colour_swap_prob_gg = max(0.0, to_double(1.0-frac_exact/frac_shower));
      // initial quark (an1b=a21b) -> quark (an1b=a21b) after flavour assignment
      frac_shower = dsi.shower_weight_qq_an1b/dsi.shower_weight_qq;
      frac_exact  = dsi. exact_weight_qq_an1b/dsi. exact_weight_qq;
      colour_swap_prob_qq = max(0.0, to_double(1.0-frac_exact/frac_shower));
    }
  }

} // namespace panscales



// Implementations for alternative mappings (should go in ShowerPanScalesGlobal::Element::do_kinematics
      
      // OPTION5: give up on beta=1, do the minimal modificaton
      //compared to the existing PanGlobal
      //   pi = r (1-ak) pitilde
      //   pj = r (1-bk) pjtilde
      //   pk = r (ak pitilde + bk pjtilde - kperp)
      // solving r to get Qbar^2=Q^2 gives
      //    (sij-kt^2) epsilon^2 + (si+sj-2 kper.Q) epsilon - (2kperp.Q+kt^2) = 0
      // precision_type ktQ = 2*dot_product(_event->Q(), perp);
      // precision_type kt2 = pow2(kt);
      // precision_type a = _dipole_m2 - kt2;
      // precision_type b = _sitilde + _sjtilde - ktQ - 2*kt2;
      // precision_type c = - ktQ - kt2;
      // assert(b*b>4*a*c);
      // // series expansion:
      // //   [sqrt(b^2-4ac)-b]/(2a)
      // //   = b/(2a) [sqrt(1-4ac/b^2)-1]
      // //   ~ -c/b
      // precision_type r_minus_one;
      // if (std::abs(4*a*c)<numeric_limit_extras<precision_type>::sqrt_epsilon()*b*b){
      //   precision_type small = a*c/(b*b);
      //   r_minus_one = (-c/b)*(1+small*(1+small*(2+small*(5+small*14*(1+3*small)))));
      // } else {
      //   r_minus_one = (sqrt(b*b-4*a*c)-b)/(2*a);
      // }
      // emission_info.r = 1 + r_minus_one;
      // assert(emission_info.r>0);
      
      // emitter_out3   = emission_info.r * (1-ak) * rp.emitter  .p3();
      // spectator_out3 = emission_info.r * (1-bk) * rp.spectator.p3();
      // radiation3     = emission_info.r * (ak * rp.emitter.p3() + bk * rp.spectator.p3() - perp.p3());
      
      // emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
      // emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
      // emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
      
      // emission_info.Qbar =
      //   Momentum::fromP3M2(_event->Q().p3()
      //                      + rp.rotn_from_z * (r_minus_one * (rp.emitter.p3() + rp.spectator.p3())
      //                                        - emission_info.r * perp.p3()), _Q2);

      // OPTION4: rescale the "emitter" (Note: rescaling the
      //"spectator" would not work because that would amout to
      //rescaling a first soft gluon for a second hard-collinear
      //emission close to the quark would affet this 1st gluon beyond
      //a power correction)
      //Post-test note: this simply fails (r<0) for the qbar-g dipole w beta=1 
      
      // OPTION3: rescale all components of all 3 momenta by r EXCEPT
      //-kt in pk.
      //  pi = r (1-f    ak) pitilde
      //  pj = r (1-fbar bk) pjtilde
      //  pk = r (ak pitilde + bk pjtilde) - kperp
      //
      //This would simplify things a little bit but would
      //still give a quartic equation for g->QQ. Ignore this option
      //for now.
      

      // OPTION2: rescale all components of all 3 momenta by r (would
      //give a quartic equation for massive g->QQbar splittings)
      //  pi = r (1-f    ak) pitilde
      //  pj = r (1-fbar bk) pjtilde
      //  pk = r (ak pitilde + bk pjtilde - kperp)
      //
      // precision_type xa = 1-  f*ak;
      // precision_type xb = 1-omf*bk;
      // precision_type ya = omf*ak;
      // precision_type yb =   f*bk;
      // precision_type za = 1+ya;
      // precision_type zb = 1+yb;
      // precision_type dkQ = 2*dot_product(_event->Q(), perp)/_dipole_m2;
      // precision_type kt2 = pow2(kt)/_dipole_m2;
      // precision_type siQ = _sitilde/_dipole_m2;
      // precision_type sjQ = _sjtilde/_dipole_m2;
      // 
      // precision_type a = za * zb - kt2;
      // precision_type b = za*siQ + zb*sjQ-dkQ-2*kt2+(ya*zb+yb*za);
      // precision_type c = ya*siQ+yb*sjQ-dkQ-kt2+ya*yb;
      // assert(b*b>4*a*c);
      // // series expansion:
      // //   [sqrt(b^2-4ac)-b]/(2a)
      // //   = b/(2a) [sqrt(1-4ac/b^2)-1]
      // //   ~ -c/b
      // precision_type r_minus_one;
      // if (4*a*c<numeric_limit_extras<precision_type>::sqrt_epsilon()*b*b){
      //   precision_type small = a*c/(b*b);
      //   r_minus_one = (-c/b)*(1+small*(1+small*(2+small*(5+small*14*(1+3*small)))));
      // } else {
      //   r_minus_one = (sqrt(b*b-4*a*c)-b)/(2*a);
      // }
      // //precision_type r_minus_one = (sqrt(b*b-4*a*c)-b)/(2*a);
      // emission_info.r = 1 + r_minus_one;
      // assert(emission_info.r>0);
      // 
      // emitter_out3   = emission_info.r * xa * rp.emitter  .p3();
      // spectator_out3 = emission_info.r * xb * rp.spectator.p3();
      // radiation3     = emission_info.r * (ak * rp.emitter.p3() + bk * rp.spectator.p3() - perp.p3());
      // 
      // emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
      // emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
      // emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
      // 
      // emission_info.Qbar =
      //   Momentum::fromP3M2(_event->Q().p3()
      //                      + rp.rotn_from_z * ((r_minus_one*za + ya) * rp.emitter.p3()
      //                                        + (r_minus_one*zb + yb) * rp.spectator.p3()
      //                                        - emission_info.r * perp.p3()), _Q2);

      // OPTION1: rescale all components of pi and pj by r:
      //  pi = r (1-f    ak) pitilde
      //  pj = r (1-fbar bk) pjtilde
      //  pk = ak pitilde + bk pjtilde - kperp
      //Yields fairly simple equations, easily generalised w
      //masses. But sometimes gives r<0. Q: can we discard these point
      //as non-physical?
      // 
      // precision_type xa = 1-  f*ak;
      // precision_type xb = 1-omf*bk;
      // precision_type a = xa * xb;
      // precision_type b = xa*(f*bk + _sitilde/_dipole_m2) + xb*(omf*ak + _sjtilde/_dipole_m2);
      // //precision_type c = (-2*dot_product(_event->Q(), rp.rotn_from_z * perp)-kt*kt + omf*ak*_sitilde + f*bk*_sjtilde + f*omf*ak*bk*_dipole_m2)/_dipole_m2;
      // precision_type c = (-2*dot_product(_event->Q(), perp)-kt*kt + omf*ak*_sitilde + f*bk*_sjtilde + f*omf*ak*bk*_dipole_m2)/_dipole_m2;
      // assert(b*b>4*a*c);
      // // series expansion:
      // //   [sqrt(b^2-4ac)-b]/(2a)
      // //   = b/(2a) [sqrt(1-4ac/b^2)-1]
      // //   ~ -c/b
      // // precision_type r_minus_one = (4*a*c<numeric_limit_extras<precision_type>::sqrt_epsilon()*b*b)
      // //   ? (-c/b)
      // //   : (sqrt(b*b-4*a*c)-b)/(2*a);
      // precision_type r_minus_one = (sqrt(b*b-4*a*c)-b)/(2*a);
      // emission_info.r = 1 + r_minus_one;
      // // this requires to add the following line to the header
      // //virtual bool do_kinematics_always_true_if_accept() const override{ return !_rescale_dipole; }
      // if (emission_info.r<=0) return false;
      // 
      // emitter_out3   = emission_info.r * xa * rp.emitter  .p3();
      // spectator_out3 = emission_info.r * xb * rp.spectator.p3();
      // radiation3     = (ak * rp.emitter.p3() + bk * rp.spectator.p3() - perp.p3());
      // 
      // emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
      // emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
      // emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
      // 
      // emission_info.Qbar = Momentum::fromP3M2(_event->Q().p3()
      //   + rp.rotn_from_z * ((r_minus_one*xa + omf*ak) * rp.emitter.p3()
      //                     + (r_minus_one*xb +   f*bk) * rp.spectator.p3()
      //                     - perp.p3()), _Q2);
      // //emission_info.Qbar = _event->Q() + rp.rotn_from_z *(- rp.emitter - rp.spectator + emission_info.emitter_out + emission_info.spectator_out + emission_info.radiation);
      // if (emission_info.Qbar.m2()/_Q2-1 > numeric_limit_extras<precision_type>::sqrt_epsilon()){
      //   cout << "Q2 difference of " << emission_info.Qbar.m2()/_Q2-1 << endl
      //        << "newQ: " << emission_info.Qbar << endl
      //        << "oldQ: " << _event->Q() << endl
      //        << "emit: " << rp.emitter << endl
      //        << "spec: " << rp.spectator << endl
      //        << "siQ = " << _sitilde << ", sjQ = " << _sjtilde << ", m2 = " << _dipole_m2 << endl
      //        << "ak = " << ak << ", bk = " << bk
      //        << ", xa = " << xa << ", xb = " << xb
      //        << ", f = " << f << ", r-1 = " << r_minus_one
      //        << ", a = " << a << ", b = " << b << ", c = " << c << ", d2 = " << b*b-4*a*c << endl;
      //   cout << "-> emit " << emission_info.emitter_out   << endl;
      //   cout << "-> radn " << emission_info.radiation     << endl;
      //   cout << "-> spec " << emission_info.spectator_out << endl;
      //   cout << *_event;
      // }
#include "autogen/auto_ShowerPanScaleGlobal_global-cc.hh"
