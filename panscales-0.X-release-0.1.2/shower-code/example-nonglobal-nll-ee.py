#!/usr/bin/env python3
"""
This script shows how to use example-nonglobal-nll-ee.cc in order to make a
single-logarithmic test of the energy flow in a rapidity slice.

- it compiles the code with the doubleexp numerical type
- it runs the shower for a tiny value of alphas. 
- it then generates a pdf file with a plot showing the result.

Run times are
- 2100 core seconds on Intel(R) Xeon(R) CPU E5-2643 v4 @ 3.40GHz 
  (10 minutes with --njobs 4)
- 800 core seconds on an Mac M2Pro (2 minutes with --njobs 10)

Usage: ./example-nonglobal-nll-ee.py [-h] [--njobs NJOBS] [--shower SHOWER]
options:
  -h, --help       show this help message and exit
  --njobs NJOBS    number of jobs to run in parallel (default: all available)
  --shower SHOWER  shower to use, (default panglobal-beta0.0, one alternative is panlocal-beta0.5)
  --only-plots     assuming you have already done the runs, only (re)produce the plots
"""

import sys
import os
import subprocess
from multiprocessing import Pool
import argparse

import matplotlib as mpl
mpl.use('Agg') # avoids requirement to have DISPLAY set
import matplotlib.pyplot as plt
from   matplotlib.backends.backend_pdf import PdfPages
from   matplotlib.ticker import ScalarFormatter
from   matplotlib import cycler
from   matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import copy

sys.path.insert(0, os.path.dirname(__file__) + "/../submodules/AnalysisTools/python")
from hfile       import *

#----------------------------------------------------------------------
# Configuration.
#
# The shower will be run for a selected set of alphas values
#  
alphass = [ 1e-9]
nevents = [ 10000]
nruns   = [ 8]

parser = argparse.ArgumentParser(description='Runs a single-log test for the energy distribution in a rapidity slice')
parser.add_argument('--njobs', '-j', type=int, default=None,
                    help='number of jobs to run in parallel')
parser.add_argument('--shower', default="panglobal-beta0.0", help='shower to use, e.g. panglobal-beta0.0 or panlocal-beta0.5')
parser.add_argument("--only-plots", help="assuming you have already done the runs, only (re)produce the plots", action="store_true")
args = parser.parse_args()

njobs                   = args.njobs  # number of jobs to be run in parallel
target_lambda           = -0.5        # targetted value of alphas L
lnobs_margin            = -10         # additional security in lnvmin at the observable boundary
slice_maxrap            = 1.0         # extent of the tested rapidity slice
lnktcut                 = -0.501e9    # maximum value of L
half_central_rap_window = 11          # generate emissions at central rapidity
outbase                 = f"example-results"
outdir                  = f"{outbase}/non-global-test/{args.shower}"   # output folder
build_dir               = "build-doubleexp" 

# get the command-line to run
def get_command(alphas, nev, iseq):
    shower = args.shower
    shower = shower.replace("-beta", " -beta ")
    out_res = f"{outdir}/runs/lambda{target_lambda}-alphas{alphas}-rseq{iseq}.res"
    out_log = f"{outdir}/runs/lambda{target_lambda}-alphas{alphas}-rseq{iseq}.log"
    for f in [out_res, out_log]:
        if os.path.isfile(f): os.remove(f)
    return f"{build_dir}/example-nonglobal-nll-ee -Q 1.0 -shower {shower} -nloops 2 -colour CATwoCF"\
           f" -strategy CentralRap -half-central-rap-window {half_central_rap_window}"\
           f" -lambda-obs-min {target_lambda} -alphas {alphas} -lnkt-cutoff {lnktcut} "\
           f" -slice-maxrap {slice_maxrap} -ln-obs-margin {lnobs_margin}"\
           f" -spin-corr off -nev {nev} -rseq {iseq}1 -out {out_res} "\
           f"> {out_log}"

#----------------------------------------------------------------------
def main():
    print("--- Running the example-nonglobal-nll-ee.py script  ---")
    print("It tests shower single-log (NLL/SL) accuracy for energy flow in a slice")
    print("After the build step, the expected run time is 1-2 minutes on a multicore machine using --njobs 8")
    # this is meant to run from the directory where the script is!
    script_path = os.path.dirname(os.path.abspath( __file__ ))
    os.chdir(script_path)

    # STEP 0: make sure the code is built and the result directory
    #         exists
    if not args.only_plots: 
        
        build_code()
        if not os.path.isdir(outdir+"/runs"):
            os.makedirs(outdir+"/runs")
        print(f"run files will be placed in {outdir}/runs")

        # STEP 1: run the shower code
        run_shower()
        
        # STEP 2: combine the runs
        combine_runs()
        
    # STEP 3: plot
    do_plot()
    
#----------------------------------------------------------------------
# STEP 1: run the shower code
def run_shower():
    print ("------------------------------------------------------------")
    print ("--- RUNNING THE SHOWER FOR THE SELECTED VALUES OF ALPHAS ---")
    print ("------------------------------------------------------------")
    if (njobs != None): print (f"Running with {njobs} jobs")
    else: print (f"Running with {os.cpu_count()} jobs")
    pool = Pool(njobs)

    for alphas, nev, nrun in zip(alphass, nevents, nruns):
        for irun in range(1, nrun+1):
            print (f"queuing command for alphas={alphas} with {nev} events and seed {irun}")
            command = get_command(alphas, nev, irun)
            pool.apply_async(subprocess.run,
                             args = [command,],
                             kwds = dict(shell=True, stdout=subprocess.PIPE))
    pool.close()
    print ("--- WAITING FOR THE JOBS TO FINISH                       ---")
    pool.join()
    print ("--- DONE                                                 ---")

#----------------------------------------------------------------------
# STEP 2: combine the runs
def combine_runs():
    print ("------------------------------------------------------------")
    print ("--- COMBINE THE RUNS FOR EACH VALUE OF ALPHAS            ---")
    print ("------------------------------------------------------------")
    for alphas in alphass:
        print (f"alphas = {alphas}")
        command = f"../submodules/AnalysisTools/scripts/combine-runs.pl {outdir}/runs/lambda{target_lambda}-alphas{alphas}-rseq*.res > {outdir}/lambda{target_lambda}-alphas{alphas}.res"
        stdouterr_from(command)
        print (f"Generated result file {outdir}/lambda{target_lambda}-alphas{alphas}.res")
    print ("--- DONE                                                 ---")
    
#----------------------------------------------------------------------
# STEP 3: do some plots
def do_plot():
    print ("------------------------------------------------------------")
    print ("--- PRODUCE THE PLOT                                     ---")
    print ("------------------------------------------------------------")  

    showertags={"panglobal-beta0.0" : r'PanGlobal($\beta_{\rm ps}=0$)'}

    fig,ax = plt.subplots()
    # Extract the shower values
    shower_res = get_array(f"{outdir}/lambda{target_lambda}-alphas{alphass[0]}.res", 'cumul_hist:slice_scalar_pt')

    # Extract the analytic results
    analytic_res = get_array(f'{outbase}/reference-results/nonglobal-ee-nll-analytics.dat', 'S_lambda')
#----------------------------------------------------------------
    ratio_err = shower_res[:,2]/analytic_res[10:,1]
    ratio = shower_res[:,1]/analytic_res[10:,1]
    ax.set_title(r"NLL accuracy test for $E_{{t,\rm max}}$ in $|\eta|<1$")
        
    ax.plot(shower_res[:,0], ratio,color='dodgerblue')
    ax.fill_between(shower_res[:,0], ratio+ratio_err, ratio-ratio_err, color='dodgerblue',alpha=0.2,edgecolor='none')
    ax.set_xlabel(r'$\lambda=\alpha_s\ln \frac{E_{t,\rm max}}{Q}$',fontsize=16)
    ax.set_ylabel(r'$\Sigma_{\rm PS}(\lambda)/\Sigma_{\rm NLL}(\lambda)$',fontsize=16)
    ax.set_ylim(0.95,1.05)
    ax.axhline(1,color='black',linestyle=':')
    shower_label = showertags[args.shower] if args.shower in showertags.keys() else args.shower
    ax.text(0.98, 0.035, f"{shower_label}", ha='right', va='baseline', transform=ax.transAxes, fontsize=9,color='black')
        
    ax.text(1.01,0.4, r'$e^+e^-\to q\bar{q}$, $\alpha_s=10^{-9}$, $\eta_c=11$', transform=ax.transAxes, ha='left', fontsize=9, rotation=270, color='grey')

    outfig = f'{outdir}/slice.png'
    plt.savefig(outfig,bbox_inches='tight',dpi=300)
    plt.close()
            
    print (f"Generated {outfig}")
    print ("--- DONE                                                 ---")



# build the code in doubleexp
def build_code():
    print(f"Building everything in {build_dir}")
    orig_dir=os.getcwd()
    if not os.path.isdir(build_dir):
        os.mkdir(build_dir)
    os.chdir(build_dir)
    # cmake will be needed if we make this part of a nightly check
    stdouterr_from(f"cmake -B . -S .. -DPSDOUBLEEXP=on")

    stdouterr_from("make -j")
    print("done\n")
    os.chdir(orig_dir)


# run a command and get the output
def stdouterr_from(arguments):
    result = subprocess.run(arguments, shell=True,
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if (result.returncode != 0):
        print(result)
        print("An error occurred")
        exit(-1)
    return result.stdout.decode('utf-8')



# matplotlib config
styles = [
    {'color':'#f06060'},
    {'color':'#4040e0'},
    {'color':'#40c040'},
    {'color':'#404040'},
    {'color':'#e0a040'},
    ]
colours = cycler('color', [style['color'] for style in styles])
# see options for things to set with 
#     python -c "import matplotlib as mpl; print(mpl.rcParams)"
plt.rc('axes',  grid=True, prop_cycle=colours)
plt.rc('figure', figsize=(5,3.8))
plt.tick_params(axis='both', which='both', direction='in', bottom=True, top=False, left=True, right=True )    

font = {'color':  'black',
        'weight': 'normal',
        'size': 18
        }

plt.rcParams['xtick.labelsize'] = 14
plt.rcParams['ytick.labelsize'] = 14
plt.rcParams['legend.fontsize'] = 14
plt.rcParams['legend.handlelength'] = 1

# Matplotlib helper to plot both a line and a band
def line_and_band(ax,x,val_and_err,**extra):
    extra_no_label = copy.copy(extra)
    if ('label' in extra_no_label): del extra_no_label['label']
    ax.fill_between(x,
                    val_and_err.value-val_and_err.error,
                    val_and_err.value+val_and_err.error,
                    alpha=0.2,
                    **extra_no_label
                    )
    ax.plot(x,val_and_err.value, **extra)


if __name__ == '__main__': main()
