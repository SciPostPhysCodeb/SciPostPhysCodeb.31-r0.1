//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __PANSCALES_INTERNALLIMITEDWARNING_HH__
#define __PANSCALES_INTERNALLIMITEDWARNING_HH__

#include <iostream>
#include <string>
#include <list>


namespace panscales{
//--------------------------------------------------------------------
/// @ingroup error_handling
/// \class LimitedWarning
/// class to provide facilities for giving warnings up to some maximum
/// number of times and to provide global summaries of warnings that have
/// been issued.
class LimitedWarning {
public:
  
  /// constructor that provides a default maximum number of warnings
  LimitedWarning() : _max_warn(_max_warn_default),_this_warning_summary(0) {}

  /// constructor that provides a user-set max number of warnings
  LimitedWarning(int max_warn_in) : _max_warn(max_warn_in), _this_warning_summary(0) {}  
  
  /// outputs a warning to standard error (or the user's default
  /// warning stream if set)
  void warn(const char * warning) {warn(warning, _default_ostr);}

  /// outputs a warning to standard error (or the user's default
  /// warning stream if set)
  void warn(const std::string & warning) {warn(warning.c_str(), _default_ostr);}

  /// outputs a warning to the specified stream
  void warn(const char * warning, std::ostream * ostr);

  /// outputs a warning to the specified stream
  void warn(const std::string & warning, std::ostream * ostr) {warn(warning.c_str(), ostr);}

  /// sets the default output stream for all warnings (by default
  /// cerr; passing a null pointer prevents warnings from being output)
  static void set_default_stream(std::ostream * ostr) {
    _default_ostr = ostr;
  }

  /// sets the default maximum number of warnings of a given kind
  /// before warning messages are silenced.
  static void set_default_max_warn(int max_warn) {
    _max_warn_default = max_warn;
  }

  /// the maximum number of warning messages that will be printed
  /// by this instance of the class
  int max_warn() const {return _max_warn;}

  /// the number of times so far that a warning has been registered
  /// with this instance of the class.
  int n_warn_so_far() const;

  /// returns a summary of all the warnings that came through the
  /// LimitedWarning class
  static std::string summary();

private:
  const int _max_warn;

  typedef std::pair<std::string, unsigned int> Summary;
  static int _max_warn_default;
  static std::ostream * _default_ostr;
  Summary* _this_warning_summary;

  // Note that this is updated internally and we use a mutex for the
  // thread-safe version. So no other specific treatment is needed at
  // this level.
  static std::list< Summary > _global_warnings_summary;
 
};
}

#endif // __FASTJET_LIMITEDWARNING_HH__
