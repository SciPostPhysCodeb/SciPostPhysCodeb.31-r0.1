//FJSTARTHEADER
//
// Copyright (c) 2007-2018, Matteo Cacciari, Gavin P. Salam and Gregory Soyez
//
//----------------------------------------------------------------------
//  This file is distributed as part of PanScales but is HEAVILY based 
//  on a part of FastJet.
//
//  FastJet is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  The algorithms that underlie FastJet have required considerable
//  development. They are described in the original FastJet paper,
//  hep-ph/0512210 and in the manual, arXiv:1111.6097. If you use
//  FastJet as part of work towards a scientific publication, please
//  quote the version you use and include a citation to the manual and
//  optionally also to hep-ph/0512210.
//
//  FastJet is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with FastJet. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------
//FJENDHEADER

#include "EECambridgeDirDiff.hh"

#include <sstream>
#include <limits>

using namespace std;
using namespace panscales;
using namespace fjcore;

string EECambridgeDirDiff::description () const {
  ostringstream desc;
  desc << "EECambridgeDirDiff plugin with ycut = " << ycut() ;
  return desc.str();
}

void EECambridgeDirDiff::run_clustering(fjcore::ClusterSequence & cs) const {
  if (cs.jet_def().recombination_scheme() != fjcore::WTA_modp_scheme) {
    throw runtime_error("EECambridgeDirDiff should only be run with the WTA_modp_scheme");
  }
  int njets = cs.jets().size();
  fjcore::NNH<EECamBriefJetDD> nnh(cs.jets());

  precision_type Q2 = cs.Q2(); 

  while (njets > 0) {
    int i, j, k;
    // here we get a minimum based on the purely angular variable from the NNH class
    // (called dij there, but vij in the Cambridge article (which uses dij for 
    // a kt distance...)
    precision_type vij = nnh.dij_min(i, j); // i,j are return values...

    // next we work out the dij (ee kt distance), and based on its
    // value decide whether we have soft-freezing (represented here by
    // a "Beam" clustering) or not
    precision_type dij;
    if (j >= 0) {
      precision_type scale = min(cs.jets()[i].E(), cs.jets()[j].E());
      dij = 2 * vij * scale * scale;
      if (dij > Q2 * ycut()) {
        // we'll call the softer partner a "beam" jet
        if (cs.jets()[i].E() > cs.jets()[j].E()) std::swap(i,j);
        j = -1;
      }
    } else {
      // for the last particle left, just use yij = 1
      dij = Q2;
    }
    
    if (j >= 0) {
      // Note that this bit of code is a bit fragile if ever (in the
      // future) we want to move to recombination schemes that do
      // more complex manipulations with UserInfo (e.g. for flavour).
      // -------------------------------------------
      // Get the newjet momentum from the recombiner (which is required
      // to be WTA, cf code at start)
      PseudoJet newjet_rec;
      cs.jet_def().recombiner()->recombine(cs.jets()[i], cs.jets()[j], newjet_rec);
      // decide on the source of the user info (the one with larger modp)
      int isource = cs.jets()[i].modp2() > cs.jets()[j].modp2() ? i : j;
      PseudoJet newjet_final(cs.jets()[isource]);
      // and then get the momentum from the recombiner (to avoid having
      // to do it ourselves), keeping in mind that reset_momentum leaves
      // user info unchanged
      newjet_final.reset_momentum(newjet_rec);
      // and finally record it
      cs.plugin_record_ij_recombination(i, j, dij, newjet_final, k);
      nnh.merge_jets(i, j, cs.jets()[k], k);
    } else {
      cs.plugin_record_iB_recombination(i, dij);
      nnh.remove_jet(i);
    }
    njets--;
  }
    
}
//
