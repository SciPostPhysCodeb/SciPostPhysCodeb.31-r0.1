//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __SHOWERBASE_HH__
#define __SHOWERBASE_HH__

#include <string>
#include <vector>
#include "Event.hh"
#include "QCD.hh"
#include "Range.hh"
#include "Matrix3.hh"
#include "fjcore_local.hh"
#include "LimitedWarning.hh"
#include "HoppetRunner.hh"

namespace panscales{

// simply insert a fwd declaration of ColourTransitionRunner (we only
// need the full definition in the source file)
class ColourTransitionRunnerBase;

/// f(eta) function used to partition dipoles for antenna showers
double f_fcn(double lnb);
void f_fcn(double lnb, double &f, double &omf);

/// g(eta) function used to partition dipoles for dipole showers
/// Eq. (69) of 2018-07-notes.pdf
double g_fcn(double lnb);

/// \class ShowerBase
/// Base class for showers.
///
/// Aspects showers have in common are the following
/// - an ordering variable v, we will typically use lnv = ln(v); v is
///   expected to be a decreasing variable as the evolution proceeds.
/// - a branching variable lnb (like lnz, \eta, etc.)
/// - an azimuthal variable \phi
/// - the idea of an element of the event, which could be a dipole or
///   an antenna, or a parton, or anything else
///
class ShowerBase {
public:
  /// ctor
  ///
  /// By default the maximal value of alphas is taken at the lnkt
  /// cutoff scale
  ShowerBase(const QCDinstance & qcd) : _qcd(qcd) {
    // make sure the max alphas has a reasonable default
    set_max_alphas(qcd.lnkt_cutoff());    
    // for now, do not include double-soft correctrions
    enable_double_soft(false);
    set_uncertainty_x_hard();   //< defaults to 1.0 (no variation)
    set_uncertainty_x_simkt();  //< defaults to 1.0 (no variation)
  }

  /// dummy virtual dtor
  virtual ~ShowerBase() {}

  /// each shower should have its own Element class derived from
  /// ShowerBase::Element
  class Element;

  /// each ShowerBase will be able to gather info about on-going
  /// emissions in order to cache the relevant bits and pieces in
  /// between different steps
  class EmissionInfo;
  virtual EmissionInfo* create_emission_info() const = 0;
  
  //--------------------------------------------------
  // basic properties
  //--------------------------------------------------
  /// returns a textual description of the shower, including its parameters
  virtual std::string description() const = 0;

  /// returns the name of the shower 
  virtual std::string name() const = 0;
  
  /// returns the physical name associated with the ordering variable
  virtual std::string name_of_lnv() const = 0;

  /// returns the physical name associated with the generic branching
  /// variable b. E.g. it might be eta, lnz, sij, etc. The branching
  /// variable should always be logarithmically distributed
  virtual std::string name_of_lnb() const = 0;

  /// returns true if the shower is global (defaults to false)
  virtual bool is_global() const { return false; }
  /// should return true for dipole-like showers, where only the emitter
  /// can split; antenna showers where both dipole members ("emitter and
  /// spectator") should return false here;
  virtual bool only_emitter_splits() const = 0;

  /// certain simplifications hold if one expects
  /// Element::do_kinematics() to always return true when
  /// accept_prob > 0
  virtual bool do_kinematics_always_true_if_accept() const {return true;}

  /// returns true if the coupling depends not just on kt, but also
  /// on z (including potential dependence of second order coefficient)
  virtual bool coupling_depends_on_z() const {return false;}

  /// returns true if the shower involves a second-order piece
  /// This means we add an O(alphas^2) contribution to the acceptance
  /// probability (as evaluated in Element::alphas2_coeff())
  virtual bool has_alphas2_coeff() const {return false;}

  /// returns the beta value for the shower
  virtual double beta() const = 0;

  /// set a dipole shower by default
  virtual unsigned int n_elements_per_dipole() const = 0;

  /// MvB 30-10-2023 Why is this a member of showerbase?
  /// ME for pp2Z
  virtual double pp2Z_one_emission_effective_ME(const Event event, HoppetRunner *hoppet_runner) const;
  /// ME for pp2H
  virtual double pp2H_one_emission_effective_ME(const Event event, HoppetRunner *hoppet_runner) const;

#ifndef SWIG
  /// allocates unique_ptrs for for the "elements" associated with dipole
  /// index i in the event and returns them as a vector of unique_ptrs
  virtual std::vector<std::unique_ptr<ShowerBase::Element> > elements(Event & event, int dipole_index) const = 0;
#endif
  
  //--------------------------------------------------
  // QCD properties
  //--------------------------------------------------

  /// QCD instance stored in the shower
  const QCDinstance & qcd() const {return _qcd;}

  /// sets internally the maximal value of alphas over the full
  /// kinematic range
  virtual void set_max_alphas(double lnvmin){
    _max_alphas = _qcd.max_alphas_above_lnkt(lnvmin);
    // build-in a sensible maximum directly here
    // note some care will be
    // needed when we have variable nf
    if (qcd().lnxmuR() > 0) {
      double max_alphasMSbar = qcd().alphasMSbar_no_cutoff(std::max(lnvmin, qcd().lnkt_cutoff()));
      _max_alphas *= (1 + 2*qcd().lnxmuR() * qcd().b0() * max_alphasMSbar); 
    }
  }

  /// retrieves the max allowed value of alphas
  const double max_alphas() const {return _max_alphas;}

  /// helper to fill the splitting weights associated with the DGLAP
  /// splitting functions
  ///
  /// One passes:
  ///  - p: a particle (that splits) 
  ///  - z: the momentum fraction of the emission
  /// and this fills weight_rad_g and weight_rad_q with the
  /// (normalised) splitting weight for radiating either a gluon or a
  /// quark
  ///
  /// Note: "normalised" DGLAP weight means we've factored out a
  /// factor alphas*CA/pi. For gluon emitters, this also includes a
  /// factor 1/2 associated with the fact that a gluon is contributing
  /// to 2 dipoles.
  void fill_dglap_splitting_weights(const Particle &p,
                                    const precision_type &z,
                                    double & weight_rad_g,
                                    double & weight_rad_q) const;
  
  //--------------------------------------------------
  // uncertainty estimates
  //
  // We introduce a set of coefficients meant to probe the theoretical
  // uncertainties. These coefficients are 1 by default (no
  // correction) and should typically be varied between 1/2 and 2 in
  // order to probe the sensitivity of a shower to effects beyond its
  // accuracy.
  //--------------------------------------------------
  /// coefficient associated with the hard matrix element corrections
  void set_uncertainty_x_hard(double coefficient=1.0){ _uncertainty_x_hard = coefficient; }
  double uncertainty_x_hard() const{ return _uncertainty_x_hard; }

  /// coefficient associated with emissions at commensurate kt
  /// Meant for "standard" dipole shower which have a spurious NLL
  /// recoil scheme
  void set_uncertainty_x_simkt(double coefficient=1.0){ _uncertainty_x_simkt = coefficient; }
  double uncertainty_x_simkt() const{ return _uncertainty_x_simkt; }

  /// returns a string with the uncertainty scale choices
  std::string scale_description() const{
    std::ostringstream ostr;
    ostr <<    "xhard = " << uncertainty_x_hard()
         << ", xsimkt = " << uncertainty_x_simkt();
    return ostr.str();
  }

  //--------------------------------------------------
  // other properties
  //--------------------------------------------------

  /// sets whether the shower uses direction differences internally
  void set_use_diffs(bool val) {_use_diffs = val;}
  bool use_diffs() const {return _use_diffs;}

  //--------------------------------------------------
  // material for matching (truncated showers)
  //--------------------------------------------------
  /// returns true if an emission happens at a phase-space point that has
  /// already been covered by the matching shower (used in "truncated" showers)
  ///
  ///   \param lnv_first               the lnv value of the first emission
  ///   \param lnkt                    the ln(kt) value of the emission to be vetoed
  ///                                  (with ln(kt) = ln(|p_k|*sin(theta_k)) )
  ///   \param eta                     the opening angle of the emission wrt the splitter
  ///                                  (with eta = -log(tan(theta_k/2)))
  virtual bool veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const{
    assert(false && "cannot use this shower for truncated matching.");
    return false;
  }

  /// sets whether the shower uses the boost at the end
  void set_boost_at_end(bool val) {_boost_at_end = val;}
  bool boost_at_end() const {return _boost_at_end;}

  /// sets whether the hard system is updated during the shower
  void set_hard_system_fixed(bool val) {_hard_system_fixed = val;}
  bool hard_system_fixed() const {return _hard_system_fixed;}

  /// returns the values of lnv and lnb for a given kinematic
  /// configuration (given through the emitter, spectator and
  /// radiation post-branching momenta)
  virtual bool find_lnv_lnb_from_kinematics(const Momentum &p_emit, 
                                            const Momentum &p_spec, 
                                            const Momentum &p_rad, 
                                            const precision_type &p_emit_dot_p_spec,
                                            const precision_type &p_emit_dot_p_rad,
                                            const precision_type &p_spec_dot_p_rad,
                                            const Event &event,
                                            double &lnv, double &lnb) const{
    std::cerr << "find_lnv_lnb_from_kinematics not implemented for this shower" << std::endl;
    return false;
  }

  //----------------------------------------------------------------------
  // double-soft material
  //----------------------------------------------------------------------

  /// enum to specify the available colour channel selections for the double-soft
  enum DoubleSoftColourChannel{
    DoubleSoftColourAny = 0, ///< any (i.e. all) colour channel
    DoubleSoftColourCF  = 1,
    DoubleSoftColourCA  = 2,
    DoubleSoftColourTR  = 3
  };

  void enable_double_soft(bool enable = true, bool enable_dKCMW = true, double dKCMW_error_offset_factor=0.0, bool dKCMW_subtract_fit=false) {
    _enable_double_soft = enable;
    _enable_double_soft_dKCMW = enable_dKCMW;
    _dKCMW_error_offset_factor = dKCMW_error_offset_factor;
    _dKCMW_subtract_fit = dKCMW_subtract_fit;
    //set_double_soft_any_colour();

    init_double_soft();
  }
  virtual void init_double_soft() {
    assert((!_enable_double_soft) || ("Double-soft corrections requested but this shower does not support double-soft corrections (yet?)"));
  }
  bool double_soft() const {return _enable_double_soft;} 
  bool double_soft_dKCMW() const {return _enable_double_soft_dKCMW;} 
  double double_soft_dKCMW_error_offset_factor() const {return _dKCMW_error_offset_factor;} 
  bool double_soft_dKCMW_subtract_fit() const { return _dKCMW_subtract_fit;}
  virtual void update_double_soft_overhead(double lnv_minus_lnvlast){}
  
  /// allows one to select a specific colour channel for the
  /// double-soft matrix element
  void set_double_colour(DoubleSoftColourChannel mode) { _double_soft_colour_channel = mode; }
  void set_double_soft_any_colour() { _double_soft_colour_channel = DoubleSoftColourAny;}
  void set_double_soft_CF_only()    { _double_soft_colour_channel = DoubleSoftColourCF; }
  void set_double_soft_CA_only()    { _double_soft_colour_channel = DoubleSoftColourCA; }
  void set_double_soft_TR_only()    { _double_soft_colour_channel = DoubleSoftColourTR; }

  
  /// double-soft-related KCMW correction (applied as an acceptance
  /// probability, potentially w an overhead factor to be handled by
  /// individual showers)
  virtual double delta_K_CMW_acceptance(typename ShowerBase::EmissionInfo * emission_info_base) const { return 1.0; }

protected:
  bool _use_diffs = false;
  bool _boost_at_end = false;
  bool _hard_system_fixed = true;

  bool _enable_double_soft = false; 
  bool _enable_double_soft_dKCMW = false;
  double _dKCMW_error_offset_factor = 0.0;
  bool _dKCMW_subtract_fit = false;
  DoubleSoftColourChannel _double_soft_colour_channel = DoubleSoftColourAny;

  QCDinstance _qcd;    ///< QCD informations
  double _max_alphas;  ///< maximal value of alphas (point below which we set it to 0)

  /// uncertainty coefficients
  double _uncertainty_x_hard;   //< hard matrix-element corrections
  double _uncertainty_x_simkt;  //< emissions at commensurate kt

};


//-----------------------------------------------------------------------
/// \class ShowerBase::Element
/// base class for elements of an event
///
/// This is meant to be used within the framework of a given shower
///
/// **************************************************************
///            Important notes for derived classes
/// **************************************************************
///
/// A derived ShowerXYZ class should specify a series of methods which
/// are declared as pure virtual in this base class:
///
///   const double alphas_lnkt(double lnv, double lnb) const;
///   double lnb_extent_const() const;
///   double lnb_extent_lnv_coeff() const;
///   Range lnb_generation_range(double lnv) const;
///   bool has_exact_lnb_range() const;
///   double lnv_lnb_max_density() const;
///   bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const;
///   bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info,
///                      const RotatedPieces & rp) const;
///   double lnb_for_min_abseta(double lnv) const;
///
class ShowerBase::Element {
public:
  /// dummy ctor
  Element() {}

  /// ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event)
    : _emitter_index(emitter_index), _spectator_index(spectator_index),
      _dipole_index(dipole_index), _event(event), _enhancement_factor(1.) {}

  /// given that we have virtual functions in a number of places,
  /// we need a virtual destructor to avoid compiler warnings
  virtual ~Element() {}

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------

  /// The functions below are added
  /// so that the EmissionVeto class, which takes ShowerBase::Element
  /// as input arguments, is using these to get info to cache (in the
  /// local recoil case), with a view to restoring the event after a
  /// do_kinematics call, should the EmissionVeto object decide that the
  /// the emission is to be discarded. Note that since we want to
  /// instantiate EmissionVeto objects up at the level of the analysis
  /// framework (to pass to ShowerRunner(Base) as a pointer) where we
  /// have only ShowerBase types to use as template arguments (instead
  /// of concrete showers), we need the following functions to appear
  /// here s.t. EmissionVeto compiles.
  const Particle & emitter()   const {return (*_event)[_emitter_index  ];}
  const Particle & spectator() const {return (*_event)[_spectator_index];}
  Particle & emitter()   {return (*_event)[_emitter_index  ];}
  Particle & spectator() {return (*_event)[_spectator_index];}
  int emitter_index()   const {return _emitter_index;}
  int spectator_index() const {return _spectator_index;}
  int dipole_index()    const {return _dipole_index;}
  int emitter_barcode()   const {return (*_event)[_emitter_index  ].barcode();}
  int spectator_barcode() const {return (*_event)[_spectator_index].barcode();}
  const Dipole & dipole() const {return _event->dipoles()[dipole_index()];}
  Dipole & dipole() {return _event->dipoles()[dipole_index()];}
  const Event & event() const {return *_event;}

  /// returns true if the "emitter" corresponds to the quark end of the dipole
  bool emitter_is_3bar_end_of_dipole() const{
    return _emitter_index == _event->dipoles()[_dipole_index].index_qbar;
  }
  /// returns true if the "emitter" corresponds to the qbar end of the dipole
  bool emitter_is_3_end_of_dipole() const{
    return _emitter_index == _event->dipoles()[_dipole_index].index_q;
  }
  bool emitter_is_quark_end_of_dipole() const{
    return emitter_is_3_end_of_dipole();
  }

  /// direct access to the 3bar/3 ends of the dipole
  const Particle & particle_3bar_end() const {
    return emitter_is_3bar_end_of_dipole() ? emitter() : spectator();
  }
  const Particle & particle_3_end() const {
    return emitter_is_3bar_end_of_dipole() ? spectator() : emitter();
  }

  /// stores information for the rotated pieces of the element
  struct RotatedPieces {
    RotatedPieces() {};

    /// a rotated version of the emitter
    Momentum emitter;
    /// a rotated version of the spectator
    Momentum spectator;
    /// the perpendicular vectors
    Momentum perp1, perp2;
    /// true if the emitter provides the reference direction, in which
    /// case the rotated emitter will be aligned along the z direction
    /// (otherwise it will be the rotated spectator that is along the z
    /// direction)
    bool emitter_is_ref;
    /// the rotation needed to return to the original frame
    /// from the one in which the reference particle is along the z
    /// axis
    Matrix3 rotn_from_z = Matrix3(1.0);

    /// assuming rotated pieces have already been set, with the emitter
    /// as reference, return a copy re-aligned with the spectator the
    /// reference. 
    RotatedPieces copy_aligned_to_spectator() const;

  };

  void set_rotated_pieces(const EmissionInfo * emission_info,
                          RotatedPieces & pieces,
                          bool emitter_is_always_ref = false) const;

  /// retuns the momentum scale to be used in the argument of the strong coupling
  virtual const double alphas_lnkt(double lnv, double lnb) const = 0;

  /// returns the coefficient of the term multiplying alphas_MSbar^2,
  /// which is to be added to alphas
  virtual double alphas2_coeff(const EmissionInfo *emission_info) const {return 0.0;}

  /// retuns the momentum scale to be used in the argument of the PDFs
  virtual const double pdf_lnkt(double lnv, double lnb) const{
    // this is sufficient at our current accuracy
    double lnv0 = 0.5*to_double(log_T(_event->Q2()));
    return lnv>lnv0 ? lnv0 : lnv0 + (lnv-lnv0)/(1+betaPS());
  }
  
  /// Allowing access to a parton shower's beta value. Motivation
  /// for this comes from a need inside the dynamic cut-off / emission
  /// veto apparatus. In particular, the desire to terminate the 
  /// whole shower evolution if it has proceeded sufficiently far
  /// below the observable-dominating emission in the Lund plane.
  virtual const double betaPS() const = 0;

  /// update the event record with the emission, as specified in
  /// emission_info, including changes to spectator/emitter, and any
  /// other potential changes (e.g. due to global recoil). 
  /// 
  /// The base implementation carries out everything that is needed
  /// for showers with dipole-local recoil. 
  /// 
  /// For showers with global recoil, the function should be overloaded
  /// and should start by calling ShowerBase::Element::update_event(...). 
  /// See e.g. the implementation in ShowerPanScaleGlobal.cc
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            ShowerBase::EmissionInfo * emission_info);

  virtual void dirdiffs_pre_update(ShowerBase::EmissionInfo * emission_info);
  virtual void dirdiffs_post_update(const ShowerBase::EmissionInfo * emission_info);
  virtual void dirdiffs_colour_flow_swap(const ShowerBase::EmissionInfo * emission_info);

  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  
  /// the lnb total extent, as a function of lnv, is parameterised
  /// as  const + lnv_deriv * lnv.
  virtual double lnb_extent_const() const = 0;
  virtual double lnb_extent_lnv_coeff() const = 0;

  /// returns the range over which the b variable should be generated
  /// for this given value of v. This may be an overestimate of the range.
  virtual Range lnb_generation_range(double lnv) const = 0;

  /// Returns true if we can supply the exact range for the lnb variable
  virtual bool has_exact_lnb_range() const {return false;}

  /// This supplies the exact range iff we know it
  virtual Range lnb_exact_range(double lnv) const {return lnb_generation_range(lnv);}

  /// returns the max density in lnv * lnb plane
  ///
  /// This is typically the soft-collinear large-Nc limit of the emission
  /// density. Note that it should use the maximum value of alphas (as
  /// running-coupling effects are taken into account elsewhere).
  ///
  /// For example, for a shower that uses kt as an evolution variable
  /// and rapidity as an auxiliary, the emission density in the
  /// soft-collinear & large-Nc limit is
  ///    alphas CA/pi
  /// so this would return
  ///    _shower->max_alphas() * _shower->qcd().CA() / M_PI;
  virtual double lnv_lnb_max_density() const = 0;

  //#TRK-ISSUE-888: maybe add a "header comment" saying that the
  //material below is related to matching. We could also have a
  //virtual double has_matching() {return false;} default. [I am not
  //fully sure but in this case, we may gain the opportunity to have
  //functions like get_emit_spec_rad_kinematics_for_matching(...),
  //etc...  automatically throw an error if matching is not avauilable
  //and do another default behaviour if it is.
  
  /// if known, this returns the exact jacobian for the first emission of the shower
  /// this is used for the matching implementations
  virtual precision_type dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const {
    throw std::runtime_error("Tried to access the exact jacobian an element of this shower, which is not implemented");
    return 0.;
  }

  /// Checks if the update event step of the shower impacts
  /// the kinematics of the dipole.
  ///
  /// This happens when the update_event does more than just a boost,
  /// for example when an additional rescaling is applied.
  virtual void get_emit_spec_rad_kinematics_for_matching(typename ShowerBase::EmissionInfo * emission_info, Momentum & emit, Momentum & spec, Momentum & rad, bool include_boost = false) const;
  
  /// get the dot products between rad.spec, rad.emit and emit.rad
  virtual void set_dot_products(typename ShowerBase::EmissionInfo * emission_info) const;

  /// get a multiplicative factor for the matching probability
  /// only needed for dipole showers, as a default it returns 1
  virtual precision_type get_partitioning_factor_matching_probability(typename ShowerBase::EmissionInfo * emission_info) const {return 1.;}

  /// Return and set constant factor which emission rate is to be enhanced by.
  /// This factor is 1. by default on construction of any and all elements. 
  /// The only way it can become != 1. is if the set function below is used.
  /// ___see_detailed_comments_below___ where _enhancement_factor is declared.
  double enhancement_factor() const { return _enhancement_factor; }
  double enhancement_factor(double enhancement_factor) {
    _enhancement_factor = enhancement_factor;
    return _enhancement_factor;
  }

  //----------------------------------------------------
  // caching info to facilitate the work of ShowerRunner
  //
  // we cache norm_a     = density * lnb_extent_const
  //          norm_b     = density * lnb_extent_lnv_coef
  //          norm_astar = max(norm_a, -norm_b lnv)
  //----------------------------------------------------

  /// get the cached values
  double norm_a()     const{ return _cached_norm_a;}
  double norm_b()     const{ return _cached_norm_b;}
  double norm_astar() const{ return _cached_norm_astar;}
  
  /// update the values (at a given lnv if needed)
  double update_norm_a(){
    return _cached_norm_a = enhancement_factor()*lnv_lnb_max_density()*lnb_extent_const();
  }
  double update_norm_b(){
    return _cached_norm_b = enhancement_factor()*lnv_lnb_max_density()*lnb_extent_lnv_coeff();
  }
  double update_norm_astar(double lnv){
    return _cached_norm_astar = enhancement_factor()*lnv_lnb_max_density()*std::max(lnb_extent_const(), -lnb_extent_lnv_coeff()*lnv);
  }

  /// update everything
  void update_cache(double lnv);
  
  /// update and retrieve everything
  void update_cache(double lnv, double &norm_a, double &norm_b, double &norm_astar);

  //---------------------------------------------------------------
  // important definitions to be implemented in all derived classes
  //---------------------------------------------------------------

  /// Determine the acceptance probability for a given kinematic 
  /// point as encoded by the lnv and lnb variables of emission_info.
  ///
  /// If the kinematic point is inaccessible, the function should return
  /// false.
  ///
  /// If it is accessible, the function should return true after filling
  /// in information in EmissionInfo as indicated below
  /// 
  /// From EmissionInfo it uses the value of lnv, lnb
  /// In EmissionInfo it should set the following: (defaults in square bracket)
  ///   emitter_weight_rad_gluon    [1.0]
  ///   emitter_weight_rad_quark    [0.0]
  ///   spectator_weight_rad_gluon  [0.0]
  ///   spectator_weight_rad_quark  [0.0]
  ///   z_radiation_wrt_emitter     [0.0] (for spin correlations)
  ///   z_radiation_wrt_spectator   [0.0] (for spin correlations)
  ///
  /// For final-state splittings, these weights are the splitting
  /// probabilities, with the assumption that alphas = alphas_max and
  /// (for gluon emission) leading colour. ShowerRunner is responsible
  /// for multiplying these weights by the appropriate alphas and
  /// colour-rescaling factors.
  ///
  /// For initial-state splittings ShowerRunner will also include the
  /// appropriate PDF ratio factors. E.g. for gluon emission from an up
  /// quark, the factor will be 
  ///
  ///   x_new*pdf_up(x_new, muF) / x_old * pdf_up(x_old,muF)
  /// 
  /// muF is determined from Element::pdf_lnkt(lnv,lnb).
  /// 
  /// Specific showers may also decide to cache additional information
  /// in EmissionInfo that can be helpful when computing the post-branching
  /// momenta in do_kinematics().
  ///
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const =0;

  /// once the acceptance probability test has passed, this function
  /// should be called to make sure that we can safely proceed to
  /// do_kinematics (at least for some values of phi).
  ///
  /// This allows one to (i) postpone some more expensive calculations
  /// instead of doing them in acceptance_probability, and (ii)
  /// maintain, as much as possible, do_kinematics returning true
  ///
  /// Note that this function may potentially cache extra things in
  /// emission_info
  ///
  /// EmissionInfo available: lnv, lnb
  /// EmissionInfo to fill  : None
  virtual bool check_after_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const{
    return true;
  }
  
  /// Once an emission has been accepted, carry out the splitting for
  /// this Element (and associated dipole), and set the emitter_out,
  /// radiation and spectator_out members
  ///
  /// The function returns true if the splitting is kinematically
  /// accessible, otherwise false. of the EmissionInfo object.
  ///
  /// In addition to the EmissionInfo class, the function also takes
  /// RotatedPieces. This contains the pre-branching emitter and
  /// spectator particles as well as two perpendicular vectors. Note
  /// that if using direction differences, these will be in a (rotated)
  /// frame, where either the emitter or spectator is aligned along the
  /// z axis. The output is to be stored in that same frame in the
  /// EmissionInfo class.
  ///
  /// EmissionInfo available: lnv, lnb, phi, do_split_emitter, radiation_pdgid and splitter_out_pdgid
  /// EmissionInfo to fill:
  ///    radiation     (post-branching radiation 4-momentum)
  ///    emitter_out   (post-branching emitter   4-momentum)
  ///    spectator_out (post-branching spectator 4-momentum)
  ///
  /// Note that in order to allow for the use of directional
  /// differences, the mapping implemented here should use the
  /// momenta provided via "RotatedPieces rp, including
  ///   rp.emitter, rp.spectator, rp.perp1, rp.perp2
  ///
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info,
                             const RotatedPieces & rp) const =0;

  /// update the internal kinematic variables (e.g. dipole mass), to be
  /// called, e.g., whenever one or other end of the dipole has been
  /// touched
  virtual void update_kinematics() {}
  virtual void print_characteristics() {}

  //--------------------------------------------------------
  // Things useful for observable-dependent dynamic cut-offs 
  //--------------------------------------------------------

  /// for a given lnv,lnb, returns approximate lnkt in the
  /// emitter-spectator dipole rest frame, which is equivalent to the
  /// frame-independent lnkt wrt to the emitter-spectator system.
  /// This lnkt will be correct in the soft+collinear limit but may
  /// differ from the true lnkt in the soft large-angle limit and the
  /// hard-collinear limit
  virtual double lnkt_approx(double lnv, double lnb) const {return std::numeric_limits<double>::max();}
  
  /// for a given lnv,lnb, returns the pseudorapidity with respect to
  /// the closer of the emitter/spectator, defined in the event
  /// centre-of-mass frame. This eta will be correct in the
  /// soft+collinear limit but may differ from the true eta in the soft
  /// large-angle limit and the hard-collinear limit.
  ///
  /// The result is signed, such that a positive eta_approx corresponds
  /// to being closer to the emitter direction.
  ///
  /// Note that eta_approx is typically discontinuous in the vicinity of
  /// eta values equal to the pseudorapidity of the emitter wrt the
  /// spectator. 
  //
  // #TRK_ISSUE-265  GPS-non-urgent: should we rename this, or at least add  
  // function such as eta_approx_wrt_3()  and eta_approx_wrt_emitter()
  // (also, now that all showers have this, we could make
  // this a pure virtual function, similarly for lnkt_approx)
  virtual double eta_approx(double lnv, double lnb) const {return std::numeric_limits<double>::max();}

  /// Quick helper to get a signed version of eta_approx which is +ve
  /// on the 3 side of the dipole
  double eta_approx_wrt_3(double lnv, double lnb) const {
    double eta = eta_approx(lnv, lnb);
    return emitter_is_quark_end_of_dipole() ? eta : -eta;
  }

  /// returns the lnb for which the eta_approx yields the smallest
  /// value of |eta| --- this corresponds roughly to emissions that
  /// bisect the emitter-spectator pair in the event centre-of-mass
  /// and is relevant for generation strategies that generate emissions
  /// only at large angles (e.g. CentralRap)
  virtual double lnb_for_min_abseta(double lnv) const = 0;
  
  /// return an estimate for the "observable"
  const double lnobs_approx(double lnv, double lnb, double beta) const;

  /// returns true if the underlying shower is using direction differences
  virtual bool use_diffs() const {return false;}

  /// returns true if the underlying shower is using double soft corrections
  virtual bool double_soft() const {return false;}

  /// Acceptance probability associated with correct double soft behaviour of the shower
  virtual double double_soft_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const;
  
  /// returns true if the underlying shower is boosting at the end (pp)
  virtual bool boost_at_end() const {return false;}

  /// returns true if the hard system is not updated during the shower (pp)
  virtual bool hard_system_fixed() const {return false;}

  /// check whether this element is the same as the one passed as
  /// argument. This is helper to debug potential issues.
  virtual bool check_equal_to(const ShowerBase::Element *other) const { return true; }


protected:
  int _emitter_index;          ///< emitter   index in the event's particle list
  int _spectator_index;        ///< spectator index in the event's particle list
  int _dipole_index;           ///< dipole index in the Event's dipole list

  Event * _event;              ///< a pointer to the full event

  double _cached_norm_a;       ///< cached value of a
  double _cached_norm_astar;   ///< cached value of a* (over-estimated a)
  double _cached_norm_b;       ///< cached value of b

  bool _requires_update_kinematics = false;

  /// The enhancement factor is 1 by default on construction of any and all elements. 
  /// The only way it can become != 1 is if the set function is used.
  ///
  /// The idea is that when this factor is on for a given element, the shower
  /// can produce fully constructed emissions from that element, with an emission
  /// rate which is higher by this factor. At time of writing, at the point where
  /// an emission is bound to be fully constructed (just before do_kinematics),
  /// we intervene in the shower with weights and/or vetoes to correct back down 
  /// to the true, physical, shower.
  /// 
  /// At time of writing, elsewhere in the code, the distribution of emissions
  /// up to the point of do_kinematics is the full true shower one modulo the
  /// fact that elements are boosted by their respective enhancement_factors.
  ///
  /// #TRK_ISSUE-266 MvB do we want to remove the following bits: seems outdated
  /// (You might well consider putting the weighting &/or rejection for the
  /// enhancement elsewhere, e.g. earlier on in the veto algorithm. It may be
  /// more efficient to do so, particularly if using very large enhancement
  /// factors, which will entail a heavy rejection rate to compensate the
  /// enhancement, perhaps heavier than the rejection rate of other veto alg
  /// steps, and if those preceding veto steps involve functions that are slow
  /// to evaluate. But since we have something working now we leave that as the
  /// next investigation after merging back to the master branch; in any
  /// case, to do it wouldn't change the edits to the code here in ShowerBase
  /// (or much else substantially either). The way/ordering of doing things
  /// re this enhancement/reweighting was arrived at naturally from extending
  /// out of the Jan 2020 developments, where whole events were vetoed only
  /// IF they made it to the point of do_kinematics. Note also that while one
  /// might find speed improvements moving the enhancement weight/rejection
  /// compensation higher up, one can expect a larger spread of weights, since
  /// with this step currently where it sits now, the event weight can only
  /// experience modification after all other veto steps are passed. Lastly,
  /// since the enhancement is _turned_off_ dynamically according to the 
  /// overlap of the shower phase space with that of the targetted observable
  /// it can be that there are no substantial performance gains to be had from
  /// the above considerations.)
  ///
  /// Hence, this factor appears everywhere that lnv_lnb_max_density() appears,
  /// i.e. all through the showering machinery and related caching, except:
  ///
  /// i)   Each shower's acceptance_probability(...) function: we intend the
  ///      acc/rej step based on this is from enhanced-crude-shower down to
  ///      enhanced-true-shower, with the `enhanced' part to be weighted/rejected
  ///      away further down the veto-alg. I.e. the enhancement factor should
  ///      not appear here as it is in both numerator and denominator of this
  ///      acceptance probability enhanced-true-shower/enhanced-crude-shower.
  ///      The same idea is in effect for earlier acc/rej steps in the veto 
  ///      alg, e.g. the αS acc/rej step (rejecting from enhanced-fixed-coupling
  ///      to enhanced-running-coupling, so the enhancement factor doesn't enter).
  ///
  /// ii)  ShowerRunner<Shower>::compute_first_sudakov(...) as this is really
  ///      an _exact_ calculation of the Sudakov associated to a given shower
  ///      for the purposes of starting the shower off at a scale lower than
  ///      the default max. We definitely want the true (unenhanced) shower
  ///      result for this.
  ///
  /// iii) ShowerRunner<Shower>::first_order(...) as this is used really just
  ///      to compute the shower's tree level leading order ME in diagnostics.
  ///      Again, we definitely want just the true (unenhanced) shower result.
  double _enhancement_factor;
};


//-----------------------------------------------------------------------
/// \class ShowerBase::EmissionInfo
/// a base class for caching information associated w each emission
///
/// #TRK_ISSUE-267  TODO: for each member, say where it is set (and potentially where it is used?)
class ShowerBase::EmissionInfo {

public:

  // Virtual destructor to make class polymorphic
  virtual ~EmissionInfo() = default;
  
  //------------------------------------------------------------
  /// basic information
  ///
  /// the generated coordinates of the emission
  /// They should be called by the runner as it becomes
  /// clear what they are
  double lnv, lnb, phi;

  /// this should be called by the runner once you know the element
  void set_element(ShowerBase::Element * element) {element_ = element;}

  /// returns a pointer to the element
  const ShowerBase::Element * element() const {return element_;}
  ShowerBase::Element * non_const_element() const {return element_;}

  /// return a pointer to a new copy of the EmissionInfo object; 
  /// Each shower should implement its own copy function so as
  /// to return a pointer to the derived EmissionInfo class
  /// (but a user who wishes to access that derived class will need to
  /// cast the pointer to the appropriate type)
  virtual EmissionInfo * clone() const {throw std::runtime_error("clone() not implemented for this shower");}

  /// determines whether the elements of the other dipoles
  /// should be updated after the emission
  /// should be set to false by default and in the case of
  /// global showers
  bool full_store_update_required = false;

  /// lnkt value used for alphas calculation, should be set in do_accept_alphas  
  double alphas_lnkt;

  //------------------------------------------------------------
  /// Collinear momentum fractions for the branching with respect to 
  /// the emitter or the spectator. Set by acceptance_probability(),
  /// used in spin correlations. In case of the PanScales showers,
  /// these correspond with ak and bk respectively.
  precision_type z_radiation_wrt_emitter, z_radiation_wrt_spectator;

  precision_type z_radiation_wrt_splitter() const {
    if (do_split_emitter) {
      return z_radiation_wrt_emitter;
    } else {
      return z_radiation_wrt_spectator;
    }
  }

  //------------------------------------------------------------
  /// information about splitting channel meant for the communication
  /// between Shower::Element::acceptance_probability, ShowerRunner
  /// and Shower::Element::do_kinematics
  ///
  /// record if the phase-point sampled in acceptance_probability was
  /// valid (acceptance_probability returned true) or not
  /// (acceptance_probability returned false).
  bool acceptance_probability_was_true;

  /// For antenna shower, there is a (relative) probability that the
  /// spectator splits. Based on
  ///   Emission_info::emitter_weight_rad_gluon
  ///   Emission_info::emitter_weight_rad_quark
  ///   Emission_info::spectator_weight_rad_gluon
  ///   Emission_info::spectator_weight_rad_quark
  /// ShowerRunner::_acceptance_probability fills
  ///   Emission_info::emitter_splitting_weight
  /// with the probability that the emisster is selected as the splitter.
  /// This is calculated using an additive expression:
  ///   emitter_splitting_weight = Pem/Ptot
  /// with
  ///   Pem  = emitter_weight_rad_gluon   + emitter_weight_rad_quark
  ///   Psp  = spectator_weight_rad_gluon + spectator_weight_rad_quark
  ///   Ptot = Pem + Psp
  double emitter_splitting_weight;

  /// true when the emitter does split (set by Runner for use in
  /// update_event)
  ///
  /// This is first set by ShowerRunner in select_channel_details.
  /// For showers w double-soft, it can later be modified by the shower.
  bool do_split_emitter;

  /// returns the index of the particle that is actually splitting
  /// (emitter or spectator, depending on do_split_emitter)
  int splitter_index() const {
    if (do_split_emitter) {
      return element()->emitter_index();
    } else {
      return element()->spectator_index();
    }
  }

  /// returns the particle that is actually splitting
  /// (emitter or spectator, depending on do_split_emitter)
  const Particle & splitter() const {
    if (do_split_emitter) {
      return element()->emitter();
    } else {
      return element()->spectator();
    }
  }

  /// returns the particle that is not splitting
  /// (emitter or spectator, depending on !do_split_emitter)
  const Particle & splitter_partner() const {
    if (do_split_emitter) {
      return element()->spectator();
    } else {
      return element()->emitter();
    }
  }

  /// returns true if the splitter is the 3 end of the dipole
  bool splitter_is_3_end() const {
    return splitter_index() == element()->dipole().index_3();
  }
  /// probabilities that the emitter/spectator splits and radiates
  /// either a gluon or a quark
  ///
  /// Note that the numbers below are stored as double because they
  /// are treated as an acceptance probability. Depending on future
  /// developments, we may have to switch them to precision_type.
  double emitter_weight_rad_gluon, emitter_weight_rad_quark;
  double spectator_weight_rad_gluon, spectator_weight_rad_quark;
  double splitter_weight_quark() const{
    if (do_split_emitter) { return emitter_weight_rad_quark; }
    else                  { return spectator_weight_rad_quark; }
  }
  double splitter_weight_gluon() const{
    if (do_split_emitter) { return emitter_weight_rad_gluon; }
    else                  { return spectator_weight_rad_gluon; }
  }
  double splitter_weight_fraction_quark() const{
    if (do_split_emitter) {
      return emitter_weight_rad_quark/(emitter_weight_rad_gluon+emitter_weight_rad_quark);
    } else {
      return spectator_weight_rad_quark/(spectator_weight_rad_gluon+spectator_weight_rad_quark);
    }
  }
  double splitter_weight_fraction_gluon() const {
    if (do_split_emitter) {
      return emitter_weight_rad_gluon/(emitter_weight_rad_gluon+emitter_weight_rad_quark);
    } else {
      return spectator_weight_rad_gluon/(spectator_weight_rad_gluon+spectator_weight_rad_quark);
    }
  }
  
  /// a structure to cache PDF info and facilitate setting/reading for 
  /// a specific beam
  struct PDFInfo {
    void reset(double x_old_in) {set = false; x_old = x_old_in;}
    double x_old, x_new;
    precision_type new_x_over_old;
    double pdf_old;
    PDF    pdf_new;
    double lnmuF;
    int    flav_old;
    bool set = false;
    double damping_factor; //< for massive quarks
    int ibeam = 0;
  };

  /// cached PDF info for beams 1 and 2
  PDFInfo pdf_info_beam1, pdf_info_beam2;
  PDFInfo * pdf_info(const Particle & particle) {
    if (particle.initial_state() == 1) {
      return &pdf_info_beam1;
    } else if (particle.initial_state() == 2){
      return &pdf_info_beam2;
    } else {
      return nullptr;
    }
  }

  /// PDF x ratio: x/xtilde (new/old) for both beams + post-splitting PDFs.
  /// A reason for keeping this outside the PDFInfo structure is that
  /// it can be used even when the full PDF info is not set.
  precision_type beam1_pdf_new_x_over_old, beam2_pdf_new_x_over_old;

  /// actual pdg id's (set by select_channel_details)
  int radiation_pdgid, splitter_out_pdgid;
  /// true if the emitter (in the sense of the emitter/spectator ends
  /// of the element) was at the 3 (quark) end of the dipole
  /// Npte that this is equivalent to element->emitter_is_quark_end_of_dipole()
  bool emitter_was_3;

  /// information about which end of the dipole is used to estimate
  /// the double-soft correction
  bool double_soft_partner_at_3_end;
  
  //------------------------------------------------------------
  /// information about where we stand in the colour transition vector
  /// (used to figure out how to built the transition vectors of the
  /// daughter dipoles)
  ///
  /// this will only get set for showers handling the CA-2CF
  /// difference (by ColourTransitionRunner::colour_factor_acceptance)
  /// and it holds an approximate eta_approx as well as the "exact"
  /// eta, signed such that positive eta is closer to the quark end of
  /// a dipole
  ///
  /// For eta, we also cache the position (index) in the list of
  /// transitions so we do not need to recompute it when we update
  /// the event
  ///
  /// eta_approx is always computed in the "colour-transition" sense
  /// where -infinity is at the 3bar end and +infinity at the 3 end
  ///
  /// #TRK_ISSUE-271  TODO: rename eta_approx -> eta_approx_wrt_3
  /// + can we get rid of emitter_was_3
  /// + get the dirdiffs cache computed in do_kinematics and not update_event
  /// + use the cached dirdiff to calculate exact eta
  /// + eta_exact should stay within the same segment (+decide what to do at the edge)
  /// + can we also get rid of eta_approx which does not seem to be needed
  double eta_approx, eta_insertion;  //< Note: could be the same
  unsigned int colour_segment_index;
  
  double eta_approx_wrt_3() const{ return eta_approx; }
    
  double eta_approx_wrt_emitter() const{
    if (emitter_was_3){ return  eta_approx; }
    else              { return -eta_approx; }
  }

  
  //------------------------------------------------------------
  /// kinematic information. Carry over the post-branching momenta
  Momentum emitter_out;
  Momentum radiation;
  Momentum spectator_out;
  Momentum k_perp;
  precision_type emit_dot_spec;
  precision_type emit_dot_rad;
  precision_type spec_dot_rad;

  /// returns the post-branching momentum of the splitter
  /// (emitter or spectator, depending on do_split_emitter)
  const Momentum & splitter_post_branching() const {
    if (do_split_emitter) {
      return emitter_out;
    } else {
      return spectator_out;
    }
  }

  /// returns the post-branching momentum of the parton that is not splitting
  /// (emitter or spectator, depending on do_split_emitter)
  const Momentum & splitter_partner_post_branching() const {
    if (do_split_emitter) {
      return spectator_out;
    } else {
      return emitter_out;
    }
  }

  const Momentum & momentum_3bar_out() const{
    return emitter_was_3 ? spectator_out : emitter_out;
  }
  const Momentum & momentum_3_out() const{
    return emitter_was_3 ? emitter_out : spectator_out;
  }

  bool use_diffs;
  bool boost_at_end;
  bool hard_system_fixed;
  
  /// difference in direction of the emitter with respect to its original direction
  Momentum3<precision_type> d_emitter_out;
  /// difference in direction of the radiation with respect to original
  /// emitter or spectator (as indicated d_radiation_is_wrt_emitter)
  Momentum3<precision_type> d_radiation;
  /// if true then d_radiation is wrt original emitter, otherwise wrt
  /// original spectator
  bool                      d_radiation_is_wrt_emitter;
  /// difference in direction of the spectator with respect to its original direction
  Momentum3<precision_type> d_spectator_out;

  /// gets set only if using differences
  Momentum3<precision_type> cached_dirdiff_3_minus_3bar;

  // direction difference (emitter - spectator) [pre-branching]
  Momentum3<precision_type> cached_dirdiff_em_minus_sp() const {
    if (emitter_was_3) {
      return  cached_dirdiff_3_minus_3bar;
    } else {
      return -cached_dirdiff_3_minus_3bar;
    }
  }
  // direction difference (radiation - emitter) [pre-branching emitter]
  Momentum3<precision_type> d_radiation_wrt_emitter() const {
    if (d_radiation_is_wrt_emitter) {
      return d_radiation;
    } else {
      return d_radiation - cached_dirdiff_em_minus_sp();
    }
  }
  // direction difference (radiation - spectator) [pre-branchinf spectator]
  Momentum3<precision_type> d_radiation_wrt_spectator() const {
    if (d_radiation_is_wrt_emitter) {
      return d_radiation + cached_dirdiff_em_minus_sp();
    } else {
      return d_radiation;
    }
  }
  // direction difference (radiation - original_dipole_3_end)
  Momentum3<precision_type> d_radiation_wrt_3() const {
    if ( emitter_was_3) return d_radiation_wrt_emitter();
    else                return d_radiation_wrt_spectator();
  }
  // direction difference (radiation - original_dipole_3bar_end)
  Momentum3<precision_type> d_radiation_wrt_3bar() const {
    if (!emitter_was_3) return d_radiation_wrt_emitter();
    else                return d_radiation_wrt_spectator();
  }
  // direction difference (new_3_end - original_3_end)
  Momentum3<precision_type> d_3_end() const {
    if ( emitter_was_3) return d_emitter_out;
    else                return d_spectator_out;
  }
  // direction difference (new_3bar_end - original_3bar_end)
  Momentum3<precision_type> d_3bar_end() const {
    if (!emitter_was_3) return d_emitter_out;
    else                return d_spectator_out;
  }

  // this function swaps emitter and spectator
  void swap_emitter_and_spectator() {

    std::swap(emitter_out, spectator_out);
    // and direction differences
    std::swap(d_emitter_out, d_spectator_out);
    // s - stilde -> e - stilde = e - etilde + (etilde - stilde)
    d_spectator_out = d_spectator_out + cached_dirdiff_em_minus_sp();
    // e - etilde -> s - etilde = s - stilde - (etilde - stilde)
    d_emitter_out = d_emitter_out - cached_dirdiff_em_minus_sp();
  }

  // this function swaps spectator and radiation
  void swap_spectator_and_radiation() {

    std::swap(spectator_out, radiation);
    // and direction differences
    if (d_radiation_is_wrt_emitter) {
      std::swap(d_radiation, d_spectator_out);
      // d_rad = rad - etilde -> s - etilde = (s-stilde) - (etilde - stilde)
      d_radiation = d_radiation - cached_dirdiff_em_minus_sp();
      // s - stilde -> rad - stilde = rad - etilde + (etilde - stilde)
      d_spectator_out = d_spectator_out + cached_dirdiff_em_minus_sp();
    }
    else {
      // d_rad = rad - stilde -> s - stilde
      std::swap(d_radiation, d_spectator_out);
    }
    // etilde - stilde -> same

  }

  // this function swaps emitter and radiation
  void swap_emitter_and_radiation() {

    std::swap(emitter_out, radiation);
    // and direction differences
    if (d_radiation_is_wrt_emitter) {
      // d_rad = rad - etilde -> e - etilde
      std::swap(d_radiation, d_emitter_out);
      // s - stilde -> same
    }
    else {
      std::swap(d_radiation, d_emitter_out);
      // d_rad = rad - stilde -> e - stilde = (e-etilde) + (etilde - stilde)
      d_radiation = d_radiation + cached_dirdiff_em_minus_sp();
      // e - etilde -> rad - etilde = rad - etilde - (stilde - etilde)
      // not sure why it works, but it does -> symmetry?
      d_emitter_out = d_emitter_out - cached_dirdiff_em_minus_sp();
      // s - stilde -> same
    }
    // etilde - stilde -> same

  }

  // compute lnkt, eta wrt the splitter (used for vetoing in matching)
  void lnkt_eta_wrt_splitter(double & lnkt, double & eta) const {
    precision_type sin_theta;

    // Version that uses direction differences. We make use of
    // delta = |d_radiation_wrt_splitter - d_splitter| with
    // delta = 2*sin(theta/2)
    if (use_diffs) {
      precision_type delta = do_split_emitter
        ? (d_radiation_wrt_emitter()   - d_emitter_out)  .modp()
        : (d_radiation_wrt_spectator() - d_spectator_out).modp();

      if (delta > numeric_limit_extras<precision_type>::sqrt_epsilon()) {
        assert(4-delta*delta >= 0);
        sin_theta = 0.5*delta*sqrt(4-delta*delta);
      } else {
        sin_theta = delta;
      }

      // Compute eta
      if (4.0-delta*delta <= 0.0)
        eta = 0.5 * numeric_limit_extras<precision_type>::log_max();
      else
        eta = -0.5 * to_double(log(delta*delta/(4.0-delta*delta)));
    } else {
      // Version without direction differences
      precision_type omc_wrt_splitter = do_split_emitter
        ? one_minus_costheta(radiation, emitter_out)
        : one_minus_costheta(radiation, spectator_out);

      // Compute eta
      if (omc_wrt_splitter > numeric_limit_extras<precision_type>::sqrt_epsilon()) {
        precision_type cos_theta = 1.0 - omc_wrt_splitter;
        precision_type theta = acos(cos_theta);
        sin_theta = sin(theta);
        eta = to_double(-log(tan(theta/2.0)));
      } else {
        // we are at small angles, so use small-angle formulas
        precision_type theta = sqrt(2. * omc_wrt_splitter);
        sin_theta = theta;
        eta = to_double(-log(theta/2));
      }
    }
    // Compute lnkt
    lnkt = log(to_double(radiation.modp()*sin_theta));
  }

  //--------------------------------------------------
  /// computation of the rapidity wrt to the emitter (or spectator)
  ///
  /// the argument tells whether to use dirdiffs info or not
  double eta_wrt_emitter() const{
    if (use_diffs){
      // Now we want to rewrite this in the restframe of the event's Q.
      // where we have (see the no-diffs case below):
      //   1-cos(theta_ij) = pi.pj Q^2/(pi.Q pj.Q)
      // and use dirdiffs to compute pi.pj precisely:
      //   pi.pj = Ei Ej (1-cos(theta_ij))
      //         = 2 Ei Ej sin^2(theta_ij/2)
      //         = 1/2 Ei Ej delta^2
      // with
      //   delta = d_radiation - d_emitter_out = 2 sin(theta_event/2)
      // i.e.
      //   1-cos(theta_ij) = delta^2  [Ei Ej  Q^2/(2 pi.Q pj.Q)]
      // That is equivalent to deining a deltaQ in the event Q's rest frame
      //   deltaQ^2 = 4 sin^2(theta/2)
      //            = 2 (1-cos(theta))
      //            = delta^2 [Ei Ej  Q^2/(pi.Q pj.Q)]
      //
      // We finally write the rapidity as
      //   eta = -1/2 log(tg^2(theta/2))
      //       = -1/2 log(sin^2(theta/2)/(1-sin^2(theta/2)))
      //       = -1/2 log(deltaQ^2/(4-deltaQ^2))
      //
      // To avoid a potential divergence at pi, in computing the
      // initiam guess for the insertion eta, we apply a "safety"
      // margin to avoid hitting -infinity (see below)
      precision_type delta2 = (d_radiation_wrt_emitter() - d_emitter_out).modpsq();

      const auto & Q = element_->event().Q();
      precision_type deltaQ2 = delta2 * ((radiation.E()*emitter_out.E()*Q.m2())
                                         /(dot_product(radiation,Q)*dot_product(emitter_out,Q)));

      if (4.0-deltaQ2 <= 0.0){
        return -0.5 * numeric_limit_extras<precision_type>::log_max();
      }
      return  -0.5 * to_double(log(deltaQ2/(4.0-deltaQ2)));
    }

    // no diffs
    //
    // We rewrite the rapidity in terms of 1-cos(theta) using
    //   cos(theta) = (1-t^2)/(1+t^2)   w t=tan(theta/2)
    //   => t^2 = (1-c)/(1+c) = (1-c)/(2-(1-c))
    //   => eta = -1/2 log((1-c)/(2-(1-c)))
    //
    // The theta angle needs to be in the restframe of the event's Q
    // For this, we write 
    //   1-cos(theta_ij) = Ei Ej [1-cos(theta_ij)]/(Ei Ej)
    // and use Ei = pi.Q/Q, (and similar for Ej), so
    //   1-cos(theta_ij) = pi.pj Q^2/(pi.Q pj.Q)
    //
    // NOTE: this assumes that all the particles are massless
    
    const auto & Q = element_->event().Q();
    precision_type omc_rad_wrt_emitter = dot_product(radiation, emitter_out)*Q.m2()
      /(dot_product(radiation, Q) * dot_product(emitter_out, Q));

    // #TRK_ISSUE-281 GS-TODO: check the test below
    if (2.0-omc_rad_wrt_emitter <= 0.0){
      return -0.5 * numeric_limit_extras<precision_type>::log_max();
    }
    return   -0.5 * to_double(log(omc_rad_wrt_emitter  /(2.0-omc_rad_wrt_emitter  )));
  }

  double eta_wrt_spectator() const{
    if (use_diffs){
      precision_type delta2 = (d_radiation_wrt_spectator() - d_spectator_out).modpsq();
      const auto & Q = element_->event().Q();
      precision_type deltaQ2 = delta2 * ((radiation.E()*spectator_out.E()*Q.m2())
                                         /(dot_product(radiation,Q)*dot_product(spectator_out,Q)));
      
      if (4.0-deltaQ2 <= 0.0){
        return 0.5 * numeric_limit_extras<precision_type>::log_max();
      }
      return   0.5 * to_double(log(deltaQ2/(4.0-deltaQ2)));
    }

    // no diffs: see eta_wrt_emitter for details
    const auto & Q = element_->event().Q();
    precision_type omc_rad_wrt_spectator = dot_product(radiation, spectator_out)*Q.m2()
      /(dot_product(radiation, Q) * dot_product(spectator_out, Q));
    
    // #TRK_ISSUE-281 GS-TODO: check the test below
    if (2.0-omc_rad_wrt_spectator <= 0.0){
      return 0.5 * numeric_limit_extras<precision_type>::log_max();
    } 
    return   0.5 * to_double(log(omc_rad_wrt_spectator/(2.0-omc_rad_wrt_spectator)));
  }

  //------------------------------------------------------------------------
  // information relevant for the handling of double-soft corrections
  //
  /// probability to swap between gg <-> qq channels
  double flavour_swap_probability; 
  /// for the probability to have a colour-flow swap, we need to consider both
  /// cases where the "new" flavour is gg or qq (dependng on a potential flavour swap) 
  double colour_flow_swap_probability_qq, colour_flow_swap_probability_gg;
  /// true if a colour swap actually happend (set towards the end of "ShowerRunner::run")
  bool colour_flow_swap;

private:
  ShowerBase::Element * element_;
 
};  

//------------------------------------------------------------------------  
/// \class ErrorZeroMassElement
/// class for throwing errors when a zero-mass element is encountered
class ErrorZeroMassElement {
public:
  ErrorZeroMassElement() {
    warn_.warn("Encountered zero-mass element");
  }
protected:
  static LimitedWarning warn_;
};


} // namespace panscales

#endif // __SHOWERBASE_HH__
