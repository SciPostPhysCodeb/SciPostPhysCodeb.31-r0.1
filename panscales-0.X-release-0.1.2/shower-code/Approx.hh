//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __APPROX_HH__
#define __APPROX_HH__
#include <iostream>
#include <cmath>

//--------------------------------------------------------------------
/// @ingroup approx_classes
/// \class ApproxBase
/// Base class for approximate comparisons, which mainly serves
/// to set the static tolerance.
/// A set of derived classes help with approximate equality tests,
/// loosely inspired by catch.hpp unit-testing framework.
/// The naming is chosen so as to avoid conflicts with catch.hpp
/// 
class ApproxBase {
public:
  ApproxBase(double value_in) : value_(value_in) {}

  inline double value() const {return value_;}

  /// returns true if other is equal to this within whatever
  /// dynamic_tolerance is being supplied;
  ///
  /// If the result is false, write out an error message.
  inline bool is_equal(double other, bool swap_output_order) const {
    double diff = value() - other;
    bool equal = (std::abs(diff) <= dynamic_tolerance(other));
    if (!equal && verbose_) write_diff_message(other, diff, swap_output_order);
    return equal;
  }
  
  /// returns true if value <= other, to  within whatever
  /// dynamic_tolerance is being used; i.e. value < other + tolerance
  ///
  /// If the result is false, write out an error message.
  inline bool is_lt_equal(double other, bool swap_output_order) const {
    double diff = value() - other;
    bool lt_equal = (diff <= dynamic_tolerance(other));
    if (!lt_equal && verbose_) write_ltgt_message(other, diff, swap_output_order, !swap_output_order);
    return lt_equal;
  }

  /// returns true if value >= other, to  within whatever
  /// dynamic_tolerance is being used; i.e. value > other - tolerance
  ///
  /// If the result is false, write out an error message.
  inline bool is_gt_equal(double other, bool swap_output_order) const {
    double diff = value() - other;
    bool gt_equal = (diff >= -dynamic_tolerance(other));
    if (!gt_equal && verbose_) write_ltgt_message(other, diff, swap_output_order, swap_output_order);
    return gt_equal;
  }
  
  /// returns the tolerance being used
  static inline double tolerance() {return tolerance_;}

  /// sets the global tolerance across this class and all derived ones
  static void set_tolerance(double tolerance_in) {
    tolerance_ = tolerance_in;
    half_tolerance_ = 0.5 * tolerance_;
  }

  /// @brief sets the global verbosity across this class and all derived ones
  /// @param verbose_in if true then failures in comparisons will be printed to cerr
  static void set_verbose(bool verbose_in) {verbose_ = verbose_in;}

  virtual double dynamic_tolerance(double other) const = 0;
  inline bool operator==(double other) const {
    bool swap_output_order = false;
    return is_equal(other,  swap_output_order);
  }
  inline bool operator<=(double other) const {
    bool swap_output_order = false;
    return is_lt_equal(other,  swap_output_order);
  }
  inline bool operator>=(double other) const {
    bool swap_output_order = false;
    return is_gt_equal(other,  swap_output_order);
  }
protected:
  double value_;
  static double tolerance_, half_tolerance_;
  static bool verbose_;
  void write_diff_message(double other, double diff, bool swap = false) const;
  void write_ltgt_message(double other, double diff, bool swap = false, bool lt = true) const;
  static std::ostream * ostr_; 
};
inline bool operator==(const double a, const ApproxBase & b) {
  bool swap_output_order = true;
  return b.is_equal(a, swap_output_order);
}
inline bool operator<=(const double a, const ApproxBase & b) {
  bool swap_output_order = true;
  return b.is_gt_equal(a, swap_output_order);
}
inline bool operator>=(const double a, const ApproxBase & b) {
  bool swap_output_order = true;
  return b.is_lt_equal(a, swap_output_order);
}

//--------------------------------------------------------------------
/// @ingroup approx_classes
/// \class ApproxAbs
/// Class for creating an approximate value for use in comparisons
/// where the tolerance is absolute
class ApproxAbs : public ApproxBase {
public:
  ApproxAbs(double value) : ApproxBase(value) {}
  double dynamic_tolerance(double other) const override {return tolerance();}
};

//--------------------------------------------------------------------
/// @ingroup approx_classes
/// \class ApproxAbs
/// Class for creating an approximate value for use in comparisons
/// where the larger of the absolute and relative tolerances is taken
class ApproxAbsRel : public ApproxBase {
public:
  ApproxAbsRel(double value) : ApproxBase(value) {}
  double dynamic_tolerance(double other) const override {
    return std::max(tolerance_, half_tolerance_ * (std::abs(value()) + std::abs(other)));
  }
};

//--------------------------------------------------------------------
/// @ingroup approx_classes
/// \class ApproxRel
/// Class for creating an approximate value for use in comparisons
/// where a relative tolerances is taken
class ApproxRel : public ApproxBase {
public:
  ApproxRel(double value) : ApproxBase(value) {}
  double dynamic_tolerance(double other) const override {
    return half_tolerance_ * (std::abs(value()) +std::abs(other));
  }
};

#endif //__APPROX_HH__
