//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "MatchedProcess.hh"
#include "MatchingHardMatrixElements.hh"
#include "ShowerPanScaleGlobal.hh"
#include <numeric>


namespace panscales {
  
// Returns true if the given combination of showers needs vetoing
// NOTE: only works (and tested!) for
//
//       Matching shower        Main shower
// -------------------------------------------
//       PanGlobal              PanLocal-Vincia
//       Powheg                 PanGlobal
//       Powheg                 PanLocal
void MatchedProcess::check_matching_needs_vetoing(ShowerBase * main_shower){
  // if using a power shower, we always need to veto
  if (_power_shower) {
    _matching_needs_vetoing = true;
    return;
  }

  // If MC@NLO, no vetoing
  if (_matching_scheme==MCAtNLOStyle) {
    _matching_needs_vetoing = false;
    return;
  }

  // Get the main shower's name
  std::string main_shower_name = main_shower->name();

  // Matching can only handle same-β showers
  assert(_shower->beta() == main_shower->beta());

  // If the matching shower is identical to the main shower
  if (main_shower_name == _shower_name) {
    // For PanLocal-Vincia standalone, we can do only MC@NLO
    if (main_shower_name == "PanScaleLocalVincia-ee" && _shower_name == "PanScaleLocalVincia-ee" &&
       _matching_scheme == PowhegStyle) {
      throw std::runtime_error("PanLocal-Vincia cannot be used as a matching shower in Powheg-style matching");
    }
    // else we don't need to veto
    _matching_needs_vetoing = false;
    return;
  } else if (_shower_name == "Powheg-ee") { // Powheg matching
    // For Powheg + PanGlobal, the lnv contours agree and it suffices to
    // continue from the same lnv value after the matched emission
    if (main_shower_name == "PanScaleGlobal-ee") {
      _matching_needs_vetoing = false;
      return;
    }
    // For Powheg + PanLocal, the lnv contours do not agree and thus
    // later emissions need to be vetoed
    else if (main_shower_name == "PanScaleLocal-ee") {
      _matching_needs_vetoing = true;
      return;
    }
    // For Powheg + Pythia, the lnv contours do not agree and thus
    // later emissions need to be vetoed
    else if (main_shower_name == "Pythia8-ee") {
      _matching_needs_vetoing = true;
      return;
    } else {
      throw std::runtime_error("The given combination of showers has not been tested");
    }
  } else if (main_shower_name == "PanScaleLocalVincia-ee" && _shower_name == "PanScaleGlobal-ee") {
    // For PanGlobal + PanLocal-Vincia, the lnv contours do not agree,
    // and later emissions need to be vetoed
    _matching_needs_vetoing = true;
    return;
  } else {
    // Any other combination has not been tested
    throw std::runtime_error("The given combination of showers has not been tested");
  }
}

// verifies that the strategy is compatible with Matching
void MatchedProcess::check_strategy_is_compatible(Strategy strategy) const {
  // Determine lnv+element generator is supported by qqgMatching.
  if(  _shower_name == "PanScaleGlobal-ee" &&
       !(strategy == 22 || strategy == 24) ) {
    // !(strategy == RoulettePeriodic || strategy == RouletteEnhanced) ) {
    assert("MatchedProcess : PanGlobal is only supported with RoulettePeriodic." 
           && false);
  }
  
  if(_power_shower && strategy == 22) {
    assert("MatchedProcess: for now, power showers should only be used with RoulettePeriodic."
           && false);
  }
}

// If the point in (lnkt,eta) was already covered by the _shower_for_matching
// contour at _lnvfirst, veto.
bool MatchedProcess::veto_emission(double & lnv_first, ShowerBase::EmissionInfo & emission_info) const {

  // Only veto if the emission is performed from the original ends,
  // which are tracked by veto_splitter_index
  if (emission_info.splitter_index() == _veto_splitter_index_1 ||
      emission_info.splitter_index() == _veto_splitter_index_2) {

    // --------- veto on lnkt
    double lnkt_k, eta_k;
    emission_info.lnkt_eta_wrt_splitter(lnkt_k, eta_k);

    // If the point in (lnkt,eta) was already covered by the _shower_for_matching
    // contour at _lnvfirst, veto.
    if (_shower->veto_emission_after_matching(lnv_first, lnkt_k, eta_k)) return true;
  }

  return false;
}

// Updates the splitter index to follow the hard branch if a splitting
// has occurred
void MatchedProcess::update_splitter_index(ShowerBase::EmissionInfo & emission_info, Event & event) {
  assert(unsigned(emission_info.splitter_index()) < event.size());

  if (emission_info.splitter_index() == _veto_splitter_index_1) {
    // get the radiation and check if its energy is larger than the splitter's
    if (event[event.size()-1].E() > event[emission_info.splitter_index()].E()) {
      // then update the index
      // the new particle index corresponds to the last particle in the event
      _veto_splitter_index_1 = event.size()-1;
    }
  }
  else if (emission_info.splitter_index() == _veto_splitter_index_2) {
    // get the radiation and check if its energy is larger than the splitter's
    if (event[event.size()-1].E() > event[emission_info.splitter_index()].E()) {
      // then update the index
      // the new particle index corresponds to the last particle in the event
      _veto_splitter_index_2 = event.size()-1;
    }
  }
}

// Acceptance probability for matching
void MatchedProcess::acceptance_probability(Event & event, ShowerBase::Element & element,
                                    ShowerBase::EmissionInfo & emission_info, double & prob){
  // Note: the phase-space boundaries are checked in the main
  // code (via emission_info.acceptance_probability_was_true).
  // The shower maps need to cover phase space (which is true
  // for PanLocal, as well as for PanGlobal with the C=1 bias).

  // The PS initial lnv, lnb values for each element according to
  // dP_new = dlnv dlnb lnv_lnb_max_density()
  //        * ( b ln(v) + a - b C ) / ( b ln(v) + a ).
  // C = 0 for PanLocal. For PanGlobal, to get an acceptance that is
  // bounded by 1, we need C > (1+β_ps)/4. We choose C = 0.5 always,
  // assuming βps < 1.
  double C = this->_find_element_alg_bias;
  double b = element.norm_b();
  double a = element.norm_a();
  precision_type dP_new = element.lnv_lnb_max_density();
  double lnv = emission_info.lnv;
  double lnb = emission_info.lnb;
  if(C!=0.) dP_new *= (b*lnv+a-b*C) / (b*lnv+a);
  
  // Get ∂Φ_rad/∂(lnv,lnb).
  precision_type dphi_rad_dlnv_dlnb = element.dPhi_radiation_dlnv_dlnb(lnv, lnb);
  // If the jacobian is zero we set prob to zero and we are done.
  if(dphi_rad_dlnv_dlnb==0) { prob = 0; return; }

  // get the relevant momenta and dot products (part of emission_info)
  Momentum p_emit; 
  Momentum p_spec;  
  Momentum p_rad ; 
  element.get_emit_spec_rad_kinematics_for_matching(&emission_info, p_emit, p_spec, p_rad, _random_axis);

  // Fill in the pieces directly from the matrix element
  // (we only need dot-products with the beams if we're generating
  // over the full Born polar angle)
  //
  // NB: the colour factor correction is included in the hard ME, i.e.
  //   - no colour correction in H -> ggg (the density is ~ CA)
  //   - TR*nf / CA           in H -> gqqbar
  //   - 2CF/CA               in Z -> qqbarg
  _hard_me->compute_hard_me(event, element, emission_info, _hoppet_runner, p_emit, p_spec, p_rad);
  // get back the splitting weights that are computed in the hard ME
  std::vector<precision_type> splitting_weights = _hard_me->splitting_weights();
  // sum them for the total probability
  precision_type P_sum_all = 0.;
  P_sum_all = std::accumulate(splitting_weights.begin(), splitting_weights.end(), P_sum_all);
  
  // Modified branching probability, modulo possible partition factor in PanLocal case:
  precision_type dP_exact = dphi_rad_dlnv_dlnb                // ∂(xE,xS)/∂(lnv,lnb)
                          * element.lnv_lnb_max_density()/4   // (α_max CA / π) / 4
                          * P_sum_all                         // rest of |ME|^2
                          * element.get_partitioning_factor_matching_probability(&emission_info); // partitioning of the dipole

  // So the acceptance probability is:
  prob = to_double(dP_exact / dP_new);
    
  // Select flavour channel accounting for the choice of splitter above
  double r = gsl.uniform();
  unsigned int channel = splitting_weights.size() - 1;
  precision_type prob_channel = P_sum_all;
  while(channel > 0){ // no need to include 0 because "0" is guaranteed to be accepted
    prob_channel -= splitting_weights[channel];
    if(r > prob_channel/P_sum_all) break;
    channel--;
  }
  // update the weights for the specific channels
  // MvB: old update_emitter_spectator_weights_channel(emission_info, channel, emitter_splits);
  update_emitter_spectator_weights_channel(emission_info, channel);
  
  return;
}

// This function fills the weights for flavour and splitter (gluon vs. quark, and emitter vs. spectator) 
// in EmissionInfo, once the SplittingChannel has been chosen
void MatchedProcess::update_emitter_spectator_weights_channel(ShowerBase::EmissionInfo & emission_info, const unsigned int splitting_channel){
  // first select who splits: emitter or spectator
  bool emitter_splits;
  if(!_shower->only_emitter_splits()){
    // antenna showers - we should make a selection
    emitter_splits = gsl.uniform() < f_fcn(emission_info.lnb) ? true : false;
    emission_info.emitter_splitting_weight = emitter_splits ? 1 : 0;
  } else {
    // dipole showers - the emitter always splits
    emitter_splits = true;
    emission_info.emitter_splitting_weight = 1;
  }
  // and now select the right weights for all the emissions
  if (splitting_channel == QCDinstance::SplittingChannel::fs_g_esr_to_qq || splitting_channel == QCDinstance::SplittingChannel::fs_g_ser_to_qq){ 
    // final-state g->qqbar
    emission_info.emitter_weight_rad_gluon   = 0;
    emission_info.emitter_weight_rad_quark   = 1;
    emission_info.spectator_weight_rad_gluon = 0;
    emission_info.spectator_weight_rad_quark = 1;
  } else if (splitting_channel == QCDinstance::SplittingChannel::fs_q_esr_to_qg || splitting_channel == QCDinstance::SplittingChannel::fs_q_ser_to_qg ||
             splitting_channel == QCDinstance::SplittingChannel::fs_g_esr_to_gg || splitting_channel == QCDinstance::SplittingChannel::fs_g_ser_to_gg)  { 
    // final-state g->gg or q->qg / qbar->qbarg           
    emission_info.emitter_weight_rad_gluon   = 1;
    emission_info.emitter_weight_rad_quark   = 0;
    emission_info.spectator_weight_rad_gluon = 1;
    emission_info.spectator_weight_rad_quark = 0;
  } else{
    assert(false && "This splitting channel is not implemented");
  }
    
  // If the channel was associated with a singularity of spec_dot_rad ->0 (channel 1/3)
  // P_ggg( g1 = spectator_out, g2 = emitter_out, g3 = radiation) or
  // P_gqq(  q = spectator_out,  g = emitter_out, qb = radiation) 
  // we swap the momenta associated to spectator_out and emitter_out, since
  // if the _emitter_is_the_splitter_ then _select_channel_details will
  // associate a gluon PDG ID to momentum spectator_out, and a q/qbar PDG
  // ID to momenta emitter_out and radiation (given the weights assigned just
  // above here). Whereas, channel selections 1 & 3 imply we should be
  // associating instead a gluon PDG ID instead to emitter_out and a q/qbar
  // PDG ID to spectator_out and radiation.
  if ((splitting_channel==QCDinstance::SplittingChannel::fs_g_ser_to_qq ||
       splitting_channel==QCDinstance::SplittingChannel::fs_g_ser_to_gg) 
      && (emitter_splits))
    emission_info.swap_emitter_and_spectator();
  
  // If the channel was associated with a singularity of emit_dot_rad ->0 (channel 2/4)
  // P_ggg( g1 = emitter_out, g2 = spectator_out, g3 = radiation) or
  // P_gqq(  q = emitter_out,  g = spectator_out, qb = radiation) 
  // AND IF the _spectator_is_the_splitter_ then _select_channel_details
  // will associate a gluon PDG ID to momentum emitter_out, and a q/qbar
  // PDG ID to momenta spectator_out and radiation (given the weights
  // assigned just above here). Whereas, channel selections 2 & 4 imply we
  // should be associating instead a gluon PDG ID instead to spectator_out
  // and a q/qbar PDG ID emitter_out and radiation. Hence we swap emitter_out
  // for spectator_out!
  if ((splitting_channel==QCDinstance::SplittingChannel::fs_g_esr_to_gg ||
       splitting_channel==QCDinstance::SplittingChannel::fs_g_esr_to_qq) 
      && (!emitter_splits))
    emission_info.swap_emitter_and_spectator();

}

} // end namespace
