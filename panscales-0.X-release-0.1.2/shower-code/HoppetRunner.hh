//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
/// \file HoppetRunner.hh
/// This file provides an interface to the Hoppet PDF evolution
/// framework.
#ifndef __HOPPET_RUNNER_HH__
#define __HOPPET_RUNNER_HH__
#include <cassert>
#include <cmath>
#include <vector>
#include "QCD.hh"
#include "LimitedWarning.hh"

#include "config.hh"
#ifdef WITH_LHAPDF
//#include <LHAPDF/LHAPDF.h>
namespace LHAPDF {
  class PDF;
}
#endif
///
/// #TRK_ISSUE-136  Questions:
///
/// - if we currently stick to a 1-loop PDF set, we would probably
///   gain in flexibility to use "t=as/(2pi) log(Q^2/Q0^2)" as the
///   reference variable. Is this doable within the hoppet interface?
///
/// - we should decide how to map the range of scales in PanScales to
///   a range of Q2 (or t) values here.
///
/// - what initial condition do we use?
///
/// - We should probably have an evolutino w a fixed nf=5.
///
/// For the moment I've essentially copied the example in Hoppet to
/// get a working base.
///
/// Basic timings:
///  example-framework -out a -shower pythia8 -nev 1e4 -lnvmin -20 -no-spin-corr
///
/// Just Hoppet init              : <t_evgen_ms> = 0.594096 +- 0.0111291
/// init+2pdfcalls                : <t_evgen_ms> = 1.3741 +- 0.00970334
/// 2pdfcalls for particle 0 and 1: <t_evgen_ms> = 0.612007 +- 0.0109068


namespace panscales{

//----------------------------------------------------------------------
/// \class PDF 
/// a small structure to hold the PDF values
class PDF{
public:
  /// the raw PDF data
  double pdf[13];

  /// returns the xf(x) for the flavour associated with the given PDG ID.
  /// Works only for quark and gluon flavours.
  double flav(int pdgid) {
    if (pdgid == 21) return pdf[6];
    else {
      assert(std::abs(pdgid) <= 6);
      return pdf[6 + pdgid];
    }
  }

  /// force the PDF to be >= 0
  void force_positive() {
    for (unsigned i = 0; i < 13; i++) {
      if (pdf[i] < 0.0) pdf[i] = 0.0;
    }
  }

  /// returns the iflv value used in an f90 context (-6...6, with 0 for the gluon)
  inline static int iflv(int pdgid) {
    if (pdgid == 21) return 0;
    else {
      assert(std::abs(pdgid) <= 6);
      return pdgid;
    }
  }

  /// returns the sum of the quarks (xf(x))
  double sum_quarks() const {
    double result = 0.0;
    for (unsigned i = 1; i <= 6; i++) {result += pdf[6+i];}
    return result;
  }
  /// returns the sum of the antiquarks (xf(x))
  double sum_antiquarks() const {
    double result = 0.0;
    for (unsigned i = 1; i <= 6; i++) {result += pdf[6-i];}
    return result;
  }
  /// returns the sum of the quarks (xf(x)) if do_quarks is 
  /// true, otherwise the sum of the anti-quarks
  double sum_quarks_or_antiquarks(bool do_quarks) const {
    return do_quarks ? sum_quarks() : sum_antiquarks();
  }

  /// get the relative contributions of all the quark flavours, i.e.
  /// the vector q_i/(\sum_j q_j)
  std::vector<double> relative_quark_weights() const{
    std::vector<double> qs(6);
    double sum=0.0;
    for (unsigned int i=1; i<=6; ++i){
      qs[i-1] = pdf[6+i];
      sum  += qs[i-1];
    }
    for (unsigned int i=1; i<=6; ++i){
      qs[i-1] /= sum;
    }
    return qs;
  }

  /// get the relative contributions of all the anti quark flavours, i.e.
  /// the vector q_i/(\sum_j q_j)
  std::vector<double> relative_antiquark_weights() const{
    std::vector<double> qbars(6);
    double sum=0.0;
    for (unsigned int i=1; i<=6; ++i){
      qbars[i-1] = pdf[6-i];
      sum     += qbars[i-1];
    }
    for (unsigned int i=1; i<=6; ++i){
      qbars[i-1] /= sum;
    }
    return qbars;
  }

  /// easier access
  double u   () const{ return pdf[6+2]; }
  double ubar() const{ return pdf[6-2]; }
  double d   () const{ return pdf[6+1]; }
  double dbar() const{ return pdf[6-1]; }
  double s   () const{ return pdf[6+3]; }
  double sbar() const{ return pdf[6-3]; }
  double c   () const{ return pdf[6+4]; }
  double cbar() const{ return pdf[6-4]; }
  double b   () const{ return pdf[6+5]; }
  double bbar() const{ return pdf[6-5]; }
  double t   () const{ return pdf[6+6]; }
  double tbar() const{ return pdf[6-6]; }
  double g   () const{ return pdf[6  ]; }
};

//----------------------------------------------------------------------
/// \class HoppetRunner
/// helper class to access PDFs via Hoppet
///
// #TRK_ISSUE-137 GS-NOTES: here's a placeholder for things we may want to consider

// - at the moment we specify Qmin & alphas(Qmin) in hoppet independently of the shower. We may want to get that sorted out so that we can have exactly the same scales in the PDFs and in the shower. 
class HoppetRunner{
public:
  /// list of PDF sets implemented
  enum PDFChoice{
    HERALHC = 2,            ///< a VFN PDF with the HERALHC initial condition at sqrt(2) GeV (with physical scales)
    ToyVFNPhysical = 6,     ///< a VFN PDF with the HERALHC initial condition at 0.5 GeV (physical scales)
    LHAPDFSet = 7,          ///< physical PDFs from LHAPDF (use -lhapdf-set to select the set)
    ToyNf5Physical = 5,     ///< a 5-flavour toy PDF with physical scales 
    ToyNf5  = 1,            ///< a 5-flavour toy PDF with remapped scales, for log tests
    ToyNf5BigAlpha = 3,     ///< a 5-flavour toy PDF with remapped scales, with a bigger alpha_s at Qmin (allowing a larger range of log scales)
    ToyNf5BiggerAlpha = 4,  ///< a 5-flavour toy PDF with remapped scales, with an even bigger alpha_s at Qmin
    ToyNf5Frozen  = 100     ///< PDFs with no evolution
  };

  static const unsigned int Q_MARGIN = 10;
  
  /// ctor (providing a choice of PDF set
  ///
  /// The PDFs will be evolved from a PDFset-dependent Qmin scale
  /// (with a given alphas(Qmin)) up to the scale Qmax.
  ///
  /// Note: even though we're doing a scale mapping at 1-loop, the
  /// Qmax scale of 2.5e7 would allow us to go to alphas=0.02 (passing
  /// either the MSbar or CMW scale to Hopet) and down to the
  /// corresponding lambda=-0.8674 for a physical reach of lambda=-0.5
  /// and a buffer of -18 (lnkt-cutoff=-43). Note that in this case we
  /// may want to insert the CMW correction on the 2-loop evolution
  /// time as well.
  HoppetRunner(PDFChoice pdf_choice_in = ToyNf5, std::string lhapdf_name = "", double Qmax = 1.0e7)
    : _pdf_choice(pdf_choice_in),
      _hoppet_initialised(false), 
      _lhapdf_name(lhapdf_name)
  {
    _set_defaults(Qmax);
  }

  ~HoppetRunner();
  
  /// (re)set the maximum scale Qmax of Hoppet
  void set_Qmax(double Qmax){
    assert(!_hoppet_initialised);
    _set_defaults(Qmax);
  }
  double Qmax() const { return _pdf_set_info.Qmax; }
  PDFChoice pdf_choice() const {return _pdf_choice;}

  /// returns true if we're using PDFs for which a scale remapping is
  /// needed
  bool pdf_needs_remapped_scales() const{ 
    if      (_pdf_choice == ToyNf5 || 
             _pdf_choice == ToyNf5Frozen || 
             _pdf_choice == ToyNf5BigAlpha || 
             _pdf_choice == ToyNf5BiggerAlpha) return true;
    else if (_pdf_choice == HERALHC ||
             _pdf_choice == ToyNf5Physical ||
             _pdf_choice == ToyVFNPhysical ||
             _pdf_choice == LHAPDFSet) return false;
    else (assert(false && "could not recognise _pdf_choice"));
  }

  /// set how the shower scales should be mapped onto the hoppet Q scales
  ///
  /// If use_physical_scales is true, the Hoppet Q is the same as the
  /// shower Q (the other parameters are ignored)
  ///
  /// If use_physical_scales is false, the reference shower scale lnQ
  /// is mapped onto Hoppet's internal Qmax scale. The shower coupling
  /// at the lnQ scale is given by alphasQ and the (fixed) beta0 given
  /// by shower_beta0 (The internal Qmax scale can be passed to the
  /// constructor)
  void set_scales_mapping(bool use_physical_scales,
                          double lnQ=0.0, double alphasQ=0.0, int nloops = 1);
  
  /// set_grid_xxx functions are to be called before "initialise"
  void set_grid_dy    (double dy)   {assert(!_hoppet_initialised); _grid_dy = dy;}
  void set_grid_ymax  (double ymax) {assert(!_hoppet_initialised); _grid_ymax = ymax;}
  void set_grid_order (int  order ) {assert(!_hoppet_initialised); _grid_order = order;}
  void set_grid_nested(bool nested) {assert(!_hoppet_initialised); _grid_nested = nested;}

  /// initialise the run (to be called before any call to get_pdfs)
  void initialise(const QCDinstance & qcd);

  /// returns an object containing x*PDF(x) at a given x and kt
  PDF operator()(double x, double lnkt) const;
  /// returns an object containing x*f(x) at a given x and kt for the given pdgid
  double operator()(double x, double lnkt, int pdgid) const;
  // translation from the shower lnkt to hoppet evolution scale
  double map_lnkt_shower_to_t_hoppet(double lnkt) const;
  // invert the map above
  double map_lnQeff_hoppet_to_lnkt_shower(double lnkt) const;

  /// returns the convolution of the iloop splitting function (for the
  /// specified nf) with the PDF. The result is returned all evaluated
  /// at x and scale Q. This call is expensive on a first invocation, but
  /// subsequent invocations come at the same price as operator()
  PDF pdf_convolution(double x, double lnkt, unsigned int iloop, unsigned int nf) const;

  double pdf_gx_convolution(double x, double lnkt, unsigned int iloop, unsigned int nf, int xx) const;

  /// returns a textual description of the PDF
  std::string description() const;

  /// returns the minimum & maximum lnkt accessible with this PDFset
  /// (and optional scale mapping)
  double lnkt_min() const;
  double lnkt_max() const{ return (_use_physical_scales) ? _pdf_set_info.lnQmax : _shower_lnQ;}

  /// returns the physical (shower) mass of the quark
  /// with flavour iflv
  double m_iflv(int iflv) const {
    double m = m_iflv_internal(iflv);
    // with physical scales, just return that
    if (_use_physical_scales) return m;

    // #TRK_ISSUE-138bis: when mapping gives us an extreme mass, we the conversion
    //                    to double could fail. We should probably handle this
    //                    by returning a precision_type
    if (m != 0.0) {
      return exp(map_lnQeff_hoppet_to_lnkt_shower(log(m)));
      //throw std::runtime_error("HoppetRunner::m_iflv: (with mapping on) mass of quark with flavour " 
      //                + std::to_string(iflv) + " is non zero: " + std::to_string(m));
    }
    return m;
  }

  /// Calculate a massive quark damping factor for the given flavour,
  /// defined as 
  ///
  ///   damping_factor = muF_in^2 / (m^2 + muF_in^2)
  ///
  /// as well as new scale for the evaluation of the PDFs
  ///
  ///   muF_damped^2 = m^2 + muF_in^2
  ///
  /// The evaluation is carried out in double precision, but in 
  /// such a way that if the logs have much greater ranges
  /// (e.g. when the shower's precision_type is double_exp), the results
  /// here will still be sensible.
  ///
  /// One exception is the the value of the mass cannot be represented
  /// in double precision -- work here still to be done...
  void get_massive_quark_damping_factor(int iflv, double lnmuF_in, 
                                        double & damping_factor, double & lnmuF_damped) const {

    double m = m_iflv(iflv);
    // special case of m=0, where we do nothing
    if (m == 0) {
      damping_factor = 1.0;
      lnmuF_damped = lnmuF_in;
      return;
    }

    double lnm2 = 2.0*log(m);
    double lnm2_kt2 = lnm2 - 2*lnmuF_in;    

    // to avoid issues with overflows, write in a way that depends on which 
    // is larger:
    // * base formula is kt^2 / (m^2 + kt^2), which can be written
    // * 1/(m^2/kt^2 + 1) or 
    // * kt^2/m^2 / (1 + kt^2/m^2)    
    if (lnm2_kt2 < 0.0) {
      // when the mass is small compared to kt2, this formula
      // will straightforward tend to 1 
      damping_factor = 1.0/(exp(lnm2_kt2) + 1.0);
      // if the lnmuF_damped formula is changed, remember to change it also
      // below and in massive_quark_damping_lnmuF_in(...)
      lnmuF_damped = lnmuF_in - 0.5 * log(damping_factor);
    } else {
      double kt2_m2 = exp(-lnm2_kt2);
      damping_factor = kt2_m2/(1.0 + kt2_m2);
      lnmuF_damped = 0.5 * (lnm2 + log(1.0 + kt2_m2));
    }

    // std::cout << "damping test: lnmuF_in=" << lnmuF_in 
    //      << ", lnm= " << 0.5*lnm2 
    //      << ", lnmuF_damped=" << lnmuF_damped
    //      << ", lnmuF_in_deduced=" << massive_quark_damping_lnmuF_in(iflv, lnmuF_damped, -10.0)
    //      << std::endl;
  }

  /// returns the value of lnmuF_in that corresponds to the given
  /// lnmuF_damped; the returned lnmuF_in will never go below ln(m)+min_offset
  /// (e.g. if muF_in^2 goes negative or very close to zero)
  double massive_quark_damping_lnmuF_in(int iflv, double lnmuF_damped, double min_ln_offset) const {
    
    double m = m_iflv(iflv);
    if (m == 0) return lnmuF_damped;

    assert(min_ln_offset <= 0);
    double lnm2 = 2.0*log(m);

    double min_lnmuF_in = 0.5*lnm2 + min_ln_offset;

    double lnm2_minus_lnmuF2_damped = lnm2 - 2*lnmuF_damped;

    // no physical situation would ever give an lnmuF2_damped that is
    // below m2
    if (lnm2_minus_lnmuF2_damped >= 0.0) return min_lnmuF_in;

    double lnmuF_in = lnmuF_damped + 0.5*log(- expm1(lnm2 - 2*lnmuF_damped));
    return std::max(lnmuF_in, min_lnmuF_in);

  }

  /// returns mass of the quark with flavour iflv (as stored in the PDF
  /// internal scale representation if using a mapping
  double m_iflv_internal(int iflv) const {
    // uds are given zero mass 
    int abs_iflv = abs(iflv);
    if (abs_iflv <= 3 || abs_iflv > 6) return 0.0;
    switch(abs_iflv) {
      case 4: return _pdf_set_info.mc;
      case 5: return _pdf_set_info.mb;
      case 6: return _pdf_set_info.mt;
      default: assert(false && "Unrecognised abs_iflv");
    };
  }

  /// returns true if the PDF is reliable at the given x value
  bool is_pdf_reliable(double x) const { return x<_pdf_set_info.max_reliable_x; }
  double pdf_max_reliable_x() const { return _pdf_set_info.max_reliable_x; }  
  void set_pdf_max_reliable_x(double max_x) {
    _pdf_set_info.max_reliable_x = max_x;
  }
  unsigned int pdf_max_active_flavours() const{ return _pdf_set_info.max_active_flavours; }
  bool force_positive() const {return _pdf_set_info.force_positive;}

  double max_ISR_overhead() const { return _pdf_set_info.max_ISR_overhead; }

  /// output a table of PDF values at scale lnkt to the given stream
  void output_pdf(std::ostream & ostr, double lnkt) const;

protected:
  // warnings for reaching scales below Qmin and above Qmax
  static LimitedWarning _warning_Qmin, _warning_Qmax;
  
  /// returns the Q value to use with the native PDF, including
  /// various range protections and warnings
  double protected_Q(double lnkt) const;

private:
  /// set the default PDFset parameters
  void _set_defaults(double Qmax);

  /// an internal structure for holding some PDFset-dependent
  /// information
  struct PDFSetInfo{
    double Qmin, Qmax;   ///< min and max available scales
    double alphasQmin;   ///< strong coupling at the Qmin scale
    double lnQmax;       ///< log of the above Qmax

    // info about heavy uarks and active flavours
    unsigned int max_active_flavours; ///< maximal number of PF flavours
    double mc, mb, mt;                ///< quark masses inside the PDFs

    double max_reliable_x; ///< the max x value up to which we'll trust the PDFs
                           ///< at the moment only used to "not complain about accept_prob>1"
    bool force_positive = false; ///< if true, the PDFs are forced to be positive

    /// @brief the maximum ISR overhead for the PDF set
    /// @details when doing ISR branching, we use ratios of the kind
    ///          f(x_new, mu)[iflv_new] / f(x_old, mu)[iflv_old]
    ///          which can go to infinity if the PDF is zero
    ///          (or if force_positive is true, and the PDF is negative)
    ///
    ///          This function returns a the value that is to be used
    ///          as an upper bound on the overall ISR overhead (summed
    ///          across flavours) with this PDF.
    double max_ISR_overhead = 1e100;

    /// a pointer to a function used for Hoppet initialisation
    void (*init_fct_ptr)(const double &, const double &, double *);
  };
  
  const PDFChoice _pdf_choice;  ///< choice of PDF set (see enum above)
  PDFSetInfo _pdf_set_info;     ///< extra info associated w the choice of PDF set

  // info about matching Hoppet scales to shower scales
  bool _use_physical_scales = true; ///< when true, no conversions are applied
                                    ///< between the shower and Hoppet scales
  double _shower_lnQ;        ///< the shower hard scale
  double _shower_alphasQ;    ///< the shower alphas(Q)
  int    _shower_nloops;     ///< the number of loops for the shower coupling
  double _hoppet_alphasQmax = -1; ///< alphas(Qmax) in Hoppet
  
  bool _hoppet_initialised;     ///< true if hoppetStart/hoppetEvolve called (or PDF ready to be used)

  double _grid_dy = 0.05, _grid_ymax = 20.0;
  int    _grid_order = -6;
  bool   _grid_nested = true;
  
#ifdef WITH_LHAPDF
  LHAPDF::PDF *_lhapdf_pdfset = nullptr;
#endif
  std::string _lhapdf_name;
#include "autogen/auto_HoppetRunner_HoppetRunner.hh"
}; // end class HoppetRunner



// /// overloaded output for PDFChoice
// std::ostream & operator<<(std::ostream & ostr, panscales::HoppetRunner::PDFChoice val);
// 
// /// overloaded input for PDFChoice
// std::istream & operator>>(std::istream & istr, panscales::HoppetRunner::PDFChoice & val);

} 



#include "autogen/auto_HoppetRunner_global-hh.hh"
#endif // __HOPPET_RUNNER_HH__
