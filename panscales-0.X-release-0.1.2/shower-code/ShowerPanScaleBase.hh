//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
/// \file ShowerPanScaleBase.hh
/// Contains (at least some of the) code that is common to the different
/// PanScale showers
#ifndef __SHOWERPANSCALEBASE_HH__
#define __SHOWERPANSCALEBASE_HH__

#include "ShowerBase.hh"

namespace panscales{

/// recoil options for the rescaling in the case of PanGlobal
enum PanGlobalRescaleOption {
  LocalRescaling = 1,            ///< e+e- and pp/DIS: rescale all dipole legs and impose Qbar^2 = Q^2 (e+e-) or Q.(pi+pj+pk) = Q.(pitilde+pjtilde) (pp/DIS)
  DoubleRescaling = 2,           ///< only for e+e-: rescale dipole first to ensure Q.(pi+pj+pk) = Q.(pitilde+pjtilde), followed by global rescaling 
  LocalijRescaling = 3,          ///< only for e+e-: rescale emitter and spectator and impose Qbar^2 = Q^2
  GlobalRescalingDeprecated = 0  ///< perform only a global rescaling (deprecated, breaks PanScales no-side-effect conditions)
}; 

//--------------------------------------------------------------------------------
/// \class ShowerPanScaleBase
/// base class for all the PanScales shower algorithms
class ShowerPanScaleBase : public ShowerBase {
public:
  /// default ctor
  ShowerPanScaleBase(const QCDinstance & qcd, double beta_in) 
    : ShowerBase(qcd), _beta(beta_in) {}

  /// given that we have virtual functions in a number of places,
  /// we need a virtual destructor to avoid compiler warnings
  virtual ~ShowerPanScaleBase() {}
  
  /// implements the element and splitting info as subclasses
  class Element;
  class EmissionInfo;
  virtual ShowerBase::EmissionInfo* create_emission_info() const override;

  /// meaning of kinematic variables
  std::string name_of_lnv() const override {return "ln(v)";}
  std::string name_of_lnb() const override {return "eta";}

  /// returns true if the coupling depends not just on kt, but also
  /// on z; for the standard NLL showers, the scale prescription
  /// of 2022-09-11 will introduce a z dependence, because the
  /// the scale cancellation will be made exact only for small z
  bool coupling_depends_on_z() const override {return qcd().lnxmuR() != 0.0;}

  /// returns true if the shower involves a second-order piece
  bool has_alphas2_coeff() const override {return qcd().lnxmuR() != 0.0;}

  /// beta of the ordering variable
  double beta() const override {return _beta;}

protected:
  double _beta;   ///< angular power for the ordering variable
};


//--------------------------------------------------------------
/// \class ShowerPanScaleBase::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs common to all the PanScales shower algorithms
class ShowerPanScaleBase::Element : public ShowerBase::Element {
public:
  /// dummy ctor  
  Element() {}

  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleBase * shower) :
    ShowerBase::Element(emitter_index, spectator_index, dipole_index, event),
    _shower_panscalebase(shower){
    update_kinematics();
  }

  /// given that we have virtual functions in a number of places,
  /// we need a virtual destructor to avoid compiler warnings
  virtual ~Element() {}

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------
  virtual const double betaPS() const override { return _shower_panscalebase->_beta ; }

  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  // all defined in the various PanScales derived classes
  
  //--------------------------------------------------
  // important definitions to be implemented in all derived classed
  //--------------------------------------------------
  // all defined in the various PanScales derived classes
  
  //----------------------------------------------------
  // caching info to facilitate the work of ShowerRunner
  //----------------------------------------------------
  void update_kinematics() override;
  void print_characteristics() override;

  /// #TRK_ISSUE-350  GPS: not clear if this is ever used
  void update_indices(unsigned emitter_index, unsigned spectator_index);

  //--------------------------------------------------------
  // Things useful for observable-dependent dynamic cut-offs 
  //--------------------------------------------------------
  /// for a given lnv,lnb, returns approximate lnkt in the
  /// emitter-spectator dipole rest frame, which is equivalent to the
  /// frame-independent lnkt wrt to the emitter-spectator system.
  /// This lnkt will be correct in the soft+collinear limit but may
  /// differ from the true lnkt in the soft large-angle limit and the
  /// hard-collinear limit
  virtual double lnkt_approx(double lnv, double lnb) const override;

  /// for a given lnv,lnb, returns the pseudorapidity with respect to
  /// the closer of the emitter/spectator, defined in the event
  /// centre-of-mass frame. This eta will be correct in the
  /// soft+collinear limit but may differ from the true eta in the
  /// soft large-angle limit and the hard-collinear limit.
  virtual double eta_approx (double lnv, double lnb) const override;

  /// by construction, the min |eta| is always for lnb = 0 for the PanScale
  /// showers
  virtual double lnb_for_min_abseta(double lnv) const override {return 0.0;}
  /// alphas for renormalisation-scale uncertainty estimates
  double alphas2_coeff(const typename ShowerBase::EmissionInfo * emission_info) const override;

  /// returns true if the underlying shower is using direction differences
  bool use_diffs() const override {return _shower_panscalebase-> use_diffs();}

  /// returns true if the underlying shower is using double soft corrections
  bool double_soft() const override {return _shower_panscalebase-> double_soft();}

  /// Analytic double soft approximation for a given shower. 
  /// Has to be specified for each shower.
  virtual precision_type double_soft_weight_shower(int history, int iflav1, 
                                                const Momentum pi, const Momentum pj, 
                                                const Momentum pk, const Momentum pl) const;

  virtual void double_soft_find_pi_pj_pk_pl(typename ShowerBase::EmissionInfo * emission_info, 
                                  Momentum & pi, Momentum & pj, Momentum & pk, Momentum & pl, int & iflav, bool &iklj) const;

  /// tells you whether we want to boost at the end of the shower (pp)
  bool boost_at_end() const override {return _shower_panscalebase-> boost_at_end();}

protected:
  /// this pointer to the base type (v. pointer to the derived type
  /// in derived classes) is a bit ugly; we might try to unify them
  /// at a later stage, even if it will be a little bit ugly in
  /// places.
  const ShowerPanScaleBase * _shower_panscalebase;
  
  precision_type _dipole_m2;
  // probably following can be double instead of precision type 
  // (same for _log_rho and _half_ln_sj_over_sij_si)
  precision_type _log_dipole_m2;
  precision_type _sitilde;
  precision_type _sjtilde;
  precision_type _Q2;
  precision_type _rho;
  precision_type _log_rho;
  precision_type _half_ln_sj_over_sij_si;
};

//--------------------------------------------------------------
/// \class ShowerPanScaleBase::EmissionInfo
/// emission information specific to all the PanScales shower
class ShowerPanScaleBase::EmissionInfo : public ShowerBase::EmissionInfo {
public:
  /// kinematic map variables
  precision_type kt, ak, bk;
};


} // namespace panscales

#include "autogen/auto_ShowerPanScaleBase_global-hh.hh"
#endif //  __SHOWERPANSCALEBASE_HH__
