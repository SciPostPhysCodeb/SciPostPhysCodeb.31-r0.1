//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __SHOWERPOWHEG_HH__
#define __SHOWERPOWHEG_HH__

#include "ShowerPanScaleBase.hh"

namespace panscales{

//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerPowheg 
/// An implementation of the Powheg (FKS) shower mapping for a first emission off 
/// a 2-body system, adapted to have a beta parameter (see arXiv:2301.09645). 
/// This is NOT a fully fledged shower, but is instead only to be used for generating 
/// a first emission for matching purposes.
class ShowerPowheg : public ShowerPanScaleBase {
public:
  /// default ctor
  ShowerPowheg(const QCDinstance & qcd, double beta_in) : ShowerPanScaleBase(qcd, beta_in){}

  /// virtual dtor
  virtual ~ShowerPowheg() {}

  /// implements the element and splitting info as subclasses
  class Element;
  class EmissionInfo;
  virtual ShowerBase::EmissionInfo* create_emission_info() const override;

  /// description of the class
  std::string description() const override {return name()+" shower (First emission only), derived from ShowerPanScaleBase";}
  /// name of the shower
  std::string name() const override{return "Powheg-ee";}

  /// meaning of kinematic variables
  std::string name_of_lnv() const override {return "ln(v)";}
  std::string name_of_lnb() const override {return "eta";}
 
  /// this is a dipole shower (2 elements per dipole)
  unsigned int n_elements_per_dipole() const override { return 2; }

  /// returns true if an emission happens at a phase-space point that has
  /// already been covered by the matching shower (used in "truncated" showers)
  virtual bool veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const override;

  /// element store
  virtual std::vector<std::unique_ptr<ShowerBase::Element> > elements(Event & event, int dipole_index) const override;

  /// this is a dipole-like shower, so only the emitter splits
  bool only_emitter_splits() const override {return true;}

  /// given kinematics from an event, returns the lnv, lnb values
  /// that correspond to (ĩ,j̃) -> (i,j,k)
  virtual bool find_lnv_lnb_from_kinematics(const Momentum &p_emit, 
                                    const Momentum &p_spec, 
                                    const Momentum &p_rad, 
                                    const precision_type &p_emit_dot_p_spec,
                                    const precision_type &p_emit_dot_p_rad,
                                    const precision_type &p_spec_dot_p_rad,
                                    const Event &event,
                                    double &lnv, double &lnb) const override;

};


//--------------------------------------------------------------
/// \class ShowerPowheg::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs for the Dire shower algorithm
class ShowerPowheg::Element : public ShowerBase::Element {
public:
  /// dummy ctor  
  Element() {}

  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPowheg * shower) :
    ShowerBase::Element(emitter_index, spectator_index, dipole_index, event),
    _shower(shower){
    update_kinematics();
  }

  /// given that we have virtual functions in a number of places,
  /// we need a virtual destructor to avoid compiler warnings
  virtual ~Element() {}

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------
  const double alphas_lnkt(double lnv, double lnb) const override {
    return lnv + _shower->beta()*fabs(lnb);
  }

  const double betaPS() const override { return _shower->_beta ; }

  double lnb_extent_const()     const override {
    return 2*to_double(log_T(_dipole_m2))/(1 + _shower->_beta);
  }

  double lnb_extent_lnv_coeff() const override {
    return -2.0/(1 + _shower->_beta);
  }
  Range lnb_generation_range(double lnv) const override;
  bool has_exact_lnb_range() const override {return true;}
  Range lnb_exact_range(double lnv) const override;
  double lnv_lnb_max_density() const override;
  // jacobian of the shower's first emission
  precision_type dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const override;
  /// returns the partitioning factor for the matching probability
  precision_type get_partitioning_factor_matching_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  //--------------------------------------------------
  // important definitions to be implemented in all derived classed
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  ///
  /// This fills the channel information as well as cached kinematic
  /// variables in emission_info
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// carry out the splitting and update the event; return true if
  /// successful; the RotatedPieces pass on a potentially rotated version
  /// of the emitter, spectator, and perpendicular vectors, and these
  /// are the ones that should be used, not those from emission_info; 
  /// the output is stored back in emission_info (possibly still rotated)
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;

  //----------------------------------------------------
  // caching info to facilitate the work of ShowerRunner
  //----------------------------------------------------
  /// update the element kinematics (called after an emission)
  virtual void update_kinematics() override;
  
  /// update the element indices and kinematics
  void update_indices(unsigned emitter_index, unsigned spectator_index);

  //--------------------------------------------------
  // Things useful for observable-dependent dynamic cut-offs (KH-02-07-2019)
  //--------------------------------------------------

  /// for a given lnv,lnb, returns approximate lnkt in the
  /// emitter-spectator dipole rest frame, which is equivalent to the
  /// frame-independent lnkt wrt to the emitter-spectator system.
  /// This lnkt will be correct in the soft+collinear limit but may
  /// differ from the true lnkt in the soft large-angle limit and the
  /// hard-collinear limit
  virtual double lnkt_approx(double lnv, double lnb) const override;
  
  /// for a given lnv,lnb, returns the pseudorapidity with respect to
  /// the closer of the emitter/spectator, defined in the event
  /// centre-of-mass frame. This eta will be correct in the
  /// soft+collinear limit but may differ from the true eta in the
  /// soft large-angle limit and the hard-collinear limit.
  ///
  /// See tests in 2019-07-approx-lnkt-eta/ and
  /// logbook/2019-07-27--approx-lnkt-eta
  virtual double eta_approx(double lnv, double lnb) const override;

  virtual double lnb_for_min_abseta(double lnv) const override;

  bool use_diffs() const override {return _shower->use_diffs();}

  double alphas2_coeff(const typename ShowerBase::EmissionInfo * emission_info) const override {  
    const auto & qcd = _shower->qcd();
    if (qcd.nloops() <= 1) {
      return 0;
    } else {
      // 
      // powheg's z_radiation_wrt_splitter() can be > 1 if we are outside the phase space
      // Here we just bound it to be <=1
      double z = std::min(1.0,to_double(emission_info->z_radiation_wrt_splitter()));
      return 2 * qcd.b0() * qcd.lnxmuR() * (1.0 - z);
    }
  }

private:
  const ShowerPowheg * _shower;
  precision_type _dipole_m2;
};


//--------------------------------------------------------------
/// \class ShowerPowheg::EmissionInfo
/// emission information specific to the Powheg shower
class ShowerPowheg::EmissionInfo : public ShowerBase::EmissionInfo {
public:
  /// powheg phase space variables
  precision_type xsi, y, omy, omy2;
};
 

} // namespace panscales

#endif // __SHOWERPOWHEG_HH__
