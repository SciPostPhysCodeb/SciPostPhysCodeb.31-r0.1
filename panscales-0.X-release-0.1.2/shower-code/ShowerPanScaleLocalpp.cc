//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerPanScaleLocalpp.hh"

namespace panscales{

  //----------------------------------------------------------------------
  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerPanScaleLocalpp::elements(
    Event & event, int dipole_index) const {
    int i3    = event.dipoles()[dipole_index].index_3()   ;
    int i3bar = event.dipoles()[dipole_index].index_3bar();
    ShowerPanScaleLocalpp::Element *elm1, *elm2;
    if (event.particles()[i3].is_initial_state()){
      if (event.particles()[i3bar].is_initial_state()){
        // II dipole
        elm1 = new ShowerPanScaleLocalpp::ElementII(i3, i3bar, dipole_index, &event, this);
        elm2 = new ShowerPanScaleLocalpp::ElementII(i3bar, i3, dipole_index, &event, this);
      } else {
        // 3 is I, 3bar is F
        elm1 = new ShowerPanScaleLocalpp::ElementIF(i3, i3bar, dipole_index, &event, this);
        elm2 = new ShowerPanScaleLocalpp::ElementFI(i3bar, i3, dipole_index, &event, this);
      }
    } else {
      if (event.particles()[i3bar].is_initial_state()){
        // 3 is F, 3bar is I
        elm1 = new ShowerPanScaleLocalpp::ElementFI(i3, i3bar, dipole_index, &event, this);
        elm2 = new ShowerPanScaleLocalpp::ElementIF(i3bar, i3, dipole_index, &event, this);
      } else {
        // 3 and 3bar are F
        elm1 = new ShowerPanScaleLocalpp::ElementFF(i3, i3bar, dipole_index, &event, this);
        elm2 = new ShowerPanScaleLocalpp::ElementFF(i3bar, i3, dipole_index, &event, this);
      }
    }      
    std::unique_ptr<typename ShowerBase::Element> elms[] = {
      std::unique_ptr<typename ShowerBase::Element>(elm1),
      std::unique_ptr<typename ShowerBase::Element>(elm2)
      };
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }

  ShowerBase::EmissionInfo* ShowerPanScaleLocalpp::create_emission_info() const{ return new ShowerPanScaleLocalpp::EmissionInfo(); }

  //----------------------------------------------------------------------
  /// base construction of the acceptance_probability;
  /// fills the alphak, betak, kappat variables
  /// same for every element so we put it here
  bool ShowerPanScaleLocalpp::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalpp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;
    
    // see Eqs. (4.3) and (4.6) in arXiv:2205.02237
    precision_type kappat = _rho*exp(precision_type(lnv + _shower->_beta*abs(lnb))); 
    precision_type alphak = sqrt(_sjtilde/(_dipole_m2*_sitilde))*kappat*exp(precision_type(lnb)); 
    // now compute betak
    // betak  = sqrt(_sitilde/(_dipole_m2*_sjtilde))*kappat*exp(-lnb);
    // but we can use 1/alpha = sqrt(_sitilde/_sjtilde)*sqrt(_dipole_m2)*1/kappat*exp(-lnb)
    // so betak = kappat^2 / _dipole_m2 * 1/alphak
    precision_type betak = pow2(kappat)/_dipole_m2/alphak;

    emission_info.kappat = kappat;
    emission_info.alphak = alphak;
    emission_info.betak  = betak;

    // this is a dipole shower: only the emitter can split
    emission_info.emitter_weight_rad_gluon   = 0.0;
    emission_info.emitter_weight_rad_quark   = 0.0;
    emission_info.spectator_weight_rad_gluon = 0.0;
    emission_info.spectator_weight_rad_quark = 0.0;

    // in case this II element will be accepted we need to update all elements in the dipole cache
    emission_info.full_store_update_required = true;

    return true;
  }


  //----------------------------------------------------------------------
  // update the element indices and kinematics
  // #TRK_ISSUE-456  is this ever used?
  void ShowerPanScaleLocalpp::Element::update_indices(unsigned emitter_index, unsigned spectator_index) {
    _emitter_index   = emitter_index;
    _spectator_index = spectator_index;
    update_kinematics();
  }

  //----------------------------------------------------------------------
  // Boost event and realign incoming particles (see Appendix B in arXiv:2205.02237 for more details)
  void ShowerPanScaleLocalpp::Element::lorentz_boost_realign(const Momentum& current_Q) const {
    // We force the initial-state particles to be back to back by performing the longitudinal boost.
    // The CM frame is the beam CM frame, so when we rotate we have the correct PDF fractions

    // need the reversed because we want to go from current_Q to its rest frame
    LorentzBoost boost(current_Q.reversed()); 
    if(use_diffs()){
      // we need to do the boost but also store the energies of the particles before they enter the boost
      // first set up the boost class
      // then handle the boost, making sure we store the energies
      std::vector<precision_type> Ei(_event->size());
      for(unsigned i = 0; i < _event->size(); ++i) {
        Particle & p = (*_event)[i];
        Ei[i] = p.E();
        (*_event)[i].reset_momentum(p.boost(boost));
      }
       // then handle the dipole differences
      for (auto & dipole: _event->dipoles()) {
        int i3bar = dipole.index_3bar();
        int i3    = dipole.index_3();
        dipole.dirdiff_3_minus_3bar = apply_linear_transform_to_dirdiff(boost, Ei[i3bar], Ei[i3], (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);       
      }

      // then handle the dipole differences for non-splitting dipoles as well
      for (auto & dipole: _event->non_splitting_dipoles()) {
        int i3bar = dipole.index_3bar();
        int i3    = dipole.index_3();
        dipole.dirdiff_3_minus_3bar = apply_linear_transform_to_dirdiff(boost, Ei[i3bar], Ei[i3], (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);      
           
      }
    }
    else{
      // just do the boost 
      for(unsigned i = 0; i < _event->size(); ++i) {
        Particle & p = (*_event)[i];
        (*_event)[i].reset_momentum(p.boost(boost));
      }
    }
    // MvB changed the below line - was not working!
    // const Particle & pa = _event->beam1().pz() > 0 ? _event->incoming1() : _event->incoming2();
    // new code:
    Momentum4<precision_type> pa = _event->incoming1().momentum();
    // reverse components if pz < 0
    if(pa.pz() < 0) pa = pa.reversed();

    // set up the rotation matrix
    Matrix3 rot_pa_to_z = Matrix3::from_direction(Momentum3<precision_type>(pa.px(), pa.py(), pa.pz())).transpose();

    // do the rotation
    for(unsigned i = 0; i < _event->size(); ++i) {
      Particle & p = (*_event)[i];
      Momentum3<precision_type> new_mom_p3;
      // enforce the exact kinematics for incoming particles
      if(p.is_initial_state()){
        if(p.pz() < 0) new_mom_p3 = Momentum3<precision_type>(0,0,-p.E());
        else           new_mom_p3 = Momentum3<precision_type>(0,0, p.E());
      } else {// particle is final state, do the rotation
        new_mom_p3 = rot_pa_to_z * Momentum3<precision_type>(p.px(), p.py(), p.pz());
      }
      (*_event)[i].reset_momentum(Momentum::fromP3M2(new_mom_p3, p.m2()));
    }

    // rotate the directional differences as well 
    if(use_diffs()){
       // then handle the dipole differences
      for (auto & dipole: _event->dipoles()) {
        dipole.dirdiff_3_minus_3bar = rot_pa_to_z *dipole.dirdiff_3_minus_3bar; 
        // explicitly set the II dipoles with 0 px and py components 
        if((*_event)[dipole.index_3()].is_initial_state() && (*_event)[dipole.index_3bar()].is_initial_state())  {
          dipole.dirdiff_3_minus_3bar  = Momentum3<precision_type>(0,0,dipole.dirdiff_3_minus_3bar.pz());
        }
      }
      // then handle the dipole differences for non-splitting dipoles as well
      for (auto & dipole: _event->non_splitting_dipoles()) {
        dipole.dirdiff_3_minus_3bar = rot_pa_to_z *dipole.dirdiff_3_minus_3bar;        
      }

    }
  
  } 

  //----------------------------------------------------------------------
  // #TRK_ISSUE-460 Postpone the boost until the end of the showering process
  // This is assuming Q in the lab frame does not have any kT component
  // so that we can always write Q = a1 * beam1() + a2 * beam2()
  void ShowerPanScaleLocalpp::Element::reset_beam_and_shower_Q(precision_type x1, precision_type x2, const Momentum& current_Q) const {

    Momentum shower_Q = (*_event).Q();
    precision_type sAB, sA, sB;
    sA = dot_product(shower_Q, (*_event).beam1());
    sB = dot_product(shower_Q, (*_event).beam2());
    sAB = dot_product((*_event).beam1(), (*_event).beam2());

    //Now reset the beams with pA = pa/xa, pB = pb/xb
    //so that they remain aligned with the incoming partons
    _event->reset_beam1((*_event)[0]/x1);
    _event->reset_beam2((*_event)[1]/x2);

    //New shower_Q using the updated beams
    shower_Q = sB/sAB * (*_event).beam1() +  sA/sAB * (*_event).beam2();
    _event->reset_Q(shower_Q);
  } 

  //======================================================================
  // ShowerPanScaleLocalpp::ElementII implementation
  //======================================================================
  double ShowerPanScaleLocalpp::ElementII::lnb_extent_const() const {
    // convert to double to avoid slowing down the code
    double xi      = to_double(event().pdf_x(emitter()));
    double xj      = to_double(event().pdf_x(spectator()));
    double xi_over_omxi = xi / (1 - xi);
    double xj_over_omxj = xj / (1 - xj);
    double lnxij_over_omxij = log(xi_over_omxi*xj_over_omxj);
    return to_double(_log_dipole_m2 - 2.*_log_rho - lnxij_over_omxij)/(1. + _shower->_beta);
  }

  Range ShowerPanScaleLocalpp::ElementII::lnb_generation_range(double lnv) const{
    // We assume here that alphak and betak are small
    double xi      = to_double(event().pdf_x(emitter()));
    double xj      = to_double(event().pdf_x(spectator()));
    double xi_over_omxi = xi / (1 - xi);
    double xj_over_omxj = xj / (1 - xj);
    double lnxi_over_omxi = log(xi_over_omxi);
    double lnxj_over_omxj = log(xj_over_omxj); 
    //
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv + lnxj_over_omxj ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv - lnxi_over_omxi ) / (1+_shower->_beta));
  }
  
  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleLocalpp::ElementII::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    //construct alphak, betak which is the same for every element
    ShowerPanScaleLocalpp::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalpp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));

    // determine the z factors that go into the splitting kernels
    // See Eq. (4.8) in arXiv:2205.02237
    // we say that z = ak/ai with ak = alphak in the soft limit, ai = (1+alphak)
    precision_type z   = emission_info.alphak / (1. + emission_info.alphak);
    precision_type omz = 1.                   / (1. + emission_info.alphak);

    
    // here we should make a decision based on flavour.
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), z,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    // the spectator PDF fraction is unchanged up to O(kt):
    // We have bj = (1+ak+bk)/(1+ak) = 1 + bk/(1+ak) \simeq 1 for bk << ak
    // So we take 'opposite_beam'_pdf_new_x_over_old = 1
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = 1.0/omz;  
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = 1.0/omz;  
    }
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = z;
    emission_info.z_radiation_wrt_spectator = 0.;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    normalisation_factor *= g_fcn(emission_info.lnb);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;
    
    return true;
  }

  //----------------------------------------------------------------------
  // carry out the splitting and update the event
  bool ShowerPanScaleLocalpp::ElementII::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    
    typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));
    
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);

    // build the radiated mapping coefficients
    // that can be found in
    // Eq. (3.168) in logbook/2020-12-09-pp-schemes.pdf
    // #TRK_ISSUE-466 For now we decided to remove the c factor from the expressions
    // This results in a diamond-shaped gap in phase-space coverage at the top of the Lund plane
    // with c factor // const precision_type c     = sqrt(1. + emission_info.alphak*emission_info.betak);
    // with c factor // we found by hand that this value of delta works nicely at the
    // with c factor // top of the plane for beta=1/2
    // with c factor // const precision_type delta = 1. + _shower->_beta*1.5;
    double gamma = _shower->gamma_value();
    
    // We remove the c factor, below are the old expressions
    // with c factor // precision_type ak = emission_info.alphak/c;
    // with c factor // precision_type bk = pow((pow(c,delta) + emission_info.alphak), (2.*gamma))*emission_info.betak/c;

    // Eq. (4.17) in arXiv:2205.02237
    precision_type ak = emission_info.alphak;
    precision_type bk = pow(1 + emission_info.alphak, (2.*gamma))*emission_info.betak;

    precision_type norm_perp = sqrt(ak*bk*_dipole_m2);

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);

    // Eqs. (B54a-c) in arXiv:2205.02237
    precision_type ai = 1. + ak;
    precision_type bi = ak * bk / ai;
    precision_type bj = (ai + bk) / ai;

    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> spectator_out3 =                        bj * rp.spectator.p3();
    
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
   
    // update the PDF x fractions (with the ones after the boost)
    // we need the momentum of the hard system here
    Momentum3<precision_type> p_hard3 = rp.rotn_from_z.transpose()* _event->momentum_sum_hard_system();
    Momentum p_hard = Momentum::fromP3M2(p_hard3, _event->momentum_sum_hard_system().m2());
    
    // construct the dot product with the hard momentum 
    precision_type dot_i = dot_product(p_hard, emission_info.emitter_out)/dot_product(p_hard, rp.emitter);
    // and with the spectator
    precision_type dot_j = dot_product(p_hard, emission_info.spectator_out)/dot_product(p_hard, rp.spectator);

    //and fill the PDF factors, depending on which one was the emitter
    //note that 1 + ak + bk = bj*ai
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = sqrt(dot_i/dot_j * bj * ai);  
      emission_info.beam2_pdf_new_x_over_old = sqrt(dot_j/dot_i * bj * ai);
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = sqrt(dot_j/dot_i * bj * ai);
      emission_info.beam2_pdf_new_x_over_old = sqrt(dot_i/dot_j * bj * ai);  
    }

    return true;
  }
  
  //----------------------------------------------------------------------
  // do the final boost
  void ShowerPanScaleLocalpp::ElementII::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {

    // Here we boost at every step of the shower evolution
    // we need to cache PDF fractions before the emission 
    // as these will be used later by the longitudinal boost
    precision_type xatilde = event().pdf_x(emitter());
    precision_type xbtilde = event().pdf_x(spectator());
    
    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalpp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    const typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));

    // construct the new PDF x-fractions
    precision_type x1, x2;
    Momentum current_Q;
    if(emitter().initial_state()==1){
      x1 = xatilde*emission_info.beam1_pdf_new_x_over_old;
      x2 = xbtilde*emission_info.beam2_pdf_new_x_over_old;
      current_Q = emitter()/x1 + spectator()/x2;
    }else{
      x1 = xbtilde*emission_info.beam1_pdf_new_x_over_old;
      x2 = xatilde*emission_info.beam2_pdf_new_x_over_old;
      current_Q = emitter()/x2 + spectator()/x1;
    }
    // 
    if(!(boost_at_end())) ShowerPanScaleLocalpp::Element::lorentz_boost_realign(current_Q);
    else ShowerPanScaleLocalpp::Element::reset_beam_and_shower_Q(x1, x2, current_Q);

  }

  //======================================================================
  // ShowerPanScaleLocalpp::ElementIF implementation
  //======================================================================

  double ShowerPanScaleLocalpp::ElementIF::lnb_extent_const() const {
    double xi      = to_double(event().pdf_x(emitter()));
    double xi_over_omxi = xi / (1 - xi);
    double lnxij_over_omxij = log(xi_over_omxi);
    return to_double(_log_dipole_m2 - 2.*_log_rho - lnxij_over_omxij)/(1. + _shower->_beta);
  }

  Range ShowerPanScaleLocalpp::ElementIF::lnb_generation_range(double lnv) const{
    // We assume here that alphak and betak are small
    double xi      = to_double(event().pdf_x(emitter()));
    double xi_over_omxi = xi / (1 - xi);
    double lnxi_over_omxi = log(xi_over_omxi);
    
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv                  ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv - lnxi_over_omxi ) / (1+_shower->_beta));
  }

  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleLocalpp::ElementIF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    //construct alphak, betak which is the same for every element
    ShowerPanScaleLocalpp::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalpp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));
    
    // z is determined in the same way as the II case
    precision_type z   = emission_info.alphak / (1. + emission_info.alphak);
    precision_type omz = 1.                   / (1. + emission_info.alphak);

    // here we should make a decision based on flavour.
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), z,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    // We have bj = (1+ak+bk)/(1+ak) = 1 + bk/(1+ak) \simeq 1 for bk << ak
    // So we take 'opposite_beam'_pdf_new_x_over_old = 1
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = 1.0/omz;   
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = 1.0/omz;  
    }
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = z;
    emission_info.z_radiation_wrt_spectator = 0.;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    normalisation_factor *= g_fcn(emission_info.lnb);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;

    // in case this IF element will be accepted we do not need to update all elements in the dipole cache
    emission_info.full_store_update_required = true;
    
    return true;
  }
  
  //----------------------------------------------------------------------
  // check after doing the acceptance prob whether the actual kinematics can return false
  bool ShowerPanScaleLocalpp::ElementIF::check_after_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // retrieve emission info
    typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));
    
    // build the radiated mapping coefficients
    // that can be found in
    // Eq. (3.168) in logbook/2020-12-09-pp-schemes.pdf
    // #TRK_ISSUE-466 we decided to remove the c factor for now
    // with c factor // const precision_type c     = sqrt(1. + emission_info.alphak*emission_info.betak);
    // with c factor // const precision_type delta = 1. + _shower->_beta*1.5;
    double gamma = _shower->gamma_value();

    // with c factor // precision_type ak = emission_info.alphak/c;
    // with c factor // precision_type bk = pow((pow(c,delta) + emission_info.alphak), (2.*gamma))*emission_info.betak/c;
    // Eq. (4.17) in arXiv:2205.02237
    emission_info.ak = emission_info.alphak; 
    emission_info.bk = pow(1 + emission_info.alphak, (2.*gamma))*emission_info.betak;
    // following can be found in
    // Eqs. (B56a-c) in arXiv:2205.02237
    emission_info.ai = 1 + emission_info.ak;
    emission_info.bi = emission_info.ak * emission_info.bk / emission_info.ai;
    emission_info.bj = 1 - emission_info.bk / emission_info.ai;
    
    // check whether bj can fall below zero. Possible when ai < bk
    // approximately betak*(1+alphak^2)/(1+alphak) , alphak > 1 but betak < 1
    if(emission_info.bj <= 0) return false;

    return true;

  }

  //----------------------------------------------------------------------
  // carry out the splitting and update the event
  bool ShowerPanScaleLocalpp::ElementIF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{

    typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));
    
    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);
    
    // retrieve cached info
    const double & phi = emission_info.phi;
    const precision_type & ak = emission_info.ak; 
    const precision_type & bk = emission_info.bk;
    const precision_type & ai = emission_info.ai;
    const precision_type & bi = emission_info.bi; 
    const precision_type & bj = emission_info.bj; 
    
    // perp normalisation 
    precision_type norm_perp = sqrt(ak*bk*_dipole_m2);

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    // store for spin, when using dir-diffs this get rotated to the right plane
    emission_info.k_perp = perp;

    //fill the emitter, radiator and spectator 3-momenta
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> spectator_out3 = bj * rp.spectator.p3();

    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    // update the PDF x fractions (with the ones after the boost)
    // we need the momentum of the hard system here
    if(emitter().is_in_hard_system()){
      emission_info.radiation_is_hard = true;
    }
    Momentum3<precision_type> p_hard3 = rp.rotn_from_z.transpose()* _event->momentum_sum_hard_system();
    Momentum p_hard = Momentum::fromP3M2(p_hard3, _event->momentum_sum_hard_system().m2());
    
    // construct the dot product with the hard momentum 
    precision_type dot_iZ = dot_product(p_hard, emission_info.emitter_out)/dot_product(p_hard, rp.emitter);
    // first figure out pB
    Momentum3<precision_type> pB3 = rp.rotn_from_z.transpose()*_event->opposite_beam(emitter());
    Momentum pB = Momentum::fromP3M2(pB3, 0);
    
    precision_type dot_j = dot_product(pB, emission_info.emitter_out)/dot_product(pB, rp.emitter);

    // fill the PDF factors
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = sqrt(dot_j*dot_iZ);   
      emission_info.beam2_pdf_new_x_over_old = sqrt(dot_j/dot_iZ);
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = sqrt(dot_j/dot_iZ);
      emission_info.beam2_pdf_new_x_over_old = sqrt(dot_j*dot_iZ);  
    }
  
    return true;
  }
  
  //----------------------------------------------------------------------
  // do the final boost
  void ShowerPanScaleLocalpp::ElementIF::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {
            
    // retrieve PDF factors
    precision_type xatilde = event().pdf_x(emitter());
    // we also need the xbtilde
    // we can retrieve the initial_state ID of the emitter (=1 or 2)
    // if the initial_state ID is 1, then the particle of the opposite beam is [1]
    // if the initial_state ID is 2, then the particle of the opposite beam is [0]
    // so we can take emitter().initial_state()%2 to select this particle
    Particle opposite_emitter = (*_event)[emitter().initial_state()%2];
    
    precision_type xbtilde = event().pdf_x(opposite_emitter);

    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalpp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    const typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));
    // Set the hard system status of the new radiated particle
    (*_event)[_event->size()-1].set_hard_system(emission_info.radiation_is_hard);

    // construct the new PDF x-fractions
    precision_type x1, x2;
    Momentum current_Q;
    if(emitter().initial_state()==1){
      x1 = xatilde*emission_info.beam1_pdf_new_x_over_old;
      x2 = xbtilde*emission_info.beam2_pdf_new_x_over_old;
      current_Q = emitter()/x1 + opposite_emitter.momentum()/x2;
    }else{
      x1 = xbtilde*emission_info.beam1_pdf_new_x_over_old;
      x2 = xatilde*emission_info.beam2_pdf_new_x_over_old;
      current_Q = emitter()/x2 + opposite_emitter.momentum()/x1;
    }

    // do the boost
    if(!(boost_at_end())) ShowerPanScaleLocalpp::Element::lorentz_boost_realign(current_Q);
    else ShowerPanScaleLocalpp::Element::reset_beam_and_shower_Q(x1, x2, current_Q);
  
  }
  
  //======================================================================
  // ShowerPanScaleLocalpp::ElementFI implementation
  //======================================================================

  double ShowerPanScaleLocalpp::ElementFI::lnb_extent_const() const{
    double xj      = to_double(event().pdf_x(spectator()));
    double xj_over_omxj = xj / (1 - xj);
    double lnxj_over_omxj = log(xj_over_omxj);
    return to_double(_log_dipole_m2 - 2.*_log_rho - lnxj_over_omxj)/(1. + _shower->_beta);
  }
  
  Range ShowerPanScaleLocalpp::ElementFI::lnb_generation_range(double lnv) const{
    // We assume here that alphak and betak are small
    double xj      = to_double(event().pdf_x(spectator()));
    double xj_over_omxj = xj / (1 - xj);
    double lnxj_over_omxj = log(xj_over_omxj); 
    
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv + lnxj_over_omxj) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv                 ) / (1+_shower->_beta));
  }
  
  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleLocalpp::ElementFI::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    //construct alphak, betak which is the same for every element
    ShowerPanScaleLocalpp::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalpp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));

    // impose some constraints so that all the mapping coefficients
    // are well-behaved
    // We want that ak < 1 with ak = alphak 
    if (emission_info.alphak >= 1) return false;
    
    // for a final-state splitting we 
    // relate the z fraction directly to alphak
    precision_type z   = emission_info.alphak;

    // here we should make a decision based on flavour.
    //
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), z,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = z;
    emission_info.z_radiation_wrt_spectator = 0.;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    //
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    normalisation_factor *= g_fcn(emission_info.lnb);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;
  
    // The PDF rescaling factors of the spectator to be updated 
    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    // exact form is for the initial-state parton
    // new_x_over_old =  1.0 + emission_info.alphak/(1.0-emission_info.betak) = 1.0 approximated
    // so both rescaling factors are 1 -> do not need to check which one is initial-state
    emission_info.beam1_pdf_new_x_over_old = 1.0;
    emission_info.beam2_pdf_new_x_over_old = 1.0;

    // in case this FI element will be accepted we do not need to update all elements in the dipole cache
    if(!_shower->boost_at_end()) emission_info.full_store_update_required = false;

    return true;
  }
  
  //----------------------------------------------------------------------
  // carry out the splitting and update the event
  bool ShowerPanScaleLocalpp::ElementFI::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
  
    typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);

    // build the radiated mapping coefficients
    // ak=alphak, bk=betak
    precision_type ak = emission_info.alphak;
    precision_type bk = emission_info.betak;
    precision_type kt = emission_info.kappat;
    
    // Eqs. (B58a-c) in arXiv:2205.02237
    precision_type ai = 1 - ak;
    precision_type bj = 1 + bk/ai;
    precision_type bi = ak * bk / ai;
    //bj = 1 + bk + bi = 1+bk+akbk/(1-ak) = (1-ak +bk - akbk+ ak bk)/(1-ak)=(1-ak+bk)/(1-ak)=1 +bk/(1-ak)=1+bk/ai

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = kt*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    // store for spin, when using dir-diffs this get rotated to the right plane
    emission_info.k_perp = perp;

    // fill the 3-momenta of the emitter, radiated and spectator
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> spectator_out3 =                        bj * rp.spectator.p3();
    
     // Now construct the final 4-momenta by imposing the masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
    if(emitter().is_in_hard_system()) emission_info.radiation_is_hard = true;

    // fill in the exact PDF rescaling
    // now the rescaling is simply given by bj as there is no boost to perform
    if (spectator().initial_state()==1){ // spectator is beam1
      emission_info.beam1_pdf_new_x_over_old = bj;   
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // spectator is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = bj;
    }

    return true;
  }

  //---------------------------------------------------------------------- 
  void ShowerPanScaleLocalpp::ElementFI::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base){
    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    const typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));

    // Set the hard system status of the new radiated particle
    (*_event)[_event->size()-1].set_hard_system(emission_info.radiation_is_hard);
  }

  //======================================================================
  // ShowerPanScaleLocalpp::ElementFF implementation
  //======================================================================
  double ShowerPanScaleLocalpp::ElementFF::lnb_extent_const() const {
    return to_double(_log_dipole_m2 - 2.*_log_rho)/(1. + _shower->_beta);
  }
  
  Range ShowerPanScaleLocalpp::ElementFF::lnb_generation_range(double lnv) const{  
    // this corresponds to Eq. (52 - 53) of 2018-07-notes.pdf (and to the soft limit used in the Sudakov)
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv ) / (1+_shower->_beta));
  }

  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleLocalpp::ElementFF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // construct alphak, betak which is the same for every element
    ShowerPanScaleLocalpp::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalpp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));
    
    // test phase space boundary (Eq. (42) of 2018-07-notes.pdf)
    if (emission_info.alphak + emission_info.betak >= 1.0) {return 0;}
    
    precision_type z   = emission_info.alphak;
    
    // here we should make a decision based on flavour.
    // pg (pq) corresponds to the emission of a gluon (quark)
    _shower->fill_dglap_splitting_weights(emitter(), z,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark); 

    // pdf x fractions do not change
    emission_info.beam1_pdf_new_x_over_old = 1.0;
    emission_info.beam2_pdf_new_x_over_old = 1.0;

    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = z;
    emission_info.z_radiation_wrt_spectator = 0.;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    normalisation_factor *= g_fcn(emission_info.lnb);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;

    // in case this FF element will be accepted we do not need to update all elements in the dipole cache
    if(!_shower->boost_at_end()) emission_info.full_store_update_required = false;

    return true;
  }
  
  //----------------------------------------------------------------------
  // carry out the splitting and update the event
  bool ShowerPanScaleLocalpp::ElementFF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalpp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));
    
    const double & phi = emission_info.phi;
    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);

    // build the radiated mapping coefficients
    const precision_type & ak = emission_info.alphak;
    const precision_type & bk = emission_info.betak;
    const precision_type & kt = emission_info.kappat;

    // Eqs. (B60a-c) in arXiv:2205.02237
    precision_type ai = 1.-ak;
    precision_type bi = ak * bk / ai;
    precision_type bj = 1. - bk / ai; 
    
    // for cases with a plane, phi=0 means in-plane
    Momentum perp = kt * (sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    // store for spin, when using dir-diffs this get rotated to the right plane
    emission_info.k_perp = perp;
    // build the momenta
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> spectator_out3 =                        bj * rp.spectator.p3();

    // Now construct the final 4-momenta by imposing the masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
    
    if(emitter().is_in_hard_system()) emission_info.radiation_is_hard = true;
    // Set the hard system status of the new radiated particle
    (*_event)[_event->size()-1].set_hard_system(emission_info.radiation_is_hard);
    
    return true;
  }

  //----------------------------------------------------------------------
  void ShowerPanScaleLocalpp::ElementFF::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base){
    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalpp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    const typename ShowerPanScaleLocalpp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalpp::EmissionInfo*>(emission_info_base));
    // Set the hard system status of the new radiated particle
    (*_event)[_event->size()-1].set_hard_system(emission_info.radiation_is_hard);
  }  
  
} // namespace panscales
#include "autogen/auto_ShowerPanScaleLocalpp_global-cc.hh"
