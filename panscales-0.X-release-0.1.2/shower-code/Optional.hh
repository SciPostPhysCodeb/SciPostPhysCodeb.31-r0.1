//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef __OPTIONAL_HH__
#define __OPTIONAL_HH__

#include <utility>
#include <stdexcept>

namespace panscales{


struct NullOpt_t
{
  explicit NullOpt_t() = default;
};
constexpr NullOpt_t NullOpt{};

/// a light-weight stand-in for some of the funtionality of C++17
/// optional, with the difference that it uses <bool,T> rather than
/// <bool,T*> storage and so does not need any memory allocation; that
/// makes it ill-advised for large complex types
///
/// note that operator bool() is _not_ implemented, to reduce the
/// likelihood of mis-using the optional bool in place of the value
template <class T>
class Optional {
public:
  using value_type = T;

  Optional() {}
  Optional(const T & value_in) : _has_value(true), _value(value_in) {}
  Optional(T && value_in) : _has_value(true), _value(std::move(value_in)) {}

  Optional & operator=(NullOpt_t) noexcept {
    reset();
    return *this;
  }

  Optional & operator=(const T & value_in) noexcept {
    _has_value = true;
    _value = value_in;
    return *this;
  }
  
  Optional & operator=(T && value_in) noexcept {
    _has_value = true;
    _value = std::move(value_in);
    return *this;
  }
  
  bool has_value() const noexcept {return _has_value;}


  /// @name access operators  
  ///@{

  T& value() {
    if (!_has_value) throw std::runtime_error("Optional has no value");
    return _value;
  }
  const T & value() const {
    if (!_has_value) throw std::runtime_error("Optional has no value");
    return _value;
  }

  T & operator*() {
    if (!_has_value) throw std::runtime_error("Optional has no value");
    return _value;
  }
  const T & operator*() const {
    if (!_has_value) throw std::runtime_error("Optional has no value");
    return _value;
  }

  T & operator->() {
    if (!_has_value) throw std::runtime_error("Optional has no value");
    return &_value;
  }
  const T & operator->() const {
    if (!_has_value) throw std::runtime_error("Optional has no value");
    return &_value;
  }

  inline const T & value_or(const T & alternative) const {
    if (has_value()) return _value;
    else             return alternative;
  }

  ///@}

  void reset() {_has_value = false;}

private:

  bool _has_value = false;
  T _value;
};


/// @brief a specialization of Optional that is convertible to T
/// @tparam T 
template <class T> 
class ConvertibleOptional : public Optional<T> {

public:

  ConvertibleOptional() {}
  ConvertibleOptional(const T & value_in) : Optional<T>(value_in) {}
  ConvertibleOptional(T && value_in)      : Optional<T>(value_in) {}

  operator T() const {return this->value();}

};


} // namespace panscales


#endif // __OPTIONAL_HH__
