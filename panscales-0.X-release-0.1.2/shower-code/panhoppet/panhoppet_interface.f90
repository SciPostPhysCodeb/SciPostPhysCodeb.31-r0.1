module panscales_hoppet_interface
  ! for info on iso_c_binding, see
  ! - https://www.nag.com/nagware/np/r70_doc/iso_c_binding.html
  ! - http://fortranwiki.org/fortran/show/iso_c_binding
  ! - https://www.ibm.com/docs/en/xcafbg/9.0.0?topic=SS3KZ4_9.0.0/com.ibm.xlf111.bg.doc/xlflr/interop-iso-c-binding-module.htm
  ! - http://geodesy.unr.edu/hanspeterplag/library/it/fortran_language/john_reid_new_2003.pdf
  use, intrinsic :: iso_c_binding
  use types; use consts_dp
  use pdf_tabulate
  use convolution; use pdf_general; use dglap_objects
  use dglap_holders; use pdf_general; use dglap_choices
  use qcd_coupling
  use qcd, only: quark_masses_def
  implicit none

  !! holds information about the grid
  type(grid_def),     save :: grid, gdarray(4)

  !! holds the splitting functions
  type(dglap_holder), save :: dh

  !! table_index_from_iloop tells us which table to loop up for a give
  !! iloop value; most of the iloop values are illegal (index -1), but
  !! for those that are allowed (e.g. 1, 2, 3, 12, 111) this gives
  !! quick access to the relevant table (without having to store 111
  !! tables, most of which would be redundant)
  !!
  !! The array gets initialised in panhoppetStartExtended
  integer, save :: table_index_from_iloop(1:111) = -1
  integer, parameter :: max_table_index = 7

  !!
  !! NB (2012-12-24): 
  !!     in future evolution of the code, we should aim to guarantee
  !!     that tables(0) remains the default PDF, even if the other
  !!     entries change
  type(pdf_table), save :: tables(0:max_table_index)
  logical,         save :: setup_done(0:max_table_index) = .false.
  integer,         save :: setup_nf(max_table_index)     = 0
  logical,         save :: alloc_already_done = .false.
  
  !! coupling
  logical,                save :: coupling_initialised = .false.
  type(running_coupling), save :: coupling
  integer,  save :: ffn_nf = -1
  logical,  save :: quark_masses_are_MSbar = .false.
  real(dp), save :: masses(4:6) = quark_masses_def(4:6)


  !! type to hold pdf_origins
  type pdf_origins
    real(dp) :: Qorigin, Qfinal
    real(dp), pointer :: pdf_Qorigin(:,:) => null()
    real(dp), pointer :: pdf_Qfinal (:,:) => null()
    !! indices are (grid, iflv_final, iflv_initial)
    real(dp), pointer :: delta_fn_fates(:,:,:) => null()
  end type pdf_origins
  type(pdf_origins) :: cached_origins

contains

  !----------------------------------------------------------------------
  ! returns the value of the table index for the given iloop and
  ! ensures that the table is actually set up (working down recursively
  ! when iloop is composite)
  recursive integer function tableIndexValue(iloop, nf) result(tabindex)
    use warnings_and_errors
    integer, intent(in) :: iloop, nf
    !------------------------------
    integer :: table_index_source, iloop_P, iQ

    ! first establish if iloop is "legal"
    if (iloop < 1 .or. iloop > ubound(table_index_from_iloop,dim=1)) then
       call wae_error('tableIndexValue','illegal value for iloop (out of bounds):',&
            &intval=iloop)
    else if (table_index_from_iloop(iloop) < 0) then
       call wae_error('tableIndexValue','illegal value for iloop (lookup < 0):',&
            &intval=iloop)
    end if
    tabindex = table_index_from_iloop(iloop)

    !write(0,*) 'iloop, tabindex ', iloop, tabindex
    if (.not. setup_done(tabindex) .or. setup_nf(tabindex) /= nf) then

       if (iloop < 10) then
          table_index_source = 0
          iloop_P = iloop
       else if (iloop < 100) then
          table_index_source = tableIndexValue(mod(iloop,10), nf)
          iloop_P = iloop / 10
       else if (iloop < 1000) then
          table_index_source = tableIndexValue(mod(iloop,100), nf)
          iloop_P = iloop / 100
       else 
          call wae_error('tableIndexValue','unsupported value for iloop (source & iloop_P determination):',&
            &intval=iloop)
       end if
       !write(0,*) 'setting up with source and iloop_P = ', table_index_source, iloop_P
       
       if (nf < 0) then
          ! use whatever nf is relevant 
          if (.not. tables(0)%nf_info_associated) call wae_error(&
               & 'hoppetEvalSplit','automatic choice of nf (nf<0) but the tabulation has no information on nf')
          do iQ = 0, tables(0)%nQ
             tables(tabindex)%tab(:,:,iQ) = dh%allP(iloop_P, tables(0)%nf_int(iQ)) &
                  &             .conv. tables(table_index_source)%tab(:,:,iQ)
          end do
       else
          ! use a fixed nf
          if (nf < lbound(dh%allP,dim=2) .or. nf > ubound(dh%allP,dim=2)) &
               &call wae_error('hoppetEvalSplit','illegal value for nf:',&
               &intval=nf)
          
          tables(tabindex)%tab = dh%allP(iloop_P, nf) .conv. tables(table_index_source)%tab
       end if
       
       setup_done(tabindex) = .true.
       setup_nf(tabindex)   = nf
  end if

  end function tableIndexValue

  !! ! testing
  !! subroutine panhoppetSetupPlainYGrid(ymax, dy) bind(c, name='panhoppetSetupPlainYGrid')
  !!   real(kind=c_double), intent(in) :: ymax, dy
  !!   integer, parameter :: order = -6
  !!   call InitGridDef(grid,dy,ymax,order)
  !! end subroutine
  !! subroutine panhoppetSetupNestedYGrid(ymax, dy) bind(c, name='panhoppetSetupNestedYGrid')
  !!   real(kind=c_double), intent(in) :: ymax, dy
  !!   integer, parameter :: order = -6
  !!   call InitGridDef(gdarray(4),dy/27.0_dp,0.2_dp, order=order)
  !!   call InitGridDef(gdarray(3),dy/9.0_dp,0.5_dp, order=order)
  !!   call InitGridDef(gdarray(2),dy/3.0_dp,2.0_dp, order=order)
  !!   call InitGridDef(gdarray(1),dy,       ymax  ,order=order)
  !!   call InitGridDef(grid,gdarray(1:4),locked=.true.)
  !! end subroutine

  !-----------------------------------------------------------------------
  !! Sets up the cached PDF origins with the specified Qorigin and Qfinal
  !! values, based on the existing table, coupling, etc. setups
  subroutine panhoppetInitOrigins(Qorigin, Qfinal) bind(c,name='panhoppetInitOrigins')
    use hoppet_v1
    use warnings_and_errors
    implicit none
    real(dp), intent(in) :: Qorigin, Qfinal
    !---------------------------------------
    type(evln_operator) :: evop
    real(dp) :: delta_fns(0:grid%ny), pdf_tmp(0:grid%ny, ncompmin:ncompmax)
    logical  :: bad_grid
    integer  :: isub, iflv
    integer  :: iy

    ! check that the grid has only negative orders
    if (grid%nsub == 0) then
      bad_grid = (grid%order >= 0)    
    else  
      bad_grid = maxval(grid%subgd%order) >= 0
    end if 
    if (bad_grid) then
      call wae_error("The grid or one of its sub-grids has a non-negative order")
    end if

    ! avoid memory leaks
    if (associated(cached_origins%pdf_Qorigin)) then
      deallocate(cached_origins%pdf_Qorigin)
      deallocate(cached_origins%pdf_Qfinal)
      deallocate(cached_origins%delta_fn_fates)
    end if

    ! allocate and evaluate the PDFs at the two scales
    call AllocPDF(grid,cached_origins%pdf_Qfinal)
    call AllocPDF(grid,cached_origins%pdf_Qorigin)
    call EvalPdfTable_Q(tables(0), Qfinal , cached_origins%pdf_Qfinal )
    call EvalPdfTable_Q(tables(0), Qorigin, cached_origins%pdf_Qorigin)

    ! get an evolution operator between our two Q values
    call EvolveGeneric(dh, coupling, Qorigin, Qfinal, evop=evop)

    ! allocate and fill the delta_function_fates object
    ! (in principle we could make do with just the evop (which is
    ! considerably more compact), but flavour handling is a little bit
    ! more transparent if we skip it -- maybe come back to this later)
    call AllocPDF(grid, cached_origins%delta_fn_fates, iflv_min, iflv_max)

    ! create delta fns, which depend on whether we have a single or a nested grid
    delta_fns = zero
    if (grid%nsub == 0) then
      delta_fns(0) = one / grid%dy
    else
      ! at each of the sub-grid starting points (recall that grid%subiy is an array)
      ! insert a delta function corresponding to 1/dy of that grid
      delta_fns(grid%subiy(:)) = one / grid%subgd(:)%dy
    end if

    ! and finally determine the impact of the delta functions
    call DisableGridLocking()
    do iflv = iflv_min, iflv_max
      pdf_tmp = zero
      pdf_tmp(:,iflv) = delta_fns
      cached_origins%delta_fn_fates(:,:,iflv) = evop .conv. pdf_tmp
    end do
    call RestoreGridLocking()

    ! testing
    ! do isub = 1, grid%nsub
    !   do iy = 0, grid%subgd(isub)%ny
    !     !write(6,*) iy * grid%subgd(isub)%dy, cached_origins%delta_fn_fates(iy+grid%subiy(isub), 1,1)
    !     !write(6,*) iy * grid%subgd(isub)%dy, evop%P%NS_V%subgc(isub)%conv(iy,1)
    !     write(6,*) iy * grid%subgd(isub)%dy, dh%P_LO%NS_V%subgc(isub)%conv(iy,1)
    !   end do
    !   write(6,*)
    !   write(6,*)      
    ! end do

    ! final cleanup
    call Delete(evop)
  end subroutine panhoppetInitOrigins
  

  !-----------------------------------------------------------------------
  !! Given a prior call to panhoppetInitOrigins (which sets Q_origin and
  !! Q_final and caches relevant info), this returns the fractional
  !! contribution to pdf_{iflv_final}(x_final,Q_final) that comes from
  !! flavour iflv_origin, at x_origin,Q_origin.
  !!
  !! Labelling the return as F, the normalisation is such that 
  !!
  !!   \sum_{iflv_origin} \int dx/x F_{iflv_origin}(x) = 1   (*)
  !! 
  !! The evaluation is relatively cheap (equivalent to three independent x 
  !! evaluations). 
  !!
  !! Note that for x_origin close to x_final, the results are very sensitive
  !! to the underlying structure of a discretised delta function representation
  !! and so become inaccurate (with standard values of |order|~5 this is visible
  !! as oscillations of the result).
  !!
  !! Because of the problems for x_origin close to x_final, if you wish to test the 
  !! integral in Eq.(*), it is wisest to use a non-nested grid and to perform
  !! the integral as a sum over grid points, multiplied by the grid spacing.
  !! Any other approach is likely to subject to nasty grid-related artefacts.
  !!
  !! This function and panhoppetInitOrigins are valid only for negative values 
  !! of the grid order.
  !!
  function panhoppetGetOrigins(iflv_origin,x_origin,iflv_final,x_final) result(retval) &
    bind(c,name='panhoppetGetOrigins')
    implicit none
    integer,  intent(in) :: iflv_origin
    real(dp), intent(in) :: x_origin
    integer,  intent(in) :: iflv_final
    real(dp), intent(in) :: x_final    
    real(dp) :: retval
    !------------------------------------

    if (x_origin < x_final) then
      retval = zero
      return
    end if
    
    retval = (cached_origins%delta_fn_fates(:,iflv_final, iflv_origin).atx.(x_final/x_origin.with.grid)) &
              * (cached_origins%pdf_Qorigin(:,iflv_origin).atx.(x_origin.with.grid)) &
              / (cached_origins%pdf_Qfinal (:,iflv_final ).atx.(x_final .with.grid)) 
  end function panhoppetGetOrigins


  !------------------------------------------------------------------------------
  !! Return 
  !!
  !!  [P(iloop,nf)_{G,XX} \otimes pdf_{XX}] (x,Q) [including the standard leading factor of x]
  !! 
  !! where XX=0 means the gluon and XX=1 means the singlet.
  !!
  !! NB: this routine does not currently cache any results and is
  !!     quite inefficient
  function panhoppetEvalGXSplit(x,Q,iloop,nf_in,xx) result(res) bind(c,name='panhoppetEvalGXSplit')
    use pdf_representation
    implicit none
    real(dp), intent(in)  :: x, Q
    integer,  intent(in)  :: iloop, nf_in, xx
    real(dp)              :: res
    !-----------------------------------
    real(dp) :: pdf_at_Q(0:grid%ny,ncompmin:ncompmax)
    real(dp) :: conv_result(0:grid%ny)

    call EvalPdfTable_Q(tables(0), Q, pdf_at_Q)

    if (xx == 0) then
      conv_result(:) = dh%allP(iloop, nf_in)%gg * pdf_at_Q(:,iflv_g)
    else if (xx == 1) then
      conv_result(:) = dh%allP(iloop, nf_in)%gq * &
             (sum(pdf_at_Q(:,-6:-1),dim=2)+sum(pdf_at_Q(:,1:6),dim=2))
    else
      call wae_error('panhoppetEvalGXSplit','illegal value for xx (only 0 or 1 are allowed):',&
                     intval=xx)
    end if 

    res = conv_result .atx. (x.with.grid)
    
  end function panhoppetEvalGXSplit
  
end module panscales_hoppet_interface


!======================================================================
!! set up the colour group (to be done before calling any of the 
!! Start routines)
subroutine panhoppetSetGroup(ca_in, cf_in, tr_in, nf_in)
  use panscales_hoppet_interface
  use qcd
  implicit none
  !--------------------------------------
  real(dp), intent(in) :: ca_in, cf_in, tr_in 
  integer, intent(in)  :: nf_in

  call qcd_SetGroup(ca_in, cf_in, tr_in)
  call qcd_SetNf(nf_in)
end subroutine panhoppetSetGroup


!======================================================================
!! return the b0 = (11CA - 4 TR nf)(12pi) coefficient as used by hoppet
function panhoppetB0() bind(c,name='panhoppetB0')
  use qcd
  implicit none
  real(dp) :: panhoppetB0
  panhoppetB0 = beta0
end function

!======================================================================
!! return the b1 = (17CA^2 - 10 CA nF TR - 6 CF nf TR) / (24 Pi^2) coefficient as used by hoppet
function panhoppetB1() bind(c,name='panhoppetB1')
  use qcd
  implicit none
  real(dp) :: panhoppetB1
  panhoppetB1 = beta1
end function

!======================================================================
!! initialise the underlying grid, splitting functions and pdf-table
!! objects, using the dy and nloop parameters as explained below.
subroutine panhoppetStart(dy,nloop)
  use panscales_hoppet_interface
  implicit none
  !--------------------------------------
  real(dp), intent(in) :: dy     !! internal grid spacing: 0.1 is a sensible value
  integer,  intent(in) :: nloop  !! the maximum number of loops we'll want (<=3)
  !--------------------------------------
  real(dp) :: ymax, Qmin, Qmax, dlnlnQ
  integer  :: order
  ymax = 12.0d0
  Qmin = 1.0d0
  Qmax = 28000d0 ! twice LHC c.o.m.
  dlnlnQ = dy/4.0_dp  ! min(0.5_dp*dy,0.07_dp)
  order = -6
  call panhoppetStartExtended(ymax,dy,Qmin,Qmax,dlnlnQ,nloop,order,factscheme_MSbar,1)
end subroutine panhoppetStart




!======================================================================
!! initialise the underlying grid, splitting functions and pdf-table
!! objects, using an extended set of parameters, as described below
subroutine panhoppetStartExtended(ymax,dy,Qmin,Qmax,dlnlnQ,nloop,order,factscheme,nested) 
  use panscales_hoppet_interface
  use qcd
  use, intrinsic :: iso_c_binding
  implicit none
  real(dp), intent(in) :: ymax   !! highest value of ln1/x user wants to access
  real(dp), intent(in) :: dy     !! internal grid spacing: 0.1 is a sensible value
  real(dp), intent(in) :: Qmin, Qmax !! range in Q
  real(dp), intent(in) :: dlnlnQ !! internal table spacing in lnlnQ
  integer,  intent(in) :: nloop  !! the maximum number of loops we'll want (<=3)
  integer,  intent(in) :: order  !! order of numerical interpolation (+ve v. -ve: see below)
  integer,  intent(in) :: factscheme !! 1=unpol-MSbar, 2=unpol-DIS, 3=Pol-MSbar
  integer,  intent(in) :: nested !! if true, use nested y grids (for normal usage should be .true.)
  !-------------------------------------
  integer :: iloop
  ! initialise our grids

  ! the internal interpolation order (with a minus sign allows
  ! interpolation to take fake zero points beyond x=1 -- convolution
  ! times are unchanged, initialisation time is much reduced and
  ! accuracy is slightly reduced)
  !order = -5 
  ! Now create a nested grid
  if (nested /= 0) then
    call InitGridDef(gdarray(4),dy/27.0_dp,0.2_dp, order=order)
    call InitGridDef(gdarray(3),dy/9.0_dp,0.5_dp, order=order)
    call InitGridDef(gdarray(2),dy/3.0_dp,2.0_dp, order=order)
    call InitGridDef(gdarray(1),dy,       ymax  ,order=order)
    call InitGridDef(grid,gdarray(1:4),locked=.true.)
  else
    call InitGridDef(grid,dy,ymax,order=order)
  end if
  ! Fill the array that will be used for table index lookup (e.g. 21 is PNLO*PLO).
  ! For now do it by hand; one day we might automate this;
  ! entries that aren't filled are automatically -1
  do iloop = 1, nloop
     table_index_from_iloop(iloop) = iloop
  end do
  table_index_from_iloop(11)  = 4
  table_index_from_iloop(111) = 5
  if (nloop >= 2) table_index_from_iloop(12)  = 6
  if (nloop >= 2) table_index_from_iloop(21)  = 7

  ! if the allocation has already been done previously, delete
  ! the existing tables and dglap holder to avoid a memory leak
  if (alloc_already_done) then
     call Delete(tables)
     call Delete(dh)
  end if


  ! create the tables that will contain our copy of the user's pdf
  ! as well as the convolutions with the pdf.
  call AllocPdfTable(grid, tables(:), Qmin, Qmax, & 
       & dlnlnQ = dlnlnQ, freeze_at_Qmin=.true.)

  ! initialise splitting-function holder
  call InitDglapHolder(grid,dh,factscheme=factscheme,&
       &                      nloop=nloop,nflo=3,nfhi=6)
  ! choose a sensible default number of flavours.
  call SetNfDglapHolder(dh,nflcl=5)
  write(6,*) "# Hoppet initialised with CA =", CA, ", CF =", CF, ", nf =", nf, ", nloop=",nloop
  if (grid%nsub==0) then 
    write(6,*) "# Hoppet has a single grid with dy=", grid%dy
  else
    write(6,*) "# Hoppet has nested grids with coarse dy=", maxval(grid%subgd(:)%dy)
  end if 
  ! indicate that allocations have already been done,
  ! to allow for cleanup if panhoppetStartExtended is
  ! called a second time
  alloc_already_done = .true.

  ! indicate the pdfs and convolutions have not been initialised...
  setup_done = .false.
end subroutine panhoppetStartExtended


!======================================================================
!! Given a pdf_subroutine with the interface shown below, initialise
!! our internal pdf table.
subroutine panhoppetAssign(pdf_subroutine)
  use panscales_hoppet_interface ! this module which provides access to the array of tables
  implicit none
  interface ! indicate what "interface" pdf_subroutine is expected to have
     subroutine pdf_subroutine(x,Q,res)
       use types; implicit none
       real(dp), intent(in)  :: x,Q
       real(dp), intent(out) :: res(*)
     end subroutine pdf_subroutine
  end interface
  !-----------------------------------

  ! set up table(0) by copying the values returned by pdf_subroutine onto 
  ! the x-Q grid in table(0)
  call FillPdfTable_LHAPDF(tables(0), pdf_subroutine)
  ! indicate that table(0) has been set up
  setup_done(0)  = .true.
  ! indicate that table(1), table(2), etc... (which will contain the
  ! convolutions with splitting matrices) have yet to be set up [they
  ! get set up "on demand" later].
  setup_done(1:) = .false.
end subroutine panhoppetAssign


!======================================================================
!! Given a pdf_subroutine with the interface shown below, fill the 
!! table by evolving the PDF from scale Q0pdf, with alphas provided 
!! at scale Q0alphas
subroutine panhoppetEvolve(asQ0, Q0alphas, nloop, muR_Q, pdf_subroutine, Q0pdf)
  use panscales_hoppet_interface ! this module which provides access to the array of tables implicit none
  implicit none
  real(dp), intent(in) :: asQ0, Q0alphas, muR_Q, Q0pdf
  integer,  intent(in) :: nloop
  interface ! indicate what "interface" pdf_subroutine is expected to have
     subroutine pdf_subroutine(x,Q,res)
       use types; implicit none
       real(dp), intent(in)  :: x,Q
       real(dp), intent(out) :: res(*)
     end subroutine pdf_subroutine
  end interface
  !! hold the initial pdf
  real(dp), pointer :: pdf0(:,:)
  !! for testing momentum sum rules
  ! integer i
  ! real(dp) :: momsum, momsum_all

  ! create our internal pdf object for the initial condition
  call AllocPDF(grid, pdf0)
  call InitPDF_LHAPDF(grid, pdf0, pdf_subroutine, Q0pdf)

  ! get a running coupling with the desired scale
  if (coupling_initialised) call Delete(coupling) 
  if (ffn_nf > 0) then
     call InitRunningCoupling(coupling, alfas=asQ0, Q=Q0alphas, nloop=nloop, &
          &                   fixnf=ffn_nf)
  else 
     call InitRunningCoupling(coupling, alfas=asQ0, Q=Q0alphas, nloop=nloop, &
          &                   quark_masses=masses, &
          &                   masses_are_MSbar = quark_masses_are_MSbar)
  end if
  call AddNfInfoToPdfTable(tables,coupling)
  coupling_initialised = .true.

  ! create the tabulation
  call EvolvePdfTable(tables(0), Q0pdf, pdf0, dh, muR_Q=muR_Q, &
       &              coupling=coupling, nloop=nloop)

  ! indicate that table(0) has been set up
  setup_done(0)  = .true.
  ! indicate that table(1), table(2), etc... (which will contain the
  ! convolutions with splitting matrices) have yet to be set up [they
  ! get set up "on demand" later].
  setup_done(1:) = .false.

  !! for testing momentum sum rules
  ! call EvalPdfTable_Q(tables(0), 100.0_dp, pdf0)
  ! momsum_all = zero
  ! do i = -6, 6
  !   momsum = TruncatedMoment(grid, pdf0(:,i), one)
  !   write(6,*) "Momentum in flavour ", i, " = ", momsum
  !   momsum_all = momsum_all + momsum
  ! end do
  ! write(6,*) "Total momentum = ", momsum_all

  ! clean up
  call Delete(pdf0)
end subroutine panhoppetEvolve


!======================================================================
!! Prepare a cached evolution
subroutine panhoppetPreEvolve(asQ0, Q0alphas, nloop, muR_Q, Q0pdf)
  use panscales_hoppet_interface
  implicit none
  real(dp), intent(in) :: asQ0, Q0alphas, muR_Q, Q0pdf
  integer,  intent(in) :: nloop

  ! get a running coupling with the desired scale
  if (coupling_initialised) call Delete(coupling) 
  if (ffn_nf > 0) then
     call InitRunningCoupling(coupling, alfas=asQ0, Q=Q0alphas, nloop=nloop, &
          &                   fixnf=ffn_nf)
  else 
     call InitRunningCoupling(coupling, alfas=asQ0, Q=Q0alphas, nloop=nloop, &
          &                   quark_masses=masses)
  end if
  call AddNfInfoToPdfTable(tables,coupling)
  coupling_initialised = .true.

  ! create the tabulation
  call PreEvolvePdfTable(tables(0), Q0pdf, dh, muR_Q=muR_Q, &
       &                 coupling=coupling, nloop=nloop)
end subroutine panhoppetPreEvolve


!======================================================================
!! Carry out a cached evolution based on the initial condition
!! that can be obtained from pdf_subroutine at the scale Q0pdf set in
!! PreEvolve
subroutine panhoppetCachedEvolve(pdf_subroutine)
  use panscales_hoppet_interface
  implicit none
  interface ! indicate what "interface" pdf_subroutine is expected to have
     subroutine pdf_subroutine(x,Q,res)
       use types; implicit none
       real(dp), intent(in)  :: x,Q
       real(dp), intent(out) :: res(*)
     end subroutine pdf_subroutine
  end interface
  !! hold the initial pdf
  real(dp), pointer :: pdf0(:,:)

  ! create our internal pdf object for the initial condition
  call AllocPDF(grid, pdf0)
  call InitPDF_LHAPDF(grid, pdf0, pdf_subroutine, tables(0)%StartScale)

  ! create the tabulation
  call EvolvePdfTable(tables(0), pdf0)

  ! indicate that table(0) has been set up
  setup_done(0)  = .true.
  ! indicate that table(1), table(2), etc... (which will contain the
  ! convolutions with splitting matrices) have yet to be set up [they
  ! get set up "on demand" later].
  setup_done(1:) = .false.

  ! clean up
  call Delete(pdf0)
end subroutine panhoppetCachedEvolve


!======================================================================
!! Return the coupling at scale Q
function panhoppetAlphaS(Q)
  use panscales_hoppet_interface ! this module which provides access to the array of tables
  use warnings_and_errors
  implicit none
  real(dp)             :: panhoppetAlphaS
  real(dp), intent(in) :: Q

  if (.not. coupling_initialised) call wae_error('hoppetAlphaS',&
       &'coupling is not yet initialised (and will remain so until',&
       &'first call to an evolution routine).')
  panhoppetAlphaS = Value(coupling, Q)
end function panhoppetAlphaS


!======================================================================
!! Set up things to be a fixed-flavour number scheme with the given
!! fixed_nf number of flavours
subroutine panhoppetSetFFN(fixed_nf)
  use panscales_hoppet_interface ! this module which provides access to the array of tables
  implicit none
  integer,  intent(in) :: fixed_nf

  ffn_nf = fixed_nf
end subroutine panhoppetSetFFN


!======================================================================
!! Set up things to be a variable-flavour number scheme with the given
!! quark masses in the pole mass scheme.
!!
!! This interface retained for legacy purposes. Preferred interface is
!! via panhoppetSetPoleMassVFN(mc,mb,mt)
subroutine panhoppetSetVFN(mc,mb,mt)
  use panscales_hoppet_interface ! this module which provides access to the array of tables
  implicit none
  real(dp) :: mc, mb, mt
  call panhoppetSetPoleMassVFN(mc,mb,mt)
end subroutine panhoppetSetVFN


!======================================================================
!! Set up things to be a variable-flavour number scheme with the given
!! quark masses in the pole mass scheme. Thresholds are crossed at the
!! pole masses, both for the coupling and the PDF evolution.
subroutine panhoppetSetPoleMassVFN(mc,mb,mt)
  use panscales_hoppet_interface ! this module which provides access to the array of tables
  implicit none
  real(dp) :: mc, mb, mt

  ffn_nf = -1
  masses = (/mc,mb,mt/)
  quark_masses_are_MSbar = .false.
end subroutine panhoppetSetPoleMassVFN


!======================================================================
!! Set up things to be a variable-flavour number scheme with the given
!! quark masses in the MSbar mass scheme, i.e. m(m). Thresholds are
!! crossed at the MSbar masses, both for the coupling and the PDF
!! evolution.
subroutine panhoppetSetMSbarMassVFN(mc,mb,mt)
  use panscales_hoppet_interface ! this module which provides access to the array of tables
  implicit none
  real(dp) :: mc, mb, mt

  ffn_nf = -1
  masses = (/mc,mb,mt/)
  quark_masses_are_MSbar = .true.
end subroutine panhoppetSetMSbarMassVFN


!======================================================================
!! Return in f(-6:6) the value of the internally stored pdf at the
!! given x,Q, with the usual LHApdf meanings for the indices -6:6.
subroutine panhoppetEval(x,Q,f)
  use panscales_hoppet_interface
  implicit none
  real(dp), intent(in)  :: x, Q
  real(dp), intent(out) :: f(-6:6)
  
  call EvalPdfTable_xQ(tables(0),x,Q,f)
end subroutine panhoppetEval

!======================================================================
!! Return the value of the internally stored pdf at the
!! given x,Q for flavour iflv
function panhoppetEvalFlav(x,Q,iflv) result(res)
  use panscales_hoppet_interface
  implicit none
  real(dp), intent(in) :: x, Q
  integer,  intent(in) :: iflv
  real(dp) :: res
  res = EvalPdfTable_xQf(tables(0),x,Q,iflv)
end function panhoppetEvalFlav


!======================================================================
!! Return in f(-6:6) the value of 
!!
!!    [P(iloop,nf) \otimes pdf] (x,Q)
!!
!! where P(iloop,nf) is the iloop-splitting function for the given
!! value of nf, and pdf is our internally stored pdf.
!!
!! The normalisation is such that the nloop dglap evolution equation is
!!
!!     dpdf/dlnQ^2 = sum_{iloop=1}^nloop 
!!                        (alphas/(2*pi))^iloop * P(iloop,nf) \otimes pdf
!!
!! Note that each time nf changes relative to a previous call for the
!! same iloop, the convolution has to be repeated for the whole
!! table. So for efficient results when requiring multiple nf values,
!! calls with the same nf value should be grouped together.
!!
!! In particular, for repeated calls with the same value of nf, the
!! convolutions are carried out only on the first call (i.e. once for
!! each value of iloop). Multiple calls with different values for
!! iloop can be carried out without problems.
!!
!! Note that iloop can also be of the form ij or ijk, which means
!! P(i)*P(j)*pdf or P(i)*P(j)*P(k)*pdf. The sum of i+j+k is currently
!! bounded to be <= 3.
!!
!! The number of loops must be consistent with iloop
subroutine panhoppetEvalSplit(x,Q,iloop,nf,f)
  use panscales_hoppet_interface; use warnings_and_errors
  implicit none
  real(dp), intent(in)  :: x, Q
  integer,  intent(in)  :: iloop, nf
  real(dp), intent(out) :: f(-6:6)
  integer :: iQ, tabindex

  tabindex = tableIndexValue(iloop, nf)
  call EvalPdfTable_xQ(tables(tabindex),x,Q,f)

end subroutine panhoppetEvalSplit


!======================================================================
!! work-in-progress function to work out PDF origin
subroutine panhoppetOrigin(iflav,iy,Qfinal,Qorigin) bind(c,name='panhoppetOrigin')
  use panscales_hoppet_interface
  use hoppet_v1
  implicit none
  integer,  intent(in) :: iy, iflav
  real(dp), intent(in) :: Qfinal, Qorigin
  !---------------------------------------
  type(evln_operator) :: evop
  real(dp), pointer   :: pdf_final(:,:), pdf_origin(:,:), pdf_tmp(:,:), pdf_result(:,:)
  ! make this a constant for now
  integer, parameter  :: nloop = 1
  integer iy_orig, iflv_orig
  real(dp) :: validation_sum
  integer :: loc(2)
  logical :: bad_grid
  ! testing c-fortran interplay
  type(c_ptr) :: some_result
  type xxxres
    type(grid_def) :: grid
    real(dp), pointer :: xxx
  end type xxxres
  type(xxxres), pointer :: xxxresult

  ! get an evolution operator between our two Q values
  call EvolveGeneric(dh, coupling, Qorigin, Qfinal, evop=evop, nloop=nloop)

  if (associated(evop%next)) then
    write(0,*) "evop%next should not be associated"
    stop
  end if

  ! check that the grid has only negative orders
  if (grid%nsub == 0) then
    bad_grid = (grid%order >= 0)    
  else  
    bad_grid = maxval(grid%subgd%order) >= 0
  end if 
  if (bad_grid) then
    write(0,*) "The grid or one of its sub-grids has a non-negative order"
    stop
  end if

  call AllocPDF(grid,pdf_final)
  call AllocPDF(grid,pdf_origin)
  call AllocPDF(grid,pdf_tmp)
  call AllocPDF(grid,pdf_result)

  ! get the PDFs at the two scales
  call EvalPdfTable_Q(tables(0), Qfinal , pdf_final )
  call EvalPdfTable_Q(tables(0), Qorigin, pdf_origin)


  ! first check that we get the right by evolving up
  pdf_tmp = evop%P .conv. pdf_origin
  write(6,*) "PDF at Qfinal = ", Qfinal
  call print_pdf(pdf_final)
  write(6,*) "PDF at Qorigin evolved up to Qfinal - (PDF at Qfinal) max deviation (up to iy)"
  pdf_tmp = pdf_tmp - pdf_final
  loc = maxloc(abs(pdf_tmp(:iy,:))) + (/ -1, -7/) ! remember maxloc starts at 1,1
  write(6,*) pdf_tmp(loc(1),loc(2)), 'v', pdf_final(loc(1),loc(2)),' at ', loc
  
  pdf_tmp = 0
  pdf_tmp(0,iflav) = one/grid%dy
  pdf_result = evop%P * pdf_tmp
  !write(6,*) "Some checks"
  !call print_pdf(pdf_result)
  ! do iy_orig = 0, grid%ny
  !   write(6,*) iy_orig*grid%dy, pdf_result(iy_orig,iflav)
  ! end do

  some_result = c_loc(xxxresult)

  ! next try to do the origin calculation
  validation_sum = 0
  pdf_result = 0
  do iflv_orig = -nf_int, nf_int
    do iy_orig = 0, iy
      pdf_tmp = 0
      pdf_tmp(iy_orig, iflv_orig) = pdf_origin(iy_orig, iflv_orig)/grid%dy
      pdf_tmp = evop%P .conv. pdf_tmp
      validation_sum = validation_sum + pdf_tmp(iy,iflav)
      pdf_result(iy_orig, iflv_orig) = pdf_tmp(iy,iflav) / pdf_final(iy,iflav)
    end do
  end do

  write(6,*) "Origin is"
  call print_pdf(pdf_result)
  write(6,*) "Validation sum = ", sum(pdf_result(:,-6:6))*grid%dy

  ! clean up
  call Delete(evop)
  deallocate(pdf_final, pdf_origin, pdf_tmp, pdf_result)

contains 
  subroutine print_pdf(pdf)
    real(dp) :: pdf(0:,ncompmin:)
    real(dp) :: yVals(0:grid%ny)
    integer :: ifl, iy_skip
    yVals = yValues(grid)
    iy_skip = iy / 10
    write(6,'(a3,200f9.5)') 'y', yVals(0:iy:iy_skip)
    do ifl = -6, 6
      write(6,'(i3,200f9.5)') ifl, pdf(0:iy:iy_skip,ifl)
    end do
  end subroutine
end subroutine
