// -*- C++ -*-
// C++ include file for the special panscales interface to hoppet
#ifndef __PANHOPPET_V1__
#define __PANHOPPET_V1__

// define nicer forms of standard f77 naming
#define panhoppetSetGroup         panhoppetsetgroup_
#define panhoppetStart            panhoppetstart_
#define panhoppetStartExtended    panhoppetstartextended_
#define panhoppetAssign           panhoppetassign_
#define panhoppetEvolve           panhoppetevolve_        
#define panhoppetPreEvolve        panhoppetpreevolve_     
#define panhoppetCachedEvolve     panhoppetcachedevolve_
#define panhoppetAlphaS           panhoppetalphas_ 
#define panhoppetSetFFN           panhoppetsetffn_       
#define panhoppetSetVFN           panhoppetsetvfn_       
#define panhoppetSetPoleMassVFN   panhoppetsetpolemassvfn_       
#define panhoppetSetMSbarMassVFN  panhoppetsetmsbarmassvfn_       
#define panhoppetEval             panhoppeteval_          
#define panhoppetEvalFlav         panhoppetevalflav_          
#define panhoppetEvalSplit        panhoppetevalsplit_

extern "C" {

  void panhoppetSetGroup(const double & ca_in, const double & cf_in, const double & tr_in, const int & nf_in) ;

  // return the b0 = (11CA - 4 TR nf)(12pi) coefficient as used by hoppet
  double panhoppetB0();
  // return the b1 coefficient as used by hoppet
  double panhoppetB1();

  /// initialise the underlying grid, splitting functions and pdf-table
  /// objects, using the dy grid spacing and splitting functions up to
  /// nloop loops; all other parameters are set automatically
  void panhoppetStart(const double & dy, const int & nloop);


  // the "factorisation" schemes
  const int factscheme_MSbar    = 1; //< the unpolarised MSbar fact. scheme
  const int factscheme_DIS      = 2; //< the unpolarised DIS fact. scheme (partial support)
  const int factscheme_PolMSbar = 3; //< the polarised MSbar fact. scheme
  

  /// an extended interface for starting panhoppet
  void panhoppetStartExtended(
       const double & ymax,    //< highest value of ln1/x user wants to access
       const double & dy,      //< internal ln1/x grid spacing: 0.1-0.25 is a sensible range
       const double & Qmin,    //< lower limit of Q range
       const double & Qmax,    //< upper limit of Q range
       const double & dlnlnQ,  //< internal table spacing in lnlnQ (e.g. dy/4)
       const int & nloop,      //< the maximum number of loops we'll want (<=3)
       const int & order,      //< order of numerical interpolation (e.g. -6)
       const int & factscheme, //< one of the factschemes defined above
       const int & nested  //< indicates whether the grid should be nested
       );


  /// Set things up to be a fixed-flavour number scheme with the given
  /// fixed_nf number of flavours
  void panhoppetSetFFN(const int & fixed_nf);


  /// Set things up to be a variable-flavour number scheme with the given
  /// quark (pole) masses. Now deprecated; use panhoppetSetPoleMassVFN instead
  void  panhoppetSetVFN(const double &mc, const double & mb, const double & mt);

  /// Set things up to be a variable-flavour number scheme with the
  /// given quark (pole) masses. Thresholds are crossed at the pole
  /// masses, both for the coupling and the PDF evolution.
  void  panhoppetSetPoleMassVFN(const double &mc, const double & mb, const double & mt);

  /// Set things up to be a variable-flavour number scheme with the given
  /// quark (MSbar) masses. Thresholds are crossed at the MSbar
  /// masses, both for the coupling and the PDF evolution.
  void  panhoppetSetMSbarMassVFN(const double &mc, const double & mb, const double & mt);

  /// Given a pdf_subroutine with the interface shown below, initialise
  /// our internal pdf table.
  void panhoppetAssign(void (* pdf_subroutine)(const double & x, 
                                            const double & Q, double * res) );


  /// Given a pdf_subroutine with the interface shown below, fill the 
  /// table by evolving the PDF from scale Q0pdf, with alphas provided 
  /// at scale Q0alphas
  void panhoppetEvolve(const double & asQ0,
                    const double & Q0alphas,
                    const int    & nloop,
                    const double & muR_Q,
                    void (* pdf_subroutine)(const double & x, 
                                            const double & Q, double * res),
                    const double & Q0pdf);


  /// Prepare a cached evolution
  void panhoppetPreEvolve(const double & asQ0, 
                       const double & Q0alphas, 
                       const int    & nloop, 
                       const double & muR_Q, 
                       const double & Q0pdf);


  /// Carry out a cached evolution based on the initial condition
  /// that can be obtained from pdf_subroutine at the scale Q0pdf set in
  /// panhoppetPreEvolve
  void panhoppetCachedEvolve(void (*pdf_subroutine)(const double & x, 
                                     const double & Q, double * res));

  /// Return the coupling at scale Q
  double panhoppetAlphaS(const double & Q);

  /// Return in f[0..12] the value of the internally stored pdf at the
  /// given x,Q, with the usual LHApdf meanings for the indices -6:6.
  void panhoppetEval(const double & x,
                  const double & Q,
                  double * f);

  /// Return the value of the internally stored pdf at the
  /// given x,Q, for the given iflv (in the range -6:6)
  double panhoppetEvalFlav(const double & x,
                  const double & Q,
                  const int & iflv);


  /// Return in f[0..12] the value of 
  ///
  ///    [P(iloop,nf) \otimes pdf] (x,Q)
  ///
  /// where P(iloop,nf) is the iloop-splitting function for the given
  /// value of nf, and pdf is our internally stored pdf.
  ///
  /// The normalisation is such that the nloop dglap evolution equation is
  ///
  ///     dpdf/dlnQ^2 = sum_{iloop=1}^nloop 
  ///                        (alphas/(2*pi))^iloop * P(iloop,nf) \otimes pdf
  ///
  /// Note that each time nf changes relative to a previous call for the
  /// same iloop, the convolution has to be repeated for the whole
  /// table. So for efficient results when requiring multiple nf values,
  /// calls with the same nf value should be grouped together.
  ///
  /// In particular, for repeated calls with the same value of nf, the
  /// convolutions are carried out only on the first call (i.e. once for
  /// each value of iloop). Multiple calls with different values for
  /// iloop can be carried out without problems.
  ///
  void panhoppetEvalSplit(const double & x,
                       const double & Q,
                       const int    & iloop,
                       const int    & nf,
                       double * f);

  // Return 
  //
  //  [P(iloop,nf)_{G,XX} \otimes pdf_{XX}] (x,Q) [including the standard leading factor of x]
  // 
  // where XX=0 means the gluon and XX=1 means the singlet.
  //
  // NB: this routine does not currently cache any results and is
  //     quite inefficient
  double panhoppetEvalGXSplit(const double & x,
                       const double & Q,
                       const int    & iloop,
                       const int    & nf,
                       const int    & xx);


  /// Sets up the cached PDF origins with the specified Qorigin and Qfinal
  /// values, based on the existing table, coupling, etc. setups
  void panhoppetInitOrigins(const double & Qorigin, const double & Qfinal);

  /// Given a prior call to panhoppetInitOrigins (which sets Q_origin and
  /// Q_final and caches relevant info), this returns the fractional
  /// contribution to pdf_{iflv_final}(x_final,Q_final) that comes from
  /// flavour iflv_origin, at x_origin,Q_origin.
  ///
  /// Labelling the return as F, the normalisation is such that 
  ///
  ///   \sum_{iflv_origin} \int dx/x F_{iflv_origin}(x) = 1
  /// 
  /// The evaluation is relatively cheap (equivalent to three independent x 
  /// evaluations).
  ///
  /// Note that for x_origin close to x_final, the results are very sensitive
  /// to the underlying structure of a discretised delta function representation
  /// and so become inaccurate (with standard values of |order|~5 this is visible
  /// as oscillations of the result).
  ///
  /// This function and panhoppetInitOrigins are valid only for negative values 
  /// of the grid order
  ///
  double panhoppetGetOrigins(const int & iflv_origin, const double & x_origin, 
                             const int & iflv_final, const double & x_final  );


}
#endif // __PANHOPPET_V1__
