//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
/// \file ShowerFromCmdLine.cc
/// handles all different options that can be 
/// passed to the PanScales framework via command line
/// arguments, e.g. shower name, number of events,...
#include "ShowerFromCmdLine.hh"

#include "QCD.hh"
#include "Type.hh"
#include "Event.hh"
#include "ShowerDipoleKt.hh"
#include "ShowerToyPythia8.hh"
#include "ShowerToyDire.hh" 
#include "ShowerPanScaleLocal.hh"
#include "ShowerPanScaleLocalpp.hh"
#include "ShowerPanScaleLocalVincia.hh"
#include "ShowerPanScaleLocalAntennapp.hh"
#include "ShowerPanScaleLocalDIS.hh"
#include "ShowerPanScaleGlobal.hh"
#include "ShowerPanScaleGlobalpp.hh"
#include "ShowerPanScaleGlobalDIS.hh"
#include "ShowerPowheg.hh"
#include "ShowerUserDefined.hh" 
#include <limits>

namespace panscales{

/// creates an object derived from the specified Base class
/// that is specific for the shower specified in the command-line
/// arguments
panscales::ShowerRunner * create_showerrunner_with_shower_and_qcd_instance(CmdLine & cmdline, std::ostream & header_str, const panscales::Collider & collider) {
  
  /*----*/ cmdline.start_section("QCD and scale options"); /*----*/

  unsigned int nloops = 0;
  double alphas = 0.1;
  double default_kt_cutoff = -1.0;
  double Q = 1.0;
  bool   physical_coupling = cmdline.present("-physical-coupling").
      help("Sets up a physical coupling, with 2-loop running, alphas(mZ)=0.118, "
           "kt-cutoff = 0.5GeV and automatically calculates and sets the lnvmin "
           "required for showering to continue down to the cutoff. "
           "Momenta are assumed to be in GeV.");
  if (physical_coupling) {
    // if the option name gets changed, watch out that 
    nloops = 2;
    alphas = 0.118;
    default_kt_cutoff = 0.5;
    Q = 91.1876;
    // remember to set physical_coupling = true when initialising QCD
  }

  // set up the QCD instance we will use
  int qcd_nf = cmdline.value<unsigned int>("-nf", 5).help("number of flavours").argname("nf");
  // 2024-02-20: we realised this was not being used properly, so comment it out for now
  //bool mass_thresholds = cmdline.present("-coupling-with-threshold").help("Sets up a running coupling with threshold jumps");

  alphas = cmdline.value("-alphas", alphas).help("fixed value of alphas").argname("alphas");
  nloops = cmdline.value<unsigned int>("-nloops", nloops)
    .help("set the number of loops for the running of alphas (0 means fixed)").argname("nloops");
  Q      = cmdline.value("-Q",Q)
    .help("hard scale (at which the coupling is specified)").argname("Q");

  // scale options
  double xmur   = cmdline.value("-xmur",   1.0).help("sets ratio of renormalisation scale to default choice");
  double xmuf   = cmdline.value("-xmuf",   1.0).help("sets ratio of factorisation scale to default choice");
  // hard scales set later when shower is known
  double xhard  = cmdline.value("-xhard",  1.0).help("sets the coefficient probing uncertainties associated with lack of control of the hard-matrix-element");
  double xsimkt = cmdline.value("-xsimkt", 1.0).help("sets the coefficient probing uncertainties associated with commensurate kt emissions (relevant for non-NLL showers)");

  bool xmur_compensation = !cmdline.present("-no-xmur-compensation").
            help("disables compensation of xmur uncertainty (in those showers that support scale compensation)");

  // the coupling cutoff scale is set in one of 3 ways (by order of precedence)
  //  - a max value for alphas
  //  - a kt cutoff
  //  - a lnkt cutoff
  // NB: both cutoff are on mu, not kt
  double alphas_max  = cmdline.value("-alphas-max", -1.0).help("value above which coupling is set to 0").argname("alphas_max");

  auto kt_cutoff_opt   = cmdline.value("-kt-cutoff",  default_kt_cutoff).help("renorm scale mu below which the coupling is set to 0").argname("kt_cutoff");
  auto lnkt_cutoff_opt = cmdline.value("-lnkt-cutoff", kt_cutoff_opt > 0 ? log(kt_cutoff_opt) : -0.999999 * numeric_limits<double>::max())
                              .help("log(renorm scale) below which the coupling is set to 0").argname("lnkt_cutoff");
  double lnkt_cutoff = lnkt_cutoff_opt;
  if (kt_cutoff_opt.present() && lnkt_cutoff_opt.present()) {
    throw std::invalid_argument("Cannot specify both -kt-cutoff and -lnkt-cutoff");
  }

  // Make sure the user does not do stuff the code cannot handle
  assert((log(Q) >= lnkt_cutoff) && "You specified the coupling at scale lnQ which is smaller then the cutoff on lnkt.");

  // Configure the colour behaviour ................
  cmdline.start_subsection("Colour handling");
  ColourScheme colour_scheme = cmdline.value<panscales::ColourScheme>("-colour", 
                  ColourScheme::NODS)
                  .help("sets the scheme used for the treatment of subleading colour corrections (if one is not working in the large-Nc limit)")
                  .choices(panscales::ColourScheme_values(), panscales::ColourScheme_doxygen());


  // Configure splitting behaviour ...........................
  cmdline.start_subsection("Control over splitting functions");
  // 2024-02-20: we realised this was not being used properly, so comment it out for now
  //bool soft = cmdline.present("-soft").help("force use of soft limit of splitting functions");
  bool g2qg   = cmdline.present("-g2qg").help("g use the q->qg splitting function");
  bool nog2qq = cmdline.present("-nog2qq").help("do not include g->qqbar splittings");
  double splitting_wgg = cmdline.value("-splitting-wgg", 0.0).help("weight parameter for partitioning the g2gg splitting function").argname("splitting_wgg");
  double splitting_wqq = cmdline.value("-splitting-wqq", 0.0).help("weight parameter for partitioning the g2qqbar splitting function").argname("splitting_wqq");
  cmdline.end_subsection();

  if (g2qg && (!nog2qq)){
    std::cout << "disabling g2qq spliting since using q->qg splitting for gluons" << std::endl;
    nog2qq=true;
  }

  // get the QCD instance
  //
  // Note that we must set the colour factors before we set the alphas
  // scales so that the test that we're above the Landau pole uses the
  // right Casimirs. We therefore set a dummy coupling first, then set
  // the colour factors and finally set the alphas information.
  //
  //panscales::QCDinstance qcd(alphas, qcd_nf, soft, mass_thresholds);
  panscales::QCDinstance qcd(nloops, alphas, log(Q), lnkt_cutoff, qcd_nf);

  // More colour handling
  cmdline.start_subsection("Colour handling");
  // for ease of switching between codes 
  if (cmdline.optional_value<double>("-TR").help("sets the TR QCD Casimir").present())
    qcd.set_TR(cmdline.optional_value<double>("-TR"));
  if (cmdline.present("-colour-factor-nog2qqbar").help("when present, do not insert transition points when doing a g->qqbar splitting"))
    qcd.set_colour_no_g2qqbar(true);
  cmdline.end_subsection();

  qcd.set_colour_scheme(colour_scheme);
  // if (colour_scheme == Colour::CATwoCF) { qcd.set_CA_2CF(); }
  // if (colour_scheme == Colour::CFHalfCA){ qcd.set_CF_halfCA(); }
  qcd.set_alphas(nloops, alphas, log(Q), lnkt_cutoff);

  qcd.set_xmuR(xmur);
  qcd.set_xmuF(xmuf);
  qcd.set_xmuR_compensation(xmur_compensation);

  // make sure that anything we do with PDFs has the same colour factors
  panhoppetSetGroup(qcd.CA(), qcd.CF(), qcd.TR(), qcd.nf());

  qcd.set_gluon_uses_qqbar(!nog2qq);
  qcd.set_gluon_uses_q2qg(g2qg);
  qcd.set_splitting_finite_weight_g2gg   (splitting_wgg);
  qcd.set_splitting_finite_weight_g2qqbar(splitting_wqq);
  qcd.set_physical_coupling(physical_coupling);

  
  if (alphas_max>0) qcd.set_max_alphas(alphas_max);
  if (cmdline.present("-no-CMW").help("turns off CMW scheme (on by default only when using nloops>=2)")) qcd.set_use_CMW(false);

  // // colour treatment
  // qcd.set_colour_factor_from_emitter(colour_factor_from_emitter);
  // qcd.set_colour_factor_nods(colour_factor_nods);
  
  /*----*/ cmdline.end_section("QCD and scale options"); /*----*/


  // set up the shower we will use, possibly with further arguments below
  panscales::ShowerBase * shower = define_shower_from_cmd_line(cmdline, qcd, collider);
  // set the variation of hard and simkt uncertainties
  shower->set_uncertainty_x_hard (xhard);
  shower->set_uncertainty_x_simkt(xsimkt);
  


  // get the strategy for showerrunner
  cmdline.start_section("Options for strategy to be used for selection of branchings");
  panscales::Strategy strategy       = cmdline.value("-strategy", panscales::RoulettePeriodic).argname("strat")
    .help("Strategy to be used for MC selection of dipole branching. "
          "MC averaged results should be independent of the choice, "
          "except for strategies that focus generation on specific rapidity windows "
          "(normally used only for log-accurace testing).")
    .choices(panscales::Strategy_values(), panscales::Strategy_doxygen());
  // set up a new shower runner instance
  panscales::ShowerRunner * class_with_shower = new panscales::ShowerRunner(shower, strategy);
  
  if (strategy == panscales::CentralRap || strategy == panscales::CentralAndCollinearRap) {
    double half_central_rap_window = cmdline.value<double>("-half-central-rap-window")
        .help("Operative for CentralRap generation strategy only: allows setting "
              "the extent of the η_approx window either side of the dipole centre "
              "at min(|η_approx|) for the dipole");
    class_with_shower->half_central_rap_window(half_central_rap_window);
  } 
  if (strategy == panscales::CollinearRap || strategy == panscales::CentralAndCollinearRap) {
    double collinear_rap_window = cmdline.value<double>("-collinear-rap-window").
                  help("set the collinear-region rapidity window in which to restrict generation");
    assert(collinear_rap_window > 0.0);
    class_with_shower->collinear_rap_window(collinear_rap_window);
  }

  // print out a description
  //
  //TRK_ISSUE-802: shower/shower_runner include several things in
  // their description which are only set after. The printout should
  // therefore be moved further down. We may also want to avoid the
  // duplicate printour (although the multi-line one is easier to read
  // than the single-line descriptino of the shower/showerrunner.
  header_str << "# qcd = "                    << qcd.description() << std::endl;
  header_str << "# shower = "                 << class_with_shower->shower()->description() << std::endl;
  header_str << "# shower runner = "          << class_with_shower->description() << endl;
  header_str << "# shower runner strategy = " << class_with_shower->strategy_description() << endl;
  
  cmdline.end_section("Options for strategy to be used for selection of branchings");
  //
  //...........................
  cmdline.start_section("Options for double-soft corrections",
      "Many of these option are valid only if -double-soft is present and some break "
      "next-to-single logarithmic accuracy.");
  bool do_double_soft = cmdline.present("-double-soft").help(
    "Turn on double soft ME corrections. Only supported for the PanGlobal e+e- shower.");
  bool do_dKCMW = !cmdline.present("-double-soft-no-dKCMW").help("Turn off double soft delta KCMW corrections");
  // For split-dipole frame PG, we turn off deltaK_CMW (it is zero).
  // Ultimately, we want to have deltaK_CMW return zero instead,
  // but for now there is no mechanism to make the matching shower
  // use deltaK from the main shower.
  do_dKCMW &= !cmdline.present("-split-dipole-frame"); // NB help specified in ShowerFromCmdLine
  double dKCMW_error_offset = cmdline.value("-double-soft-dKCMW-error-offset", 0.0).help("Evaluate systematics (uses deltaK+factor*uncert)");
  bool dKCMW_subtract_fit = cmdline.present("-double-soft-dKCMW-subtract-fit").help(
    "Subtract the fitted value from deltaK before interpolating "
    "(not recommended for production, but useful for debugging).");
  
  if (do_double_soft){
    class_with_shower->enable_double_soft(true, do_dKCMW, dKCMW_error_offset, dKCMW_subtract_fit);
    header_str << "# double soft ME corrections = on" << endl;

    // the following options are not in markdown, but as 
    // they should be used only for testing, this is ok
    if (cmdline.present("-double-soft-CF-only").help("use only the CF part of the real double-soft emission (for testing only)")){
      class_with_shower->shower()->set_double_soft_CF_only();
      header_str << "# double soft only CF channel " << endl;
    }
    if (cmdline.present("-double-soft-CA-only").help("use only the CA part of the real double-soft emission (for testing only)")){
      class_with_shower->shower()->set_double_soft_CA_only();
      header_str << "# double soft only CA channel " << endl;
    }
    if (cmdline.present("-double-soft-TR-only").help("use only the TR part of the real double-soft emission (for testing only)")){
      class_with_shower->shower()->set_double_soft_TR_only();
      header_str << "# double soft only TR channel " << endl;
    }

    if (do_dKCMW){
      header_str << "# double soft dKCMW corrections = on" << endl;
      header_str << "# double soft dKCMW error offset factor = " << dKCMW_error_offset << endl;
      header_str << "# double soft dKCMW rough fit subtraction = "
              << (dKCMW_subtract_fit ? "on" : "off") << endl;
      auto * pg_shower = dynamic_cast<panscales::ShowerPanScaleGlobal*>(class_with_shower->shower());
      if (pg_shower){
        header_str << "# deltaK regularised using: ";
        switch (pg_shower->deltaK_regularisation()){
        case panscales::PanGlobalDeltaKRegularisationOption::Tanh:          header_str << "tanh"; break;
        case panscales::PanGlobalDeltaKRegularisationOption::Linear:        header_str << "linear"; break;
        case panscales::PanGlobalDeltaKRegularisationOption::Tanh3:         header_str << "tanh3"; break;
        case panscales::PanGlobalDeltaKRegularisationOption::PiecewiseTanh: header_str << "piece-wise tanh"; break;
        default:
          assert(false && "Unknown deltaK regularisation option");
        };
        header_str << endl;
      }
    } else {
      header_str << "# double soft dKCMW corrections = off" << endl;
    }
  } else {
    header_str << "# double soft ME corrections = off" << endl;
  }
  if(cmdline.present("-double-soft-no-colour-flow-swaps").help("Turn off colour-flow swaps in the double soft ME corrections")){
    class_with_shower->disable_double_soft_colour_flow_swaps();
    header_str << "# double soft ME corrections have colour-flow swaps switched off" << endl;
  }  
  cmdline.end_section();


  // Control switches for spin correlations
  cmdline.section("Spin correlations");
  bool do_spin_corr = cmdline.value_bool("-spin-corr", true).help("determines if spin correlations are turned on");
  bool do_soft_spin = cmdline.value_bool("-soft-spin", true).help("determines if soft spin correlations are turned on, arXiv:2111.01161") && do_spin_corr;
  bool do_coll_proj = cmdline.value_bool("-collinear-spin-projection", false).help("When using spin correlations to determine azimuthal angle of a branching, "
                  "evaluate it in the collinear limit, which guarantees a pure cos(2phi) style distribution "
                  "in the shower phi variable. This is only available for PanLocal, PanLocal-Vincia/Antenna and PanGlobal showers"
                  "and not yet fully tested.") && do_spin_corr;

  // Flag for declustering analysis
  bool do_declustering_analysis = cmdline.value_bool("-spin-do-decluster-analysis", false).help("if present, "
                  "the shower records necessary kinematic information in the spin correlation tree, for use "
                  "as a proxy to a declustering analysis");
  class_with_shower->set_do_declustering_analysis(do_declustering_analysis);

  // Flag for storage of extra spin tree information
  bool store_extra_info = cmdline.value_bool("-spin-store-extra", false).help("if present, energies and angles of branching particles are stored "
                  "in the spin correlation tree (necessary for certain analyses)");
  class_with_shower->store_extra_branching_info(store_extra_info);
  // check that we do not try to do spin correlations with double-soft
  assert(!(do_spin_corr && do_double_soft)
    && "Spin correlations cannot be used in conjunction with double-soft corrections at the moment. Please use '-spin-corr off'.");
  // check that we do not try to run spin with collinear spin projection with showers it is not meant to work with
  if(do_coll_proj) {
    assert((class_with_shower->shower()->name() == "panlocal"        ||
            class_with_shower->shower()->name() == "panlocal-vincia" ||
            class_with_shower->shower()->name() == "panlocal-antenna"||
            class_with_shower->shower()->name() == "panglobal"
            )
          && "Collinear projection spin correlation algorithm is only available for PanLocal, PanLocal-Vincia/Antenna and PanGlobal showers." 
        );
  }
  cmdline.end_section("Spin correlations");
  if (do_spin_corr) {
    if (!do_soft_spin) class_with_shower->disable_soft_spin_corrections();
    if (!do_coll_proj) class_with_shower->disable_collinear_spin_projection();
  } else {    
    class_with_shower->disable_spin_correlations();
  }
  auto on_off = [](bool b) { return b ? "on" : "off"; };
  header_str << "# spin correlations = "     << on_off(do_spin_corr) << endl;
  header_str << "# soft spin corrections = " << on_off(do_soft_spin) << endl;
  header_str << "# collinear spin projection = " << on_off(do_coll_proj) << endl;


  return class_with_shower;
}



/// @brief function to create a shower given a cmdline, name of the shower, qcd instance and collider
/// @param cmdline 
/// @param shower_name 
/// @param qcd 
/// @param collider 
/// @return pointer to a shower, the caller takes ownership
panscales::ShowerBase * define_shower_from_cmd_line_with_name_given(CmdLine & cmdline, const std::string & shower_name, const panscales::QCDinstance & qcd, const panscales::Collider & collider){
  panscales::ShowerBase * shower = nullptr;
  // setting up the shower depends on the collider type
  panscales::Collider::ColliderType collider_type = collider.type();


  cmdline.start_section("Shower options");

  CmdLine::Result<double> betaps = cmdline.optional_value<double>("-beta").help(
    "Beta dependence of ordering variable v = kt theta^beta. "
    "If not supplied, uses to shower's default (see each shower for details)");

  bool additive = ! cmdline.value_bool("-mult-branching",false).help(
    "Deprecated. Specifically for antenna showers (PanGlobal and PanLocal-antenna), if true/yes, uses a "
    "multiplicative combination of splittings functions from the two dipoles ends.");

  //-----------------------------------------------------
  cmdline.start_subsection("PanGlobal showers",
            "Class of antenna showers with global transverse momentum recoil and dipole-local longitudinal recoil. "
            "Default beta is 0, which can be changed with the -beta option (recommended values 0.0 or 0.5)");
  // if PanGlobal is to be used as the matching shower
  PanGlobalRescaleOption rescaling_option_PG = 
        cmdline.value<PanGlobalRescaleOption>("-panglobal-rescaling", LocalRescaling).help(
                  "Sets the rescaling option for the FF dipoles in PanGlobal showers. "
                  "See 2307.11142 (App.1) and 2305.08645 (App.B) for details.")
                    .choices(PanGlobalRescaleOption_values(), PanGlobalRescaleOption_doxygen());
  bool dynamic_hard_system = cmdline.present("-dynamic-hard-system").help(
       "Option to evolve the hard system dynamically in PanGlobalpp (not yet validated)");
  bool dipole_frame = cmdline.present("-split-dipole-frame")
                            .help("In PanGlobal-ee, use zero rapidity in the dipole frame for "
                                    "transitioning between splitting functions for the two ends. "
                            "If this is not specified, the transition is the dipole bisector in the hard-system frame.");
  // allows one to adjust the way deltaK is regularised 
  PanGlobalDeltaKRegularisationOption deltaK_regul = 
        cmdline.value("-deltaK-regularisation", PanGlobalDeltaKRegularisationOption::Tanh)
        .argname("deltaK-reg")
        .help("Prescription to regularise 1+x with x=as/(2pi) deltaKCMW")
        .choices(PanGlobalDeltaKRegularisationOption_values(), PanGlobalDeltaKRegularisationOption_doxygen());
  double def_boost_rescaling_cutoff = 0.0;
  double boost_rescaling_cutoff = cmdline.value("-boost-rescaling-cutoff", def_boost_rescaling_cutoff).help(
    "In PanGlobal-ee, sets the cutoff below which no event-wide boost rescaling is performed");
  cmdline.end_subsection("PanGlobal showers");

  if(shower_name == "panglobal" || shower_name == "PanScaleGlobal-pp" || shower_name == "PanScaleGlobal-ee" || shower_name == "PanScaleGlobal-DIS"){
    // PG depends in general on two additional quantities: the rescaling option and the beta value
    // get the rescaling option for Panglobal
    // the default depends on whether to use the ee shower or pp shower
    // NOTE: this also needs to be done consistently when we call create_showers_for_matching(),
    double beta = betaps.value_or(0.0);
        
    if(collider_type == panscales::Collider::ColliderType::pp){
      shower = new panscales::ShowerPanScaleGlobalpp(qcd, beta, rescaling_option_PG, dynamic_hard_system);
    } else if (collider_type == panscales::Collider::ColliderType::dis){
      shower = new panscales::ShowerPanScaleGlobalDIS(qcd, beta, rescaling_option_PG);
    } else {
      // NB: this variable is in practice unused...
      double additive_suppression_power = 0.0;
      shower = new panscales::ShowerPanScaleGlobal(qcd, beta, additive, additive_suppression_power, boost_rescaling_cutoff, rescaling_option_PG, dipole_frame);
      
      // set the value in the shower
      dynamic_cast<panscales::ShowerPanScaleGlobal*>(shower)->set_deltaK_regularisation(deltaK_regul);
    }

  } 
  
  //-----------------------------------------------------
  cmdline.start_subsection("PanLocal-dipole showers",
        "Class of dipole showers with dipole-local longitudinal and transverse recoil, "
        "split at dipole bisector in hard-process frame. "
        "Default beta is 0.5, which can be changed with the -beta option (recommended value: 0.5)");
  ShowerPanScaleLocalpp::GammaChoice gamma_choice = 
      cmdline.value("-gamma-choice", ShowerPanScaleLocalpp::GammaFlat).help(
         "For PanLocal-pp/DIS: Sets an additional exponent factor in the mapping coefficients, "
         "impacting the contours in the hard-collinear part of the Lund plane."
      ).choices(ShowerPanScaleLocalpp::GammaChoice_values(), ShowerPanScaleLocalpp::GammaChoice_doxygen());


  cmdline.end_subsection("PanLocal-dipole showers");
  if (shower_name == "panlocal" || shower_name == "PanScaleLocal-pp" 
     || shower_name == "PanScaleLocal-ee" || shower_name == "PanScaleLocal-DIS") {
    double beta = betaps.value_or(0.5);

    if(collider_type == panscales::Collider::ColliderType::pp){
      // in this case we may add an additional scaling to the mapping coefficients
      shower = new panscales::ShowerPanScaleLocalpp(qcd, beta, gamma_choice);
    } else if(collider_type == panscales::Collider::ColliderType::dis){
      shower = new panscales::ShowerPanScaleLocalDIS(qcd, beta);
    } else{
      shower = new panscales::ShowerPanScaleLocal(qcd, beta);
    }
  } 
  
  //-----------------------------------------------------
  cmdline.start_subsection("PanLocal-antenna showers",
      "Class of antenna showers with dipole-local longitudinal and transverse recoil, split at dipole bisector in event frame. "
      "Default beta is 0.5, which can be changed with the -beta option (recommended value: 0.5)");
  cmdline.end_subsection("PanLocal-antenna showers");
  if (shower_name == "panlocal-antenna" || shower_name == "panlocal-vincia" || shower_name == "PanScaleLocalVincia-ee" || shower_name == "PanScaleLocalAntenna-pp"){
    double beta = betaps.value_or(0.5);
    if(collider_type == panscales::Collider::ColliderType::pp){
      shower = new panscales::ShowerPanScaleLocalAntennapp(qcd, beta);
    } else if(collider_type == panscales::Collider::ColliderType::dis){
      throw std::invalid_argument("PanLocal antenna shower does not exist for DIS events");
    } else{
      shower = new panscales::ShowerPanScaleLocalVincia(qcd, beta, additive);
    }
  } 
  
  //-----------------------------------------------------
  cmdline.start_subsection("Dipole-kt showers",
        "Class of kt-ordered showers with dipole-local recoil, "
        "splitting dipole in dipole centre-of-mass frame.");
  bool if_is_global = !cmdline.value_bool("-if-is-local",false).help(
    "If yes/true, the recoil in initial-final (IF) dipoles is dipole-local, "
    "taken entirely by the final-state leg. Otherwise it is global.");
  bool compensate_lxmuR = cmdline.value_bool("-compensate-lxmuR", false).help("If yes/true, the shower includes an NLO compensation for the uncertainty on the scale xmur");
  cmdline.end_subsection("Dipole-kt showers");
  if (shower_name == "dipole-kt"){
    //if(collider_type == panscales::Collider::ColliderType::pp || collider_type == panscales::Collider::ColliderType::dis){
    // note that the FF implementation of dipole-kt 
    // is not equal to dire, as the implementation 
    // of the splitting functions is different away
    // from the strict soft/collinear limit
    if(if_is_global && collider_type == panscales::Collider::ColliderType::dis)
      throw std::invalid_argument("Tried to run dipole-kt with global IF recoil for DIS events");

    if (betaps.value_or(0.0) != 0.0) throw std::invalid_argument("Dipole-kt shower does not support beta != 0.0");
    shower = new panscales::ShowerDipoleKt(qcd, if_is_global, compensate_lxmuR);
  } 
  
  //-----------------------------------------------------
  //cmdline.start_subsection("ToyDire showers");
  //cmdline.end_subsection("ToyDire showers");
  if ((shower_name == "toydire") || (shower_name == "dire")){
    // if (shower_name == "dire"){  
    //   std::cerr << "The use of -shower dire is deprecated. Please use -shower toydire instead." << std::endl;
    // }
    if(collider_type == panscales::Collider::ColliderType::pp || collider_type == panscales::Collider::ColliderType::dis){
        throw std::invalid_argument("The (toy) PanScales implementation of the dire shower can only be run with e+e- events");
    };
    if (betaps.value_or(0.0) != 0.0) throw std::invalid_argument("ToyDire shower does not support beta != 0.0");

    shower = new panscales::ShowerToyDire(qcd);

  } 
  
  //-----------------------------------------------------
  if ((shower_name == "toypythia8") || (shower_name == "pythia8")) {
    if (shower_name == "pythia8"){
      std::cerr << "The use of -shower pythia8 is deprecated. Please use -shower toypythia8 instead." << std::endl;
    }
    if(!(collider_type == panscales::Collider::ColliderType::epem)) throw std::invalid_argument("The (toy) PanScales implementation of the pythia8 shower can only be run with e+e- events");
    if (betaps.value_or(0.0) != 0.0) throw std::invalid_argument("ToyPythia8 shower does not support beta != 0.0");
    shower = new panscales::ShowerToyPythia8(qcd);

  } 
  
  //-----------------------------------------------------
  if (shower_name == "user-defined") {    
    std::cout << "#================================================================================================" << std::endl;
    std::cout << "#      !!!     !!!     !!!     !!!     !!!   WARNING     !!!     !!!     !!!     !!!     !!!     " << std::endl;
    std::cout << "# The user-defined shower requires inputs in ShowerUserDefined.{hh,cc} in order to be functional." << std::endl;
    std::cout << "#================================================================================================" << std::endl;
    if (betaps.present()) throw std::invalid_argument("User-defined shower does not support beta argument");
    if(!(collider_type == panscales::Collider::ColliderType::epem)) throw std::invalid_argument("The user-defined shower can only be run with e+e- events");
    shower = new panscales::ShowerUserDefined(qcd);
  }
  
  //-----------------------------------------------------
  if (shower_name == "powheg") {
    cmdline.start_subsection("Options for Powheg 'shower'");
    if(!(collider_type == panscales::Collider::ColliderType::epem)) throw std::invalid_argument("The PanScales implementation of the PowHeg mapping can only be run with e+e- events");
    // note that this is not a full powheg shower
    // rather it is the FKS mapping modified such that
    // we take a beta argument
    // furthermore, this shower is to be used in the context of matching
    double beta = betaps.value_or(0.0);
    shower = new panscales::ShowerPowheg(qcd, beta);
    cmdline.end_subsection("Options for Powheg 'shower'");
  }

  //-----------------------------------------------------
  if (shower == nullptr) {
    std::cout << "Unrecognised shower " << shower_name << " for ColliderType " << collider_type << std::endl;
    exit(-1);
  }

  if (cmdline.present("-boost-at-end")
         .help("for certain showers, allows any post-emission boosts to "
               "be collated and postponed to the end of the event")) shower->set_boost_at_end(true);

  cmdline.end_section("Shower options");

  return shower;
}

/// @brief function to create a shower given a cmdline, qcd instance and collider
/// @param cmdline 
/// @param qcd 
/// @param collider 
/// @return pointer to a shower
panscales::ShowerBase * define_shower_from_cmd_line(CmdLine & cmdline, const panscales::QCDinstance & qcd, const panscales::Collider & collider){
  // get the name of the shower
  cmdline.start_section("Shower options");  
  std::string shower_name = cmdline.value<std::string>("-shower","panglobal")
    .help("name of shower (valid choices include: panlocal, panglobal, panlocal-vincia, panlocal-antenna, dipole-kt, toy-pythia8, dire, user-defined)")
    .argname("showername");
  cmdline.end_section("Shower options");
  return define_shower_from_cmd_line_with_name_given(cmdline, shower_name, qcd, collider);
}

/// @brief creates the shower used for matching in showerrunner, and sets up the pointers in showerrunner
/// @param cmdline 
/// @param shower_runner 
/// @param collider 
void set_shower_for_matching(CmdLine & cmdline, panscales::ShowerRunner * shower_runner, const panscales::Collider & collider) {
  panscales::ShowerBase * shower_for_matching = nullptr;
  // option to have a matched shower different from the main shower
  auto matching_shower_opt = cmdline.optional_value<string>("-matching-shower");
  if (matching_shower_opt.present()){
    std::string matching_shower_name = matching_shower_opt.value();
    shower_for_matching = define_shower_from_cmd_line_with_name_given(cmdline, matching_shower_name, shower_runner->shower()->qcd(), collider);
  } else{

    shower_for_matching = define_shower_from_cmd_line_with_name_given(cmdline, shower_runner->shower()->name(), shower_runner->shower()->qcd(), collider);
  }
  // turn on directional differences if the main shower does this as well
  if(shower_runner->shower()->use_diffs()) {
    shower_for_matching->set_use_diffs(true);
  }
  // set the matching shower in showerrunner
  shower_runner->set_shower_for_matching(shower_for_matching);
  // We need to transfer the double-soft options from the main shower
  shower_runner->shower_for_matching()->enable_double_soft(shower_runner->shower()->double_soft(),
                                                            shower_runner->shower()->double_soft_dKCMW(),
                                                            shower_runner->shower()->double_soft_dKCMW_error_offset_factor(),
                                                            shower_runner->shower()->double_soft_dKCMW_subtract_fit());
}

/// @brief creates a hoppetrunner, returns a pointer
///        the user owns the pointer and is responsible for cleaning it up
/// @param cmdline 
/// @param header_str 
/// @return pointer of a hoppetrunner instance
panscales::HoppetRunner * create_hoppetrunner(CmdLine & cmdline, std::ostream & header_str, bool collider_has_pdfs){
  // get the pdf_choice from the command line
  cmdline.start_section("PDF settings");
  panscales::HoppetRunner::PDFChoice pdf_choice = 
        cmdline.value<panscales::HoppetRunner::PDFChoice>("-pdf-choice", panscales::HoppetRunner::ToyNf5)
          .help("the PDF choice. PDFs with remapped scales are to be used only for log testing")
          .choices(panscales::HoppetRunner::PDFChoice_values(), panscales::HoppetRunner::PDFChoice_doxygen());
  // see whether we need LHA-PDF
  string lhapdf_set = cmdline.value<string>("-lhapdf-set", "").help("Name of the LHAPDF set");
  cmdline.end_section("PDF settings");
  if (lhapdf_set != "") pdf_choice = panscales::HoppetRunner::LHAPDFSet;

  if (!collider_has_pdfs) return nullptr;

  // create a new instance
  panscales::HoppetRunner * hoppet_runner = new panscales::HoppetRunner(pdf_choice, lhapdf_set);
  return hoppet_runner;
}

/// function to pass the hoppetrunner to the showerrunner instance
/// it also sets the max reliable x fraction (default as stored in hoppetrunner)
/// and it sets a scale remapping if running with toy PDFs
void init_hoppetrunner_in_showerrunner(CmdLine & cmdline, panscales::ShowerRunner * shower_runner, panscales::HoppetRunner * hoppet_runner, double lnv_max){
  // set hoppet_runner in shower_runner
  shower_runner->set_PDF_runner(hoppet_runner);
  // if we're using toy PDFs, switch on scale mapping
  cmdline.start_section("PDF settings");
  cmdline.start_subsection("Hoppet settings");

  // to Hoppet max scale Qmax (hoppet_Qmax)
  // we will match the shower max scale (pdf_lnQref)
  // lnQ = lnv
  // alphas(lnQ)
  double pdf_lnQref  = cmdline.value("-pdf-lnQref",  lnv_max)
                              .help("for PDFs with remapped scales, sets the shower scale lnQ that is mapped to hoppet's Qmax");
  auto hoppet_Qmax_opt = cmdline.optional_value<double>("-hoppet-Qmax")
                                .help("hoppet's Qmax (default depends on PDF set)");
  auto max_reliable_x_opt = cmdline.optional_value<double>("-pdf-max-reliable-x")
                                   .help("sets the max PDF x fraction that we can trust (default depends on PDF)");

  if (!hoppet_runner) return;

  double hoppet_Qmax = hoppet_Qmax_opt.value_or(hoppet_runner->Qmax());
  double max_reliable_x = max_reliable_x_opt.value_or(hoppet_runner->pdf_max_reliable_x());

  if (hoppet_runner->pdf_needs_remapped_scales()){
    hoppet_runner->set_Qmax(hoppet_Qmax);
    hoppet_runner->set_scales_mapping(false, pdf_lnQref ,
                                        shower_runner->shower()->qcd().alphasMSbar(pdf_lnQref),
                                        shower_runner->shower()->qcd().nloops());      
  }
  // set the max reliable PDF
  hoppet_runner->set_pdf_max_reliable_x(max_reliable_x);
  cmdline.end_subsection("Hoppet settings");
  cmdline.end_section("PDF settings");
}

}

