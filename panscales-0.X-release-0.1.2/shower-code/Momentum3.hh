//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __MOMENTUM3_HH__
#define __MOMENTUM3_HH__

#include <math.h>
#include <iostream>
#include <iomanip>
#include "Type.hh"

namespace panscales{

//----------------------------------------------------------------------
/// \class Momentum3
/// class that contains the 3 components of a 4-momentum
template <class T>
class Momentum3 {
public:
  Momentum3() : px_(0.0), py_(0.0), pz_(0.0) {}
  Momentum3(T px, T py, T pz) : px_(px), py_(py), pz_(pz) {}

  /// for access to the underlying precision type
  typedef T prec_type;

  const Momentum3 & p3() const {return *this;}

  /// px
  T px() const {return px_;}
  /// py
  T py() const {return py_;}
  /// pz
  T pz() const {return pz_;}

  /// return the squared modulus of the 3-momentum
  T modpsq() const {
    return px_*px_ + py_*py_ + pz_*pz_;
  }

  /// return the modulus of the 3-momentum
  T modp() const {
    return sqrt(modpsq());
  }

  /// return the squared transverse momentum
  T pt2() const {
    return px_*px_ + py_*py_;
  }
  
  /// return the transverse momentum
  T pt() const {
    return sqrt(pt2());
  }

  /// return a 3-momentum with all components reversed
  Momentum3<T> operator-() const {
    return Momentum3(-px_, -py_, -pz_);
  }

  /// multiplication by T
  Momentum3<T> & operator*=(T coeff) {
    px_ *= coeff;
    py_ *= coeff;
    pz_ *= coeff;
    return *this;
  }
  
  /// division by T
  Momentum3<T> & operator/=(T coeff) {
    px_ /= coeff;
    py_ /= coeff;
    pz_ /= coeff;
    return *this;
  }
  
  /// addition with momentum
  Momentum3<T> & operator+=(const Momentum3<T> & jet) {
    px_ += jet.px();
    py_ += jet.py();
    pz_ += jet.pz();
    return *this;
  }
  
  /// subtraction with momentum
  Momentum3<T> & operator-=(const Momentum3<T> & jet) {
    px_ -= jet.px();
    py_ -= jet.py();
    pz_ -= jet.pz();
    return *this;
  };

  /// returns a copy of the momentum with reversed components
  Momentum3<T> reversed() const {
    return Momentum3<T>(-px_, -py_, -pz_);
  }

protected:
  T px_, py_ ,pz_;
};

//----------------------------------------------------------------------
/// return "sum" of two pseudojets
template <class T>
Momentum3<T> operator+ (const Momentum3<T> & jet1, const Momentum3<T> & jet2) {
  return Momentum3<T>(jet1.px()+jet2.px(),
		  jet1.py()+jet2.py(),
		  jet1.pz()+jet2.pz());
} 

//----------------------------------------------------------------------
/// return difference of two pseudojets
template <class T>
Momentum3<T> operator- (const Momentum3<T> & jet1, const Momentum3<T> & jet2) {
  return Momentum3<T>(jet1.px()-jet2.px(),
		  jet1.py()-jet2.py(),
		  jet1.pz()-jet2.pz());
}
//----------------------------------------------------------------------
/// return the product, coeff * jet
template <class T>
Momentum3<T> operator* (T coeff, const Momentum3<T> & jet) {
  Momentum3<T> coeff_times_jet(jet);
  coeff_times_jet *= coeff;
  return coeff_times_jet;
}

//----------------------------------------------------------------------
/// return the division, jet / coeff
template <class T>
Momentum3<T> operator/ (const Momentum3<T> & jet, T coeff) {
  Momentum3<T> jet_over_coeff(jet);
  jet_over_coeff /= coeff;
  return jet_over_coeff;
}

//----------------------------------------------------------------------
/// output a momentum
template <class T>
inline std::ostream & operator<<(std::ostream & ostr, const Momentum3<T> & p) {
  ostr << std::setw(13) << p.px() << " "
       << std::setw(13) << p.py() << " "
       << std::setw(13) << p.pz();
  return ostr;
}

//----------------------------------------------------------------------
/// Returns the 3-vector cross-product of p1 and p2. If lightlike is false
/// then the energy component is zero; if it's true the the energy component
/// is arranged so that the vector is lightlike
template <class T>
Momentum3<T> cross(const Momentum3<T> & p1, const Momentum3<T> & p2) {
  T px = p1.py() * p2.pz() - p2.py() * p1.pz();
  T py = p1.pz() * p2.px() - p2.pz() * p1.px();
  T pz = p1.px() * p2.py() - p2.px() * p1.py();
  return Momentum3<T>(px, py, pz);
}

//---------------------------------------------------------------------
/// returns the 3d dot product between two momenta
template <class T>
T dot_product_3(const Momentum3<T> & p1, const Momentum3<T> & p2) {
  return p1.px() * p2.px() +  p1.py() * p2.py() + p1.pz() * p2.pz();
}

} // namespace panscales
  
#endif //  __MOMENTUM3_HH__
