//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "JetObservables.hh"
#include "Momentum.hh"

using namespace panscales;

//==========================================================================
// mMDTmass class, mass of heavy hemisphere after mMDT, using exclusive ee CA algorithm with WTA axis
bool mMDTmass::analyse(const Event& event) {
  // cast everything in fjcore's PseudoJet (not very efficient...)
  std::vector<fjcore::PseudoJet> particles;
  for(unsigned int i=0; i<event.size(); ++i) {
    particles.push_back(fjcore::PseudoJet(event[i].px(),event[i].py(),
					  event[i].pz(),event[i].E()));
  }
  // run exclusive clustering
  fjcore::ClusterSequence cs(particles, _jet_def);
  std::vector<fjcore::PseudoJet> exclusive_jets = cs.exclusive_jets(2);

  // get the mass of heavier groomed jet
  _mMDTmass = 0.0;
  for (unsigned int i=0; i<exclusive_jets.size(); ++i) {
    precision_type mass = groomed_mass(exclusive_jets[i]);
    if (mass > _mMDTmass) _mMDTmass = mass;
  }
  return true;
}

precision_type mMDTmass::groomed_mass(const fjcore::PseudoJet& jet) {
  fjcore::PseudoJet subjet, j1, j2;
  subjet = jet;
  while (subjet.has_parents(j1, j2)) {
    if (j1.E() < j2.E()) std::swap(j1,j2);
    // min(E1, E2)/(E1+E2)
    precision_type E1 = j1.E();
    precision_type E2 = j2.E();
    // make sure denominator is non-zero
    precision_type sym = E1 + E2;
    if (sym == 0) return 0.0;
    sym = E2 / sym;
    if (to_double(sym) > _zcut) break;
    subjet = j1;
  }
  return subjet.m2()/subjet.E()/subjet.E();
}


//--------------------------------------------------------------------------

// Provide a listing of the info.
void mMDTmass::list() const {
  std::cout << "\n -----  mMDT mass with zcut= " << _zcut << "  ------ \n";
  std::cout << " mMDTmass = " << _mMDTmass << "\n";
  std::cout << "\n ----------------------" << std::endl;
}
