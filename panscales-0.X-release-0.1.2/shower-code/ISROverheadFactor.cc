
//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include "ISROverheadFactor.hh"

using namespace std;

namespace panscales{

//------------------------------------------------------------------------
// implementation of ISROverheadFactor
//------------------------------------------------------------------------
ISROverheadFactor::ISROverheadFactor(const QCDinstance &qcd, HoppetRunner *hoppet_runner) :
  _qcd(qcd), _hoppet_runner(hoppet_runner),
  _dLsmall(0.2), _dLlarge(0.2),
  _safety_factor(1.2),
  _points_per_grid(10), _grid_iterations(4){
  // set a series of initial default values
  _Lsmall_max = 10.0;  //GS-NOTE: tmp for tests. Should probably be set to 15 
  _Llarge_max = 1.0;   //GS-NOTE: tmp for tests. Should probably be set to 2
  _transition_x = 0.5;
  _transition_logx = -log(_transition_x);
  
  //cout << "# Initialising ISROverheadFactor..." << flush;
  _initialise();
  //cout << "# Initialising ISROverheadFactor done" << endl;
}

/// query the overhead factor for 2 partons of x fractions x1 and x2
/// and flavours id1 and id2.  lnktmax is the current PDF lnkt in
/// the shower (used as an upper bound when finding the max)
///
/// Note that this automatically updates the cache if x1|x2 go
/// outside the already cached range (hence this method being
/// non-const)
double ISROverheadFactor::operator()(double x1, double x2,
                                      int pdgid1, int pdgid2){
  double C1 = _search_and_extend(x1, pdgid1); 
  double C2 = _search_and_extend(x2, pdgid2);  
  
  //cout << " [C1,C2,safety=" << C1 << "," << C2 << "," << _safety_factor << "] ";
  return std::min(std::max(C1, C2) * _safety_factor, _hoppet_runner->max_ISR_overhead());
}

// when only one beam is needed 
double ISROverheadFactor::operator()(double x, 
                                      int pdgid){
  double C = _search_and_extend(x, pdgid); 
  return  std::min(C * _safety_factor, _hoppet_runner->max_ISR_overhead());
}
  
/// fill the initial grids
void ISROverheadFactor::_initialise(){
  // initialise small
  unsigned int nsmall = (unsigned int)(_Lsmall_max/_dLsmall+0.1);
  _Csmall.resize(13); //GS-NOTE: does this work w tops?
  for (int iid=0;iid<13;++iid){
    int pdgid = (iid==6) ? 21 : iid-6;
    _Csmall[iid].resize(nsmall+1);
    for (unsigned int i=0;i<=nsmall;++i){
      double Lsmall = i*_dLsmall; // L=log(x_trans/x) => log(1/x) = L+log(1/xtrans)
      _Csmall[iid][i] = _find_max_probability(Lsmall+_transition_logx, pdgid);
      //if (pdgid == 3) cout << Lsmall+_transition_logx << ", Csmall[" << i << "] = " << _Csmall[iid][i] << endl;
    }
  }

  // initialise large
  unsigned int nlarge = (unsigned int)(_Llarge_max/_dLlarge+0.1);
  _Clarge.resize(13);
  for (int iid=0;iid<13;++iid){
    int pdgid = (iid==6) ? 21 : iid-6;
    _Clarge[iid].resize(nlarge+1);
    for (unsigned int i=0;i<=nlarge;++i){
      double Llarge = i*_dLlarge;
      // L = log((1-x_trans)/(1-x))
      //   => log(1/x) = -log(1-(1-x_trans)*exp(-L))
      _Clarge[iid][i] = _find_max_probability(-log(1-(1-_transition_x)*exp(-Llarge)), pdgid);
    }
  }
}

/// return the max overhead factor for a parton of a given x and
/// pdgid, knowing that one probes ktscales below lnktmax
double ISROverheadFactor::_search_and_extend(double x, int pdgid){

  if (x<_transition_x){  // small-x region
    double L = log(_transition_x/x);

    // extend the reach if needed
    if (L>_Lsmall_max){
      unsigned int old_size = _Csmall[0].size();
      unsigned int new_size = (unsigned int)(L/_dLsmall+0.1)+1;
      for (int iid=0;iid<13;++iid){
        int local_pdgid = (iid==6) ? 21 : iid-6;
        _Csmall[iid].resize(new_size+1);
        for (unsigned int i=old_size;i<=new_size;++i){
          double Lsmall = i*_dLsmall; // log(1/(2x))
          _Csmall[iid][i] = _find_max_probability(Lsmall-_transition_logx, local_pdgid);
        }
      }
      _Lsmall_max = new_size*_dLsmall;
    }

    // get the max
    unsigned int n = (unsigned int)(L/_dLsmall);
    if (n+1>=_Csmall[0].size()) { n = _Csmall[0].size()-2; }

    unsigned int id = (pdgid==21) ? 6 : (unsigned int)(pdgid+6);
    //std::cout << "Exiting  ISROverheadFactor::search_and_extend_" << std::endl;
    // cout << "ISROverheadFactor::_search_and_extend: x="
    //      << x << ", pdgid=" << pdgid
    //      << " -> Cn=" << _Csmall[id][n]
    //      << ", Cn+1=" << _Csmall[id][n+1] << endl;
    return std::max(_Csmall[id][n], _Csmall[id][n+1]);
  }
  
  // large-x region
  double L = log((1-_transition_x)/(1-x));

  // extend the reach if needed
  if (L>_Llarge_max){
    unsigned int old_size = _Clarge[0].size();
    unsigned int new_size = (unsigned int)(L/_dLlarge+0.1)+1;
    for (int iid=0;iid<13;++iid){
      int local_pdgid = (iid==6) ? 21 : iid-6;
      _Clarge[iid].resize(new_size+1);
      for (unsigned int i=old_size;i<=new_size;++i){
        double Llarge = i*_dLlarge; // log(1/(2x))
        _Clarge[iid][i] = _find_max_probability(-log(1-(1-_transition_x)*exp(-Llarge)), local_pdgid);
      }
    }
    _Llarge_max = new_size*_dLlarge;
  }
  
  // get the max
  unsigned int n = (unsigned int)(L/_dLlarge);
  if (n+1>=_Clarge[0].size()) { n = _Clarge[0].size()-2; }
  
  unsigned int id = (pdgid==21) ? 6 : (unsigned int)(pdgid+6);    
  // cout << "ISROverheadFactor::_search_and_extend: x="
  //      << x << ", pdgid=" << pdgid
  //        << " -> Cn=" << _Clarge[id][n]
  //      << ", Cn+1=" << _Clarge[id][n+1] << endl;
  return std::max(_Clarge[id][n], _Clarge[id][n+1]);
}


/// compute the maximal probability for a given x0 (given as log(1/x0)), pdgid0, lnktmax
double ISROverheadFactor::_find_max_probability(double lnx0, int pdgid0) const {
  //, double lnktmax) const{

  // handle cases where we're beyond the max number of active
  // flavours in the PDF Set
  if ((pdgid0!=21) && (abs(pdgid0)>_hoppet_runner->pdf_max_active_flavours()))
    return 1.0;

  double x0 = exp(-lnx0);    

  // GPS tmp
  double tmp_verbose = false; //fabs(log(x0/0.03714)) < 0.08 && pdgid0 == -4;
  //if (pdgid0 == 3 && lnx0 >10.4) tmp_verbose = true;

  // a bunch of constants to try to avoid issues with rounding errors;
  // - the safety margin on lxmax helps avoid an issue where one is
  //   almost exactly on x=x0 and the probability is effectively 1 to
  //   within rounding errors, leading to inconsistent results from one
  //   machine to another (notably because the lk value that is
  //   associated with the maximum is machine-dependent, and this is
  //   then used for a subsequent iteration)
  constexpr double lnx_safety_margin = 1e-6;
  constexpr double lnkt_safety_margin = 1e-8;
  //constexpr double maxp_safety_margin = 100 * numeric_limits<double>::epsilon();

  double lxmin = lnx0*lnx_safety_margin;
  double lxmax = lnx0*(1-lnx_safety_margin);
  double lkmin = _hoppet_runner->lnkt_min() + lnkt_safety_margin;
  double lkmax = _hoppet_runner->lnkt_max() - lnkt_safety_margin;

  // special handling of damping conversions: if we have a massive quark
  // then the lnmuF that we supply here will get shifted to a higher
  // value (by get_massive_quark_damping_factor); we need to adjust
  // lkmax downwards to compensate for this, otherwise we won't be
  // exploring the full range
  constexpr double min_ln_offset = -6.0; // don't go below this relative to lnm
  lkmin = _hoppet_runner->massive_quark_damping_lnmuF_in(pdgid0, lkmin, min_ln_offset);

  if (tmp_verbose) cout << "lkmin=" << lkmin << ", lkmax=" << lkmax << " points_per_grid" << _points_per_grid << endl;

  double dx = (lxmax-lxmin)/(_points_per_grid-1);
  double dk = (lkmax-lkmin)/(_points_per_grid-1);

  unsigned int ixmax = 0, iktmax=0;
  double pmax=1.0;
  
  for (unsigned int istep=0; istep<_grid_iterations; ++istep){
    if (tmp_verbose) cout << "starting iteration " << istep << " with " 
                          << lxmin << " < lx < " << lxmax << " and " 
                          << lkmin << " < lk < " << lkmax << " and " 
                          << endl;
    // compute the grid spacing
    dx = (lxmax-lxmin)/(_points_per_grid-1);
    dk = (lkmax-lkmin)/(_points_per_grid-1);
    
    // find the max on a grid scan
    pmax = 0.0;
    for (unsigned int ikt=0;ikt<_points_per_grid;++ikt){
      double lnkt = lkmin + ikt*dk;
      // steps for handling mass thresholds:
      // - work out shifted_lnmuF (replacing lnkt, in pdf0 evaluation and get_weight)
      // - work out damping factor, and pass it to get_weight, so that get weight
      //   can include the factor for the quarks evolving back to gluons
    
      double massive_quark_damping_factor, lnmuF;
      _hoppet_runner->get_massive_quark_damping_factor(pdgid0, lnkt, massive_quark_damping_factor, lnmuF);
      double pdf0 = (*_hoppet_runner)(x0, lnmuF, pdgid0);
    
      for (unsigned int ix=0;ix<_points_per_grid;++ix){
        double x = exp(-(lxmin+ix*dx));
        double p = _get_weight(pdgid0, x0, pdf0, x, lnmuF, massive_quark_damping_factor);
        if (p>pmax){
          if (tmp_verbose) {
            cout << "updating max: x=" << x << ", log(x/x0) = " << log(x) + lnx0
                 << " lnkt=" << lnkt << " lnmuF=" << lnmuF 
                 << " p-1=" << p-1 << ", larger than previous pmax by " << p-pmax << endl;
          }
          pmax = p;
          ixmax = ix;
          iktmax = ikt;
        }
      }

    }

    // deduce the bounds for the next iteration
    if (istep+1<_grid_iterations){
      // deduce range fo next search
      if (ixmax==0){
        lxmax = lxmin+dx;
      } else if (ixmax==(_points_per_grid-1)){
        lxmin = lxmax-dx;
      } else {
        lxmin = (lxmin+(ixmax-1)*dx);
        lxmax = lxmin+2*dx;
      }

      if (iktmax==0){
        lkmax = lkmin+dk;
      } else if (iktmax==(_points_per_grid-1)){
        lkmin = lkmax-dk;
      } else {
        lkmin = (lkmin+(iktmax-1)*dk);
        lkmax = lkmin+2*dk;
      }

    }
  }

  return pmax;
}

/// returns the weight for a given initial particle to branch into a
/// given final particle
double ISROverheadFactor::_get_weight(double pdgid0, double x0, 
                                      Optional<double> pdf0_opt, 
                                      double x, double lnkt, 
                                      double massive_quark_damping_factor) const{

  // sort out optional values                                    
  double pdf0;
  if (pdf0_opt.has_value()) pdf0 = pdf0_opt.value();
  else                      pdf0 = (*_hoppet_runner)(x0, lnkt, pdgid0);

  // if pdf0 is <= 0, then we will saturate the weight                                
  if (pdf0 <= 0) return _hoppet_runner->max_ISR_overhead();

  // get the splitting parts
  //
  // x = x0/(1-z) => 
  double z = 1-x0/x;
  double w_rad_gluon, w_rad_quark;
  if (pdgid0 == 21){
    w_rad_gluon = _qcd.splitting_isr_zomzhalfPg2gg_normalised(z);
    w_rad_quark = _qcd.splitting_isr_zomzPq2gq_normalised(z);
  } else {
    w_rad_gluon = _qcd.splitting_isr_zomzPq2qg_normalised(z);
    w_rad_quark = _qcd.splitting_isr_zomzPg2qqbar_normalised(z);
  }      

  // get the PDFs
  PDF pdfs = (*_hoppet_runner)(x, lnkt);

  // convert into PDF factors
  double pdf_rad_gluon = pdfs.flav(pdgid0)/pdf0;
  double pdf_rad_quark = 0.0;
  if (pdgid0 == 21){
    // gluon can split at 3bar or 3 end. Take the max
    pdf_rad_quark = std::max(pdfs.sum_quarks(), pdfs.sum_antiquarks())/pdf0;
  } else { // quark emitter, backwards evolving to gluon
    pdf_rad_quark = pdfs.flav(21)/pdf0 * massive_quark_damping_factor;
  }

  // combine and print
  w_rad_gluon *= pdf_rad_gluon;
  w_rad_quark *= pdf_rad_quark;

  double weight = w_rad_gluon+w_rad_quark;
  if (weight > _hoppet_runner->max_ISR_overhead()) weight = _hoppet_runner->max_ISR_overhead();
  return weight;
}

/// output the ISROverheadFactor to the stream
std::ostream & ISROverheadFactor::output(std::ostream & ostr) {
  ostr << "# ISROverheadFactor" << endl
        << "# Columns: x pdf[-6] ... pdf[6]" << endl;
  int prec_save = ostr.precision(4);
  ostr.precision(4);

  // first do small x region
  int nxsmall = _Csmall[0].size();
  for (int ix = nxsmall-1; ix >= 0; ix--) {
    double x = 0.5 * exp(-ix * _dLsmall);
    ostr << setw(12) << x << " ";
    for (int iid = 0; iid < 13; iid++) ostr << setw(8) << _Csmall[iid][ix] << " ";
    ostr << endl;
  }

  // then do large x region
  int nxlarge = _Clarge[0].size();
  for (int ix = 0; ix < nxlarge; ix++) {
    double x = 1.0 - exp(-ix * _dLlarge)/2;
    ostr << setw(12) << x << " ";
    for (int iid = 0; iid < 13; iid++) ostr << setw(8) << _Clarge[iid][ix] << " ";
    ostr << endl;
  }

  ostr.precision(prec_save);
  return ostr;
}

} // namespace panscales
