#ifndef __EECAMBRIDGEONEMINUSCOSTHETA_HH__
#define __EECAMBRIDGEONEMINUSCOSTHETA_HH__

//FJSTARTHEADER
//
// Copyright (c) 2005-2018, Matteo Cacciari, Gavin P. Salam and Gregory Soyez
//
//----------------------------------------------------------------------
//  This file is distributed as part of PanScales but is HEAVILY based 
//  on a part of FastJet.
//
//  FastJet is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  The algorithms that underlie FastJet have required considerable
//  development. They are described in the original FastJet paper,
//  hep-ph/0512210 and in the manual, arXiv:1111.6097. If you use
//  FastJet as part of work towards a scientific publication, please
//  quote the version you use and include a citation to the manual and
//  optionally also to hep-ph/0512210.
//
//  FastJet is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with FastJet. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------
//FJENDHEADER

#include "Type.hh"
#include "fjcore_local.hh"

// forward declaration to reduce includes
namespace fjcore {
  class ClusterSequence;
}

#include "Momentum.hh"

//----------------------------------------------------------------------
/// @ingroup jet_algorithms
/// \class EECamBriefJet
///  class to help run the standard e+e- Cambridge algorithm 
/// (normally this would be hidden elsewhere, but it is
///  useful for our high precision Lund Plane implementation)
class EECamBriefJet {
public:
  void init(const fjcore::PseudoJet & jet) {
    precision_type modp2 = jet.modp2();
    if (modp2 > 0) {
      precision_type norm = 1.0/sqrt(modp2);
      precision_type nx = jet.px() * norm;
      precision_type ny = jet.py() * norm;
      precision_type nz = jet.pz() * norm;
      _jet_mom = panscales::Momentum(nx, ny, nz, 1, 0);
    } else {
      // occasionally (e.g. at end of clustering), we get a momentum
      // that has zero modp2. In that case, choose an arbitrary
      // direction (here, z): in cases where this happens it should
      // either not make any difference (end of clustering), or the
      // bevaviour of the algorithm was anyway arbitrary.
      _jet_mom = panscales::Momentum(0.0, 0.0, 1.0, 1, 0);
    }
  }

  precision_type distance(const EECamBriefJet * jet) const {
    precision_type dij = one_minus_costheta(_jet_mom,jet->_jet_mom);
    return dij;
  }

  precision_type beam_distance() const {
    return numeric_limit_extras<precision_type>::max();
  }

private:
  panscales::Momentum _jet_mom;
};

//----------------------------------------------------------------------
//
/// @ingroup plugins
/// \class EECambridgeOneMinusCosTheta
/// Implementation of the e+e- Cambridge algorithm (plugin for fastjet v2.4 upwards)
///
/// EECambridgeOneMinusCosTheta is a plugin for fastjet (v2.4 upwards)
/// It implements the Cambridge algorithm, as defined in 
/// 
/// Better jet clustering algorithms
/// Yuri Dokshitzer, Garth Leder, Stefano Moretti,  Bryan Webber 
/// JHEP 9708 (1997) 001
/// http://www-spires.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+j+JHEPA%2C9708%2C001
///
/// On construction one must supply a ycut value.
///
/// To get the jets at the end call ClusterSequence::inclusive_jets();
class EECambridgeOneMinusCosTheta : public fjcore::JetDefinition::Plugin {
public:
  /// Main constructor for the EECambridgeOneMinusCosTheta Plugin class.  
  /// It takes the dimensionless parameter ycut (the Q value for normalisation
  /// of the kt-distances is taken from the sum of all particle energies).
  EECambridgeOneMinusCosTheta (precision_type ycut_in) : _ycut(ycut_in) {}

  /// copy constructor
  EECambridgeOneMinusCosTheta (const EECambridgeOneMinusCosTheta & plugin) {
    *this = plugin;
  }

  // the things that are required by base class
  virtual std::string description () const override;
  virtual void run_clustering(fjcore::ClusterSequence &) const override;

  precision_type ycut() const {return _ycut;}

  /// the plugin mechanism's standard way of accessing the jet radius.
  /// This must be set to return something sensible, even if R
  /// does not make sense for this algorithm!
  virtual precision_type R() const override {return precision_type(1.0);}

  /// avoid the warning whenever the user requests "exclusive" jets
  /// from the cluster sequence
  virtual bool exclusive_sequence_meaningful() const override {return true;}

  /// returns true because this plugin is intended for spherical
  /// geometries (i.e. it's an e+e- algorithm).
  virtual bool is_spherical() const override {return true;}

private:
  precision_type _ycut;
};
//
#endif // __EECAMBRIDGEONEMINUSCOSTHETA_HH_
//
