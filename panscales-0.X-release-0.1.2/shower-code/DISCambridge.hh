#ifndef __DISCAMBRIDGE_HH__
#define __DISCAMBRIDGE_HH__

//FJSTARTHEADER
//
// Copyright (c) 2005-2018, Matteo Cacciari, Gavin P. Salam and Gregory Soyez
//
//----------------------------------------------------------------------
//  This file is distributed as part of PanScales but is HEAVILY based 
//  on a part of FastJet.
//
//  FastJet is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  The algorithms that underlie FastJet have required considerable
//  development. They are described in the original FastJet paper,
//  hep-ph/0512210 and in the manual, arXiv:1111.6097. If you use
//  FastJet as part of work towards a scientific publication, please
//  quote the version you use and include a citation to the manual and
//  optionally also to hep-ph/0512210.
//
//  FastJet is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with FastJet. If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------
//FJENDHEADER

#include "Type.hh"
#include "fjcore_local.hh"
// forward declaration to reduce includes
namespace fjcore {
  class ClusterSequence;
}
#include "Momentum.hh"

//----------------------------------------------------------------------
/// @ingroup jet_algorithms
/// \class DISCamBriefJet
/// class to help run a DIS Cambridge algorithm
///
/// It bears many similarities to the EECambridgeOneMinusCosTheta
/// version and should arguably be derived from it. But to help 
/// make the algorithm clear, we spell it all out here
class DISCamBriefJet {
public:
  void init(const fjcore::PseudoJet & jet) {
    precision_type modp2 = jet.modp2();
    if (modp2 > 0) {
      precision_type norm = 1.0/sqrt(modp2);
      precision_type nx = jet.px() * norm;
      precision_type ny = jet.py() * norm;
      precision_type nz = jet.pz() * norm;
      _jet_mom = panscales::Momentum(nx, ny, nz, 1, 0);
    } else {
      // occasionally (e.g. at end of clustering), we get a momentum
      // that has zero modp2. In that case, choose an arbitrary
      // direction (here, z): in cases where this happens it should
      // either not make any difference (end of clustering), or the
      // bevaviour of the algorithm was anyway arbitrary.
      _jet_mom = panscales::Momentum(0.0, 0.0, 1.0, 1, 0);
    }
    // From the point of view of calculating the angular distances
    // we just need a massless unit-length vector 
    // _sgn_beam_direction = sgn_beam_direction;
  }

  precision_type distance(const DISCamBriefJet * jet) const {
    // precision_type dij = 1 - nx*jet->nx
    //                - ny*jet->ny
    //                - nz*jet->nz;
    precision_type dij = one_minus_costheta(_jet_mom,jet->_jet_mom);
    return dij;
  }

  precision_type beam_distance() const {
    // beam direction chosen with _sgn_beam_direction
    // We want 1 - cos(theta_iz), which is 
    // 1 - nz. If nz is too close to one, 
    // we switch 
    precision_type diB = 1.0 - _sgn_beam_direction * _jet_mom.pz();
    if (diB < numeric_limit_extras<precision_type>::sqrt_epsilon()) {
      // this is approximate, i.e. does not have the same complete 
      // accuracy of the normal one_minus_costheta function
      diB = one_minus_costheta(panscales::Momentum(0.0, 0.0, _sgn_beam_direction, 1, 0), _jet_mom);
    }
    return diB;
  }

private:
  panscales::Momentum _jet_mom;
  int _sgn_beam_direction = -1; 
};

//----------------------------------------------------------------------
//
/// @ingroup plugins
/// \class DISCambridge
/// Implementation of a DIS version of the Cambridge algorithm (plugin for fastjet v2.4 upwards)
///
/// It is inspired from the e+e- Cambridge version
/// 
/// Better jet clustering algorithms
/// Yuri Dokshitzer, Garth Leder, Stefano Moretti,  Bryan Webber 
/// JHEP 9708 (1997) 001
/// http://www-spires.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+j+JHEPA%2C9708%2C001
///
/// On construction one must supply a dimensionful dcut value (but as of 2022-05-14, GPS is still 
/// thinking this through)
///
/// To get the jets at the end call ClusterSequence::inclusive_jets();
class DISCambridge : public fjcore::JetDefinition::Plugin {
public:
  /// Main constructor for the DISCambridge Plugin class.
  ///
  /// The algorithm runs in two modes
  ///
  /// - dcut >= 0: dcut is a parameter with dimensions of squared energy that acts
  ///   like the ycut of the e+e- Cambridge algorithm. To access the jets, one
  ///   should use inclusive_jets(), and all inclusive_jets() are perturbatively good jets,
  //    but not all particles are in jets.
  ///
  /// - dcut <  0: the exact value of dcut is ignored and the algorithm works more 
  ///   like an inclusive Aachen type algorithm. That means that all particles end
  ///   up in the inclusive_jets, and only those jets j formed with with 
  ///   2Ej^2(1-cos theta_jz)>threshold are perturbatively safe to use. 
  ///
  ///   This version is probably the safest for Lund diagram type studies and for determining
  ///   the kt^2 scale of the hardest emission (take the 2nd largest of the )
  /// 
  /// Note that in both cases the dij values in the cluster sequence always include
  /// the min(Ei^2,Ej^2) or Ei^2 factor.
  DISCambridge (precision_type dcut_in) : _dcut(dcut_in) {}

  /// copy constructor
  DISCambridge (const DISCambridge & plugin) {
    *this = plugin;
  }

  // the things that are required by base class
  virtual std::string description () const override;
  virtual void run_clustering(fjcore::ClusterSequence &) const override;

  precision_type dcut() const {return _dcut;}

  /// set the sign of the z component of the beam
  /// default is assumed in the positive z direction
  void set_beam_direction(int sgn){
    _beam_direction = sgn;
  }
  int beam_direction() const {
    return _beam_direction;
  }

  /// the plugin mechanism's standard way of accessing the jet radius.
  /// This must be set to return something sensible, even if R
  /// does not make sense for this algorithm!
  virtual precision_type R() const override {return precision_type(1.0);}

  /// avoid the warning whenever the user requests "exclusive" jets
  /// from the cluster sequence
  virtual bool exclusive_sequence_meaningful() const override {return false;}

  /// returns false because this plugin is not intended for spherical
  /// geometries (i.e. it's a DIS algorithm).
  virtual bool is_spherical() const override {return false;}

private:
  precision_type _dcut;
  int _beam_direction = 1;
};

/// returns jets sorted by decreasing DIS diB, as registered in the CS
/// The jets _must_ be associated with the CS
std::vector<fjcore::PseudoJet> sorted_by_DISdiB(const std::vector<fjcore::PseudoJet> & jets);
//
#endif // __DISCAMBRIDGE_HH_
//
