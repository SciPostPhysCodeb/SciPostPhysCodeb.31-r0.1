//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "AnalysisFramework.hh"
#include "ShowerRunner.hh"
#include "fjcore_local.hh"
#include <sstream>

using namespace std;
using namespace fjcore;
using namespace panscales;

//----------------------------------------------------------------------
// Code to examine the boson and leading jet pt in pp->Z events.
//
// example of command line for pp->Z (Drell-Yan) at fixed rapidity:
/*
   ./build-double/example-pp -process pp2Z -shower panglobal -beta 0 -mZ 91.1876 -yZ 0.5 \
     -physical-coupling -lnvmax 4.51292 -rts 13600 -nev 100000 \ 
     -out example-results/example-pp.dat
*/
// [this should run in a few tens of seconds]
//
class ExamplePP : public AnalysisFramework {
public:
  /// ctor, uses the default constructor of AnalysisFramework
  /// in there, the setup of the showers and event is handled
  ExamplePP(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in) { 
    // check that we are running with the correct process
    if (!dynamic_cast<Processpp2Z*>(f_process.get()) && !cmdline->help_requested()){
      throw runtime_error("This analysis is only to be used with a pp->Z process");
    }
  }
  //----------------------------------------------------------------------
  // options needed by the "user" analysis can be set here
  void user_startup() override {

      // define cumulative spectrum for boson and leading jet pt
      double mZ = -1;
      if (dynamic_cast<Processpp2Z*>(f_process.get()))
         mZ = dynamic_cast<Processpp2Z*>(f_process.get())->mZ();
      Binning pt_binning(0.0, mZ/3, 50);
      cumul_hists_err["boson.pt"  ].declare(pt_binning);
      cumul_hists_err["jet.pt"    ].declare(pt_binning);

      // declare clustering tools
      cmdline->start_section("Analysis-specific options");
      double R = cmdline->value("-R",0.4).help("jet radius").argname("R");
      cmdline->end_section("Analysis-specific options");
      _jet_def = JetDefinition(antikt_algorithm, R);

  }
  //----------------------------------------------------------------------
  // print the jet definition in the header of the output file
  void user_post_startup() override {
      header << "# jet_def = " << _jet_def.description() << endl;
  }

  //----------------------------------------------------------------------
  // this gets called once for every event and should carry
  // out the analysis and output histograms
  void user_analyse_event() override {

      double evwgt = event_weight();
      precision_type pTX = f_event.particles()[2].pt(); // boson is particle #2 always

      // Fill the histogram
      cumul_hists_err["boson.pt"].add_entry(to_double(pTX), evwgt);

      // run the jet finding excluding the incoming legs and the boson
      vector<PseudoJet> fj_particles;
      for (unsigned int i=3; i<f_event.particles().size(); ++i){
          const Particle & p = f_event.particles()[i];
          PseudoJet pfj = PtYPhiM(p.pt(), p.rap(), p.phi(), p.m());
          fj_particles.push_back(pfj);
      }

      auto jets = _jet_def(fj_particles);
      // select the leading jet pt
      precision_type jet_pt = jets.size() > 0 ? jets[0].pt() : numeric_limits<precision_type>::min(); 
      if (jets.size() > 0) {
          cumul_hists_err["jet.pt"].add_entry(to_double(jet_pt), evwgt);
      }

  }
  
protected:
  JetDefinition _jet_def; ///< the jet definition used for clustering

};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv);
  ExamplePP driver(&cmdline);
  driver.run();
}
