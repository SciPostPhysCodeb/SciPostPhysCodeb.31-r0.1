//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __ShowerPanScaleLocalDIS_HH__
#define __ShowerPanScaleLocalDIS_HH__

#include "ShowerPanScaleDISBase.hh"

namespace panscales{
  
//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerPanScaleLocalDIS
/// the PanScales shower w local recoil (dipole-variant) for DIS and VBF processes.
/// arxiV:2305.08645. Note that VBF is treated as two DIS chains. 
class ShowerPanScaleLocalDIS : public ShowerPanScaleDISBase {
public:

  /// default ctor for the Panscale Showers
  ShowerPanScaleLocalDIS(const QCDinstance & qcd, double beta = 0.5)
    : ShowerPanScaleDISBase(qcd, beta) {}

  /// virtual dtor
  virtual ~ShowerPanScaleLocalDIS() {}

  /// implements the element and splitting info as subclasses
  class Element;    //< base class
  class ElementIF;  //< handles an element w I emitter and F spectator
  class ElementFI;  //< handles an element w F emitter and I spectator
  class ElementFF;  //< handles an element w F emitter and F spectator
  class EmissionInfo;
  virtual ShowerBase::EmissionInfo* create_emission_info() const override;

  /// description of the class
  std::string description() const override { 
    std::ostringstream ostr;
    ostr << name() << " dipole shower with beta = " << _beta;
    return ostr.str();
  }
  /// name of the shower
  std::string name() const override{return "PanScaleLocal-DIS";}

  /// returns true if the shower is global 
  bool is_global() const override { return false; }
 
  /// this is a dipole shower (2 elements per dipole)
  unsigned int n_elements_per_dipole() const override { return 2; }

  /// element store
  virtual std::vector<std::unique_ptr<ShowerBase::Element> > elements(Event & event, int dipole_index) const override;

  // the only case where the check after the acceptance probability
  // is the IF case. We check it separately, do_kinematics will be true
  virtual bool do_kinematics_always_true_if_accept() const override {return true;}

  /// this is a dipole shower, so both the emitter and spectator
  /// can split
  bool only_emitter_splits() const override {return true;}
};


//--------------------------------------------------------------
/// \class ShowerPanScaleLocalDIS::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs for the ShowerPanScaleLocalDIS
class ShowerPanScaleLocalDIS::Element : public ShowerPanScaleBase::Element {
public:
  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleLocalDIS * shower,
	  bool additive_branching = true) :
    ShowerPanScaleBase::Element(emitter_index, spectator_index, dipole_index, event, shower),  _shower(shower){
    update_kinematics();
  }

  //--------------------------------------------------
  // basic information
  //--------------------------------------------------
  /// Approximate calculation of the log of the kT of the emission relative
  /// to its parents in their rest frame, (eq. 57 of logbook 2019-07-02).
  const double alphas_lnkt(double lnv, double lnb) const override {
  /// MB-note:
  /// check this:
    precision_type lnkt = _log_rho + lnv + _shower->_beta*fabs(lnb);
    return to_double(lnkt);
  }


  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  /// the lnb total extent, as a function of lnv, is parametrised
  /// as  const + lnv_deriv * lnv.
  virtual double lnb_extent_const() const override = 0;

  virtual double lnb_extent_lnv_coeff() const override {return -2.0/(1. + _shower->_beta);}

  virtual Range lnb_generation_range(double lnv) const override = 0;
  
  virtual bool has_exact_lnb_range() const override {return false;}

  virtual double lnv_lnb_max_density() const override {
    return _shower->max_alphas() * _shower->qcd().CA() / M_PI;
  }
  /// jacobian for this shower is not computed
  precision_type dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const override {
    throw std::runtime_error("Tried to access the exact jacobian an element of shower " + _shower->name() + ", which is not implemented");
    return 0.;
  }
  /// this function should not be accessed by any function
  void get_emit_spec_rad_kinematics_for_matching(typename ShowerBase::EmissionInfo * emission_info, Momentum & emit, Momentum & spec, Momentum & rad, bool include_boost = false) const override{
    throw std::runtime_error("Tried to access kinematics for matching of the shower " + _shower->name() + ", for which matching is not implemented");
  }



  //--------------------------------------------------
  // important definitions to be implemented in all derived classes
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override = 0;

  // goes in the derived classes (only needed for IF)
  virtual bool check_after_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override = 0;

  /// carry out the splitting and update the event; return true if
  /// successful; the RotatedPieces pass on a potentially rotated version
  /// of the emitter, spectator, and perpendicular vectors, and these
  /// are the ones that should be used, not those from emission_info; 
  /// the output is stored back in emission_info (possibly still rotated)
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override = 0;

  /// check whether this element is the same as the one passed as
  /// argument. This is helper to debug potential issues.
  virtual bool check_equal_to(const ShowerBase::Element *other_base) const override{
    const typename ShowerPanScaleLocalDIS::Element & other = *(static_cast<const typename ShowerPanScaleLocalDIS::Element*>(other_base));
    double precision     = 1e-10;
    bool check_dipole_m2 = std::abs(other._dipole_m2 - _dipole_m2) < precision;
    bool check_dipole_si = std::abs(other._sitilde - _sitilde)     < precision;
    bool check_dipole_sj = std::abs(other._sjtilde - _sjtilde)     < precision;
    bool return_true = true;
    if(!check_dipole_m2){
      std::cout << "Dipole mass does not agree;   stored: " << other._dipole_m2 << " actual: " << _dipole_m2 << std::endl;
      return_true = false;
    }
    if(!check_dipole_si){
      std::cout << "sitilde does not agree;   stored: " << other._sitilde << " actual: " << _sitilde << std::endl;
      return_true = false;
    }
    if(!check_dipole_sj){
      std::cout << "sjtilde does not agree;   stored: " << other._sjtilde << " actual: " << _sjtilde << std::endl;
      return_true = false;
    }
    return return_true;
  }

protected:
  /// lorentz boost to perform on all partons in the DIS chain
  void boost_all_in_chain(typename ShowerBase::EmissionInfo * emission_info_base, int dis_chain) const;

  const ShowerPanScaleLocalDIS * _shower;
};

//--------------------------------------------------
/// \class ShowerPanScaleLocalDIS::ElementIF
/// implementation of the elements for IF splittings  
class ShowerPanScaleLocalDIS::ElementIF : public ShowerPanScaleLocalDIS::Element {
public:
  /// full ctor w initialisation
  ElementIF(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleLocalDIS * shower) :
    ShowerPanScaleLocalDIS::Element(emitter_index, spectator_index, dipole_index, event, shower){}

  /// generation ranges 
  virtual double lnb_extent_const()     const override;
  virtual Range lnb_generation_range(double lnv) const override;

  /// return the acceptance probability for the given kinematic point
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// for the IF class we need this function, for the others it needs to return true
  virtual bool check_after_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// carry out the splitting and update the event
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;

  /// do the final boost
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

};

//--------------------------------------------------
/// \class ShowerPanScaleLocalDIS::ElementFI
/// implementation of the elements for FI splittings    
class ShowerPanScaleLocalDIS::ElementFI : public ShowerPanScaleLocalDIS::Element {
public:
  /// full ctor w initialisation
  ElementFI(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleLocalDIS * shower) :
    ShowerPanScaleLocalDIS::Element(emitter_index, spectator_index, dipole_index, event, shower){}

  /// generation ranges
  virtual double lnb_extent_const()     const override;
  virtual Range lnb_generation_range(double lnv) const override;

  /// return the acceptance probability for the given kinematic point
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// returns false only for IF
  bool check_after_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override { return true;}

  /// carry out the splitting and update the event
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;

  /// update the chain information
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

};

//--------------------------------------------------
/// \class ShowerPanScaleLocalDIS::ElementFF
/// implementation of the elements for FF splittings   
class ShowerPanScaleLocalDIS::ElementFF : public ShowerPanScaleLocalDIS::Element {
public:
  /// full ctor w initialisation
  ElementFF(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleLocalDIS * shower) :
    ShowerPanScaleLocalDIS::Element(emitter_index, spectator_index, dipole_index, event, shower){}

  /// exact range available for the FF case
  virtual double lnb_extent_const() const override;
  virtual Range lnb_generation_range(double lnv) const override;

  /// return the acceptance probability for the given kinematic point
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// returns false only for IF
  bool check_after_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override { return true;}

  /// carry out the splitting and update the event
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info, const RotatedPieces & rp) const override;

  /// update the chain information
  virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
                            typename ShowerBase::EmissionInfo * emission_info) override;

};

/// \class ShowerPanScaleLocalDIS::EmissionInfo
/// emission information specific to the ShowerPanScaleLocalDIS shower
class ShowerPanScaleLocalDIS::EmissionInfo : public ShowerPanScaleBase::EmissionInfo {
  public:
    // needed in the PanScaleLocal shower
    precision_type kappat, alphak, betak;
    precision_type ak, bk, ai, bi, bj;
    Momentum pf_new;
};

} // namespace panscales

#endif // __ShowerPanScaleLocalDIS_HH__

