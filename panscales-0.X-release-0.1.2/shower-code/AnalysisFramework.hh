//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __ANALYSISFRAMEWORK_HH__
#define __ANALYSISFRAMEWORK_HH__


#include <chrono>
#include <memory>

#include "AnalysisBase.hh"
#include "Type.hh"
#include "Event.hh"
#include "Collider.hh"
// #TRK_ISSUE-12 todo: can we have a fwd declaration instead of the full header
#include "ShowerRunner.hh"
#include "LimitedWarning.hh"
#include "ProcessHandler.hh"
#include "Optional.hh"

namespace panscales{

//--------------------------------------------------------------------
/// @ingroup analysis_base_classes
/// \class AnalysisFramework
/// Class that holds everything to do with running a shower;
/// The user should then derive from this to carry out a specific analysis.
class AnalysisFramework : public  AnalysisBase {
public:
  AnalysisFramework(CmdLine * cmdline_in, const string & default_output_filename = "", bool return_after_git_info = false);

  /// this will take care of any required caching, etc.
  void post_startup() override;
  /// this will take care of any pre-run initialisations
  void pre_run() override;
  /// This gets called by the base class nev times
  bool generate_event() override;
  /// this will get used for finishing any timing
  void post_analyse_event() override;

  void generator_output(std::ostream & ostr) override;
  
  ~AnalysisFramework() override;

  panscales::ShowerRunner & shower_runner() {return *f_shower_runner;};
  panscales::HoppetRunner & hoppet_runner() {return *f_hoppet_runner;};

protected:
  // all new framework variables will be prefixed by f_
  // (those in the base class do not currently have a prefix)
  double f_rts;
  double f_lnvmax, f_lnvmin, f_lnvrange;
  int    f_max_emsn;
  bool   f_first_order;
  bool   f_second_order;
  std::string f_second_order_injection;
  vector<panscales::Injection> f_second_order_injection_vec;
  int    f_tree_level;
  double f_second_order_lnb_window;
  double f_dynamic_lncutoff;
  bool   f_weighted_generation;
  unsigned f_nev_cache;
  // options connected with Sudakov cache
  bool   f_auto_lnv;
  unsigned f_n_sudakov_cache;
  double f_lnsudakov_spacing;
  uint64_t f_max_print;

  panscales::IntegrationStrategy f_integration_strategy;

  std::shared_ptr<panscales::ShowerRunner> f_shower_runner;
  panscales::Event f_event_base, f_born_event, f_event;
  std::unique_ptr<panscales::Collider> f_collider;
  std::unique_ptr<panscales::ProcessBase> f_process;

  //------ things related to PDFs
  std::shared_ptr<panscales::HoppetRunner> f_hoppet_runner;
  // /// if true, then a basic summary of the PDFs will be dumped to output_filename.pdfs
  // bool                    _opt_dump_pdfs = false;
  // HoppetRunner::PDFChoice _opt_pdf_choice = panscales::HoppetRunner::ToyNf5;
  // std::string             _opt_lhapdf_set = "";
  // Optional<double>        _opt_pdf_max_reliable_x;
  // /// if using a PDF choice with remapping, then this specifies the value
  // /// of lnQ in the shower that is matched to hoppet's Qmax
  // Optional<double>        _opt_pdf_lnQref;
  // Optional<double>        _opt_hoppet_Qmax;

  //------ things related to matchings
  panscales::MatchingScheme f_matching_scheme;
  std::unique_ptr<panscales::MatchingHardMatrixElement> f_3jet_me;
  std::unique_ptr<panscales::MatchedProcess> f_3jet_matching;
  panscales::ShowerRunner::MCatNLOIntegral f_mcatnlo_integral;
  bool f_mcatnlo_hard_event = false;
  
  //-------- things related to timing -----------
  bool   f_timingInfo;
  std::chrono::time_point<std::chrono::high_resolution_clock> f_evt_gen_start;
  std::chrono::time_point<std::chrono::high_resolution_clock> f_evt_gen_end;
  std::chrono::time_point<std::chrono::high_resolution_clock> f_evt_ana_start;
  std::chrono::time_point<std::chrono::high_resolution_clock> f_evt_ana_end;

  // ------- things related to the GSL state
  bool f_save_state_on_fail; 
  std::string f_gsl_save_state_file;
  std::string f_gsl_get_state_file;
  int64_t f_save_state_before_iev = -1;
  
  //---------------------------
  // we could refer to these directly in the code as averages["multiplicity_unweighted"]
  // etc, but there's a penalty associated with this; declaring a reference
  // resolves the problem.
  AverageAndErrorWithRef & multiplicity_unweighted = averages["multiplicity_unweighted"];
  AverageAndErrorWithRef & multiplicity_weighted   = averages["multiplicity_weighted"];
  AverageAndError        & xsection_total        = xsections["xsc_total"];
  AverageAndError        & xsection_positive     = xsections["xsc_positive"];
  AverageAndError        & xsection_negative     = xsections["xsc_negative"];
  AverageAndError        & xsection_mcatnlo_hard = xsections["xsc_mcatnlo_hard"];
  AverageAndErrorWithRef & t_evgen_ms = averages["t_evgen_ms"];
  AverageAndErrorWithRef & t_evana_ms = averages["t_evana_ms"];
  DefaultHistWithError & lnmult_hist = hists_err["ln_multiplicity"];
  DefaultHistWithError * weight_hist = nullptr;

  static panscales::LimitedWarning _badMom;

};

} // end namespace panscales
#endif // __ANALYSISFRAMEWORK_HH__
