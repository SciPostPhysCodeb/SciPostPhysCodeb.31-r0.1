//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __MOMENTUM_HH__
#define __MOMENTUM_HH__

#include "config.hh"

#if PANSCALES_MOMENTUM == 4

#include "Momentum4.hh"
namespace panscales{
  typedef Momentum4<precision_type> Momentum;
} // namespace panscales

#elif PANSCALES_MOMENTUM == 2

#include "MomentumM2.hh"
namespace panscales{
  typedef MomentumM2<precision_type> Momentum;
  typedef LorentzBoostGen<MomentumM2<precision_type>> LorentzBoost;
} // namespace panscales

#else

#error Did not recognise value for momentum choice: PANSCALES_MOMENTUM

#endif // PANSCALES_MOMENTUM check

#endif //  __MOMENTUM_HH__
