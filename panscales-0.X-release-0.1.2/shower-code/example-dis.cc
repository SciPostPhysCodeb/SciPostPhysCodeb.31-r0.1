//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "AnalysisFramework.hh"
#include <iomanip>
#include <algorithm>

using namespace std;
using namespace panscales;

//----------------------------------------------------------------------
/// Code to examine a few event shapes in DIS events.
///
/// example of command line is
/**
    ./build-double/example-dis -process DIS -shower panlocal -beta 0.5 \
      -Q2 8300 -y 0.2 -rts 320 -physical-coupling \
      -nev 100000 -out example-results/example-dis.dat
*/
/// This should run in a few seconds.
class ExampleDIS : public  AnalysisFramework {
public:
  /// ctor, uses the default constructor of AnalysisFramework
  /// in there, the setup of the showers and event is handled
  ExampleDIS(CmdLine * cmdline_in)
    : AnalysisFramework(cmdline_in) {
      // check that we are running with the correct process
      if (!dynamic_cast<ProcessDIS*>(f_process.get()) && !cmdline_in->help_requested()){
        throw runtime_error("This analysis is only to be used with a DIS process");
      }
    }

  /// options needed by the "user" analysis can be set here
  /// here, we set up the jet definition and declare the histogram binnings
  void user_startup() override {
    // get the minimal energy fraction for computing the observables
    cmdline->start_section("Analysis-specific options");
    _min_energy_fraction = cmdline->value<double>("-min-energy-fraction", 0.1).help("Minumum energy fraction in terms of Q^2 energy in the current hemisphere should be");

    // declare histograms and binning
    int nbins = cmdline->value<int>("-nbins", 50).help("Number of bins for the observable outputs");
    
    cmdline->end_section("Analysis-specific options");
    // we define all the observables in the current hemisphere (see e.g. 0512014v1)
    cumul_hists_err["evshp:tau_z"].declare(0, 1, 1./nbins);           // thrust wrt to z axis
    cumul_hists_err["evshp:B_z"].declare(0, 0.5, 0.5/nbins);          // max of broadening (wrt z axis) is 0.5
    cumul_hists_err["evshp:tau_wrt_axis"].declare(0, 0.5, 0.5/nbins); // max of thrust wrt thrust axis is 0.5
    cumul_hists_err["evshp:rho"].declare(0, 0.25, 0.25/nbins);        // max of jet mass (rho) is 0.25
    cumul_hists_err["evshp:CParam"].declare(0, 1, 1./nbins);          // C parameter
    // for the maximum of Qt get the invariant mass of the DIS system
    
    double Q2 = -1;
    if(dynamic_cast<ProcessDIS*>(f_process.get()))
      Q2 =  dynamic_cast<ProcessDIS*>(f_process.get())->Q2_dis();
    cumul_hists_err["Qt"].declare(0, sqrt(Q2)/2, sqrt(Q2)/(2*nbins)); // Qt variable (hep-ph/0606285)
  }

  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  /// here we calculate the observables declared above
  /// in the base class AnalysisFramework we also automatically
  /// keep track of timing info and the multiplicity of the event
  void user_analyse_event() override {
    // we work in the Breit frame, so we may define the
    // current hemisphere as the -z or +z definition
    // we also need to have the definition of the current 
    // hemisphere, so we need to get the photon
    // in the way the events are setup this is the first
    // particle in the event
    const Particle & photon_momentum = f_event.particles()[0];
    if(photon_momentum.pdgid() != 22)
      throw std::runtime_error("The first particle in the event is not a photon");
    
    // then we construct some of the observables from 0512014v1
    // - thrust (eqn 1)
    // - broadening w.r.t z (eqn 3)
    // - jet mass rho (eqn 4), for which we need to total momentum in the current hemisphere
    // all these are normalised
    precision_type thrust = 0., B_z = 0., rho = 0.;
    Momentum p_sum_current_hemisphere; //< also needed for Q_t
    // these observables are normalised to sum |p|
    precision_type sum_p_abs = 0.;
    // events are only accepted if the energy in the current hemisphere
    // is larger than some limit, which in 0512014v1 is Q/10.
    precision_type sum_energy = 0.;
    
    // for the thrust w.r.t. the thrust axis and for
    // c-parameter, we need again access to the
    // momenta in the current hemisphere
    // to avoid looping multiple times over the event
    // we store the set of current hemisphere final-state
    // momenta here
    std::vector<MomentumM2<precision_type>> final_state_momenta_in_current_hemisphere;
    // build these quantities by looping over the event
    for(const auto & p : f_event.particles()) {
      // if it is an initial state we continue
      if(p.is_initial_state()) continue;
      // check if the particles are in the current hemisphere
      // if not, we can continue
      if(!_in_current_hemisphere(p.momentum(), photon_momentum)) continue;
      // otherwise build the observables / sums
      sum_p_abs  += p.modp();
      sum_energy += p.E();
      thrust     += abs(p.pz());
      B_z        += p.pt();
      p_sum_current_hemisphere += p;
      // and push back the momentum itself
      final_state_momenta_in_current_hemisphere.push_back(p.momentum());
    }
    // we only store the events if the energy in the current
    // hemisphere exceeds a threshold
    precision_type Q_DIS          = sqrt(f_event.Q2());
    precision_type minimal_energy = Q_DIS*_min_energy_fraction;
    if(sum_energy < minimal_energy) return;

    // now normalise thrust and broadening
    thrust /= sum_p_abs;
    B_z    /= (2*sum_p_abs);
    // for rho we need to calculate the jet mass of the current hemisphere
    precision_type m2_in_hemisphere = p_sum_current_hemisphere.m2();
    // build in a safe for when the mass is evaluated to -0 (otherwise underflow bin gets filled)
    if(m2_in_hemisphere < 0){
      if(m2_in_hemisphere > -std::numeric_limits<precision_type>::epsilon()) m2_in_hemisphere = 0.;
      else{
        std::cout.precision(16);
        std::cout << "m2 = " << m2_in_hemisphere << std::endl;
        throw std::runtime_error("rho (jet mass in current hemisphere) is negative");
      }
    }
    rho     = m2_in_hemisphere / pow2((2 * sum_p_abs));

    // add this information to the histograms
    cumul_hists_err["evshp:tau_z"].add_entry(to_double(1 - thrust), event_weight());
    cumul_hists_err["evshp:B_z"].add_entry(to_double(B_z), event_weight());
    cumul_hists_err["evshp:rho"].add_entry(to_double(rho), event_weight());
    
    // compute the Q_t observable
    precision_type Qt = p_sum_current_hemisphere.pt();
    cumul_hists_err["Qt"].add_entry(to_double(Qt), event_weight()); //< https://arxiv.org/pdf/hep-ph/0606285.pdf
    
    // now get thrust w.r.t. thrust axis (eqn 2 of 0512014v1)
    precision_type thrust_wrt_t = _get_thrust_wrt_thrust_axis(final_state_momenta_in_current_hemisphere);
    thrust_wrt_t /= sum_p_abs;
    cumul_hists_err["evshp:tau_wrt_axis"].add_entry(to_double(1 - thrust_wrt_t), event_weight());

    // then we calculate the C parameter (eqn 5 of 0512014v1)
    // note there is a mistake in the preprint in the defn of C
    // this sums over all partons in the current hemisphere only
    precision_type C_param = _calculate_C_parameter(final_state_momenta_in_current_hemisphere);
    // now normalise it (do not include a factor of 2 because the sum over particle pairs is only completed half in above)
    C_param *= 3./pow2(sum_p_abs);
    // and bin it
    cumul_hists_err["evshp:CParam"].add_entry(to_double(C_param), event_weight());
  }

private:
  // minimal energy fraction for storing the observables
  double _min_energy_fraction;
  // set the precision for the thrust axis calculation
  precision_type _thrust_axis_precision = sqrt(std::numeric_limits<precision_type>::epsilon());


  /// small function that tells us whether a particle is in the current (true)
  /// or remnant (false) hemisphere given the photon momentum
  bool _in_current_hemisphere(const Momentum & p, const Particle & photon_momentum){
    if(photon_momentum.pz() < 0) return p.rap() < 0;
    else                         return p.rap() > 0;
  }

  /// calculation of the thrust axis// get the thrust axis over the sum of all final-state partons that are in the current hemisphere
  /// function returns the unnormalised thrust
  precision_type _get_thrust_wrt_thrust_axis(std::vector<MomentumM2<precision_type>> & final_state_momenta){ //< no const because there is a sort that will change it
    // declare the unnormalised thrust variable
    precision_type unnormalised_thrust = 0.;
    // declare the thrust axis
    Momentum thrust_axis(0.,0.,0.,0.);
    // get the number of final-state partons
    unsigned int n_fs = final_state_momenta.size();
    // if there is only one parton - the trust axis is equal to the three momentum of that parton
    // and the trust is equal to 1
    if (n_fs == 1){
      thrust_axis = final_state_momenta[0]/final_state_momenta[0].modp(); 
      unnormalised_thrust = final_state_momenta[0].modp();
      return unnormalised_thrust;
    }
    // otherwise first sort the vectors in terms of their modp()
    std::sort(final_state_momenta.begin(), final_state_momenta.end(), [](const Momentum & p1, const Momentum & p2) {
      return p1.modpsq() > p2.modpsq();
    });

    // handle special case of thrust if there are only 2 particles
    if (n_fs == 2) {
      // the maximum will just be alligned with the particle with the highest pAbs()
      thrust_axis = final_state_momenta[0]/final_state_momenta[0].modp(); 
      // compute the trust
      for (unsigned int k = 0 ; k < n_fs ; k++) unnormalised_thrust += abs(dot3(thrust_axis, final_state_momenta[k]));
      return unnormalised_thrust;
    }

    // otherwise we need to do some work
    // we use the algorithm written schematically in the pythia manual around eqn 224
    std::vector<precision_type>             thrust_values;
    std::vector<MomentumM2<precision_type>> thrust_axises;
    // we either take the first four or three leading particles
    // resulting in eight or four different starting vectors for thrust
    // for three partons we have (one of these needs to maximize the thrust)
    // n0 = - p0 - p1 - p2
    // n1 =   p0 - p1 - p2
    // n2 = - p0 + p1 - p2
    // n3 =   p0 + p1 - p2  >> sign takes care of this
    // for four or more partons we have:
    // n0 = - p0 - p1 - p2 - p3
    // n1 =   p0 - p1 - p2 - p3 
    // n2 = - p0 + p1 - p2 - p3 
    // n3 =   p0 + p1 - p2 - p3 
    // n4 = - p0 - p1 + p2 - p3
    // n5 =   p0 - p1 + p2 - p3 
    // n6 = - p0 + p1 + p2 - p3 
    // n7 =   p0 + p1 + p2 - p3 
    // if we have exactly four partons, one of these again maximises the thrust
    unsigned int max_n = 4;
    for(int i = 0; i < pow(2, std::min(max_n,n_fs) - 1); i++){
      int sign = i;
      // first build a vector for the initial four jets
      thrust_axis = Momentum(0,0,0,0); // < reset the axis
      // take the four highest energetic partons
      // std::cout << "Trail i = " << i << std::endl;
      for (unsigned int k = 0; k < std::min(max_n,n_fs); k++){
        (sign % 2) == 1 ? thrust_axis += final_state_momenta[k] : thrust_axis -=final_state_momenta[k];
        sign /= 2;
      }
      thrust_axis /= thrust_axis.modp(); //< make it a unit vector
      // std::cout << "n" << i << " = " << thrust_axis << std::endl;
      // with this as a starting point we start the convergence algorithm
      // we calculate the projection of each of the momenta on the thrust axis
      // depending on the sign of the projection we add it or subtract it
      // the algorithm converges if the difference between the newly calculated vector and the current
      // thrust axis is smaller than some number
      precision_type diff = numeric_limits<precision_type>::max();
      // int l = 0;
      while (diff > _thrust_axis_precision) {
        MomentumM2<precision_type> test_all_partons(0.,0.,0.,0.);
        // loop over all partons
        for (unsigned int k = 0; k < n_fs; k++){
          dot3(test_all_partons, final_state_momenta[k]) > 0 ? test_all_partons+=final_state_momenta[k] : test_all_partons-=final_state_momenta[k];
        }
        // calculate the difference
        diff = (thrust_axis - test_all_partons/test_all_partons.modp()).modp();
        // reset the benchmark
        thrust_axis = test_all_partons/test_all_partons.modp();
        // l++;
      }
      // and calculate the thrust value for this axis
      unnormalised_thrust = 0.;
      for (unsigned int k = 0 ; k < n_fs ; k++) unnormalised_thrust += abs(dot3(thrust_axis, final_state_momenta[k]));
      // now store the quantities
      thrust_axises.push_back(thrust_axis); 
      thrust_values.push_back(unnormalised_thrust);
    }

    // pick the solution with the largest thrust
    unnormalised_thrust = 0.;
    for (unsigned int i=0 ; i < thrust_axises.size(); i++){
      if (thrust_values[i] > unnormalised_thrust){
        unnormalised_thrust = thrust_values[i];
        thrust_axis         = thrust_axises[i];
      }
    }
    return unnormalised_thrust;
  }
  

  /// calculation of the c-parameter
  /// = sum_{i,j} |p_i| |p_j| sin^2 theta_ij= sum_{i,j} pow2(p_i X p_j)/(|p_i|*|p_j|)
  /// returns the above quantity without the normalisation
  precision_type _calculate_C_parameter(const std::vector<MomentumM2<precision_type>> & final_state_momenta){
    precision_type unnormalised_C_parameter = 0.;
    unsigned int n_fs = final_state_momenta.size();
    for(unsigned int i = 0; i < n_fs; i++){
      for(unsigned int j = i + 1; j < n_fs; j++){
        // don't need a protection for modp == 0, particles are massless
        unnormalised_C_parameter += cross(final_state_momenta[i], final_state_momenta[j]).modpsq() / (final_state_momenta[i].modp() * final_state_momenta[j].modp());
      }
    }
    return unnormalised_C_parameter;
  }
};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv,true);
  ExampleDIS driver(&cmdline);
  driver.run();
}
