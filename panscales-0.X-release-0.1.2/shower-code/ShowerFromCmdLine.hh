//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
/// \file ShowerFromCmdLine.hh
/// handles all different options that can be 
/// passed to the PanScales framework via command line
/// arguments, e.g. shower name, number of events,...
#ifndef __SHOWERFROMCMDLINE__
#define __SHOWERFROMCMDLINE__

#include "CmdLine.hh"
#include "Type.hh"
#include "ShowerRunner.hh"
#include "Collider.hh"
#include "panhoppet_v1.h"

namespace panscales{

/// creates an object derived from the specified Base class
/// that is specific for the shower specified in the command-line
/// arguments
panscales::ShowerRunner * create_showerrunner_with_shower_and_qcd_instance(CmdLine & cmdline, std::ostream & header_str,
                                                                           const panscales::Collider & collider);

/// @brief function to create a shower given a cmdline, name of the shower, qcd instance and collider
/// @param cmdline 
/// @param shower_name 
/// @param qcd 
/// @param collider 
/// @return pointer to a shower, the caller takes ownership
panscales::ShowerBase * define_shower_from_cmd_line_with_name_given(CmdLine & cmdline, const std::string & shower_name,
                                                                    const panscales::QCDinstance & qcd, const panscales::Collider & collider);

/// @brief function to create a shower given a cmdline, qcd instance and collider
/// @param cmdline 
/// @param qcd 
/// @param collider 
/// @return pointer to a shower
panscales::ShowerBase * define_shower_from_cmd_line(CmdLine & cmdline,
                                                    const panscales::QCDinstance & qcd,
                                                    const panscales::Collider & collider);

/// @brief creates the shower used for matching in showerrunner, and sets up the pointers in showerrunner
/// @param cmdline 
/// @param shower_runner 
/// @param collider 
void set_shower_for_matching(CmdLine & cmdline, panscales::ShowerRunner * shower_runner,
                             const panscales::Collider & collider);

/// @brief creates a hoppetrunner, returns a pointer
///        the user owns the pointer and is responsible for cleaning it up
/// @param cmdline 
/// @param header_str 
/// @return pointer of a hoppetrunner instance
panscales::HoppetRunner * create_hoppetrunner(CmdLine & cmdline, std::ostream & header_str, bool collider_has_pdfs);

/// function to pass the hoppetrunner to the showerrunner instance
/// it also sets the max reliable x fraction (default as stored in hoppetrunner)
/// and it sets a scale remapping if running with toy PDFs
void init_hoppetrunner_in_showerrunner(CmdLine & cmdline, panscales::ShowerRunner * shower_runner,
                                       panscales::HoppetRunner * hoppet_runner, double lnv_max);

}
  
#endif // __SHOWERFROMCMDLINE__
