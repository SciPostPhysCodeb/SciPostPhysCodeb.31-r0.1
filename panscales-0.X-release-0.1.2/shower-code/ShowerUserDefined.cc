//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerUserDefined.hh"

namespace panscales{

  //----------------------------------------------------------------------
  // USER-TODO: this function is responsible for creating the elements 
  // given a single dipole (two elements/dipole for a dipole shower,
  // one element/dipole for an antenna shower)
  // One should make sure the appropriate elements are created
  // (see example code in cc)
  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerUserDefined::elements(
    Event & event, int dipole_index) const {
    throw std::runtime_error("USER-TODO: lnb_generation_range(lnv) for shower " 
                             + name() + " should be implemented");
    
    // retrieve the dipole ends
    int i3    = event.dipoles()[dipole_index].index_3()   ;
    int i3bar = event.dipoles()[dipole_index].index_3bar();

    // We implement a final-state shower
    assert(!event.particles()[i3].is_initial_state() && !event.particles()[i3bar].is_initial_state() && 
      "This toy shower only handles final-state showers");

    // a dipole shower has two elements per dipole
    std::unique_ptr<typename ShowerBase::Element> elms[] = {
      std::unique_ptr<typename ShowerBase::Element>(new ShowerUserDefined::Element(i3, i3bar, dipole_index, &event, this)),
      std::unique_ptr<typename ShowerBase::Element>(new ShowerUserDefined::Element(i3bar, i3, dipole_index, &event, this))
    };

    // // an antenna shower has only a single element per dipole
    // std::unique_ptr<typename ShowerBase::Element> elms[] = {
    //   std::unique_ptr<typename ShowerBase::Element>(new ShowerUserDefined::Element(i3, i3bar, dipole_index, &event, this)),
    // };

    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }
  ShowerBase::EmissionInfo* ShowerUserDefined::create_emission_info() const{ return new ShowerUserDefined::EmissionInfo(); }

  //----------------------------------------------------------------------
  // the lnb total extent, as a function of lnv, is parametrised
  // as  const + lnv_deriv * lnv.
  // USER-TODO: fill the constant and lnv_deriv coefficients
  //            watch out: the lnv_coeff is usually negative
  double ShowerUserDefined::Element::lnb_extent_const() const {
    throw std::runtime_error("USER-TODO: lnb_extent_const() for shower " + _shower->name() + " should be implemented");
    return 0.0;
  }  
  double ShowerUserDefined::Element::lnb_extent_lnv_coeff() const {
    throw std::runtime_error("USER-TODO: lnb_extent_lnv_coeff() for shower " + _shower->name() + " should be implemented");
    return 0.0;
  }

  //----------------------------------------------------------------------
  // USER-TODO: for a given shower evolution
  // scale lnv, this function needs to return
  // the range over which the lnb auxiliary
  // variable should be generated
  Range ShowerUserDefined::Element::lnb_generation_range(double lnv) const {
    throw std::runtime_error("USER-TODO: lnb_generation_range(lnv) for shower " 
                             + _shower->name() + " should be implemented");
    return Range(0.0, 0.0);
  }

  //----------------------------------------------------------------------
  // USER-TODO: This is the soft-collinear, large-Nc limit of the
  // emission density. Note that it should use the maximum value of
  // alphas (as running-coupling effects are taken into account
  // elsewhere).
  //
  // For example, for a shower that uses kt as an evolution variable
  // and rapidity as an auxiliary, the emission density in the
  // soft-collinear & large-Nc limit is
  //    alphas CA/pi
  // so this would return
  //    _shower->max_alphas() * _shower->qcd().CA() / M_PI;
  double ShowerUserDefined::Element::lnv_lnb_max_density() const { 
    throw std::runtime_error("USER-TODO: lnv_lnb_max_density() for shower " 
                             + _shower->name() + " should be implemented");
    return 0.0;
  }

  //------------------------------------------------------------------------
  // USER-TODO: for a given lnv,lnb, this function should return the
  // approximate lnkt in the emitter-spectator dipole rest frame, 
  // which is equivalent to the frame-independent lnkt with respect 
  // to the emitter-spectator system.
  // This lnkt will be correct in the soft-collinear limit but may
  // differ from the true lnkt in the soft large-angle limit and the
  // hard-collinear limit
  double ShowerUserDefined::Element::lnkt_approx(double lnv, double lnb) const {
    throw std::runtime_error("USER-TODO: lnkt_approx() " + _shower->name() + " should be implemented");
    return lnv; //< this assumes a kt-ordered shower, i.e. v=kt
  }

  //------------------------------------------------------------------------
  // USER-TODO: for a given lnv,lnb, this function returns
  // the pseudorapidity with respect to
  // the closer of the emitter/spectator, defined in the event
  // centre-of-mass frame. This eta will be correct in the
  // soft-collinear limit but may differ from the true eta in the
  // soft large-angle limit and the hard-collinear limit.
  // Typically, one would get an expression the rapidity wrt the emitter
  //    double eta_emitter = ...;
  // and a similar one wrt the spectator
  //   double eta_spectator = ...;
  // and then decide which side we are closer to, making sure that
  // the positive end is closer to the emitter and the negative end
  // closer to the spectator
  double ShowerUserDefined::Element::eta_approx(double lnv, double lnb) const {
    throw std::runtime_error("USER-TODO: eta_approx() " + _shower->name() + " should be implemented");
    
    return 0.0;
  }  
  
  //------------------------------------------------------------------------
  // USER-TODO: for a given lnv, returns the lnb for which eta_approx is the smallest  
  double ShowerUserDefined::Element::lnb_for_min_abseta(double lnv) const {
    throw std::runtime_error("USER-TODO: eta_approx() " + _shower->name() + " should be implemented");
    return 0.0;
  }

  //----------------------------------------------------------------------
  // USER-TODO: 
  // From the given lnv and lnb, this
  // function
  //   ( i) returns true or false depending on whether the phase-space is accessible or not
  //   (ii) computes the ingredients needed to calculate the acceptance probability for the emission
  //
  // Step (ii) requires the following emission_info variables to be filled (defaults in bracket):
  //    emission_info->emitter_weight_rad_gluon   [1.0]
  //    emission_info->emitter_weight_rad_quark   [0.0]
  //    emission_info->spectator_weight_rad_gluon [0.0]
  //    emission_info->spectator_weight_rad_quark [0.0]
  //    emission_info->z_radiation_wrt_emitter    [0.0]
  //    emission_info->z_radiation_wrt_spectator  [0.0]
  //
  // The
  //    _shower->fill_dglap_splitting_weights(<emitter>, <radiation z fraction>,
  //                                          &<weight_gluon,
  //                                          &<weight_quark);
  //
  // helper can be used to compute the weights based on our implementation of
  // the DGLAP splitting kernels (more precisely, it computes zP(z) with z the
  // momentum fraction of the emitted gluon or quark)
  // For clarity a simple sketch is included in the cc file. 
  bool ShowerUserDefined::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const {
    throw std::runtime_error("USER-TODO: acceptance_probability() for shower " + _shower->name() + " should be implemented");

    // simple sketch
    //
    //   // WATCH OUT: the following line has an undefined behaviour if the
    //   // Emission info is mistakenly not that of ShowerUserDefined (a dynamic
    //   // cast has a small speed penalty)
    //   typename ShowerUserDefined::EmissionInfo & emission_info = *(static_cast<typename ShowerUserDefined::EmissionInfo*>(emission_info_base));
    //   
    //   const double & lnv = emission_info.lnv;
    //   const double & lnb = emission_info.lnb;
    //    
    //   precision_type z = ...;
    //
    //    // cache it for future use
    //   emission_info.z = z;
    //   if (z < 0 || z > 1) {return false;}
    //   
    //   _shower->fill_dglap_splitting_weights(emitter(), z,
    //                                         emission_info.emitter_weight_rad_gluon,
    //                                         emission_info.emitter_weight_rad_quark);
    //   
    //   // Store collinear momentum fractions in emission info for spin correlations
    //   emission_info.z_radiation_wrt_emitter   = z;
    //   emission_info.z_radiation_wrt_spectator = 0.;

    return true;
  }


  //----------------------------------------------------------------------
  // USER-TODO: Based on the full kinematic set of
  // variables (lnv, lnb, phi, and everything cached in
  // acceptance_probability), choice of whether the emitter or
  // spectator split, and flavour information, this method should
  // compute the radiation, emitter_out and spectator_out members of
  // emission info.
  //
  // Note that in order to allow for the use of directional
  // differences, the mapping implemented here should use the
  // momenta provided via "RotatedPieces rp, including
  //   rp.emitter, rp.spectator, rp.perp1, rp.perp2
  // A sketch is included in the cc file
  //
  // Although a bool, this function may not return
  // false when turning on the implementation of the spin-correlations.
  // That means that phase-space bounds should have been checked before
  // running this function
  bool ShowerUserDefined::Element::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const {
    throw std::runtime_error("USER-TODO: acceptance_probability() for shower " + _shower->name() + " should be implemented");

    // sketch:
    //
    //  typename ShowerUserDefined::EmissionInfo & emission_info = *(static_cast<typename ShowerUserDefined::EmissionInfo*>(emission_info_base));
    //  
    //  // retrieve cached info
    //  const double & lnv       = emission_info.lnv;
    //  const double & lnb       = emission_info.lnb;
    //  const double & phi       = emission_info.phi;
    //  const precision_type & z = emission_info.z;
    //  
    //  Momentum3<precision_type> emitter_out3   = ... * rp.emitter.p3() + ... * rp.spectator.p3() - perp.p3();
    //  Momentum3<precision_type> radiation3     = ... * rp.emitter.p3() + ... * rp.spectator.p3() + perp.p3();
    //  Momentum3<precision_type> spectator_out3 =                         ... * rp.spectator.p3();
    //  
    //  // then construct the final 4-momenta, making use of the on-shell condition
    //  emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    //  emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    //  emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    return true;
  }
  //--------------------------------------------------------------
  // USER-TODO: 
  // For a global shower, one can also apply some operations at the end of the emission process.
  // In this case, one should make sure that one calls
  //    ShowerBase::Element::update_event(transition_runner, emission_info);
  /// which carries out everything that is needed for showers with dipole-local recoil. 
  // see the implementation of ShowerPanScaleGlobal for extra details
  //
  // virtual void update_event(const ColourTransitionRunnerBase *transition_runner,
  //                           typename ShowerBase::EmissionInfo * emission_info) override{
  // // do some operations  
  //}

  //--------------------------------------------------------------
  // USER-TODO: this is the place where one should update the
  // element's cached information (e.g. the dipole mass) when the
  // element gets updated after a branching.
  void ShowerUserDefined::Element::update_kinematics() {
    throw std::runtime_error("USER-TODO: update_kinematics() " + _shower->name() + " should be implemented");
  }

} // namespace panscales
