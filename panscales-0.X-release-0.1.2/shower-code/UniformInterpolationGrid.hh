//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef __UNIFORM_INTERPOLATION_GRID_HH__
#define __UNIFORM_INTERPOLATION_GRID_HH__

#include <vector>
#include <cmath>


/// \class UniformInterpolationGrid
/// holds a grid and provide polynomial interpolation
///
/// At the moment, the grid is allocated with a fixed size
class UniformInterpolationGrid : public std::vector<double>{
public:
  /// argument is the interpolation order: 1 for linear, 2 for quadratic, ...
  ///
  /// A grid of "n" elements runs from 0 to n-1
  UniformInterpolationGrid(unsigned int interpolation_order=1) : _order(interpolation_order){

    // we'll use a n-point polynomial interpolation. This takes the form
    //   \sum_i c_i y_i
    // with
    //   x_i = (lnkt_i-lnkt)/dlnkt
    //   y_i is the value at scale lnkt_i
    //   c_i = \prod_{j!=i} x_j/(x_j-x_i)
    //       = [\prod_{j!=i} x_j]/(i! (n-i)!)
    //       = [\prod_{j!=i} x_j] * norm_i
    //   norm_i = 1/(i! (n-i)!)
    _norms.reserve(_order+1);
    _norms[0] = 1.0;
    
    for (unsigned int i=1; i<=_order; ++i)
      _norms[i] = -_norms[i-1]/i;

    double fac=1.0;
    for (unsigned int i=1; i<=_order; ++i){
      _norms[_order-i] *= fac;
      fac/=(i+1);
    }
    
  }

  double operator()(double x) const{
    // find were we are in the vector
    //
    // we want to use the points
    //  ix, ix+i, ..., ix+_order
    // we therefore need ix>=0 and ix+_order<=size-1
    int ix = std::min(std::max(int(x - 0.5*_order), 0), int(size()-_order-1));

    vector<double> xi(_order+1);
    double product = 1.0;
    for (unsigned int i=0;i<=_order;++i){
      xi[i] = ix+i-x;
      if (xi[i]==0.0) return (*this)[ix+i];
      product *= xi[i];
    }
    
    double sum = 0.0;
    for (unsigned int i=0;i<=_order;++i)
      sum += _norms[i] * (*this)[i+ix] / xi[i];
    return sum * product;
  }     


private:
  unsigned int _order;
  std::vector<double> _norms;
};


//----------------------------------------------------------------------
// Version based on Gavin's code; essentially similar apart from the initialisation
//----------------------------------------------------------------------
//
// /// \class UniformInterpolationGrid
// /// holds a grid and provide polynomial interpolation
// ///
// /// At the moment, the grid is allocated with a fixed size
// class UniformInterpolationGrid : public std::vector<double>{
// public:
//   /// argument is the interpolation order: 1 for linear, 2 for quadratic, ...
//   ///
//   /// A grid of "n" elements runs from 0 to n-1
//   UniformInterpolationGrid(unsigned int interpolation_order=1) : _order(interpolation_order){
//     _interp_normalisations.resize(_order+1);
//     _interp_normalisations[0] = std::pow(-1, _order) * std::tgamma(_order+1);
//     
//     for (unsigned int i=1; i<=_order; ++i) 
//       _interp_normalisations[i] = _interp_normalisations[i-1] * i / (i-1.0-_order);
// 
//     for (unsigned int i=0; i<=_order; ++i) 
//       _interp_normalisations[i] = 1.0/_interp_normalisations[i];
//   }
//   
//   double operator()(double x) const{
//     // find were we are in the vector
//     //
//     // we want to use the points
//     //  ix, ix+i, ..., ix+_order
//     // we therefore need ix>=0 and ix+_order<=size-1
//     int ix = std::min(std::max(int(x - 0.5*_order), 0), int(size()-_order-1));
// 
//     std::cout << "x=" << x << " -> ix=" << ix << " in vec of size " << size() << std::endl;
// 
//     std::vector<double> dists(_order+1);
//     for (unsigned int i=0; i<=_order; ++i){
//       dists[i] = x-i-ix;
//       if (dists[i]==0) return (*this)[i+ix];
//       std::cout << "dist[" << i << "]  " << dists[i] << std::endl;
//     }
// 
//     double prod_dists = 1.0;
//     for (unsigned int i=0; i<=_order; ++i)
//       prod_dists *= dists[i];
//     
//     double sum = 0.0;
//     for (unsigned int i=0; i<=_order; ++i){
//       std::cout << "norm[" << i << "] = " << _interp_normalisations[i]
//                 << ", elm = " << (*this)[i+ix]
//                 << ", coef = " << prod_dists * _interp_normalisations[i] / dists[i] << std::endl;;
//       sum += prod_dists * _interp_normalisations[i] / dists[i] * (*this)[i+ix];
//     }
//     return sum;
//   }     
// 
// 
// private:
//   unsigned int _order;
//   std::vector<double> _interp_normalisations;
// };


#endif  // __UNIFORM_INTERPOLATION_GRID_HH__
