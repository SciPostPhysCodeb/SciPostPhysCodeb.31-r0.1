PanScales examples
==================

This file contains some notes about the examples in this directory.

# Table of Contents
1. [Cloning the repository](#cloning-the-repository)
2. [Basic build instructions](#basic-build-instructions)
3. [Logarithmic accuracy tests in $e^{+}e^{-}$](#LogTests)
    1. [Global observables](#global-observables)
    2. [Energy in a slice](#energy-in-a-slice)
4. [Examples with physical coupling](#examples-with-physical-coupling)
    1. [$e^{+}e^{-}\to q\bar{q}$](#ee-to-qbarq)
    2. [DIS ($\gamma^{*}q\to q$)](#DIS)
    3. [$q\bar{q}\to Z$](#qbarqto-z)

## Cloning the repository

Depending on your setup, use **one** of the following commands to clone the repository

    git clone --recursive https://gitlab.com/panscales/panscales-0.X.git
    git clone --recursive git@gitlab.com:panscales/panscales-0.X.git

If you already cloned the repository without the `--recursive` flag, you
can initialise the submodules from within the repository with

    git submodule update --init --recursive

## Basic build instructions

To build the code and examples, you will need a C++ and a Fortran
compiler as well as CMake. The python plotting scripts assume Python 3
and the Matplotlib module (install it with `pip3 install matplotlib`). 

After cloning (as above), the code can be built as follows

```
cd panscales-0.X/shower-code/
../scripts/build.py -j
```
This creates a `build-double` directory with the build files and
executables. The `-j` flag is optional and specifies a multi-core build.

The `scripts/build.py` script uses CMake to organise the build. More advanced builds are
described in [BUILD.md](BUILD.md).

<a id="LogTests"></a>
## Logarithmic accuracy tests in $e^+e^-$
All shower ingredients beyond strict next-to-leading log accuracy, e.g.
matching or double-soft corrections, are switched off in these series of
examples.

### Global observables
The [example-global-ee.py](example-global-nll-ee.py) script provides the
shower NLL baseline for Lund observables and $y_{23}$ in $e^{+}e^{-}$
collisions. From within the [shower-code/](./) directory (not the build
directory), NLL testing can be performed by running
```
./example-global-nll-ee.py [--njobs] [--shower showername] [--only-plots]
```
The `--njobs` takes an integer which should be equal to the number of
cores available to run on. Depending on the machine, the script takes
around a few tens of CPU minutes (*i.e.* a few minutes on a modern
multi-core machine). The script is limited to showers and observables
with the same $\beta_{\rm PS}=\beta_{\rm obs}=0$ for illustrative purposes. By default it uses
the $\beta_{\rm PS}=0$ variant of PanGlobal (`--shower panglobal`). The script
can also run with the Dipole-kt shower, which is not NLL accurate
(`--shower dipole-kt`). The plots below illustrate the output from the
script. For each of several observables $v$, the plots show the ratio
of the cumulative distribution $\Sigma(v)$ to the NLL analytic result
as a function of $\alpha_s \ln(v)$ for different values of $\alpha_s$.
The plots provide a numerical indication of the NLL accuracy of the
shower, since the reproduces the NLL result (ratio=1) in the
$\alpha_s\to 0$ limit. The `--only-plots` option can be used to
produce plots from existing datafiles without running the shower
again. 

<img src="example-results/reference-results/Mj.png" alt="Mj" width="300"/>
<img src="example-results/reference-results/Sj.png" alt="Sj" width="300"/>
<img src="example-results/reference-results/sqrt_y3.png" alt="sqrt_y3" width="300"/>

### Energy in a rapidity slice
Similarly to the previous example, run the following script
```
./example-nonglobal-nll-ee.py [--njobs] [--shower showername] [--only-plots]
```
to analyse a non-global observable, namely, the energy distribution in a
rapidity slice of (half) width $1$, i.e. |$\eta$|<1, at leading colour.
Note that this should take $\mathcal{O}(10)$ CPU minutes and give results
good enough for single-log tests with a percent relative accuracy, as we
show below. The analytic expectation is presented in the file
`example-results/reference-results/nonglobal-ee-nll-analytics.dat`. 

<img src="example-results/reference-results/slice.png" alt="slice" width="300"/>

### Further tests

A more complete set of NLL tests can be found in
[../analyses/nll-validation/](../analyses/nll-validation/). See the
[README.md](../analyses/nll-validation/README.md) file in that directory
for further details.

## Examples with physical coupling

This set of examples considers a semi-physical setup and cannot be used
for NLL tests. Examples with incoming hadrons use a toy PDF as described
in Appendix A.3 of [https://arxiv.org/abs/2207.09467](arXiv:2207.09467). 

Note that the kinematics and flavour of Born events are fixed rather
than sampled over. Note also that all events have unit weight, *i.e.* in
order to recover the physical cross-section the proper normalisation has
to be included.  


### $e^{+}e^{-}\to q\bar{q}$
Run it with a command such as
```
build-double/example-ee -shower panglobal -beta 0 -process ee2qq -physical-coupling -rts 91.1876 -nev 100000 -out example-results/example-ee.dat
```
This script calculates a few event shapes in $e^+e^-$ collisions
including: thrust, broadening, C-parameter, fragmentation function and
the $y_{23}$ resolution parameter. Matching and double-soft corrections
can be switched on by including in the command line the
`-3-jet-matching` and `-double-soft` flags, respectively. 

Below we plot the results of the above command line for thrust,
broadening, C-parameter and $y_{23}$. To reproduce these figures run
`./example-results/plot-example-ee.py`.  

<img src="example-results/reference-results/thrust.png" alt="thrust" width="300"/>
<img src="example-results/reference-results/broadening.png" alt="broadening" width="300"/>

<img src="example-results/reference-results/cparam.png" alt="cparam" width="300"/>
<img src="example-results/reference-results/y23.png" alt="y23" width="300"/>

<a id="DIS"></a>
### DIS ($\gamma^{*}q\to q$)
Run it with a command such as
```
./build-double/example-dis -process DIS -shower panlocal -beta 0.5 -Q2 8300 -y 0.2 -rts 320 -physical-coupling -nev 100000 -out example-results/example-dis.dat
```

Below we plot the results of the above command line for thrust,
broadening, C-parameter and the jet mass $\rho$. To reproduce these
figures run `./example-results/plot-example-dis.py`.

<img src="example-results/reference-results/thrust-dis.png" alt="thrust-dis" width="300"/>
<img src="example-results/reference-results/broadening-dis.png" alt="broadening-dis" width="300"/>

<img src="example-results/reference-results/cparam-dis.png" alt="cparam-dis" width="300"/>
<img src="example-results/reference-results/rho-dis.png" alt="rho-dis" width="300"/>

### $q\bar{q}\to Z$
This analysis examines Drell-Yan events at fixed $Z$ rapidity in LHC kinematics:
```
./build-double/example-pp -process pp2Z -shower panglobal -beta 0 -mZ 91.1876 -yZ 0.5 -physical-coupling -lnvmax 4.51292 -rts 13600 -nev 100000 -out example-results/example-pp.dat
```

Below we plot the results of the above command line for the
$p_t$-distribution of the $Z$ boson and leading jet. We limit the plot
to the region around the Sudakov peak as no matching is included. To
reproduce these figures run `./example-results/plot-example-pp.py`.

<img src="example-results/reference-results/boson-pt-pp.png" alt="boson-pt-pp" width="300"/>
<img src="example-results/reference-results/jet-pt-pp.png" alt="jet-pt-pp" width="300"/>
