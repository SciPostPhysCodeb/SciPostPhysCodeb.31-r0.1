//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerPanScaleLocal.hh"

using namespace std;

namespace panscales{
  
  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerPanScaleLocal::elements(
    Event & event, int dipole_index) const {
    int iq    = event.dipoles()[dipole_index].index_q   ;
    int iqbar = event.dipoles()[dipole_index].index_qbar;
    std::unique_ptr<typename ShowerBase::Element> elms[] = {
      std::unique_ptr<typename ShowerBase::Element>(new ShowerPanScaleLocal::Element(iq, iqbar, dipole_index, &event, this)),
      std::unique_ptr<typename ShowerBase::Element>(new ShowerPanScaleLocal::Element(iqbar, iq, dipole_index, &event, this))
    };
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }

  //----------------------------------------------------------------------
  Range ShowerPanScaleLocal::Element::lnb_generation_range(double lnv) const {
    // this corresponds to Eq. (52 - 53) of 2018-07-notes.pdf (and to the soft limit used in the Sudakov)
    double lower_bound = to_double(log_T(_sitilde/_sjtilde*pow2(_rho)/_dipole_m2) + 2.*lnv)
      /2./(1. + _shower->_beta);
    double upper_bound = to_double(log_T(_sitilde/_sjtilde*_dipole_m2/pow2(_rho)) - 2.*lnv)
      /2./(1. + _shower->_beta);
    return Range(lower_bound,upper_bound);
  }

  double ShowerPanScaleLocal::Element::lnv_lnb_max_density() const {
    // For now we work in a large-NC limit, with CF defined to be equal
    // to CA/2; the density that is returned here corresponds to that
    // for a quark or for half a gluon
    return _shower->max_alphas() * _shower->qcd().CA() / M_PI* double_soft_overhead();
  }

  precision_type ShowerPanScaleLocal::Element::dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const{
    double bps = _shower->beta();
    double lnQ = to_double(0.5*log(event().Q2()));
    //
    // Is there enough phase space for ak<1 and bk<1?
    if(lnb >= (lnQ-lnv)/(1+bps) || lnb <= (lnv-lnQ)/(1+bps) ) return 0;
    //
    precision_type ak_exp = lnv + lnb + bps * fabs(lnb) - lnQ;
    precision_type bk_exp = lnv - lnb + bps * fabs(lnb) - lnQ;
    //
    precision_type omak;
    if(fabs(ak_exp) < numeric_limit_extras<double>::sqrt_epsilon())
      omak = - ak_exp * (1 + 0.5 * ak_exp);
    else
      omak = 1 - exp(ak_exp);
    //
    precision_type bk        = exp(bk_exp);
    precision_type kt2_on_Q2 = exp( 2 * lnv + 2 * bps * fabs(lnb) - 2 * lnQ );
    //
    // Is the ak+bk<1 phase space boundary respected?
    if(bk>=omak) return 0;
    // otherwise return the jacobian
    return 2 * kt2_on_Q2 * (omak-bk) / pow(omak,2);
  }

  precision_type ShowerPanScaleLocal::Element::get_partitioning_factor_matching_probability(typename ShowerBase::EmissionInfo * emission_info) const {
    double etabar_i = to_double(0.5*log_T(0.5*event().Q2()*emission_info->spec_dot_rad/emission_info->emit_dot_rad/emission_info->emit_dot_spec));
    double etabar_j = to_double(0.5*log_T(0.5*event().Q2()*emission_info->emit_dot_rad/emission_info->spec_dot_rad/emission_info->emit_dot_spec));
    // Partitioning for dP_exact ( g(η_i)/(g(η_i)+g(η_j)) ).
    return g_fcn(etabar_i)/(g_fcn(etabar_i) + g_fcn(etabar_j));
  }

  //----------------------------------------------------------------------
  bool ShowerPanScaleLocal::veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const {
    assert(false && "cannot use PanLocal truncated (yet).");
    return false;
  }
  
  //----------------------------------------------------------------------

  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  bool ShowerPanScaleLocal::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocal!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocal::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocal::EmissionInfo*>(emission_info_base));

    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;

    // Eq. (2) of arXiv:2002.11114
    precision_type kt = _rho * exp(precision_type(lnv + _shower->_beta * fabs(lnb)));

    // Eq. (3) of arXiv:2002.11114
    precision_type ak   = sqrt(_sjtilde/_sitilde/_dipole_m2) * kt * exp(precision_type(lnb));
    precision_type bk   = pow2(kt)/_dipole_m2/ak;

    // cache this info for future usage
    emission_info.kt = kt;
    emission_info.ak = ak;
    emission_info.bk = bk;

    // test phase space boundary (Eq. (42) of 2018-07-notes.pdf)
    if (ak + bk >= 1.0) {return false;}

    // here we should make a decision based on flavour
    _shower->fill_dglap_splitting_weights(emitter(), ak,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark); 

    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter   = ak;
    emission_info.z_radiation_wrt_spectator = 0.;

    // we need to include the proper normalisation as well as the
    // transition between the 2 elements (decision of emitter and
    // spectator)
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    normalisation_factor *= g_fcn(emission_info.lnb);
    
    // include the DS overhead factors
    normalisation_factor *= double_soft_overhead();
    
    // We have dlnb = deta as our integration measure (Eq. (68) of 2018-07-notes.pdf)
    // We only have to normalise by the max density to get the acceptance
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;

    return true; 
  }

  //------------------------------------------------------------------------
  bool ShowerPanScaleLocal::Element::do_kinematics(
        typename ShowerBase::EmissionInfo * emission_info_base, 
        const RotatedPieces & rp) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocal!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocal::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocal::EmissionInfo*>(emission_info_base));

    const double & phi = emission_info.phi;

    const precision_type & kt = emission_info.kt;
    const precision_type & ak = emission_info.ak;
    const precision_type & bk = emission_info.bk;

    // light cone component for the parents
    // Eqs. (7) of arXiv:2002.11114
    precision_type ai = 1. - ak;
    precision_type bi = ak * bk/ai;
    precision_type bj = (ai - bk)/ai;
  
    // for cases with a plane, phi=0 means in-plane
    Momentum perp = kt * (sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    emission_info.k_perp = perp;

    // Eqs. (1a-c) with f = 1 of arXiv:2002.11114
    // Use Momentum3 class to avoid issues with branchings at small invariant mass
    Momentum3<precision_type> emitter_out3   = ai * (rp.emitter).p3() + bi * (rp.spectator).p3() + perp.p3();
    Momentum3<precision_type> radiation3     = ak * (rp.emitter).p3() + bk * (rp.spectator).p3() - perp.p3();
    Momentum3<precision_type> spectator_out3 = bj * (rp.spectator).p3();

    // Now construct the final 4-momenta by imposing the masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    return true;
  }

  //----------------------------------------------------------------------
  bool ShowerPanScaleLocal::find_lnv_lnb_from_kinematics(const Momentum &p_emit,
                                     const Momentum &p_spec,
                                     const Momentum &p_rad,
                                     const precision_type &p_emit_dot_p_spec,
                                     const precision_type &p_emit_dot_p_rad,
                                     const precision_type &p_spec_dot_p_rad,
                                     const Event &event,
                                     double &lnv, double &lnb) const {

    // Note: the code below is not fully tested
    precision_type sijtilde = (p_emit + p_rad + p_spec).m2();

    precision_type a = p_spec_dot_p_rad/p_emit_dot_p_spec;
    precision_type ak = a/(1. + a);

    // for ak->1, bk->0, we use the same trick as in ShowerPowheg.cc, i.e. starting from
    //
    //    kt_i = xi*θ_itildei = kt_k = xk*θ_itildek
    //
    if (fabs(ak-1) < numeric_limit_extras<precision_type>::sqrt_epsilon()) {
      // There are two configurations which lead to ak ~ 1:
      //   - /1/ if θ_ik ~ π
      //   - /2/ if θ_ik ~ 0
      // (see logbook/2021-11-11-3-jet-matching for the details)

      // We check if 1-cos(θ_ik) ~ 0 or 2 (i.e. if we're in the collinear or anti-collinear limit)
      //
      precision_type omc_theta_ik = p_emit_dot_p_rad/p_emit.E()/p_rad.E();
      // Case /1/
      if (fabs(omc_theta_ik-2) < fabs(omc_theta_ik)) {
        lnb = -to_double(log_T(sqrt(2*p_emit_dot_p_spec/p_emit.E()/p_spec.E())/(1.+p_spec.E()/p_emit.E())/2));
      }
      // Case /2/
      else {
        lnb = -to_double(log_T(sqrt(2*p_emit_dot_p_rad/p_emit.E()/p_rad.E())/(1.+p_rad.E()/p_emit.E())/2));
      }
      lnv = to_double(log_T(sqrt(event.Q2())) - (1+_beta)*lnb);

      return true;
    }
    precision_type bj = 2*p_spec_dot_p_rad / ak / sijtilde;
    precision_type bk = 2*p_emit_dot_p_rad*(1.-ak)/sijtilde;

    Momentum3<precision_type> pjtilde_3   = 1./bj*p_spec;
    Momentum pjtilde = Momentum::fromP3M2(pjtilde_3  , 0.0);
    Momentum3<precision_type> pitilde_3   = p_emit + p_spec + p_rad - pjtilde;
    Momentum pitilde = Momentum::fromP3M2(pitilde_3  , 0.0);

    precision_type sitilde = 2*dot_product(pitilde, event.Q());
    precision_type sjtilde = 2*dot_product(pjtilde, event.Q());

    precision_type kt2 = ak*bk*sijtilde;

    lnb = to_double(log_T(ak/bk)*sitilde/sjtilde/2);
    lnv = to_double(log_T(kt2)/2 - _beta*fabs(lnb) - _beta/2 * log_T(sitilde*sjtilde/sijtilde/event.Q2()));

    return true;
  }

  //----------------------------------------------------------------------
  // Double soft below
  //----------------------------------------------------------------------
  /// This function returns the double soft acceptance probability computed as the ratio
  /// acc_prob = (double soft approximation of shower)/(double soft approximation of full QCD)
  double ShowerPanScaleLocal::Element::double_soft_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    typename ShowerPanScaleLocal::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocal::EmissionInfo*>(emission_info_base));

    // make sure we start with decent defaults
    emission_info.flavour_swap_probability = 0.0;
    emission_info.colour_flow_swap_probability_gg = 0.0;
    emission_info.colour_flow_swap_probability_qq = 0.0;
    
    // only do something non-trivial if double soft corrections are
    // turned on
    if(!double_soft()) {
      assert(double_soft_overhead() == 1.0); 
      return 1.0;
    }

    DoubleSoftInfo ds_info;

    // first fill the momenta involved in the double-soft corrections
    if (!_double_soft_fill_momenta(emission_info_base, &ds_info)){
      return 1.0/double_soft_overhead();
    }

    // fill the shower histories
    _double_soft_fill_weights(&ds_info);

    // compute the flavour-swap probability
    emission_info.flavour_swap_probability = (_shower->qcd().gluon_uses_qqbar())
      ? _double_soft_swap_flavours_prob(emission_info_base, &ds_info) : 0.0;
    
    // Function that returns the acceptance probability for keeping the present colour connection.
    _double_soft_swap_colour_connections(emission_info_base, &ds_info,
                                         emission_info.colour_flow_swap_probability_gg,
                                         emission_info.colour_flow_swap_probability_qq);
    

    // compute the global acceptance probability
    double retval = to_double(ds_info.exact_weight/ds_info.shower_weight);

    // make sure that the value is in the expected range
    // AK Some points with extreme kinematics seem to violate the below bounds (they are NaN)
    if ((retval > double_soft_overhead()) || (retval < 0.5)){
      //GS-NOTE-DS: do we want an assert? (at the very least a Warning)
      cerr << "the computed double-soft probability (" << retval <<  ") is not between 0.5 and 3 as expected" << endl;
      cerr << "Event: " << endl;
      //cerr << *_event << endl << endl;
      //_event->print_following_dipoles();
      cerr << "p_new       = " << ds_info.p_new       << endl;
      cerr << "p_first     = " << ds_info.p_first     << endl;
      cerr << "p_aux_new   = " << ds_info.p_aux_new   << endl;
      cerr << "p_aux_first = " << ds_info.p_aux_first << endl;
      cerr << endl;
      cerr << "sn1 = " << 2.0*dot_product(ds_info.p_new,     ds_info.p_first)     << "     1-cos = " << one_minus_costheta(ds_info.p_new,     ds_info.p_first)     << endl;
      cerr << "sna = " << 2.0*dot_product(ds_info.p_new,     ds_info.p_aux_new)   << "     1-cos = " << one_minus_costheta(ds_info.p_new,     ds_info.p_aux_new)   << endl;
      cerr << "snb = " << 2.0*dot_product(ds_info.p_new,     ds_info.p_aux_first) << "     1-cos = " << one_minus_costheta(ds_info.p_new,     ds_info.p_aux_first) << endl;
      cerr << "s1a = " << 2.0*dot_product(ds_info.p_first,   ds_info.p_aux_new)   << "     1-cos = " << one_minus_costheta(ds_info.p_first,   ds_info.p_aux_new)   << endl;
      cerr << "s1b = " << 2.0*dot_product(ds_info.p_first,   ds_info.p_aux_first) << "     1-cos = " << one_minus_costheta(ds_info.p_first,   ds_info.p_aux_first) << endl;
      cerr << "sab = " << 2.0*dot_product(ds_info.p_aux_new, ds_info.p_aux_first) << "     1-cos = " << one_minus_costheta(ds_info.p_aux_new, ds_info.p_aux_first) << endl;
      cerr << "snQ = " << 2.0*dot_product(ds_info.p_new,       _event->Q()) << endl;
      cerr << "s1Q = " << 2.0*dot_product(ds_info.p_first,     _event->Q()) << endl;
      cerr << "saQ = " << 2.0*dot_product(ds_info.p_aux_new,   _event->Q()) << endl;
      cerr << "sbQ = " << 2.0*dot_product(ds_info.p_aux_first, _event->Q()) << endl;
      cerr << "Q2  = " <<     dot_product(_event->Q(),_event->Q());
      cerr << endl;
      cerr << "shower_weight_gg_same    = " << ds_info.shower_weight_gg_same    << endl;
      cerr << "shower_weight_gg_swapped = " << ds_info.shower_weight_gg_swapped << endl;
      cerr << "shower_weight_qq_same    = " << ds_info.shower_weight_qq_same    << endl;
      cerr << "shower_weight_qq_swapped = " << ds_info.shower_weight_qq_swapped << endl;
      cerr << "shower_weight_gg         = " << ds_info.shower_weight_gg         << endl;
      cerr << "shower_weight_qq         = " << ds_info.shower_weight_qq         << endl;
      cerr << "shower_weight            = " << ds_info.shower_weight            << endl;
      cerr << "exact_weight_gg_same     = " << ds_info.exact_weight_gg_same     << endl;
      cerr << "exact_weight_gg_swapped  = " << ds_info.exact_weight_gg_swapped  << endl;
      cerr << "exact_weight_qq_same     = " << ds_info.exact_weight_qq_same     << endl;
      cerr << "exact_weight_qq_swapped  = " << ds_info.exact_weight_qq_swapped  << endl;
      cerr << "exact_weight_gg          = " << ds_info.exact_weight_gg          << endl;
      cerr << "exact_weight_qq          = " << ds_info.exact_weight_qq          << endl;
      cerr << "exact_weight             = " << ds_info.exact_weight             << endl;

      assert(false);
      return 0.0;
    }    

    return retval/double_soft_overhead();
  }

  //----------------------------------------------------------------------
  // material to handle double-soft corrections
  //----------------------------------------------------------------------

  // fill the 4 momenta associated w the double-soft correction
  //
  // mainly, this function decides at which end of the splitting
  // dipole we have to take the partner gluon and the 2 auxiliarries
  //
  // WATCH OUT: If needed, this method overwrites
  // emission_info->do_split_emitter so that the splitter points to
  // p_first
  bool ShowerPanScaleLocal::Element::_double_soft_fill_momenta(
      typename ShowerBase::EmissionInfo * emission_info_base,
      DoubleSoftInfo * double_soft_info) const{
    typename ShowerPanScaleLocal::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocal::EmissionInfo*>(emission_info_base));

    // at this point, we have already handled the case with only 2
    // particles in the event.

    // double-soft corrections happen in the (phase-space) vicinity of
    // a soft gluon. We therefore have no currections fot qqbar
    // dipoles.
    if ((dipole().dipole_at_3_end()    == nullptr) &&
        (dipole().dipole_at_3bar_end() == nullptr)){
      return false;
    }

    // set the new emission
    double_soft_info->p_new = emission_info.radiation;
    double_soft_info->flav_init = emission_info.radiation_pdgid;

    // We want to make sure to never take hard Born partons as the soft partons
    // in the double-soft correction: we check if we need to invalidate either the
    // 3 or the 3bar end of the current emitting dipole
    int born1, born2; _event->get_Born_indices(born1, born2);
    bool invalidate_3_end = false, invalidate_3bar_end = false;
    if(dipole().index_3() == born1 || dipole().index_3() == born2 ) invalidate_3_end = true;
    if(dipole().index_3bar() == born1 || dipole().index_3bar() == born2 ) invalidate_3bar_end = true;

    // If we are splitting a "Born" colour line for the first time (i.e. if both ends
    // are unvalidated), we never correct for double-soft
    if(invalidate_3bar_end && invalidate_3_end) {
      return false;
    }

    // first treat the case of a gluon emission
    Momentum3<precision_type> vn1, vna, vnb, v1a, v1b, vab;
    if (emission_info.radiation_pdgid==21){
      // we decide whether we take the partner at the 3 or 3bar end of the dipole.
      //
      // If there are no "next" (splitting) dipole at either the 3 or
      // 3bar end of the current splitting dipole, there is a unique
      // assignment. Otherwise, we select the smallest of
      //    p_radiation.p_3_end    and    p_radiation.p_3bar_end
      bool first_at_3_end = true;
      // If both potential emitting ends are invalidated, we don't do anything
      // This situation appears in H->gg where one of the Born gluons has
      // split to a qqbar pair, say if we're splitting from the [g qbar]
      // dipole below:
      /*
                                       qbar = 3bar
                                     /
        (Born g) [INV]              /
         3     --------------------/
         3bar  -------------------- \
                                     \
                                       q = 3 (Born) [INVALIDATED]
      */
      if ((dipole().dipole_at_3_end() == nullptr && invalidate_3bar_end) ||
          (dipole().dipole_at_3bar_end() == nullptr && invalidate_3_end)) {
        return false;
      }
      else if (dipole().dipole_at_3_end() == nullptr || invalidate_3_end) {
        // we only have a dipole at the 3bar end
        first_at_3_end = false;
      } else if (dipole().dipole_at_3bar_end() != nullptr && !invalidate_3bar_end){
        // we have a dipole at both the 3 and 3bar ends
        //
        // compute the dot products at both ends
        //
        // AK: Another option would be to evaluate the ME and take
        // configuration that has maximal true ME.
        precision_type d3    = dot_product(emission_info.radiation, emission_info.momentum_3_out());
        precision_type d3bar = dot_product(emission_info.radiation, emission_info.momentum_3bar_out());
        first_at_3_end = (d3<d3bar);
      }  // otherwise, we only have a dipole at the 3 end and the "true"
         // default is fine

      if (first_at_3_end){
        // we have the following setup (in PanGlobal, "emitter = 3end"):
        //
        //                first
        //               3  3bar
        //                ||     
        //          new   ||
        //            \\  ||
        //    3bar  -----/  \----- 3
        // aux_new              aux_first
        //
        // The splitting dipole is (aux_new,first)
        assert(dipole().particle_3().pdgid()==21);
        double_soft_info->p_first     = emission_info.momentum_3_out();
        double_soft_info->p_aux_new   = emission_info.momentum_3bar_out();
        double_soft_info->p_aux_first = dipole().dipole_at_3_end()->particle_3();

        // We need to make sure that the "splitter" is set to use the 3 
        // end of the jk dipole. 
        //
        //GS-NOTE-DS: do we want to postpone this? (I've put an
        //explicit note in the description)
        //LuSi: CANNOT CHANGE THIS IN PANLOCAL
        //emission_info.do_split_emitter = true;
        
        //GS-NOTE-DS: I'm still a bit puzzled by this: 
        //
        // In the antenna case, there are situations where, initially,
        // the shower decides to split one end of the dipole, and
        // where the partner is still taken as the other dipole's
        // end. In this case, the re-assignmentof do_split_emitter
        // guarantees that we do the proper flavour/colour-flow swaps
        // if needed (even though the kinematc map has been computed
        // at the initial dipole's end). In principles, this should
        // still be OK in a dipole shower.
        //
        // This means that I'd still enforce that "do_split_emmitter"
        // points on the side of the particle labelled as "first" i.e.
        //
        // if "first" is at the 3 end
        //   emission_info.do_split_emitter = emitter_is_3_end_of_dipole();
        // if "first" is at the 3bar end
        //   emission_info.do_split_emitter = emitter_is_3bar_end_of_dipole();
        //
        // From what I can tell from the rest of the code, this would
        // have no -ve side impact (other than guaranteeing that
        // potential DS swaps are done at the right end of the dipole)
        //
        // Note that the above 2 lines of code would also work for an
        // antenna shower (where the 1st line would always be true and
        // the 2nd line would always be false)

        // cache angular distances w dirdiff
        if (use_diffs()){
          // Use 2*(1-cos(theta)) = ddiff^2
          // a few shortcuts (v_i_j = j-i; b==bout)
          Momentum3<precision_type> v_1_n;    //= emission_info.d_radiation_wrt_emitter();
          Momentum3<precision_type> v_a_n;    //= emission_info.d_radiation_wrt_spectator();
          Momentum3<precision_type> v_1_1out; //= emission_info.d_emitter_out;
          Momentum3<precision_type> v_a_aout; //= emission_info.d_spectator_out;
          Momentum3<precision_type> v_a_1;    //= dipole().dirdiff_3_minus_3bar;
          Momentum3<precision_type> v_1_b;    //= dipole().dipole_at_3_end()->dirdiff_3_minus_3bar;

          if(emitter_is_3_end_of_dipole ()){
            v_1_n    = emission_info.d_radiation_wrt_emitter();
            v_a_n    = emission_info.d_radiation_wrt_spectator();
            v_1_1out = emission_info.d_emitter_out;
            v_a_aout = emission_info.d_spectator_out;
          } else {
            v_1_n    = emission_info.d_radiation_wrt_spectator();
            v_a_n    = emission_info.d_radiation_wrt_emitter();
            v_1_1out = emission_info.d_spectator_out;
            v_a_aout = emission_info.d_emitter_out;
          }

          v_a_1    = dipole().dirdiff_3_minus_3bar;
          v_1_b    = dipole().dipole_at_3_end()->dirdiff_3_minus_3bar;
          //double_soft_info->dn1 = (v_1_n-v_1_1out).modpsq(); 
          //double_soft_info->dna = (v_a_n-v_a_aout).modpsq();
          //double_soft_info->dnb = (v_1_n-v_1_b).modpsq(); 
          //double_soft_info->d1a = (v_a_1+v_1_1out-v_a_aout).modpsq();  
          //double_soft_info->d1b = (v_1_b-v_1_1out).modpsq(); 
          //double_soft_info->dab = (v_1_b+v_a_1-v_a_aout).modpsq(); 

          vn1 = v_1_1out-v_1_n;
          vna = v_a_aout-v_a_n;
          vnb = v_1_b-v_1_n;
          v1a = v_a_aout-v_a_1-v_1_1out;
          v1b = v_1_b-v_1_1out;
          vab = v_1_b+v_a_1-v_a_aout;
        }
      } else {
        // we have the following setup:
        //
        //                first
        //               3  3bar
        //                ||     
        //                ||   new
        //                ||  //    
        //    3bar  -----/  \----- 3
        // aux_first           aux_new
        //
        // The splitting dipole is (first,aux_new)
        assert(dipole().particle_3bar().pdgid()==21);
        double_soft_info->p_first     = emission_info.momentum_3bar_out();
        double_soft_info->p_aux_new   = emission_info.momentum_3_out();
        double_soft_info->p_aux_first = dipole().dipole_at_3bar_end()->particle_3bar();
        
        // We need to make sure that the "splitter" is set to use the 3bar 
        // end of the kj dipole. 
        // Recall that our convention is 3=emitter, 3bar=spectator.
        
        //LuSi-NOTE: CANNOT CHANGE THIS IN PANLOCAL
        //emission_info.do_split_emitter = false;

        // cache angular distances w dirdiff
        if (use_diffs()){
          // Use 2*(1-cos(theta)) = ddiff^2
          // a few shortcuts (v_i_j = j-i; b==bout)
          Momentum3<precision_type> v_1_n;    //= emission_info.d_radiation_wrt_spectator();
          Momentum3<precision_type> v_a_n;    //= emission_info.d_radiation_wrt_emitter();
          Momentum3<precision_type> v_1_1out; //= emission_info.d_spectator_out;
          Momentum3<precision_type> v_a_aout; //= emission_info.d_emitter_out;
          Momentum3<precision_type> v_1_a;    //= dipole().dirdiff_3_minus_3bar;
          Momentum3<precision_type> v_b_1;    //= dipole().dipole_at_3bar_end()->dirdiff_3_minus_3bar;

          if(emitter_is_3bar_end_of_dipole ()){
            v_1_n    = emission_info.d_radiation_wrt_emitter();
            v_a_n    = emission_info.d_radiation_wrt_spectator();
            v_1_1out = emission_info.d_emitter_out;
            v_a_aout = emission_info.d_spectator_out;
          } else {
            v_1_n    = emission_info.d_radiation_wrt_spectator();
            v_a_n    = emission_info.d_radiation_wrt_emitter();
            v_1_1out = emission_info.d_spectator_out;
            v_a_aout = emission_info.d_emitter_out;
          }
          v_1_a = dipole().dirdiff_3_minus_3bar;
          v_b_1 = dipole().dipole_at_3bar_end()->dirdiff_3_minus_3bar;
          
          //double_soft_info->dn1 = (v_1_n-v_1_1out).modpsq(); 
          //double_soft_info->dna = (v_a_n-v_a_aout).modpsq();
          //double_soft_info->dnb = (v_1_n+v_b_1).modpsq(); 
          //double_soft_info->d1a = (v_1_a+v_a_aout-v_1_1out).modpsq();  
          //double_soft_info->d1b = (v_b_1+v_1_1out).modpsq(); 
          //double_soft_info->dab = (v_1_a+v_a_aout+v_b_1).modpsq();

          vn1 = v_1_1out-v_1_n;
          vna = v_a_aout-v_a_n;
          vnb = -(v_1_n+v_b_1);
          v1a = v_1_a+v_a_aout-v_1_1out;
          v1b = -(v_b_1+v_1_1out);
          vab = -(v_1_a+v_a_aout+v_b_1);
        }
      }
    } else {     //g->qqbar splitting
      // For a quark spitting the choice of partner is fixed
      //
      //      splitting at 3 end             splitting at 3bar end
      //
      //         first     new                    new     first
      //             3   3bar                        3   3bar        
      //    splitn    \  /                            \  /    splitn            
      //    dipole     ||                              ||     dipole 
      //               ||                              ||            
      //               ||                              ||              
      //    3bar -----/  \----- 3          3bar  -----/  \----- 3        
      // b=aux_first         a=aux_new    a=aux_new           b=aux_first

      // This takes care of the case of a g->qqbar splitting of one of the
      // Born gluons in H->gg.

      if(emission_info.splitter_index() == born1 || emission_info.splitter_index() == born2) {
        return false;
      }
      // LuSi-NOTE: here, the splitter is always the emitter
      if(emission_info.splitter_is_3_end()){
        // we are emitting at the 3 end of the (aux_first,first)
        // dipole the emission should therefore be an anti-quark
        assert(emission_info.radiation_pdgid < 0);
        assert(dipole().particle_3().pdgid()==21);
        double_soft_info->p_first     = emission_info.momentum_3_out();
        double_soft_info->p_aux_new   = dipole().dipole_at_3_end()->particle_3();
        double_soft_info->p_aux_first = emission_info.momentum_3bar_out();
        //LuSi-NOTE: CANNOT CHANGE THIS IN PANLOCAL
        //emission_info.do_split_emitter = true;

        // cache angular distances w dirdiff
        if (use_diffs()){
          // Use 2*(1-cos(theta)) = ddiff^2
          const Momentum3<precision_type> &v_1_n    = emission_info.d_radiation_wrt_emitter();
          const Momentum3<precision_type> &v_b_n    = emission_info.d_radiation_wrt_spectator();
          const Momentum3<precision_type> &v_1_1out = emission_info.d_emitter_out;
          const Momentum3<precision_type> &v_b_bout = emission_info.d_spectator_out;
          const Momentum3<precision_type> &v_b_1    = dipole().dirdiff_3_minus_3bar;
          const Momentum3<precision_type> &v_1_a    = dipole().dipole_at_3_end()->dirdiff_3_minus_3bar;

          //double_soft_info->dn1 = (v_1_n-v_1_1out).modpsq(); 
          //double_soft_info->dna = (v_1_a-v_1_n).modpsq();
          //double_soft_info->dnb = (v_b_n-v_b_bout).modpsq(); 
          //double_soft_info->d1a = (v_1_a-v_1_1out).modpsq();  
          //double_soft_info->d1b = (v_b_1+v_1_1out-v_b_bout).modpsq(); 
          //double_soft_info->dab = (v_1_a+v_b_1-v_b_bout).modpsq(); 

          vn1 = v_1_1out-v_1_n;
          vna = v_1_a-v_1_n;
          vnb = v_b_bout-v_b_n;
          v1a = v_1_a-v_1_1out;
          v1b = v_b_bout-v_b_1-v_1_1out;
          vab = v_b_bout-v_1_a-v_b_1; 
        }
      } else {
        // we are emitting at the 3bar end of the (first,aux_first)
        // dipole the emission should therefore be a quark
        assert(emission_info.radiation_pdgid > 0);
        assert(dipole().particle_3bar().pdgid()==21);
        double_soft_info->p_first     = emission_info.momentum_3bar_out();
        double_soft_info->p_aux_new   = dipole().dipole_at_3bar_end()->particle_3bar();
        double_soft_info->p_aux_first = emission_info.momentum_3_out();
        //LuSi-NOTE: CANNOT CHANGE THIS IN PANLOCAL
        //emission_info.do_split_emitter = false;
        if (use_diffs()){
          // Use 2*(1-cos(theta)) = ddiff^2
          const Momentum3<precision_type> &v_1_n    = emission_info.d_radiation_wrt_spectator();
          const Momentum3<precision_type> &v_b_n    = emission_info.d_radiation_wrt_emitter();
          const Momentum3<precision_type> &v_1_1out = emission_info.d_spectator_out;
          const Momentum3<precision_type> &v_b_bout = emission_info.d_emitter_out;
          const Momentum3<precision_type> &v_1_b    = dipole().dirdiff_3_minus_3bar;
          const Momentum3<precision_type> &v_a_1    = dipole().dipole_at_3bar_end()->dirdiff_3_minus_3bar;

          //double_soft_info->dn1 = (v_1_n-v_1_1out).modpsq(); 
          //double_soft_info->dna = (v_a_1+v_1_n).modpsq();
          //double_soft_info->dnb = (v_b_n-v_b_bout).modpsq(); 
          //double_soft_info->d1a = (v_a_1+v_1_1out).modpsq();  
          //double_soft_info->d1b = (v_1_b-v_1_1out+v_b_bout).modpsq(); 
          //double_soft_info->dab = (v_1_b+v_b_bout+v_a_1).modpsq(); 

          vn1 = v_1_1out-v_1_n;
          vna = -(v_a_1+v_1_n);
          vnb = v_b_bout-v_b_n;
          v1a = -(v_a_1+v_1_1out);
          v1b = v_1_b-v_1_1out+v_b_bout; 
          vab = v_1_b+v_b_bout+v_a_1;
        }
      }
    }

    // distance wo dirdiff
    if (use_diffs()) {
      double_soft_info->dn1 = vn1.modpsq(); 
      double_soft_info->dna = vna.modpsq();
      double_soft_info->dnb = vnb.modpsq(); 
      double_soft_info->d1a = v1a.modpsq();  
      double_soft_info->d1b = v1b.modpsq(); 
      double_soft_info->dab = vab.modpsq();
      
      // dcross = dot(d1b*(v1a+v2a) - d1a*(v1b+v2b)).v12 = dot(vcross,v12)
      double_soft_info->dcross = -dot_product_3(double_soft_info->d1b*(v1a+vna)-double_soft_info->d1a*(v1b+vnb),vn1);
    } else {
      double_soft_info->dn1 = 2.0*one_minus_costheta(double_soft_info->p_new,     double_soft_info->p_first);
      double_soft_info->dna = 2.0*one_minus_costheta(double_soft_info->p_new,     double_soft_info->p_aux_new);
      double_soft_info->dnb = 2.0*one_minus_costheta(double_soft_info->p_new,     double_soft_info->p_aux_first);
      double_soft_info->d1a = 2.0*one_minus_costheta(double_soft_info->p_first,   double_soft_info->p_aux_new);
      double_soft_info->d1b = 2.0*one_minus_costheta(double_soft_info->p_first,   double_soft_info->p_aux_first);
      double_soft_info->dab = 2.0*one_minus_costheta(double_soft_info->p_aux_new, double_soft_info->p_aux_first);
      double_soft_info->dcross = double_soft_info->dna*double_soft_info->d1b
                               - double_soft_info->d1a*double_soft_info->dnb;
    }
    
    return true;
  }

  // analytic double soft approximation for this shower (shower and exact weights)
  void ShowerPanScaleLocal::Element::_double_soft_fill_weights(
      DoubleSoftInfo * double_soft_info) const{
    // shorthand
    DoubleSoftInfo &dsi = *double_soft_info;
    
    // compute all the necessary dot products
    // 
    // short-hand notation:
    //  n = new
    //  1 = first
    //  a = aux_new
    //  b = aux_first
    //
    //GS-NOTE-DS: this will probably need to be adapted for dirdiff
    precision_type dn1 = dsi.dn1;
    precision_type dna = dsi.dna;
    precision_type dnb = dsi.dnb;
    precision_type d1a = dsi.d1a;
    precision_type d1b = dsi.d1b;
    precision_type dab = dsi.dab;
    precision_type dcross = dsi.dcross;
    precision_type dnQ = 2.0*dot_product(dsi.p_new,       _event->Q())/dsi.p_new.E();
    precision_type d1Q = 2.0*dot_product(dsi.p_first,     _event->Q())/dsi.p_first.E();
    precision_type daQ = 2.0*dot_product(dsi.p_aux_new,   _event->Q())/dsi.p_aux_new.E();
    precision_type dbQ = 2.0*dot_product(dsi.p_aux_first, _event->Q())/dsi.p_aux_first.E();
    precision_type dQQ =     dot_product(_event->Q(),     _event->Q());
    precision_type zn  = dsi.p_new  .E()/(dsi.p_first.E()+dsi.p_new.E());
    precision_type z1  = dsi.p_first.E()/(dsi.p_first.E()+dsi.p_new.E());
    
    // compute the shower weights
    //
    // we compute all flavour and colour channel
    // Each of them receives contributions from 2 histories:
    //  - the one where "first" is emitted before "new"
    //  - the one where "new"   is emitted before "first"
    //
    // We have a total of 8 configurations to consider
    //  0: g->gg, colour aux_new - new - first - aux_first, "first" then "new"
    //  1: g->gg, colour aux_new - new - first - aux_first, "new" then "first"
    //  2: g->gg, colour aux_new - first - new - aux_first, "first" then "new"
    //  3: g->gg, colour aux_new - first - new - aux_first, "new" then "first"
    //  4: g->qq, colour aux_new - new - first - aux_first, "first" then "new"
    //  5: g->qq, colour aux_new - new - first - aux_first, "new" then "first"
    //  6: g->qq, colour aux_new - first - new - aux_first, "first" then "new"
    //  7: g->qq, colour aux_new - first - new - aux_first, "new" then "first"
    //
    // 2i and 2i+1 are summed.
    // i and i+2 are the same up to a swap of "new" and "first" in the
    // ME we therefore introduce a function that computes a single
    // colour flow since g->gg and g->qq only differ by a weight
    // factor, both are somputed at the same time
    // Default colour ordering for quarks is "swapped"
    //
    // Comments:
    // - the second call only differs from the first one by the inversion 
    //   of "first" (1) and "new" (n)
    // - note that, in the quark case, the first call set the "swapped" version. 
    //   In a nutshell, this is associated to our specific choice of dipole 
    //   structure where, for a quark (or antiquark) emission, the new emission 
    //   is associted with the previous or next dipole (instrad of the splitting 
    //   dipole)
    _double_soft_set_weight_shower(dn1, d1a, d1b, dna, dnb, dab,
                                   d1Q, dnQ, daQ, dbQ, dQQ, z1, zn,
                                   &dsi.shower_weight_gg_same,
                                   &dsi.shower_weight_qq_swapped);
                                   //&dsi.shower_weight_qq_same);
    _double_soft_set_weight_shower(dn1, dna, dnb, d1a, d1b, dab,
                                   dnQ, d1Q, daQ, dbQ, dQQ, zn, z1,
                                   &dsi.shower_weight_gg_swapped,
                                   &dsi.shower_weight_qq_same);
                                   //&dsi.shower_weight_qq_swapped);

    if(! _shower->qcd().gluon_uses_qqbar()){
      dsi.shower_weight_qq_same = dsi.shower_weight_qq_swapped = 0.0;
    }

    dsi.shower_weight_gg = dsi.shower_weight_gg_same + dsi.shower_weight_gg_swapped;
    dsi.shower_weight_qq = dsi.shower_weight_qq_same + dsi.shower_weight_qq_swapped;
    dsi.shower_weight = dsi.shower_weight_gg + dsi.shower_weight_qq;

    // compute the exact weights
    //
    // this is the full QCD double soft ME
    //
    // same-colour: a=a, b=n, c=1, d=b
    dsi.exact_weight_gg_same = pow(2.*_shower->qcd().CA(),2)
      * _shower->qcd().ant_sbc(dna, d1a, dab, dn1, dnb, d1b, zn, z1, dcross);
    // opposite colour: a=a, b=1, c=n, d=b
    dsi.exact_weight_gg_swapped = pow(2.*_shower->qcd().CA(),2)
      * _shower->qcd().ant_sbc(d1a, dna, dab, dn1, d1b, dnb, z1, zn, dcross);

    // old version (w extra 1/s12)
    // dsi.exact_weight_gg_same = pow(2.*_shower->qcd().CA(),2)
    //   * _shower->qcd().ant(dsi.p_aux_new, dsi.p_new, dsi.p_first, dsi.p_aux_first);
    // dsi.exact_weight_gg_swapped = pow(2.*_shower->qcd().CA(),2)
    //   * _shower->qcd().ant(dsi.p_aux_new, dsi.p_first, dsi.p_new, dsi.p_aux_first);
    
    if (_shower->qcd().gluon_uses_qqbar()){
      dsi.exact_weight_qq_same = dsi.exact_weight_qq_swapped = 
        16 * _shower->qcd().ant_qqbar_sqp_qbp(dab, d1a, dna, d1b, dnb, dn1, zn, z1, dcross);
      // old version without an sn1 factor
      //dsi.exact_weight_qq_same = dsi.exact_weight_qq_swapped = 
      //  16 * _shower->qcd().gamma_to_qqbarqqbar(dsi.p_aux_new, dsi.p_aux_first, dsi.p_first, dsi.p_new);
    } else {
      dsi.exact_weight_qq_same = dsi.exact_weight_qq_swapped = 0.0;
    }
    
    dsi.exact_weight_gg = dsi.exact_weight_gg_same + dsi.exact_weight_gg_swapped;
    dsi.exact_weight_qq = dsi.exact_weight_qq_same + dsi.exact_weight_qq_swapped;
    dsi.exact_weight = dsi.exact_weight_gg + dsi.exact_weight_qq;
  }
  
  /// analytic double soft approximation for this shower. 
  void ShowerPanScaleLocal::Element::_double_soft_set_weight_shower(
      precision_type &d12, precision_type &d1a,
      precision_type &d1b, precision_type &d2a,
      precision_type &d2b, precision_type &dab,
      precision_type &d1Q, precision_type &d2Q,
      precision_type &daQ, precision_type &dbQ,
      precision_type &dQQ,
      precision_type &z1, precision_type &z2,
      precision_type * weight_gg,
      precision_type * weight_qq) const{
    (*weight_gg) = 0.0;
    (*weight_qq) = 0.0;

    // compared to the previous version: ilkj => a21b
    const double CF = _shower->qcd().CF();
    const double nfTR = _shower->qcd().nf()*_shower->qcd().TR();
    const double beta = _shower->_beta;
    
    // history 1+3: 2 is emitted last, and both emissions are from the hard legs
    // As in logbook Eq. (122), which is identical to PanGlobal
    precision_type lnb1     = 0.5*log((daQ/dbQ) * (d1b/d1a));
    precision_type kappa1sq = d1b * pow2(z1*d1a+z2*d2a)/(d1a*dab);
    precision_type lnrho1   = 0.5*beta*log(daQ*dbQ/(dQQ*dab));
    precision_type lnv1 = 0.5*log(kappa1sq/dQQ) - lnrho1 - beta*fabs(lnb1);

    precision_type lnb2     = 0.5*log((daQ/d1Q) * (d12/d2a)); //the gluon(1) is the 3bar end
    precision_type kappa2sq = z2*z2*(d2a/d1a) * d12;
    precision_type lnrho2   = 0.5*beta*log(daQ*d1Q/(dQQ*d1a));
    precision_type lnv2 = 0.5*log(kappa2sq/dQQ) - lnrho2 - beta*fabs(lnb2); // factor log(E_{1+2}/Q) removed
    if(lnv2 < lnv1){
      // For this history, the Jacobian works out to be the same as PanGlobal
      precision_type jac = (dab/d1b)/(z2*d2a) * (d1a/(z1*d1a+z2*d2a));
      // when using "ant" for the exact QCD
      //precision_type jac = sab/(s1b * (s1a+s2a)) * s1a/(s2a*s12);

      precision_type sum  = z1*d1a+z2*d2a;
      precision_type zg   = z2*d2a/sum;   // 1 -z = pl.piTilde/(piTilde.pkTilde) ~ pl.pi/(pi.(pk+pl))
      precision_type omzg = z1*d1a/sum;
      precision_type gp2 = g_fcn( to_double(lnb2));
      precision_type gm2 = g_fcn(-to_double(lnb2));

      //GS-NOTE-DS: probably replace CF -> CA/2 (beyond large Nc)
      // LuSi-NOTE: This corresponds to the first term in the PanGlobal weight, the term
      // corresponding to the gluon splitting is handled separately (see history 2+4 below)
      // because it leads to different kinematics
      (*weight_gg) += jac * CF * gp2 * 2;
      (*weight_qq) += 0;

      if (! isfinite(*weight_qq)){
        std::cout << "history1+3" << std::endl;
        std::cout << "  lnb1 = " << lnb1 << ", kappa1sq = " << kappa1sq << ", lnrho1 = " << lnrho1 << ", lnv1 = " << lnv1 << std::endl;
        std::cout << "  lnb2 = " << lnb2 << ", kappa2sq = " << kappa2sq << ", lnrho2 = " << lnrho2 << ", lnv2 = " << lnv2 << std::endl;
        std::cout << "  zg = " << zg << ", omzg = " << omzg << ", jac = " << jac << std::endl;
        std::cout << "  gp = " << gp2 << ", gm = " << gm2 << endl;
        std::cout << "  --> weight_qq = " << (*weight_qq) << std::endl;
      }
    }

    // history 2+4: 2 is emitted last, and 2 is from splitting of 1
    // As in logbook Eq. (123), which is identical to PanGlobal
    //
    lnb1     = 0.5*log((daQ/dbQ) * ((z1*d1b+z2*d2b)*(z1*d1a+z2*d2a) - z1*z2*dab*d12) / pow2(z1*d1a+z2*d2a));
    kappa1sq = ((z1*d1b+z2*d2b)*(z1*d1a+z2*d2a) - z1*z2*dab*d12) / dab;
    lnrho1   = 0.5*beta*log(daQ*dbQ/(dQQ*dab));
    lnv1 = 0.5*log(kappa1sq/dQQ) - lnrho1 - beta*fabs(lnb1);

    lnb2     = 0.5*log((d2a/d1a)*((z1*d1a+z2*d2a)*(z1*d1Q+z2*d2Q)-z1*z2*d12*daQ) / (z1*z1*daQ*d12));
    kappa2sq = pow2(z1*z2/(z1*d1a+z2*d2a))*d1a*d2a*d12;
    lnrho2   = 0.5*beta*log((daQ/dQQ)*((z1*d1a+z2*d2a)*(z1*d1Q+z2*d2Q)-z1*z2*d12*daQ) / pow2(z1*d1a+z2*d2a));
    lnv2 = 0.5*log(kappa2sq/dQQ) - lnrho2 - beta*fabs(lnb2); // factor log(E_{1+2}/Q) removed
    if(lnv2 < lnv1){
      precision_type jac = (dab/d2a) * (z1*d1a+z2*d2a) / (z2*d2a) / ((z1*d1b+z2*d2b)*(z1*d1a+z2*d2a) - z1*z2*dab*d12);
      // when using "ant" for the exact QCD
      //precision_type jac = sab/(s1b * (s1a+s2a)) * s1a/(s2a*s12);

      precision_type sum  = z1*d1a+z2*d2a;
      precision_type zg   = z2*d2a/sum;   // 1 -z = pl.piTilde/(piTilde.pkTilde) ~ pl.pi/(pi.(pk+pl))
      precision_type omzg = z1*d1a/sum;
      precision_type gp2 = g_fcn( to_double(lnb2));
      precision_type gm2 = g_fcn(-to_double(lnb2));

      //GS-NOTE-DS: probably replace CF -> CA/2 (beyond large Nc)
      // LuSi-NOTE: This corresponds to the second term in PanGlobal
      (*weight_gg) += jac * CF * gp2 * (1 + pow3(omzg));
      (*weight_qq) += jac * gp2 * nfTR * zg*omzg*omzg;

      if (! isfinite(*weight_qq)){
        std::cout << "history2+4" << std::endl;
        std::cout << "  lnb1 = " << lnb1 << ", kappa1sq = " << kappa1sq << ", lnrho1 = " << lnrho1 << ", lnv1 = " << lnv1 << std::endl;
        std::cout << "  lnb2 = " << lnb2 << ", kappa2sq = " << kappa2sq << ", lnrho2 = " << lnrho2 << ", lnv2 = " << lnv2 << std::endl;
        std::cout << "  zg = " << zg << ", omzg = " << omzg << ", jac = " << jac << std::endl;
        std::cout << "  gp = " << gp2 << ", gm = " << gm2 << endl;
        std::cout << "  --> weight_qq = " << (*weight_qq) << std::endl;
      }
    }

    // history 5+7: where 2 is emitted before 1. Can be obtained from 1+3,
    // swapping a<-->b, 1<-->2 and the sign of lnb1 (which is however irrelevant)
    lnb1     = -0.5*log((dbQ/daQ) * (d2a/d2b));
    kappa1sq = d2a * pow2(z2*d2b+z1*d1b)/(d2b*dab);
    lnrho1   = 0.5*beta*log(daQ*dbQ/(dQQ*dab));
    lnv1 = 0.5*log(kappa1sq/dQQ) - lnrho1 - beta*fabs(lnb1);

    lnb2     = 0.5*log((dbQ/d2Q) * (d12/d1b)); //the gluon(1) is the 3bar end
    kappa2sq = z1*z1*(d1b/d2b) * d12;
    lnrho2   = 0.5*beta*log(daQ*d1Q/(dQQ*d1a));
    lnv2 = 0.5*log(kappa2sq/dQQ) - lnrho2 - beta*fabs(lnb2); // factor log(E_{1+2}/Q) removed
    if(lnv2 < lnv1){
      // For this history, the Jacobian works out to be the same as PanGlobal
      precision_type jac = (dab/d2a)/(z1*d1b) * (d2b/(z2*d2b+z1*d1b));
      // when using "ant" for the exact QCD
      //precision_type jac = sab/(s1b * (s1a+s2a)) * s1a/(s2a*s12);

      precision_type sum  = z2*d2b+z1*d1b;
      precision_type zg   = z1*d1b/sum;   // 1 -z = pl.piTilde/(piTilde.pkTilde) ~ pl.pi/(pi.(pk+pl))
      precision_type omzg = z2*d2b/sum;
      precision_type gp2 = g_fcn( to_double(lnb2));
      precision_type gm2 = g_fcn(-to_double(lnb2));

      //GS-NOTE-DS: probably replace CF -> CA/2 (beyond large Nc)
      // LuSi-NOTE: This corresponds to the first term in the PanGlobal weight, the term
      // corresponding to the gluon splitting is handled separately (see history 2+4 below)
      // because it leads to different kinematics
      (*weight_gg) += jac * CF * gp2 * 2;
      (*weight_qq) += 0;

      if (! isfinite(*weight_qq)){
        std::cout << "history5+7" << std::endl;
        std::cout << "  lnb1 = " << lnb1 << ", kappa1sq = " << kappa1sq << ", lnrho1 = " << lnrho1 << ", lnv1 = " << lnv1 << std::endl;
        std::cout << "  lnb2 = " << lnb2 << ", kappa2sq = " << kappa2sq << ", lnrho2 = " << lnrho2 << ", lnv2 = " << lnv2 << std::endl;
        std::cout << "  zg = " << zg << ", omzg = " << omzg << ", jac = " << jac << std::endl;
        std::cout << "  gp = " << gp2 << ", gm = " << gm2 << endl;
        std::cout << "  --> weight_qq = " << (*weight_qq) << std::endl;
      }
    }
    // history 6+8: where 2 is emitted before 1. Can be obtained from 2+4,
    // swapping a <--> b, 1 <--> 2 and changing the sign of lnb1 (which is however irrelevant)
    //
    lnb1     = -0.5*log((dbQ/daQ) * ((z2*d2a+z1*d1a)*(z2*d2b+z1*d1b) - z1*z2*dab*d12) / pow2(z2*d2b+z1*d1b));
    kappa1sq = ((z2*d2a+z1*d1a)*(z2*d2b+z1*d1b) - z1*z2*dab*d12) / dab;
    lnrho1   = 0.5*beta*log(daQ*dbQ/(dQQ*dab));
    lnv1 = 0.5*log(kappa1sq/dQQ) - lnrho1 - beta*fabs(lnb1);

    lnb2     = 0.5*log((d1b/d2b)*((z2*d2b+z1*d1b)*(z2*d2Q+z1*d1Q)-z1*z2*d12*dbQ) / (z2*z2*dbQ*d12));
    kappa2sq = pow2(z1*z2/(z2*d2b+z1*d1b))*d2b*d1b*d12;
    lnrho2   = 0.5*beta*log((dbQ/dQQ)*((z2*d2b+z1*d1b)*(z2*d2Q+z1*d1Q)-z1*z2*d12*dbQ) / pow2(z2*d2b+z1*d1b));
    lnv2 = 0.5*log(kappa2sq/dQQ) - lnrho2 - beta*fabs(lnb2); // factor log(E_{1+2}/Q) removed
    if(lnv2 < lnv1){
      precision_type jac = (dab/d1b) * (z2*d2b+z1*d1b) / (z1*d1b) / ((z2*d2a+z1*d1a)*(z2*d2b+z1*d1b) - z1*z2*dab*d12);
      // when using "ant" for the exact QCD
      //precision_type jac = sab/(s1b * (s1a+s2a)) * s1a/(s2a*s12);

      precision_type sum  = z2*d2b+z1*d1b;
      precision_type zg   = z1*d1b/sum;   // 1 -z = pl.piTilde/(piTilde.pkTilde) ~ pl.pi/(pi.(pk+pl))
      precision_type omzg = z2*d2b/sum;
      precision_type gp2 = g_fcn( to_double(lnb2));
      precision_type gm2 = g_fcn(-to_double(lnb2));

      //GS-NOTE-DS: probably replace CF -> CA/2 (beyond large Nc)
      // LuSi-NOTE: This corresponds to the second term in PanGlobal
      (*weight_gg) += jac * CF * gp2 * (1 + pow3(omzg));
      (*weight_qq) += jac * gp2 * nfTR * zg*omzg*omzg;

      if (! isfinite(*weight_qq)){
        std::cout << "history6+8" << std::endl;
        std::cout << "  lnb1 = " << lnb1 << ", kappa1sq = " << kappa1sq << ", lnrho1 = " << lnrho1 << ", lnv1 = " << lnv1 << std::endl;
        std::cout << "  lnb2 = " << lnb2 << ", kappa2sq = " << kappa2sq << ", lnrho2 = " << lnrho2 << ", lnv2 = " << lnv2 << std::endl;
        std::cout << "  zg = " << zg << ", omzg = " << omzg << ", jac = " << jac << std::endl;
        std::cout << "  gp = " << gp2 << ", gm = " << gm2 << endl;
        std::cout << "  --> weight_qq = " << (*weight_qq) << std::endl;
      }
    }

    // insert the factor 64 that was previously in the callee
    (*weight_gg) *= 64;
    (*weight_qq) *= 64;

    //The first one is always a soft gluon emission from a qqbar dipole
    //
    //GS-NOTE-DS: probably replace 2 CF -> CA (beyond large Nc), or
    //change the "exact" colour factors (for gg; qq seems already ok)
    (*weight_gg) *= 2 * CF;
    (*weight_qq) *= 2 * CF;
  }

  /// compute the probability for a flavour swap
  ///
  ///GS-NOTE-DS: we could think of filling em_info wo returnig the probability
  double ShowerPanScaleLocal::Element::_double_soft_swap_flavours_prob(
      typename ShowerBase::EmissionInfo * emission_info,
      DoubleSoftInfo * double_soft_info) const{
    // shorthand
    DoubleSoftInfo &dsi = *double_soft_info;

    double w_exact, w_shower;
    if(emission_info->radiation_pdgid == 21){
      w_shower = to_double(dsi.shower_weight_gg/dsi.shower_weight);
      w_exact  = to_double(dsi.exact_weight_gg /dsi.exact_weight );
    } else {
      w_shower = to_double(dsi.shower_weight_qq/dsi.shower_weight);
      w_exact  = to_double(dsi.exact_weight_qq /dsi.exact_weight );
    }
    return (w_shower > w_exact) ? 1.0 - w_exact/w_shower : 0.0;
  }

  /// compute the probabilities for a colour swap
  /// We have 2 probabilities depending on the flavour of the splitting
  ///
  ///GS-NOTE-DS: we could think of filling em_info wo passing the last 2 arguments
  void ShowerPanScaleLocal::Element::_double_soft_swap_colour_connections(
     typename ShowerBase::EmissionInfo * emission_info, DoubleSoftInfo * double_soft_info,
     double &colour_swap_prob_gg, double &colour_swap_prob_qq) const{

    // shorthand
    DoubleSoftInfo &dsi = *double_soft_info;

    // the shower weight involves the initial pdgid, i.e. the one
    // _before_ a potential flavour swap
    precision_type w_shower_old_same, w_shower_old;
    if(emission_info->radiation_pdgid == 21){
      w_shower_old_same    = dsi.shower_weight_gg_same;
      w_shower_old         = dsi.shower_weight_gg;
    } else {
      w_shower_old_same    = dsi.shower_weight_qq_same;
      w_shower_old         = dsi.shower_weight_qq;
    }

    // probability to swap from the "same" to the "swapped" colour
    // flow (in case the shower has generated too much of the "same"
    // flow). This hs to be computed for both the quark and gluon
    // channels (after a potential flavour swap)
    
    colour_swap_prob_gg = max(0.0, to_double(1.0 
      - (dsi.exact_weight_gg_same/dsi.exact_weight_gg)/(w_shower_old_same/w_shower_old)));
    colour_swap_prob_qq = max(0.0, to_double(1.0
      - (dsi.exact_weight_qq_same/dsi.exact_weight_qq)/(w_shower_old_same/w_shower_old)));

  }
  
} // namespace panscales
