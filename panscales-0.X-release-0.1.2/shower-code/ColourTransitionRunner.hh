//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
/// \file ColourTransitionRunner.hh
///
/// This file contains the code that handles the physics of the colour transitions.
///
/// See ColourTransitions.hh for more details


#ifndef __PANSCALES_COLOUR_TRANSITION_RUNNER_HH__
#define __PANSCALES_COLOUR_TRANSITION_RUNNER_HH__

#include <vector>
#include "ColourTransitions.hh"
#include "QCD.hh"
#include "Type.hh"
#include "Event.hh"

#include "fjcore_local.hh"

#include "ShowerBase.hh"
#include "LimitedWarning.hh"
#include <cassert>

namespace panscales{
  //------------------------------------------------------------------------
  /// @ingroup colour_classes
  /// \class ColourTransitionRunnerBase
  /// base class to handle the physics associated with colour transitions
  ///
  /// By default, this version does nothing (which is suited for the
  /// large-Nc case where CA=2CF).  Non-trivial strategies are to be
  /// implemented in derived classes.
  ///
  /// Physics note: g->qqbar splittings, always use a weight of 1,
  /// even if apparently in CF segment (both for segment and ME
  /// approach)
  ///
  class ColourTransitionRunnerBase{
  public:
    /// default ctor
    ColourTransitionRunnerBase(const QCDinstance &qcd) : _qcd(qcd) {}

    /// virtual dtor since we have virtual methods
    virtual ~ColourTransitionRunnerBase(){}

    /// add a description
    virtual std::string description() const{
      return "dummy colour scheme (valid at large Nc)";
    }
    
    /// get the acceptance probability due to colour associated w an emission
    ///
    /// The acceptance probability is meant to be used w accept_lazy().
    /// If needed, this should fill the following members of emission_info:
    ///
    ///   eta_approx     contains the approximate eta
    ///
    ///   eta_insertion  contains the "exact" value of eta to be
    ///                  inserted i transition point vectors when
    ///                  updating the event
    ///
    ///   colour_segment_index  the index in the colour transition
    ///                         vector where eta+approx belongs
    ///
    /// Arguments:
    ///  - event          reference to the full event
    ///  - element        refernce to the element that is splitting
    ///  - emission_info  info about the splitting (non-const because we cache extra info)
    ///
    /// Note: at the moment, ShowerRunner is coded so that the colour
    /// factor acceptance is applied after having computed the
    /// splitting kinematics. For most of the strategies, this is not
    /// needed but at least it allows for a uniform approach
    virtual double colour_factor_acceptance(const Event & event,
                                            const typename ShowerBase::Element &element,
                                            typename ShowerBase::EmissionInfo &emission_info){
      return 1.0;
    }
  
    /// get the acceptance probability due to colour associated w an
    /// emission when one compute the Sudakov associated w the first
    /// emission.
    ///
    /// Since the "first-emisison Sudakov" calculation does not
    /// compute the full kinematics, this decides the colour factor
    /// based on eta_approx.
    ///
    /// A warning is issued if another strategy is used and there are
    /// more than 2 particles in the initial event.
    ///
    /// See colour_factor_acceptance for more details
    virtual double colour_factor_acceptance_first_sudakov(const Event & event,
                                                          const typename ShowerBase::Element &element,
                                                          typename ShowerBase::EmissionInfo &emission_info){
      return 1.0;
    }

    /// update of the transition chains following an emission
    ///
    /// the arguments are
    ///  - a pointer to the event
    ///  - the index of the newly created dipole (splitting or non-splitting)
    ///  - the emission info
    ///
    /// Note that this method is called after the event structure has
    /// been updated. Conversely, the cached elements have not been
    /// updated so pointers to elements are still valid.
    ///
    /// It calls one of the methods below depending on
    /// the specific branching type and then updates the event.
    void update_transitions(Event *event,
                            int new_dipole_index,
                            typename ShowerBase::EmissionInfo & emission_info) const{
      // We have 6 potential cases:
      //  1. gluon insertion in a gluon segment
      //  2. gluon insertion in a quark segment
      //  3. gluon to qqbar at 3bar end of splitting dipole
      //  4. gluon to qqbar at 3    end of splitting dipole
      //  5. g_in -> q_in + qbar_out in the initial state (q    backwards evolutving to g)
      //  6. g_in -> qbar_in + q_out in the initial state (qbar backwards evolutving to g)
      //
      // IMPORTANT NOTE: the current implementation assumes that cases
      // 3 and 4 are the same for
      //    FSR   g -> q qbar
      // and
      //    ISR   q_in -> g_in + q_out   or   qbar_in -> g_in + qbar_out (g bkwds evolving to q(bar))
      //
      // As long as this is fine for the event structure, I think this
      // is now correct for colour (in particular for the value of the
      // new transition points where the existing dipole should get a
      // new transition point at eta=0 given that it becomes a
      // small-angle dipole). See notes for details.
      

      // get transition info from the emisison info
      // Here we use the exact eta
      unsigned int insertion_segment_index = emission_info.colour_segment_index;

      // get the splitting existing 
      auto & existing_dipole = event->dipoles()[emission_info.element()->dipole_index()];
      
      // first handle the cases with gluon radiation (1 & 2 above)
      if (emission_info.radiation_pdgid == 21){
        //
        auto & new_dipole      = event->dipoles()[new_dipole_index];
        ColourTransitionVector transitions_left, transitions_right;
        if (existing_dipole.is_quark_below_transition(insertion_segment_index)){
          // case 1: emission from a quark-like segment
          update_transitions_gluon_emission_from_quark
            (existing_dipole.colour_transitions,
             transitions_left, transitions_right, emission_info);
        } else {
          // Case 2: emission from a gluon-like segment
          update_transitions_gluon_emission_from_gluon
            (existing_dipole.colour_transitions,
             transitions_left, transitions_right, emission_info);
        }      
        existing_dipole.colour_transitions = std::move(transitions_right);
        new_dipole.colour_transitions      = std::move(transitions_left);

        return;
      }

      // now we need to handle the case of a quark emission
      //      
      // cases 3 and 4 correspond to a non-splitting new dipole
      if (new_dipole_index < -1){
        // first grab hold of the new (non-splitting) dipole
        auto & new_dipole  = event->non_splitting_dipoles()[-2-new_dipole_index];

        // decide if we're at the 3 or 3bar end of the dipole based on
        // the pdg id.  Since 3-3bar match final-states pdgid's we take
        // the decision based on the radiated particle so it does work
        // for both ISR & FSR.
        if (emission_info.radiation_pdgid > 0){
          // radiated a 3=quark => splitting at the 3bar end
          //
          // the "next dipole" just follows the newly created
          // (non-splitting) one
          auto & next_dipole = event->dipoles()[new_dipole.index_next];
          update_transitions_gluon2qqbar_at_dipole_3bar_end
            (existing_dipole.colour_transitions, next_dipole.colour_transitions,
             emission_info, next_dipole);
        } else {
          // radiated a 3bar=antiquark => splitting at the 3 end
          //
          // the "next dipole" just follows the newly created
          // (non-splitting) one
          auto & previous_dipole = event->dipoles()[new_dipole.index_previous];
          update_transitions_gluon2qqbar_at_dipole_3_end
            (existing_dipole.colour_transitions, previous_dipole.colour_transitions,
             emission_info, previous_dipole);
        }

        return;
      }
      
      // cases 5 and 6 are currently unimplemented
      // they correspond to a splitting new dipole
      assert(new_dipole_index >= 0);
      
      // first grab hold of the new (splitting) dipole
      auto & new_dipole  = event->dipoles()[new_dipole_index];

      // decide if we're at the 3 or 3bar end of the dipole based on
      // the pdg id. Given our convention for the initial state, the
      // 3-3bar status is the same as the pdg id of the radiated parton
      if (emission_info.radiation_pdgid > 0){
        // radiated a 3=quark => splitting at the 3 (qbar) end
        update_transitions_backwards_qbar2gluon(existing_dipole.colour_transitions,
                                                new_dipole.colour_transitions,
                                                emission_info);
      } else {
        // radiated a 3bar=antiquark => splitting at the 3bar (q) end
        update_transitions_backwards_q2gluon(existing_dipole.colour_transitions,
                                             new_dipole.colour_transitions,
                                             emission_info);
      }

    }
    

    
    /// split the current "existing" chain following a gluon emission from a quark segment
    /// 
    /// The split breaks the chain into a left chain (on the 3bar end)
    /// and a right chain (At the quark end) [In the current shower
    /// implementation, the left one would correspond to the new
    /// dipole 3bar-g and the right one would correspond to the
    /// existng g-3 dipole after emission]
    ///
    /// The "existing_transitions" vectir is non-const because the
    /// updates use some "move" operations for optimisation
    ///
    /// existing_transitions : colour transitions in the initial/existing dipole 
    /// left_transitions     : colour transitions in the left (3bar-g) dipole (set by this function)
    /// right_transitions    : colour transitions in the right (g-3)   dipole (set by this function)
    /// emission_info        : details about the emisison (incl eta_insertion and colour_segment_index)
    virtual void update_transitions_gluon_emission_from_quark(ColourTransitionVector &existing_transitions,
                                                              ColourTransitionVector &left_transitions,
                                                              ColourTransitionVector &right_transitions,
                                                              typename ShowerBase::EmissionInfo & emission_info) const{
      return;
    }
  
    /// split the current "existing" chain following a gluon emission from a gluon segment
    /// 
    /// The split breaks the chain into a left chain (on the 3bar end)
    /// and a right chain )At the quark end) [In the current shower
    /// implementation, the left one would correspond to the new
    /// dipole 3bar-g and the right one would correspond to the
    /// existing g-3 dipole after emission]
    ///
    /// The "existing_transitions" vectir is non-const because the
    /// updates use some "move" operations for optimisation
    ///
    /// existing_transitions : colour transitions in the initial/existing dipole 
    /// left_transitions     : colour transitions in the left (3bar-g) dipole (set by this function)
    /// right_transitions    : colour transitions in the right (g-3)   dipole (set by this function)
    /// emission_info        : details about the emisison (incl eta_insertion and colour_segment_index)
    virtual void update_transitions_gluon_emission_from_gluon(ColourTransitionVector &existing_transitions,
                                                              ColourTransitionVector &left_transitions,
                                                              ColourTransitionVector &right_transitions,
                                                              typename ShowerBase::EmissionInfo & emission_info) const{
      return;
    }
  
    /// update the current "existing" chain and the previous dipole
    /// chain following a g->qqbar splitting at the 3 end of the
    /// gluon. The existing dipole will receive the quark resulting
    /// from the splitting. The "previous" dipole will receive the
    /// qbar product of the splitting.
    /// 
    /// existing_transitions : colour transitions in the existing dipole (updated by this function)
    /// previous_transitions : colour transitions in the previous dipole (updated by this function)
    /// emission_info        : details about the emisison (incl eta_insertion and colour_segment_index)
    /// previous_dipole      : (const) reference to the previous dipole (useful for kinematics)
    virtual void update_transitions_gluon2qqbar_at_dipole_3_end(ColourTransitionVector &existing_transitions,
                                                                ColourTransitionVector &previous_transitions,
                                                                typename ShowerBase::EmissionInfo & emission_info,
                                                                const Dipole & previous_dipole) const {
      return;
    }
  
    /// update the current "existing" chain and the next dipole chain
    /// following a g->qqbar splitting at the 3bar end of the
    /// gluon. The existing dipole will receive the qbar resulting
    /// from the splitting. The "next" dipole will receive the quark
    /// product of the splitting.
    /// 
    /// existing_transitions : colour transitions in the existing dipole (updated by this function)
    /// next_transitions     : colour transitions in the next dipole (updated by this function)
    /// emission_info        : details about the emisison (incl eta_insertion and colour_segment_index)
    /// next_dipole          : (const) reference to the next dipole (useful for kinematics)
    virtual void update_transitions_gluon2qqbar_at_dipole_3bar_end(ColourTransitionVector &existing_transitions,
                                                                   ColourTransitionVector &next_transitions,
                                                                   typename ShowerBase::EmissionInfo & emission_info,
                                                                   const Dipole & next_dipole) const{
      return;
    }

    /// update the current "existing" chain and the previous dipole
    /// chain following a g_in->qbar_in + q_out splitting at the 3 end
    /// of the gluon. The existing dipole will replace the qbar by the
    /// gluon and the newly created dipole (next of existing) will
    /// have the gluon at is 3bar end and the emitted final-state
    /// quark at its 3 end
    virtual void update_transitions_backwards_qbar2gluon(ColourTransitionVector &existing_transitions,
                                                         ColourTransitionVector &new_transitions,
                                                         typename ShowerBase::EmissionInfo & emission_info) const {
      return;
    }

    /// update the current "existing" chain and the previous dipole
    /// chain following a g_in->q_in + qbar_out splitting at the 3bar
    /// end of the gluon. The existing dipole will replace the q by
    /// the gluon and the newly created dipole (next of existing) will
    /// have the gluon at is 3 end and the emitted final-state
    /// quark at its 3bar end
    virtual void update_transitions_backwards_q2gluon(ColourTransitionVector &existing_transitions,
                                                      ColourTransitionVector &new_transitions,
                                                      typename ShowerBase::EmissionInfo & emission_info) const {
      return;
    }

  protected:
    const QCDinstance & _qcd;

    static LimitedWarning _warning_incomplete_colour_for_isr;
  };

  //------------------------------------------------------------------------
  /// @ingroup colour_classes
  /// \class ColourTransitionRunnerEmitter
  /// class to handle colour factors when they are taken as the Casimir of the emitter
  ///
  /// See ColourTransitionRunnerBase for longer descriptions of each
  /// the methods inherited here.
  ///
  /// In our papers, this is referred to as CFFE (colour-factor from emitter)
  ///
  class ColourTransitionRunnerEmitter : public ColourTransitionRunnerBase{
  public:
    /// default ctor
    ColourTransitionRunnerEmitter(const QCDinstance &qcd)
      : ColourTransitionRunnerBase(qcd){
      // not in Base because Base provides a "dummy" option valid at large Nc
      _two_CF_over_CA = 2*qcd.CF()/qcd.CA();
    }
  
    /// virtual dtor since we have virtual methods
    virtual ~ColourTransitionRunnerEmitter(){}

    /// add a description
    virtual std::string description() const override{
      return "colour factor from emitter";
    }

    /// get the acceptance probability due to colour associated w an emission
    /// see ColourTransitionRunnerBase for details
    virtual double colour_factor_acceptance(const Event & event,
                                            const typename ShowerBase::Element &element,
                                            typename ShowerBase::EmissionInfo &emission_info) override{
      // we first need to figure out which is the "real" emitter,
      // emitter or spectator (in antenna showers, the emission can
      // come from the particle labelled as spectator)
      bool is_quark;
      if (emission_info.do_split_emitter){
        is_quark = !(element.emitter().pdgid() == 21);
      } else {
        is_quark = !(element.spectator().pdgid() == 21);
      }

      //----------------------------------------------------------------------
      // #TRK_ISSUE-21 In principles we should make sure that the backwards
      // evolution if a quark into a gluon gets the right colour
      // acceptance of 1. This is determined by looking at the radiated
      // PDGId. I.e.
      //      
      //   // for quark radiation: the colour factor is 1
      //   if (emission_info.radiation_pdgid != 21) is_quark=false;
      //
      // The issue comes from the fact that
      // colour_factor_acceptance_first_sudakov (defined below) calls
      // this function. As of 2023-11-16, the radiation_pdgid is
      // available but it involves a random selection. This would
      // therefore not work if we want to integrate the first Sudakov
      // using Gaussian quadrature. The solution there would be to
      // avoid the randomness by performing explicitly a weighted sum
      // over teh splitter and flavour. For now , we leave this for a
      // future upgrade of the code.
      //
      // Getting this of the randomness would not only allow us to
      // insert the 2 lines above, but it would also allow us to get
      // rid of the colour_factor_acceptance_first_sudakov in this ALL
      // the colour treatments (including in
      // ColourTransitionRunnerSegment (used also by NODS) below.
      //
      //----------------------------------------------------------------------
      
      emission_info.eta_approx = 0.0;         // dummy
      emission_info.eta_insertion = 0.0;      // dummy
      emission_info.colour_segment_index = 0; // dummy
      return is_quark ? _two_CF_over_CA : 1.0;
    }

    virtual double colour_factor_acceptance_first_sudakov(const Event & event,
                                                          const typename ShowerBase::Element &element,
                                                          typename ShowerBase::EmissionInfo &emission_info) override{
      // same as for the standard acceptance
      return colour_factor_acceptance(event, element, emission_info);
    }

  protected:
    double _two_CF_over_CA;      ///< 2CF/CA

  };

  //------------------------------------------------------------------------
  /// @ingroup colour_classes
  /// \class ColourTransitionRunnerSegment
  /// class to handle colour factors from the Casimir of segments in a ColourTransitionVector
  ///
  /// See ColourTransitionRunnerBase for longer descriptions of each
  /// of the methods inherited here.
  class ColourTransitionRunnerSegment : public ColourTransitionRunnerBase{
  public:
    /// default ctor
    ColourTransitionRunnerSegment(const QCDinstance &qcd)
      : ColourTransitionRunnerBase(qcd){
      // not in Base because Base provides a "dummy" option valid at large Nc
      _two_CF_over_CA = 2*qcd.CF()/qcd.CA();
    }

    /// virtual dtor since we have virtual methods
    virtual ~ColourTransitionRunnerSegment(){
    }

    /// add a description
    virtual std::string description() const override{
      return "colour factor from transition segments";
    }

    /// get the acceptance probability due to colour associated w an emission
    /// see ColourTransitionRunnerBase for details
    virtual double colour_factor_acceptance(const Event & event,
                                            const typename ShowerBase::Element &element,
                                            typename ShowerBase::EmissionInfo &emission_info) override{
      // grab simple info
      const Dipole & dipole = event.dipoles()[element.dipole_index()];
      const ColourTransitionVector & transitions = dipole.colour_transitions;

      // cache eta_approx once and for all
      double eta_approx = element.eta_approx_wrt_3(emission_info.lnv, emission_info.lnb);
      emission_info.eta_approx = eta_approx;
      emission_info.colour_segment_index = transitions.get_transition_index_above(eta_approx);

      // compute the insertion eta
      //
      // Note that it is forced to stay within the same colour segment
      // so no need for an update of the transition point index
      //
      // It's very tempting to call this just before updating the
      // transitions. However, at that later stage, we do not have an
      // (easy) access to the dipole which is required to compute the
      // exact eta.
      emission_info.eta_insertion = _compute_insertion_eta_wrt_3(dipole, emission_info);

      // with no transition points, the acceptance is just given by colour factors
      if (transitions.size() == 0){
        bool is_quark = transitions.is_3bar_end_CF();
        // for quarks radiating quarks (which happens for ISR), our
        // convention (and normalisation of the splitting functions)
        // is that the colour acceptance is 1 (see also comment below)
        emission_info.colour_segment_index = 0;
        bool colour_is_quark = is_quark && (emission_info.radiation_pdgid == 21);
        return colour_is_quark ? _two_CF_over_CA : 1.0;
      }

      // for a g->qqbar splitting, always return 1
      //
      // WATCH OUT: the fact that this is 1 is connected with our
      // choice of normalisation of the splitting functions
      // (e.g. 2CF/CA for q_is->g_is + q_fs ISR). Do not change this
      // without also checking the normlisation of the splitting
      // functions in QCD.hh.
      if (emission_info.radiation_pdgid != 21){
        return 1.0;
      }

      // for a gluon emission, decide the colour based on the Casimir
      // of the segment
      bool is_quark = transitions.is_quark_below_transition(emission_info.colour_segment_index);
      return is_quark ? _two_CF_over_CA : 1.0;
    }

    //--------------------------------------------------
    /// get the acceptance probability due to colour associated w an
    /// see ColourTransitionRunnerBase for details
    virtual double colour_factor_acceptance_first_sudakov(const Event & event,
                                                          const typename ShowerBase::Element &element,
                                                          typename ShowerBase::EmissionInfo &emission_info) override{

      // NOTE: if, in the case of the Sudakov computatios, we fix the
      // generation in ShowerRunner so that the emitter/spectator and
      // the radiated pdgID are summed over (rather than randomly
      // selected) we can get rid of
      // colour_factor_acceptance_first_sudakov altogether (in other
      // classes as well). See #TRK_ISSUE-21 above for extra details.
      
      // WATCH OUT: in principle we'd want quark radiation to be
      // associated with a colour factor of 1. In
      // colour_factor_acceptance we enforce that by explicitly
      // checking the PDGId of the radiated particle. Unfortunately,
      // this information is not available when computing the first
      // sudakov (essentially because ShowerRunner does not call
      // _select_channel_details). We could still use the information
      // in emission_info to deduce the probability to radiate a quark
      // based on the following info:
      //
      //  - Pem is the probability that the emitter splits
      //
      //  - P(q_rad|em) is the probability that we radiate a quark if
      //    we split at the emitter end
      //
      //  - P(q_rad|sp) is the probability that we radiate a quark if
      //    we split at the spectator end
      //
      // In this case we get the overall probability to radiate a
      // quark to be
      //
      //   Pq = Pem * P(q_rad|em) + (1-Pem) * P(q_rad|sp)
      //
      //   Pg = 1 - Pq
      //
      // For the Pq part, we use a colour factor of 1 and for the Pg
      // part, we deduce the colour factor from the segments
      //
      // Note that Pem is available as emission_info::emitter_splitting_weight
      

      // with no transition points, the acceptance is just given by colour factors
      const Dipole & dipole = event.dipoles()[element.dipole_index()];
      const ColourTransitionVector & transitions = dipole.colour_transitions;

      // first decide whether the emission comes from a quark or a gluon line
      bool is_quark;
      if (transitions.size() == 0){
        is_quark = transitions.is_3bar_end_CF();
      } else {
        double eta_approx = element.eta_approx(emission_info.lnv, emission_info.lnb);
        if (!element.emitter_is_quark_end_of_dipole()){ eta_approx = -eta_approx;}
        emission_info.eta_approx = eta_approx;
        emission_info.colour_segment_index = transitions.get_transition_index_above(eta_approx);
        is_quark = transitions.is_quark_below_transition(emission_info.colour_segment_index);
      }

      // for gluons, we always either we have a gluon emission with
      // a colour acceptance of 1, or we emit a quark also with an
      // acceptance of 1
      if (!is_quark) return 1.0;
      
      // for quarks:
      //   gluon radiation would come with an acceptance of 2CF/CA
      //   quark radiation would come with an acceptance of 1
      // we weight the two cases based on the probabilities available in emission_info
      //
      double w_emit_rad_g = emission_info.emitter_weight_rad_gluon;
      double w_emit_rad_q = emission_info.emitter_weight_rad_quark;
      double w_spec_rad_g = emission_info.spectator_weight_rad_gluon;
      double w_spec_rad_q = emission_info.spectator_weight_rad_quark;
      double w_emit_rad   = w_emit_rad_g + w_emit_rad_q;
      double w_spec_rad   = w_spec_rad_g + w_spec_rad_q;

      double P_emit_split = emission_info.emitter_splitting_weight;
      double P_spec_split = 1-P_emit_split;

      // now the relative q/g probabilities for either the emitter or the spectator
      double P_rad_q_if_emit = (w_emit_rad==0.0) ? 0.0 : w_emit_rad_q/w_emit_rad;
      double P_rad_g_if_emit = 1-P_rad_q_if_emit;  // this would anyway contribute 0 later

      double P_rad_q_if_spec = (w_spec_rad==0.0) ? 0.0 : w_spec_rad_q/w_spec_rad;
      double P_rad_g_if_spec = 1-P_rad_q_if_spec;  // this would anyway contribute 0 later
      
      // The colour acceptance is
      //
      //   Pcolour = P(em splits) (P(rad=q|em splits) 1 + P(rad=g|em splits) 2CF/CA)
      //           + P(sp splits) (P(rad=q|sp splits) 1 + P(rad=g|sp splits) 2CF/CA)
      return P_emit_split * (P_rad_q_if_emit + P_rad_g_if_emit * _two_CF_over_CA)
        +    P_spec_split * (P_rad_q_if_spec + P_rad_g_if_spec * _two_CF_over_CA);
    }

    //--------------------------------------------------
    /// split the current "existing" chain following a gluon emission from a quark segment    
    /// 
    /// see ColourTransitionRunnerBase for details
    virtual void update_transitions_gluon_emission_from_quark(ColourTransitionVector &existing_transitions,
                                                              ColourTransitionVector &left_transitions,
                                                              ColourTransitionVector &right_transitions,
                                                              typename ShowerBase::EmissionInfo & emission_info) const override{
      // The chain (*)
      //    [CR; (eta0, ..., eta_{i-1}, eta_i, ..., eta_{n-1})]  (3bar-3)
      // should be broken into
      //    [CR; (eta0, ..., eta_{i-1}, max(0,eta)]    (3bar-g: left  dipole)
      //    [CA; (min(0,eta), eta_i, ..., eta_{n-1})]  (g-3   : right dipole)
      //
      // In the current shower implementation, the "left" corresponds
      // to the new dipole, the "right" to the existing one
      //
      // For the left  dipole, the inserted transition point auxiliary variable is 3 
      // For the right dipole, the inserted transition point auxiliary variable is 3bar
      //
      // (*) note that the () labels refer to the left-right in the
      //     chain and are inverted wrt to the dipole, i.e. we start w
      //     the qbar then the q
      //
      // Special cases: eta_insertion can be outside the
      // eta_{i-1}--eta_i range, in which case we simply delete the
      // points.
      // For max(0,eta) <= eta_{i-1}, we get
      //    [CR; (eta0, ..., eta_{i-2}]                (3bar-g: left  dipole)
      //    [CA; (min(0,eta), eta_i, ..., eta_{n-1})]  (g-3   : right dipole)
      // and for min(0,eta) >= eta_i, we get
      //    [CR; (eta0, ..., eta_{i-1}, max(0,eta)]    (3bar-g: left  dipole)
      //    [CA; (eta_{i+1}, ..., eta_{n-1})]          (g-q   : right dipole)
      //
      // GPS example: insert into CF segment of following array, but 
      // with eta_insertion = -3
      //    [-infty; CA; -2 ; CF; 8; CA; infty] aka [CA; -2, 8]
      // ->
      //    [-infty; CA ; infty]                aka [CA]   
      //    [-infty; CA; -3; CF; 8; CA; infty]  ala [CA; -3, 8]

      double eta_transition = emission_info.eta_insertion;
      unsigned int isegment = emission_info.colour_segment_index;

      double eta_L = std::max(eta_transition, 0.0);
      double eta_R = std::min(eta_transition, 0.0);

      // create the new transition points
      //
      // this "segment" method does not need any info about
      // auxiliaries. However, for code readability, we want to avoid
      // repeating this whole function twice. We therefore introduce a
      // method that creates the transition points w or wo auxiliary
      // info. They may not be both used but we have not noticed any
      // sizeable impact on timings
      //
      // Info about auxiliary variables is given in the NODS class
      // below.
      ColourTransitionPoint transition_L, transition_R;
      _create_transitions_gluon_emission_from_quark(
                   emission_info, eta_L, eta_R, transition_L, transition_R);
      
      if (!existing_transitions.eta_above_segment_L(eta_L, isegment)) {
        // if eta_L is not above the the lower (left-hand) edge of
        // this segment, do not include this segment and do not
        // include a transition_L point in left_transitions
        assert(isegment >= 1);
        existing_transitions.split(left_transitions, isegment-1, nullptr, 
                                   &transition_R, isegment, right_transitions);
      } else if (!existing_transitions.eta_below_segment_R(eta_R, isegment)) {
        // if eta_R is to the right of the upper (right-hand) edge of
        // this segment, do not include this segment and do not
        // include a transition_R point in right_transitions
        // (NB: this and the previous condition cannot occur
        // simultaneously because eta_L >= eta_R and the segment
        // edges are ordered.
        existing_transitions.split(left_transitions, isegment, &transition_L, 
                                   nullptr, isegment+1, right_transitions);
      } else {
        existing_transitions.split(left_transitions, isegment, &transition_L, 
                                   &transition_R, isegment, right_transitions);
      }
            
      return;
    }

    /// split the current "existing" chain following a gluon emission from a gluon segment
    /// 
    /// see ColourTransitionRunnerBase for details
    virtual void update_transitions_gluon_emission_from_gluon(ColourTransitionVector &existing_transitions,
                                                              ColourTransitionVector &left_transitions,
                                                              ColourTransitionVector &right_transitions,
                                                              typename ShowerBase::EmissionInfo & emission_info) const override{
      // The chain
      //    [CR; (eta0, ..., eta_{i-1}, eta_i, ..., eta_{n-1})]  (qbar-q)
      // should be broken into
      //    [CR; (eta0, ..., eta_{i-1}]    (qbar-g: left  dipole)
      //    [CA; (eta_i, ..., eta_{n-1})]  (g-q   : right dipole)
      //
      // In the current shower implementation, the "left" corresponds
      // to the new dipole, the "right" to the existing one
      //
      // Algorithmically, it is likely cheaper to
      //  1. move all the existing into new
      //  2. redefine existing (by moving the end of new)
      //  3. resize new
      unsigned int isegment = emission_info.colour_segment_index;

      // split things up
      existing_transitions.split(left_transitions, isegment, nullptr, 
                                 nullptr, isegment, right_transitions);
    }

    /// update the current "existing" chain and the previous dipole
    /// chain following a g->qqbar splitting at the 3 (quark) end of
    /// the gluon
    /// 
    /// existing_transitions    : colour transitions in the initial/existing dipole (updated by this function)
    /// previous_transitions    : colour transitions in the previous dipole (updated by this function)
    /// emission_info           : details about the emisison (incl eta_insertion and colour_segment_index)
    /// index_3bar              : particle index of the "emitted" 3bar (qbar) from the g->qqbar splitting
    /// index_3                 : particle index of the "emitted" 3    (q)    from the g->qqbar splitting
    virtual void update_transitions_gluon2qqbar_at_dipole_3_end(ColourTransitionVector &existing_transitions,
                                                                ColourTransitionVector &previous_transitions,
                                                                typename ShowerBase::EmissionInfo & emission_info,
                                                                const Dipole & previous_dipole) const override{ 
      // Assume we have dipoles
      //   (3,g)     (previous)
      //   (g,3bar)  (existing --- splitting at 3 end of g)
      // with a g->q' qbar' splitting
      // In the ideal/basic setup (where the splitting is collinear), the chains 
      //    [CR; (eta_{1,0}, ..., eta_{1,n1-1}] (3bar-g: existing dipole)
      //    [CA; (eta_{2,0}, ..., eta_{2,n2-1}] (g-3:    previous dipole)
      // should be replaced by
      //    [CR; (eta_{1,0}, ..., eta_{1,n1-1}, |eta|]  (3bar-q': existing dipole)
      //    [CF; (-|eta|, eta_{2,0}, ..., eta_{2,n2-1}] (qbar'-3: previous dipole)
      //
      // However, extra care is needed because the insertion could
      // happen in a segment which is not at the end of the colour
      // transition chain.  If this is the case, we make the point
      // collapse w the last pre-existing transition point i.e. we
      // remove the last transition point wo adding a new one (the CF
      // - CA/2 alternance is maintained)
      //
      // Existing dipole:
      //  . insertion above the last transition point (|eta| > eta_{n1-1}):
      //        [CR; (eta_{1,0}, ..., eta_{n1-1})]          (3bar-g : existing dipole)
      //     -> [CR; (eta_{1,0}, ..., eta_{n1-1}, |eta|)]   (3bar-q': existing dipole)
      //
      //  . insertion below the last transition point (|eta| <= eta_{n1-1}):
      //        [CR; (eta_{1,0}, ..., eta_{n1-1}]           (3bar-g : existing dipole)
      //     -> [CR; (eta_{1,0}, ..., eta_{n1-2}]           (3bar-q': existing dipole)
      // 
      // Previous dipole:
      //  . insertion below the first transition point (-|eta| < eta_{2,0}):
      //        [CA; (eta_{2,0}, ..., eta_{2,n2-1}]         (g    -3: previous dipole)
      //        [CF; (-|eta|, eta_{2,0}, ..., eta_{2,n2-1}] (qbar'-3: previous dipole)
      //
      //  . insertion above the first transition point (-|eta| >= eta_{2,0}):
      //        [CA; (eta_{2,0}, ..., eta_{2,n2-1}]         (g    -3: previous dipole)
      //        [CF; (eta_{2,1}, ..., eta_{2,n2-1}]         (qbar'-3: previous dipole)

      // if we want the resulting quarks to behave like gluons CA/2
      // instead of CF), just do not insert the transition points
      //
      // This is helpful to assess the size of the NLL multiplicity mismatch
      if (_qcd.colour_no_g2qqbar()) return;

      double eta_insertion = emission_info.eta_insertion;

      // first handle the existing dipole
      double abseta = std::abs(eta_insertion);

      // alternative solution for NLL multiplicity mismatch
      //
      // to mimic a situation where, after a g->qqbar splitting,
      // radiation close to the quark behaves w a CA/2 factor, we send
      // the transition point to infinite rapidity
      //
      // if (_qcd.colour_no_g2qqbar()){
      //   //std::cout << "g->qqbar sets a dummy transition point" << std::endl;
      //   abseta = std::numeric_limits<double>::max();
      // }

      // check if we have a normal insertion or an insertion before
      // the last pre-existing transition point
      if (!existing_transitions.eta_above_segment_L(abseta, existing_transitions.size())) {
        // collapse w the end
        existing_transitions.pop_back();
      } else {
        ColourTransitionPoint transition_point = _create_transition_existing_gluon2qqbar_3_end(emission_info,
                                                                                               abseta);
        existing_transitions.push_back(transition_point);
      }
      // Safety assertion
      assert(existing_transitions.is_3_end_CF());

      // update the previous dipole transitions
      //
      // search insertion point
      double minus_abseta = -abseta;

      // check if we have a normal insertion or an insertion after
      // the first pre-existing transition point
      if (!previous_transitions.eta_below_segment_R(minus_abseta, 0)) {
        // collapse w the end
        previous_transitions.pop_front();
      } else {
        ColourTransitionPoint transition_point = _create_transition_previous_gluon2qqbar_3_end(emission_info,
                                                                                               minus_abseta,
                                                                                               previous_dipole);
        previous_transitions.push_front(transition_point);
      }
      // Safety assertion
      assert(previous_transitions.is_3bar_end_CF());
    }

    /// update the current "existing" chain and the next dipole chain
    /// following a g->qqbar splitting at the 3bar (anti-quark) end of
    /// the gluon
    /// 
    /// existing_transitions    : colour transitions in the initial/existing dipole (updated by this function)
    /// next_transitions        : colour transitions in the next dipole (updated by this function)
    /// emission_info           : details about the emisison (incl eta_insertion and colour_segment_index)
    /// index_3bar              : particle index of the "emitted" 3bar (qbar) from the g->qqbar splitting
    /// index_3                 : particle index of the "emitted" 3    (q)    from the g->qqbar splitting
    virtual void update_transitions_gluon2qqbar_at_dipole_3bar_end(ColourTransitionVector &existing_transitions,
                                                                   ColourTransitionVector &next_transitions,
                                                                   typename ShowerBase::EmissionInfo & emission_info,
                                                                   const Dipole & next_dipole) const override{
      // Assume we have dipoles
      //   (3,g)     (existing --- splitting at 3bar end of g)
      //   (g,3bar)  (next)
      // with a g->q' qbar' splitting
      // In the ideal/basic setup (where the splitting is collinear), the chains 
      //    [CR; (eta_{1,0}, ..., eta_{1,n1-1}] (3bar-g: next dipole)
      //    [CA; (eta_{2,0}, ..., eta_{2,n2-1}] (g-3:    existing dipole)
      // should be replaced by
      //    [CR; (eta_{1,0}, ..., eta_{1,n1-1}, |eta|]  (3bar-q': next dipole)
      //    [CF; (-|eta|, eta_{2,0}, ..., eta_{2,n2-1}] (qbar'-3: existing dipole)
      //
      // Extra care is needed because the insertion could happen in a
      // segment which is not at the end of the colour transition
      // chain.  If this is the case, we just remove the last
      // transition points so as to guarantee that the CF - CA/2
      // alternance is maintained
      //
      // The situation is the same as for the insertion of a g->qqbar
      // splitting at the quark end of a dipole up to
      //   - the "existing" dipole there is the "next" dipole here
      //   - the "previous" dipole there is the "existing" dipole here
      //   - the searches for the eta insertion points should reflect
      //     this properly: the insertion for the "next" dipole should be
      //     computed explicitly and the insertion of "-|eta|" for the
      //     existing dipole should only be recomputed for +ve eta (for
      //     -ve eta, -|eta|=eta)

      // if we want the resulting quarks to behave like gluons CA/2
      // instead of CF), just do not insert the transition points
      //
      // This is helpful to assess the size of the NLL multiplicity mismatch
      if (_qcd.colour_no_g2qqbar()) return;

      double eta_insertion = emission_info.eta_insertion;

      // first handle the "next" dipole
      double abseta = std::abs(eta_insertion);

      // check if we have a normal insertion or an insertion before
      // the last pre-existing transition point
      if (!next_transitions.eta_above_segment_L(abseta, next_transitions.size())) {
        // collapse w the end
        next_transitions.pop_back();
      } else {
        ColourTransitionPoint transition_point = _create_transition_next_gluon2qqbar_3bar_end(emission_info, abseta, next_dipole);
        next_transitions.push_back(transition_point);
      }
      // Safety assertion
      assert(next_transitions.is_3_end_CF());

      // update the existing dipole transitions
      //
      // search insertion point
      double minus_abseta = -abseta;

      // check if we have a normal insertion or an insertion after
      // the first pre-existing transition point
      if (!existing_transitions.eta_below_segment_R(minus_abseta, 0)) {
        // collapse w the end
        existing_transitions.pop_front();
      } else {
        ColourTransitionPoint transition_point = _create_transition_existing_gluon2qqbar_3bar_end(emission_info, minus_abseta);
        existing_transitions.push_front(transition_point);
      }
      // Safety assertion
      assert(existing_transitions.is_3bar_end_CF());
    }

    /// update the current "existing" chain and the previous dipole
    /// chain following a g_in->qbar_in + q_out splitting at the 3 end
    /// of the gluon. The existing dipole will replace the qbar by the
    /// gluon and the newly created dipole (next of existing) will
    /// have the gluon at is 3bar end and the emitted final-state
    /// quark at its 3 end
    virtual void update_transitions_backwards_qbar2gluon(ColourTransitionVector &existing_transitions,
                                                         ColourTransitionVector &new_transitions,
                                                         typename ShowerBase::EmissionInfo & emission_info) const override {
      // Assume we started with the dipoles
      //   (qbar,3bar)    (existing --- splitting at 3 end corresponding to an ISR qbar)
      // and after splitting we have
      //   (g,3bar)      (existing --- including 3 end of g)
      //   (q,g)         (new --- including 3bar end of gluon and radiated q)
      // corresponding to a g_in->qbar_in + q_out splitting
      //
      // The initial situation includes a single colour-transition chain
      //    [CR; (eta_0, ..., eta_n)]              (qbar-3bar: existing dipole)
      // which should be replaced by
      //    [CR; (eta_0, ..., eta_n, max(0,eta)]   (g-3bar: existing dipole)
      //    [CA; (min(0,eta))]                     (q,g   : new dipole)
      //
      // Extra care is needed because the insertion could happen in a
      // segment which is not at the end of the colour transition
      // chain.  If this is the case, we just remove the first
      // transition points so as to guarantee that the CF - CA/2
      // alternance is maintained

      double eta_insertion = emission_info.eta_insertion;

      // first handle the "new" dipole
      //
      // It has just been created so it has no transition point and is
      // assumed to be a CF
      double eta_new = std::min(0.0, eta_insertion);
      assert(new_transitions.transition_points().size()==0);
      // make sure the 3bar end is a gluon
      new_transitions.set_3bar_end_CF(false);
      ColourTransitionPoint new_transition_point = _create_transition_new_backwards_qbar2gluon(emission_info, eta_new);
      new_transitions.push_back(new_transition_point);
      // Safety assert
      assert(! new_transitions.is_3bar_end_CF());
 
      // update the existing dipole transitions
      //
      // search insertion point and check if we have a normal
      // insertion or an insertion after the first pre-existing
      // transition point
      double eta_existing = std::max(0.0, eta_insertion); 
      if (existing_transitions.eta_above_segment_L(eta_existing, existing_transitions.size())) {
        ColourTransitionPoint transition_point = _create_transition_existing_backwards_qbar2gluon(emission_info, eta_existing);
        existing_transitions.push_back(transition_point);
      } else {
        // collapse w the end
        existing_transitions.pop_back();
      }
    }

    /// update the current "existing" chain and the previous dipole
    /// chain following a g_in->q_in + qbar_out splitting at the 3bar
    /// end of the gluon. The existing dipole will replace the q by
    /// the gluon and the newly created dipole (next of existing) will
    /// have the gluon at is 3 end and the emitted final-state
    /// quark at its 3bar end
    virtual void update_transitions_backwards_q2gluon(ColourTransitionVector &existing_transitions,
                                                      ColourTransitionVector &new_transitions,
                                                      typename ShowerBase::EmissionInfo & emission_info) const override {
      // Assume we started with the dipoles
      //   (3,q)     (existing --- splitting at 3bar end corresponding to an ISR q)
      // and after splitting we have
      //   (3,g)     (existing --- including 3bar end of g)
      //   (g,qbar)  (new --- including 3 end of gluon and radiated qbar)
      // corresponding to a g_in->q_in + qbar_out splitting
      //
      // The initial situation includes a single colour-transition chain
      //    [CF; (eta_0, ..., eta_n)]             (3-q:  existing dipole)
      // which should be replaced by
      //    [CA; (min(0,eta), eta_0, ..., eta_n)] (3-g: existing dipole)
      //    [CF; (max(0,eta))]                    (q-qbar':  new dipole)
      //
      // Extra care is needed because the insertion could happen in a
      // segment which is not at the end of the colour transition
      // chain.  If this is the case, we just remove the first
      // transition points so as to guarantee that the CF - CA/2
      // alternance is maintained

      double eta_insertion = emission_info.eta_insertion;

      // first handle the "new" dipole
      //
      // It has just been created so it has no transition point and is
      // assumed to be a CF
      double eta_new = std::max(0.0, eta_insertion);
      // initialise the transition vector with a purely quark segment
      assert(new_transitions.transition_points().size()==0);
      new_transitions.set_3bar_end_CF(true);
      ColourTransitionPoint new_transition_point = _create_transition_new_backwards_q2gluon(emission_info, eta_new);
      new_transitions.push_back(new_transition_point);
 
      // update the existing dipole transitions
      //
      // search insertion point
      double eta_existing = std::min(0.0, eta_insertion);

      // check if we have a normal insertion or an insertion after
      // the first pre-existing transition point
      if (existing_transitions.eta_below_segment_R(eta_existing, 0)) {
        ColourTransitionPoint transition_point = _create_transition_existing_backwards_q2gluon(emission_info, eta_existing);
        existing_transitions.push_front(transition_point);
      } else {
        // collapse w the end
        existing_transitions.pop_front();
      }
      // Safety assert
      assert(! existing_transitions.is_3bar_end_CF());
    }

    
  protected:
    /// compute the exact eta from a given emission info.
    /// This is oriented so that the 3 end of the dipole corresponds
    /// to positive rapidiitied
    double _compute_insertion_eta_wrt_3(const Dipole & dipole,
                                        typename ShowerBase::EmissionInfo & emission_info) const{
      // In determining the colour transitions, work out the exact eta
      // between the radiation and the closer of the emitter / spectator
      //
      // Note (GS-2020-04-20):
      //   taking the closer is potentially dangerous. There are cases
      //   where the two are similar. If we pick the sign according to
      //   which one is closer this can give a huge change compared to
      //   the approx (typically for a boosted dipole splitting close to
      //   similar angles). In this region we'd be inclined to trust the
      //   approx.
      //
      //   We could work around this by explicitly computing both the
      //   rapidity wrt the emitter and spectator and take the one
      //   closest to the approx.
      //
      //   However, we should be aware that getting rapidities for
      //   angles close to pi is potentially dangerous. By taking the
      //   "closest in the eta_approx sense" approach, we limit
      //   this potential issue so this is what we have curently
      //   implemented
      //
      // the sign is determined so that +ve rapidities are on the colour-triplet
      // side and -ve rapidities on the anti-triplet side
      //
      // The goal is that it is also correct in the hard-collinear limit
      // (and we correctly reproduce the integrated rate for soft-gluon
      // emission in subsequet splittings)
      //
      // Finally, an overal sign depends on whether the emitter is a
      // triplet (so that, for antenna showers, emissions are always
      // ordered from 3bar to 3). Since do_kinematics() has already
      // been called, we can take it from emission_info.emitter_was_3
      int sign = (emission_info.emitter_was_3) ? 1 : -1;

      // the side of the emission is decided based on what eta_approx gives.
      //   1. +ve eta_approx => eta measured wrt to 3 (quark)
      //   2. we use emitter_was_3 to map 3/3bar to emitter/spectator
      // i.e.
      //   +ve eta_approx,  emitter_was_3  ==> ref for eta is emitter
      //   +ve eta_approx, !emitter_was_3  ==> ref for eta is spectator
      //   -ve eta_approx,  emitter_was_3  ==> ref for eta is spectator
      //   -ve eta_approx, !emitter_was_3  ==> ref for eta is emitter
      //
      // The one exception to this is the case of g->qqbar splitting
      // where the emitter is taken as the gluon that splits to qqbar
      bool ref_for_eta_is_emitter;
      if (emission_info.radiation_pdgid==21){
        // "standard" gluon emission
        ref_for_eta_is_emitter = (emission_info.eta_approx<=0) ^ (emission_info.emitter_was_3);
      } else {
        // g-> qqbar splitting
        ref_for_eta_is_emitter = emission_info.do_split_emitter;
      }
      
      double insertion_eta =
        sign * (ref_for_eta_is_emitter
                ? emission_info.eta_wrt_emitter  ()
                : emission_info.eta_wrt_spectator());
      
      return insertion_eta;
    }

    //--------------------------------------------------
    /// handle the creation of transition points w optional calculation
    /// of the auxiliary variable in the case of a gluon->qqbar
    /// splitting at the gluon quark end
    ///
    ///   emission_info   carries the info about the emission! 
    ///   eta_transition  the actual/calculated rapidity of the transition
    ///   eta_existing    rapidity to insert for the existing dipole
    ///   eta_new         rapidity to insert for the new dipole
    ///   transition_existing   transition for the existing dipole (filled here)
    ///   transition_new        transition for the new dipole      (filled here)
    virtual void _create_transitions_gluon_emission_from_quark(typename ShowerBase::EmissionInfo & emission_info,
                                                               double eta_left, double eta_right,
                                                               ColourTransitionPoint & transition_left,
                                                               ColourTransitionPoint & transition_right) const{
      transition_left  = ColourTransitionPoint(eta_left );
      transition_right = ColourTransitionPoint(eta_right);
    }

    /// handle the creation of transition points w optional calculation
    /// of the auxiliary variable in the case of a gluon->qqbar
    /// splitting at the gluon quark end
    ///
    /// for the existing dipole
    virtual ColourTransitionPoint _create_transition_existing_gluon2qqbar_3_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                                double abseta) const{
      
      return ColourTransitionPoint(abseta);
    }
    ///
    /// for the previous dipole
    virtual ColourTransitionPoint _create_transition_previous_gluon2qqbar_3_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                                double minus_abseta,
                                                                                const Dipole & previous_dipole) const{
      return ColourTransitionPoint(minus_abseta);
    }    

    /// handle the creation of transition points w optional calculation
    /// of the auxiliary variable in the case of a gluon->qqbar
    /// splitting at the gluon anti-quark end
    ///
    /// for the existing dipole
    virtual ColourTransitionPoint _create_transition_existing_gluon2qqbar_3bar_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                                   double minus_abseta) const{
      
      return ColourTransitionPoint(minus_abseta);
    }    
    ///
    /// for the next dipole
    virtual ColourTransitionPoint _create_transition_next_gluon2qqbar_3bar_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                               double abseta,
                                                                               const Dipole & next_dipole) const{
      return ColourTransitionPoint(abseta);
    }    

    /// handle the creation of transition points w optional calculation
    /// of the auxiliary variable in the case of a g_in -> q_in + qbar_out ISR
    ///
    /// for the existing dipole
    virtual ColourTransitionPoint _create_transition_existing_backwards_qbar2gluon(typename ShowerBase::EmissionInfo & emission_info,
                                                                                   double eta_existing) const{
      
      return ColourTransitionPoint(eta_existing);
    }    
    ///
    /// for the new dipole
    virtual ColourTransitionPoint _create_transition_new_backwards_qbar2gluon(typename ShowerBase::EmissionInfo & emission_info,
                                                                              double eta_new) const{
      return ColourTransitionPoint(eta_new);
    }    

    /// handle the creation of transition points w optional calculation
    /// of the auxiliary variable in the case of a g_in -> qbar_in + q_out ISR
    ///
    /// for the existing dipole
    virtual ColourTransitionPoint _create_transition_existing_backwards_q2gluon(typename ShowerBase::EmissionInfo & emission_info,
                                                                                double eta_existing) const{
      
      return ColourTransitionPoint(eta_existing);
    }    
    ///
    /// for the new dipole
    virtual ColourTransitionPoint _create_transition_new_backwards_q2gluon(typename ShowerBase::EmissionInfo & emission_info,
                                                                           double eta_new) const{
      return ColourTransitionPoint(eta_new);
    }    

    /// 2CF/CA
    double _two_CF_over_CA;
  };


  //------------------------------------------------------------------------
  /// @ingroup colour_classes
  /// \class ColourTransitionRunnerNODS
  /// class to handle the physics associated with colour transitions
  ///
  /// different strategies are possible (see above)
  class ColourTransitionRunnerNODS : public ColourTransitionRunnerSegment{
  public:
    /// default ctor
    ///
    /// #TRK_ISSUE-52  COLOUR_TODO we may want to set the default colour treatment to NODS
    ColourTransitionRunnerNODS(const QCDinstance &qcd)
      : ColourTransitionRunnerSegment(qcd){}

    /// dummy virtual dtor
    virtual ~ColourTransitionRunnerNODS(){}
    
    /// add a description
    virtual std::string description() const override{
      return "colour factor from transition segments with matrix-element reweighting";
    }

    /// get the acceptance probability due to colour associated w an emission
    /// See ColourTransitionRunnerBase for details
    virtual double colour_factor_acceptance(const Event & event,
                                            const typename ShowerBase::Element &element,
                                            typename ShowerBase::EmissionInfo &emission_info) override{
      // grab simple info
      const Dipole & dipole = event.dipoles()[element.dipole_index()];
      const ColourTransitionVector & transitions = dipole.colour_transitions;

      // cache eta_approx once and for all
      double eta_approx = element.eta_approx(emission_info.lnv, emission_info.lnb);
      if (!element.emitter_is_quark_end_of_dipole()){
        eta_approx = -eta_approx;
      }
      emission_info.eta_approx = eta_approx;
      emission_info.colour_segment_index = transitions.get_transition_index_above(eta_approx);

      // compute the insertion eta
      //
      // Note that is is forced to stay within the same colour segment
      // so no need for an update of the transition point index
      emission_info.eta_insertion = _compute_insertion_eta_wrt_3(dipole, emission_info);

      // with no transition points, the acceptance is just given by colour factors
      if (transitions.size() == 0){
        bool is_quark = transitions.is_3bar_end_CF();
        // for quarks radiating quarks (which happens for ISR), our
        // convention (and normalisation of the splitting functions)
        // is that the colour acceptance is 1 (see also comment below)
        emission_info.colour_segment_index = 0;
        bool colour_is_quark = is_quark && (emission_info.radiation_pdgid == 21);
        return colour_is_quark ? _two_CF_over_CA : 1.0;
      }

      // for a g->qqbar splitting, always return 1
      //
      // WATCH OUT: the fact that this is 1 is connected with our
      // choice of normalisation of the splitting functions
      // (e.g. 2CF/CA for q_is->g_is + q_fs ISR). Do not change this
      // without also checking the normalisation of the splitting
      // functions in QCD.hh.
      if (emission_info.radiation_pdgid != 21){
        return 1.0;
      }

      bool is_quark = transitions.is_quark_below_transition(emission_info.colour_segment_index);

      // for each CF segment we have sth of the form
      //
      //   auxiliaries:            abar     a
      //                            |       |
      //                            |       |
      //        dipole:   3bar =====---------===== 3
      //
      // with 3bar & 3 the ends of the dipole,
      //      abar & a the auxiliaries on the left and right of the segment
      // we associate a correction factor
      //
      //   w(3bar,abar,a,3) = 1 - (1-2CF/CA) (k|abar,a)/((k|abar,3bar) + (k|3bar,3) + (k|3,a))
      //
      // with (k|a,b) = (a.b)/((a.k)(k.b))
      // 
      // 
      // For semi-unbounded segments (CF extends to -infinity):
      //   3bar - a - 3
      // we set
      //   w-(3bar,a,3) = 1 - (1-2CF/CA) (k|3bar,a)/((k|3bar,3) + (k|3,a))
      //
      // For semi-unbounded segments (CF extends to +infinity):
      //   3bar - abar - 3
      // we set
      //   w+(3bar,abar,3) = 1 - (1-2CF/CA) (k|abar,3)/((k|abar,3bar) + (k|3bar,3))
      //                   = w-(3,abar,3bar)
      //
      // The total weight is the product of the w's for each CF segment
      //
      // Details for finite segments:
      //
      //  - it is like of the dipole ends play the role of 2 gluons:
      //      gL and gR at the respective 3bar and 3 ends of the dipole
      //    and the 2 auxiliaries qL=abar and qR=a play the role of quarks
      //
      //  - the distances we need:
      //     qL-qR, gL-gR, qL-gL, qR-gR
      //     k-gL, k-gR, k-qL, k-qR
      //
      //  - gL-gR, k-gL, k-gR are obtained from the dipole kinematics
      //    these are computed once and for all at the beginning
      //
      //  - distances involving qL depend on what reference is used for qL (3bar or 3)
      //    distances involving qR depend on what reference is used for qL (3bar or 3)
      //    these depend on the segment
      //
      //  - w = 1 - (1-2CF/CA) (k|abar,a)/((k|abar,3bar) + (k|3bar,3) + (k|3,a))
      //      = 1 - (1-2CF/CA) [d_abar_a/(d_abar_k*d_a_k)]/([d_3bar_abar/(d_3bar_k*d_abar_k)]+[d_3bar_3/(d_3bar_k*d_3_k)] + [d_a_3/(d_a_k*d_3_k)])
      //      = 1 - (1-2CF/CA) (d_abar_a*d_3bar_k*d_3_k)/(d_3bar_abar*d_a_k*d_3_k + d_3bar_3*d_abar_k*d_a_k + d_a_3*d_abar_k*d_3bar_k)
      //
      //  - if a denominator is 0, we fall back to the 2CF/CA factor associated w the segment
      //
      // Details of the semi-infinite segment at the left (3bar) end of the dipole
      //
      //  - we still need the dipole kinematics distances
      //
      //  - w = 1 - (1-2CF/CA) (k|3bar,a)/((k|3bar,3) + (k|3,a))
      //      = 1 - (1-2CF/CA) (d_3bar_a*d_3_k)/(d_3bar_3*d_a_k + d_a_3*d_3bar_k)
      //
      //  - we need the distances between the auxiliary a and 3bar and k, i.e. the "right" distances
      //
      // Details of the semi-infinite segment at the right (3) end of the dipole
      //
      //  - we still need the dipole kinematics distances
      //
      //  - w = 1 - (1-2CF/CA) (k|abar,3)/((k|abar,3bar) + (k|3bar,3))
      //      = 1 - (1-2CF/CA) (d_abar_3*d_3bar_k)/(d_abar_3bar*d_3_k + d_3bar_3*d_abar_k)
      //
      //  - we need the distances between the auxiliary abar and 3 and k, i.e. the "left" distances
      precision_type d_3bar_3=0.0, d_3bar_k=0.0, d_3_k=0.0;
      precision_type d_3bar_abar=0.0, d_abar_a=0.0, d_a_3=0.0;
      precision_type d_abar_k=0.0, d_a_k=0.0;

      // get the differences involving only the dipole
      // convention: vec_ab = b-a
      Momentum3<precision_type> vec_3bar_3, vec_3bar_k, vec_3_k;
      if (element.use_diffs()){
        vec_3bar_3 = emission_info.cached_dirdiff_3_minus_3bar;
        vec_3bar_k = emission_info.d_radiation_wrt_3bar();
        vec_3_k    = emission_info.d_radiation_wrt_3   ();
      } else {
        const Momentum & p3bar = element.particle_3bar_end().momentum();
        const Momentum & p3    = element.particle_3_end().momentum();
        const Momentum & pk    = emission_info.radiation;
        vec_3bar_3 = p3.direction_diff(p3bar);
        vec_3bar_k = pk.direction_diff(p3bar);
        vec_3_k    = pk.direction_diff(p3);
      }
      d_3bar_3 = vec_3bar_3.modpsq();
      d_3bar_k = vec_3bar_k.modpsq();
      d_3_k    = vec_3_k   .modpsq();
 
      double w_total = 1.0;

      // (optional) leftmost contribution
      unsigned int isegment;
      if (transitions.is_3bar_end_CF()){
        // contribution from the 0th segment
        const ColourTransitionPoint & p_a = transitions[0];
        
        // compute remaining distances
        precision_type d_a_3bar;
        _get_auxiliary_distances(p_a, vec_3bar_3, vec_3bar_k, vec_3_k,
                                 d_a_3bar, d_a_3, d_a_k);
        // compute the weight
        precision_type num   = _dipole_factor(d_a_3bar, d_a_k   , d_3bar_k);
        precision_type denom = _dipole_factor(d_3bar_3, d_3bar_k, d_3_k)
                             + _dipole_factor(d_a_3,    d_a_k,    d_3_k);
        if (denom<=0){
          _ColourTransitionRunnerNODSWeightDivisionByZero.warn("ColourTransitionRunnerNODS::colour_factor_acceptance: zero denominator in acceptance weight. Using (segment) colour factor only.");
          return is_quark ? _two_CF_over_CA : 1.0;
        }
        double w = 1 - (1-_two_CF_over_CA) * to_double(num/denom);

        w_total *= w;

        // then start from segment 2
        isegment = 2;
      } else {
        // start w "1st" segment (1st comes after 0th)
        isegment = 1;
      }

      // central segments
      while (isegment < transitions.size()){
        const ColourTransitionPoint & p_abar = transitions[isegment-1];
        const ColourTransitionPoint & p_a    = transitions[isegment];
        
        // compute remaining distances
        _get_auxiliary_distances_left(p_abar, vec_3bar_3, vec_3bar_k, vec_3_k,
                                      d_3bar_abar, d_abar_k);
        _get_auxiliary_distances_right(p_a, vec_3bar_3, vec_3bar_k, vec_3_k,
                                       d_a_3, d_a_k);
        d_abar_a = _get_auxiliary_inter_distance(p_abar, p_a, vec_3bar_3);
        
        // compute the weight
        precision_type num   = _dipole_factor(d_abar_a,    d_abar_k, d_a_k   );
        precision_type denom = _dipole_factor(d_3bar_abar, d_3bar_k, d_abar_k)
                             + _dipole_factor(d_3bar_3,    d_3bar_k, d_3_k   )
                             + _dipole_factor(d_a_3,       d_a_k,    d_3_k   );
        if (denom<=0){
          _ColourTransitionRunnerNODSWeightDivisionByZero.warn("ColourTransitionRunnerNODS::colour_factor_acceptance: zero denominator in acceptance weight. Using (segment) colour factor only.");
          return is_quark ? _two_CF_over_CA : 1.0;
        }
        double w = 1 - (1-_two_CF_over_CA) * to_double(num/denom);

        w_total *= w;
        isegment += 2;
      }

      // (optional) rightmost contribution
      if (isegment == transitions.size()){
        // contribution from the 0th segment
        const ColourTransitionPoint & p_abar = transitions[isegment-1];
        
        // compute remaining distances
        precision_type d_3_abar;
        _get_auxiliary_distances(p_abar, vec_3bar_3, vec_3bar_k, vec_3_k,
                                 d_3bar_abar, d_3_abar, d_abar_k);
        // compute the weight
        precision_type num   = _dipole_factor(d_3_abar,    d_abar_k, d_3_k   );
        precision_type denom = _dipole_factor(d_3bar_abar, d_3bar_k, d_abar_k)
                             + _dipole_factor(d_3bar_3,    d_3bar_k, d_3_k   );
        if (denom<=0){
          _ColourTransitionRunnerNODSWeightDivisionByZero.warn("ColourTransitionRunnerNODS::colour_factor_acceptance: zero denominator in acceptance weight. Using (segment) colour factor only.");
          return is_quark ? _two_CF_over_CA : 1.0;
        }
        double w = 1 - (1-_two_CF_over_CA) * to_double(num/denom);

        w_total *= w;
      }

      return w_total;
    }

    //--------------------------------------------------
    /// get the acceptance probability due to colour associated w an
    /// emission when one compute the Sudakov associated w the first
    /// emission.
    ///
    /// See ColourTransitionRunnerBase for details
    virtual double colour_factor_acceptance_first_sudakov(const Event & event,
                                                          const typename ShowerBase::Element &element,
                                                          typename ShowerBase::EmissionInfo &emission_info) override{
      if (event.size()>2){
        _ColourTransitionNODSRunnerFirstSudakov.warn("ColourTransitionRunnerNODS::colour_factor_acceptance_first_sudakov will use the colour_transition_approx_eta strategy to compute the first-emission Sudakov");
      }

      // Note that the line below would properly handle the (weighted)
      // case of an ISR backwards evolving quark (which includes a CF
      // piece for gluon emissions and a CA piece for backwards
      // evolution to a gluon)
      return ColourTransitionRunnerSegment::colour_factor_acceptance_first_sudakov(event, element, emission_info);
    }

  protected:
    static LimitedWarning _ColourTransitionRunnerNODSWeightDivisionByZero, _ColourTransitionNODSRunnerFirstSudakov;
    //--------------------------------------------------
    /// handle calculation of distances for auxiliary momentum in the case of a gluon emission from a quark line
    virtual void _create_transitions_gluon_emission_from_quark(typename ShowerBase::EmissionInfo & emission_info,
                                                               double eta_left, double eta_right,
                                                               ColourTransitionPoint & transition_left,
                                                               ColourTransitionPoint & transition_right) const override{
      // in the case of a gluon emitted from a quark segment, measure
      // the auxiliary distances wrt the emitted gluon
      //
      // for the "left"  transition (3bar-g dipole w 3    as auxiliary): we use 3    - g, measured wrt 3    end)
      // for the "right" transition (3   -g dipole w 3bar as auxiliary): we use 3bar - g, measured wrt 3bar end)
      Momentum3<precision_type> diff_3bar_minus_new, diff_3_minus_new;

      // See ColourTransitionRunnerSegment for the details on how to
      // reorganise the transition vectors.
      //
      // For the left  dipole, the inserted transition point auxiliary variable is "3"
      // For the right dipole, the inserted transition point auxiliary variable is "3bar"
      //
      // Auxiliaries: [currently left==new, right==existing]
      //  - emitter at the 3 end
      //     left_dipole  = (spectator,radiation)  w auxiliary  emitter
      //     right_dipole = (radiation,emitter)    w auxiliary  spectator
      //  - emitter at the 3bar end
      //     left_dipole  = (emitter,radiation)    w auxiliary  spectator
      //     right_dipole = (radiation,spectator)  w auxiliary  emitter
      if (emission_info.use_diffs){
        diff_3bar_minus_new = emission_info.d_3bar_end() - emission_info.d_radiation_wrt_3bar();
        diff_3_minus_new    = emission_info.d_3_end()    - emission_info.d_radiation_wrt_3();
      } else {
        const Momentum & pout_3bar = emission_info.momentum_3bar_out();
        const Momentum & pout_3    = emission_info.momentum_3_out();
      
        diff_3bar_minus_new = pout_3bar.direction_diff(emission_info.radiation);
        diff_3_minus_new    = pout_3   .direction_diff(emission_info.radiation);  
      }

      transition_left  = ColourTransitionPoint(eta_left,  diff_3_minus_new,   false);
      transition_right = ColourTransitionPoint(eta_right, diff_3bar_minus_new, true);
    }

    /// handle calculation of distances for auxiliary momentum in the
    /// case of a gluon->qqbar splitting at the quark end of the
    /// dipole  
    ///
    /// auxiliary for the existing dipole
    virtual ColourTransitionPoint _create_transition_existing_gluon2qqbar_3_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                                double abseta) const override{
      
      // (3 of existing) q'    qbar' (radiation)
      //                  \    / 
      //       existing    \  /     previous
      //                    ||
      //      3bar --------/  \---------- 3 of previous
      //                                                                            
      // the existing 3bar-g dipole becomes 3bar-q' with auxiliary qbar',
      // the neighbouring g-3 dipole becomes qbar'-3 ("previous")
      // and the qbar' is the radiation, while the q' is the 
      // 3 end of the updated existing dipole.
      //
      // The distance is qbar'-q', wrt to the 3 end of the dipole
      // We use aux-q' = radiation - (dip 3_out)
      Momentum3<precision_type> diff_newqbar_minus_newq;
      if (emission_info.use_diffs){
        diff_newqbar_minus_newq = emission_info.d_radiation_wrt_3()    - emission_info.d_3_end();
      } else {
        const Momentum & pout_3 = emission_info.momentum_3_out();
        diff_newqbar_minus_newq = emission_info.radiation.direction_diff(pout_3);
      }

      return ColourTransitionPoint(abseta, diff_newqbar_minus_newq, false);
    }
    
    ///
    /// auxiliary for the previous dipole
    virtual ColourTransitionPoint _create_transition_previous_gluon2qqbar_3_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                                double minus_abseta,
                                                                                const Dipole & previous_dipole) const override{
      // the previous g-q dipole becomes qbar'-q with auxiliary q'
      //
      // We use the q' - qbar' distance, wrt to the 3bar end of the dipole
      // It is calculated as aux-qbar' = (existing dip 3_out) - radiation
      Momentum3<precision_type> diff_newq_minus_newqbar;
      if (emission_info.use_diffs){
        diff_newq_minus_newqbar = emission_info.d_3_end() - emission_info.d_radiation_wrt_3();
      } else {
        const Momentum & pout_3 = emission_info.momentum_3_out();
        diff_newq_minus_newqbar = pout_3.direction_diff(emission_info.radiation);
      }
      
      return ColourTransitionPoint(minus_abseta, diff_newq_minus_newqbar, true);
    }

    /// handle the creation of transition points w optional calculation
    /// of the auxiliary variable in the case of a gluon->qqbar
    /// splitting at the gluon anti-quark end
    ///
    /// auxiliary for the existing dipole
    virtual ColourTransitionPoint _create_transition_existing_gluon2qqbar_3bar_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                                   double minus_abseta) const override{
      // the existing g-q dipole becomes qbar'-q with auxiliary q'
      // We use the q'-qbar' distance, wrt to the 3bar end of the dipole
      // The distances is calculated as aux-qbar' = radiation - (dip 3bar_out)
      Momentum3<precision_type> diff_newq_minus_newqbar;
      if (emission_info.use_diffs){
        diff_newq_minus_newqbar = emission_info.d_radiation_wrt_3bar() - emission_info.d_3bar_end();
      } else {
        const Momentum & pout_3bar = emission_info.momentum_3bar_out();
        diff_newq_minus_newqbar = emission_info.radiation.direction_diff(pout_3bar);
      }

      return ColourTransitionPoint(minus_abseta, diff_newq_minus_newqbar, true);
    }    
    ///
    /// auxiliary for the next dipole
    virtual ColourTransitionPoint _create_transition_next_gluon2qqbar_3bar_end(typename ShowerBase::EmissionInfo & emission_info,
                                                                               double abseta,
                                                                               const Dipole & next_dipole) const override{
      // the next qbar-g dipole becomes qbar-q' with auxiliary qbar'
      // We use the distance qbar'-q', wrt the 3 end of the dipole
      // The distance is calculated as aux-q' = (existing dip 3bar_out) - radiation
      Momentum3<precision_type> diff_newqbar_minus_newq;
      if (emission_info.use_diffs){
        diff_newqbar_minus_newq = emission_info.d_3bar_end() - emission_info.d_radiation_wrt_3bar();
      } else {
        const Momentum & pout_3bar = emission_info.momentum_3bar_out();
        diff_newqbar_minus_newq = pout_3bar.direction_diff(emission_info.radiation);
      }

      return ColourTransitionPoint(abseta, diff_newqbar_minus_newq, false);
    }    

    /// handle the creation of transition points w optional calculation
    /// of the auxiliary variable in the case of a g_in -> q_in + qbar_out ISR
    ///
    /// for the existing dipole
    virtual ColourTransitionPoint _create_transition_existing_backwards_qbar2gluon(typename ShowerBase::EmissionInfo & emission_info,
                                                                                   double eta_existing) const override{
      // the existing dipole was, pre-branching, having the qbar as
      // its 3 end. This will become the gluon. The auxiliary momentum
      // of the post-branching existing dipole should point to the
      // radiated q.
      //
      // Since this is a collinear branching, we take the angle
      // between the outgoing quark and the post-branching gluon as
      // the reference distance
      //
      // This is measured wrt the gluon, i.e. wrt the 3 end of the existing dipole
      Momentum3<precision_type> diff_qout_minus_gluon;
      if (emission_info.use_diffs){
        // qout-gluon = rad-3_new = (rad-3_old) - (3_new-3_old)
        diff_qout_minus_gluon = emission_info.d_radiation_wrt_3() - emission_info.d_3_end();
      } else {
        const Momentum & pout_3 = emission_info.momentum_3_out();
        diff_qout_minus_gluon = emission_info.radiation.direction_diff(pout_3);
      }
      return ColourTransitionPoint(eta_existing, diff_qout_minus_gluon, false);
    }    
    ///
    /// for the new dipole
    virtual ColourTransitionPoint _create_transition_new_backwards_qbar2gluon(typename ShowerBase::EmissionInfo & emission_info,
                                                                              double eta_new) const override{
      // the existing dipole was, pre-branching, having the qbar as
      // its 3 end. This will become the gluon. The auxiliary momentum
      // of the new dipole (which has the new IS gluon at its 3bar end
      // and the radiated quark at its 3 end) should point to the 3bar
      // end of the existing dipole.
      //
      // Since this is a collinear branching, we take the angle
      // between the post-branching 3bar and the post-branching gluon
      // (3 end) as the reference distance
      //
      // This is measured wrt the gluon, i.e. wrt the 3bar end of the new dipole
      Momentum3<precision_type> diff_existnew_3bar_minus_gluon;
      if (emission_info.use_diffs){
        // new3bar-new3 = (new3bar-old_3bar) + (old3bar-old3) - (new3-old3)
        diff_existnew_3bar_minus_gluon = emission_info.d_3bar_end() - emission_info.cached_dirdiff_3_minus_3bar - emission_info.d_3_end();
      } else {
        const Momentum & pout_3 = emission_info.momentum_3_out();
        diff_existnew_3bar_minus_gluon = emission_info.momentum_3bar_out().direction_diff(pout_3);
      }
      return ColourTransitionPoint(eta_new, diff_existnew_3bar_minus_gluon, true);
    }    

    /// handle the creation of transition points w optional calculation
    /// of the auxiliary variable in the case of a g_in -> q_in + qbar_out ISR
    ///
    /// for the existing dipole
    virtual ColourTransitionPoint _create_transition_existing_backwards_q2gluon(typename ShowerBase::EmissionInfo & emission_info,
                                                                                double eta_existing) const override{
      // the existing dipole was, pre-branching, having the (IS) q as
      // its 3bar end. This will become the gluon. The auxiliary momentum
      // of the post-branching existing dipole should point to the
      // radiated qbar.
      //
      // Since this is a collinear branching, we take the angle
      // between the outgoing qbar and the post-branching gluon as
      // the reference distance
      //
      // This is measured wrt the gluon, i.e. wrt the 3bar end of the existing dipole
      Momentum3<precision_type> diff_qbarout_minus_gluon;
      if (emission_info.use_diffs){
        // qbarout-gluon = rad-3bar_new = (rad-3bar_old) - (3bar_new-3bar_old)
        diff_qbarout_minus_gluon = emission_info.d_radiation_wrt_3bar() - emission_info.d_3bar_end();
      } else {
        const Momentum & pout_3bar = emission_info.momentum_3bar_out();
        diff_qbarout_minus_gluon = emission_info.radiation.direction_diff(pout_3bar);
      }
      return ColourTransitionPoint(eta_existing, diff_qbarout_minus_gluon, true);
    }    
    ///
    /// for the new dipole
    virtual ColourTransitionPoint _create_transition_new_backwards_q2gluon(typename ShowerBase::EmissionInfo & emission_info,
                                                                           double eta_new) const override{
      // the existing dipole was, pre-branching, having the q as
      // its 3bar end. This will become the gluon. The auxiliary momentum
      // of the new dipole (which has the new IS gluon at its 3 end
      // and the radiated qbar at its 3bar end) should point to the 3
      // end of the existing dipole.
      //
      // Since this is a collinear branching, we take the angle
      // between the post-branching 3 and the post-branching gluon
      // (3bar end) as the reference distance
      //
      // This is measured wrt the gluon, i.e. wrt the 3 end of the new
      // dipole
      Momentum3<precision_type> diff_existnew_3_minus_gluon;
      if (emission_info.use_diffs){
        // new3bar-new3bar = (new3-old_3) + (old3-old3bar) - (new3bar-old3bar)
        diff_existnew_3_minus_gluon = emission_info.cached_dirdiff_3_minus_3bar - emission_info.d_3bar_end() + emission_info.d_3_end();
      } else {
        const Momentum & pout_3bar = emission_info.momentum_3bar_out();
        diff_existnew_3_minus_gluon = emission_info.momentum_3_out().direction_diff(pout_3bar);
      }
      return ColourTransitionPoint(eta_new, diff_existnew_3_minus_gluon, false);
    }    
    
    
  private:
    /// compute relevant distances for a transition point at the left-end of a CF segment
    ///
    /// the vec_* arguments carry the internal differemces within the dipole
    /// the d_* results are filled by this cunction
    void _get_auxiliary_distances_left(const ColourTransitionPoint &p,
                                       const Momentum3<precision_type> & vec_3bar_3,
                                       const Momentum3<precision_type> & vec_3bar_k,
                                       const Momentum3<precision_type> & vec_3_k,
                                       precision_type &d_aux_3bar,
                                       precision_type &d_aux_k) const{
      if (p.is_dirdiff_wrt_3bar()){
        const Momentum3<precision_type> & vec_3bar_aux = p.dirdiff();
        // aux-3bar = d_aux
        // aux-k = aux-3bar - (k-3bar)
        d_aux_3bar = vec_3bar_aux.modpsq();
        d_aux_k    = (vec_3bar_aux - vec_3bar_k).modpsq();
      } else {
        const Momentum3<precision_type> & vec_3_aux = p.dirdiff();
        // aux-3bar = (aux-3) + (3-3bar)
        // aux-k = (aux-3) - (k-3)
        d_aux_3bar = (vec_3bar_3 + vec_3_aux).modpsq();
        d_aux_k    = (vec_3_aux - vec_3_k).modpsq();            
      }
    }

    /// compute relevant distances for a transition point at the right-end of a CF segment
    ///
    /// the vec_* arguments carry the internal differemces within the dipole
    /// the d_* results are filled by this cunction
    void _get_auxiliary_distances_right(const ColourTransitionPoint &p,
                                        const Momentum3<precision_type> & vec_3bar_3,
                                        const Momentum3<precision_type> & vec_3bar_k,
                                        const Momentum3<precision_type> & vec_3_k,
                                        precision_type &d_aux_3,
                                        precision_type &d_aux_k) const{
      if (p.is_dirdiff_wrt_3bar()){
        const Momentum3<precision_type> & vec_3bar_aux = p.dirdiff();
        // aux-3 = (aux-3bar) - (3-3bar)
        // aux-k = (aux-3bar) - (k-3bar)
        d_aux_3 = (vec_3bar_aux - vec_3bar_3).modpsq();
        d_aux_k = (vec_3bar_aux - vec_3bar_k).modpsq();
      } else {
        const Momentum3<precision_type> & vec_3_aux = p.dirdiff();
        // aux-3 = d_aux
        // aux-k = (aux-3) - (k-3)
        d_aux_3 = vec_3_aux.modpsq();
        d_aux_k = (vec_3_aux - vec_3_k).modpsq();            
      }
    }

    /// compute all between a transition point and the dipole momenta
    ///
    /// the vec_* arguments carry the internal differemces within the dipole
    /// the d_* results are filled by this function
    void _get_auxiliary_distances(const ColourTransitionPoint &p,
                                  const Momentum3<precision_type> & vec_3bar_3,
                                  const Momentum3<precision_type> & vec_3bar_k,
                                  const Momentum3<precision_type> & vec_3_k,
                                  precision_type &d_aux_3bar,
                                  precision_type &d_aux_3,
                                  precision_type &d_aux_k) const{
      if (p.is_dirdiff_wrt_3bar()){
        const Momentum3<precision_type> & vec_3bar_aux = p.dirdiff();
        // aux-3bar = daux
        // aux-3    = (aux-3bar) - (3-3bar)
        // aux-k    = (aux-3bar) - (k-3bar)
        d_aux_3bar = vec_3bar_aux.modpsq();
        d_aux_3    = (vec_3bar_aux - vec_3bar_3).modpsq();
        d_aux_k    = (vec_3bar_aux - vec_3bar_k).modpsq();
      } else {
        const Momentum3<precision_type> & vec_3_aux = p.dirdiff();
        // aux-3bar = (aux-3) + (3-3bar)
        // aux-3    = d_aux
        // aux-k    = (aux-3) - (k-3)
        d_aux_3bar = (vec_3bar_3 + vec_3_aux).modpsq();
        d_aux_3    = vec_3_aux.modpsq();
        d_aux_k    = (vec_3_aux - vec_3_k).modpsq();            
      }
    }

    /// get the distnce between 2 auxiliary points
    precision_type _get_auxiliary_inter_distance(const ColourTransitionPoint &p_left,
                                                 const ColourTransitionPoint &p_right,
                                                 const Momentum3<precision_type> & vec_3bar_3) const{
      // both wrt 3bar or both wrt 3  : just get the diff of the dirdiffs
      // left wrt 3bar, right wrt 3   : left-right = (left-3bar) - (right-3)    - (3-3bar)
      // left wrt 3,    right wrt 3bar: left-right = (left-3)    - (right-3bar) + (3-3bar)
      const Momentum3<precision_type> & aux_left  = p_left .dirdiff();
      const Momentum3<precision_type> & aux_right = p_right.dirdiff();
      
      if (p_left.is_dirdiff_wrt_3bar()){
        if (p_right.is_dirdiff_wrt_3bar()){
          return (aux_left-aux_right).modpsq();
        }
        // we have (aux_left-3bar) - (aux_right-3) - (3-3bar)
        return (aux_left-aux_right-vec_3bar_3).modpsq();
      }
      if (p_right.is_dirdiff_wrt_3bar()){
        // we have (aux_left-3) - (aux_right-3bar) + (3-3bar)
        return (aux_left-aux_right+vec_3bar_3).modpsq();
      }
      // aux left and aux right are both wrt 3
      return (aux_left-aux_right).modpsq();
    }
    
    /// an attempt at computing precisely d_ij / (d_ik d_kj)
    /// e.g. avoiding a possible underflow when computing d_ik d_kj.
    ///
    /// Note that this uses squared distances so differs by a factor
    /// of 2 compared to the same expression built w factors
    /// (1-cos_ab))
    precision_type _dipole_factor(const precision_type & d_ij,
                                  const precision_type & d_ik,
                                  const precision_type & d_kj) const{
      // take the largest distance out first
      //
      // This would work nicely for a collinear splitting where the
      // factored-out ratio goes to 1 So we'd only be limited by how
      // small the smallest d_*k can go. In double this would give a
      // reach of m^2=10^-318 i.e. of lnktmin=10^-159
      //
      // For a splitting at infinitely large angles, the d_ik would be
      // finite (max 2) so the distance would essentially be given by
      // d_ij n the worst case and so this is fine again
      return d_ik>d_kj
        ? ((d_ij/d_ik) / d_kj)
        : ((d_ij/d_kj) / d_ik);
    }

  };

  
  //------------------------------------------------------------------------
  /// helper that returns a pointer to the right colour runner class
  /// based on the "QCD" settings
  ColourTransitionRunnerBase * create_colour_transition_runner(const QCDinstance &qcd);

  
} // nemaspace panscales

#endif  // __PANSCALES_COLOUR_TRANSITION_RUNNER_HH__
