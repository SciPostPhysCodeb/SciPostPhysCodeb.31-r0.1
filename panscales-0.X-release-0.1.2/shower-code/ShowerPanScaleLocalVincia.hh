//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __SHOWERPANSCALELOCALVINCIA_HH__
#define __SHOWERPANSCALELOCALVINCIA_HH__

#include "ShowerPanScaleBase.hh"

namespace panscales{
  
//--------------------------------------------------------------------------------
/// @ingroup showers
/// \class ShowerPanScaleLocalVincia
/// the PanScales shower w dipole-local recoil (antenna variant) for e+e- collisions  
/// introduced in arXiv:2002.11114. This is NOT an implementation of the Vincia 
/// shower.

class ShowerPanScaleLocalVincia : public ShowerPanScaleBase {
public:
  /// default ctor
  ShowerPanScaleLocalVincia(const QCDinstance & qcd, double beta = 0.5, bool additive_branching = true) 
    : ShowerPanScaleBase(qcd, beta), _additive_branching(additive_branching) {}

  /// virtual dtor
  virtual ~ShowerPanScaleLocalVincia() {}

  /// implements the element as a subclass (EmissionInfo is common to
  /// all PanScales showers and defined in the ShowerPanScaleBase base
  /// class)
  class Element;

  /// description of the class
  virtual std::string description() const override {
    std::ostringstream ostr;
    ostr << name() << " shower with beta = " << _beta << " and additive_branching = " << _additive_branching;
    return ostr.str();
  }
  /// name of the shower
  std::string name() const override{return "PanScaleLocalVincia-ee";}

  /// this is an antenna shower (1 element per dipole)
  virtual unsigned int n_elements_per_dipole() const override{ return 1;}
  
  /// returns true if an emission happens at a phase-space point that has
  /// already been covered by the matching shower (used in "truncated" showers)
  virtual bool veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const override;
  
  /// element store
  std::vector<std::unique_ptr<ShowerBase::Element> > elements(Event & event, int dipole_index) const override;

  /// this is an antenna-like shower, so both the emitter and spectator
  /// can split
  bool only_emitter_splits() const override {return false;}

  /// given kinematics from an event, returns the lnv, lnb values
  /// that correspond to (ĩ,j̃) -> (i,j,k) (NOT IMPLEMENTED)
  virtual bool find_lnv_lnb_from_kinematics(const Momentum &p_emit, 
                                     const Momentum &p_spec, 
                                     const Momentum &p_rad, 
                                     const precision_type &p_emit_dot_p_spec,
                                     const precision_type &p_emit_dot_p_rad,
                                     const precision_type &p_spec_dot_p_rad,
                                     const Event &event,
                                     double &lnv, double &lnb) const override;

private:
  bool _additive_branching;  ///< true: additive branching (default); false: mutliplicative branching
};

//----------------------------------------------------------------------
/// \class ShowerPanScaleLocalVincia::Element
/// stores the kinematics associated w dipole emitter/spectator
/// pairs for the ShowerPanScaleLocalVincia shower
class ShowerPanScaleLocalVincia::Element : public ShowerPanScaleBase::Element {
public:
  /// dummy ctor  
  Element() {}

  /// full ctor w initialisation
  Element(int emitter_index, int spectator_index, 
          int dipole_index, Event * event, const ShowerPanScaleLocalVincia * shower) :
    ShowerPanScaleBase::Element(emitter_index, spectator_index, dipole_index, event, shower), _shower(shower) {}

  /// given that we have virtual functions in a number of places,
  /// we need a virtual destructor to avoid compiler warnings
  virtual ~Element() {}
    
  //--------------------------------------------------
  // basic information
  //--------------------------------------------------
  /// Approximate calculation of the log of the kT of the emission relative
  /// to its parents in their rest frame, (eq. 57 of logbook 2019-07-02).
  const double alphas_lnkt(double lnv, double lnb) const override {
    precision_type lnkt = log_T(_rho) + lnv + _shower->_beta*fabs(lnb);
    return to_double(lnkt);
  }
  
  //--------------------------------------------------
  // info associated w kinematic ranges
  //--------------------------------------------------
  /// the lnb total extent, as a function of lnv, is parametrised
  /// as  const + lnv_deriv * lnv.
  ///
  double lnb_extent_const()     const override {return to_double(log_T(_dipole_m2) - 2.*log_T(_rho))/(1. + _shower->_beta);}
  double lnb_extent_lnv_coeff() const override {return -2.0/(1. + _shower->_beta);}

  Range lnb_generation_range(double lnv) const override;
  
  // #TRK_ISSUE-454  [FORLATER] : The exact range is actually known (Eq. (42) of 2018-07-notes.pdf)
  // but it depends both on lnv and lnb
  bool has_exact_lnb_range() const override {return false;}
  
  Range lnb_exact_range(double lnv) const override { return Range(0., 0.); }
  
  double lnv_lnb_max_density() const override;
  /// jacobian for this shower is not computed
  precision_type dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const override;


  
  //--------------------------------------------------
  // important definitions to be implemented in all derived classed
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  virtual bool acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const override;
  // goes in the derived classes
  virtual bool check_after_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info) const override;

  /// carry out the splitting and update the event; return true if
  /// successful; the RotatedPieces pass on a potentially rotated version
  /// of the emitter, spectator, and perpendicular vectors, and these
  /// are the ones that should be used, not those from emission_info; 
  /// the output is stored back in emission_info (possibly still rotated)
  virtual bool do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const override;// {return false;}



private:
  const ShowerPanScaleLocalVincia * _shower;
};

} // namespace panscales

#endif // __SHOWERPANSCALELOCALVINCIA_HH__
