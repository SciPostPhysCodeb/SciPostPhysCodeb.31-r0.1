//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "MomentumM2.hh"
#include "Type.hh"
#include <cassert>
#include <vector>
using namespace std;

namespace panscales {

// put this near the top to have it available for the functions
// that come below
template <class T>
inline T dot_product(const MomentumM2<T> & a, const MomentumM2<T> & b) {
  T result = a.E()*b.E() - a.px()*b.px() - a.py()*b.py() - a.pz()*b.pz();

  // Previous test:
  // if the particles are massless and the result is coming out lower
  // than sqrt(epsilon) (times a normalisation Ea*Eb), then one is in a
  // situation where rounding error is likely to be a problem; so take
  // an alternative route to try to get an answer that will be robust
  // down to smaller masses
  //
  // #TRK_ISSUE-190  NB: GPS 2021-05-12, switched to abs(result) in test, because 
  //     of issue raised by Melissa and Silvia where ndot products with 
  //     negative-energy four vectors were giving incorrect results.
  //
  //     negligible impact on timing on with Apple clang version 13.1.6 on Intel
  //     ```time ./example-framework -out a -shower panlocal -lnvmin -100 -use-diffs -alphas 0.002 -nev 1e4 -output-interval 1e3```
  if (a.m2() == 0 && 
      b.m2() == 0 && 
      std::abs(result) < std::abs(MomentumM2<T>::dot_product_cross_transition() * a.E()*b.E())) {
    // we are in a situation where two massless particles are quasi collinear.
    // So we are going to try and evaluate the dot product in a
    // way that is less sensitive to rounding errors
    // 
    // For massless particles we have
    //
    //     a.b = Ea * Eb * (1-cos theta_ab) ~= Ea * Eb * (sin^2 theta_ab)/2
    //                                       = (Ea*Eb*sin(theta_ab))^2 / (2*EaEb)
    //                                       = |a cross b|^2/(2*Ea*Eb)
    //
    // We can get sin^2 theta_ab using a cross product
    // (NB false as third arg, means that the energy is set to zero).
    //
    // the mass^2 of cross_result is equal to -(px^2 + py^2 + pz^2) 
    // = (Ea*Eb*sintheta_ab)^2
    // so we can get
    //
    // #TRK_ISSUE-191  gs note on 2019-05-25: the normalisation factor 1/2 is OK up to
    // sqrt(epsilon) corrections when computed for
    // 1-cos()<sqrt(epsilon). For more general cases one should use
    // sth more precise:
    //
    //  a.b = Ea Eb (1-cos)
    //      = Ea Eb sin^2/(1+cos)    <--- here 1+cos cannot be replaced by 2
    //      = Ea Eb (axb/(Ea Eb))^2/(2-a.b/(Ea Eb))
    //      = -(axb)^2 * [-1/(2 Ea Eb - a.b)]]
    //
    // Since this is only a small modification, we leave it in even
    // for dot_product_cross_transition==sqrt(epsilon)
    //    
    // old expression was:
    //   T result_new = cross_m2_norm(a,b, -0.5/(a.E()*b.E()));
    T result_new = cross_m2_norm(a,b, -1.0/(2*a.E()*b.E()-result));
    return result_new;
    // MomentumM2<T> cross_result = cross(a,b, false);
    // T result_old = -0.5*(cross_result.m2())/(a.E()*b.E());
    // //assert(result_new == ApproxRel(result_old));
    // return result_old;
  } else {
    return result;
  }
}

//----------------------------------------------------------------------
/// rapidity of the jet
template <class T>
T MomentumM2<T>::rap() const  {
  T rap;
  if(m2_==0) {
    T pt2 = px_*px_+py_*py_;
    if(pz_<0) {
      if(pt2==0) return -this->maxrap_;
      T EMinusPz2 = E_-pz_;
      EMinusPz2   = EMinusPz2*EMinusPz2;
      rap =  0.5 * log( pt2 / EMinusPz2 );
    } else {
      if(pt2==0) return  this->maxrap_;
      T EPlusPz2 = E_+pz_;
      EPlusPz2   = EPlusPz2*EPlusPz2;
      rap = -0.5 * log( pt2 / EPlusPz2 );
    };
  } else {
    rap = Momentum4<T>::rap();
  }
  return rap;
}

//----------------------------------------------------------------------
/// multiply the jet's MomentumM2 by the coefficient
template <class T>
MomentumM2<T> & MomentumM2<T>::operator*=(T coeff) {
  Momentum4<T>::operator*=(coeff);
  // multiply twice sequentially by coeff, rather than by
  // coeff^2, because sometimes coeff^2 may be outside the accessible
  // range (1e-308 to 1e308 roughly)
  m2_ *= coeff;
  m2_ *= coeff;
  return *this;
}

//----------------------------------------------------------------------
// return the division, jet/coeff
template <class T>
MomentumM2<T> & MomentumM2<T>::operator/=(T coeff) {
  Momentum4<T>::operator/=(coeff);
  // divide twice sequentially by coeff, rather than by
  // coeff^2, because sometimes coeff^2 may be outside the accessible
  // range (1e-308 to 1e308 roughly)
  m2_ /= coeff;
  m2_ /= coeff;
  return *this;
}
//----------------------------------------------------------------------
// return "sum" with MomentumM2
template <class T>
MomentumM2<T> & MomentumM2<T>::operator+=(const MomentumM2<T> & jet) {
  // get the dot product before the 4-vector operations, because we
  // need the dot product to be taken with the original value of *this
  T dot = dot_product(*this,jet);
  Momentum4<T>::operator+=(jet);
  internal_set_m2_from_pair(m2_, jet.m2(), dot);
  return *this;
} 

//----------------------------------------------------------------------
// return difference with MomentumM2
template <class T>
MomentumM2<T> & MomentumM2<T>::operator-=(const MomentumM2<T> & jet) {
  T dot = dot_product(*this,jet);
  Momentum4<T>::operator-=(jet);
  // work out the mass before the 4-vector operations, because we
  // need the dot product to be taken with the original value of *this
  internal_set_m2_from_pair(m2_, jet.m2(), -dot);
  return *this;
}


/// reset MomentumM2
template <class T>
void MomentumM2<T>::reset_momentum(const MomentumM2<T> & jet) {
  Momentum4<T>::reset_momentum(jet);
  m2_ = jet.m2();

}
  
/// reset MomentumM2
template <class T>
void MomentumM2<T>::reset_momentum(T px_in, T py_in, T pz_in, T E_in) {
  Momentum4<T>::reset_momentum(px_in, py_in, pz_in, E_in) ;
  m2_ = m2_internal_function();
}

/// reset MomentumM2
template <class T>
void MomentumM2<T>::reset_momentum(T px_in, T py_in, T pz_in, T E_in, T m2_in) {
  Momentum4<T>::reset_momentum(px_in, py_in, pz_in, E_in) ;
  m2_ = m2_in;
#ifdef PANSCALES_MOMENTUM_CHECKS
  internal_m2_check();
#endif  
}

//----------------------------------------------------------------------
/// transform this jet (given in the rest frame of prest) into a jet
/// in the lab frame 
//
// #TRK_ISSUE-192  NB1: code adapted from that in herwig f77 (checked how it worked
// long ago)
//
// NB2: given potential importance of correct masses, do not use 
//      the Momentum4 class here (which would use its mass function)
template <class T>
MomentumM2<T> & MomentumM2<T>::boost(const MomentumM2<T> & prest) {

  if (prest.px() == 0.0 && prest.py() == 0.0 && prest.pz() == 0.0) 
    return *this;

  T m_local = prest.m();
  assert(m_local != 0);

  T pf4  = (  px()*prest.px() + py()*prest.py()
                 + pz()*prest.pz() + E()*prest.E() )/m_local;
  T fn   = (pf4 + E()) / (prest.E() + m_local);
  px_ +=  fn*prest.px();
  py_ +=  fn*prest.py();
  pz_ +=  fn*prest.pz();
  E_ = pf4;

  return *this;
}


//----------------------------------------------------------------------
/// transform this jet (given in lab) into a jet in the rest
/// frame of prest  
//
// #TRK_ISSUE-193  NB: code adapted from that in herwig f77 (checked how it worked
// long ago)
//
// NB2: given potential importance of correct masses, do not use 
//      the Momentum4 class here (which would use its mass function)
template <class T>
MomentumM2<T> & MomentumM2<T>::unboost(const MomentumM2<T> & prest) {
  
  if (prest.px() == 0.0 && prest.py() == 0.0 && prest.pz() == 0.0) 
    return *this;

  T m_local = prest.m();
  assert(m_local != 0);

  // Let pf = momentum after boost
  //
  // In rest frame of prest, dot_product(pf*prest) = pf.E() * prest.m()
  // Since the dot product is boost invariant, we can use the dot
  // product in the original frame to deduce pf.E()
  //
  //     pf.E() = dot_product(p, prest) / prest.m()
  //
  T pf4  = ( -px()*prest.px() - py()*prest.py()
                 - pz()*prest.pz() + E()*prest.E() )/m_local;

  // the 3-vector modification has to be parallel to the boost vector
  // (transverse components don't get any modification). One
  // can verify that the following coefficient gives the correct final
  // answer, e.g. by checking invariant mass of the final vector         
  T fn   = (pf4 + E()) / (prest.E() + m_local);
  px_ -=  fn*prest.px();
  py_ -=  fn*prest.py();
  pz_ -=  fn*prest.pz();
  E_ = pf4;

  return *this;
}

template <class T>
MomentumM2<T> & MomentumM2<T>::boost(const LorentzBoostGen<MomentumM2<T>> & lboost) {
  if (lboost.no_action_) return *this;
  T Enew = lboost.new_energy(*this);
  T fn = lboost.coeff_3mom(*this,Enew);

  px_ += fn * lboost.pboost_.px();
  py_ += fn * lboost.pboost_.py();
  pz_ += fn * lboost.pboost_.pz();
  E_   = Enew;
  // mass is unchanged
  return *this;

}


//----------------------------------------------------------------------
// return "sum" of two pseudojets
template <class T>
MomentumM2<T> operator+ (const MomentumM2<T> & jet1, const MomentumM2<T> & jet2) {  //return MomentumM2(jet1.four_mom()+jet2.four_mom());
  // use the constructor that is intended for sums of two objects
  return MomentumM2<T>(
      jet1.px()+jet2.px(),
		  jet1.py()+jet2.py(),
		  jet1.pz()+jet2.pz(),
		  jet1.E() +jet2.E(),
      jet1.m2(), 
      jet2.m2(), 
      dot_product(jet1,jet2)
  );
} 

//----------------------------------------------------------------------
// return difference of two pseudojets
template <class T>
MomentumM2<T> operator- (const MomentumM2<T> & jet1, const MomentumM2<T> & jet2) {
  return MomentumM2<T>(
      jet1.px()-jet2.px(),
		  jet1.py()-jet2.py(),
		  jet1.pz()-jet2.pz(),
		  jet1.E() -jet2.E() ,
      jet1.m2(), 
      jet2.m2(), 
      -dot_product(jet1,jet2)
  );
}

//----------------------------------------------------------------------
// unary minus
template <class T>
MomentumM2<T> operator-(const MomentumM2<T> & jet) {
  return MomentumM2<T>(-jet.px(),-jet.py(),-jet.pz(),-jet.E(),jet.m2());
}

//----------------------------------------------------------------------
// return the product, coeff * jet
template <class T>
MomentumM2<T> operator* (T coeff, const MomentumM2<T> & jet) {
  MomentumM2<T> coeff_times_jet(jet);
  coeff_times_jet *= coeff;
  return coeff_times_jet;
}

//----------------------------------------------------------------------
// return the product, jet / coeff
template <class T>
MomentumM2<T> operator/ (const MomentumM2<T> & jet, T coeff) {
  MomentumM2<T> jet_over_coeff(jet);
  jet_over_coeff /= coeff;
  return jet_over_coeff;
}

//----------------------------------------------------------------------
/// Returns the 3-vector dot-product of p1 and p2.
template <class T>
T dot3(const MomentumM2<T> & p1, const MomentumM2<T> & p2) {
  return p1.px() * p2.px() + p1.py() * p2.py() + p1.pz() * p2.pz();
}

//----------------------------------------------------------------------
/// Returns the 3-vector cross-product of p1 and p2. If lightlike is false
/// then the energy component is zero; if it's true the the energy component
/// is arranged so that the vector is lightlike
template <class T>
MomentumM2<T> cross(const MomentumM2<T> & p1, const MomentumM2<T> & p2, bool lightlike) {
  T px = p1.py() * p2.pz() - p2.py() * p1.pz();
  T py = p1.pz() * p2.px() - p2.pz() * p1.px();
  T pz = p1.px() * p2.py() - p2.px() * p1.py();

  T E, m2;
  if (lightlike) {
    E = sqrt(px*px + py*py + pz*pz);
    m2 = 0.0;
  } else {
    E = 0.0;
    m2 = -(px*px + py*py + pz*pz);
  }
  return MomentumM2<T>(px, py, pz, E, m2);
}

//----------------------------------------------------------------------
/// Returns the squared mass of the 3-vector cross-product of p1 and p2. 
/// The result is multiplied by norm before being returned 
/// (this can sometimes help with avoiding
/// over or underflows, notably in the calculation of the mass)
template <class T>
T cross_m2_norm(const MomentumM2<T> & p1, const MomentumM2<T> & p2, T norm) {
  T px = (p1.py() * p2.pz() - p2.py() * p1.pz());
  T py = (p1.pz() * p2.px() - p2.pz() * p1.px());
  T pz = (p1.px() * p2.py() - p2.px() * p1.py());

  T m2_norm = -((norm*px)*px + (norm*py)*py + (norm*pz)*pz);
  return m2_norm;
}


//----------------------------------------------------------------------
/// Returns two 4-vectors, each with square = -1, that have a zero
/// (4-vector) dot-product with dir1 and dir2.
///
/// When dir1 and dir2 form a plane, then perp1 will be out of that
/// plane and perp2 will be in the plane.
template <class T>
void twoPerpOld(const MomentumM2<T> & dir1, const MomentumM2<T> & dir2,
            MomentumM2<T> & perp1, MomentumM2<T> & perp2) {

  // First get a 3-vector that is perpendicular to both dir1 and dir2.
  //perp1 = cross(dir1+dir2,dir2-dir1);
  perp1 = cross(dir1,dir2);

  // for now test for exact zero -- later we will have to be more sophisticated...
  if (perp1.pt2() + perp1.pz()*perp1.pz() == 0.0) {
    if (fabs(dir1.px()) < fabs(dir1.py()) && fabs(dir1.px()) < fabs(dir1.pz())) {
      // x is smallest direction, so use that as a starting point
      // for a cross product
      perp2 = cross(dir1, MomentumM2<T>(1,0,0,0));
    } else if (fabs(dir1.py()) < fabs(dir1.pz())) {
      perp2 = cross(dir1, MomentumM2<T>(0,1,0,0));
    } else {
      perp2 = cross(dir1, MomentumM2<T>(0,0,1,0));
    }
    perp1 = cross(dir1,perp2);
    // arrange norm -1 for perp1
    perp1 /= sqrt((-perp1.m2()));
  } else {
    // arrange norm -1 for perp1. NB: this is done at this early stage
    // to help limit the risk of under/overflow when we calculate perp2
    // below using dir1rest .cross. perp1
    // (if dir1 and dir2 are both order delta, then perp1 is order delta^2
    // perp2  = dir1rest.cross. may be order delta^3 and perp2.m2() may be
    // order delta^6
    perp1 /= sqrt((-perp1.m2()));
    // requirements for perp2:
    // - 3-vector should be perpendicular to perp1
    // - 3-vector dot-product with dir1 and dir2 should be zero
    // - norm = -1
    //-----
    // go to centre-of-mass frame of the dipole
    MomentumM2<T> dir12 = dir1 + dir2;
    MomentumM2<T> dir1rest = dir1; dir1rest.unboost(dir12);
    // get something perpendicular to perp1 and either of the dipole directions
    // (since perp1 is perpendicular to both, it doesn't change with the boost)
    perp2 = cross(perp1,dir1rest);
    
    // MomentumM2<T> dir2rest = dir2; dir2rest.unboost(dir12);
    // cout << dir1rest << "  xxxd1" << endl;
    // cout << dir2rest << "  xxxd2" << endl;
    // cout << perp2 << " xxxp2" << endl;
    // boost back
    perp2.boost(dir12);
  }

  // finally, arrange norm -1 for perp2
  perp2 /= sqrt((-perp2.m2()));

}

template <class T>
void two_perp(const MomentumM2<T> & dir1, const MomentumM2<T> & dir2,
            MomentumM2<T> & perp1, MomentumM2<T> & perp2) {

  assert(dir1.m2() == 0);
  assert(dir2.m2() == 0);

  // #TRK_ISSUE-195  old
  // make sure directions are massless, so that we can use the
  // energy to normalise them to one
  // assert(dir1.m2() == 0);
  // assert(dir2.m2() == 0);

  // #TRK_ISSUE-195 new - MvB 16-05-2022
  // We check whether the masses are zero
  // if they are not, the normalisation should be |p| = sqrt(E^2 - m^2)
  // GPS 2022-07-26: replaced a bunch of code with just modp call
  // (this is clever enough to return E() if the mass is zero)
  // Note that it's better to avoid sqrt(E^2 - m^2), because rounding
  // errors (when m>>modp) could make modp quite innacurate and even 
  // negative
  //
  // modp is clever enough to return E() if the mass is zero
  T norm_1 = dir1.modp();
  T norm_2 = dir2.modp();
  
  MomentumM2<T> dir1unit = dir1/norm_1;
  MomentumM2<T> dir2unit = dir2/norm_2;
  
  // If particles are almost back to back, the algorithm in this routine
  // becomes dangerous numerically.
  //
  // So check if the angle between dir1unit and dir2unit is larger than pi/2,
  // and if so use the old routines based on boosts and cross products
  // in a centre-of mass frame
  //
  // NB: one should be able to do things even in this case without
  // needing to carry out boosts. For example use cross(perp1,dir1unit-dir2unit)
  // to set a direction and then fix up the (potentially small) energy
  // to arrange to suitable zeros in the diri.perp2 dot products.
  T dot12 = dot_product(dir1unit,dir2unit);


  // for now test for exact zero -- later we may have to be more sophisticated...
  if (dot12 > 1) {
    // First get a 3-vector that is perpendicular to both dir1unit and
    // dir2unit. This particular way of writing things involves
    // small-number * large-number when the the two directions are almost
    // back-to-back, which helps with rounding accuracy (specifically,
    // ensuring that if the result is non-zero, then it is genuinely
    // perpendicular to both directions)
    Momentum3<T> dir12sum_3  = dir1unit.mom3() + dir2unit.mom3();
    Momentum3<T> dir21diff_3 = dir2unit.mom3() - dir1unit.mom3();
    perp1 = MomentumM2<T>(cross(dir12sum_3, dir21diff_3), 0.0);

    if (perp1.pt2() + perp1.pz()*perp1.pz() == 0.0) {
      if (fabs(dir1unit.px()) < fabs(dir1unit.py()) && fabs(dir1unit.px()) < fabs(dir1unit.pz())) {
        // x is smallest direction, so use that as a starting point
        // for a cross product
        perp2 = cross(dir1unit, MomentumM2<T>(1,0,0,0));
      } else if (fabs(dir1unit.py()) < fabs(dir1unit.pz())) {
        perp2 = cross(dir1unit, MomentumM2<T>(0,1,0,0));
      } else {
        perp2 = cross(dir1unit, MomentumM2<T>(0,0,1,0));
      }
      perp1 = cross(dir1unit,perp2);
    } else {
      // this algorithm should be the more accurate one when the two
      // directions are almost back-to-back

      Momentum3<T> perp2_3 = -cross(perp1,dir21diff_3);
      T perp2_E = dot_product_3(perp2_3, dir1unit) / dir1unit.E();

      perp2 = MomentumM2<T>(perp2_3, perp2_E);
    }

  } else {

    // route to take when angles become small

    // when things are at small angle, this code is intended to be
    // reliable only if one of the two directions is along an x, y or z
    // axis, which ensures that the cross product is purely transverse
    perp1 = cross(dir1unit, dir2unit);

    // This algorithm is designed to be accurate when the two directions
    // are at small angles to each other (notably if one is aligned), but
    // it is not ideal when the two directions are almost back-to-back

    // It is derived in the
    // "Construction of perpendicular vectors" appendix/section of
    // ../../nll-shower-notes/2018-07-notes.tex
    
    // now work out the components and squared mass
    T x,y,z,E,m2;
    x = dir1unit.px() + dir2unit.px();
    y = dir1unit.py() + dir2unit.py();
    z = dir1unit.pz() + dir2unit.pz();
    E = dir1unit.E () + dir2unit.E () - dot12/dir1unit.E();
    m2 = -2.0*dot12 + pow2(dot12/dir1unit.E());
    perp2.reset_momentum(x,y,z,E,m2);
  }
  
  // arrange norm -1 for perp1 and perp2
  perp1 /= sqrt((-perp1.m2()));
  perp2 /= sqrt((-perp2.m2()));

}


/// Returns (1-cos theta) where theta is the angle between p1 and p2
template <class T>
T one_minus_costheta(const MomentumM2<T> & p1, const MomentumM2<T> & p2) {

  if (p1.m2() == 0 && p2.m2() == 0) {
    // use the 4-vector dot product. 
    // For massless particles it gives us E1*E2*(1-cos theta)
    return dot_product(p1,p2) / (p1.E() * p2.E());
  } else {
    T p1mod = p1.modp(); // sqrt(p1.px()*p1.px() + p1.py()*p1.py() + p1.pz()*p1.pz());
    T p2mod = p2.modp(); // sqrt(p2.px()*p2.px() + p2.py()*p2.py() + p2.pz()*p2.pz());
    T p1p2mod = p1mod*p2mod;
    T dot = dot3(p1,p2);

    // T result = p1mod * p2mod - dot3(p1,p2);
    // 
    // if (result < numeric_limit_extras<T>::sqrt_epsilon() * p1p2mod) {
    if (dot > (1-numeric_limit_extras<T>::sqrt_epsilon()) * p1p2mod) {
      // use the same trick that we had for the dot_product function,
      // but now with pimod instead of Ei
      MomentumM2<T> cross_result = cross(p1, p2, false);
      // the mass^2 of cross_result is equal to 
      // -(px^2 + py^2 + pz^2) = (p1mod*p2mod*sintheta_ab)^2
      // so we can get
      return -cross_result.m2()/(p1p2mod * (p1p2mod+dot));
    }

    return 1.0 - dot/p1p2mod;
    
  }
}

  //------------------------------------------------------------------------
  // for the direction_diff, introduce an intermediate class to help
  // with precision
  //
  // This stores each coordinate x as an integer x_i and a
  // floating-point number x_f such that
  //   x = x_i + x_f
  // and the coordinates are computed precisely if x is close to +-1
  template <typename T>
  class DirectionHelper{
  public:
    class SplitFloat{
    public:
      SplitFloat() : int_part(0), float_part(0.0){}
      SplitFloat(int int_part_in, T float_part_in)
        : int_part(int_part_in), float_part(float_part_in){};
      int int_part;
      T float_part;
    };

    
    DirectionHelper(const Momentum3<T> &direction){
      std::vector<T> coords = {direction.px(), direction.py(), direction.pz()};
      //dir.reserve(3);
      dir.resize(3);
      for (unsigned int i=0;i<3;++i){
        // close to the endpoints, compute the coordinate from the other 2
        if (1.0-std::abs(coords[i]) < numeric_limit_extras<T>::sqrt_epsilon()){
          int sign = (coords[i]>0) ? 1 : -1;
          unsigned int j = (i+1)%3;
          unsigned int k = (i+2)%3;
          // use xi - sign = sign * (sqrt(1-xj^2-xk^2) - 1)
          //               ~ - sign * ( (xj^2+xk^2)/2 +  (xj^2+xk^2)^2/8 )
          T xt2 = coords[j]*coords[j] + coords[k]*coords[k];
          dir[i] = SplitFloat(sign, -sign*xt2/2*(1+xt2/4));
        } else {
          dir[i] = SplitFloat(0, coords[i]);
        }
      }          
    }

    std::vector<SplitFloat> dir;
      
  };

  // direction difference: a-b
  template<typename T>
  Momentum3<T> operator-(const DirectionHelper<T> &a, const DirectionHelper<T> &b){
    return Momentum3<T>((a.dir[0].int_part-b.dir[0].int_part) + (a.dir[0].float_part-b.dir[0].float_part),
                        (a.dir[1].int_part-b.dir[1].int_part) + (a.dir[1].float_part-b.dir[1].float_part),
                        (a.dir[2].int_part-b.dir[2].int_part) + (a.dir[2].float_part-b.dir[2].float_part));
  }

  
  template<typename T>
  Momentum3<T> MomentumM2<T>::direction_diff(const MomentumM2 &other) const{
    return DirectionHelper<T>(direction()) - DirectionHelper<T>(other.direction());
  }
    

template class MomentumM2<precision_type>;
template MomentumM2<precision_type> operator+<precision_type>(const MomentumM2<precision_type>&,
					    const MomentumM2<precision_type>&);
template MomentumM2<precision_type> operator-<precision_type>(const MomentumM2<precision_type>&,
					    const MomentumM2<precision_type>&);
template MomentumM2<precision_type> operator-<precision_type>(const MomentumM2<precision_type>&);
template MomentumM2<precision_type> operator/<precision_type>(const MomentumM2<precision_type>&, precision_type);
template MomentumM2<precision_type> operator*<precision_type>(precision_type, const MomentumM2<precision_type>&);
template precision_type dot_product(const MomentumM2<precision_type> &,
			    const MomentumM2<precision_type> &);
//template precision_type pow2(precision_type);
template std::ostream & operator<<(std::ostream &, const MomentumM2<precision_type> &);
template precision_type dot3(const MomentumM2<precision_type> &, const MomentumM2<precision_type> &);
template MomentumM2<precision_type> cross(const MomentumM2<precision_type> &, const MomentumM2<precision_type> &, bool);
template void twoPerpOld(const MomentumM2<precision_type> &, const MomentumM2<precision_type> &,
		      MomentumM2<precision_type> &, MomentumM2<precision_type> &);
template void two_perp(const MomentumM2<precision_type> &, const MomentumM2<precision_type> &,
		      MomentumM2<precision_type> &, MomentumM2<precision_type> &);
template precision_type one_minus_costheta(const MomentumM2<precision_type> &, const MomentumM2<precision_type> &);

} // namespace panscales
