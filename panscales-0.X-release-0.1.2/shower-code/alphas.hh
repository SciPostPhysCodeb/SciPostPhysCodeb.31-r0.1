//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __ALPHAS_HH__
#define __ALPHAS_HH__

#include <cassert>
#include <limits>
#include <memory>
#include <vector>
#include "Type.hh"
#include "UniformInterpolationGrid.hh"

namespace panscales{

class AlphasBase;
class AlphasRunning;
class AlphasRGRunning;
class Alphas5VFS;
class AlphasPowhegStyleVFS;
  
//--------------------------------------------------------------------
/// @ingroup alphas_classes
/// \class AlphasBase
/// base clase to handle alphas
/// Use one of the derived classes below:
///  - AlphasRunning    running coupling (explicit log expansion)
///  - AlphasRGRunning  running coupling (exact RG solution)              [work in progress]
///  - Alphas5VFS       running coupling with variable number of flavours
///  - AlphasPowhegStyleVFS  running coupling with variable number of flavours as in Powheg
///
/// Note on the treatment of K. By default we will write
///     alphas_CMW = alphas + (K/2pi) alphas^2 + [K2/(2pi)^2 + A3] alphas^3 + ...
/// (allowing for an A3 != K_2, and 0 by default in the 2-loop case, 
/// to play w subleading contributions.) This is not what was in the PRL so we leave the
/// option (at least for the AlphasRunning class, to instead use the
/// 1-loop alphas (squared) for the K term.
///
/// Fixed-coupling can be obtained with AlphasRunning(nloops=0, ...)
class AlphasBase {
public:
  /// default ctor
  ///  \param nloops_in        number of loops
  ///  \param alphasMSbarQ_in  strong coupling in the MSbar scheme at the scale Q
  ///  \param lnQ_in           logarithm od the reference scale Q
  ///  \param lnkt_cutoff_in   kt scale below which we set alphas to 0
  AlphasBase(unsigned int nloops_in, double alphasMSbarQ_in,
             double lnQ_in, double lnkt_cutoff_in);

  /// default dtor  (virtual so that we 
  virtual ~AlphasBase() {};

  /// possibilitty to handle QCD colour factors.
  /// since this is the case for all options except the fixed-coupling
  /// one, return true by default
  virtual bool has_qcd_constants() const { return true; }
  void set_qcd_constants(double CF_in, double CA_in, double TR_in=0.5);

  /// possibilitty to handle mass thresholds.
  virtual bool has_mass_thresholds() const { return false; }
  void set_ln_mass_thresholds(double lnmc_in, double lnmb_in);

  /// number of loops
  unsigned int nloops() const{ return _nloops; }

  /// change the use of the CMW scheme
  virtual void set_use_CMW(bool use_CMW_in=true){ _use_CMW = use_CMW_in; }
  bool use_CMW() const {return _use_CMW; }

  /// if false, defer the KCMW correction to alphas_calc
  virtual void set_global_CMW(bool global_CMW_in=true){ _global_CMW = global_CMW_in; }
  bool global_CMW() const {return _global_CMW; }

  /// if false, defer the A3 correction to alphas_calc
  virtual void set_effective_A3_CMW(double effective_A3_in=0.0){ _effective_A3 = effective_A3_in;}
  double effective_A3_CMW() const {return _effective_A3; }

  /// get the reference scale and the coupling there
  double lnQ() const { return _lnQ; }
  double alphasMSbarQ() const { return _alphasMSbarQ; }

  /// get (the log of the) scale at which alphas is set to 0
  void set_lnkt_cutoff(double lnkt_cutoff_in){
    _lnkt_cutoff = lnkt_cutoff_in;
    _check_cutoff_validity();
  }
  double lnkt_cutoff() const {return _lnkt_cutoff; }

  /// description
  virtual std::string description() const = 0;

  /// get info about various constants
  virtual double b0() const { return 0.0; }
  virtual double b1() const { return 0.0; }
  virtual double b2() const { return 0.0; }
  virtual double K() const { return 0.0; }
  virtual double K2() const { return 0.0; }
  virtual double b0(double lnkt) const { return b0(); }
  virtual double b1(double lnkt) const { return b1(); }
  
  /// returns the value of alphas at a given scale;
  /// At two or three loops, if the CMW flag is on, then it's 
  /// the physical (CMW-scheme) coupling that is returned.
  double alphas(double lnkt) const{
    if ((lnkt< _lnkt_cutoff) && (_lnQ>=_lnkt_cutoff))
      return 0.0;
    return alphas_no_cutoff(lnkt);
  }

  /// returns the value of the MSbar-scheme alphas at a given scale.
  double alphasMSbar(double lnkt) const{
    if ((lnkt< _lnkt_cutoff) && (_lnQ>=_lnkt_cutoff))
      return 0.0;
    return alphasMSbar_no_cutoff(lnkt);
  }

  /// returns the value of alphas at a given scale (no cutoff imposed)
  /// At two or three loops, if the CMW flag is on, then it is the physical
  /// (CMW-scheme) coupling that is returned.
  ///
  /// We make this virtual so that we can include mass threshold
  /// effects in our treatment of KCMW in classes below
  virtual double alphas_no_cutoff(double lnkt) const;

  /// returns the value of the MSbar-scheme alphas at a given scale (no cutoff imposed)
  double alphasMSbar_no_cutoff(double lnkt) const{
    return _alphas_calc(lnkt, false);
  }

  /// a shorter version valid for fixed coupling
  double alphas() const{
    assert((_nloops==0) && "QCDinstance: alphas() without arguments is only allowed for fixed coupling (nloops==0)");
    return _alphasMSbarQ;
  }
        
  /// running the largest value alphas can take at scales bigger
  /// than lnkt_min (in the same scheme as the alphas(lnkt) function)
  double max_alphas_above_lnkt(double lnkt_min=0.0) const{
    // if lnkt_min is below the cutoff scale, alphas has to be taken
    // at the cutoff scale. otherwise, it is taken at lnkt_min.
    return alphas_no_cutoff(std::max(lnkt_min,_lnkt_cutoff));
  }

protected:
  /// where the actual computation of alphas happens. This is the
  /// main method that one should implement in derived classes.
  virtual double _alphas_calc(double lnkt, bool include_CMW=false) const = 0;

  /// This will be called after each parameter change.
  /// It should perform sanity checks and update internal objects.
  virtual void _check_and_update_if_needed() {}

  /// possibility to check if the provided cutoff makes sense
  virtual void _check_cutoff_validity() const {};

  // fundamental alphas parameters
  unsigned int _nloops; ///< order (# loops) at which we compute alphas
  double _alphasMSbarQ; ///< starting value of alphas
  double _lnQ;          ///< at the reference scale Q
  double _lnkt_cutoff;  ///< scale at which we set alphas to 0
  bool _use_CMW;        ///< when true, calls to alphas(lnkt) using the CMW scheme

  // (optional) QCD constant parameters
  double _CF, _CA, _TR;
  unsigned int _nf;

  bool _global_CMW;     ///< when true (the default) the CMW correction is applied
                        ///< globally on the full alphas expression (otherwise
                        ///< it is deferred to the_alphas_calc in each class)
  double _K;            ///< CMW alphas^2 constant
  double _K2;           ///< CMW alphas^3 constant
  double _effective_A3; ///< effective A3 alphas^3 correction

  // (optional) mass parameters
  double _lnmc, _lnmb;
};



//--------------------------------------------------------------------------------
/// @ingroup alphas_classes
/// \class AlphasRunning
/// alphas running with fixed number of flavours and a given number of loops
/// [Currently supported: 1, 2 or 3 loops]
/// In this case, the running of alphas is limited to what is
/// necessary for log accuracy
class AlphasRunning  : public AlphasBase{
public:
  /// ctor
  ///  \param nloops_in        number of loops
  ///  \param alphasMSbarQ_in  strong coupling in the MSbar scheme at the scale Q
  ///  \param lnQ_in           logarithm od the reference scale Q
  ///  \param lnkt_cutoff_in   kt scale below which we set alphas to 0
  ///  \param nf_in            (fixed) number of active flavours
  /// GS-NOTE: do we want to put the basic QCD constants in an even more basic class?
  /// 
  AlphasRunning(unsigned int nloops_in, double alphasMSbarQ_in,
                double lnQ_in, double lnkt_cutoff_in,
                unsigned int nf_in = 5);

  /// default destructor
  virtual ~AlphasRunning() {};

  /// description
  virtual std::string description() const override;

  /// get info about various constants
  virtual double b0() const override { return _b0; }
  virtual double b1() const override { return _b1; }
  virtual double b2() const override { return _b2; }
  virtual double K() const override  { return _K; }
  virtual double K2() const override { return _K2; }
  
  /// adjust the lnkt cutoff so as to get the requested maximal alphas value
  /// Note: at 2,3-loop, this is set using the 1-loop running
  void set_max_alphas(double alphas_max){
    assert(_nloops>0);
    set_lnkt_cutoff(_lnQ + (_alphasMSbarQ-alphas_max)/(2*_alphasMSbarQ*alphas_max*_b0));
  }

protected:
  /// actual calculation 
  virtual double _alphas_calc(double lnkt, bool include_CMW) const override;

  /// update the coefficients of the beta function and cusp anomalous
  /// dimension needed for the running of the coupling
  virtual void _check_and_update_if_needed() override;

  /// check if the requested lnkt_cutoff is above the Landau pole
  virtual void _check_cutoff_validity() const override;

  double _b0, _b1, _b2;  ///< QCD Beta function (CMW is in the base class)

  // allow VFS classes to call our protected members (essentially to
  // relay the _alphas_calc calls)
  friend Alphas5VFS;
};
  
  
//--------------------------------------------------------------------------------
/// @ingroup alphas_classes
/// \class AlphasRGRunning
/// running alphas obtained by solving explicitly the QCD
/// renormalisation group equation
///
/// We use (L=log(kt/Q))
///   1/2 das/dL = - b0 as^2 - b1 as^3 + ...
///   d(1/as)/dL = 2 b0 + 2 b1/(1/as) + ...
///
/// WATCH OUT: this implementation is still very preliminary... use at
/// your own risks. Things to be aware of:
///  - it uses a 3-loop RGE
///  - it uses a linear interpolation
///  - upper limit is hardcoded
class AlphasRGRunning  : public AlphasRunning{
public:
  /// ctor
  AlphasRGRunning(unsigned int nloops_in, double alphasMSbarQ_in, double lnQ_in, double lnkt_cutoff_in,
                  unsigned int nf_in = 5);

  /// default dtor
  virtual ~AlphasRGRunning() {}

  /// description
  virtual std::string description() const override;
  
protected:
  /// actually calculate alphas
  virtual double _alphas_calc(double lnkt, bool include_CMW) const override;
  
  /// update the coefficients of the beta function and cusp anomalous
  /// dimension needed for the running of the coupling
  virtual void _check_and_update_if_needed() override{
    AlphasRunning::_check_and_update_if_needed();
    _update_interp_table();
  }

  /// update of the interpolation table used
  virtual void _update_interp_table();

  const double _tabulation_lnkt_spacing = 0.02; //< TODO: fix the step instead of the number of points (Gavin uses 0.025 steps in lnkt)
  const double _upper_lnkt_margin = 5.0;
  unsigned int _n_tabulated_below; // table goes from 0 to n_tabulated_below-1
  unsigned int _n_tabulated_above; // table goes from 0 to n_tabulated_above-1
  std::vector<double> _tabulated_lnkt_below,            _tabulated_lnkt_above;
  UniformInterpolationGrid _tabulated_one_over_alphas_below, _tabulated_one_over_alphas_above;
  
  // allow VFS classes to call our protected members (essentially to
  // relay the _alphas_calc calls
  friend Alphas5VFS;
};
  
//--------------------------------------------------------------------------------
/// @ingroup alphas_classes
/// \class Alphas5VFS
/// alphas running with variable number of flavours and a given number of loops
/// [Currently supported: 1 or 2 loops]
/// For the underlying fixed-nf segments, one has the option to use either a
/// "log-accurate" running or the exact RG equation (only the former is
/// currently implemented)
///
/// Note: flavour transitions below lnkt_cutoff are not implemented!!
class Alphas5VFS  : public AlphasBase{
public:
  /// ctor
  Alphas5VFS(unsigned int nloops_in, double alphasMSbarQ_in, double lnQ_in, double lnkt_cutoff_in,
             double lnmc_in, double lnmb_in, bool use_rg = false);

  // destructor
  virtual ~Alphas5VFS(){}
  
  /// possibility to handle mass thresholds.
  virtual bool has_mass_thresholds() const override{ return true; }

  /// description
  virtual std::string description() const override;

  /// relay info to individual pieces
  virtual void set_use_CMW(bool use_CMW_in=true) override;
  virtual void set_global_CMW(bool global_CMW_in=true) override;
  virtual void set_effective_A3_CMW(double effective_A3_in=0.0) override;

  double get_effective_A3_CMW(double lnkt){
    return _get_alphas_object_at_scale(lnkt, _use_CMW)->effective_A3_CMW();
  }
  
  /// get info about various constants
  virtual double b0() const override { return _alphas5->b0(); }
  virtual double b1() const override { return _alphas5->b0(); }
  virtual double b0(double lnkt) const override { return _get_alphas_object_at_scale(lnkt, _use_CMW)->b0(); }
  virtual double b1(double lnkt) const override { return _get_alphas_object_at_scale(lnkt, _use_CMW)->b0(); }

  /// redefine this so that we automatically pick the right "K_CMW" if
  /// it is to be applied globally
  virtual double alphas_no_cutoff(double lnkt) const override{
    return _get_alphas_object_at_scale(lnkt, _use_CMW)->alphas_no_cutoff(lnkt);
  }
  
protected:
  /// actual computation of alphas
  virtual double _alphas_calc(double lnkt, bool include_CMW) const override{
    // The call below requires AlphasBase to be friend w this class
    return _get_alphas_object_at_scale(lnkt, include_CMW)->_alphas_calc(lnkt, include_CMW);
  }

  /// check if things need to be updated
  virtual void _check_and_update_if_needed() override;

  /// simple tool to create the right instance for the underlying
  /// classes handling the running
  AlphasRunning * _create_alphas_fixed_nf(double alphasQ, double lnQ, unsigned int nf) const;

  /// compute the point at which alphas_CMW is continuous
  double _compute_alphasCMW_thresholds(AlphasRunning *alphas_above, AlphasRunning *alphas_below,
                                       double lnm) const;

  /// check if the lnkt_cutoff is above the Landau pole
  virtual void _check_cutoff_validity() const override;

  /// returns one of _alphas{3,4,5} depending on which one is relevant
  /// at the scale lnkt
  const AlphasRunning * _get_alphas_object_at_scale(double lnkt, bool include_CMW) const;
  
  /// (pointers to) the alphas evaluations w fixed flavours
  /// available only if the mass threshold is above the cutoff scale
  bool _use_rg;
  std::unique_ptr<AlphasRunning> _alphas5, _alphas4, _alphas3;
  double _lnmc_CMW, _lnmb_CMW;
};




//--------------------------------------------------------------------------------
/// @ingroup alphas_classes
/// \class AlphasPowhegStyleVFS
/// old implementation (borrowed from Powheg) of the
/// variable-flavour thresholds (use only as cross-check).
class AlphasPowhegStyleVFS  : public AlphasRunning{
public:
  /// ctor
  AlphasPowhegStyleVFS(unsigned int nloops_in, double alphasMSbarQ_in, double lnQ_in, double lnkt_cutoff_in)
    : AlphasRunning(nloops_in, alphasMSbarQ_in, lnQ_in, lnkt_cutoff_in){
    set_global_CMW(false); 
  }

  // destructor
  virtual ~AlphasPowhegStyleVFS(){}
  
  /// possibilitty to handle mass thresholds.
  virtual bool has_mass_thresholds() const override{ return true; }

  /// description
  virtual std::string description() const override;
  
protected:
  /// actual calculation of alphas
  virtual double _alphas_calc(double lnkt, bool include_CMW) const override;
};
  
} // namespace panscales

#endif  // __ALPHAS_HH__
