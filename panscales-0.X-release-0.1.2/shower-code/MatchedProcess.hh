//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __MatchedProcess_HH__
#define __MatchedProcess_HH__

#include <string>
#include <vector>
#include "ShowerBase.hh"
#include "Event.hh"
#include "GSLRandom.hh"

namespace panscales {
  /// matching scheme
  enum MatchingScheme : unsigned int {PowhegStyle, MCAtNLOStyle};
  // forward declaration of Strategy
  enum Strategy : unsigned int;
  // forward declaration of gsl
  extern GSLRandom gsl;
  // forward declaration to MatchingHardMatrixElement
  class MatchingHardMatrixElement;

  //----------------------------------------------------------------------
  /// @ingroup matching
  /// \class MatchedProcess
  /// Base class for matching to fixed order
  class MatchedProcess {
  public:

    /// Sets the hard matrix element pointer, main shower, matching shower, strategy
    /// of selecting the elements, the matching scheme
    MatchedProcess(
      MatchingHardMatrixElement * hard_me, ShowerBase * main_shower, ShowerBase * shower, Strategy strategy,
      MatchingScheme matching_scheme, bool f_random_axis, bool power_shower, HoppetRunner * hoppet_runner = NULL) :
        _hard_me(hard_me),
        _shower(shower),
        _matching_scheme(matching_scheme),
        _power_shower(power_shower),
        _random_axis(f_random_axis),
        _hoppet_runner(hoppet_runner) {

      // Determine whether the shower & strategy combination is supported by
      // the MatchedProcess and set _is_active accordingly.
      _shower_name = _shower->name();

      // For PanGlobal showers, there is an issue that the hard boundary
      // (diagonal) in the Dalitz plane for emission from a dipole, is
      // unpopulated by the default lnv+element generators. We have adapted
      // (so far) RoulettePeriodic with a so-called bias factor to populate
      // this region. These next lines take care of setting that.
      if(_shower_name=="PanScaleGlobal-ee" && _matching_scheme==PowhegStyle){
        _find_element_alg_bias = _random_axis ? 1.0 : 0.5;
      } else {
        _find_element_alg_bias = 0.0;
      }
      
      // Set _matching_needs_vetoing
      check_matching_needs_vetoing(main_shower);
    }
    // Dtor
    virtual ~MatchedProcess() {}


    /// Returns true if the given combination of showers needs vetoing
    /// NOTE: only works (and tested!) for
    ///
    ///       Matching shower        Main shower
    /// -------------------------------------------
    ///       PanGlobal              PanLocal-Vincia
    ///       Powheg                 PanGlobal
    ///       Powheg                 PanLocal
    void check_matching_needs_vetoing(ShowerBase * main_shower);

    /// verifies that the strategy is compatible with Matching
    void check_strategy_is_compatible(Strategy strategy) const;

    /// If the point in (lnkt,eta) was already covered by the _shower_for_matching
    /// contour at _lnvfirst, veto.
    bool veto_emission(double & lnv_first, ShowerBase::EmissionInfo & emission_info) const;

    /// returns true if matching needs vetoing
    bool matching_needs_vetoing() const { return _matching_needs_vetoing; }

    /// Switch on/off matching veto: if it was needed, the result will not be
    /// correct at NNDL!
    void enable_matching_veto(bool value = true) { _matching_needs_vetoing = value; }

    /// returns true if we set up matching with a shower that starts from the top
    bool is_power_shower() const { return _power_shower; }

    MatchingScheme get_matching_scheme() const { return _matching_scheme;}

    /// Return a bias factor for the initial lnv & element generator. This value
    /// is also needed in MatchedProcess to get the right acceptance_probability.
    double find_element_alg_bias() const { return _find_element_alg_bias; }

    /// Allow the latter bias factor to be reset from the outside world. This is
    /// is relevant to compute the acceptance probability correctly in instances
    /// where the lnv generation is not done by a _find_element_* function, e.g.
    /// as in numerical integration routines to get the first emission Sudakov.
    void set_find_element_alg_bias(double find_element_alg_bias) {
      _find_element_alg_bias = find_element_alg_bias;
    }

    /// Description of the matching class
    virtual std::string description() const {return "MatchedProcess with matching_scheme = " + std::to_string(_matching_scheme);}

    /// resets tracked indices
    void reset_splitter_index() {
      _veto_splitter_index_1 = 0;
      _veto_splitter_index_2 = 1;
    }

    /// Updates the splitter index to follow the hard branch if a splitting
    /// has occurred
    void update_splitter_index(ShowerBase::EmissionInfo & emission_info, Event & event);

    /// checks wheter the splitting is born
    bool is_splitter_born(ShowerBase::EmissionInfo & emission_info) {
      return ( emission_info.splitter_index() == _veto_splitter_index_1
            || emission_info.splitter_index() == _veto_splitter_index_2 );
    }

    /// Acceptance probability for matching
    void acceptance_probability(Event & event, ShowerBase::Element & element,
                                        ShowerBase::EmissionInfo & emission_info, double & prob);
    
    /// This function fills the weights for flavour and splitter
    /// (gluon vs. quark, and emitter vs. spectator) in EmissionInfo,
    /// once the SplittingChannel has been chosen
    void update_emitter_spectator_weights_channel(ShowerBase::EmissionInfo & emission_info, const unsigned int splitting_channel);

  protected:

    // Pointer to the exact matrix element
    MatchingHardMatrixElement* _hard_me;

    // Pointer to the shower in the ctor, currently initiated in AnalysisFramework.cc.
    ShowerBase * _shower;

    // Matching scheme
    MatchingScheme _matching_scheme;

    // Is true if we want to start the shower from the top of the Lund plane
    bool _power_shower = false;

    // Flag is set to true if the matching needs vetoing
    bool _matching_needs_vetoing = false;

    // Switch off the matching veto even if it is needed (use carefully!)
    bool _disable_matching_veto = false;

    // particle indices to veto against (for shower combinations which need vetoing)
    int _veto_splitter_index_1 = 0;
    int _veto_splitter_index_2 = 1;

    // The name of the shower.
    std::string _shower_name;

    // Bias factor for lnv+element generators. Detailed comments in ctor.
    double _find_element_alg_bias;

    // set to true if the shower generates the event with full angles
    bool _random_axis;

    // Pointer to hoppetrunner
    HoppetRunner * _hoppet_runner;

  };
} // namespace panscales



#endif // __MatchedProcess_HH__
