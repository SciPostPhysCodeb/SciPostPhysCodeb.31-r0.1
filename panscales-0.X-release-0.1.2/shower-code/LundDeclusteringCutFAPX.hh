//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __LUND_DECLUSTERING_CUT_FAPX_HH__
#define __LUND_DECLUSTERING_CUT_FAPX_HH__

#include "ShowerRunner.hh"

namespace panscales{

///-----------------------------------------------------------------------------------
/// @ingroup emveto_classes
/// \class LundDeclusteringCutFAPX
/// class for vetoing emissions when computing Lund-declustering based observables.
 
// We consider the deltaPsi distribution (in a Lund declustering sense) in a given
// lnkt1 bin (L_low < lnkt1 < L_high), where kt1 is the transverse momentum of the
// hardest (kt) emission.
//
// 1. For beta_PS > beta_OBS, we want to veto all events with emissions whose lnkt is larger than
// the upper generation cut of the shower, which in turn must be set to lnvmax=L_high + buffer,
// where buffer is a positive quantity (at least 5).
//
// 2. Analogously, at the low end we want to set the shower cutoff such that
// lnvmin=(L_low - buffer)*(1+beta_PS), where once again buffer is a positive
// quantity.
//
// In this routine we implement the veto (1.) for events with emissions whose lnkt
// is larger than lnvmax. All other conditions must be given as command line options
// depending on what lnkt bin the user is interested in
//-----------------------------------------------------------------------------------

template <class Shower>
class LundDeclusteringCutFAPX : public EmissionVeto<Shower> {
//
public:

  /// Default ctor
  LundDeclusteringCutFAPX(Float lnvmin, Float lnvmax) : EmissionVeto<Shower>(), _lnvmin(lnvmin), _lnvmax(lnvmax) {}

  /// veto the events with emissions much above L_high before the
  /// calculation of the acceptance probability.
  /// This function returns true if the emission is to be vetoed.
  /// It sets lnv to -std::numeric_limits<double>::max() if the
  /// event as a whole is to be terminated.
  virtual bool pre_accept_prob_veto(Event & event, typename Shower::Element & element,
                                    double & lnv, double & lnb, double & weight,
                                    bool is_global = false) override {
    
    /// Terminate the shower if it generates emissions with a transverse
    /// momentum larger than _lnvmax (i.e. the lnktmax at eta=0)
    if ((element.lnkt_approx(lnv, lnb) > _lnvmax)) {
      lnv = -std::numeric_limits<double>::max();
      return true;
    }

    /// Terminate shower if an emission is generated below the lnkt corresponding to the
    /// hard collinear limit for lnvmin
    if (element.lnkt_approx(lnv, lnb) < _lnvmin/(1.+element.betaPS())) {
      lnv = -std::numeric_limits<double>::max();
      return true;
    }

    return false;
  }

protected:

  /// upper and lower generation cuts of the shower
  Float _lnvmin;
  Float _lnvmax;
};

} // namespace panscales

#endif // __LUND_DECLUSTERING_CUT_FAPX_HH__
