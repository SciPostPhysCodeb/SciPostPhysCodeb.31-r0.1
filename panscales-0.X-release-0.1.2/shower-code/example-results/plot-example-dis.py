#!/usr/bin/env python3

import numpy as np
import matplotlib as mpl
mpl.use('Agg') # avoids requirement to have DISPLAY set
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches as patches
import math
import os.path
import sys
sys.path.insert(0, os.path.dirname(__file__) + "/../../submodules/AnalysisTools/python")
from hfile import *

font = {'color':  'black',
    'weight': 'normal',
    'size': 18
    }

#plt.rcParams['text.usetex'] = True
#plt.rcParams['font.family'] = 'serif'
#plt.rcParams['font.serif'] = ['Helvetica']
plt.rcParams['xtick.labelsize'] = 14
plt.rcParams['ytick.labelsize'] = 14
plt.rcParams['legend.fontsize'] = 14
plt.rcParams['legend.handlelength'] = 1

# Extract the shower values
def get_numerics(fname, dist):
    data = get_array(fname,'diff_hist:evshp:'+dist)
    xval = data[:,1]
    avg = data[:,3]
    err = data[:,4]
    return xval, avg, err

# File name
#f = os.path.dirname(__file__) + '/example-dis.dat'
f = 'example-dis.dat'
# Observables
observables = ['tau_wrt_axis','B_z','CParam', 'rho']
# Plotting details
xlabels = [r'$\tau$', r'$B_z$', r'$C$', r'$\rho$']
ylabels = [r'$1/N~dN/d\tau$', r'$1/N~dN/dB_z$', r'$1/N~dN/dC$', r'$1/N~dN/d\rho$']
# Image name
output = ['thrust', 'broadening', 'cparam', 'rho']
#----------------------------------------------------------------
for obs, xlab, ylab, out in zip(observables, xlabels, ylabels, output):
    fig = plt.figure(figsize=(5,3.8))
    ax = plt.gca()
    plt.grid(True, lw=0.5, ls=':', zorder=0,color='silver')
    plt.tick_params(axis='both', which='both', direction='in', bottom=True, top=False, left=True, right=True )    

    plt.plot(get_numerics(f,obs)[0], get_numerics(f,obs)[1],color='dodgerblue')
    plt.fill_between(get_numerics(f,obs)[0], get_numerics(f,obs)[1]+get_numerics(f,obs)[2], get_numerics(f,obs)[1]-get_numerics(f,obs)[2], color='dodgerblue',alpha=0.2,edgecolor='none')
    plt.xlabel(xlab,fontsize=16)
    plt.ylabel(ylab,fontsize=16)
    plt.yscale('log')
    plt.ylim(0.001,100)
    if((out == "cparam")):
        plt.xlim(0,1.0)
    elif (out == 'rho'):
        plt.xlim(0,0.25)
    else:
        plt.xlim(0,0.5)
    plt.text(1.01,0.02, r'PanLocal$(\beta_{ps}=1/2)$, $\sqrt{s} = 320$ GeV, $Q^2 = 100$ GeV$^2$, $\alpha_s(M_Z)=0.118$', transform=ax.transAxes, ha='left', fontsize=6.5, rotation=270, color='grey')

    plt.savefig(out+'-dis.png',bbox_inches='tight',dpi=300)
    print(f"Plotting {out} in {out}-dis.png")
    
    plt.close()
