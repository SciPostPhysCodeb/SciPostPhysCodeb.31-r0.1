# ../submodules/AnalysisTools/scripts/combine-runs.pl 'example-results/non-global-test/panglobal-beta0.0/runs/lambda-0.5-alphas1e-09-rseq1.res' 'example-results/non-global-test/panglobal-beta0.0/runs/lambda-0.5-alphas1e-09-rseq2.res' 'example-results/non-global-test/panglobal-beta0.0/runs/lambda-0.5-alphas1e-09-rseq3.res' 'example-results/non-global-test/panglobal-beta0.0/runs/lambda-0.5-alphas1e-09-rseq4.res' 'example-results/non-global-test/panglobal-beta0.0/runs/lambda-0.5-alphas1e-09-rseq5.res' 'example-results/non-global-test/panglobal-beta0.0/runs/lambda-0.5-alphas1e-09-rseq6.res' 'example-results/non-global-test/panglobal-beta0.0/runs/lambda-0.5-alphas1e-09-rseq7.res' 'example-results/non-global-test/panglobal-beta0.0/runs/lambda-0.5-alphas1e-09-rseq8.res'
# until occurrence of 'nev', comments that follow are from first of the 8 files, example-results/non-global-test/panglobal-beta0.0/runs/lambda-0.5-alphas1e-09-rseq1.res
# build-doubleexp/example-nonglobal-nll-ee -Q 1.0 -shower panglobal -beta 0.0 -nloops 2 -colour CATwoCF -strategy CentralRap -half-central-rap-window 11 -lambda-obs-min -0.5 -alphas 1e-09 -lnkt-cutoff -501000000.0 -slice-maxrap 1.0 -ln-obs-margin -10 -spin-corr off -nev 10000 -rseq 11 -out example-results/non-global-test/panglobal-beta0.0/runs/lambda-0.5-alphas1e-09-rseq1.res 
# from path: /Users/melissa/Work/Panscales/panscales-main/2020-eeshower/shower-code
# started at: 2023-12-07 17:53:59 (CET)
# by user: melissa
# running on: Darwin dhcp-135-073.nikhef.nl 21.3.0 Darwin Kernel Version 21.3.0: Wed Jan  5 21:37:58 PST 2022; root:xnu-8019.80.24~20/RELEASE_ARM64_T8101 arm64
# git state (if any): e16ec6902980823ec72f7bc29bd151766b3b78e2  (HEAD -> master, origin/master, origin/HEAD) of Thu Dec 7 17:30:52 2023 +0100;;  M 2020-eeshower/analyses/pp-analyses/plot-lnvfirst.py, M 2020-eeshower/shower-code/ShowerRunner.cc, M 2020-eeshower/shower-code/ShowerToyDire.cc, M 2020-eeshower/shower-code/example-nonglobal-nll-ee.py, M logbook/2021-08-01-triple-collinear-structure/c++/analysis-veto-scheme.cc, M logbook/2021-08-01-triple-collinear-structure/c++/soft-drop.cc, M logbook/2021-08-01-triple-collinear-structure/hoppet-refs/mkmk, M logbook/2021-08-01-triple-collinear-structure/math/results-PM/OneLoopMatchingCoefficient/CoefficientFunctionSmallR-ktminShower.nb,
# precision mode = double_exp
# precision estimate [2 - pow(sqrt(2.0),2)] = -4.44089e-16, epsilon = 2.22045e-16, min = 1e-6.94128e+17, max = 1e6.94128e+17
# libpanscales compiled git info: e16ec6902980823ec72f7bc29bd151766b3b78e2, 2023-12-07 17:30:52 +0100 + UNCOMMITTED CHANGES:  M 2020-eeshower/analyses/pp-analyses/plot-lnvfirst.py; M 2020-eeshower/shower-code/ShowerRunner.cc; M 2020-eeshower/shower-code/ShowerToyDire.cc; M 2020-eeshower/shower-code/example-nonglobal-nll-ee.py; M logbook/2021-08-01-triple-collinear-structure/c++/analysis-veto-scheme.cc; M logbook/2021-08-01-triple-collinear-structure/c++/soft-drop.cc; M logbook/2021-08-01-triple-collinear-structure/hoppet-refs/mkmk; M logbook/2021-08-01-triple-collinear-structure/math/results-PM/OneLoopMatchingCoefficient/CoefficientFunctionSmallR-ktminShower.nb
# seed = 11
# qcd = QCD with (2-loop) alpha_s^MSbar(lnQ=0) = 1e-09, QCD constants[CF=1.33333, CA=2.66667, TR=0.5, nf=5], CMW scheme returned for alphas(lnkt) calls (included locally), xmuR = 1, xmuR compensation = 1 (if supported in shower), xmuF = 1, set to 0 for lnmuR < -5.01e+08 where alpha_s^MSbar = 2.05701e-09, CA = 2.66667, CF = 1.33333, nf = 5, TR = 0.5, pure_soft_splitting = 0, gluon_uses_q2qg = 0, gluon_uses_qqbar = 1, splitting_wgg = 0, splitting_wqq = 0, mc = 0, mb = 0, colour handling = CATwoCF
# shower = PanScaleGlobal-ee shower with beta = 0, boost/rescaling cutoff = 0, sum of splitting functions with suppression power p = 0, rescaling according to scheme: 1
# shower runner = PanScaleGlobal-ee shower with beta = 0, boost/rescaling cutoff = 0, sum of splitting functions with suppression power p = 0, rescaling according to scheme: 1 with strategy CentralRap, half_central_rap_window = 11
# shower runner strategy = CentralRap, half_central_rap_window = 11
# double soft ME corrections = off
# spin correlations = off
# soft spin corrections = off
# collinear spin projection = off
# rts = 1
# lnvmax = 0
# lnvmin = -5e+08
# max_emsn = -1
# first_order = 0
# second_order = 0
# tree_level = -1
# dynamic_lncutoff = 0
# weighted_generation = 0
# 3-jet matching = off
# Shower scale choices: xhard = 1, xsimkt = 1
# half central rap window = 11
# Process: e+e- -> Z -> qqbar (flavour = +/-1) with sqrt(s)=1.000000, skew_angle=0.000000, skew_eta=n/a, random_generation=0
# time after pre_run initialisation = 2023-12-07 17:53:59 (CET), elapsed = 0 s = 0m = 0 h
#---------------------------------------------------- 
# time now = 2023-12-07 17:54:50 (CET), elapsed = 51 s = 0.85 m = 0.0142 h
# combined nev = 80000, from 8 files (version 2.2.1 (2020-02-09))
#----------------------------------------------------
# xsc_mcatnlo_hard: xsc = 0 +- 0 (n entries = 0)
# xsc_negative: xsc = 0 +- 0 (n entries = 0)
# xsc_positive: xsc = 1 +- 0 (n entries = 10000)
# xsc_total: xsc = 1 +- 0 (n entries = 10000)
# <multiplicity_unweighted> = 206.155625 +- 1.24227625 (n entries = 10000 xsection = 1 nb) range=[8 -- 1051]
# <multiplicity_weighted> = 206.155625 +- 1.24227625 (n entries = 10000 xsection = 1 nb) range=[8 -- 1051]
# <t_evana_ms> = 0.0235825375 +- 0.001336439875 (n entries = 10000 xsection = 1 nb) range=[0.0005 -- 5.71642]
# <t_evgen_ms> = 5.17694625 +- 0.064534225 (n entries = 10000 xsection = 1 nb) range=[0.03125 -- 94.4751]
#----------------------------------------------------
# diff_hist:ln_multiplicity
# ecol = 5
# total_weight = 1
# n_entries = 10000
# mean = 5.14072375
# cols: vlo vmid vhi dhist/dv derr/dv (outflow not normalised)
# under flow bin 0 +- 0
0 0.25 0.5 0 0
0.5 0.75 1 0 0
1 1.25 1.5 2.5e-05 2.499875e-05
1.5 1.75 2 0.0001 4.99975e-05
2 2.25 2.5 0.001325 0.00018193353926752
2.5 2.75 3 0.00595 0.000385087983175233
3 3.25 3.5 0.021 0.000720712890135741
3.5 3.75 4 0.0739 0.00133385520693426
4 4.25 4.5 0.218125 0.00220408153814584
4.5 4.75 5 0.440125 0.00292931439763028
5 5.25 5.5 0.62095 0.00327149562560507
5.5 5.75 6 0.471375 0.00300103565056533
6 6.25 6.5 0.136975 0.00178587446430882
6.5 6.75 7 0.01 0.000498730815158902
7 7.25 7.5 0.00015 6.12320786781733e-05
7.5 7.75 8 0 0
8 8.25 8.5 0 0
8.5 8.75 9 0 0
9 9.25 9.5 0 0
9.5 9.75 10 0 0
10 10.25 10.5 0 0
10.5 10.75 11 0 0
11 11.25 11.5 0 0
11.5 11.75 12 0 0
12 12.25 12.5 0 0
12.5 12.75 13 0 0
13 13.25 13.5 0 0
13.5 13.75 14 0 0
14 14.25 14.5 0 0
14.5 14.75 15 0 0
15 15.25 15.5 0 0
15.5 15.75 16 0 0
16 16.25 16.5 0 0
16.5 16.75 17 0 0
17 17.25 17.5 0 0
17.5 17.75 18 0 0
18 18.25 18.5 0 0
18.5 18.75 19 0 0
19 19.25 19.5 0 0
19.5 19.75 20 0 0
# over flow bin 0 +- 0


# diff_hist:slice_scalar_pt
# ecol = 5
# total_weight = 1
# n_entries = 10000
# mean = -4.189075e+298
# cols: vlo vmid vhi dhist/dv derr/dv (outflow not normalised)
# under flow bin 0.0824004664731942 +- 0.0042272825
-0.5 -0.495 -0.49 1.19125 0.0383575783481488
-0.49 -0.485 -0.48 1.24125 0.0391403899156613
-0.48 -0.475 -0.47 1.205 0.0385744429804242
-0.47 -0.465 -0.46 1.23875 0.0391044280856561
-0.46 -0.455 -0.45 1.27 0.0395880303352936
-0.45 -0.445 -0.44 1.26125 0.0394523075541058
-0.44 -0.435 -0.43 1.31875 0.0403306496226428
-0.43 -0.425 -0.42 1.32875 0.0404818529411251
-0.42 -0.415 -0.41 1.40125 0.0415563296248673
-0.41 -0.405 -0.4 1.35375 0.0408537926326155
-0.4 -0.395 -0.39 1.32875 0.0404810337210526
-0.39 -0.385 -0.38 1.40625 0.0416276990731532
-0.38 -0.375 -0.37 1.38875 0.0413738371400031
-0.37 -0.365 -0.36 1.45375 0.042315920718079
-0.36 -0.355 -0.35 1.4125 0.0417205383895705
-0.35 -0.345 -0.34 1.39 0.041390979854651
-0.34 -0.335 -0.33 1.52875 0.0433782126533586
-0.33 -0.325 -0.32 1.48625 0.0427797501753166
-0.32 -0.315 -0.31 1.61 0.0444977567815924
-0.31 -0.305 -0.3 1.5625 0.0438430961743836
-0.3 -0.295 -0.29 1.51375 0.0431672846424958
-0.29 -0.285 -0.28 1.63 0.0447640724741254
-0.28 -0.275 -0.27 1.5375 0.0434970066865238
-0.27 -0.265 -0.26 1.505 0.0430448686946249
-0.26 -0.255 -0.25 1.64625 0.0449875244908241
-0.25 -0.245 -0.24 1.5125 0.0431468283768851
-0.24 -0.235 -0.23 1.60125 0.044376199741176
-0.23 -0.225 -0.22 1.62875 0.0447515067555342
-0.22 -0.215 -0.21 1.58625 0.0441709002380951
-0.21 -0.205 -0.2 1.56875 0.0439322703223027
-0.2 -0.195 -0.19 1.64 0.0449036185236293
-0.19 -0.185 -0.18 1.6425 0.0449334990708491
-0.18 -0.175 -0.17 1.6525 0.0450695184404396
-0.17 -0.165 -0.16 1.62 0.044632019446
-0.16 -0.155 -0.15 1.66875 0.0452856388647728
-0.15 -0.145 -0.14 1.6525 0.0450698922483874
-0.14 -0.135 -0.13 1.64 0.0449018212038429
-0.13 -0.125 -0.12 1.5875 0.0441880527972848
-0.12 -0.115 -0.11 1.68625 0.0455188589558287
-0.11 -0.105 -0.1 1.6475 0.0450046348493339
-0.1 -0.095 -0.09 1.7325 0.046129066045885
-0.09 -0.085 -0.08 1.72625 0.0460484856708462
-0.08 -0.075 -0.07 1.6575 0.0451368653929357
-0.07 -0.065 -0.06 1.76875 0.0466014902482675
-0.06 -0.055 -0.05 1.725 0.0460309546809943
-0.05 -0.045 -0.04 1.64 0.0449029227586092
-0.04 -0.035 -0.03 1.76375 0.0465367814501
-0.03 -0.025 -0.02 1.7 0.0457027728251388
-0.02 -0.015 -0.01 1.75625 0.0464377394462198
-0.01 -0.005 0 1.6825 0.0454709824780115
# over flow bin 0 +- 0


# cumul_hist:slice_scalar_pt
# ecol = 3
# total_weight = 1
# n_entries = 10000
# mean = -4.189075e+298
# cols: v hist_integral_up_to_v err
-0.5 0.233025 0.00149459972126528
-0.49 0.2449375 0.00152039015571251
-0.48 0.25735 0.00154559383083564
-0.47 0.2694 0.00156848349392365
-0.46 0.2817875 0.00159048998448984
-0.45 0.2944875 0.00161151131121777
-0.44 0.3071 0.00163088157283109
-0.43 0.3202875 0.00164959528746495
-0.42 0.333575 0.00166692124763224
-0.41 0.3475875 0.00168359328696235
-0.4 0.361125 0.00169819356580566
-0.39 0.3744125 0.00171107843328776
-0.38 0.388475 0.00172321355407609
-0.37 0.4023625 0.00173371566476771
-0.36 0.4169 0.00174316280122601
-0.35 0.431025 0.00175084626587848
-0.34 0.444925 0.00175699525015543
-0.33 0.4602125 0.00176213984324147
-0.32 0.475075 0.00176554757234565
-0.31 0.491175 0.0017674646729691
-0.3 0.5068 0.00176757692334467
-0.29 0.5219375 0.00176604783086681
-0.28 0.5382375 0.00176255665739608
-0.27 0.5536125 0.00175754954430167
-0.26 0.5686625 0.00175099093842646
-0.25 0.585125 0.00174193186980361
-0.24 0.60025 0.00173184477937229
-0.23 0.6162625 0.00171928199102625
-0.22 0.63255 0.00170448553137697
-0.21 0.6484125 0.00168805982905903
-0.2 0.6641 0.00166980086173144
-0.19 0.6805 0.00164851098846239
-0.18 0.696925 0.00162483051557693
-0.17 0.71345 0.00159854373850272
-0.16 0.72965 0.0015702192198842
-0.15 0.7463375 0.00153829149281608
-0.14 0.7628625 0.00150372545156552
-0.13 0.7792625 0.00146628428245728
-0.12 0.7951375 0.00142688910883082
-0.11 0.812 0.00138133100578131
-0.1 0.828475 0.00133273773895364
-0.09 0.8458 0.00127678310302078
-0.08 0.8630625 0.00121540803345458
-0.07 0.8796375 0.00115038327296343
-0.06 0.897325 0.00107313902769079
-0.05 0.914575 0.000988194527635343
-0.04 0.930975 0.000896200890857311
-0.03 0.9486125 0.000780569548469858
-0.02 0.9656125 0.000644228333905185
-0.01 0.983175 0.000454709824780115
0 1 6.50781475631414e-11
# with_overflow 0.353553390593274 +- 1.541885e-10


