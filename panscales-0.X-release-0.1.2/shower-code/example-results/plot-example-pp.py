#!/usr/bin/env python3

import numpy as np
from scipy.integrate import quad
import matplotlib
import matplotlib as mpl
mpl.use('Agg') # avoids requirement to have DISPLAY set
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches as patches
import math
from scipy import special
from matplotlib import gridspec
import os.path
from os import path
import os
import sys
from scipy import stats
from scipy import integrate
sys.path.insert(0, os.path.dirname(__file__) + "/../../submodules/AnalysisTools/python")
from hfile import *
#from poly_fit import get_polynomial_coefficients

font = {'color':  'black',
    'weight': 'normal',
    'size': 18
    }

#plt.rcParams['text.usetex'] = True
#plt.rcParams['font.family'] = 'serif'
#plt.rcParams['font.serif'] = ['Helvetica']
plt.rcParams['xtick.labelsize'] = 14
plt.rcParams['ytick.labelsize'] = 14
plt.rcParams['legend.fontsize'] = 14
plt.rcParams['legend.handlelength'] = 1

# Extract the shower values
def get_numerics(fname, dist):
    data = get_array(fname,'diff_hist:'+dist)
    xval = data[:,1]
    avg = data[:,3]
    err = data[:,4]
    return xval, avg, err

# File name
f = 'example-pp.dat'
# Observables
observables = ['boson.pt','jet.pt']
# Plotting details
xlabels = [r'$p_{t,Z}$ [GeV]', r'$p_{t,j}$ [GeV]']
ylabels = [r'$1/N~dN/dp_{t,Z}$ [GeV$^{-1}$]', r'$1/N~dN/dp_{t,j}$ [GeV$^{-1}$]']
# Image name
output = ['boson-pt', 'jet-pt']
#----------------------------------------------------------------
for obs, xlab, ylab, out in zip(observables, xlabels, ylabels, output):
    fig = plt.figure(figsize=(5,3.8))
    ax = plt.gca()
    plt.grid(True, lw=0.5, ls=':', zorder=0,color='silver')
    plt.tick_params(axis='both', which='both', direction='in', bottom=True, top=False, left=True, right=True )    

    plt.plot(get_numerics(f,obs)[0], get_numerics(f,obs)[1],color='dodgerblue')
    plt.fill_between(get_numerics(f,obs)[0], get_numerics(f,obs)[1]+get_numerics(f,obs)[2], get_numerics(f,obs)[1]-get_numerics(f,obs)[2], color='dodgerblue',alpha=0.2,edgecolor='none')
    plt.xlabel(xlab,fontsize=16)
    plt.ylabel(ylab,fontsize=16)
    plt.minorticks_on()
    plt.xlim(0,30)
    plt.text(1.04,0.12, r'PanGlobal$(\beta_{ps}=0)$, $\sqrt{s} = 13.6$ TeV, $y_Z = 0.5$, $\alpha_s(M_Z)=0.118$', transform=ax.transAxes, ha='left', fontsize=7, rotation=270, color='grey')
    plt.text(1.01,0.78, r'anti-$k_t$ $R=0.4$', transform=ax.transAxes, ha='left', fontsize=7, rotation=270, color='grey')
    
    plt.savefig(out+'-pp.png',bbox_inches='tight',dpi=300)
    print(f"Plotting {out} in {out}-pp.png")
    
    plt.close()
