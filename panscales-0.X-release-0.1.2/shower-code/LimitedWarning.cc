//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
/////////////////////////////////////////////
// Implements a warning system for PanScales
// Inspired by the FastJet LimitedWarning class
/////////////////////////////////////////////

#include "LimitedWarning.hh"
#include <sstream>
#include <limits>


namespace panscales{
    std::ostream * LimitedWarning::_default_ostr = &std::cerr;
    int LimitedWarning::_max_warn_default = 5;

    std::list< LimitedWarning::Summary > LimitedWarning::_global_warnings_summary;

    /// the number of times so far that a warning has been registered
    /// with this instance of the class.
    int LimitedWarning::n_warn_so_far() const{
        // explicitly cast to the pointer type (useless wo thread-safety
        // features but works around an issue with the intel compiler
        // (v13.1.3) with thread-safety features
        if (((LimitedWarning::Summary *)_this_warning_summary) == 0) return 0;
        return (*_this_warning_summary).second;
    }


    void LimitedWarning::warn(const char * warning, std::ostream * ostr) {
        // update the summary
        if (((LimitedWarning::Summary *)_this_warning_summary) == 0){

            // prepare the information for the summary
            _global_warnings_summary.push_back(Summary(warning, 0));
            _this_warning_summary = & (_global_warnings_summary.back());
        }


        // maintain the count, but do not allow overflow
        unsigned int count = (*_this_warning_summary).second;
        // add one, as one more warning has been triggered
        (*_this_warning_summary).second += 1;
        
        // print the warning if we have not done it enough already
        if ((_max_warn<0) || (count < (unsigned int)_max_warn)) {
            // prepare the warning within a string stream
            std::ostringstream warnstr;
            warnstr << "WARNING from PanScales: ";
            warnstr << warning;
            if ((_max_warn>0) && (count+1 == (unsigned int)_max_warn))
            warnstr << " (LAST SUCH WARNING)";
            warnstr << std::endl;
            // arrange for the whole warning to be output in one go (that way
            // user can easily insert their own printout, e.g. event number
            // before the warning string).
            if (ostr) {
                (*ostr) << warnstr.str();
                ostr->flush(); // get something written to file even if the program aborts
            }
        }

    }

    //----------------------------------------------------------------------
    std::string LimitedWarning::summary() {
        std::ostringstream str;
        for (std::list<Summary>::const_iterator it = _global_warnings_summary.begin();
            it != _global_warnings_summary.end(); it++) {
            str << it->second << " times: " << it->first << std::endl;
        }
        return str.str();
    }
}