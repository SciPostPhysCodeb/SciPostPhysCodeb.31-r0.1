//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "SpinCorrelations.hh"
#include "LimitedWarning.hh"

namespace panscales{

LimitedWarning Spls_Smin_neg_dotprod_warning;

/// Compute spinor products from momenta
/// The spinor products computed here are normalized such that |S| = 1, 
/// i.e. they are only a phase
void get_Spls_Smin(const Momentum &pi, const Momentum &pj, int ref_direction,
    PSCOMPLEX &Spls, PSCOMPLEX &Smin) {
  
  PSCOMPLEX i(0,1);

  auto di = pi.direction();
  auto dj = pj.direction();

  // Ref is x direction
  if (ref_direction == 1) {
    precision_type diPls = di.px() >= 0 ? 1. + di.px() : (di.py()*di.py() + di.pz()*di.pz())/(1. - di.px());
    precision_type djPls = dj.px() >= 0 ? 1. + dj.px() : (dj.py()*dj.py() + dj.pz()*dj.pz())/(1. - dj.px());
    Spls = ( (di.py() + i*di.pz())*djPls - (dj.py() + i*dj.pz())*diPls );
  }
  // Ref is y direction
  else if (ref_direction == 2) {
    precision_type diPls = di.py() >= 0 ? 1. + di.py() : (di.pz()*di.pz() + di.px()*di.px())/(1. - di.py());
    precision_type djPls = dj.py() >= 0 ? 1. + dj.py() : (dj.pz()*dj.pz() + dj.px()*dj.px())/(1. - dj.py());
    Spls = ( (di.pz() + i*di.px())*djPls - (dj.pz() + i*dj.px())*diPls );
  }
  // Ref is z direction
  else if (ref_direction == 3) {
    precision_type diPls = di.pz() >= 0 ? 1. + di.pz() : (di.px()*di.px() + di.py()*di.py())/(1. - di.pz());
    precision_type djPls = dj.pz() >= 0 ? 1. + dj.pz() : (dj.px()*dj.px() + dj.py()*dj.py())/(1. - dj.pz());
    Spls = ( (di.px() + i*di.py())*djPls - (dj.px() + i*dj.py())*diPls );
  }
  else {
    throw std::runtime_error("Spin Correlations: Unknown reference direction.");
  }

  precision_type Spls_abs = std::abs(Spls);
  Spls = Spls_abs == 0. ? PSCOMPLEX(0.,0.) : Spls/Spls_abs;

  Smin = -std::conj(Spls); 
}

/// Compute spinor products from momenta, this time including the full norm
void get_Spls_Smin_with_norm(const Momentum &pi, const Momentum &pj, int ref_direction,
    PSCOMPLEX &Spls, PSCOMPLEX &Smin) {
  
  get_Spls_Smin(pi, pj, ref_direction, Spls, Smin);
  precision_type dotprod = dot_product(pi, pj);
  if (dotprod < 0) {
    Spls_Smin_neg_dotprod_warning.warn("get_Spls_Smin_with_norm: negative dot product = "
                                         + to_string(to_double(dotprod)) + " (setting to zero).");
    dotprod = 0.0;
  }
  precision_type norm = sqrt(dotprod);
  Spls *= norm;
  Smin *= norm;
}

/// Compute normalized spinor product for collinear projections.
/// NOTE: These are not proper spinor products. Rather, they are normalized 
/// such that the second argument does not have to be lightlike.
void get_Spls_Smin_collinear_projection(const Momentum &p, const Momentum &n, int ref_direction,
    PSCOMPLEX &Spls, PSCOMPLEX &Smin) {

    PSCOMPLEX i(0,1);
    if (ref_direction == 1) {
      precision_type pE_plus_px
        = p.px() < 0 ? ( p.py()*p.py() + p.pz()*p.pz() ) / (p.E() - p.px() ) 
                     : p.E() + p.px();
      Spls = (n.E() + n.px())*(p.py() + i*p.pz()) - (n.py() + i*n.pz())*pE_plus_px;
    }
    else if (ref_direction == 2) {
      precision_type pE_plus_py
        = p.py() < 0 ? ( p.px()*p.px() + p.pz()*p.pz() ) / (p.E() - p.py() ) 
                     : p.E() + p.py();
      Spls = (n.E() + n.py())*(p.pz() + i*p.px()) - (n.pz() + i*n.px())*pE_plus_py;
    }
    else if (ref_direction == 3) {
      precision_type pE_plus_pz
        = p.pz() < 0 ? ( p.px()*p.px() + p.py()*p.py() ) / (p.E() - p.pz() ) 
                     : p.E() + p.pz();
      Spls = (n.E() + n.pz())*(p.px() + i*p.py()) - (n.px() + i*n.py())*pE_plus_pz;
    }
    else {
      throw std::runtime_error("Spin Correlations: Unknown reference direction.");
    }

    // Normalise spinor products to 1, for consistency with analogous get_Spls_Smin.
    precision_type Spls_abs = std::abs(Spls);

    // If everything is working as expected the abs of the spinor product should
    // relate to the dot product of the momentum and the reference four-vector
    // s.t. the following couts are always returning 1. (See sect 5.1 of logbook 
    // 2022-08-10-large-angle-double-soft-spin.) The following is left commented 
    // out should we ever need quick reassurance. N.B. the latter equality also
    // implies that std::abs above should never give zero, as a basic premise
    // (demand) of the spinor product formalism is that the momentum in the spinor
    // never lines up with the k0 reference vector. I.e. if this output is inf we
    // are using the formalism in a place where it doesn't work.
    // if (ref_direction == 1) {
    //   std::cout << " (Spls_abs*sqrt(-1/dot_product(n,n)))/dot_product(p,Momentum(-1,0,0,1)) = " << (Spls_abs*sqrt(-1/dot_product(n,n)))/dot_product(p,Momentum(-1,0,0,1)) << "\n";
    // } else if (ref_direction == 2) {
    //   std::cout << " (Spls_abs*sqrt(-1/dot_product(n,n)))/dot_product(p,Momentum(0,-1,0,1)) = " << (Spls_abs*sqrt(-1/dot_product(n,n)))/dot_product(p,Momentum(0,-1,0,1)) << "\n";
    // } else if (ref_direction == 3) {
    //   std::cout << " (Spls_abs*sqrt(-1/dot_product(n,n)))/dot_product(p,Momentum(0,0,-1,1)) = " << (Spls_abs*sqrt(-1/dot_product(n,n)))/dot_product(p,Momentum(0,0,-1,1)) << "\n";
    // } 

    if(Spls_abs==0) {
      throw std::runtime_error("Spin Correlations: get_Spls_Smin_collinear_projection computed spinor product with zero norm.");
    }

    // Overall factor of -1, coming from the fact that the PanLocal, PanLocalVincia/Antenna,
    // PanGlobal all implement their kinematic maps, in do_kinematics, with a swap
    // of k_perp -> -k_perp relative to the Feb 2020 PRL maps used to derive the
    // relations above.
    Spls *= -1.;

    Spls /= Spls_abs;

    Smin = -std::conj(Spls); 
}

/// Compute spinor products from momenta using directional differences
void get_Spls_Smin_dirdiff(const Momentum &pi, const Momentum &pj, 
    Momentum3<precision_type> &dirdiff_i_minus_j, int ref_direction, 
    PSCOMPLEX &Spls, PSCOMPLEX &Smin) {

  PSCOMPLEX i(0,1);

  // Get directions
  auto di = pi.direction();
  auto dj = pj.direction();
  
  // Ref is x direction
  if (ref_direction == 1) {
    precision_type diPls = di.px() > 0 ? 1. + di.px() : (di.py()*di.py() + di.pz()*di.pz())/(1. - di.px());
    precision_type djPls = dj.px() > 0 ? 1. + dj.px() : (dj.py()*dj.py() + dj.pz()*dj.pz())/(1. - dj.px());

    // Pick Spls expression according to the smallest of diPls and djPls
    if (diPls < djPls) {
      Spls = ( (dirdiff_i_minus_j.py() + i*dirdiff_i_minus_j.pz())*diPls - 
               (di.py() + i*di.pz())*dirdiff_i_minus_j.px() );
    }
    else {
      Spls = ( (dirdiff_i_minus_j.py() + i*dirdiff_i_minus_j.pz())*djPls - 
               (dj.py() + i*dj.pz())*dirdiff_i_minus_j.px() );
    }
  }
  // Ref is y direction
  else if (ref_direction == 2) {
    precision_type diPls = di.py() > 0 ? 1. + di.py() : (di.pz()*di.pz() + di.px()*di.px())/(1. - di.py());
    precision_type djPls = dj.py() > 0 ? 1. + dj.py() : (dj.pz()*dj.pz() + dj.px()*dj.px())/(1. - dj.py());

    // Pick Spls expression according to the smallest of diPls and djPls
    if (diPls < djPls) {
      Spls = ( (dirdiff_i_minus_j.pz() + i*dirdiff_i_minus_j.px())*diPls - 
               (di.pz() + i*di.px())*dirdiff_i_minus_j.py() );
    }
    else {
      Spls = ( (dirdiff_i_minus_j.pz() + i*dirdiff_i_minus_j.px())*djPls - 
               (dj.pz() + i*dj.px())*dirdiff_i_minus_j.py() );
    }
  }
  // Ref is z direction
  else if (ref_direction == 3) {
    precision_type diPls = di.pz() > 0 ? 1. + di.pz() : (di.px()*di.px() + di.py()*di.py())/(1. - di.pz());
    precision_type djPls = dj.pz() > 0 ? 1. + dj.pz() : (dj.px()*dj.px() + dj.py()*dj.py())/(1. - dj.pz());

    // Pick Spls expression according to the smallest of diPls and djPls
    if (diPls < djPls) {
      Spls = ( (dirdiff_i_minus_j.px() + i*dirdiff_i_minus_j.py())*diPls - 
               (di.px() + i*di.py())*dirdiff_i_minus_j.pz() );
    }
    else {
      Spls = ( (dirdiff_i_minus_j.px() + i*dirdiff_i_minus_j.py())*djPls - 
               (dj.px() + i*dj.py())*dirdiff_i_minus_j.pz() );
    }
  }
  else {
    throw std::runtime_error("Spin Correlations: Unknown reference direction.");
  }

  precision_type Spls_abs = std::abs(Spls);
  Spls = Spls_abs == 0. ? PSCOMPLEX(0.,0.) : Spls/Spls_abs;

  Smin = -std::conj(Spls);
}

/// Get splitting amplitude from element
Tensor<3> get_splitting_amplitude(ShowerBase::Element &element, 
    ShowerBase::EmissionInfo &emission_info, int spinor_product_reference_direction, bool do_soft_spin, bool collinear_projection) {

  // The collinear limit of the splitting amplitude has unit soft spin correction
  assert(!(do_soft_spin && collinear_projection));

  // Get kinematic information
  precision_type z = emission_info.z_radiation_wrt_splitter();
  precision_type omz = 1. - z;

  // We denote the emitter by pi, the spectator by pj and the radiation by pk
  Momentum pi = emission_info.do_split_emitter ?
    emission_info.emitter_out :
    emission_info.spectator_out;
  Momentum pj = emission_info.do_split_emitter ?
    emission_info.spectator_out :
    emission_info.emitter_out;
  Momentum pk = emission_info.radiation;

  // Compute normalized spinor products
  PSCOMPLEX Smin_ij, Spls_ij;
  PSCOMPLEX Smin_ik, Spls_ik;
  PSCOMPLEX Smin_kj, Spls_kj;

  // The collinear projection spinor product function, so far, is numerically unstable
  // for VERY small angles. However, _the fractional_difference_ of the real and imaginary
  // components in the unit-normalized spinor products returned by the latter vs those
  // obtained from the regular spinor product code, is of the same order as the angle
  // between the splitter and radiation, as the small angle limit is approached. So we
  // have chosen to put a condition in here s.t. we only use the collinear projection code
  // for 1-cos(opening angle) > epsilon^2. I.e. the fractional difference mentioned above,
  // between the real and imaginary spinor product components obtained with the code below,
  // w.r.t that which one would ideally like to have, i.e. a get_Spls_Smin_collinear_projection
  // function that was numerically stable to arbitrarily small angles, is not more than
  // O(epsilon). This should be more than good enough for everything we want to do, unless
  // we want to start probing differences in \phi angles at the level of ~O(epsilon).
  if (collinear_projection &&
      one_minus_costheta(pi,pk)>pow2(numeric_limit_extras<precision_type>::epsilon())) {

    Momentum p_splitter = emission_info.splitter()/emission_info.splitter().E();
    Momentum & k_perp   = emission_info.k_perp;
    get_Spls_Smin_collinear_projection(p_splitter, k_perp, spinor_product_reference_direction, Spls_ik, Smin_ik);
    // With dir-diffs on the debug output below can be used to explore the comment above.
    // if(element.use_diffs()) {
    //   precision_type angle = sqrt(2*one_minus_costheta(pi,pk));
    //   if(angle<1e-4) {
    //     auto dirdiff_i_min_k = emission_info.do_split_emitter ?
    //       emission_info.d_emitter_out   - emission_info.d_radiation_wrt_emitter():
    //       emission_info.d_spectator_out - emission_info.d_radiation_wrt_spectator();
    //     PSCOMPLEX dbg_Spls_ik,dbg_Smin_ik,ratio_minus_1;
    //     get_Spls_Smin_dirdiff(pi, pk, dirdiff_i_min_k, spinor_product_reference_direction, dbg_Spls_ik, dbg_Smin_ik);
    //     ratio_minus_1 = (dbg_Spls_ik/Spls_ik - PSCOMPLEX(1,0));
    //     if(std::abs(ratio_minus_1.real())>angle || std::abs(ratio_minus_1.real())>angle) {
    //       std::cout << "\n\n";
    //       std::cout << "\t sqrt(2(1-cos(theta_ik))) = " << angle << "\n";
    //       std::cout << "\t Coll Proj Spls_ik        = " << Spls_ik << "\n";
    //       std::cout << "\t [Dir Diff/Coll Proj] - 1 = " << ratio_minus_1 << "\n";
    //     }
    //   }
    // }
  }
  else {
    if (element.use_diffs()) {
      auto dirdiff_i_min_j = emission_info.do_split_emitter ?
        emission_info.d_emitter_out   - emission_info.d_spectator_out + emission_info.cached_dirdiff_em_minus_sp():
        emission_info.d_spectator_out - emission_info.d_emitter_out - emission_info.cached_dirdiff_em_minus_sp();
      auto dirdiff_i_min_k = emission_info.do_split_emitter ?
        emission_info.d_emitter_out   - emission_info.d_radiation_wrt_emitter():
        emission_info.d_spectator_out - emission_info.d_radiation_wrt_spectator();
      auto dirdiff_k_min_j = emission_info.do_split_emitter ?
        emission_info.d_radiation_wrt_spectator() - emission_info.d_spectator_out:
        emission_info.d_radiation_wrt_emitter()   - emission_info.d_emitter_out;

      get_Spls_Smin_dirdiff(pi, pk, dirdiff_i_min_k, spinor_product_reference_direction, Spls_ik, Smin_ik);
      get_Spls_Smin_dirdiff(pi, pj, dirdiff_i_min_j, spinor_product_reference_direction, Spls_ij, Smin_ij);
      get_Spls_Smin_dirdiff(pk, pj, dirdiff_k_min_j, spinor_product_reference_direction, Spls_kj, Smin_kj);
    }
    else {
      get_Spls_Smin(pi, pk, spinor_product_reference_direction, Spls_ik, Smin_ik);
      get_Spls_Smin(pi, pj, spinor_product_reference_direction, Spls_ij, Smin_ij);
      get_Spls_Smin(pk, pj, spinor_product_reference_direction, Spls_kj, Smin_kj);
    }
  }

  // Figure out the channel
  Channel chan;
  // Initial state cases
  if (emission_info.splitter().is_initial_state()) {
    if (emission_info.splitter().pdgid() == 21) {
      if (emission_info.radiation_pdgid == 21)  chan = gItogIgF;
      else                                      chan = gItoqIqF;
    }
    else {
      if (emission_info.radiation_pdgid == 21)  chan = qItoqIgF;
      else                                      chan = qItogIqF;
    }
  }
  // Final state cases
  else {
    if (emission_info.splitter().pdgid() == 21) {
      if (emission_info.radiation_pdgid == 21)  chan = gFtogFgF;
      else                                      chan = gFtoqFqF;
    }
    else                                        chan = qFtoqFgF;
  }

  Tensor<3> out(0);

  // --------------------------------- qI backwards evolves to gI qF ---------------------------------
  if (chan == qItogIqF) {
    out(0,0,1) =  Smin_ik*omz;
    out(0,1,1) = -Spls_ik*z;
    out(1,1,0) =  Spls_ik*omz;
    out(1,0,0) = -Smin_ik*z;
  }

  // --------------------------------- gI backwards evolves to qI qF ---------------------------------
  if (chan == gItoqIqF) {
    out(0,0,0) = Spls_ik;
    out(0,1,1) = Spls_ik*z;
    out(1,1,1) = Smin_ik;
    out(1,0,0) = Smin_ik*z;
  }

  // --------------------------------- gF to qF qF ---------------------------------
  if (chan == gFtoqFqF) {
    out(0,0,1) = -Smin_ik*omz;
    out(0,1,0) =  Smin_ik*z;
    out(1,1,0) = -Spls_ik*omz;
    out(1,0,1) =  Spls_ik*z;
  }

  // --------------------------------- gI backwards evolves to gI gF ---------------------------------
  if (chan == gItogIgF) {
    out(0,0,0) = Spls_ik;
    out(0,0,1) = Smin_ik*omz*omz;
    out(0,1,1) = Spls_ik*z*z;
    out(1,0,0) = Smin_ik*z*z;
    out(1,1,0) = Spls_ik*omz*omz;
    out(1,1,1) = Smin_ik;

    if (do_soft_spin) {
      out(0,0,0) *= Smin_ij/Smin_kj;
      out(0,0,1) *= Spls_ij/Spls_kj;
      out(1,1,0) *= Smin_ij/Smin_kj;
      out(1,1,1) *= Spls_ij/Spls_kj;
    }
  }
  //
  // and for g to g gF we need the separate case
  // --------------------------------- q(I/F) to q(I/F) gF ---------------------------------
  if (chan == qFtoqFgF || chan == qItoqIgF) {
    out(0,0,0) = Spls_ik;
    out(0,0,1) = Smin_ik*omz;
    out(1,1,0) = Spls_ik*omz;
    out(1,1,1) = Smin_ik;

    if (do_soft_spin) {
      out(0,0,0) *= Smin_ij/Smin_kj;
      out(0,0,1) *= Spls_ij/Spls_kj;
      out(1,1,0) *= Smin_ij/Smin_kj;
      out(1,1,1) *= Spls_ij/Spls_kj;
    }
  }

  // --------------------------------- gF to gF gF ---------------------------------
  if (chan == gFtogFgF) {
    out(0,0,0) = Spls_ik;
    out(0,0,1) = Smin_ik*omz*omz;
    out(0,1,0) = Smin_ik*z*z;
    out(1,0,1) = Spls_ik*z*z;
    out(1,1,0) = Spls_ik*omz*omz;
    out(1,1,1) = Smin_ik;

    if (do_soft_spin) {
      out(0,0,0) *= Smin_ij/Smin_kj;
      out(0,0,1) *= Spls_ij/Spls_kj;
      out(1,1,0) *= Smin_ij/Smin_kj;
      out(1,1,1) *= Spls_ij/Spls_kj;
    }
  }
  return out;
}


// -------------------------------------------------------------------------
// ----------------------------- Parton Class ------------------------------
// -------------------------------------------------------------------------

/// Recomputes spin density matrix for a parton that has already been decayed
Tensor<2> Parton::recompute_spindensity(Tensor<2> & density_matrix_a) {
  // Get the decay matrix of the partner
  auto density_matrix_c = p_partner_->get_decay_matrix();

  // Contractions
  auto tmp = product_contract_single_index(p_parent_->splitting_amplitude_cached_, density_matrix_a);
  tmp      = product_contract_single_index(tmp, density_matrix_c);
  auto density_matrix_b = product_contract_double_index(tmp, p_parent_->splitting_amplitude_cached_star_);
  density_matrix_b.normalize();

  return density_matrix_b;
}

/// Recompute the decay matrix
void Parton::recompute_decay_matrix() {
  // Get the decay matrix of the children
  auto density_matrix_b = p_daughter_->get_decay_matrix();
  auto density_matrix_c = p_son_->get_decay_matrix();
  
  // Contractions
  auto tmp = product_contract_single_index(splitting_amplitude_cached_, density_matrix_b);
  tmp      = product_contract_single_index(tmp, density_matrix_c);
  decay_matrix_ = product_contract_double_index(tmp, splitting_amplitude_cached_star_);
  decay_matrix_.normalize();

  // Need to reset the indices to the own class members
  decay_matrix_(a1_,a2_);
}

// -------------------------------------------------------------------------
// ------------------------- Spin Correlation Tree -------------------------
// -------------------------------------------------------------------------

// Compute accept probability for a branching
// This returns a pair of Weights:
// - the first  is the genuine spin weight
// - the second is the overhead
// such that "weight<=overhead" for all phis
  pair<Weight,Weight> SpinCorTree::compute_phi_weight_and_overhead(ShowerBase::Element &element, ShowerBase::EmissionInfo &emission_info) {
  // Find out which parton is splitting - do this first because we may need the barcode in the analysis
  barcode_p_current_ = emission_info.do_split_emitter ? element.emitter().barcode() : element.spectator().barcode();
  
  // Just return 1 if we're not running spin correlations
  if (!do_spin_corr_) return make_pair(Weight::exact_one, Weight::exact_one);

  // compute_accept_prob may be called multiple times due to the rejection sampling procedure
  // However, the procedures in this loop only have to be performed once, and are cached afterwards
  if (!cached_info_) {
    // Retrieve parent parton in the internal map
    auto it = parton_map_.find(barcode_p_current_);
    if (it == parton_map_.end()) {
      throw std::runtime_error("Spin Correlations: Tried to split a parton that doesn't exist.");
    }

    // We now trace the binary tree to the matrix element back from the currently branching parton
    // Make sure the parton list is empty
    p_branch_current_.clear();

    // Start from the current branching
    Parton* current_parton = &it->second;

    // Go back in the branch to the hard ME, and store the list of vertices along the way
    do {
      p_branch_current_.push_back(current_parton);

      // Stop when we're at the hard matrix element
      if (current_parton->get_p_parent_ptr() == nullptr) break;

      // Jump down 1 step in the branch
      current_parton = current_parton->get_p_parent_ptr();
    } while (true);

    // Set the elements of p_branch_current_ in the right order
    std::reverse(p_branch_current_.begin(), p_branch_current_.end());

    // We now recompute the spin density matrix matrix along the current branch
    // First recompute the spin density matrix for the parton outgoing from the ME
    switch(hard_ME_size_) {
      case 2:
        spindensity_in_current_= hard_ME_two_.recompute_spindensity(current_parton);
        break;
      case 3:
        spindensity_in_current_= hard_ME_three_.recompute_spindensity(current_parton);
        break;
      case 4:
        spindensity_in_current_= hard_ME_four_.recompute_spindensity(current_parton);
        break;
      case 5:
        spindensity_in_current_ = hard_ME_five_.recompute_spindensity(current_parton);
        break;
      default:
        throw std::runtime_error("Spin Correlations: Incorrect hard ME size.");      
    }

    // Then follow the branching to the emitter, recomputing density matrices along the way
    for (unsigned int idx = 1; idx < p_branch_current_.size(); idx++) {
      current_parton = p_branch_current_[idx];
      spindensity_in_current_ = p_branch_current_[idx]->recompute_spindensity(spindensity_in_current_);
    }
    // At the end of this loop, spindensity_in_current_ is the spin 
    // density matrix of the branching parton

    // Don't do all of this again
    cached_info_ = true;
  }
  
  // Move on to computing the acceptance probability
  Parton* p_now = p_branch_current_.back();

  // Get the splitting function and set indices
  Index b1, b2;
  Tensor<3> splitting_amplitude = 
    get_splitting_amplitude(element, emission_info, spinor_product_reference_direction_, do_soft_spin_)(p_now->a1_, b1, b2);

  // Compute the conjugate branching amplitude
  Tensor<3> splitting_amplitude_star = splitting_amplitude.conj()(p_now->a2_, b1, b2);

  // Store the branching amplitudes in the parton object 
  // NOTE: these do not have the correct indices yet
  p_branch_current_.back()->splitting_amplitude_cached_      = splitting_amplitude;
  p_branch_current_.back()->splitting_amplitude_cached_star_ = splitting_amplitude_star;

  // Contract with the density matrix
  Tensor<2> amp_ampstar;

  // if collinear spin on, ampstar needs to be computed with collinear projections
  if (do_collinear_spin_projection_) {
    Tensor<3> splitting_amplitude_collinear_projection = 
      get_splitting_amplitude(element, emission_info, spinor_product_reference_direction_, false, true)(p_now->a1_, b1, b2);

    Tensor<3> splitting_amplitude_collinear_projection_star = splitting_amplitude_collinear_projection.conj()(p_now->a2_, b1, b2);

    p_branch_current_.back()->splitting_amplitude_collinear_projection_cached_ = splitting_amplitude_collinear_projection;
    p_branch_current_.back()->splitting_amplitude_collinear_projection_cached_star_ = splitting_amplitude_collinear_projection_star;

    amp_ampstar = product_contract_double_index(splitting_amplitude_collinear_projection, splitting_amplitude_collinear_projection_star);  
  } else {
    amp_ampstar = product_contract_double_index(splitting_amplitude, splitting_amplitude_star);
  }

  PSCOMPLEX weight_complex = product_contract_double_index(amp_ampstar, spindensity_in_current_).scalar();
  
  // Cache weight of this branching
  weight_current_ = real(weight_complex);
  // Cache the relative weight (normalized to the uncorrelated weight)
  weight_current_relative_ = 2*weight_current_ / amp_ampstar.tr().real();

  // We get to this overestimate by realising that 
  // - spindensity_in_current_ is hermitian and has 
  //   spindensity_in_current_(0,0) + spindensity_in_current_(1,1) = 1
  // - V2 is hermitian and has V2(0,0) = V2(1,1)
  precision_type weight_over = abs(amp_ampstar(0,0)) + 2*abs(amp_ampstar(0,1))*abs(spindensity_in_current_(0,1));

  return make_pair(to_double(weight_current_), to_double(weight_over));
}

void SpinCorTree::update_tree(ShowerBase::EmissionInfo &emission_info, ShowerBase::Element &element, Event &event) {
  // Just return if we're not running spin correlations
  if (!do_spin_corr_) return;
  
  assert(p_branch_current_.size() > 0);
  // A pointer to the splitting parton
  Parton* p = p_branch_current_.back();

  // Get the two new partons

  // The emitting parton can be either the emitter or the spectator
  int barcode_out_1 = 
    emission_info.do_split_emitter ? element.emitter().barcode() : element.spectator().barcode();
  int pdgid_out_1 = emission_info.do_split_emitter ? element.emitter().pdgid() : element.spectator().pdgid();
  
  // The emitted parton is always at the end of the event
  int barcode_out_2 = event[event.size()-1].barcode();
  int pdgid_out_2 = event[event.size()-1].pdgid();

  // Create two new partons and add them to the list
  parton_map_[barcode_out_1] = Parton(pdgid_out_1, barcode_out_1, p);
  parton_map_[barcode_out_2] = Parton(pdgid_out_2, barcode_out_2, p);

  // compute and store energies/angles if need be for analysis
  if (_store_extra_branching_info) {
    // Set the energies of the new partons
    precision_type E_1 = 
      emission_info.do_split_emitter ? emission_info.emitter_out.E() : emission_info.spectator_out.E();
    parton_map_[barcode_out_1].set_energy(E_1);
    parton_map_[barcode_out_2].set_energy(emission_info.radiation.E());

    // Set the angles of the previous parton
    if (element.use_diffs()) {
      auto dirdiff = emission_info.do_split_emitter ?
        emission_info.d_emitter_out   - emission_info.d_radiation_wrt_emitter():
        emission_info.d_spectator_out - emission_info.d_radiation_wrt_spectator();

      p->set_theta(2*asin(dirdiff.modp()/2));
      p->set_azimuthal_plane(cross(emission_info.radiation.direction(), -dirdiff));
    }
    else {
      p->set_theta(acos(1. - one_minus_costheta(
        emission_info.do_split_emitter ? emission_info.emitter_out : emission_info.spectator_out,
        emission_info.radiation)));
      p->set_azimuthal_plane(cross(
        emission_info.do_split_emitter ? emission_info.emitter_out.direction() : emission_info.spectator_out.direction(),
        emission_info.radiation.direction()));
    }
  }

  Parton* p1 = &parton_map_[barcode_out_1];
  Parton* p2 = &parton_map_[barcode_out_2];

  p1->set_p_partner_ptr(&parton_map_[barcode_out_2]);
  p2->set_p_partner_ptr(&parton_map_[barcode_out_1]);

  p1->init_decay_matrix();
  p2->init_decay_matrix();

  p->set_p_daughter_ptr(&parton_map_[barcode_out_1]);
  p->set_p_son_ptr(&parton_map_[barcode_out_2]);

  p->splitting_amplitude_cached_(p->a1_, p1->a1_, p2->a1_);
  p->splitting_amplitude_cached_star_(p->a2_, p1->a2_, p2->a2_);

  // Follow the constructed branch back to the hard ME and set decay 
  // matrices along the way
  while ( p_branch_current_.size() > 0 ) {
    p_branch_current_.back()->recompute_decay_matrix();
    p_branch_current_.pop_back();
  }

  // Make sure p_branch_current is empty
  assert(p_branch_current_.empty());

  // Switch cached_info_ back to false for the next emission
  cached_info_ = false;
}

void SpinCorTree::do_analysis_step(ShowerBase::EmissionInfo &emission_info, ShowerBase::Element &element, Event &event) {
  // ---------------------------------------- Same side analysis ----------------------------------------
  // Is this a primary emission?
  if (barcode_p_current_ == barcode_original_parton_sameside_1_ || barcode_p_current_ == barcode_original_parton_sameside_2_) {

    // Barcodes of the post-branching partons
    int barcode_emission_1 = emission_info.do_split_emitter ? element.emitter().barcode() : element.spectator().barcode();
    int barcode_emission_2 = event[event.size()-1].barcode();

    // Which leg is the hardest?
    Momentum momentum_emission_1 = emission_info.do_split_emitter ? emission_info.emitter_out : emission_info.spectator_out;
    Momentum momentum_emission_2 = emission_info.radiation;      
    bool hardest_is_1 = momentum_emission_1.modp() > momentum_emission_2.modp();
    precision_type z = hardest_is_1 ? 
      momentum_emission_2.modp() / (momentum_emission_1.modp() + momentum_emission_2.modp()) :
      momentum_emission_1.modp() / (momentum_emission_1.modp() + momentum_emission_2.modp());

    // Do we pass the z cut
    if (z > z_1_cut_sameside_) {
      // Compute the primary plane
      Momentum3<precision_type> primary_plane;
      if (element.use_diffs()) {
        auto dirdiff = emission_info.do_split_emitter ?
          emission_info.d_emitter_out   - emission_info.d_radiation_wrt_emitter():
          emission_info.d_spectator_out - emission_info.d_radiation_wrt_spectator();

        primary_plane = hardest_is_1 ? 
          cross(momentum_emission_1.direction(), -dirdiff) : 
          cross(momentum_emission_2.direction(), dirdiff);
      }
      else {
        primary_plane = hardest_is_1 ? 
          cross(momentum_emission_1.direction(), momentum_emission_2.direction()) : 
          cross(momentum_emission_2.direction(), momentum_emission_1.direction());
      }
      primary_plane /= primary_plane.modp();

      // Record the barcode of the secondary, which is the softest leg
      if (barcode_p_current_ == barcode_original_parton_sameside_1_) {
        barcode_first_passed_emission_sameside_1_ = hardest_is_1 ? barcode_emission_2 : barcode_emission_1;
        found_primary_sameside_1_ = true;
        z_primary_sameside_1_ = to_double(z);
        primary_plane_sameside_1_ = primary_plane;
      }
      else if (barcode_p_current_ == barcode_original_parton_sameside_2_) {
        barcode_first_passed_emission_sameside_2_ = hardest_is_1 ? barcode_emission_2 : barcode_emission_1;
        found_primary_sameside_2_ = true;
        z_primary_sameside_2_ = to_double(z);
        primary_plane_sameside_2_ = primary_plane;
      }
    }

    // If we didn't pass the z cut, we have to update the currently followed parton
    else {
      if (barcode_p_current_ == barcode_original_parton_sameside_1_ ) {
        barcode_original_parton_sameside_1_ = hardest_is_1 ? barcode_emission_1 : barcode_emission_2;
      }
      if (barcode_p_current_ == barcode_original_parton_sameside_2_ ) {
        barcode_original_parton_sameside_2_ = hardest_is_1 ? barcode_emission_1 : barcode_emission_2;
      }
    }
  }

  // Is this a secondary emission?
  if (barcode_p_current_ == barcode_first_passed_emission_sameside_1_ || barcode_p_current_ == barcode_first_passed_emission_sameside_2_) {

    // Barcodes of the post-branching partons
    int barcode_emission_1 = emission_info.do_split_emitter ? element.emitter().barcode() : element.spectator().barcode();
    int barcode_emission_2 = event[event.size()-1].barcode();

    // Which leg is the hardest?
    Momentum momentum_emission_1 = emission_info.do_split_emitter ? emission_info.emitter_out : emission_info.spectator_out;
    Momentum momentum_emission_2 = emission_info.radiation;      
    bool hardest_is_1 = momentum_emission_1.modp() > momentum_emission_2.modp();
    precision_type z = hardest_is_1 ? 
      momentum_emission_2.modp() / (momentum_emission_1.modp() + momentum_emission_2.modp()) :
      momentum_emission_1.modp() / (momentum_emission_1.modp() + momentum_emission_2.modp());

    if (z > z_2_cut_sameside_) {
      Momentum3<precision_type> secondary_plane;
      if (element.use_diffs()) {
        auto dirdiff = emission_info.do_split_emitter ?
          emission_info.d_emitter_out   - emission_info.d_radiation_wrt_emitter():
          emission_info.d_spectator_out - emission_info.d_radiation_wrt_spectator();

        secondary_plane = hardest_is_1 ? 
          cross(momentum_emission_1.direction(), -dirdiff) : 
          cross(momentum_emission_2.direction(), dirdiff);
      }
      else {
        secondary_plane = hardest_is_1 ? 
          cross(momentum_emission_1.direction(), momentum_emission_2.direction()) : 
          cross(momentum_emission_2.direction(), momentum_emission_1.direction());
      }
      secondary_plane /= secondary_plane.modp();

      // Record outgoing pdgids
      int pdg_id_1 = emission_info.do_split_emitter ? element.emitter().pdgid() : element.spectator().pdgid();
      int pdg_id_2 = event[event.size()-1].pdgid();

      if (barcode_p_current_ == barcode_first_passed_emission_sameside_1_) {
        // Compute the angle between planes, making sure to include the correct sign
        precision_type dpsi_abs = acos(dot_product_3(primary_plane_sameside_1_, secondary_plane));
        Momentum3<precision_type> twice_perp = cross(primary_plane_sameside_1_, secondary_plane);
        precision_type sign = dot_product_3(twice_perp, hardest_is_1  ? momentum_emission_1.direction() : momentum_emission_2.direction());
        if (sign > 0) dpsi_sameside_1_ =  to_double(dpsi_abs);
        else          dpsi_sameside_1_ = -to_double(dpsi_abs);

        // Scale to be between -pi and pi 
        if (dpsi_sameside_1_ >  fjcore::pi) {dpsi_sameside_1_ -= 2*to_double(fjcore::pi);}
        if (dpsi_sameside_1_ < -fjcore::pi) {dpsi_sameside_1_ += 2*to_double(fjcore::pi);}

        // Store other stuff
        z_secondary_sameside_1_ = to_double(z);
        found_secondary_sameside_1_ = true;

        // gg or qqbar?
        if (pdg_id_1 == 21 && pdg_id_2 == 21) {is_gg_sameside_1_ = true;}
        else if (abs(pdg_id_1) == abs(pdg_id_2)) {is_qqbar_sameside_1_ = true;}
      }

      else if (barcode_p_current_ == barcode_first_passed_emission_sameside_2_) {
        // Compute the angle between planes, making sure to include the correct sign
        precision_type dpsi_abs = acos(dot_product_3(primary_plane_sameside_2_, secondary_plane));
        Momentum3<precision_type> twice_perp = cross(primary_plane_sameside_2_, secondary_plane);
        precision_type sign = dot_product_3(twice_perp, hardest_is_1  ? momentum_emission_1.direction() : momentum_emission_2.direction());
        if (sign > 0) dpsi_sameside_2_ =  to_double(dpsi_abs);
        else          dpsi_sameside_2_ = -to_double(dpsi_abs);

        // Scale to be between -pi and pi 
        if (dpsi_sameside_2_ >  fjcore::pi) {dpsi_sameside_2_ -= 2*to_double(fjcore::pi);}
        if (dpsi_sameside_2_ < -fjcore::pi) {dpsi_sameside_2_ += 2*to_double(fjcore::pi);}

        // Store other variables
        z_secondary_sameside_2_ = to_double(z);
        found_secondary_sameside_2_ = true;

        // gg or qqbar?
        if (pdg_id_1 == 21 && pdg_id_2 == 21) {is_gg_sameside_2_ = true;}
        else if (abs(pdg_id_1) == abs(pdg_id_2)) {is_qqbar_sameside_2_ = true;}
      }
    }

    else {
      if (barcode_p_current_ == barcode_first_passed_emission_sameside_1_) {
        barcode_first_passed_emission_sameside_1_ = hardest_is_1 ? barcode_emission_1 : barcode_emission_2;
      }
      if (barcode_p_current_ == barcode_first_passed_emission_sameside_2_) {
        barcode_first_passed_emission_sameside_2_ = hardest_is_1 ? barcode_emission_1 : barcode_emission_2;
      }
    }
  }

  // ---------------------------------------- Same side analysis ----------------------------------------
  // Is this a primary emission?
  if ((barcode_p_current_ == barcode_original_parton_opposite_1_ && !found_primary_opposite_1_) || 
      (barcode_p_current_ == barcode_original_parton_opposite_2_ && !found_primary_opposite_2_)) {

    // Barcodes of the post-branching partons
    int barcode_emission_1 = emission_info.do_split_emitter ? element.emitter().barcode() : element.spectator().barcode();
    int barcode_emission_2 = event[event.size()-1].barcode();

    // Which leg is the hardest?
    Momentum momentum_emission_1 = emission_info.do_split_emitter ? emission_info.emitter_out : emission_info.spectator_out;
    Momentum momentum_emission_2 = emission_info.radiation;      
    bool hardest_is_1 = momentum_emission_1.modp() > momentum_emission_2.modp();
    precision_type z = hardest_is_1 ? 
      momentum_emission_2.modp() / (momentum_emission_1.modp() + momentum_emission_2.modp()) :
      momentum_emission_1.modp() / (momentum_emission_1.modp() + momentum_emission_2.modp());

    // Do we pass the z cut
    if (z > z_cut_opposite_) {
      // Compute the primary plane
      Momentum3<precision_type> primary_plane;
      if (element.use_diffs()) {
        auto dirdiff = emission_info.do_split_emitter ?
          emission_info.d_emitter_out   - emission_info.d_radiation_wrt_emitter():
          emission_info.d_spectator_out - emission_info.d_radiation_wrt_spectator();

        primary_plane = hardest_is_1 ? 
          cross(momentum_emission_1.direction(), -dirdiff) : 
          cross(momentum_emission_2.direction(), dirdiff);
      }
      else {
        primary_plane = hardest_is_1 ? 
          cross(momentum_emission_1.direction(), momentum_emission_2.direction()) : 
          cross(momentum_emission_2.direction(), momentum_emission_1.direction());
      }
      primary_plane /= primary_plane.modp();

      // Record outgoing pdgids
      int pdg_id_1 = emission_info.do_split_emitter ? element.emitter().pdgid() : element.spectator().pdgid();
      int pdg_id_2 = event[event.size()-1].pdgid();

      // Store the primary plane
      if (barcode_p_current_ == barcode_original_parton_opposite_1_) {
        found_primary_opposite_1_ = true;
        z_primary_opposite_1_ = to_double(z);
        primary_plane_opposite_1_ = primary_plane;
                  
        // gg or qqbar?
        if (pdg_id_1 == 21 && pdg_id_2 == 21) {is_gg_opposite_1_ = true;}
        else if (abs(pdg_id_1) == abs(pdg_id_2)) {is_qqbar_opposite_1_ = true;}
      }
      else if (barcode_p_current_ == barcode_original_parton_opposite_2_) {
        found_primary_opposite_2_ = true;
        z_primary_opposite_2_ = to_double(z);
        primary_plane_opposite_2_ = primary_plane;

        // gg or qqbar?
        if (pdg_id_1 == 21 && pdg_id_2 == 21) {is_gg_opposite_2_ = true;}
        else if (abs(pdg_id_1) == abs(pdg_id_2)) {is_qqbar_opposite_2_ = true;}
      }

      // If we found both primaries, compute dpsi
      if (found_primary_opposite_1_ && found_primary_opposite_2_) {
        // Compute the angle between planes, making sure to include the correct sign
        precision_type dpsi_abs = acos(dot_product_3(primary_plane_opposite_1_, primary_plane_opposite_2_));
        Momentum3<precision_type> twice_perp = cross(primary_plane_opposite_1_, primary_plane_opposite_2_);
        precision_type sign = dot_product_3(twice_perp, hardest_is_1  ? momentum_emission_1.direction() : momentum_emission_2.direction());
        if (sign > 0) dpsi_opposite_ =  to_double(dpsi_abs);
        else          dpsi_opposite_ = -to_double(dpsi_abs);

        // Scale to be between -pi and pi 
        if (dpsi_opposite_ >  fjcore::pi) {dpsi_opposite_ -= 2*to_double(fjcore::pi);}
        if (dpsi_opposite_ < -fjcore::pi) {dpsi_opposite_ += 2*to_double(fjcore::pi);}
      }
    }

    // If we didn't pass the z cut, we have to update the currently followed parton
    else {
      if (barcode_p_current_ == barcode_original_parton_opposite_1_ ) {
        barcode_original_parton_opposite_1_ = hardest_is_1 ? barcode_emission_1 : barcode_emission_2;
      }
      if (barcode_p_current_ == barcode_original_parton_opposite_2_ ) {
        barcode_original_parton_opposite_2_ = hardest_is_1 ? barcode_emission_1 : barcode_emission_2;
      }
    }
  }
}

// Testing area for matching
void SpinCorTree::test_matching(Event &event) {
  assert(event.size() == 3);

  // Find the particles
  int i_gluon = -1;
  int i_quark = -1;
  int i_antiquark = -1;

  for (int i=0; i<3; i++) {
    if (event[i].pdgid() == 21)     i_gluon = i;
    else if (event[i].pdgid() > 0)  i_quark = i;
    else                            i_antiquark = i;
  }

  Momentum p_gluon = event[i_gluon].momentum();
  Momentum p_quark = event[i_quark].momentum();
  Momentum p_antiquark = event[i_antiquark].momentum();
  Momentum p_electron(0, 0, sqrt(event.Q2())/2, sqrt(event.Q2())/2);
  Momentum p_positron(0, 0,-sqrt(event.Q2())/2, sqrt(event.Q2())/2);

  // Figure out which parton we're collinear with
  precision_type omcostheta_q    = one_minus_costheta(p_quark, p_gluon);
  precision_type omcostheta_qbar = one_minus_costheta(p_antiquark, p_gluon);

  // Only do things if it is the quark
  if (omcostheta_q > omcostheta_qbar) return;

  // Full ME
  // Tensor indices are e- e+ > q qbar g
  Tensor<5> meqqg(0);

  // This allows me to double-use the names of variables
  {
    PSCOMPLEX Smin_q_g, Spls_q_g; // p1,k
    PSCOMPLEX Smin_qbar_g, Spls_qbar_g; // p2,k

    PSCOMPLEX Smin_em_q, Spls_em_q; // q1,p1
    PSCOMPLEX Smin_ep_q, Spls_ep_q; // q2,p1
    PSCOMPLEX Smin_em_qbar, Spls_em_qbar; // q1,p2
    PSCOMPLEX Smin_ep_qbar, Spls_ep_qbar; // q2,p2
    PSCOMPLEX Smin_em_ep, Spls_em_ep; // q1,q2
    
    get_Spls_Smin_with_norm(p_quark, p_gluon,     spinor_product_reference_direction_, Spls_q_g, Smin_q_g);
    get_Spls_Smin_with_norm(p_antiquark, p_gluon, spinor_product_reference_direction_, Spls_qbar_g, Smin_qbar_g);

    get_Spls_Smin_with_norm(p_electron, p_quark, spinor_product_reference_direction_, Spls_em_q, Smin_em_q);
    get_Spls_Smin_with_norm(p_positron, p_quark, spinor_product_reference_direction_, Spls_ep_q, Smin_ep_q);

    get_Spls_Smin_with_norm(p_electron, p_antiquark, spinor_product_reference_direction_, Spls_em_qbar, Smin_em_qbar);
    get_Spls_Smin_with_norm(p_positron, p_antiquark, spinor_product_reference_direction_, Spls_ep_qbar, Smin_ep_qbar);

    get_Spls_Smin_with_norm(p_electron, p_positron, spinor_product_reference_direction_, Spls_em_ep, Smin_em_ep);

    // CAREFUL: 0 = +, 1 = -
    // Manually derived
    meqqg(0,1,0,1,0) =  Smin_em_qbar*Smin_em_qbar*Spls_em_ep/Smin_q_g/Smin_qbar_g;
    meqqg(0,1,0,1,1) =  Spls_ep_q   *Spls_ep_q   *Smin_em_ep/Spls_q_g/Spls_qbar_g;
    meqqg(0,1,1,0,0) = -Smin_em_q   *Smin_em_q   *Spls_em_ep/Smin_q_g/Smin_qbar_g;
    meqqg(0,1,1,0,1) = -Spls_ep_qbar*Spls_ep_qbar*Smin_em_ep/Spls_q_g/Spls_qbar_g;

    meqqg(1,0,1,0,1) = -conj(meqqg(0,1,0,1,0));
    meqqg(1,0,1,0,0) = -conj(meqqg(0,1,0,1,1));
    meqqg(1,0,0,1,1) = -conj(meqqg(0,1,1,0,0));
    meqqg(1,0,0,1,0) = -conj(meqqg(0,1,1,0,1));

  }

  // Born-level ME
  // Tensor indices are e- e+ > q qbar
  Tensor<4> meqq(0);
  {
    // Born-level kinematics
    Momentum p_qg = p_quark + p_gluon;
    PSCOMPLEX Smin_em_qg, Spls_em_qg; // q1,p1
    PSCOMPLEX Smin_ep_qg, Spls_ep_qg; // q2,p1
    PSCOMPLEX Smin_em_qbar, Spls_em_qbar; // q1,p2
    PSCOMPLEX Smin_ep_qbar, Spls_ep_qbar; // q2,p2

    get_Spls_Smin_with_norm(p_electron, p_qg, spinor_product_reference_direction_, Spls_em_qg, Smin_em_qg);
    get_Spls_Smin_with_norm(p_positron, p_qg, spinor_product_reference_direction_, Spls_ep_qg, Smin_ep_qg);

    get_Spls_Smin_with_norm(p_electron, p_antiquark, spinor_product_reference_direction_, Spls_em_qbar, Smin_em_qbar);
    get_Spls_Smin_with_norm(p_positron, p_antiquark, spinor_product_reference_direction_, Spls_ep_qbar, Smin_ep_qbar);

    meqq(0,1,0,1) = Spls_ep_qg*Smin_em_qbar;
    meqq(0,1,1,0) = Spls_ep_qbar*Smin_em_qg;
    meqq(1,0,1,0) = conj(meqq(0,1,0,1));
    meqq(1,0,0,1) = conj(meqq(0,1,1,0));
  }

  // Branching amplitude
  Tensor<3> branch_amp;
  {
    PSCOMPLEX Smin_q_g, Spls_q_g; // p1,k
    get_Spls_Smin_with_norm(p_quark, p_gluon, spinor_product_reference_direction_, Spls_q_g, Smin_q_g);

    precision_type z   = p_quark.modp() / (p_quark.modp() + p_gluon.modp());
    precision_type omz = p_gluon.modp() / (p_quark.modp() + p_gluon.modp());
    precision_type prop = dot_product(p_quark, p_gluon);

    branch_amp(0,0,0) = Spls_q_g/sqrt(omz)/prop; 
    branch_amp(0,0,1) = Smin_q_g*z/sqrt(omz)/prop; 
    branch_amp(1,1,0) = Spls_q_g*z/sqrt(omz)/prop; 
    branch_amp(1,1,1) = Smin_q_g/sqrt(omz)/prop; 
  }

  // Note that the order of spinor indices is not natural in the product
  Index a, b, c, d, e, x;
  meqq(a,b,x,c);
  branch_amp(x,d,e);

}


}
