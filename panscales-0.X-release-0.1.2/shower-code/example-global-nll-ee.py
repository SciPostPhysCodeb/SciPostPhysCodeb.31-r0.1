#!/usr/bin/env python3
"""
This script shows how to use example-global-nll-ee.cc in order to make an
NLL-accuracy test for global event shapes.

- it compiles the code with the double or doubleexp numerical type
- it runs the shower for a few values of alphas. 
- it then generates a pdf file with plots showing the results for a
  few event shapes at those different values of alphas and the
  extrapolation to alphas=0.

Run times are
- 2100 core seconds on Intel(R) Xeon(R) CPU E5-2643 v4 @ 3.40GHz 
  (10 minutes with --njobs 4)
- 800 core seconds on an Mac M2Pro (2 minutes with --njobs 10)

Here we require a beta==0 shower. 

Usage: ./example-global-nll-ee.py [-h] [--njobs NJOBS] [--shower SHOWER]
options:
  -h, --help       show this help message and exit
  --njobs NJOBS    number of jobs to run in parallel (default: all available)
  --shower SHOWER  beta==0 shower to use, (default panglobal-beta0.0, one alternative is dipole-kt)
  --only-plots     assuming you have already done the runs, only (re)produce the plots

"""

import sys
import os
import subprocess
from multiprocessing import Pool
from math import pi, lgamma
import argparse

import matplotlib as mpl
mpl.use('Agg') # avoids requirement to have DISPLAY set
import matplotlib.pyplot as plt
from   matplotlib.backends.backend_pdf import PdfPages
from   matplotlib.ticker import ScalarFormatter
from   matplotlib import cycler
from   matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import copy

sys.path.insert(0, os.path.dirname(__file__) + "/../submodules/AnalysisTools/python")
from hfile       import *
from poly_interp import *

#----------------------------------------------------------------------
# Configuration.
#
# The shower will be run for a selected set of alphas values
#  
alphass = [ 0.0016,  0.008,  0.016]
nevents = [ 750000, 300000,  20000]
nruns   = [     10,      3,      5]

parser = argparse.ArgumentParser(description='Runs a NLL test for global event shapes (for betaPS=0 showers and betaObs=0 event shapes)')
parser.add_argument('--njobs', '-j', type=int, default=None,
                    help='number of jobs to run in parallel')
parser.add_argument('--shower', default="panglobal-beta0.0", help='beta==0 shower to use (panglobal-beta0.0 or dipole-kt)')
parser.add_argument("--only-plots", help="assuming you have already done the runs, only (re)produce the plots", action="store_true")
args = parser.parse_args()

njobs         = args.njobs  # number of jobs to be run in parallel
target_lambda = -0.5        # targetted value of alphas L
lnbuffer      = -15         # margin in lnv for "resolved" emissions
outdir        = f"example-results/nll-global-test/{args.shower}"   # output folder
use_doubleexp = False
build_dir = "build-doubleexp" if use_doubleexp else "build-double"

observables = ["Mj", "Sj", "sqrt_y3"] # watch out: sqrt_y3 only
                                      # available for beta=0 showers

# get the command-line to run
def get_command(alphas, nev, iseq):
    lnktcut = target_lambda/alphas+lnbuffer
    shower = args.shower
    shower = shower.replace("-beta", " -beta ")
    nev_cache = max(50000, nev/10.0)
    out_res = f"{outdir}/runs/lambda{target_lambda}-alphas{alphas}-rseq{iseq}1.res"
    out_log = f"{outdir}/runs/lambda{target_lambda}-alphas{alphas}-rseq{iseq}1.log"
    for f in [out_res, out_log]:
        if os.path.isfile(f): os.remove(f)
    return f"{build_dir}/example-global-nll-ee -Q 1.0 -shower {shower} -spin-corr off -nloops 2"\
           f" -weighted-generation -nev-cache {nev_cache} -colour NODS"\
           f" -dynamic-lncutoff {lnbuffer} -lambda-obs-min {target_lambda} -alphas {alphas} -lnkt-cutoff {lnktcut} "\
           f" -nev {nev} -rseq {iseq}1 -out {out_res} "\
           f"> {out_log}"


#----------------------------------------------------------------------
def main():
    # this is meant to run from the directory where the script is
    script_path = os.path.dirname(os.path.abspath( __file__ ))
    os.chdir(script_path)

    # STEP 0: make sure the code is built and the result directory
    #         exists
    if not args.only_plots: 
        
        build_code()
        if not os.path.isdir(outdir+"/runs"):
            os.makedirs(outdir+"/runs")
        print(f"run files will be placed in {outdir}/runs")

        # STEP 1: run the shower code
        run_shower()
        
        # STEP 2: combine the runs
        combine_runs()
        
        # STEP 3: extrapolate alphas -> 0
        extrapolate_as_to_zero()

    # STEP 4: some plots
    do_plots()
    
#----------------------------------------------------------------------
# STEP 1: run the shower code
def run_shower():
    print ("------------------------------------------------------------")
    print ("--- RUNNING THE SHOWER FOR THE SELECTED VALUES OF ALPHAS ---")
    print ("------------------------------------------------------------")
    if (njobs != None): print (f"Running with {njobs} jobs")
    else: print (f"Running with {os.cpu_count()} jobs")
    pool = Pool(njobs)

    for alphas, nev, nrun in zip(alphass, nevents, nruns):
        for irun in range(1, nrun+1):
            print (f"queuing command for alphas={alphas} with {nev} events and seed {irun}")
            command = get_command(alphas, nev, irun)
            pool.apply_async(subprocess.run,
                             args = [command,],
                             kwds = dict(shell=True, stdout=subprocess.PIPE))
    pool.close()
    print ("--- WAITING FOR THE JOBS TO FINISH                       ---")
    print ("--- This might take a few minutes...                     ---")
    pool.join()
    print ("--- DONE                                                 ---")

#----------------------------------------------------------------------
# STEP 2: combine the runs
def combine_runs():
    print ("------------------------------------------------------------")
    print ("--- COMBINE THE RUNS FOR EACH VALUE OF ALPHAS            ---")
    print ("------------------------------------------------------------")
    for alphas in alphass:
        print (f"alphas = {alphas}")
        command = f"../submodules/AnalysisTools/scripts/combine-runs.pl {outdir}/runs/lambda{target_lambda}-alphas{alphas}-rseq*.res > {outdir}/lambda{target_lambda}-alphas{alphas}.res"
        stdouterr_from(command)
        print (f"Generated result file {outdir}/lambda{target_lambda}-alphas{alphas}.res")
    print ("--- DONE                                                 ---")
    
#----------------------------------------------------------------------
# STEP 3: extrapolate alphas -> 0
def extrapolate_as_to_zero():
    print ("------------------------------------------------------------")
    print ("--- EXTRAPOLATE SIGMA_PS/SIGMA_NLL TO ALPHAS -> 0        ---")
    print ("------------------------------------------------------------")

    ofile = open(f"{outdir}/lambda{target_lambda}-alphas_to_0.res", "w")
    for hist in observables:
        print (f"Histogram for {hist}")
        sigmas  = []
        dsigmas = []
        lambda_values = None
        for alphas in alphass:
            # read the results for this alphas value and store it
            data = get_array(f"{outdir}/lambda{target_lambda}-alphas{alphas}.res",
                             f"cumul_hist:{hist}")
            if lambda_values is None: lambda_values = data[:-1,0]
            an = analytic_nll(lambda_values, hist, alphas)
            sigmas .append(data[:-1,1]/an)
            dsigmas.append(data[:-1,2]/an)
        sigmas  = np.asarray(sigmas)
        dsigmas = np.asarray(dsigmas)

        extrap, dextrap = get_extrapolation_to_zero(np.asarray(alphass),
                                                    sigmas, dsigmas)

        print (f"# cumul_hist:{hist} (Sigma_PS/Sigma_NLL)", file=ofile)
        print (f"# columns: lambda extapolated_value extrapolation_error", file=ofile)
        for i in range(sigmas.shape[1]):
            print(f"{lambda_values[i]}  {extrap[i]}  {dextrap[i]}", file=ofile)
        print ("", file=ofile)
        print ("", file=ofile)
            
    print (f"Generated result file {outdir}/lambda{target_lambda}-alphas_to_0.res")
    print ("--- DONE                                                 ---")

#----------------------------------------------------------------------
# STEP 4: do some plots
def do_plots():
    print ("------------------------------------------------------------")
    print ("--- PRODUCE A FEW PLOTS                                  ---")
    print ("------------------------------------------------------------")

    legend_loc={"Mj"      : "upper right",
                "Sj"      : "lower right",
                "sqrt_y3" : "upper right"}
    text_yloc={"Mj"      : 0.035,
               "Sj"      : 0.935,
               "sqrt_y3" : 0.035}
    obs_label={"Mj"      : r'$M_{j,0}$',
               "Sj"      : r'$S_{j,0}$',
               "sqrt_y3" : r'$\sqrt{y_{23}}$'}
    showertags={"panglobal-beta0.0" : r'PanGlobal($\beta_{\rm ps}=0$)'}
    for obs in observables:
        fig,ax = plt.subplots()

        ax.set_title(f"NLL accuracy test for "+obs_label[obs], fontsize=20)
        
        ax.set_xlim([target_lambda,0])
        ax.set_xlabel(r'$\lambda=\alpha_s\ln $'+obs_label[obs], fontsize=18)
        ax.set_ylabel(r'$\Sigma_{{\rm PS}}(\lambda)/\Sigma_{{\rm NLL}}(\lambda)$', fontsize=18)
        

        shower_label = showertags[args.shower] if args.shower in showertags.keys() else args.shower
        ax.text(0.98, text_yloc[obs], f"{shower_label}", ha='right', va='baseline', transform=ax.transAxes, fontsize=14,color='gray')
        ax.text(1.01, 0.25, r'$e^+e^-\to q\bar{q}$, $\alpha_s^{{\rm (CMW)}}(2-{{\rm loops}})$, NODS', ha='left', transform=ax.transAxes, fontsize=9,color='gray', rotation=270)
            
        ax.tick_params(top=True,right=True,direction='in',which='both')
        ax.grid(True,ls=":")

        # individual alphas values
        alphas_strs = [str(a) for a in alphass[::-1]]
        alphas_tags = [rf"$\alpha_s={a}$" for a in alphass[::-1]]
        for alphas, alphas_str, label in zip(alphass[::-1], alphas_strs, alphas_tags):
            data = get_array_plus_comments(f"{outdir}/lambda{target_lambda}-alphas{alphas_str}.res",
                                           f"cumul_hist:{obs}", columns={'x':0,'y':(1,2)})
            mask=(data.x>target_lambda-0.0001) & (data.x<-0.0001)
            data.y = data.y[mask]
            data.x = data.x[mask]
            an = analytic_nll(data.x, obs, alphas)
            data.y /= an
            line_and_band(ax, data.x, data.y, lw=0.75, label=label)

        # extrapolation (and expected)
        data = get_array_plus_comments(f"{outdir}/lambda{target_lambda}-alphas_to_0.res",
                                       f"cumul_hist:{obs}", columns={'x':0,'y':(1,2)})
        mask=(data.x>target_lambda-0.0001) & (data.x<-0.0001)
        data.y = data.y[mask]
        data.x = data.x[mask]
        line_and_band(ax, data.x, data.y, label=r"$\alpha_s\to 0$")
        ax.plot(data.x, 0.0*data.x+1.0, lw=1.5, ls="--", color='purple', label=r"NLL accurate")
        
        ax.legend(loc=legend_loc[obs])
        outfig = outdir+'/'+obs+'.pdf'
        plt.savefig(outfig,bbox_inches='tight',dpi=300)
        plt.close()
            
        print (f"Generated {outfig}")
    print ("--- DONE                                                 ---")
    
#----------------------------------------------------------------------
# analytic NLL results
def analytic_nll(lambda_values, observable, alphas, beta=0):
    if beta != 0:
        print("Analytic results are hardcoded for beta=0 observables for the moment")
        exit(-1)

    # we're currently running at full Nc
    CF = 4.0/3.0
    CA = 3.0
    nf = 5.0
    TR = 0.5

    b0 = (11*CA-4*nf*TR)/(12*pi)
    b1 = (17*CA*CA-10*CA*nf*TR-6*CF*nf*TR)/(24*pi**2)
    K  = (67.0/18.0-pi**2/6.0)*CA - 10.0/9.0*nf*TR

    Bq = -3.0/4.0

    ls     = -b0*lambda_values
    om2ls  = 1-2*ls
    lom2ls = np.log(om2ls)
    
    T = lambda logargs : -1.0/(pi*b0) * logargs
    if beta==0:
        g1 = CF/(pi*b0*ls) * (2*ls + lom2ls)

        r2 = CF/(pi*b0**2) * (-K/(2*pi) * (lom2ls + 2*ls/om2ls)
                              +b1/b0 * (0.5*lom2ls*lom2ls + (lom2ls+2*ls)/om2ls))
        hc = -2*CF * Bq * T(lom2ls)
        rp = 4*CF/(pi*b0)*ls/(1-2*ls)

    else:
        om2lbs = 1-2*ls/(1+beta)
        lom2lbs = np.log(om2lbs)   
        g1 = CF/(pi*b0*ls*beta) * (om2ls*lom2ls - (1+beta)*om2lbs*lom2lbs)

        r2 = CF/(pi*b0**2*beta) * (-K/(2*pi) * ((1+beta)*lom2lbs - lom2ls)
                                   +b1/b0 * (0.5*lom2ls*lom2ls - 0.5*(1+beta)*lom2lbs*lom2lbs + lom2ls - (1+beta)*lom2lbs))
        hc = -2*CF * Bq * T(lom2lbs)
        rp = 2*CF*(T(lom2ls) - T(lom2lbs))/beta

    lnf = -np.euler_gamma*rp-lngamma(1+rp) if observable=="Sj" else 0.0

    g2 = r2 + hc + lnf

    return np.exp(g1*(-lambda_values/alphas)+g2)
    
#----------------------------------------------------------------------
# helpers


# build the code in doubleexp
def build_code():
    print(f"Building everything in {build_dir}")
    orig_dir=os.getcwd()
    if not os.path.isdir(build_dir):
        os.mkdir(build_dir)
    os.chdir(build_dir)
    # cmake will be needed if we make this part of a nightly check
    if use_doubleexp:
        stdouterr_from(f"cmake -B . -S .. -DPSDOUBLEEXP=on")
    else:
        stdouterr_from(f"cmake -B . -S ..")
    stdouterr_from("make -j")
    print("done\n")
    os.chdir(orig_dir)


# run a command and get the output
def stdouterr_from(arguments):
    result = subprocess.run(arguments, shell=True,
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if (result.returncode != 0):
        print(result)
        print("An error occurred")
        exit(-1)
    return result.stdout.decode('utf-8')

# compute ln(Gamma(a)) on an np array a
def lngamma(a):
    return np.asarray([lgamma(x) for x in a])


# matplotlib config
styles = [
    {'color':'#f06060'},
    {'color':'#4040e0'},
    {'color':'#40c040'},
    {'color':'#404040'},
    {'color':'#e0a040'},
    ]
colours = cycler('color', [style['color'] for style in styles])
# see options for things to set with 
#     python -c "import matplotlib as mpl; print(mpl.rcParams)"
plt.rc('axes',  grid=True, prop_cycle=colours)
plt.rc('figure', figsize=(5,3.8))


plt.rcParams['xtick.labelsize'] = 15
plt.rcParams['ytick.labelsize'] = 15
plt.rcParams['legend.fontsize'] = 15
plt.rcParams['legend.handlelength'] = 1

# Matplotlib helper to plot both a line and a band
def line_and_band(ax,x,val_and_err,**extra):
    extra_no_label = copy.copy(extra)
    if ('label' in extra_no_label): del extra_no_label['label']
    ax.fill_between(x,
                    val_and_err.value-val_and_err.error,
                    val_and_err.value+val_and_err.error,
                    alpha=0.2,
                    **extra_no_label
                    )
    ax.plot(x,val_and_err.value, **extra)


if __name__ == '__main__': main()
