//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "Momentum4.hh"
#include "Type.hh"
#include <cassert>

namespace panscales{

template <class T>
bool Momentum4<T>::verbose_ = false;
template <class T>
void Momentum4<T>::set_verbose (bool v) {verbose_ = v;}
template <class T> 
bool Momentum4<T>::verbose() {return verbose_;}

//----------------------------------------------------------------------
/// rapidity of the jet
template <class T>
T Momentum4<T>::rap() const  {
  T rap;
  if (E_ == fabs(pz_)) {
    if (pz_ >= 0.0) {
      rap = maxrap_;
    } else {
      rap = -maxrap_;
    }
  } else {
    rap = 0.5*log_T((E_ + pz_)/(E_ - pz_));
  }
  return rap;
}

//----------------------------------------------------------------------
/// phi of the jet
template <class T>
T Momentum4<T>::phi() const {
  T phi;
  if (px_*px_ + py_*py_ == 0.0) {
    phi = 0.0;
  } else {
    phi = atan2(py_,px_);
  }
  if (phi < 0.0) {phi += 2.0*M_PI;}
  if (phi >= 2.0*M_PI) {phi -= 2.0*M_PI;}
  return phi;
}

//----------------------------------------------------------------------
/// multiply the jet's momentum by the coefficient
template <class T>
Momentum4<T> & Momentum4<T>::operator*=(T coeff) {
  px_ *= coeff;
  py_ *= coeff;
  pz_ *= coeff;
  E_  *= coeff;
  return *this;
}

//----------------------------------------------------------------------
// return the division, jet/coeff
template <class T>
Momentum4<T> & Momentum4<T>::operator/=(T coeff) {
  px_ /= coeff;
  py_ /= coeff;
  pz_ /= coeff;
  E_  /= coeff;
  return *this;
}
//----------------------------------------------------------------------
// return "sum" with Momentum4
template <class T>
Momentum4<T> & Momentum4<T>::operator+=(const Momentum4<T> & jet) {
  px_ += jet.px();
  py_ += jet.py();
  pz_ += jet.pz();
  E_  += jet.E ();
  return *this;
} 

//----------------------------------------------------------------------
// return difference with Momentum4
template <class T>
Momentum4<T> & Momentum4<T>::operator-=(const Momentum4<T> & jet) {
  px_ -= jet.px();
  py_ -= jet.py();
  pz_ -= jet.pz();
  E_  -= jet.E ();
  return *this;
}


/// reset momentum of a jet
template <class T>
void Momentum4<T>::reset_momentum(const Momentum4<T> & jet) {
  px_ = jet.px();
  py_ = jet.py();
  pz_ = jet.pz();
  E_  = jet.E ();
}
  
/// reset momentum
template <class T>
void Momentum4<T>::reset_momentum(T px_in, T py_in, T pz_in, T E_in) {
  px_ = px_in;
  py_ = py_in;
  pz_ = pz_in;
  E_  = E_in;  
}

//----------------------------------------------------------------------
/// transform this jet (given in the rest frame of prest) into a jet
/// in the lab frame 
//
// NB: code adapted from that in herwig f77.

template <class T>
Momentum4<T> & Momentum4<T>::boost(const Momentum4<T> & prest) {
  
  if (prest.px() == 0.0 && prest.py() == 0.0 && prest.pz() == 0.0) 
    return *this;

  T m_local = prest.m();
  assert(m_local != 0);

  T pf4  = (  px()*prest.px() + py()*prest.py()
                 + pz()*prest.pz() + E()*prest.E() )/m_local;
  T fn   = (pf4 + E()) / (prest.E() + m_local);
  px_ +=  fn*prest.px();
  py_ +=  fn*prest.py();
  pz_ +=  fn*prest.pz();
  E_ = pf4;

  return *this;
}


//----------------------------------------------------------------------
/// transform this jet (given in lab) into a jet in the rest
/// frame of prest  
//
// NB: code adapted from that in herwig f77.

template <class T>
Momentum4<T> & Momentum4<T>::unboost(const Momentum4<T> & prest) {
  
  if (prest.px() == 0.0 && prest.py() == 0.0 && prest.pz() == 0.0) 
    return *this;

  T m_local = prest.m();
  assert(m_local != 0);

  T pf4  = ( -px()*prest.px() - py()*prest.py()
                 - pz()*prest.pz() + E()*prest.E() )/m_local;
  T fn   = (pf4 + E()) / (prest.E() + m_local);
  px_ -=  fn*prest.px();
  py_ -=  fn*prest.py();
  pz_ -=  fn*prest.pz();
  E_ = pf4;

  return *this;
}

//----------------------------------------------------------------------
/// return "sum" of two pseudojets
template <class T>
Momentum4<T> operator+ (const Momentum4<T> & jet1, const Momentum4<T> & jet2) {
  return Momentum4<T>(jet1.px()+jet2.px(),
		  jet1.py()+jet2.py(),
		  jet1.pz()+jet2.pz(),
		  jet1.E() +jet2.E()  );
} 

//----------------------------------------------------------------------
/// return difference of two pseudojets
template <class T>
Momentum4<T> operator- (const Momentum4<T> & jet1, const Momentum4<T> & jet2) {
  return Momentum4<T>(jet1.px()-jet2.px(),
		  jet1.py()-jet2.py(),
		  jet1.pz()-jet2.pz(),
		  jet1.E() -jet2.E()  );
}
//----------------------------------------------------------------------
/// return the product, coeff * jet
template <class T>
Momentum4<T> operator* (T coeff, const Momentum4<T> & jet) {
  Momentum4<T> coeff_times_jet(jet);
  coeff_times_jet *= coeff;
  return coeff_times_jet;
}

//----------------------------------------------------------------------
/// return the division, jet / coeff
template <class T>
Momentum4<T> operator/ (const Momentum4<T> & jet, T coeff) {
  Momentum4<T> jet_over_coeff(jet);
  jet_over_coeff /= coeff;
  return jet_over_coeff;
}

//----------------------------------------------------------------------
/// Returns the 3-vector dot-product of p1 and p2.
template <class T>
T dot3(const Momentum4<T> & p1, const Momentum4<T> & p2) {
  return p1.px() * p2.px() + p1.py() * p2.py() + p1.pz() * p2.pz();
}

//----------------------------------------------------------------------
/// Returns the 3-vector cross-product of p1 and p2. If lightlike is false
/// then the energy component is zero; if it's true the the energy component
/// is arranged so that the vector is lightlike
template <class T>
Momentum4<T> cross(const Momentum4<T> & p1, const Momentum4<T> & p2, bool lightlike) {
  T px = p1.py() * p2.pz() - p2.py() * p1.pz();
  T py = p1.pz() * p2.px() - p2.pz() * p1.px();
  T pz = p1.px() * p2.py() - p2.px() * p1.py();

  T E;
  if (lightlike) {
    E = sqrt(px*px + py*py + pz*pz);
  } else {
    E = 0.0;
  }
  return Momentum4<T>(px, py, pz, E);
}

//----------------------------------------------------------------------
/// Returns two 4-vectors, each with square = -1, that have a zero
/// (4-vector) dot-product with dir1 and dir2.
///
/// When dir1 and dir2 form a plane, then perp1 will be out of that
/// plane and perp2 will be in the plane.
template <class T>
void twoPerp(const Momentum4<T> & dir1, const Momentum4<T> & dir2,
            Momentum4<T> & perp1, Momentum4<T> & perp2) {

  // First get a 3-vector that is perpendicular to both dir1 and dir2.
  perp1 = cross(dir1,dir2);

  // for now test for exact zero -- later we will have to be more sophisticated...
  if (perp1.pt2() + perp1.pz()*perp1.pz() == 0.0) {
    if (fabs(dir1.px()) < fabs(dir1.py()) && fabs(dir1.px()) < fabs(dir1.pz())) {
      // x is smallest direction, so use that as a starting point
      // for a cross product
      perp2 = cross(dir1, Momentum4<T>(1,0,0,0));
    } else if (fabs(dir1.py()) < fabs(dir1.pz())) {
      perp2 = cross(dir1, Momentum4<T>(0,1,0,0));
    } else {
      perp2 = cross(dir1, Momentum4<T>(0,0,1,0));
    }
    perp1 = cross(dir1,perp2);
  } else {
    // requirements for perp2:
    // - 3-vector should be perpendicular to perp1
    // - 3-vector dot-product with dir1 and dir2 should be zero
    // - norm = -1
    //-----
    // go to centre-of-mass frame of the dipole
    Momentum4<T> dir12 = dir1 + dir2;
    Momentum4<T> dir1rest = dir1; dir1rest.unboost(dir12);
    // get something perpendicular to perp1 and either of the dipole directions
    // (since perp1 is perpendicular to both, it doesn't change with the boost)
    perp2 = cross(dir1rest,perp1);
    // boost back
    perp2.boost(dir12);
  }
  
  // arrange norm -1 for perp1 and perp2
  perp1 /= sqrt((-perp1.m2()));
  perp2 /= sqrt((-perp2.m2()));

}

/// angular distance for Durham algorithm in e+e-
template <class T>
T one_minus_costheta(const Momentum4<T> & p1, const Momentum4<T> & p2) {

  T p1mod = sqrt(p1.px()*p1.px() + p1.py()*p1.py() + p1.pz()*p1.pz());
  T p2mod = sqrt(p2.px()*p2.px() + p2.py()*p2.py() + p2.pz()*p2.pz());
  
  return 1.0 - dot3(p1,p2)/p1mod/p2mod;
}

template class Momentum4<precision_type>;
template Momentum4<precision_type> operator+<precision_type>(const Momentum4<precision_type>&,
					    const Momentum4<precision_type>&);
template Momentum4<precision_type> operator-<precision_type>(const Momentum4<precision_type>&,
					    const Momentum4<precision_type>&);
template Momentum4<precision_type> operator/<precision_type>(const Momentum4<precision_type>&, precision_type);
template Momentum4<precision_type> operator*<precision_type>(precision_type, const Momentum4<precision_type>&);
template precision_type dot_product(const Momentum4<precision_type> &,
			    const Momentum4<precision_type> &);
// #TRK_ISSUE-186 todo: check why we need the following line?
//template precision_type pow2(precision_type);
template std::ostream & operator<<(std::ostream &, const Momentum4<precision_type> &);
template precision_type dot3(const Momentum4<precision_type> &, const Momentum4<precision_type> &);
template Momentum4<precision_type> cross(const Momentum4<precision_type> &, const Momentum4<precision_type> &, bool);
template void twoPerp(const Momentum4<precision_type> &, const Momentum4<precision_type> &,
		      Momentum4<precision_type> &, Momentum4<precision_type> &);
template precision_type one_minus_costheta(const Momentum4<precision_type> &, const Momentum4<precision_type> &);

} // namespace panscales
