//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerPanScaleLocalAntennapp.hh"

using namespace std;

namespace panscales{

  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerPanScaleLocalAntennapp::elements(
    // This is an antenna shower, so we need one element per dipole
    // Definition is that we have [3end, 3bar-end] for each dipole
    // For IF and FI, the definition is then that the first letter corresponds
    // to the 3end of the dipole, and the 2nd letter corresponds to the 3bar-end
    Event & event, int dipole_index) const {
    int i3    = event.dipoles()[dipole_index].index_3()   ;
    int i3bar = event.dipoles()[dipole_index].index_3bar();
    ShowerPanScaleLocalAntennapp::Element *elm;
    if (event.particles()[i3].is_initial_state()){
      if (event.particles()[i3bar].is_initial_state()){
        // II dipole
        elm = new ShowerPanScaleLocalAntennapp::ElementII(i3, i3bar, dipole_index, &event, this);
      } else {
        // 3 is I, 3bar is F
        elm = new ShowerPanScaleLocalAntennapp::ElementIF(i3, i3bar, dipole_index, &event, this);
      }
    } else {
      if (event.particles()[i3bar].is_initial_state()){
        // 3 is F, 3bar is I
        // we make this choice such that we do not copy the kinematics of IF for an FI element
        elm = new ShowerPanScaleLocalAntennapp::ElementIF(i3bar, i3, dipole_index, &event, this);
      } else {
        // 3 and 3bar are F
        elm = new ShowerPanScaleLocalAntennapp::ElementFF(i3, i3bar, dipole_index, &event, this);
      }
    }      
    std::unique_ptr<typename ShowerBase::Element> elms[] = {std::unique_ptr<typename ShowerBase::Element>(elm)};
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }

  ShowerBase::EmissionInfo* ShowerPanScaleLocalAntennapp::create_emission_info() const{ return new ShowerPanScaleLocalAntennapp::EmissionInfo(); }

  //----------------------------------------------------------------------
  Range ShowerPanScaleLocalAntennapp::Element::lnb_generation_range(double lnv) const {
    assert(false && "this should be reimplemented in the derived element classes");
    return Range(0,0);
  }
  
  Range ShowerPanScaleLocalAntennapp::Element::lnb_exact_range(double lnv) const {
      std::cerr << "Error: We do not expect lnb_exact_range to be called " << std::endl;
      return Range(0,0);
  }
  
  //----------------------------------------------------------------------
  // bit of acceptance probability that is common to all the elements
  bool ShowerPanScaleLocalAntennapp::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    
    typename ShowerPanScaleLocalAntennapp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalAntennapp::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;

    // see Eqs. (4.3) and (4.6) in arXiv:2205.02237
    precision_type kappat = _rho*exp(precision_type(lnv + _shower->_beta*abs(lnb)));
    precision_type alphak = sqrt(_sjtilde/(_dipole_m2*_sitilde))*kappat*exp(precision_type(lnb));
    // now compute betak
    // betak  = sqrt(_sitilde/(_dipole_m2*_sjtilde))*kappat*exp(-lnb);
    // but we can use 1/alpha = sqrt(_sitilde/_sjtilde)*sqrt(_dipole_m2)*1/kappat*exp(-lnb)
    // so betak = kappat^2 / _dipole_m2 * 1/alphak
    precision_type betak = pow2(kappat)/_dipole_m2/alphak;

    emission_info.kappat = kappat;
    emission_info.alphak = alphak;
    emission_info.betak  = betak;

    // antenna shower: either emitter or spectator can split
    f_fcn(lnb, emission_info.f, emission_info.omf);
    emission_info.emitter_weight_rad_gluon   = 0.0;
    emission_info.emitter_weight_rad_quark   = 0.0;
    emission_info.spectator_weight_rad_gluon = 0.0;
    emission_info.spectator_weight_rad_quark = 0.0;
    
    return true;
  }


  //----------------------------------------------------------------------
  // Boost event and realign incoming particles (see Appendix B in arXiv:2205.02237 for more details)
  void ShowerPanScaleLocalAntennapp::Element::lorentz_boost_realign(const Momentum& current_q) const {
    // We force the initial-state particles to be back to back by performing the longitudinal boost.
    // The CM frame is the beam CM frame, so when we rotate we have the correct PDF fractions
    
    // need the reversed because we want to go from current_Q to its rest frame
    const LorentzBoost boost(current_q.reversed()); 
    if(use_diffs()){
      // we need to do the boost but also store the energies of the particles before they enter the boost
      // first set up the boost class
      // then handle the boost, making sure we store the energies
      std::vector<precision_type> Ei(_event->size());
      for(unsigned i = 0; i < _event->size(); ++i) {

        Particle & p = (*_event)[i];
        Ei[i] = p.E();
      
        (*_event)[i].reset_momentum(p.boost(boost));

      }
       // then handle the dipole differences
      for (auto & dipole: _event->dipoles()) {
        int i3bar = dipole.index_3bar();
        int i3    = dipole.index_3();
        dipole.dirdiff_3_minus_3bar = apply_linear_transform_to_dirdiff(boost, Ei[i3bar], Ei[i3], (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);      
      }
      // then handle the dipole differences for non-splitting dipoles as well
      for (auto & dipole: _event->non_splitting_dipoles()) {
        int i3bar = dipole.index_3bar();
        int i3    = dipole.index_3();
        dipole.dirdiff_3_minus_3bar = apply_linear_transform_to_dirdiff(boost, Ei[i3bar], Ei[i3], (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);      
      }

    }
    else{
      // just do the boost 
      for(unsigned i = 0; i < _event->size(); ++i) {
        Particle & p = (*_event)[i];
        (*_event)[i].reset_momentum(p.boost(boost));
      }
    }
    
    // MvB changed the below line - was not working!
    // const Particle & pa = _event->beam1().pz() > 0 ? _event->incoming1() : _event->incoming2();
    // new code:
    Momentum4<precision_type> pa = _event->incoming1().momentum();
    // reverse components if pz < 0
    if(pa.pz() < 0) pa = pa.reversed();

    // setting up the rotation matrix
    Matrix3 rot_pa_to_z = Matrix3::from_direction(pa).transpose();

    // do the rotation
    for(unsigned i = 0; i < _event->size(); ++i) {
      Particle & p = (*_event)[i];
      
      Momentum3<precision_type> new_mom_p3;
      // enforce the exact kinematics for incoming particles
      if(p.is_initial_state()){
        if(p.pz() < 0) new_mom_p3 = Momentum3<precision_type>(0,0,-p.E());
        else           new_mom_p3 = Momentum3<precision_type>(0,0, p.E());
      } else {// particle is final state, do the rotation
        new_mom_p3 = rot_pa_to_z * p;
      }
      (*_event)[i].reset_momentum(Momentum::fromP3M2(new_mom_p3, p.m2()));
    }

    // rotate the directional differences as well 
    if(use_diffs()){
       // then handle the dipole differences
      for (auto & dipole: _event->dipoles()) {
        dipole.dirdiff_3_minus_3bar = rot_pa_to_z *dipole.dirdiff_3_minus_3bar;  
        // explicitly set the II dipoles with 0 px and py components 
        if((*_event)[dipole.index_3()].is_initial_state() && (*_event)[dipole.index_3bar()].is_initial_state())  {
          dipole.dirdiff_3_minus_3bar  = Momentum3<precision_type>(0,0,dipole.dirdiff_3_minus_3bar.pz());
        }  
      }
      // then handle the dipole differences for non-splitting dipoles as well
      for (auto & dipole: _event->non_splitting_dipoles()) {
        dipole.dirdiff_3_minus_3bar = rot_pa_to_z *dipole.dirdiff_3_minus_3bar;        
      }

    }
  }

  //----------------------------------------------------------------------
  // Postpone the boost until the end of the showering process
  // This is assuming Q in the lab frame does not have any kT component
  // so that we can always write Q = a1 * beam1() + a2 * beam2()
  void ShowerPanScaleLocalAntennapp::Element::reset_beam_and_shower_Q(precision_type x1, precision_type x2, const Momentum& current_Q) const {

    Momentum shower_Q = (*_event).Q();
    
    //Decompose current Q using the beams as basis 
    //Here the transverse components are assumed to be 0
    precision_type sAB, sA, sB;
    sA = dot_product(shower_Q, (*_event).beam1());
    sB = dot_product(shower_Q, (*_event).beam2());
    sAB = dot_product((*_event).beam1(), (*_event).beam2());

    //Now reset the beams with pA = pa/xa, pB = pb/xb
    //so that they remain aligned with the incoming partons
    _event->reset_beam1((*_event)[0]/x1);
    _event->reset_beam2((*_event)[1]/x2);

    //New shower_Q using the updated beams
    shower_Q = sB/sAB * (*_event).beam1() +  sA/sAB * (*_event).beam2();
    _event->reset_Q(shower_Q);
  } 

  //======================================================================
  // ShowerPanScaleLocalAntennapp::ElementII implementation
  //======================================================================
  double ShowerPanScaleLocalAntennapp::ElementII::lnb_extent_const() const{
    double xi      = to_double(event().pdf_x(emitter()));
    double xj      = to_double(event().pdf_x(spectator()));
    double xi_over_omxi = xi / (1 - xi);
    double xj_over_omxj = xj / (1 - xj);
    double lnxij_over_omxij = log(xi_over_omxi*xj_over_omxj);
    return to_double(_log_dipole_m2 - 2.*_log_rho - lnxij_over_omxij)/(1. + _shower->_beta);
  }  

  Range ShowerPanScaleLocalAntennapp::ElementII::lnb_generation_range(double lnv) const{
    // range is a slight overestimate of the exact range
    double xi      = to_double(event().pdf_x(emitter()));
    double xj      = to_double(event().pdf_x(spectator()));
    double xi_over_omxi = xi / (1 - xi);
    double xj_over_omxj = xj / (1 - xj);
    double lnxi_over_omxi = log(xi_over_omxi);
    double lnxj_over_omxj = log(xj_over_omxj);
    
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv + lnxj_over_omxj) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv - lnxi_over_omxi) / (1+_shower->_beta));
  }
  
  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleLocalAntennapp::ElementII::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{

    //construct alphak, betak which is the same for every element
    ShowerPanScaleLocalAntennapp::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalAntennapp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalAntennapp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalAntennapp::EmissionInfo*>(emission_info_base));

    // determine the z factors that go into the splitting kernels
    // See Eq. (4.8) in arXiv:2205.02237
    // we say that za = ak/ai with ak = alphak and ai = 1+alphak in the 
    // limit that betak -> 0 (this is the collinear limit for the emitter)
    
    precision_type za   = emission_info.alphak / (1. + emission_info.alphak);
    precision_type omza = 1.                   / (1. + emission_info.alphak); 
    precision_type zb   = emission_info.betak  / (1. + emission_info.betak);
    precision_type omzb = 1.                   / (1. + emission_info.betak);  

    
    // here we should make a decision based on flavour.
    //
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), za,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    _shower->fill_dglap_splitting_weights(spectator(), zb,
                                          emission_info.spectator_weight_rad_gluon,
                                          emission_info.spectator_weight_rad_quark);

    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    // we use the omza = 1/ra and omzb = 1/rb approximated expressions from above 
    if (emitter().initial_state()==1){ // emitter is beam1, spectator is beam 2
      emission_info.beam1_pdf_new_x_over_old = 1.0/omza; 
      emission_info.beam2_pdf_new_x_over_old = 1.0/omzb; 
    } else {                           // emitter is beam2, spectator is beam 1
      emission_info.beam1_pdf_new_x_over_old = 1.0/omzb; 
      emission_info.beam2_pdf_new_x_over_old = 1.0/omza; 
    }

      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = za;
    emission_info.z_radiation_wrt_spectator = zb;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    
    // f(eta) is multiplied in the ShowerRunner _acceptance_probability
    emission_info.emitter_weight_rad_gluon   *= emission_info.  f * normalisation_factor;
    emission_info.emitter_weight_rad_quark   *= emission_info.  f * normalisation_factor;
    emission_info.spectator_weight_rad_gluon *= emission_info.omf * normalisation_factor;
    emission_info.spectator_weight_rad_quark *= emission_info.omf * normalisation_factor;
    
    // in case this II element will be accepted we need to update all elements in the dipole cache
    emission_info.full_store_update_required = true;

    return true;
  }

  //----------------------------------------------------------------------
  // carry out the splitting and update the event
  bool ShowerPanScaleLocalAntennapp::ElementII::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalAntennapp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalAntennapp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalAntennapp::EmissionInfo*>(emission_info_base));
    
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    // build the radiated mapping coefficients
    // that can be found in
    // Eq. (4.17) in arXiv:2205.02237
    double gamma = (2.0/(1.0+_shower->beta()));
    precision_type ak  = pow(1.+emission_info.betak  ,gamma) * emission_info.alphak;
    precision_type bk  = pow(1.+emission_info.alphak ,gamma) * emission_info.betak ;
    precision_type norm_perp = sqrt(ak*bk*_dipole_m2);

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    // store for spin, when using dir-diffs this get rotated to the right plane
    emission_info.k_perp = perp;
    
    // helpers to build the ai, bi, aj, bj coefficients
    precision_type feta   = emission_info.  f;
    precision_type omfeta = emission_info.omf;
    precision_type akbk   = ak*bk;
    
    // lambda1, lambda2 are defined below Eqs.(B.62a-d) in arXiv:2205.02237
    precision_type lambda1 = 1. + ak + bk;
    precision_type lambda2 = lambda1 + 4. * feta * omfeta * akbk;
    precision_type s1 = sqrt(lambda1);
    precision_type s2 = sqrt(lambda2);

    // build the ai, bi, aj, bj coefficients
    // Eqs. (B.62a-d) in arXiv:2205.02237
    precision_type ai = (pow2(s1 + s2) + 4 * pow2(feta)   * akbk) / (4 * (1 + bk));
    precision_type bi = (pow2(s1 - s2) + 4 * pow2(feta)   * akbk) / (4 * (1 + ak));
    precision_type aj = (pow2(s1 - s2) + 4 * pow2(omfeta) * akbk) / (4 * (1 + bk));
    precision_type bj = (pow2(s1 + s2) + 4 * pow2(omfeta) * akbk) / (4 * (1 + ak));

    //fill the emitter, radiator and spectator 3-momenta
    // need a plus sign because emitter() + spectator()
    //          = emitter_out + spectator_out - radiation
    // (true for ISR)
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() +   feta * perp.p3() ;
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() +          perp.p3() ;
    Momentum3<precision_type> spectator_out3 = aj * rp.emitter.p3() + bj * rp.spectator.p3() + omfeta * perp.p3() ;
    
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    // update the PDF x fractions
    // we conserve the rapidity of the hard system
    // we need the momentum of the hard system here
    // so let us first retrieve it 

    // for now we take the hard system completely
    // maybe we need just a Z in case of a Z + hard_jet event
    // this needs to be checked once we get there
    Momentum3<precision_type> p_hard3 = rp.rotn_from_z.transpose()* _event->momentum_sum_hard_system();
    Momentum p_hard = Momentum::fromP3M2(p_hard3, _event->momentum_sum_hard_system().m2());
    
    // construct the dot product with the hard momentum 
    precision_type dot_i = dot_product(p_hard, emission_info.emitter_out)/dot_product(p_hard, rp.emitter);
    // and with the spectator
    precision_type dot_j = dot_product(p_hard, emission_info.spectator_out)/dot_product(p_hard, rp.spectator);

    // and fill the PDF factors, depending on which one was the emitter
    // see Eq. (B48) in arXiv:2205.02237 
    // with p_hard = p_Z
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = sqrt(dot_i/dot_j * lambda1);  
      emission_info.beam2_pdf_new_x_over_old = sqrt(dot_j/dot_i * lambda1);
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = sqrt(dot_j/dot_i * lambda1);
      emission_info.beam2_pdf_new_x_over_old = sqrt(dot_i/dot_j * lambda1);  
    }

    return true;
  }

  //----------------------------------------------------------------------
  // do the final boost
  void ShowerPanScaleLocalAntennapp::ElementII::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {
    
    // we need to cache PDF fractions before the emission 
    // as these will be used later by the longitudinal boost
    precision_type xatilde = event().pdf_x(emitter());
    precision_type xbtilde = event().pdf_x(spectator());
    
    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalAntennapp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    const typename ShowerPanScaleLocalAntennapp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalAntennapp::EmissionInfo*>(emission_info_base));

    // construct the new PDF x-fractions
    precision_type x1, x2;
    Momentum current_Q;
    if(emitter().initial_state()==1){
      x1 = xatilde*emission_info.beam1_pdf_new_x_over_old;
      x2 = xbtilde*emission_info.beam2_pdf_new_x_over_old;
      current_Q = emitter()/x1 + spectator()/x2;
    }else{
      x1 = xbtilde*emission_info.beam1_pdf_new_x_over_old;
      x2 = xatilde*emission_info.beam2_pdf_new_x_over_old;
      current_Q = emitter()/x2 + spectator()/x1;
    }

    if(!(boost_at_end())) ShowerPanScaleLocalAntennapp::Element::lorentz_boost_realign(current_Q);
    else ShowerPanScaleLocalAntennapp::Element::reset_beam_and_shower_Q(x1, x2, current_Q);
    
  }

  //======================================================================
  // ShowerPanScaleLocalAntennapp::ElementIF implementation
  //======================================================================
  
  double ShowerPanScaleLocalAntennapp::ElementIF::lnb_extent_const() const{
    double xi      = to_double(event().pdf_x(emitter()));
    double xi_over_omxi = xi / (1 - xi);
    double lnxi_over_omxi = log(xi_over_omxi);
    return to_double(_log_dipole_m2 - 2.*_log_rho - lnxi_over_omxi)/(1. + _shower->_beta);
  }

  Range ShowerPanScaleLocalAntennapp::ElementIF::lnb_generation_range(double lnv) const{
    double xi      = to_double(event().pdf_x(emitter()));
    double xi_over_omxi = xi / (1 - xi);
    double lnxi_over_omxi = log(xi_over_omxi);
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv                 ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv - lnxi_over_omxi) / (1+_shower->_beta));
  }
  
  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleLocalAntennapp::ElementIF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    
    //construct alphak, betak which is the same for every element
    ShowerPanScaleLocalAntennapp::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalAntennapp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalAntennapp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalAntennapp::EmissionInfo*>(emission_info_base));
        
    // bk needs to be smaller then 1 to prevent negative energies for the spectator
    if(emission_info.betak >= 1 ) return false;
    // to prevent lambda1 from going negative
    if((emission_info.alphak - emission_info.betak) <= -1) return false;

    // determine the z factors that go into the splitting kernels
    // z = ak/ai = alpha/(1+alpha) in the collinear limit
    
    precision_type za   = emission_info.alphak / (1. + emission_info.alphak);
    precision_type omza = 1.                   / (1. + emission_info.alphak); 
    precision_type zb   = emission_info.betak;
    
    // here we should make a decision based on flavour.
    //
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), za,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    _shower->fill_dglap_splitting_weights(spectator(), zb,
                                          emission_info.spectator_weight_rad_gluon,
                                          emission_info.spectator_weight_rad_quark);

    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    if (emitter().initial_state()==1){ // emitter is beam1, spectator is beam 2
      emission_info.beam1_pdf_new_x_over_old = 1.0/omza; 
      emission_info.beam2_pdf_new_x_over_old = 1.0; 
    } else {                           // emitter is beam2, spectator is beam 1
      emission_info.beam1_pdf_new_x_over_old = 1.0; 
      emission_info.beam2_pdf_new_x_over_old = 1.0/omza; 
    }

      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = za;
    emission_info.z_radiation_wrt_spectator = zb;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    
    emission_info.emitter_weight_rad_gluon   *= emission_info.  f * normalisation_factor;
    emission_info.emitter_weight_rad_quark   *= emission_info.  f * normalisation_factor;
    emission_info.spectator_weight_rad_gluon *= emission_info.omf * normalisation_factor;
    emission_info.spectator_weight_rad_quark *= emission_info.omf * normalisation_factor;
    
    // in case this IF element will be accepted we need to update all elements in the dipole cache
    emission_info.full_store_update_required = true;

    return true;
    
  }

  //----------------------------------------------------------------------
  // check after doing the acceptance prob whether the actual kinematics can return false
  bool ShowerPanScaleLocalAntennapp::ElementIF::check_after_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalAntennapp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalAntennapp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalAntennapp::EmissionInfo*>(emission_info_base));
    
    // build the radiated mapping coefficients
    // that can be found in
    // Eq. (4.17) in arXiv:2205.02237
    emission_info.ak   = emission_info.alphak;
    emission_info.bk   = pow(1+emission_info.alphak, 2./(1+_shower->beta()))*emission_info.betak;

    // helpers to build the ai, bi, aj, bj coefficients
    precision_type feta   = emission_info.  f;
    precision_type omfeta = emission_info.omf;
    precision_type akbk   = emission_info.ak*emission_info.bk;
    precision_type one_minus_bk = 1 - emission_info.bk;

    // lambda1, lambda2 are defined below Eqs.(B.42a-c) in arXiv:2205.02237
    precision_type lambda1      = one_minus_bk + emission_info.ak;
    precision_type lambda2      = lambda1 - 4 * feta * omfeta * akbk;
    if(lambda2 <=0 ) return false;
    
    precision_type s1 = sqrt(lambda1);
    precision_type s2 = sqrt(lambda2);

    // construct the map coefficients
    // Eqs. (B.64a-c) in arXiv:2205.02237
    emission_info.ai = (  pow2(s1+s2) - 4 * pow2(feta)   * akbk) / (4*(one_minus_bk));
    emission_info.aj = (- pow2(s1-s2) + 4 * pow2(omfeta) * akbk) / (4*(one_minus_bk));
    emission_info.bi = (- pow2(s1-s2) + 4 * pow2(feta)   * akbk) / (4*(1+ emission_info.ak));
    emission_info.bj = (  pow2(s1+s2) - 4 * pow2(omfeta) * akbk) / (4*(1+ emission_info.ak)); 
    // should all be positive
    if(( emission_info.ai < 0) || ( emission_info.aj < 0) || ( emission_info.bi < 0) || ( emission_info.bj < 0)) return false; 

    return true;
  }

  //----------------------------------------------------------------------
  // carry out the splitting
  bool ShowerPanScaleLocalAntennapp::ElementIF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalAntennapp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalAntennapp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalAntennapp::EmissionInfo*>(emission_info_base));
    // retrieve cached info
    const double & phi = emission_info.phi;
    const precision_type & ak = emission_info.ak;
    const precision_type & bk = emission_info.bk;
    const precision_type & ai = emission_info.ai;
    const precision_type & aj = emission_info.aj;
    const precision_type & bi = emission_info.bi;
    const precision_type & bj = emission_info.bj;
    
    // build the perp vector
    precision_type norm_perp = sqrt(ak*bk*_dipole_m2);
    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    // store for spin, when using dir-diffs this get rotated to the right plane
    emission_info.k_perp = perp;
    // fill the emitter, radiator and spectator 3-momenta
    // here we need a - sign for the spectator as it is a final-state particle
    precision_type feta   = emission_info.  f;
    precision_type omfeta = emission_info.omf;
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() +   feta * perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() +          perp.p3();
    Momentum3<precision_type> spectator_out3 = aj * rp.emitter.p3() + bj * rp.spectator.p3() - omfeta * perp.p3();
    
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
    
    // update the PDF x fractions (with the ones after the boost)
    // we need the momentum of the hard system here
    Momentum3<precision_type> p_hard3 = rp.rotn_from_z.transpose()* _event->momentum_sum_hard_system();
    Momentum p_hard = Momentum::fromP3M2(p_hard3, _event->momentum_sum_hard_system().m2());

    // construct the dot product with the hard momentum 
    precision_type dot_iZ = dot_product(p_hard, emission_info.emitter_out)/dot_product(p_hard, rp.emitter);
    // first figure out pB
    Momentum3<precision_type> pB3 = rp.rotn_from_z.transpose()*_event->opposite_beam(emitter());
    Momentum pB = Momentum::fromP3M2(pB3, 0);
    
    precision_type dot_j = dot_product(pB, emission_info.emitter_out)/dot_product(pB, rp.emitter);
    // fill the PDF factors
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = sqrt(dot_j*dot_iZ);   
      emission_info.beam2_pdf_new_x_over_old = sqrt(dot_j/dot_iZ);
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = sqrt(dot_j/dot_iZ);
      emission_info.beam2_pdf_new_x_over_old = sqrt(dot_j*dot_iZ);  
    }

    return true;
  }

  //----------------------------------------------------------------------
  // do the final boost
  void ShowerPanScaleLocalAntennapp::ElementIF::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {
    // retrieve the PDF factors
    precision_type xatilde = event().pdf_x(emitter());
    // we also need the xbtilde
    // we can retrieve the initial_state ID of the emitter (=1 or 2)
    // if the initial_state ID is 1, then the particle of the opposite beam is [1]
    // if the initial_state ID is 2, then the particle of the opposite beam is [0]
    // so we can take emitter().initial_state()%2 to select this particle
    Particle opposite_emitter = (*_event)[emitter().initial_state()%2];
    
    precision_type xbtilde = event().pdf_x(opposite_emitter);
    
    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalAntennapp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    const typename ShowerPanScaleLocalAntennapp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalAntennapp::EmissionInfo*>(emission_info_base));

    // construct the new PDF x-fractions
    precision_type x1, x2;
    Momentum current_Q;
    if(emitter().initial_state()==1){
      x1 = xatilde*emission_info.beam1_pdf_new_x_over_old;
      x2 = xbtilde*emission_info.beam2_pdf_new_x_over_old;
      current_Q = emitter()/x1 + opposite_emitter.momentum()/x2;
    }else{
      x1 = xbtilde*emission_info.beam1_pdf_new_x_over_old;
      x2 = xatilde*emission_info.beam2_pdf_new_x_over_old;
      current_Q = emitter()/x2 + opposite_emitter.momentum()/x1;
    }

    // do the boost
    if(!(boost_at_end())) ShowerPanScaleLocalAntennapp::Element::lorentz_boost_realign(current_Q);
    else ShowerPanScaleLocalAntennapp::Element::reset_beam_and_shower_Q(x1, x2, current_Q);

  }
  
  //======================================================================
  // ShowerPanScaleLocalAntennapp::ElementFF implementation
  //======================================================================
  double ShowerPanScaleLocalAntennapp::ElementFF::lnb_extent_const() const{
    return to_double(_log_dipole_m2 - 2.*_log_rho)/(1. + _shower->_beta);
  }

  Range ShowerPanScaleLocalAntennapp::ElementFF::lnb_generation_range(double lnv) const{  
    // this corresponds to Eq. (52 - 53) of 2018-07-notes.pdf (and to the soft limit used in the Sudakov)
    return Range(to_double(- _log_dipole_m2 - _half_ln_sj_over_sij_si + _log_rho + lnv ) / (1+_shower->_beta), 
                 to_double(- _half_ln_sj_over_sij_si                  - _log_rho - lnv ) / (1+_shower->_beta));
  }

  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerPanScaleLocalAntennapp::ElementFF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    
    //construct alphak, betak which is the same for every element
    ShowerPanScaleLocalAntennapp::Element::acceptance_probability(emission_info_base);
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalAntennapp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalAntennapp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalAntennapp::EmissionInfo*>(emission_info_base));

    // to prevent lambda1 to become negative
    // impose that ak and bk are smaller then 1
    // if they are larger, they will result in negative energies of our emitter/spectator
    // after rescaling them
    if((emission_info.alphak + emission_info.betak) >= 1) return false;

    // both are final state so za = ak, zb = bk
    precision_type za   = emission_info.alphak;
    precision_type zb   = emission_info.betak;
    
    // here we should make a decision based on flavour.
    //
    // pg (pq) corresponds to the emission of a gluon (quark)
    // In PanScales we pass z to the splitting weights
    _shower->fill_dglap_splitting_weights(emitter(), za,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    _shower->fill_dglap_splitting_weights(spectator(), zb,
                                          emission_info.spectator_weight_rad_gluon,
                                          emission_info.spectator_weight_rad_quark);

    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    emission_info.beam1_pdf_new_x_over_old = 1.0;
    emission_info.beam2_pdf_new_x_over_old = 1.0; 
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter   = za;
    emission_info.z_radiation_wrt_spectator = zb;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
    
    emission_info.emitter_weight_rad_gluon   *= emission_info.  f * normalisation_factor;
    emission_info.emitter_weight_rad_quark   *= emission_info.  f * normalisation_factor;
    emission_info.spectator_weight_rad_gluon *= emission_info.omf * normalisation_factor;
    emission_info.spectator_weight_rad_quark *= emission_info.omf * normalisation_factor;
    
    // in case this FF element will be accepted we do not need to update all elements in the dipole cache
    // set it to false explicitly because the other elements might have changed it to true
    if(!_shower->boost_at_end()) emission_info.full_store_update_required = false;
    
    return true;
  }

  //----------------------------------------------------------------------
  // carry out the splitting
  bool ShowerPanScaleLocalAntennapp::ElementFF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPanScaleLocalAntennapp!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalAntennapp::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalAntennapp::EmissionInfo*>(emission_info_base));
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    // mapping coefficients for radiated particle
    // ak=alphak, bk=betak since both are final-state
    precision_type ak = emission_info.alphak;
    precision_type bk = emission_info.betak;
    precision_type one_minus_ak = 1 - ak;
    precision_type one_minus_bk = 1 - bk;

    precision_type feta   = emission_info.  f;
    precision_type omfeta = emission_info.omf;
    precision_type akbk   = ak*bk;

    // lambda1 and lambda2 are defined below Eqs. (B.66a-c) in arXiv:2205.02237
    precision_type lambda1 = one_minus_ak - bk;
    precision_type lambda2 = lambda1 + 4 * feta * omfeta * akbk;

    precision_type s1 = sqrt(lambda1);
    precision_type s2 = sqrt(lambda2);

    // Eqs. (B.66a-c) in arXiv:2205.02237
    precision_type ai = (pow2(s1+s2) + 4 * pow2(feta)   * akbk) / (4 * (one_minus_bk));
    precision_type aj = (pow2(s1-s2) + 4 * pow2(omfeta) * akbk) / (4 * (one_minus_bk));
    precision_type bi = (pow2(s1-s2) + 4 * pow2(feta)   * akbk) / (4 * (one_minus_ak));
    precision_type bj = (pow2(s1+s2) + 4 * pow2(omfeta) * akbk) / (4 * (one_minus_ak));

    // build the perp vector
    precision_type norm_perp = sqrt(akbk*_dipole_m2);
    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    // store for spin, when using dir-diffs this get rotated to the right plane
    emission_info.k_perp = perp;

    // Fill the emitter, radiator and spectator 3-momenta
    // FSR so we need - signs for the kperp for the emitter and spectator
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() -   feta * perp.p3() ;
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() +          perp.p3() ;
    Momentum3<precision_type> spectator_out3 = aj * rp.emitter.p3() + bj * rp.spectator.p3() - omfeta * perp.p3() ;
    
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
    
    return true;  
  }  

} // namespace panscales
