//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerPanScaleLocalVincia.hh"

namespace panscales{

  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerPanScaleLocalVincia::elements(
    Event & event, int dipole_index) const {
    int iq    = event.dipoles()[dipole_index].index_q   ;
    int iqbar = event.dipoles()[dipole_index].index_qbar;
    std::unique_ptr<typename ShowerBase::Element> elms[] = {
      std::unique_ptr<typename ShowerBase::Element>(new ShowerPanScaleLocalVincia::Element(iq, iqbar, dipole_index, &event, this))
    };
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }

  //----------------------------------------------------------------------
  bool ShowerPanScaleLocalVincia::find_lnv_lnb_from_kinematics(const Momentum &p_emit, 
                                     const Momentum &p_spec, 
                                     const Momentum &p_rad, 
                                     const precision_type &p_emit_dot_p_spec,
                                     const precision_type &p_emit_dot_p_rad,
                                     const precision_type &p_spec_dot_p_rad,
                                     const Event &event,
                                     double &lnv, double &lnb) const {

    std::cerr << "find_lnv_lnb_from_kinematics not implemented" << std::endl;
    return false;
  }

  //----------------------------------------------------------------------
  
  Range ShowerPanScaleLocalVincia::Element::lnb_generation_range(double lnv) const {
    // this corresponds to Eq. (52 - 53) of 2018-07-notes.pdf (and to the soft limit used in the Sudakov)
    double lower_bound = to_double(log_T(_sitilde/_sjtilde*pow2(_rho)/_dipole_m2) + 2.*lnv)
      /2./(1. + _shower->_beta);
    double upper_bound = to_double(log_T(_sitilde/_sjtilde*_dipole_m2/pow2(_rho)) - 2.*lnv)
      /2./(1. + _shower->_beta);
    return Range(lower_bound,upper_bound);
  }
  
  double ShowerPanScaleLocalVincia::Element::lnv_lnb_max_density() const {
    // For now we work in a large-NC limit, with CF defined to be equal
    // to CA/2; the density that is returned here corresponds to that
    // for a quark or for half a gluon
    return _shower->max_alphas() * _shower->qcd().CA() / M_PI;
  }
  /// returns the jacobian for the real phase space of the first emission
  precision_type ShowerPanScaleLocalVincia::Element::dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const {
    // precision_type sitilde = 2.*dot_product(element.emitter(),   event.Q());
    // precision_type sjtilde = 2.*dot_product(element.spectator(), event.Q());
    // precision_type dipole_m2 = (element.emitter() + element.spectator()).m2();
    // precision_type Q2 = event().Q2();
    // precision_type rho;
    // if (_shower->beta() == 0) {
    //   rho = 1;
    // } else {
    //   rho = pow(sitilde*sjtilde/dipole_m2/Q2, precision_type(0.5 * _shower->beta()));
    // }
    precision_type kt = _rho * exp(precision_type(lnv + _shower->beta() * fabs(lnb)));
    precision_type ak = sqrt(_sjtilde/_sitilde/_dipole_m2) * kt * exp(precision_type(lnb));
    precision_type bk = pow2(kt)/_dipole_m2/ak;

    // Compute lambda1, lambda2, Eq. (8) from 2018-07-notes.pdf
    precision_type fv = lnb > 0 ? 1.0/(exp(-2*lnb) + 1.0) : exp(2*lnb)/(1.0 + exp(2*lnb));
    precision_type w = exp(lnv);
    precision_type lambda1 = (1.0 - ak - bk)/(ak * bk);
    precision_type lambda2 = lambda1 + 4*fv*(1-fv);

    precision_type sqrt_lambda = sqrt(lambda1*lambda2);

    // small ak expansion
    if (ak < numeric_limit_extras<precision_type>::sqrt_epsilon() && fabs(1-bk) > numeric_limit_extras<precision_type>::sqrt_epsilon()) {
      precision_type jac = -ak*bk*(-1+bk*fv)*(-2+2*bk*fv+bk/pow2(cosh(lnb)))/(-1+bk);
      return jac;
    }
    // small bk expansion
    if (bk < numeric_limit_extras<precision_type>::sqrt_epsilon() && fabs(1-ak) > numeric_limit_extras<precision_type>::sqrt_epsilon()) {
      precision_type jac = ak*bk*exp(-2*lnb)*(1+ak*(-1+fv))*(ak-2*ak*pow2(exp(lnb)+exp(3*lnb)*(-1+fv))-8*exp(4*lnb)*pow2(cosh(lnb)))/(-1+ak)/pow2(1+exp(2*lnb));
      return jac;
    }

    // formula in 2018-07-notes
    precision_type jac = 2.*fv*(1-fv)*pow(ak-bk,2)*(1-ak-bk)/w/sqrt_lambda/(1-ak)/(1-bk) -
      (1-2*fv)*ak*bk*(ak-bk)*(2-ak-bk)*(1-ak-bk)/w/pow(1-ak,2)/pow(1-bk,2) +
      pow(ak-bk,2)*pow(1-ak-bk,2)/w/sqrt_lambda/ak/bk/(1-ak)/(1-bk) +
      sqrt_lambda*ak*bk*(pow(ak,3)+ak*(4-3*bk)*bk-pow(bk,2)*(1-bk)+pow(ak,2)*(-1+bk*(-3+2*bk)))/w/pow(1-ak,2)/pow(1-bk,2) +
      (-ak*bk*(2-ak-bk)+(1-2*fv)*(ak-bk)*(1-ak-bk)/sqrt_lambda)*(ak+bk)/w/(1-ak)/(1-bk)*2*exp(2*lnb)/pow(1+exp(2*lnb),2);

    return jac*w;
  }

  //----------------------------------------------------------------------
  bool ShowerPanScaleLocalVincia::veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const {
    assert(false && "cannot use PanLocal-Vincia truncated.");
    return false;
  }
  
  //----------------------------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  /// which should be bounded between 0 and 1
  bool ShowerPanScaleLocalVincia::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocalVincia!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalVincia::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalVincia::EmissionInfo*>(emission_info_base));

    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;

    // Eq. (2) of arXiv:2002.11114
    precision_type kt = _rho * exp(precision_type(lnv + _shower->_beta * fabs(lnb)));

    // Eq. (3) of arXiv:2002.11114
    precision_type ak   = sqrt(_sjtilde/_sitilde/_dipole_m2) * kt * exp(precision_type(lnb));
    precision_type bk   = pow2(kt)/_dipole_m2/ak;

    // cache this info for future usage
    emission_info.kt = kt;
    emission_info.ak = ak;
    emission_info.bk = bk;

    // test phase space boundary (Eq. (42) of 2018-07-notes.pdf)
    if (ak + bk >= 1.0) {return false;}

    // handle the splitting functions at both ends based on the flavour
    // Note that for the gluon case, there's an extra factor 1/2
    // accounting for the fact that a gluon is split over 2 dipoles
    _shower->fill_dglap_splitting_weights(emitter(), ak,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    _shower->fill_dglap_splitting_weights(spectator(), bk,
                                          emission_info.spectator_weight_rad_gluon,
                                          emission_info.spectator_weight_rad_quark);

    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter   = ak;
    emission_info.z_radiation_wrt_spectator = bk;

    // normalisation factor
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();

    // This is an antenna shower, either the emitter of the spectator can split
    double f, omf;
    f_fcn(lnb, f, omf);
    
    emission_info.emitter_weight_rad_gluon   *=   f * normalisation_factor;
    emission_info.emitter_weight_rad_quark   *=   f * normalisation_factor;
    emission_info.spectator_weight_rad_gluon *= omf * normalisation_factor;
    emission_info.spectator_weight_rad_quark *= omf * normalisation_factor;
  
    return true;
  }
  //----------------------------------------------------------------------
  bool ShowerPanScaleLocalVincia::Element::check_after_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const {
    // We need to check phase-space before constructing the
    // kinematics for PanLocal-Vincia with matching on. Otherwise we run
    // into trouble when we call _do_kinematics() if momentum checks are on.

    // retrieve emission info
    typename ShowerPanScaleLocalVincia::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalVincia::EmissionInfo*>(emission_info_base));
    // retrieve cached info
    const precision_type & ak = emission_info.ak;
    const precision_type & bk = emission_info.bk;
    // (Eq. (42) of 2018-07-notes.pdf)
    if (ak + bk > 1.0) {return false;}

    return true;
  }


  //----------------------------------------------------------------------
  bool ShowerPanScaleLocalVincia::Element::do_kinematics(
    typename ShowerBase::EmissionInfo * emission_info_base,
    const RotatedPieces & rp
  ) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is not that of ShowerPanScaleLocalVincia!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPanScaleLocalVincia::EmissionInfo & emission_info = *(static_cast<typename ShowerPanScaleLocalVincia::EmissionInfo*>(emission_info_base));
    
    // retrieve cached info
    const double & lnb = emission_info.lnb;
    const double & phi = emission_info.phi;

    const precision_type & kt = emission_info.kt;
    const precision_type & ak = emission_info.ak;
    const precision_type & bk = emission_info.bk;

    // Compute lambda1, lambda2, Eq. (14a) in arXiv:2002.11114
    precision_type fv = f_fcn(lnb);
    precision_type lambda1 = (1.0 - ak - bk)/(ak * bk);
    precision_type lambda2 = lambda1 + 4*fv*(1-fv);
  
    // compute ai, bi, aj, bj using Eq. (13a-d) in arXiv:2002.11114
    precision_type sqrtLambda1 = sqrt(lambda1);
    precision_type sqrtLambda2 = sqrt(lambda2);
    precision_type sqrPlus = (sqrtLambda1 + sqrtLambda2)*(sqrtLambda1 + sqrtLambda2);
    precision_type sqrMinus = (sqrtLambda1 - sqrtLambda2)*(sqrtLambda1 - sqrtLambda2);
    precision_type ai = (sqrPlus  + 4*fv*fv)*ak*bk/(4.0*(1.0 - bk));
    precision_type bi = (sqrMinus + 4*fv*fv)*ak*bk/(4.0*(1.0 - ak));
    precision_type aj = (sqrMinus + 4*(1.0-fv)*(1.0-fv))*ak*bk/(4.0*(1.0 - bk));
    precision_type bj = (sqrPlus  + 4*(1.0-fv)*(1.0-fv))*ak*bk/(4.0*(1.0 - ak));
  
    // for cases with a plane, phi=0 means in-plane
    Momentum perp = kt * (sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    emission_info.k_perp = perp;

    
    // Use Momentum3 class to avoid issues with branchings at small invariant mass
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() + fv*perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> spectator_out3 = aj * rp.emitter.p3() + bj * rp.spectator.p3() + (1.0-fv)*perp.p3();

    // Now construct the final 4-momenta by imposing the masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    return true;
  }
} // namespace panscales
