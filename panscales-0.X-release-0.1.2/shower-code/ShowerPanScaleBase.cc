//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerPanScaleBase.hh"

namespace panscales{
  ShowerBase::EmissionInfo* ShowerPanScaleBase::create_emission_info() const{ return new ShowerPanScaleBase::EmissionInfo(); }

  //----------------------------------------------------------------------
  void ShowerPanScaleBase::Element::update_kinematics() {
    if (_shower_panscalebase->use_diffs()) {
      _dipole_m2 = 2*dot_product_with_dirdiff(emitter(),spectator(),
                                              dipole().dirdiff_3_minus_3bar);
    } else {
      _dipole_m2 = (emitter() + spectator()).m2();
    }
    _log_dipole_m2 = log(_dipole_m2);
    _sitilde = 2.*dot_product(emitter(),_event->Q());
    _sjtilde = 2.*dot_product(spectator(),_event->Q());
    _Q2      = _event->Q2();
    // Eq. (2) of 2018-07-notes.pdf
    if (_shower_panscalebase->_beta == 0) {
      _rho     = 1.0;
      _log_rho = 0.0;
    } else {
      // The line below requires an explicit cast to
      // the floating type T because for dd_real (and possibly qd_real)
      // pow(dd_real, double) ends up calling pow(dd_real, int)
      // which is definitely not what we want!
      _rho = pow(_sitilde*_sjtilde/_dipole_m2/_Q2, precision_type(0.5 * _shower_panscalebase->_beta));
      _log_rho = log(_rho);
    }
    _half_ln_sj_over_sij_si = 0.5 * log(_sjtilde/_sitilde/_dipole_m2);
  }


  void ShowerPanScaleBase::Element::print_characteristics(){
       std::cout << " _dipole_m2 " << _dipole_m2 << " _sitilde " << _sitilde << " _sjtilde " << _sjtilde << " _rho " << _rho <<  " norm_a " << norm_a() << " norm_b " << norm_b() << " norm_astar " << norm_astar() << std::endl;
  }

  /// #TRK_ISSUE-344  Not clear if this is ever used
  void ShowerPanScaleBase::Element::update_indices(unsigned emitter_index, unsigned spectator_index) {
    _emitter_index   = emitter_index;
    _spectator_index = spectator_index;
    update_kinematics();
  }

  //----------------------------------------------------------------------
  // for a given lnv,lnb, returns approximate lnkt in the
  // emitter-spectator dipole rest frame, which is equivalent to the
  // frame-independent lnkt wrt to the emitter-spectator system.
  // This lnkt will be correct in the soft+collinear limit but may
  // differ from the true lnkt in the soft large-angle limit and the
  // hard-collinear limit
  double ShowerPanScaleBase::Element::lnkt_approx(double lnv, double lnb) const {
    // Implement Eq.(69) from 2019-07-02-obs-dep-dyn-cut-off
    if (_shower_panscalebase->beta() == 0) return lnv;
    else return lnv + _shower_panscalebase->beta() * abs(lnb) + to_double(log_T(_rho));
  }

  // for a given lnv,lnb, returns the pseudorapidity with respect to
  // the closer of the emitter/spectator, defined in the event
  // centre-of-mass frame. This eta will be correct in the
  // soft+collinear limit but may differ from the true eta in the
  // soft large-angle limit and the hard-collinear limit.
  double ShowerPanScaleBase::Element::eta_approx (double lnv, double lnb) const {
    // Implement Eqs.(77,78) from 2019-07-02-obs-dep-dyn-cut-off,
    // Start off with an lnb-independent constant term
    double result = -to_double(0.5*log(_dipole_m2*_Q2/_sitilde/_sjtilde));
    // Give it a sign according to whether we are along the emitter (lnb>0)
    // of "spectator" (lnb<0) (this distinction doesn't really hold for antenna
    // showers, but the signing is kept the same)
    if (lnb < 0) result = -result;
    // then add the lnb term (whose sign coincides with the sign we want
    // for the overall eta)
    result += lnb;
    return result;
  }

    precision_type ShowerPanScaleBase::Element::double_soft_weight_shower(int history, 
                                                int iflav1, const Momentum pi, 
                                                const Momentum pj, const Momentum pk, 
                                                const Momentum pl) const {

    assert(false && "cannot use this shower with double soft.");
    return 0.0;                                              
  }

  void ShowerPanScaleBase::Element::double_soft_find_pi_pj_pk_pl(typename ShowerBase::EmissionInfo * emission_info, 
                                                                 Momentum & pi, Momentum & pj,
                                                                 Momentum & pk, Momentum & pl,
                                                                 int & iflav, bool &iklj) const {
    assert(false && "cannot use this shower with double soft.");
    return; 
  }

  double ShowerPanScaleBase::Element::alphas2_coeff(const typename ShowerBase::EmissionInfo * emission_info) const {
    const auto & qcd = _shower_panscalebase->qcd();
    if (qcd.nloops() <= 1) return 0;
    else {
      // 
      return 2 * qcd.b0() * qcd.lnxmuR() * to_double(1.0 - emission_info->z_radiation_wrt_splitter());
      // 
      // #TRK_ISSUE-347  // the following lines only works for PanGlobal, because for PanLocal z_radiation_wrt_spectator=0.
      // // They are intended to work around the fact that in the transition region between the two
      // // dipole halves, using the "splitter" can give a small z, when there would actually be a larger
      // // z from the other side.
      // double z = std::max(emission_info->z_radiation_wrt_emitter, emission_info->z_radiation_wrt_spectator);
      // return 2 * qcd.b0() * qcd.lnxmuR() * (1.0 - z);
    }
  }


} // namespace panscales
#include "autogen/auto_ShowerPanScaleBase_global-cc.hh"
