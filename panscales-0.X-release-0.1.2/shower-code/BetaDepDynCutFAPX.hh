//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __BETA_DEP_DYN_CUT_FAPX_HH__
#define __BETA_DEP_DYN_CUT_FAPX_HH__

#include "ShowerRunner.hh"
#include "EmissionVeto.hh"

namespace panscales{
//----------------------------------------------------------------------
/// @ingroup emveto_classes
/// \class BetaDepDynCutFAPX
/// class for dynamically-vetoing emissions based on a Fast Approximation 
/// of the observable. The idea is to use a quick-estimate of the
/// observable to decide whether a given emission will contribute to it or
/// not. If not, the generated values of lnv and lnb are vetoed. 

template <class Shower>
class BetaDepDynCutFAPX : public EmissionVeto<Shower> {
//
public:

  /// Default ctor
  BetaDepDynCutFAPX(double beta,
                    double veto_buffer,
                    bool   enhanced_emissions,
                    double secondary_eta_margin,
                    double ln_obs_max_target = +std::numeric_limits<double>::max(),
                    double ln_obs_min_target = -std::numeric_limits<double>::max()
                    )
    : EmissionVeto<Shower>(), _beta(beta), _veto_buffer(veto_buffer),
      _enhanced_emissions(enhanced_emissions),
      /// |η| Margin factor in deciding whether an emission is a secondary.
      /// Choosing 0 here gave biased results. In particular, for PanGlobal
      /// β_ps = 0.5, β_obs = 0.0, for αS = 0.0025 we expected, from before,
      /// for a target window of λ_obs,approx < -0.5 
      /// Σ(λ = -0.500)  =   2.860e-66   +-   1e-69 , but we got
      /// Σ(λ = -0.500)  =   2.880e-66   +-   7e-69 , almost 3σ off.
      /// Setting to 4 units of rapidity gave
      /// Σ(λ = -0.500)  =   2.852e-66   +-   8e-69 , ~1σ off.
      /// Avg. multiplicity with no secondary vetoing was 85. Secondary
      /// vetoing with zero margin reduced that to 45. Secondary vetoing
      /// with margin set to 4 units of |η| had avg. multiplicity 67.
      _ln_obs_max_target(ln_obs_max_target), _ln_obs_min_target(ln_obs_min_target),
      _secondary_eta_margin(secondary_eta_margin) {
         assert(veto_buffer < 0.0); 
         _init_element_enhancement_factor = 1.0;
      }

  /// Reset anything that needs initialisation on an event-by-event basis.
  virtual void initialise(Event & event, double lnv) override {

    /// Max change in the approximate observable so far produced by an emission.
    _ln_obs_max_change = -std::numeric_limits<double>::max();

    /// Log of the COM energy of the event.
    _lnQ = 0.5 * to_double(log( event.Q2() ));

    /// This monitors the lnv value at which we last passed through the
    /// elements with update_element_in_store_pre_splitting.
    _last_searched_lnv_pre_splitting = lnv;

    /// Monitors event multiplicity, in the first case to detect splittings.
    _n_particles = event.size();

    /// Initial two particles have ∞ rapidity separation w.r.t each other.
    _abs_eta_approx_NN_list.clear();
    _abs_eta_approx_NN_list.push_back(std::numeric_limits<double>::infinity());
    _abs_eta_approx_NN_list.push_back(std::numeric_limits<double>::infinity());

  }

  /// In this approach the emission veto happens in this piece of code, before
  /// do_split and the acceptance probability are computed, or not at all.
  /// This function returns true if the emission is to be vetoed.
  /// It sets lnv to -std::numeric_limits<double>::max() if the
  /// event as a whole is to be terminated.
  virtual WeightedAction pre_accept_prob_veto(const Event & event,
                                              const typename Shower::EmissionInfo & emission_info,
                                              Weight& weight, bool is_global=false) override {

    /// Terminate the shower if it evolves such that the phase space
    /// surrounding the observable-dominating emission, in the Lund,
    /// plane is all covered, including by a sizeable buffer region.
    const auto & element = *(emission_info.element());
    double lnv = emission_info.lnv;
    double lnb = emission_info.lnb;

    double betaPS    = element.betaPS();
    double betaOBS   = _beta;

    /// If β_ps > β_obs, the relevant place to compare shower and
    /// observable is at the collinear boundary, so we translate both
    /// the observable and the shower values into kt values and use
    /// the latter to determine whether to terminate the showering.
    /// For β_ps < β_obs, the relevant place to compare the shower
    /// and observable contours is just η=0.
    double preFacPS  = ( betaPS > betaOBS ) ? 1. / (1 + betaPS  ) : 1.;
    double preFacOBS = ( betaPS > betaOBS ) ? 1. / (1 + betaOBS ) : 1.;
    if(  _ln_obs_max_change > -std::numeric_limits<double>::max() && 
         preFacPS  * ( lnv - _lnQ )
       < preFacOBS * ( _ln_obs_max_change - _lnQ + _veto_buffer ) ) {
      //lnv = -std::numeric_limits<double>::max();
      return EmissionAction::terminate_shower;
    }

    /// Change in approx observable is v = kt exp(-β|η|) / Q of emitted parton,
    /// with kt & η measured relative to emitter, defined as closest (in angle)
    /// parton in event frame.

    /// Compute approx of emission's contribution to the observable used for the veto.
    _current_abs_eta_approx = element.eta_approx(lnv,lnb);
    bool from_emitter = _current_abs_eta_approx >= 0 ? true : false;
    _current_abs_eta_approx = fabs(_current_abs_eta_approx); 

    /// Veto secondary emissions.
    double _abs_eta_approx_NN = from_emitter ? 
                                _abs_eta_approx_NN_list[element.emitter_index()]
                              : _abs_eta_approx_NN_list[element.spectator_index()];
    if( _current_abs_eta_approx > _abs_eta_approx_NN + _secondary_eta_margin )
      return EmissionAction::veto_emission;

    /// Compute overestimated contribution to the observable from proposed emission.
    _ln_obs_change = element.lnkt_approx(lnv,lnb) - _beta*_current_abs_eta_approx;

    /// If log(δ(obs)) < log(Max(δ(obs))) + _veto_buffer veto the emission.
    if(_ln_obs_change < _ln_obs_max_change + _veto_buffer)
      return EmissionAction::veto_emission;

    /// If a target region has been set and the emission is down inside it
    /// with an enhancement factor attached, we can either correct for that
    /// enhancement by acc/rej here or downstairs after it passes all other
    /// acc/rej vetoes in post_do_split_veto (actually we can do this at any
    /// point from here to there). This particular emission veto step commutes
    /// with all others (unlike the reweighting step), because it's limited
    /// within the target region. For large emission enhancements, the rejection
    /// rate is huge. It therefore makes way more sense to do it up here before
    /// all the other, more complicated/time-consuming, acc/rej steps in the 
    /// veto algorithm that follow it.
    if( _ln_obs_max_target < std::numeric_limits<double>::max() &&
        _ln_obs_change <= _ln_obs_max_target &&
        _init_element_enhancement_factor > 1. &&
        !gsl.accept(1./_init_element_enhancement_factor) ) return EmissionAction::veto_emission;

    /// Return to ShowerRunner: true = reject emission, false = keep emission.
    return EmissionAction::accept;
  }

  /// In this approach the emission veto either happens before do_split and the
  /// acceptance probability are computed, or not at all. But we may need to
  /// update _ln_obs_max_change.
  virtual WeightedAction post_do_split_veto(const Event & event,
                                            const typename Shower::EmissionInfo & emission_info,
                                            Weight& weight, bool is_global = false) override {

    // If the emission isn't vetoed & made it through to this point it is 
    // a AAA, bona fide, accepted, possibly enhanced, shower emission. If
    // there is no further rejection from dynamic cuts (below here) or
    // rejection for enhancement compensation (also below here) the event
    // will be updated with the emission after this function call.

    // Note _ln_obs_max_change is initialised for every event to be impossibly
    // small (-std::numeric_limits<double>::max()), only acquiring a real,
    // physical, value below (when an emission is bound to be generated).

    // If the approx. observable is above the target region either the whole
    // event or the emission are going to get vetoed.
    if(  _ln_obs_change > _ln_obs_max_target ) {
      /// If we DO NOT have an enhanced emission rate ...
      if(_init_element_enhancement_factor<=1.) {
        // ... we DISCARD the WHOLE EVENT. This is achieved by simply
        // replacing the event with an empty event here. ShowerRunner::run
        // checks for empty events if this function returns true, and 
        // breaks out of the shower evolution loop.
        //event = Event();
        return EmissionAction::abort_event;
        // If we DO have an enhanced emission rate ...
      } else {
        // ... we just DISCARD the EMISSION and REWEIGHT the EVENT event.
        // ShowerRunner::run sees the return value is true and keeps evolving
        // down from this lnv.
        weight *= (1.-1./_init_element_enhancement_factor);
        return EmissionAction::veto_emission;
      }
    /// If the approx. observable is below the upper edge of the target region ...
    } else {
      /// ... the emission is going to get constructed, so we may need to update
      /// the max value of the approx. observable found so far, before returning
      /// false (i.e. signal ShowerRunner::run to go on and build the emission).
      /// N.B. we already rejected down for the enhancement factor of this emission
      /// way back in pre_accept_prob_veto.
      if(_ln_obs_change > _ln_obs_max_change) _ln_obs_max_change = _ln_obs_change;
      /// Since the emission will be constructed we also update the local list
      /// of particle |η,NN|'s .
      _abs_eta_approx_NN_list.push_back(_current_abs_eta_approx);

      return EmissionAction::accept;
    }

  }

  /// reject the event 
  virtual WeightedAction final_event_veto(const Event & event) const override {
    if (_ln_obs_max_change < _ln_obs_min_target) return EmissionAction::abort_event;
    else                                         return EmissionAction::accept;
  }

  // If we are not running in a mode where emission rates for individual
  // elements have been modified, we have no associated book-keeping to
  // do hence if(!_enhanced_emissions) return false; . Otherwise, this
  // function signals whether to call update_element_in_store_pre_splitting
  // for all elements, if lnv has evolved by more than 1 unit since 
  // last we called it, to see whether we want to alter those emission
  // rates. This will happen just after find_element_alg_ptr has run to
  // select a tentative lnv and element.
  virtual bool modify_elements_pre_splitting(Event & event, 
    typename Shower::Element & element, const double & lnv) override{

    // No artificial enhancement of individual emission rates, hence 
    // no associated modifications/updates are ever needed.  
    if(!_enhanced_emissions) return false;

    // Otherwise, we are running in a mode where individual emission
    // rates have been altered w.r.t the norm. We need to get hold of
    // the element's emission factor, as it was in the generation of
    // the incoming lnv. We need to decide whether to review element
    // enhancement factors too.
    
    // Save current emission enhancement factor of the element selected
    // by find_element_alg_ptr, s.t. we can reweight consistently with it
    // should it make it as far as postDoSplitVeto.
    _init_element_enhancement_factor = element.enhancement_factor();

    // If there was a branching, this is a queue that we may want to 
    // edit enhancement factors.
    if(_n_particles < event.size()) {
      _last_searched_lnv_pre_splitting = lnv;
      _n_particles = event.size();
      return true;
    }

    // Decide whether to scan over all elements to update them or not.
    if( std::abs(_last_searched_lnv_pre_splitting - lnv) > 1. ) {
      _last_searched_lnv_pre_splitting = lnv;
      return true;
    } else {
      return false;
    }
  }

  // Enable modification of elements just after the find_element_alg_ptr
  // has run to select a tentative lnv and element. 
  virtual bool modify_element_pre_splitting(Event & event, 
    typename Shower::Element & element, const double & lnv) override{

    // Flag signalling back to the main code whether we modified the element.
    bool update_element_in_store = false;

    // Think about modifying the element's emission rate. 
    if(element.enhancement_factor()>1.) {
      double beta_ps  = element.betaPS();
      double beta_obs = _beta;
      double lnObs_max;
      if(beta_ps<=beta_obs) {
        double lnb_for_min_abseta = element.lnb_for_min_abseta(lnv);
        double eta_lnb_for_min_abseta  = element.eta_approx (lnv,lnb_for_min_abseta);
        double lnkt_lnb_for_min_abseta = element.lnkt_approx(lnv,lnb_for_min_abseta);
        lnObs_max = lnkt_lnb_for_min_abseta - beta_obs*fabs(eta_lnb_for_min_abseta);
        //
      } else {
        double lnb_min = element.lnb_generation_range(lnv).min();
        double lnb_max = element.lnb_generation_range(lnv).max();
        double lnb_for_max_abslnb = fabs(lnb_max) > fabs(lnb_min) ? lnb_max : lnb_min;
        double eta_lnb_for_max_abslnb  = element.eta_approx (lnv,lnb_for_max_abslnb);
        double lnkt_lnb_for_max_abslnb = element.lnkt_approx(lnv,lnb_for_max_abslnb);
        lnObs_max = lnkt_lnb_for_max_abslnb - beta_obs*fabs(eta_lnb_for_max_abslnb);
      }
      if(lnObs_max < _ln_obs_max_target) {
        element.enhancement_factor(1.);
        update_element_in_store = true;
      }
    }
    return update_element_in_store;
  }

protected:

  /// β in the definition of the observable we want to cut in: obs = kt exp(-β|η|).
  double _beta;

  /// Buffer size: intended to have the same meaning as dynamic_lncutoff
  /// in ShowerRunner::run. There emissions are vetoed (shower is halted) if
  /// lnv < lnvfirst + dynamic_lncutoff. Note dynamic_lncutoff must be -ve and has
  /// a recommended value of -18 corresponding to, basically, v < vfirst * 10^-8.
  /// Analogously here we have _obsChange < _obsMaxChange + _veto_buffer.
  double _veto_buffer;

  /// Indicates whether we are running in a mode where individual emissions
  /// have artificially modified emission rates of not.
  double _enhanced_emissions;

  /// Log of COM energy of the event.
  double _lnQ;

  /// Approx contribution to veto-observable from current proposed branching.
  double _ln_obs_change;

  /// Max instance of _ln_obs_change found for successfully constructed branchings.
  double _ln_obs_max_change;

  /// Max value of log(v,observable) that is of interest to us. We will use
  /// this to terminate evolution if the approximate value of the observable
  /// exceeds it. In setting this target one should be aware that approximations
  /// are being used, and so it should include some `safety buffer', i.e. it
  /// should be set ~10 or so units of log(v,observable) above where you 
  /// are seeking reliable results. For smaller |log(v,observable)| targets,
  /// a smaller buffer may suffice (but such runs typically aren't time
  /// constrained anyway).
  double _ln_obs_max_target;
  double _ln_obs_min_target;

  /// The lnv value at which we last swept through all the elements to
  /// see whether or not we should modify their enhancement factors.
  double _last_searched_lnv_pre_splitting;

  /// Monitors event multiplicity, in the first case to detect splittings.
  unsigned int _n_particles;

  /// When find_element_alg_ptr runs to select a tentative lnv and
  /// element this variable holds the enhancement factor associated
  /// to the element in that process. The purpose of this variable
  /// to save that variable, s.t. _if_ the enhancement factor is 
  /// subsequently modified by modify_element_in_store_pre_splitting
  /// we are still able to access the original value used to generate
  /// the current lnv in the first place, as it is that value which
  /// is required in order to consistently apply the reweightings 
  /// and modified acceptances within postDoSplitVeto. 
  double _init_element_enhancement_factor;

  /// List of |η_approx| values for each emitted particle. η_approx
  /// is the η w.r.t the closer of the emitter and spectator in the
  /// dipole that produced the particle. I.e. this is a list of approx
  /// η's for each particle w.r.t it's nearest neighbour in η.
  std::vector<double> _abs_eta_approx_NN_list;

  /// This is a local copy of the |η_approx| of the emission, which we used
  /// in potentially vetoing the trial emission, put in this wider scope to
  /// facilitate updates of _abs_eta_approx_NN for fully accepted emissions.
  double _current_abs_eta_approx;

  /// A margin of error used in deciding whether an emission is a secondary.
  /// If an emission has |η_approx| > |η_approx,NN| + _secondary_eta_margin,
  /// where |η_approx,NN| is the angle between the particle that is trying
  /// to emit the current emission and the closest thing in η to it, the trial
  /// emission is a secondary.
  double _secondary_eta_margin;

};


} // namespace panscales

#endif  // __BETA_DEP_DYN_CUT_FAPX_HH__
