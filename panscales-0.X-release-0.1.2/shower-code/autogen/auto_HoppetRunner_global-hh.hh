#pragma once
// auto-generated helpers for code in HoppetRunner.hh (category global-hh)
// generated by: ../scripts/augment-panscales-enums.py
// (do not edit this file directly)


// auto-generated code for enum panscalesPDFChoice
namespace panscales {

  // auto-generated code for outputting enum PDFChoice as string
  std::ostream& operator<<(std::ostream& os, HoppetRunner::PDFChoice value);

  // auto-generated code for reading enum PDFChoice from string
  std::istream& operator>>(std::istream& is, HoppetRunner::PDFChoice& value);
} // namespace panscales


