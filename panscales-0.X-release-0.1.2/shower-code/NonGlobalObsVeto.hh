//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __NON_GLOBAL_OBS_VETO_HH__
#define __NON_GLOBAL_OBS_VETO_HH__

#include "EmissionVeto.hh"

namespace panscales{

//--------------------------------------------------------------------------------------
// EmissionVeto for NGLs: veto emissions at small angles w.r.t dipole ends in event COM.
//--------------------------------------------------------------------------------------
template <class Shower>
class NonGlobalObsVeto : public EmissionVeto<Shower> {
//
public:

  /// Default ctor
  NonGlobalObsVeto(double eta_max) : EmissionVeto<Shower>(), _eta_max(eta_max) {
    assert(eta_max > 0.0);
  }

  std::string description() const override {
    std::ostringstream ostr;
    ostr << "NonGlobalObsVeto with eta_max = " << _eta_max; 
    return ostr.str();
  }

  /// Reset anything that needs initialisation on an event-by-event basis.
  virtual void initialise(Event & event, double lnv) override { return; }

  /// In this approach the emission veto happens in this piece of code, before
  /// do_split and the acceptance probability are computed, or not at all.
  /// This function returns true if the emission is to be vetoed.
  /// It sets lnv to -std::numeric_limits<double>::max() if the
  /// event as a whole is to be terminated.
  virtual WeightedAction pre_accept_prob_veto(const Event & event,
                                              const typename Shower::EmissionInfo & eminfo,
                                              Weight & weight, bool is_global = false) override {

    // Skip the emission if it's too collinear to the dipole ends in the event COM.
    return fabs(eminfo.element()->eta_approx(eminfo.lnv, eminfo.lnb)) > _eta_max
      ? EmissionAction::veto_emission : EmissionAction::accept;
  }

protected:
  /// Emissions with |η_approx| > _eta_max are vetoed in the shower.
  double _eta_max;
};


} // namespace panscales

#endif  // __NON_GLOBAL_OBS_VETO_HH__
