//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef __COLLIDER_HH__
#define __COLLIDER_HH__

#include <string>
#include <vector>

namespace panscales {

//----------------------------------------------------------------------
/// \class Collider
/// class that holds information on the colliding beam particles, that
/// can either be: e+e-, pp, DIS or VBF.  
class Collider {
public:

  /// structure to hold the type of collider
  enum ColliderType {
    epem,  ///< e+e- machine
    pp,    ///< pp machine
    dis    ///< e+p/e-p machine or VBF, which is treated as DIS^2
  };

  /// @brief default constructor to allow other classes to own this uninitialised
  Collider() {};

  /// @brief Constructor of the class
  /// @param collider_type type of collider (e+e-, pp or DIS)
  /// @param rts CM energy of the colliding beams
  Collider(ColliderType collider_type, double rts) : _collider_type(collider_type), _rts(rts){
    if(collider_type == ColliderType::epem) _has_pdfs = false;
    else                                    _has_pdfs = true;
  }

  /// 
  ~Collider() {};

  /// access to the sqrt(S) energy of the colliding beams
  double rts() const {return _rts;}
  
  /// return the type of the collider
  ColliderType type() const {return _collider_type;}

  /// returns whether we need to use pdfs
  bool has_pdfs() const {return _has_pdfs;}

private:

  ColliderType _collider_type; ///< type of collider
  bool         _has_pdfs;      ///< determines whether PDFs are needed for the beams
  double       _rts;           ///< CM energy of the colliding beams
#include "autogen/auto_Collider_Collider.hh"
}; // end class Collider

} // namespace

#include "autogen/auto_Collider_global-hh.hh"
#endif //__COLLIDER_HH_
