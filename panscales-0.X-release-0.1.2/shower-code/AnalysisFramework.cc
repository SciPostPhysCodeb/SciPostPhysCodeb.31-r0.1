//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "AnalysisFramework.hh"
#include "ShowerFromCmdLine.hh"
#include "config.hh"
#include "LimitedWarning.hh"
#include "git.hh"

// Make FPE trapping functionality conditional, because it only seems
// to work on linux machines.
#ifdef __linux
#define PANSCALES_FPETRAP 
#endif // linux

#ifdef PANSCALES_FPETRAP
// trapping FPE: http://jayconrod.com/posts/33/trapping-floating-point-exceptions-in-linux
//#define _GNU_SOURCE
#include <fenv.h>
#endif // PANSCALES_FPETRAP

using namespace std;
using namespace chrono;
using namespace panscales;

/// @brief return a string with "in" character replaced by "out"
/// @param input the input string
/// @param in    character to replace
/// @param out   character with which to replace it
/// @return string with the replace
string str_replace(const string &input, char in, char out) {
  string output = input;
  for(char & c: output) {
    if (c == in) c = out;
  }
  return output;
}

/// @brief split string at the given character
/// @param input input string
/// @param split character on which to do the split
/// @return vector of split strings
vector<string> str_split(const string & input, char split) {
  vector<string> result;
  string::size_type pos = 0;
  while (true) {
    string::size_type oldpos = pos;
    pos = input.find(split,pos);
    if (pos == string::npos) {
      result.push_back(input.substr(oldpos));
      break;
    } else {
      result.push_back(input.substr(oldpos,pos));
      pos++;
    }
  }
  return result;
}

LimitedWarning AnalysisFramework::_badMom;

AnalysisFramework::AnalysisFramework(CmdLine * cmdline_in,
                                     const string & default_output_filename,
                                     bool return_after_git_info) :
#ifdef GIT_WATCHER
                        AnalysisBase(cmdline_in, default_output_filename) 
#else
                        AnalysisBase(&(cmdline_in->set_git_info_enabled(false)), default_output_filename) 
#endif
{
  header << precision_header();
  if (GitMetadata::Populated()) {
    header << "# libpanscales compiled git info: " << GitMetadata::CommitSHA1() 
           << ", " << GitMetadata::CommitDate();
    if (GitMetadata::AnyUncommittedChanges()) {
      header << " + UNCOMMITTED CHANGES: " << GitMetadata::StatusUNO();
    }
  } else {
    header << "# libpanscales git info missing";
  }
  header << endl;
  if (return_after_git_info) return;

  hists_err["ln_multiplicity"].declare(0.0,20.0,0.5);
  cmdline->section("Run steering");
  f_timingInfo = ! cmdline->present("-no-timing-info").help("turn off output of timing information");


  // choose the random-number generation seed
  auto seed = cmdline->any_optional_value<unsigned>({"-rseq","-seed"})
                      .help("Random number generator seed/sequence").argname("seed");
  cmdline->end_section("Run steering");
          
  if (seed.present()) {
    gsl.set(seed.value());
    header << "# seed = " << seed << endl;
  } else {
    header << "# seed = default" << endl;
  }
  
  
  // the following only works on linux systems
#ifdef PANSCALES_FPETRAP
  if (cmdline->present("-trap-fpe")) {
    // GPS 2019-04-09: we do not necessarily want to trap on underflow
    feenableexcept(FE_INVALID | FE_DIVBYZERO);
    //feenableexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW);
    //feenableexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW);
    //feraiseexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW);
  }
  // mac alternatives (not tested):
  // https://stackoverflow.com/questions/247053/enabling-floating-point-interrupts-on-mac-os-x-intel
  // http://www-personal.umich.edu/~williams/archive/computation/fe-handling-example.c
#endif // PANSCALES_FPETRAP

  // determine the collider type
  panscales::Collider::ColliderType collider_type;
  cmdline->start_section("Process selection");
  panscales::ProcID hard_procces_ID = cmdline->value("-process", ProcID::ee2qq).argname("procid")
    .help("The underlying hard process to generate (by default most "
          "of these involve fixed kinematics and a single Born flavour). "
          "There are several process-specific options, run '`-process proc -h`' to see the list.")
    .choices(panscales::ProcID_values(), panscales::ProcID_doxygen());
  // from the process ID we determine the collidertype
  if( hard_procces_ID == ProcID::pp2Z  || 
      hard_procces_ID == ProcID::pp2H  || 
      hard_procces_ID == ProcID::pp2Xj || 
      hard_procces_ID == ProcID::pp2jj ){ //< pp collider
    collider_type = panscales::Collider::ColliderType::pp;
  } else if (hard_procces_ID == ProcID::DIS || hard_procces_ID == ProcID::VBF){ //< dis process
    collider_type = panscales::Collider::ColliderType::dis;
  } else { //< default: collider is e+e- machine
    collider_type = panscales::Collider::ColliderType::epem;
  }


  // get the CM energy of the collider
  f_rts = cmdline->value("-rts",1.0).argname("rts").help(
         "Center of mass energy for pp, ee and DIS processes");
  cmdline->end_section("Process selection");
  // set up the collider
  f_collider.reset(new Collider(collider_type, f_rts));

  // set up the shower runner instance
  f_shower_runner.reset(create_showerrunner_with_shower_and_qcd_instance(*cmdline, header, *f_collider));
  
  // set properties of the showering
  cmdline->start_section("Showering properties");
  f_lnvmax   = cmdline->value("-lnvmax", log(f_rts)).argname("lnvmax").help("set the maximum lnv value");
  if (f_shower_runner->shower()->qcd().physical_coupling()) {
    double beta = f_shower_runner->shower()->beta();
    // lnkt_cutoff is really the renormalisation scale of the 
    // coupling below which it is set to zero
    double lnmuR_cutoff = f_shower_runner->shower()->qcd().lnkt_cutoff();
    // include the lnxmuR factor here, because the shower will need to go
    // further down in lnv if we are probing the coupling at lnmur = lnkt + lnxmuR
    // (keeping in mind that lnkt_cutoff, is really the smallest value of lnmur
    // for which the coupling is non-zero, so if lnxmuR > 0, we need to reach
    // physical lnkt values that are below the lnmur_cutoff)
    f_lnvrange = (1+beta) * (f_lnvmax - lnmuR_cutoff + std::max(0.0, f_shower_runner->shower()->qcd().lnxmuR()));
  } else {
    f_lnvrange = 10;  
  }
  f_lnvrange = cmdline->value("-lnvrange", f_lnvrange).argname("lnvrange").help("set the extent (range) of lnv values (should be positive)");
  f_lnvmin   = cmdline->value("-lnvmin", f_lnvmax - f_lnvrange).argname("lnvmin").help("set the lower limit of lnv range");
  f_lnvrange = f_lnvmax - f_lnvmin;
  f_max_emsn = cmdline->value("-max-emsn", -1).argname("max").help("stop showering after max(< 0 is unlimited)");
  f_first_order           = cmdline->present("-first-order").help("generate first-order (tree-level) weighted events");
  f_second_order          = cmdline->present("-second-order").help("generate second-order (tree-level) weighted events");
  f_second_order_lnb_window = cmdline->any_value({"-second-order-lnb-window","-second_order_lnb_window"}, -1.0).choices({-1.0})
                                      .help("lnb window for second order events. NOT CURRENTLY SUPPORTED");
  auto injection = cmdline->optional_value<string>("-second-order-injection").argname("injection-string")
    .help("Inject emissions prior to the second order running;\n"
          "each injection should be in the format lnv,lnb,phi,ielement\n"
          "and multiple injections can be specified by separating with a colon");
  f_second_order_injection = injection.value_or("");
  f_tree_level = cmdline->value<int>("-tree-level",-1).
                  help("Generates a tree-level event with n_emsn emissions and fills in an associated event weight").argname("n_emsn");
  f_max_print = cmdline->value("-max-print", 1.0).help("maximum number of events to print").argname("max");

  // NB: only negative values activate things here
  f_dynamic_lncutoff      = cmdline->value("-dynamic-lncutoff", 0.0)
                                    .help("If this is negative, then the generation stops when the evolution "
                                          "variable lnv<lnvfirst+dynamic_lncutoff, "
                                          "where lnvfirst is the lnv value corresponding to the "
                                          "first (hardest) emission");
  assert(f_dynamic_lncutoff <= 0.0
        && "lncutoff>0 would be ignored: did you want to put a negative value");

  // directional differences
  if (cmdline->present("-use-diffs").help("keep track of, and use, differences between directions of "
                                         "dipole ends to increase the numerical precision of  "
                                         "evaluations. Typically comes with a speed penalty of a few tens of percent")) {
    f_shower_runner->shower()->set_use_diffs(true);
  }
  header << "# direction differences = " << f_shower_runner->shower()->use_diffs() << endl;

  

  cmdline->start_subsection("Weighted generation", "Many of these options are valid only if -weighted-generation is present");
  f_weighted_generation   = cmdline->present("-weighted-generation")
                                    .help("Weighted event generation where the weight is roughly "
                                          "proportional to the Sudakov form factor for the first emission, "
                                          "making it possible to probe a wide range of lnv values.");
  f_nev_cache             = cmdline->value("-nev-cache", 0.0)
                                      .help("number of events to use in MC evaluation per step of the cached Sudakov; "
                                            "0 means use nev");
  if (f_weighted_generation || cmdline->help_requested()) {
    f_auto_lnv = cmdline->value_bool("-auto-lnv", true)
                          .help("manually set the number of steps in the Sudakov caching (default is automatic)");
    f_n_sudakov_cache   = cmdline->value("-n-sudakov-cache", 20).argname("n")
                                      .help("set the number of steps in the Sudakov caching (relevant only if auto-lnv is false)");
    f_lnsudakov_spacing = cmdline->value("-lnsudakov-spacing", 0.7).argname("step")
                                      .help("ln(Sudakov) step size for the steps in the Sudakov caching (relevant only if auto-lnv is true)");
  }
  f_integration_strategy = cmdline->value("-integration-strategy",MC)
                                    .choices(IntegrationStrategy_values(),IntegrationStrategy_doxygen())
                                    .help("The integration strategy to use to evaluate the cached Sudakov form factor\n"
                                    //" - MC: Monte Carlo integration\n"
                                    //" - GQ: Gaussian quadrature (in lnv and lnb)\n"
                                    //" - GQ3: Gaussian quadrature (in lnv, lnb and phi)\n"
                                    );
  f_shower_runner->set_integration_strategy(f_integration_strategy);
  cmdline->end_subsection();

  // set up the vetos

  cmdline->start_subsection("Emission vetoes");
  auto lnzmin = cmdline->optional_value<double>("-lnzmin").argname("lnzmin")
    .help("Set a lnzmin value for the shower (where z is emission energy / Q)");
  if (lnzmin.present()) f_shower_runner->set_veto(new EmissionVetoLnZmin<ShowerBase>(lnzmin));

  auto etacut = cmdline->optional_value<double>("-etacut").argname("etacut")
    .help("Set a maximum (-from-above) (minimum -from-below) eta value.");
  if (etacut.present()) {
    bool   from_above = cmdline->present("-from-above") || !cmdline->present("-from-below");
    f_shower_runner->set_veto(new EmissionVetoEta<ShowerBase>(etacut, from_above));
  }
  if (cmdline->present("-two-scale-VBF").help("Evolve the VBF system using two separate dipoles")){
    bool kin_limit = cmdline->present("-use-kin-limit");
    f_shower_runner->set_veto(new EmissionVetoVBF<ShowerBase>(kin_limit));
  }

  cmdline->end_subsection("Emission vetoes");

  //
  cmdline->start_subsection("Diagnostics"); //-----------------------------------------
  // options to save the state / load it in from a file
  f_save_state_on_fail =
        cmdline->present("-save-state-on-fail")
            .help(
                "If present, the state of the GSLRandom generator will be "
                "written to a file (whose name is the same as the event number "
                "followed by -GSL-state.out).");

  f_save_state_before_iev = cmdline->value("-save-state-before-iev",f_save_state_before_iev).
                            help("Save the GSLRandom state before starting event iev");

  if (f_save_state_on_fail) f_shower_runner->set_save_state_on_fail(f_save_state_on_fail);

  if (f_save_state_on_fail || f_save_state_before_iev >= 0)
    f_gsl_save_state_file = cmdline->value<string>("-save-state-to-file");
  
  auto load_state_from_file = cmdline->optional_value<string>("-load-state-from-file").argname("GSLRandom-file")
                               .help("If present, load the random seed from GSLRandom-file.");
  if (load_state_from_file.present()) {
    gsl.read_state_from_file(load_state_from_file.value());
  }
  bool dump_pdf = cmdline->present("-dump-pdf").help("write out info about PDFs (to output_filename.pdfs)");
  cmdline->end_subsection("Diagnostics"); 
  //-------------------------------------
  cmdline->end_section("Showering properties");
  
  // set up the hard process
  cmdline->start_section("Process selection");
  switch (hard_procces_ID){
    default: 
      assert(false && "Process ID not recognised! Maybe you need to re-run autogen?");
    case ProcID::pp2Z:
    // pp->Z
    f_process = std::unique_ptr<Processpp2Z>(new Processpp2Z(cmdline, f_collider->rts()));
    break;
    case ProcID::pp2H:
    // pp->H
    f_process = std::unique_ptr<Processpp2H>(new Processpp2H(cmdline, f_collider->rts()));
    break;
    case ProcID::pp2Xj:
    // pp->Xj where X = Z or H
    f_process = std::unique_ptr<Processpp2Xj>(new Processpp2Xj(cmdline, f_collider->rts()));
    break;
    case ProcID::pp2jj:
    // pp->jj (=qqbar -> jj)    
    f_process = std::unique_ptr<Processpp2jj>(new Processpp2jj(cmdline, f_collider->rts()));
    break;
    case ProcID::DIS :
    // DIS event
    f_process = std::unique_ptr<ProcessDIS>(new ProcessDIS(cmdline, f_collider->rts()));
    break;
    case ProcID::VBF:
    // VBF
    f_process = std::unique_ptr<ProcessVBF>(new ProcessVBF(cmdline, f_collider->rts()));
    break;
    case ProcID::ee2gg:
    // e+e- -> H -> gg
    f_process = std::unique_ptr<ProcessH2gg>(new ProcessH2gg(cmdline, f_collider->rts()));
    break;
    case ProcID::ee2qq:
    // e+e- -> Z -> qqbar
    f_process = std::unique_ptr<ProcessZ2qq>(new ProcessZ2qq(cmdline, f_collider->rts()));
    break; 
  }  
  cmdline->end_section("Process selection");
  
  // handling of the matching
  // first part of matching options
  string matching_section = "Matching options";
  cmdline->start_section(matching_section, "Most of these options are only enabled if -match-process is present");
  bool do_matching = cmdline->any_present({"-match-process","-3-jet-matching"})
                            .help("Turn on matching to the Born process + 1 emission");
  if (do_matching || cmdline->help_requested()) {
    bool f_random_axis       = cmdline->present("-random-axis");

    // set the shower for matching (it has its own section, so end and restart section)
    auto matching_shower_opt = cmdline->optional_value<string>("-matching-shower").argname("shower")
      .help("Shower to use for matching (default is the main shower)");
    cmdline->end_section(matching_section);
    set_shower_for_matching(*cmdline, f_shower_runner.get(), *f_collider);
    cmdline->start_section(matching_section);

    // set up the matching scheme
    MatchingScheme matching_scheme;
    if (cmdline->present("-mcatnlo-matching").help("Change the matching scheme to mc@nlo matching (where supported), "
                                                  "rather than the default multiplicative matching")) {
      matching_scheme = MCAtNLOStyle;
    } else {
      matching_scheme = PowhegStyle;
    }

    if (matching_scheme == MCAtNLOStyle) { // MC@NLO-type matching
      // We use the main shower as a matching shower (to later compute R - R_s)
      // this means we cannot use an explicit matching shower MC@NLO-like matching
      assert(!matching_shower_opt.present() &&
        "The MC@NLO scheme should be run with main_shower = matching_shower.");
      // add the info to the header
      header << "# MC@NLO matching" << endl;
    } else if (matching_scheme == PowhegStyle) { // Powheg-type matching
      header << "# Powheg-type matching with matching shower " << f_shower_runner->shower_for_matching()->name()
            << ", beta(matching) = " << f_shower_runner->shower_for_matching()->beta() 
            << " [internal description: " << f_shower_runner->shower_for_matching()->description() << "]"
            << endl;
    }
    else {
      std::cerr << "AnalysisFramework.cc: matching scheme not recognized." << std::endl;
      exit(-1);
    }
    // store the matching scheme
    f_matching_scheme = matching_scheme;

    //
    // Option to switch g->gg/g->qq splitting weights in the matching
    // MvB - not sure where to put these options, I'd like to make them part of the hard ME
    auto matching_wgg = cmdline->optional_value<double>("-matching-wgg-splitting").help("value of wgg partitioning parameter for g->gg in the matching matrix element (default is main shower's value); any other value breaks NNDL accuracy for processes with Born gluons");
    auto matching_wqq = cmdline->optional_value<double>("-matching-wqq-splitting").help("value of wqq partitioning parameter for g->qqbar in the matching matrix element (default is main shower's value); any other value breaks NNDL accuracy for processes with Born gluons");
    bool power_shower = cmdline->present("-matching-power-shower")
                                .help("Turn on the 'power shower' matching option for POWHEG-style matching, "
                                      "which restarts the main shower from lnvmax, rather than from the lnv "
                                      "of the first emission and then vetoes previously covered phasespace "
                                      "(only tested with POWHEG-style matching + Pythia).");
    // Option to disable the matching veto to study NNDL effects
    bool disable_matching_veto = cmdline->present("-disable-matching-veto")
                                        .help("When matching involves a combination of two showers, this option disables that veto that aligns the phase space main-shower emissions with the phase space constraint from the matching shower's emission");

    // then actually set up the matching only if do_matching is true (rather than just help)
    if (do_matching) {
      // Setup the hard matrix elements
      f_process->set_hard_matching_me(f_shower_runner->shower()->qcd());
      if (matching_wgg.present()) f_process->hard_me()->set_g2gg_splitting_weight(matching_wgg.value());
      if (matching_wqq.present()) f_process->hard_me()->set_g2qq_splitting_weight(matching_wqq.value());
      header << "# Exact matrix-element: " << f_process->hard_me()->description() << endl;

      header << "# Power shower = " << power_shower << endl;

      // Setup the matching pointer
      f_3jet_matching.reset(new MatchedProcess(
                            f_process->hard_me(),
                            f_shower_runner->shower(),
                            f_shower_runner->shower_for_matching(), f_shower_runner->strategy(),
                            matching_scheme, f_random_axis, power_shower));

      if (disable_matching_veto) f_3jet_matching->enable_matching_veto(false);

      f_shower_runner->set_matching(f_3jet_matching.get());
    }
  }
  cmdline->end_section(matching_section);

  // //-------------------------------------------------
  // // read info for PDFs from the command line
  // //-------------------------------------------------
  // cmdline->section("PDF choices");
  // _opt_dump_pdfs = cmdline->present("-dump-pdfs").help("dump a basic summary of the PDFs to output_filename.pdfs");
  // _opt_pdf_choice = cmdline->value<HoppetRunner::PDFChoice>("-pdf-choice", HoppetRunner::ToyNf5)
  //                            .help("the PDF choice").choices(HoppetRunner::PDFChoice_values());
  // cmdline->end_section("PDF choices");


  f_hoppet_runner.reset(create_hoppetrunner(*cmdline, header, f_collider->has_pdfs()));
  // pass the hoppetrunner instance to showerrunner
  init_hoppetrunner_in_showerrunner(*cmdline, f_shower_runner.get(), f_hoppet_runner.get(), f_lnvmax);

  if (f_collider->has_pdfs() && f_hoppet_runner) {
    if(f_hoppet_runner->pdf_needs_remapped_scales()){
      header << "# Using toy PDFs - setting scale mapping to true " << std:: endl;
    }

    if (dump_pdf) {
      f_shower_runner->dump_PDFs(output_filename + ".pdfs");
    }
  } 
  // create the weight histogram only if requested
  cmdline->start_section("Analysis-specific options");
  if (cmdline->present("-weight-hist").help("Create a histogram of the event weights")) {
    weight_hist = &hists_err["weights"].declare(-50.0,50.0,1.0);
  }
  cmdline->end_section("Analysis-specific options");
}

//======================================================================
void AnalysisFramework::post_startup() {
  header << "# rts = "    << f_rts << endl;
  header << "# lnvmax = " << f_lnvmax << endl;
  header << "# lnvmin = " << f_lnvmin << endl;
  header << "# max_emsn = "            << f_max_emsn             << endl;
  header << "# first_order = "         << f_first_order          << endl;
  header << "# second_order = "        << f_second_order         << endl;
  header << "# tree_level = "          << f_tree_level           << endl;
  header << "# dynamic_lncutoff = "    << f_dynamic_lncutoff     << endl;
  header << "# weighted_generation = " << f_weighted_generation  << endl;
  std::string on_off = (f_shower_runner->getMatching()) ? "on" : "off";
  header << "# 3-jet matching = " << on_off << endl;
  header << "# Shower scale choices: " << f_shower_runner->shower()->scale_description() << endl;
  if(f_shower_runner->strategy()==panscales::CentralRap) {
    header << "# half central rap window = " << f_shower_runner->half_central_rap_window() << endl;
  }

  if (f_second_order_injection != "") {
    header << "# second_order_injection = " << f_second_order_injection << endl;
    vector<string> injection_strings = str_split(f_second_order_injection,':');
    for (auto & str: injection_strings) {
      str = str_replace(str, ',', ' ');
      istringstream istr(str);
      Injection inj;
      istr >> inj.lnv >> inj.lnb >> inj.phi >> inj.ielement;
      f_second_order_injection_vec.push_back(inj);
    }
  }
  header << "# Process: " << f_process->description() << std::endl;
  // if we are using a fixed underlying born, generate a base hard event here
  if(f_process->use_fixed_born_event()) f_event_base = f_process->generate_event();
  else if (f_weighted_generation) {
    // make sure that we're not using weighted generation (which only works
    // if the Born event is fixed)
    throw std::invalid_argument("Weighted generation can only be used with fixed Born configurations.");
  }

  // initialise hoppet if it has not been done yet
  if (f_hoppet_runner){
    f_hoppet_runner->initialise(f_shower_runner->shower()->qcd());
    header << "# Hoppet: " << f_hoppet_runner->description() << endl;
  }

}

void AnalysisFramework::pre_run() {
 
  // put out the header
  cout << header.str() << endl;

  unsigned nev_cache = (f_nev_cache == 0) ? nev : f_nev_cache;
  if (f_weighted_generation) {
    if (!f_auto_lnv) {
      cout << "Constructing the Sudakov cache with " << nev_cache << " events and "
           << f_n_sudakov_cache << " lnv cache bins "
           << endl;
      cout << "This may take a while..." << endl;
      f_shower_runner->cache_sudakov_values_uniform_lnv(f_event_base, f_lnvmax, f_lnvmin,
                                                        f_n_sudakov_cache, nev_cache);
    } else {
      f_shower_runner->cache_sudakov_values_uniform_lnsudakov(f_event_base, f_lnvmax, f_lnvmin,
                                                              f_lnsudakov_spacing, nev_cache);
    }
    cout << "------> Cache constructed." << endl;
    header << "# Sudakov cache constructed with " << nev_cache << " events per bin"
           << endl;
    f_shower_runner->write_sudakov_values(header);
  }
  else if (f_3jet_matching && f_3jet_matching->get_matching_scheme() == MCAtNLOStyle) {
    cout << "Computing the MC@NLO integral with " << nev_cache << " events"
          << endl;
    f_mcatnlo_integral = 
          f_shower_runner->compute_mcatnlo_integral(f_event_base, f_lnvmax, f_lnvmin, nev_cache);
    header << "# MC@NLO integral computed with " << nev_cache << " events = " 
           << f_mcatnlo_integral.integral << " +/- " << f_mcatnlo_integral.err
           << " (abs weight: " << f_mcatnlo_integral.integral_abs << " +/- " << f_mcatnlo_integral.err_abs << ")"
           << endl;
  }
}

//======================================================================
bool AnalysisFramework::generate_event() {

  if (int64_t(iev) == f_save_state_before_iev ) {
    cout << "Saving GSL state before event " << iev << " to file " << f_gsl_save_state_file << endl;
    gsl.write_state_to_file(f_gsl_save_state_file);
  }

  if (f_timingInfo) f_evt_gen_start = high_resolution_clock::now();
  
  // generate the hard event
  f_event = f_process->generate_event();
  Momentum initial_momentum = f_event.momentum_sum();
  // record the Born event
  f_born_event = f_event;

  // generate events
  if (f_first_order) {
    event_weight_ = f_shower_runner->first_order(f_event, f_lnvmax, f_lnvmin);
  } else if (f_second_order) {
    event_weight_ = f_shower_runner->second_order(f_event, f_lnvmax, f_lnvmin, f_second_order_injection_vec, f_second_order_lnb_window);
  } else if (f_tree_level >= 0) {
    event_weight_ = f_shower_runner->tree_level(f_event, f_lnvmax, f_lnvmin, f_tree_level);
  } else {
    double lnv = f_lnvmax;
    if (f_3jet_matching && f_3jet_matching->get_matching_scheme() == MCAtNLOStyle){
      // ----------------------------
      // MC@NLO-style matching
      // ----------------------------

      // 1) Select hard events with probability int_abs / (1 + int_abs)
      double p_hard_event = f_mcatnlo_integral.integral_abs / (1 + f_mcatnlo_integral.integral_abs);
      // 2) Total normalization is 1/(1 + int), the numerator is distributed over the born events (1) and the hard events (int_abs).
      event_weight_ = (1 + f_mcatnlo_integral.integral_abs) / (1 + f_mcatnlo_integral.integral);
      f_mcatnlo_hard_event = gsl.accept(p_hard_event);
      if (f_mcatnlo_hard_event) {
        // 3) Correct hard weights to differential, i.e. (weight_matched - weight_ps)/int_abs
        event_weight_ *= f_shower_runner->first_order(f_event, f_lnvmax, f_lnvmin) / f_mcatnlo_integral.integral_abs;
      }
    } else {
      event_weight_ = 1.0;
    }
    // run the shower
    try{
      f_shower_runner->run(f_event, lnv, f_lnvmin, event_weight_, f_weighted_generation,
                           f_dynamic_lncutoff, f_max_emsn);
      /// #TRK_ISSUE-9  KH 2020-APR-02 :
      /// The following line inherits from the towards-faster-event-generation
      /// branch. At the time of writing it is only ever activated by the 
      /// BetaDepDynCutFAPX code -- the only current source of size()==0 events.
      /// It is employed for the purposes of skipping analysis of events with a
      /// bare minimum of overhead. This is relevant in the case when targetting
      /// a window of an observable in the Lund plane with unweighted events 
      /// as in the global event shapes and delta-psi analyses, for which MANY
      /// events are rapidly detected to be outside the target window.
      if (f_event.size() == 0) return false;
    } catch (ErrorZeroMassElement &error){
      cerr << "Caught ErrorZeroMassElement exception, discarding the event" << endl;
      cerr << "Event: " << f_event.size() << " particles" << endl; 
      for (const auto & particle : f_event.particles())
        cerr << "   " << particle << endl; 
      return false;
    }
  }

  // check momentum conservation
  Momentum4<precision_type> momentum_difference;
  Momentum4<precision_type> momentum_in = f_event.momentum_partons_in();
  // for e+e-, the momentum in should just be the initial momentum
  // as evaluated before showering ()
  if (momentum_in.m2() == 0) momentum_in = initial_momentum;
  momentum_difference = f_event.momentum_sum() - momentum_in;

  precision_type E = momentum_in.E();
  for (unsigned i = 0; i<4; i++) {
    double reldiff = to_double(momentum_difference[i]/E);
    if (fabs(reldiff) > 1e-6 || (reldiff != reldiff)) {
    //if (fabs(reldiff) > 100*std::numeric_limits<precision_type>::epsilon() || (reldiff != reldiff)) {
       
      ostringstream ostr;
      ostr << "Relative difference " << reldiff
           << " in component " << i
           << " of total event momentum (before/after generation) "
           << "exceeds threshold or is NaN" << endl;
      ostr << "Incoming momentum is " << momentum_in << endl;
      ostr << "Final    momentum is " << f_event.momentum_sum() << endl;
      ostr << "Difference is        " << momentum_difference << endl;
      ostr << f_event << endl;
      _badMom.warn(ostr.str());
      if(f_save_state_on_fail){
        f_shower_runner->get_gsl_copy().write_state_to_file(f_gsl_save_state_file);
      }
      assert(false);
      // we won't need the warning for all the components of this particular momentum
      break;
    }
  }
  
  // record the timing info if relevant
  if (f_timingInfo) {
    f_evt_gen_end = high_resolution_clock::now();
    duration<double, std::milli> t_evt_gen = f_evt_gen_end - f_evt_gen_start;
    averages["t_evgen_ms"].add_entry(t_evt_gen.count(), 1.0);
    f_evt_ana_start = high_resolution_clock::now();
  }
  
  // track multiplicity
  multiplicity_unweighted.add_entry(f_event.size(), 1.0);
  multiplicity_weighted  .add_entry(f_event.size(), event_weight_);
  lnmult_hist            .add_entry(log(f_event.size()), event_weight_);
  if (weight_hist != nullptr)  weight_hist->add_entry(event_weight_, 1.0);
  
  xsection_total += event_weight_;
  if (event_weight_ >= 0) xsection_positive += event_weight_;
  else                    xsection_negative += event_weight_;
  if (f_mcatnlo_hard_event) xsection_mcatnlo_hard += event_weight_;

  if (iev < f_max_print) {
    cout << "iev = " << iev << "\n";
    cout << "Event weight = " << event_weight_ << endl;
    cout << "Base (unshowered) event:" << endl;
    cout << f_born_event;
    cout << "Showered event:" << endl;
    cout << f_event;
    cout << endl;
  }

  return true;
}



void AnalysisFramework::post_analyse_event() {
  if (f_timingInfo) {
    f_evt_ana_end = high_resolution_clock::now();
    duration<double, std::milli> t_evt_ana = f_evt_ana_end - f_evt_ana_start;
    averages["t_evana_ms"].add_entry(t_evt_ana.count(), 1.0);
    
  }
}

void AnalysisFramework::generator_output(ostream & ostr) {
  string warning_summary = LimitedWarning::summary();
  if (warning_summary != "") {
    ostr << "\n\n"
         << "#-------------- WARNING SUMMARY ------------------\n"
         << warning_summary
         << endl;
  }
}

AnalysisFramework::~AnalysisFramework() {
  string warning_summary = LimitedWarning::summary();
  if (warning_summary != "") {
    cout << "\n\n"
         << "===================================================\n"
         << " WARNING SUMMARY \n"
         << "===================================================\n"
         << warning_summary;
  }
}
