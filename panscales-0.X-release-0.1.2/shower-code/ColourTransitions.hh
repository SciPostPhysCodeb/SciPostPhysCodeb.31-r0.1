//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
/// \file ColourTransitions.hh
///
/// This file contains the code that handles the colour transitions
/// along a dipole chain
/// 
/// The colour treatment involves 3 classes:
///    
///   ColourTransitionPoint    info associated w a single transition point
///   ColourTransitionVector   full vector of transition points along a dipole chain
///   ColourTransitionRunner   event-level handling of colour transitions
/// 
/// The 3rd of these classes is declared and implemented in
/// ColourTransitionRunner.hh (mostly so as to avoid circular
/// dependencies in headers)
///
/// The implementation/flow is as follows:
///
/// - each dipole has an instance of ColourTransitionVector
///   (initialised manually together w the dipoles in the event)
///
/// - ShowerRunner has an instance of ColourTransitionRunner
///
/// - In the event loop:
///   . ShowerRunner calls ColourTransitionRunner::colour_factor_acceptance
///     once the kinematics has been decided, to get an acceptance
///     probability associated w colour factors
///     --- stores eta_approx, colour_transition_index and eta_insertion in emission_info
///   . when updating the event in Showerbase::update_event, call one of 
///       ColourTransitionRunner::update_transitions_gluon_emission_from_quark
///       ColourTransitionRunner::update_transitions_gluon_emission_from_gluon
///       ColourTransitionRunner::update_transitions_gluon2qqbar_at_dipole_q_end
///       ColourTransitionRunner::update_transitions_gluon2qqbar_at_dipole_qbar_end
///     dependng on the splitting type
///
/// Possible ideas for speedup:
///  - break colour_factor_acceptance in bits:
///    . a part before the kinematics is decided
///    . a part after the acceptance probability has been decided
///  - make ColourTransitionPoint a base class w different
///    specialisations for different need. ColourTransitionVector
///    would then be a template

#ifndef __PANSCALES_COLOUR_TRANSITIONS_HH__
#define __PANSCALES_COLOUR_TRANSITIONS_HH__

#include <vector>
#include <cassert>
#include "Momentum3.hh"
#include "Type.hh"
#include "LimitedWarning.hh"

namespace panscales{

  //------------------------------------------------------------------------
  /// @ingroup colour_classes
  /// \class ColourTransitionPoint
  /// holds the info about a single transition point
  ///
  /// For the current approaches to colour, each transition point
  /// stores a rapidity and an "auxiliary spectator" index
  ///
  /// Note that depending on the approach, if may be that not all the
  /// information is used by the shower (e.g. at this stage, the
  /// auxiliary index is neither set nor used)
  class ColourTransitionPoint{
  public:
    /// ctor with full initialisation
    ///   eta               rapidity of the transition point (wrt emitter, in event frame)
    ColourTransitionPoint(double eta_in=0.0)
      : _eta(eta_in),
        _dirdiff(Momentum3<precision_type>()), _is_dirdiff_wrt_3bar(true) {}
   
    /// ctor with full initialisation
    ///   eta               rapidity of the transition point (wrt emitter, in event frame)
    ///   dirdiff           distance from the auxiliary to either the 3 or 3bar end of the associated dipole
    ///                     (i.e. aux-3bar or aux-3)
    ///   is_dirdiff_wrt_3bar  true if distance if to the 3bar and, false if to 3
    ColourTransitionPoint(double eta_in,
                          const Momentum3<precision_type> &dirdiff_in,
                          bool is_dirdiff_wrt_3bar_in)
      : _eta(eta_in),
        _dirdiff(dirdiff_in), _is_dirdiff_wrt_3bar(is_dirdiff_wrt_3bar_in) {}

    /// access info
    const double eta() const { return _eta; }
    const Momentum3<precision_type> & dirdiff() const{ return _dirdiff; }
    bool is_dirdiff_wrt_3bar() const {return _is_dirdiff_wrt_3bar; }

    /// set info
    void set_eta(double eta_in){ _eta = eta_in; }

  private:
    double _eta;           ///< rapidity of the transition point in the event frame

    /// dir difference wrt either the 3bar or the 3 end of the dipole
    Momentum3<precision_type> _dirdiff;

    /// true if closer end is the 3bar
    bool _is_dirdiff_wrt_3bar;
  };
  
  //------------------------------------------------------------------------
  /// @ingroup colour_classes
  /// \class ColourTransitionVector
  /// class to handle the colour transitions along a dipole chain
  ///
  /// colour transitions are stored as a vector of transition
  /// point. Each transition point consists of a rapidity and a
  /// "spectator" index. The vector is ordered in rapidity.
  ///
  /// Each transition point correspond to the transition between a
  /// CA/2 region and a CF region.
  class ColourTransitionVector{
  public:
    /// ctor w end-type specification
    ColourTransitionVector(bool qbar_end_is_CF=false) :
      _3bar_end_is_CF(qbar_end_is_CF){}

    ColourTransitionVector(const ColourTransitionVector & other) :
       _3bar_end_is_CF(other._3bar_end_is_CF),
       _transition_points(other._transition_points) {}

    // move assignment operator
    ColourTransitionVector & operator=(ColourTransitionVector && other) {
      _3bar_end_is_CF = other._3bar_end_is_CF;
      _transition_points = std::move(other._transition_points);
      return *this;
    }

    // normal move assignment
    ColourTransitionVector & operator=(const ColourTransitionVector & other) {
      _3bar_end_is_CF = other._3bar_end_is_CF;
      _transition_points = other._transition_points;
      return *this;
    }


    //--------------------------------------------------
    // a few simple helpers
    
    /// returns the number of transition points
    unsigned int size() const{
      return _transition_points.size();
    }

    /// returns true if the 3bar end of the chain is quark-like
    bool is_3bar_end_CF() const{
      return _3bar_end_is_CF;
    }    

    /// returns true if the 3 end of the chain is quark-like
    bool is_3_end_CF() const{
      return is_quark_below_transition(size());
    }

    /// set the 3bar end 
    void set_3bar_end_CF(bool is_CF){
      _3bar_end_is_CF = is_CF;
    }    

    /// access to the full vector
    const std::vector<ColourTransitionPoint> & transition_points() const{ return _transition_points;}

    /// access to one element in the vector
    const ColourTransitionPoint & operator[](unsigned int i) const{
      assert(i<size());
      return _transition_points[i];
    }

    
    //--------------------------------------------------
    /// get the index associated w a given rapidity.
    ///
    /// For a given rapidity eta, this returns the index of the
    /// rapidity eta_transition at eta<eta_transition.
    unsigned int get_transition_index_above(double eta) const{
      // #TRK_ISSUE-69  we could simply use
      //return _transition_points.upper_bound(_transition_points.begin(), _transition_points.end(), eta) - _transition_points.begin();
      //TODO: time+test all 3 methods
      
      // make a bisection search
      // idx_min <= idx < idx_max
      unsigned int idx_min = 0, idx_max = _transition_points.size()+1, idx;
      while (idx_max != idx_min+1){
        // take the midpoint
        //
        // idx_max is at least idx_min+2, so this runs between idx_min+1 and idx_max-1
        idx = (idx_min + idx_max)/2;

        // test on which side we are (note the -1 offset when looking at the transition eta)
        if (eta>_transition_points[idx-1].eta()){
          idx_min = idx;
        } else {
          idx_max = idx;
        }
      }

      // the few lines of code below can be used to make sure that the
      // above bisection works. It has been used from 2020-07-13 to
      // 2023-10-18 at which point it has been commented out.
      //
      //unsigned int test_index = 0;
      //while ((test_index < _transition_points.size()) &&
      //       (eta > _transition_points[test_index].eta())){
      //  test_index++;
      //}
      //assert(test_index == idx_min);

      return idx_min;
      
    }
    
    /// a synonym for get_transition_index_above
    unsigned int get_segment_index(double eta) const {
      return get_transition_index_above(eta);
    }

    /// returns true if eta is above the left-hand edge of the segment
    bool eta_above_segment_L(double eta, unsigned int segment_index) const {
      // if we have no transition points, or we are in segment zero
      // then lower edge is -infinity and so we are above that
      if (size() == 0 || segment_index == 0) return true;
      // otherwise a straight check is fine
      return eta > _transition_points[segment_index-1].eta();
    }

    /// returns true if eta is below the right-hand edge of the segment
    bool eta_below_segment_R(double eta, unsigned int segment_index) const {
      // if we are on (or beyond!) the last segment, then eta is always
      // below its upper limit of +infty
      if (segment_index >= size()) return true;
      // otherwise a straight check is fine
      return eta < _transition_points[segment_index].eta();
    }

    /// split this sequence into two new sequences, up to and including
    /// last_segment_L, and starting from (including) first_segment_R
    ///
    /// if the transition_L pointer is non-null, it is added to the end
    /// of sequence_L. Similarly for transition_R which goes to the
    /// beginning of sequence_R.
    ///
    void split(ColourTransitionVector &      sequence_L,
               unsigned                      last_segment_L,
               const ColourTransitionPoint * transition_L,
               const ColourTransitionPoint * transition_R,
               unsigned                      first_segment_R,
               ColourTransitionVector &      sequence_R) {

      // first deal with sequence_R (which must start off empty)
      assert(sequence_R.size() == 0);
      if (transition_R != nullptr) {
        // if we are adding an extra transition at the beginning of R,
        // then the sequence starts with the opposite colour factor to
        // that for this segment 
        sequence_R._3bar_end_is_CF = ! is_quark_below_transition(first_segment_R);
        sequence_R._transition_points.push_back(*transition_R);
      } else {
        // otherwise, we start with this segment
        sequence_R._3bar_end_is_CF = is_quark_below_transition(first_segment_R);
      }
      // perhaps not the most efficient implementation but go with it for now
      for (unsigned i = first_segment_R; i < size(); ++i) {
        sequence_R._transition_points.push_back(_transition_points[i]);
      }

      // then deal with sequence_L
      // first do an O(1) move (which carries the 3bar_end_is_CF info)
      sequence_L = std::move(*this);
      // then adjust the size to remove extraneous segments/transitions
      // and if need be add on a final transition
      if (transition_L == nullptr) {
        sequence_L._transition_points.resize(last_segment_L);
      } else {
        sequence_L._transition_points.resize(last_segment_L + 1);
        sequence_L._transition_points[last_segment_L] = *transition_L;
      }
    }

    /// push a new transition point at the back of the vector
    /// (Note: this does not change the flavour of the 1st segment)
    void push_back(const ColourTransitionPoint & new_transition){
      _transition_points.push_back(new_transition);
    }
    void push_back(ColourTransitionPoint && new_transition){
      _transition_points.push_back(new_transition);
    }

    /// remove the last transition point 
    /// (Note: this does not change the flavour of the 1st segment)
    void pop_back(){
      assert(size()>0);
      _transition_points.pop_back();
    }

    /// push a new transition point at the front of the vector
    /// WATCH OUT: O(n) operation w the current implementation as a std::vector
    /// (Note: this does change the flavour of the 1st segment)
    void push_front(const ColourTransitionPoint & new_transition){
      _transition_points.insert(_transition_points.begin(), new_transition);
      _3bar_end_is_CF = !_3bar_end_is_CF;
    }
    void push_front(ColourTransitionPoint && new_transition){
      _transition_points.insert(_transition_points.begin(), new_transition);
      _3bar_end_is_CF = !_3bar_end_is_CF;
    }

    /// remove the first transition point
    /// WATCH OUT: O(n) operation w the current implementation as a std::vector
    /// (Note: this does change the flavour of the 1st segment)
    void pop_front(){
      assert(size()>0);
      _transition_points.erase(_transition_points.begin());
      _3bar_end_is_CF = !_3bar_end_is_CF;
    }
    
    //--------------------------------------------------
    /// same as above with a hint for the search starting point
    ///
    /// Knowing that the 'initial_eta' was at the 'initial_index'
    /// position, find the index associated with 'eta'
    unsigned int get_transition_index_above(double eta, double initial_eta, unsigned int initial_index) const{
      // Reminder: The new index is st
      //   colour_transitions[new_index-1] < eta < colour_transitions[new_index]
      unsigned int new_index = initial_index;
    
      // if eta is larger than the initial one we can only move towards larger indices
      // otherwise, we only search towards smaller rapidities
      if (eta > initial_eta){
        while ((new_index < _transition_points.size()) &&
               (eta > _transition_points[new_index].eta())){
          ++new_index;
        }
        return new_index;
      }

      // eta <= initial_eta
      while ((new_index > 0) &&
             (eta < _transition_points[new_index-1].eta())){
        --new_index;
      }
      
      return new_index;
    }

    //--------------------------------------------------
    /// returns whether the ith segment in the transition list is
    /// quark-like (CF) or gluon-like (CA/2)
    bool is_quark_below_transition(unsigned int index) const{
      // if the 3bar end is a quark, every even index is a quark
      //                      gluon,       odd
      // so we have a quark if "3bar_end_is_quark" and "index" have
      // opposie parities
      //
      // longer explanation: see [NoteDetails:00001]
      // If the anticolour end of the dipole (aka "qbar_end") IS a
      // q/qbar colour transitions alternate as ({index,colour-factor}),
      // [ {0,CF}, {1,CA}, {2,CF}, {3,CA}, {4,CF}, {5,CA}, ... ]
      // i.e. all even values of "index" are associated to a CF.
      //  
      // If the anticolour end of the dipole (aka "qbar end") ISN'T a
      // q/qbar colour transitions alternate as ({index,colour-factor}),
      // [ {0,CA}, {1,CF}, {2,CA}, {3,CF}, {4,CA}, {5,CF}, ... ]
      // i.e. all even values of "index" are associated to a CA.
      //  
      // Below, index & 0x1 = 1 if index is odd and = 0 if it's even.
      //
      // if qbar_end_is_quark = true  then index & 0x1 = 0 (= even) means 
      // index is associated to a CF.
      // if qbar_end_is_quark = false then index & 0x1 = 1 (= odd)  means
      // index is associated to a CF.
      //
      // Bitwise xor ^ between qbar_end_is_quark and (index & 0x1)
      // then means we return true only if index is associated to a CF.

      return _3bar_end_is_CF ^ (index & 0x1);
    }

    /// debugging helper that checks the ordering of the transition points
    bool check_ordering() const{
      if (_transition_points.size() == 0) return true;
      double eta = _transition_points[0].eta();
      unsigned int i=1;
      while (i<_transition_points.size()){
        assert(_transition_points[i].eta()>eta);
        eta = _transition_points[i].eta();
        ++i;
      }
      return true;
    }

  private:
    /// true if the 3bar end of the chain is a quark (CF); false if it
    /// is a gluon (CA)
    bool _3bar_end_is_CF;

    /// the rapidities of each transition point i.e. rapidities at
    /// which we transition between quarks (CF) and gluons (CA/2).
    /// The list starts from the qbar end (-infinity) to the quark
    /// end (+infinity)
    std::vector<ColourTransitionPoint> _transition_points;
  };

  /// outputs a colour transition point
  inline std::ostream & operator<<(std::ostream & ostr, const ColourTransitionPoint & p) {
    ostr << p.eta();
    return ostr;
  }

  /// outputs a full colour transition vector
  inline std::ostream & operator<<(std::ostream & ostr, const ColourTransitionVector & v) {
    // print full-colour transitions
    ostr << "[-inf; ";
    bool qflav = v.is_3bar_end_CF();
    for (const auto & trans_pt : v.transition_points()){
      ostr << (qflav ? "CF; {" : "CA; {") << trans_pt << "}; ";
      qflav = !qflav;
    }
    ostr << (qflav ? "CF; " : "CA; ") << " inf]";
    return ostr;
  } 

  
} // nemaspace panscales

#endif  // __PANSCALES_COLOUR_TRANSITIONS_HH__
