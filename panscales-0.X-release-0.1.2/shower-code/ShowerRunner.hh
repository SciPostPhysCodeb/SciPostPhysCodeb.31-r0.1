//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __SHOWERHELPERS_HH__
#define __SHOWERHELPERS_HH__

#include "Weight.hh"
#include "Event.hh"
#include "ShowerBase.hh"
#include "Type.hh"
#include "Approx.hh"
#include "EmissionVeto.hh"
#include "GSLRandom.hh"
#include "SpinCorrelations.hh"
#include "ColourTransitionRunner.hh"
#include "HoppetRunner.hh"
#include "ShowerPowheg.hh"
// #include <gsl/gsl_multiroots.h>

#include <cmath>
#include <cassert>
#include <tuple>
#include <vector>

#include "dgauss.hh"
#include "LimitedWarning.hh"
#include "EnhancedRoulette.hh"

#include "ISROverheadFactor.hh"

namespace panscales {

extern GSLRandom gsl;

/// several possible strategies
///
/// #TRK_ISSUE-659  See 2018-standalone-shower/eeshower for additional strategies
enum Strategy : unsigned int {
  /// historical pythia approach where each element is recreated on the spot
  PythiaHistorical      = 1,
  /// roulette wheel with occasional update of the max (note: this uses the total astar)
  RoulettePeriodic      = 22,
  /// approximate all densities by same global overestimate, if first acc/rej fails generate next lnv (lazy variant)
  RouletteOneShot       = 23,
  /// roulette periodic optimised for elements with different enhancement factors
  RouletteEnhanced      = 24,
  /// generate a fixed window around the dipole's most central rapidities
  /// (NB: valid only if looking just at central emissions)
  CentralRap = 30,
  /// generate only in a fixed window along the dipole's most collinear
  /// rapidities (NB: valid only if looking just at energetic collinear emissions)
  CollinearRap = 31,
  /// combine the above two strategies
  /// (valid e.g. for soft spin studies, when looking at collinear splittings
  /// from large-angle emissions)
  CentralAndCollinearRap = 32
};

/// Numerical integration strategy for use in computing the Sudakov exponent
///
enum IntegrationStrategy {
  /// Original MC approach
  MC      = 1,
  /// Gaussian quadrature
  GQ      = 2,
  /// 3D Gaussian quadrature (including phi, for cases where
  /// do_kinematics can return false, or for pp events)
  GQ3     = 3
};

// forward declaration
enum MatchingScheme : unsigned int;

}

#include "MatchedProcess.hh"

namespace panscales {

/// @brief  Struct to help with injection of a specific branching 
struct Injection {
  double lnv, lnb, phi;
  int ielement;
};


//-----------------------------------------------------------------
/// \class StepByStepEvent
/// helper class that stores the lnv, lnb, phi, event, ... at each emissions step
class StepByStepEvent{
public:
  /// default ctor
  StepByStepEvent(){ reset(); }

  /// reset the stored data
  void reset();

  /// set/get the cached initial event 
  void set_initial_event(const Event & initial_event_in){ initial_event_ = initial_event_in; }
  const Event & initial_event() const {return initial_event_; }

  /// append an event+info after one step of the shower
  void push_back(unsigned int emitter_index_in,
                 unsigned int spectator_index_in,
                 unsigned int split_index_in,
                 double lnv_in, double lnb_in, double phi_in,
                 const Event &event_in);

  /// Return the total number of steps in the shower.
  unsigned int nsteps() const{ return events_.size(); }
  /// index of the emitter particle at the given istep
  unsigned int emitter_index(unsigned int istep) const {return emitter_indices_[istep]; }
  /// index of the spectator particle at the given istep
  unsigned int spectator_index(unsigned int istep) const {return spectator_indices_[istep]; }
  /// index of the radiated particle the given istep (NB: this should be renamed...)
  unsigned int splitted_index(unsigned int istep) const {return split_emitter_indices_[istep]; }
  double lnv(unsigned int istep) const {return lnvs_[istep]; }
  double lnb(unsigned int istep) const {return lnbs_[istep]; }
  double phi(unsigned int istep) const {return phis_[istep]; }
  const Event & event(unsigned int istep) const {return events_[istep]; }

private:
  Event initial_event_; 
  std::vector<unsigned int> emitter_indices_, spectator_indices_, split_emitter_indices_;
  std::vector<double> lnvs_, lnbs_, phis_;
  std::vector<Event> events_;

}; // end class StepByStepEvent
  
  
//-----------------------------------------------------------------
/// \class ShowerRunner
/// helper class to run a shower
class ShowerRunner {
public: 
  /// for Sudakov storage and return (and more generally return of a
  /// lnv and weight combination)
  class Sudakov {
  public:
    Sudakov() {}
    Sudakov(double lnv_in, double value_in, double error_in = 0.0)
      : lnv(lnv_in), value(value_in), error(error_in) {}
    Sudakov & operator*=(const Sudakov & other) {
      double rel_error = sqrt(pow2(error/value) + pow2(other.error/other.value));
      value *= other.value;
      error = rel_error * value;
      return *this;
    }
    double lnv, value = 0, error = 0;
  };
  
  /// constructor from a given shower, with the class taking ownership of the shower.
  ///   \param shower   pointer to the shower to run
  ///   \param strategy the generation strategy (at the moment keep the historicalby default)
  ShowerRunner(ShowerBase * shower, Strategy strategy = RoulettePeriodic);

  /// dtor
  ~ShowerRunner(){
    delete _colour_transition_runner;
  }

  /// prints the banner
  void print_banner(); 

  /// returns a description of the shower
  std::string description() const;

  //-----------------------------------------------------------------
  ///
  /// @name main shower configurations
  /// @{
  //-----------------------------------------------------------------

  /// return a const pointer to the shower
  const ShowerBase * shower() const {return _shower.get();}
  /// return a non-const pointer to the shower (use with caution)
  ShowerBase * shower() {return _shower.get();}

  
  /// Will be used to select the integration method for the Sudakov exponent
  void set_integration_strategy(IntegrationStrategy integration_strategy) {
    _integration_strategy = integration_strategy;
  }

  /// Will be used to pass the shower a pointer to a veto object.
  /// Note that ShowerRunner will grab ownership of the pointer
  void set_veto(EmissionVeto<ShowerBase> * veto_ptr) {
    _veto_ptr = std::unique_ptr<EmissionVeto<ShowerBase>>(veto_ptr);
  }
  // return a non-const pointer to the veto (because the functions in veto are not marked const)
  std::unique_ptr<EmissionVeto<ShowerBase>> & veto_ptr() {return _veto_ptr;}
  
  /// set/get the PDF runner
  void set_PDF_runner(HoppetRunner *hoppet_runner_in){ _hoppet_runner = hoppet_runner_in; }

  /// Access the MatchedProcess in ShowerRunner.
  HoppetRunner * PDF_runner() const { return _hoppet_runner; }



  //-----------------------------------------------------------------
  /// @}
  /// @name main functions to run the shower
  /// @{
  //-----------------------------------------------------------------

  /// main code to run a shower, which evolves the shower from lnv down to lnv_min
  /// and fills the event.
  ///
  /// Arguments:
  /// \param event               (initialised) event to fill
  /// \param lnv                 initial value of the evolution variable
  /// \param lnv_min             minimal value of lnv (evolution is run to lnv_min)
  /// \param dynamic_lncutoff    if this is negative, then the
  ///                            generation stops when the evolution
  ///                            variable lnv < lnvfirst +
  ///                            dynamic_lncutoff where lnvfirst is
  ///                            the lnv value corresponding to the
  ///                            first (hardest) emission
  /// \param weighted_generation when true the first emission is generated with a weight
  /// \param max_emsn            stop if max_emsn have been generated (-ve means infinity)
  /// \param continuation        continue earlier showering of this event
  void run(Event & event, double lnv, double lnv_min,
           double & weight, bool weighted_generation,
           double dynamic_lncutoff, int max_emsn = -1, bool continuation=false);

  /// returns a weight factor associated with n real emissions.
  ///
  /// Arguments:
  /// \param event        initial event (will be returned modified with at
  ///                     most n emissions)
  /// \param lnv_max      max value of the shower variable lnv
  /// \param lnv_min      min value of the shower variable lnv
  /// \param n_emissions  (max) number of requested emissions
  /// \param weight       event weight (should be passed initialised and
  ///                     will be returned updated)
  ///
  /// note that this does not include any virtual corrections
  /// #TRK_ISSUE-663  \todo: we may want to rename this (e.g. to indicate that a fixed coupling is used)
  void do_n_real_emissions(Event & event, double lnv_max, double lnv_min,
                           unsigned int n_emissions, double & weight);

  /// Generate an array of Sudakov form factors at values of the
  /// (logarithm of the) evolution variable lnv between lnv_max and
  /// lnv_min chosen such that the the ln(Sudakov form factor) goes
  /// down by roughly lnsudakov_spacing at each stage
  ///
  ///   \param event    event on which to base the Sudakov calculation
  ///   \param lnv_max  maximum value of the evolution variable
  ///   \param lnv_min  minimum value of the evolution variable
  ///   \param lnsudakov_spacing rough specification of the spacing in log(sudakov)
  ///   \param nev      the number of "trials" used to stimate the Sudakov
  void cache_sudakov_values_uniform_lnsudakov(Event & event, double lnv_max, double lnv_min,
                                              double lnsudakov_spacing, uint64_t nev = 1000);

  
  //-----------------------------------------------------------------
  /// @}
  /// @name main entry points to do a single emission
  /// @{
  //-----------------------------------------------------------------

  /// \enum GenerationMode
  /// decide how to treat a parameter
  enum GenerationMode {
    GenerationRandom           = 0,  ///< decide the parameter randomly
    GenerationFromEmissionInfo = 1,  ///< take the value stored in emission_info
    GenerationIntegrateOrSum   = 2   ///< integrate (or sum) over all allowed values
  };

  /// code which does a full emission
  /// It takes as input:
  /// \param event           the event to which we try adding an emission
  /// \param emission_info   an emission info where lnv, lnb and
  ///                        the element have already been decided
  /// \param weighted        true for weighted events (a pre-initialised
  ///                        weight is returned through "weight",
  ///                        false for unweighted events
  /// \param weight          event weight (for "unweighted" generation
  ///                        this would still include potential veto weights)
  /// \param phi_mode        how phi is generated (see GenerationMode above)
  ///                        random (default) means uniform in 0..2pi modulo optional spin effects
  /// \param pdgid_radiation_mode
  ///                        how the radiation PDGid is generated (see GenerationMode above)
  ///                        random (default) means according to PDF*ME^2
  /// \param i_splitting_end_mode
  ///                        how the splitter is selected (see GenerationMode above)
  ///                        random (default) follows the shower weight; for fixed values:
  ///                            1: split emitter
  ///                           -1: split spectator
  ///
  /// Note: for weighted events, when a specific splitting is requested
  /// (abs_pdgid or splitter), the corresponding fraction of the weight
  /// is included in the returned weight. E.g. if one explicitly asks
  /// for a gluon emission, the returned weight includes the probability
  /// for the emission to be a gluon. This is not included if the
  /// selection is done randomly.
  ///
  /// This returns the status of the attempted emission generation:
  ///   EmissionSuccess   if the emission has been genearted
  ///   EmissionVetoed    if the emission has been vetoed
  ///   AbortEvent        if the whole event should be aborted.
  ///
  /// #TRK_ISSUE-671 GS-NEW-CODE: [w Gavin]
  /// - We should probably think of implementing sth like
  ///   std::optional (or using it directly) is order to pass optional
  ///   arguments
  /// - Do we put these optional things directly in EmissionInfo?
  /// - do we put weight and/or weighted in EmissionInfo?
  ///
  EmissionAction do_one_emission(Event &event, std::unique_ptr<ShowerBase::EmissionInfo> &emission_info,
                                 bool weighted, double& weight,
                                 GenerationMode phi_mode            =GenerationRandom,
                                 GenerationMode pdgid_radiation_mode=GenerationRandom,
                                 GenerationMode i_splitting_end_mode=GenerationRandom);

  ///@}

  /// check if an emission is vetoed or not. If it is not and we are
  /// dealing with a weighted emission, compute its weight
  EmissionAction do_accept_emission(Event &event, std::unique_ptr<ShowerBase::EmissionInfo> &emission_info,
                                    bool weighted, double& weight,
                                    GenerationMode phi_mode            =GenerationRandom,
                                    GenerationMode pdgid_radiation_mode=GenerationRandom,
                                    GenerationMode i_splitting_end_mode=GenerationRandom);

  /// the 2nd half of do_one emission: once an emission has been accepted, do all the necessary updates
  EmissionAction do_process_emission(Event &event, std::unique_ptr<ShowerBase::EmissionInfo> &emission_info, bool weighted);

  /// computes the Sudakov form factor describing the no-emission
  /// probability between the hard scale lnv_max and a scale lnv_min
  ///   \param event     event to read from
  ///   \param lnv_max   maximum value of the evolution variable
  ///   \param lnv_min   minimum value of the evolution variable
  ///   \param nev       the number of MC points/lnv-bin to estimate the Sudakov (only relevant for MC integration)
  ///   \param precision Sudakov exponent integration precision target:
  ///                    only effective for _integration_strategy = GQ or GQ3
  Sudakov compute_first_sudakov(Event & event, double lnv_max, double lnv_min,
                                uint64_t nev = 1000,
                                double precision = 1e-6);
    
  /// Generates the first emission roughly uniformly between lnv_max and lnv_min
  /// and returns a Sudakov, which encodes the current lnv value and the weight
  /// associated with the Sudakov
  ///
  ///   \param event          event to read from
  ///   \param lnv_min        minimum value of the evolution variable
  ///   \param emission_info  the emission_info object in which to store register the outcome
  ///
  Sudakov generate_weighted_first_emission(Event & event, double lnv_min,
                    std::unique_ptr<ShowerBase::EmissionInfo> & emission_info);


  //-----------------------------------------------------------------
  /// @}
  /// @name Additional ways to generate emissions
  /// @{
  //-----------------------------------------------------------------

  // #TRK_ISSUE-673  Notes
  //  - we have not brought in anything using the "injection"
  //    mechanism (currently moved to the "deprecated" section
  //  - in some cases, we have removed the "g2qqbar" last argument
  //    [the idea is that one should use the versions which take
  //    EmissionInfo as a replacement


  /// generates a tree-level (fixed-order) event with n_emsn emissions;
  /// returns the weight associated with that event
  ///
  /// #TRK_ISSUE-679  \todo: should just become an alias to do_n_real_emission (unless
  /// we just want to remove do_n_real_emission and keep tree_level)
  double tree_level(Event & event, double lnv_max, double lnv_min, int n_emsn);
  
  /// returns a weight factor associated with a first order contribution
  ///   \param event    event to read from
  ///   \param lnv_max  maximum value of the evolution variable
  ///   \param lnv_min  minimum value of the evolution variable
  /// #TRK_ISSUE-674  \todo rename this first_order_weight
  double first_order(Event & event, double lnv_max, double lnv_min);

  /// returns a weight factor associated with a second order contribution
  ///   \param event    event to read from
  ///   \param lnv_max  maximum value of the evolution variable
  ///   \param lnv_min  minimum value of the evolution variable
  /// #TRK_ISSUE-676  \todo rename this second_order_weight
  double second_order(Event & event, double lnv_max, double lnv_min);

  
  /// Produces one emission with a fixed lnv
  /// \param event          event to read from (and write to)
  /// \param lnv            fixed value of lnv for the emission
  /// \param emission_info  an emission info where lnv has already been decided
  /// \param weight         event weight (should be passed initialised and
  ///                       will be returned updated)
  /// \param lnb_range      (optional) user-defined lnb range
  ///                       the appropriate 1/size_of_range factor is included in the weight
  /// \param ielement       (optional) force the index of the element to split
  ///                       -1 means random (thereby including a factor nelements in the weight)
  /// this returns the emission status
  /// #TRK_ISSUE-664 GS-NEW-CODE: should we instead return a tuple with the status and weight?
  ///GS-NEW-CODE: do we drop the "real" so as to keep the old naming convention
  ///             and have better backwards compatibility?
  EmissionAction do_one_real_emission_with_fixed_lnv(Event & event, double lnv, 
                                                     std::unique_ptr<ShowerBase::EmissionInfo> &emission_info,
                                                     double &weight,
                                                     Range lnb_range = Range(0,0), int ielement = -1);


  /// Generate an array of Sudakov form factors at equally spaced
  /// values of the (logarithm of the) evolution variable lnv
  /// between lnv_max and lnv_min
  ///   \param event    event to read from
  ///   \param lnv_max  maximum value of the evolution variable
  ///   \param lnv_min  minimum value of the evolution variable
  ///   \param nev      the number of "trials" used to stimate the Sudakov
  void cache_sudakov_values_uniform_lnv(Event & event, double lnv_max, double lnv_min,
                                        unsigned int npoints, uint64_t nev = 1000);

  //-----------------------------------------------------------------
  /// @}
  /// @name Deprecated interfaces meant mostly for backwards compatibility
  /// @{
  //-----------------------------------------------------------------
  
  /// version with extra arguments, currently unsupported
  /// \deprecated Use the above version of first_order instead
  double first_order(Event & event, double lnv_max, double lnv_min, 
                     const std::vector<Injection> & injections,
                     double lnb_window = -1, bool g2qqbar=false);


  /// version with extra arguments, currently unsupported
  /// \deprecated Use the above version of second_order instead
  double second_order(Event & event, double lnv_max, double lnv_min, 
                      const std::vector<Injection> & injections,
                      double lnb_window = -1, bool g2qqbar=false);

  /// Produces one emission with a fixed lnv, lnb, phi
  /// \param event              event to read from (and write to)
  /// \param i_element          index of the element to split
  /// \param lnv                fixed value of lnv for the emission
  /// \param lnb                fixed value of lnb for the emission
  /// \param phi                the azimuth for the emission (phi is supposed to be in [0,2*pi])
  /// \param pdgid_radiation    the pdgid of the radiation (0 = select as normal)
  /// \param i_splitting_end    determines which dipole end splits (0 = select as normal, 1 = emitter, -1 = spectator)
  ///
  /// \deprecated Use ShowerRunner::do_one_emission instead
  //
  /// #TRK_ISSUE-665 NEW-CODE the ordering of the arguments is different from
  /// above. This is ugly but cannot be changed above (since the
  /// argument is optional) and changing it here would break backwards
  /// compatibility. There might be approaches using an extra
  /// intermediate function but this seems (i) overkill and (ii) not
  /// really helpful
  ///
  /// #TRK_ISSUE-666 NEW-CODE: do we want the pdgid or its absolute value? The
  /// former is more flexible but adds the non-negligible extra pain
  /// that one has to specify carefully the signe based on which end of
  /// the dipole we emit from (which may even be impossible for antenna
  /// showers without also specifying the splitter index)
  ///
  /// #TRK_ISSUE-667 NEW-CODE: this is a case where using std::optional would avoid
  ///the danger of fixing a value to indicate that we should select
  ///things randomly
  ///
  /// #TRK_ISSUE-668 NEW-CODE: do we also want to return the EmissionAction?
  ///
  /// #TRK_ISSUE-669 NEW-CODE: do we want this at all (it's mostly for backwards compatibility)
  ///
  /// #TRK_ISSUE-670 NEW-CODE: this does not have the optional emisison_info
  ///(pointer) argument which would not work because do_one_emission
  ///currently takes a unique_ptr.
  double do_one_real_emission_with_fixed_lnv_lnb(Event & event, unsigned int i_element,
                                                 double lnv, double lnb, double phi=-1,
                                                 int pdgid_radiation=0, int i_splitting_end=0);
  double do_one_real_emission_with_fixed_lnv_lnb(Event & event, std::unique_ptr<ShowerBase::EmissionInfo> &emission_info,
                                                 unsigned int i_element,
                                                 double lnv, double lnb, double phi=-1,
                                                 int pdgid_radiation=0, int i_splitting_end=0);
  
  // /// Produces one emission with a fixed lnv
  // ///   \param event     event to read from (and write to)
  // ///   \param lnv       fixed value of lnv for the emission
  // ///   \param lnb_range (optional) user-defined lnb range
  // ///   \param ielement  (optional) force the index of the element to split
  // double do_one_emission_with_fixed_lnv(Event & event, double lnv, Range lnb_range = Range(0,0), int ielement = -1);

  /// Produces one emission with a fixed lnv, lnb, phi
  ///   \param event                event to read from (and write to)
  ///   \param i_element            index of the element to split
  ///   \param lnv                  fixed value of lnv for the emission
  ///   \param lnb                  fixed value of lnb for the emission
  ///   \param phi                  the azimuth for the emission (phi is supposed to be in [0,2*pi])
  ///   \param abs_pdgid_radiation  the pdgid of the radiation (0 = select as normal)
  ///   \param i_split              determines which dipole end splits (0 = select as normal, 1 = emitter, -1 = spectator)
  ///
  /// \deprecated  Use ShowerRunner::do_one_emission instead
  double do_one_emission_with_fixed_lnv_lnb(Event & event, unsigned int i_element,
                                            double lnv, double lnb, double phi=-1, int pdg_id_radiation=0, int i_split=0,
                                            ShowerBase::EmissionInfo *emission_info = nullptr);


  /// Switch for double-soft corrections
  void enable_double_soft(bool enable = true, bool enable_dKCMW = true, double dKCMW_error_offset_factor=0.0, bool dKCMW_subtract_fit=false) {
    shower()->enable_double_soft(enable, enable_dKCMW, dKCMW_error_offset_factor, dKCMW_subtract_fit);
  }
  
  //----------------------------------------------------------------------
  /// @}
  /// @name Shower inversion and related tools
  /// @{
  ///
  /// #TRK-ISSUE-803 2023-12-04: the code of the following functions
  /// should probably be moved into the individual showers.
  //----------------------------------------------------------------------
  
  /// Find lnv, lnb, phi from z, delta, phi in the collinear approximation
  /// and returns the Jacobian dz ddelta dphi = |J| dlnv dlnb dphi
  ///   \param event          event to read from (and write to)
  ///   \param ielement       index of the element to split
  ///   \param emitter_splits true if the emitter splits collinearly (in case of antenna shower)
  ///   \param z_target       momentum fraction of the collinear splitting, 
  ///                         defined as the momentum fraction of the gluon for q->qg
  ///                         defined as the fraction of the outgoing q end if it is a gluon splitting
  ///   \param delta_target   opening angle of the collinear splitting
  ///   \param phi_target     phi of the collinear splitting, as defined with respect to the splitting momentum and the x axis
  ///   \param lnv            reference to lnv variable (to write to)
  ///   \param lnb            reference to lnb variable (to write to)
  ///   \param phi            reference to phi variable (to write to)
  double find_lnv_lnb_phi_from_z_delta_phi_collinear(Event &event, int i_element, bool emitter_splits,
    double z_target, double delta_target, double phi_target, double &lnv, double &lnb, double &phi);

  /// Find lnv, lnb, phi from z, delta, phi in the soft approximation
  /// and returns the Jacobian dz ddelta dphi = |J| dlnv dlnb dphi
  ///   \param event          event to read from (and write to)
  ///   \param ielement       index of the element to split
  ///   \param z_target       momentum fraction of the soft emission with respect to the event energy
  ///   \param delta_target   polar angle of the soft emission
  ///   \param phi_target     azimuthal angle of the soft emission
  ///   \param lnv            reference to lnv variable (to write to)
  ///   \param lnb            reference to lnb variable (to write to)
  ///   \param phi            reference to phi variable (to write to)
  double find_lnv_lnb_phi_from_z_delta_phi_soft(Event &event, int i_element,
    double z_target, double delta_target, double phi_target, double &lnv, double &lnb, double &phi);


  //-----------------------------------------------------------------
  /// @}
  /// @name Parts related to matching
  /// @{
  //-----------------------------------------------------------------

  
  /// Insert a MatchedProcess in ShowerRunner.
  void set_matching(MatchedProcess * matching_ptr) {_matching_ptr = matching_ptr; }

  /// Access the MatchedProcess in ShowerRunner.
  MatchedProcess * getMatching() const { return _matching_ptr; };

  /// @brief  type for returning the MCatNLOIntegral
  struct MCatNLOIntegral {
    double integral = 0, err = 0;
    double integral_abs = 0, err_abs = 0;
  };
  
  /// This function computes the integral of (R-R_s) and |R-R_s| in MC@NLO-style
  /// matching, which is used to define the probability of generating
  /// a hard event vs. a shower event, as well as the overall event weight
  ///
  /// the answer is returned as a MCatNLOIntegral
  MCatNLOIntegral compute_mcatnlo_integral(Event & event, double lnv_max, double lnv_min,
                                           uint64_t nev = 1000);

  /// set the shower for matching
  void set_shower_for_matching(ShowerBase * shower_for_matching_ptr){
    _main_shower         = _shower;
    _shower_for_matching = std::shared_ptr<ShowerBase>(shower_for_matching_ptr);
  }
  
  /// return a const pointer to the shower used for matching
  const ShowerBase * shower_for_matching() const {return _shower_for_matching.get();}
  /// return a non-const pointer to the shower (use with caution)
  ShowerBase * shower_for_matching() {return _shower_for_matching.get();}
  


  //-----------------------------------------------------------------
  /// @}
  /// @name Parts related to spin correlations
  /// @{
  //-----------------------------------------------------------------

  /// return a pointer to the event's spin-correlation tree
  SpinCorTree* get_spin_cor_tree_ptr() {return &_spin_cor_tree;}
    
  // Switch off correlations
  void disable_spin_correlations() {_spin_cor_tree.disable_spin_correlations();}

  // Switch off soft corrections
  void disable_soft_spin_corrections() {_spin_cor_tree.disable_soft_spin_corrections();}

  // Switch off collinear spin projection
  void disable_collinear_spin_projection() {_spin_cor_tree.disable_collinear_spin_projection();}

  // enables/disables the on-the-fly declustering analysis for spin
  void set_do_declustering_analysis(bool value) {_spin_cor_tree.set_do_declustering_analysis(value);}

  // Store branching information in the spin tree (useful for certain analyses)
  void store_extra_branching_info(bool value) {_spin_cor_tree.store_extra_branching_info(value);}
  
  //-----------------------------------------------------------------
  /// @}
  /// @name Tools meant for controlling the flow (caching, interrupt/resume, ...)
  /// @{
  //-----------------------------------------------------------------

  /// set/get the step-by step event caching
  void set_step_by_step_event_caching(bool value){ _do_cache_step_by_step_event = value; }
  bool step_by_step_event_caching() const { return _do_cache_step_by_step_event; }

  /// access the cached step-by-step events
  const StepByStepEvent & cached_step_by_step_event() const { return _step_by_step_event; }

  // ------- things related to the GSL state
  void set_save_state_on_fail  (bool value) { _save_state_on_fail   = value; }
  bool save_state_on_fail()   const { return _save_state_on_fail;   }
  void copy_state(const GSLRandom &gsl_to_copy) { _gsl_copy.copy_state(gsl_to_copy);}
  GSLRandom get_gsl_copy()        const { return _gsl_copy; }

  //----------------------------------------------------------------------
  /// @}
  /// @name Extra access for interfacing w external tools (like Pythia)
  /// @{
  // ----------------------------------------------------------------------

  /// initialise the run
  void initialise_run(Event & event, double lnv = 0);

  /// get the next (candidate) splitting element (also fills lnv with
  /// the next candidate splitting scale)
  typename ShowerBase::Element * find_next_element_and_lnv(Event &event, double & lnv){
    return (this->*_find_element_alg_ptr)(event, lnv);
  }

  /// get the next (candidate) lnb
  bool find_next_lnb(Event & event,
		     typename ShowerBase::Element *element,
		     double lnv, double &lnb);

  /// Allow the outside world (e.g. the pythia interface) to set lnvfirst
  void set_lnvfirst(double lnvfirst){ _lnvfirst = lnvfirst; }


  //-----------------------------------------------------------------
  /// @}
  /// @name Additional low-level configuration and information 
  /// @{
  //-----------------------------------------------------------------

  /// returns which strategy is used to find the next element
  Strategy strategy() const {return _strategy;}

  /// returns a string description of the strategy used to find the next element
  std::string strategy_description() const;

  /// For CentralRap generation strategy only: allow setting the extent of the
  /// η_approx window either side of the dipole centre (at min |η_approx|
  /// for each dipole):
  void half_central_rap_window(double half_central_rap_window) {
    _half_central_rap_window = half_central_rap_window;
  }
  /// and getting it out too:
  double half_central_rap_window() const { return _half_central_rap_window; }

  /// For CollinearRap generation strategy only: allow setting the extent of the
  /// η_approx window either side of the dipole centre (at min |η_approx|
  /// for each dipole):
  void collinear_rap_window(double collinear_rap_window) {
    _collinear_rap_window = collinear_rap_window;
  }
  /// and getting it out too:
  double collinear_rap_window() const { return _collinear_rap_window; }

  /// retrieve the elements
#ifndef SWIG
  const std::vector<std::unique_ptr<typename ShowerBase::Element> > & elements() const{ return _elements;}
#endif
  
  /// Allow the outside world to get their hands on lnv of the first branching
  double lnvfirst() const {return _lnvfirst;}
  double lnbfirst() const {return _lnbfirst;}
  
  /// writes out the Sudakov values (e.g. for the header) so that one
  /// view what is going on
  void write_sudakov_values(std::ostream & ostr) const;

  /// Sets the shower runner's default emission enhancement factor for all
  /// elements from the outside world.
  void enhancement_factor(double enhancement_factor);

  //----------------------------------------------------------------------
  // some hooks to tweak the behaviour of double soft for fixed-order emissions

  /// allows one to disable the colour-flow swaps associated with
  /// double-soft corrections (WARNING: this will not procude the
  /// right double-soft matrix element)
  void disable_double_soft_colour_flow_swaps(bool value=true) {_double_soft_no_colour_flow_swap = value;}

  /// by default, the colour and flavour swaps are determined
  /// randomly. These hooks allow one to force that one gets a fixed
  /// flavour after applying the double-soft corrections.
  /// A 0 argunent means random sampling. 
  void set_double_soft_abspdgid_for_fixed_emission(unsigned int pdgid=0){
    _double_soft_fixed_emission_pdgid = pdgid;
  }


  //----------------------------------------------------------------------
  /// @brief  instruct the shower runner to dump info about the PDFs to a file
  /// @param pdf_dump_filename: the filename to dump to
  void dump_PDFs(const std::string & pdf_dump_filename){ _pdf_dump_filename = pdf_dump_filename;}


  /// @}
  //======================================================================

  
private:

  //-----------------------------------------------------------------
  /// 
  /// @name Main steps for doing one emission
  /// @{
  //-----------------------------------------------------------------

  
  /// Initialise shower element info storage based on the input event
  void _initialise_run(Event &event, double lnv, bool post_matching_reinitialisation=false);

  /// returns the acceptance probability associated with alphas
  Weight _alphas_acceptance_probability(const typename ShowerBase::Element *element,
                                        typename ShowerBase::EmissionInfo *emission_info) const;


  /// helper to compute the acceptance probability
  ///
  /// This combines information from the shower's
  /// acceptance_probability and, when needed the PDFs.
  ///
  /// Shower::acceptance_probability fills
  ///   emission_info::emitter_weight_rad_gluon
  ///   emission_info::emitter_weight_rad_quark
  ///   emission_info::spectator_weight_rad_gluon
  ///   emission_info::spectator_weight_rad_quark
  /// from which this method computes and returns a unique acceptance
  /// probability and emission_info::emitter_splitting_weight 
  ///
  ///GS-NOTE: should this (and the other guys below) be const?
  inline Weight _acceptance_probability(const typename ShowerBase::Element *element,
                                        typename ShowerBase::EmissionInfo *emission_info);
  
  /// helper to decide, based on the information provided by
  /// Element::acceptance_probability, which end of the dipole splits
  /// and into which channel.
  ///
  /// This uses
  ///  - emission_info::emitter_splitting_weight            (possibly only for antenna showers)
  ///  - emission_info::emitter_splitting_weight_g2qqbar
  ///  - emission_info::spectator_splitting_weight_g2qqbar  (only for antenna showers)
  /// and fills
  ///  - emission_info.do_split_emitter
  ///  - emission_info.radiation_abs_pdgid
  ///  - emission_info.splitter_out_pdgid
  ///
  /// The requested_radiation_abs_pdgid argument allows one to enforce
  /// the PDGid of the radiation. Setting it to 0 (the default) yields
  /// to a random selection (using the information in emission_info)
  ///
  /// Similarly, a splitter can be enforced through the "reqyuested_channel" argument:
  ///   0 correspoinds to a random selection w the appropriate weight (the defaukt)
  ///  -1 corresponds to splitting the spectator
  ///  +1 corresponds to splitting the emitter
  ///
  /// This method returns the weight corresponding the (optional)
  /// requested pdgid and channel.
  ///
  /// Note that if the weight is 0, the entries in emission_info
  /// supposedly set by this function are not guaranteed to be valid.
  ///
  /// If this is called with pre_matching=true, then it only sets
  /// do_split_emitter, and the other two are left ill-defined 
  /// (because they will get set only by a separate call after matching)
  Weight _select_channel_details(const typename ShowerBase::Element *element,
                                 typename ShowerBase::EmissionInfo * emission_info, 
                                 unsigned int requested_radiation_abs_pdgid=0,
                                 int requested_channel = 0,
                                 bool pre_matching = false);

  /// helper to compute the PDF factors associated with a given particle
  /// (emitter or spectator)
  ///
  /// This fills the pdf_factor_quark and pdf_factor_gluon coresponding
  /// quark and gluon radiation,respectively
  inline void _compute_pdf_factor(typename ShowerBase::EmissionInfo *emission_info,
                                  const Particle & particle,
                                  bool is_particle_at_3_end, 
                                  precision_type pdf_new_x_beam1,
                                  precision_type pdf_new_x_beam2,
                                  double & pdf_factor_quark_rad,
                                  double & pdf_factor_gluon_rad) const;
  
  /// Apply the kinematic map, i.e. fill the 4-momenta of the post-branching emitter, spectator and radiation
  /// Do kinematics can return false for 2 reasons:
  ///  - the shower do_kinematics failed
  ///  - a PDF fraction goes above 1
  bool _do_kinematics(typename ShowerBase::Element * element,
                      typename ShowerBase::EmissionInfo * emission_info) const;

  /// returns the spin acceptance probability.
  /// For "fixed_phi", a single value is uses. Otherwise, we loop
  /// until a random phi is accepted.
  Weight _spin_acceptance(typename ShowerBase::Element * element,
                          typename ShowerBase::EmissionInfo * emission_info,
                          bool inside_phi_loop);

  /// double-soft deltaK correction
  /// this returns a number that can be understood as an extended probability
  Weight _double_soft_dKCMW_acceptance(typename ShowerBase::Element * element,
                                       typename ShowerBase::EmissionInfo * emission_info) const;
  
  /// double-soft matrix-element correction
  double _double_soft_ME_acceptance(typename ShowerBase::Element * element,
                                    typename ShowerBase::EmissionInfo * emission_info) const;

  /// double-soft swaps
  void _double_soft_swaps(typename ShowerBase::Element * element,
                          typename ShowerBase::EmissionInfo * emission_info,
                          Weight &weight);

  // steps related to matching
  // The main matched emission process is done in 3 steps:
  //  - compute the acceptance probability
  //  - if (accepted | mc@nlo matching): update spin and acceptance
  //  - for MC@NLO matching,  apply the acceptnace at the end

  /// matching acceptance probability
  /// this is suited for a treatment with handle_probability_extended.
  ///
  /// The flag postpone_for_mcatnlo will be set to true in case of
  /// MC@NLO matching. In this case, the acceptance probability only
  /// needs to be imposed after the treatment of spin and colour (the
  /// physics reason behind comes from a subtraction term in the
  /// colour step). In this case, the probablility is returned as a
  /// number >1 so that it passes thransparently through
  /// handle_probability_extended.
  Weight _matching_acceptance_probability(Event & event,
                                          typename ShowerBase::Element * element,
                                          typename ShowerBase::EmissionInfo * emission_info,
                                          bool weighted,
                                          bool &mcatnlo_matching,
                                          bool &discard_emission);

  /// apply the updates needed once we know a matched emission has been accepted
  /// this returns the colour acceptance (needed later for MC@NLO matching)
  double _matching_post_acceptance_update(Event &event,
                                          typename ShowerBase::Element * element,
                                          typename ShowerBase::EmissionInfo * emission_info);
  
  /// apply the updates needed once we know a matched emission has been accepted
  /// this returns the colour acceptance (needed later for MC@NLO matching)
  /// This returns a probability that can go through handle_probability_lazy
  double _matching_finalize_mcatnlo(typename ShowerBase::Element * element,
                                    typename ShowerBase::EmissionInfo * emission_info,
                                    double ps_acceptance,
                                    double matching_acceptance,
                                    double colour_acceptance);
  
  
  /// return true if the emission is accepted, false if it is vetoed
  bool _post_matching_contour_veto(bool match_emission,
                                   typename ShowerBase::EmissionInfo * emission_info);
  

  /// performs all the steps needed to update the event
  bool _update_event(Event & event,
                     typename ShowerBase::Element * element,
                     typename ShowerBase::EmissionInfo * emission_info);
  
  /// Set up everything we need to match the upcoming emission
  /// Other strategies can be used for matched runs,
  /// by using RoulettePeriodic for the first emission only
  /// and then switching to the one requested by the user.
  ///  
  /// #TRK_ISSUE-683   - we still need to work out what happens if the main
  ///    strategy needs some kind of initialisation (not
  ///    the case for CentralRap, but we haven't ruled it out for others)
  void _initialise_matching_for_event();

  /// Turn the matching off for the rest of the emissions in the event
  void _terminate_matching_for_event(Event & event,
                                     typename ShowerBase::EmissionInfo * emission_info);

  /// Returns true if Powheg-style matching is currently on for that event, i.e. if:
  ///   - the current trial emission is the first (event.can_be_matched() return value)
  ///   - matching is on, at all (_matching_ptr is set)
  ///   - the matching scheme is Powheg-like (only for unweighted events)
  bool _match_current_emission(const Event & event, bool weighted=false) const;

  // double-soft-related material
  //--------------------------------------------------------------------------------
  /// handle the flavour swap
  void _double_soft_flavour_swap(typename ShowerBase::EmissionInfo * emission_info) const;

  /// guarantee that the splitter is set to the doube-soft partner
  void _double_soft_set_splitter_to_partner(typename ShowerBase::EmissionInfo * emission_info) const;

  //-----------------------------------------------------------------
  /// @}
  /// @name Helpers for approximately weighting the first emissions according to the Sudakov
  /// @{
  //-----------------------------------------------------------------
  
  /// Given a density that goes as a + b*lnv (i.e. implicitly a
  /// fixed-coupling approximation), and a current value of lnv, this
  /// routine generates and returns a random choice for the next lnv
  inline double _sudakov_next_lnv(double current_lnv, double a, double b);

  /// returns the integrand of the Sudakov w.r.t lnv and lnb.
  ///   \param event    To input to _colour_factor_acceptance.
  ///   \param element  For access to acceptance_probability and alphas_lnkt.
  ///   \param lnv      Current value of evolution variable.
  ///   \param lnb      Current auxiliary splitting variable lnb.
  ///   \param integrate_phi   when true, integrate over phi and
  ///                          test the outcome of do_kineamtics.
  double _lnv_lnb_integrand(Event & event, typename ShowerBase::Element *element,
                            double lnv, double lnb, bool integrate_phi, double precision);

  /// integration of the single-emission weight between lnvmin and
  /// lnvmax using a Gaussian Quadrature approach.
  ///  \param event      the initial event to emit from
  ///  \param element    within the event, emit from this element
  ///  \param lnv_min    lower integration bound
  ///  \param lnv_max    upper integration bound
  ///  \param precision  target accuracy
  ///  \param integrate_phi    when true, integrate over phi and
  ///                          test the outcome of do_kineamtics.
  double _integrate_over_lnv_gauss(Event & event, typename ShowerBase::Element *element,
                                   double lnv_min, double lnv_max,
                                   double precision,
                                   bool integrate_phi);

  /// integration of the single-emission weight between lnvmin and
  /// lnvmax using a Monte Carlo approach.
  ///  \param event      the initial event to emit from
  ///  \param elements   list of elements to include in the integral
  ///  \param lnv_min    lower integration bound
  ///  \param lnv_max    upper integration bound
  ///  \param nev        number of Monte Carlo points
  ///  \param result     filled by this function with the result of the integration
  ///  \param error      filled by this function with the estimated integration error
  void _integrate_over_lnv_mc(Event & event,
                              std::vector<std::unique_ptr<typename ShowerBase::Element> > &elements, 
                              double lnv_min, double lnv_max,
                              uint64_t nev,
                              double &result, double &error);

  /// auxiliary function for gaussian quadrature (gq) integration option in
  /// compute_first_sudakov, for determining lnb integration limits in there.
  ///   \param event    For calls to get _lnv_lnb_integrand.
  ///   \param element  For calls to get _lnv_lnb_integrand.
  ///   \param lnv      Current value of evolution variable.
  ///   \param lnb      Current auxiliary splitting variable lnb.
  ///   \param integrate_phi   when true, integrate over phi and
  ///                          test the outcome of do_kineamtics.
  ///   \param lnb_min  Passed by reference. On the way in this sets the
  ///                   underestimated (envelope) lnb integration limit
  ///                   with which to start searching for a better one from.
  ///                   In the course of the function execution this will
  ///                   be overwritten by the improved integration limit.
  ///   \param lnb_max  Analogous to lnb_min argument, but dealing with the
  ///                   upper lnb integration limit.
  ///
  /// #TRK_ISSUE-680  GPS-20190330:
  /// we'd really like event here to be const;
  /// - but this routine calls Shower::elements(event,...)
  /// - the elements end up having a non-const pointer/ref to the event
  /// - this is necessary because do_split can then modify the event
  ///
  /// An alternative way of doing things might be that
  /// - elements only get a const ref to the event
  /// - do_split must be supplied with a non-const event to act on
  ///   (it might also check that the element's event and the actual event are the same)
  void _gq_integration_lnb_limits(Event & event, typename ShowerBase::Element *element,
                                  double lnv, bool integrate_phi, double precision,
                                  double & lnb_min, double & lnb_max);

  
  //-----------------------------------------------------------------
  /// @}
  /// @name Helpers for handling the element store
  /// @{
  //-----------------------------------------------------------------


  /// Add an element corresponding to a given dipole to
  /// element_vector, update cached info
  void _add_dipole_elements_to_store(Event &event, unsigned int dipole_index, double lnv);

  /// update the kinematics of an element and update cache accordingly
  void _update_element_in_store(unsigned int element_index, double lnv);

  /// update the kinematics of an element and update cache accordingly
#ifndef SWIG
  void _update_element_in_store(unsigned int element_index, double lnv,
                                std::unique_ptr<typename ShowerBase::Element> & last_element);
#endif
  
  /// update all elements after shift in lnv, prior to actual splitting
  void _update_elements_in_store_pre_splitting(Event & event, typename ShowerBase::Element * element, double lnv);

  /// update all elements affected by last branching in the shower
  void _update_elements_in_store_post_splitting(Event &event, double lnv, typename ShowerBase::EmissionInfo *emission_info);

  /// update all elements affected by last branching in the shower
  void _update_max_astar_min_b(double lnv);

  
  //-----------------------------------------------------------------
  /// @}
  /// @name Strategies for finding the next lnv/element
  ///
  /// This consists of a list of algorithms for deciding which element split at which lnv.
  ///
  /// for a given event and current lnv, these algs updates lnv to the
  /// value at which the next splitting occurs and returns the element
  /// which does split.
  /// @{
  //-----------------------------------------------------------------

  /// Set pointers according to the chosen generation strategy
  void _set_strategy_pointers();

  /// An N2 algorithm which uses the pythia approach where we try to
  /// split each single dipole and select the one with the max lnv. In
  /// this "historical"version, no caching is used and all the
  /// elements are re-generated on the spot.
  typename ShowerBase::Element* _find_element_pythia_historical_alg(Event & event, double & lnv);

  /// The initial trial lnv generation is based on the total density
  /// (tot_astar+tot_b lnv). Channel selection is done with
  /// stochastic roulette, using an overestimated weight for all
  /// channels min(b)*lnv+max(astar), where min(b) and max(astar) are
  /// determined periodically, every time the event evolves a unit of
  /// lnv. The min(b), max(astar) determination here is hence
  /// #TRK_ISSUE-687  identical to RouletteOneShot. To do: determining min(b)
  /// repeatedly in this way is not very clever, as b will only ever
  /// take about 3 values (unless we start throwing out some basic
  /// assumptions we've held to now about how the code might be built
  /// on). Equally, the periodicity for refreshing max(astar), while
  /// good, can probably be optimised (with limited return).
  typename ShowerBase::Element* _find_element_RoulettePeriodic_alg(Event & event, double & lnv);

  /// Sudakov is product of identical Sudakovs for each element, each one
  /// having a->max(astar), b->min(b), where max(astar) and min(b) overestimate
  /// a and b, and are periodically updated (every unit of lnv) as the event
  /// evolves.
  typename ShowerBase::Element* _find_element_RouletteOneShot_alg(Event & event, double & lnv);

  /// RoulettePeriodic rearranging elements into bins of their enhancement factor. 
  typename ShowerBase::Element* _find_element_EnhancedRoulette_alg(Event & event, double & lnv);

  /// default algorithm for finding lnb (just samples within range)
  bool _find_lnb_default_alg(const typename ShowerBase::Element * element, double lnv, double & lnb);

  /// functions for handling central case where we generate only central
  /// rapidities
  typename ShowerBase::Element* _find_element_central_rap_alg(Event & event, double & lnv);
  bool _find_lnb_central_rap_alg(const typename ShowerBase::Element * element, double lnv, double & lnb);

  /// functions for handling central case where we generate only
  /// _collinear_ rapidities
  typename ShowerBase::Element* _find_element_collinear_rap_alg(Event & event, double & lnv);
  bool _find_lnb_collinear_rap_alg(const typename ShowerBase::Element * element, double lnv, double & lnb);

  /// functions for handling case where the above two strategies are combined
  typename ShowerBase::Element* _find_element_central_and_collinear_rap_alg(Event & event, double & lnv);
  bool _find_lnb_central_and_collinear_rap_alg(const typename ShowerBase::Element * element, double lnv, double & lnb);

  
  //-----------------------------------------------------------------
  /// @}
  /// @name Additional helpers:
  /// - initialisations
  /// - overhead updates
  /// @{
  //-----------------------------------------------------------------

  /// set the default behaviour for the double-soft treatment
  void _init_double_soft_defaults();

  /// set all the uncertainty-related overhead factors based on the shower 
  /// uncertainty parameters
  void _initialise_overhead_factors();

  /// update the global overhead factor
  void _update_global_overhead_factor();


  //-----------------------------------------------------------------
  /// @}
  /// @name Verbose output and validation tools
  /// @{
  //-----------------------------------------------------------------
 
  void _verbose_info_shower_acceptance(double ps_accept_prob,
                                       const Event &event,
                                       typename ShowerBase::EmissionInfo *emission_info) const;
  void _verbose_info_acceptance_above_one_pp(double accept_prob,
                                             const Event &event,
                                             typename ShowerBase::EmissionInfo *emission_info) const;
  void _verbose_info_phi_selection(const Event &event,
                                   typename ShowerBase::EmissionInfo *emission_info) const;
  void _verbose_info_do_kinematics(const Event &event,
                                   typename ShowerBase::EmissionInfo *emission_info) const;
  void _verbose_info_acceptance_above_one_DIS(double accept_prob,
                                             const Event &event,
                                             typename ShowerBase::EmissionInfo *emission_info) const;

  /// check that the q/qbar indices of the cached elements match with what
  /// one should expect from the set of dipoles in the event
  bool _check_cached_element_indices(Event &event) const;


  //========================================================================
  //
  // Unsorted
  //
  //========================================================================

  
  //-----------------------------------------------------------------
  /// @}
  /// @name Internal variables
  /// @{
  //-----------------------------------------------------------------
  
  /// holds a pointer to the shower (and takes ownership)
  std::shared_ptr<ShowerBase> _shower;

  /// holds a pointer to the main shower (and takes ownership)
  std::shared_ptr<ShowerBase> _main_shower;

  /// holds a pointer to the shower that is used for the first matched emission
  std::shared_ptr<ShowerBase> _shower_for_matching;
  
  /// the strategy to be used for event generation
  Strategy _strategy, _strategy_no_matching;

  /// tool to handle colour transitions
  ColourTransitionRunnerBase *_colour_transition_runner;

  /// Tree for spin correlations
  SpinCorTree _spin_cor_tree;

  //--------------------------------------------------------------------------------
  // cached run info
  //--------------------------------------------------------------------------------
  
  /// the elements associated w the current shower
  std::vector<std::unique_ptr<typename ShowerBase::Element> > _elements;
  
  /// cache the number of dipoles (this is used to check whether a
  /// splitting changed the number of dipoles, so new elements have
  /// to be added to the store, or not)
  /// This is updated by _update_elements_in_store_post_splitting()
  unsigned int _cached_ndipoles;
  
  /// cached totals
  double _tot_a, _tot_b, _tot_astar;

  /// Return value of last max astar check, and last min b check,
  /// among all shower elements
  double _max_astar;
  double _min_b;
  double _max_astar_o_min_b;
  /// The lnv value at which _max_astar and _min_b were last determined
  /// inside _update_elements_in_store_post_splitting
  double _last_searched_lnv;

  /// Enhanced Roulette is intended as an optimised version of RoulettePeriodic,
  /// specifically for the case that elements carry emission enhancement factors.
  EnhancedRoulette _enhanced_roulette;
  
  /// Enhanced Roulette's shower caching and updates takes place offsite, outside
  /// of ShowerRunner. This flag tells ShowerRunner if and when to run those calls.
  bool _do_enhanced_roulette;

  /// pointer to (strategy-dependent) function that chooses the next lnv
  /// and returns the element that is candidate for next branching
  /// #TRK_ISSUE-688  GPS query (2020-08-28): should it return a reference to the element perhaps?
  typename ShowerBase::Element* (ShowerRunner::*_find_element_alg_ptr)(Event & event, double & lnv);

  /// pointer to strategy-dependent algorithm for finding lnb Returns
  /// true if it managed to generate an lnb value (which is assigned to
  /// the lnb variable); returns false if it failed, in which case the
  /// event 
  bool (ShowerRunner::*_find_lnb_alg_ptr)(const typename ShowerBase::Element * element, double lnv, double & lnb);

  /// value of lnv at which the first emission has been generated
  /// (used for weighted event generation)
  double _lnvfirst, _lnbfirst;

  /// value of lnv at which the shower hs been started
  /// (used to match power showers)
  double _lnvmax;
  
  /// vector containing the values of the first emission Sudakov
  /// at a fixed number of points [used for weighted generation]
  //
  // #TRK_ISSUE-689  GPS-20190330: the use of a tuple here makes for harder-to-read
  // code later on. I would create a mini sub-class, e.g.
  ///
  // class Sudakov {
  //   double lnv, sudakov_value;
  // };
  // vector<Sudakov> _sudakov_array;
  std::vector<Sudakov> _sudakov_array;
  bool _sudakov_array_is_uniform = false;
  //std::tuple<std::vector<double>, std::vector<double>> _sudakov_array;

  /// Kind of integration method is to be used in
  /// compute_first_sudakov. So far we have MC for Monte Carlo,
  /// GQ for Gaussian-Quadrature and GQ3 for 3D Gaussian quadrature.
  IntegrationStrategy _integration_strategy = MC;

  //--------------------------------------------------------------------------------
  // Stuff relating to the object that is going to veto emissions
  // effecting an observable-dependent dynamic cut-off
  //--------------------------------------------------------------------------------

  // Set default veto object pointer to NULL. The user can overwrite
  // this by creating a veto object and supplying its pointer to the
  // shower with set_veto(<the pointer>).
  std::unique_ptr<EmissionVeto<ShowerBase>> _veto_ptr;

  /// Default factor which emission rate for elements is to be enhanced by,
  /// for the purposes of reweighting in combination with vetoing
  double _enhancement_factor;

  /// security margin/tolerance for acceptance probabilities 
  ///
  /// accceptance probabilities are allowed to go above 1 by this
  /// security margin multiplied by the precision_type epsilon
  /// The largest deviation we have seen so far is 2 epsilon. 
  //static const unsigned int ACCEPTANCE_PROBABILITY_MARGIN = 10;
  //static const unsigned int ACCEPTANCE_PROBABILITY_MARGIN = 10;
  static const unsigned int ACCEPTANCE_PROBABILITY_MARGIN;

  /// accceptance probabilities are allowed to go above 1 by this
  /// security margin multipled by the precision_type epsilon
  /// The largest deviation we have seen so far is about 20 epsilon.
  static const unsigned int MATCHING_ACCEPTANCE_PROBABILITY_MARGIN = 100;

  /// Operative only for generation strategy CentralRap, wherein only emissions
  /// in the central rapidity region of each dipole are generated. The following
  /// holds the extent of the η_approx emission window, either side of the 
  /// dipole centre at min(|η_approx|) for each given dipole.
  double _half_central_rap_window;

  /// Operative only for generation strategy CollinaerRap, wherein only
  /// emissions in the collinear rapidity region of a dipole are
  /// generated (both collinear regions for an antenna shower). The
  /// following holds the extent of the η_approx emission window in the
  /// collinear region
  double _collinear_rap_window;

  /// when true, cache the events and basic emisison info at each step
  /// of the showering process (uses the structure below to do so)
  bool _do_cache_step_by_step_event = false;
  
  /// Optionally stores the events + basic emission info at each step
  /// of the showering process
  StepByStepEvent _step_by_step_event;

  // ------- things related to the GSL state
  bool _save_state_on_fail = false;
  GSLRandom _gsl_copy;

  // This variable only applies to _find_element_RoulettePeriodic_alg 
  // It means that we generate lnv_next according to
  //
  //   dP_trial = Σ_elements dln(v) [ b ln(v) + a - b C ],
  //
  // which differs from our usual dP_trial by the '-b C' term.
  //
  // #TRK_ISSUE-690 GS-NOTE: how does that go w the _enhancement_factor? [this depends
  //on how the bias relates to C]; Since _find_element_alg_bias is
  //used together w the ratio a/b in
  //_find_element_RoulettePeriodic_alg, I've so far assumed that it
  //did not need a rescaling by the _isr_overhead_factor.
  double _find_element_alg_bias;

  // parameters for edge reflexion
  //double _lnb_lh_edge_reflect = 0;
  //double _lnb_rh_edge_reflect = 0;

  // Set default matching object pointer to NULL. This can be overwritten
  // in AnalysisFramework by creating a matchedProcess object and supplying
  // its pointer to the shower with set_matching(<the pointer>).
  MatchedProcess * _matching_ptr;

  /// PDF tools
  HoppetRunner * _hoppet_runner;
  std::string    _pdf_dump_filename;
  std::unique_ptr<ISROverheadFactor> _isr_overhead_factor_handler;


  //-------------------------------------------------------------
  // #TRK_ISSUE-691 GS-NEW-CODE
  // temporary flags for extra tweaks we want in do_accept_emission
  // These are mostly for the conmputation of the 1st Sudakov
  // All of these should be cleaned at some point!!
  
  //GS-NEW-CODE: a temporary flag to be able to switch on/off the
  //inclusion of matching for weighted MC@NLO event generation in
  //do_one_emission. It s true by default (as used in tree_level,
  //etc...)
  bool _mcatnlo_subtract_ps_for_weighted;
  
  //GS-NEW-CODE: a temporary flag to be able to use the
  //"first_sudakov" version of the colour acceptance which uses the
  //flavour-averaged version rather than the info from the radiated
  //PDGId which would not be siuted for Gaussian quadrature. The
  //workaround here would be to explicitly sum over flavours
  bool _use_first_sudakov_for_colour;

  //-------------------------------------------------------------
  /// Overhead factors:
  ///
  /// Sometimes a probability (or correction factor) grows above one.
  /// We handle this by inserting an overhead factor in
  /// the lnv_lnb_max_density
  ///
  /// The main idea is to multiply the "a log + b log^2" Sudakov
  /// exponent by _overhead_factor (currently done independently in
  /// each find_next_element_... strategies) and insert a factor
  /// 1/_overhead_factor in _acceptance_probability
  ///
  /// In practice, we can have several sources of overhead.
  /// We introduce one factor per source and compute a "global" 
  /// overhead factor by multiplying the different sources together.
  ///
  /// for ISR, the product of PDF and splitting functions can
  /// overshoot 1. We therefore have an overhead factor associated with this.
  double _isr_overhead_factor;
  /// a record of the actual _isr_radiation_factor that was seen (should
  /// always be less than _isr_overhead_factor)
  double _isr_radiation_factor;

  /// potential overhead factor associated with the hard scale variation
  double _unc_hard_overhead_factor;

  /// potential overhead factor associated with emissions at commensurate kt
  double _unc_simkt_overhead_factor;

  /// global overhead factor (easier to have a single number to 
  /// insert in various probbilities and Sudakov weights)
  /// This HAS TO be updated whenever any of the individual acceptance factors changes
  double _global_overhead_factor;

  //-------------------------------------------------------------
  // double-soft material
  
  /// flag to switch off colour-flow swaps in the double-soft corrections
  bool _double_soft_no_colour_flow_swap;

  /// if not flavour_any, fix the flavour of the emission after
  /// double-soft corrections
  int _double_soft_fixed_emission_pdgid;

  /// printing of banner happens only at the first time
  static bool _first_printout_of_banner;

  /// we tolerate problems at large x, in which case we issue a
  /// warning instead of an assertion
  static LimitedWarning _warning_acceptance_probability_large_x_violation, 
                        _warning_do_kinematics_true_if_accept,
                        _warning_ISR_rescaling,
                        _warning_ISR_safety_increased;
  /// this warning is to remind us to check the combination of 
  /// matching and weighted generation                        
  static LimitedWarning _warning_matched_weighted_needs_checking;

  /// @brief class for error that will be thrown when an acceptance is above threshold
  class AcceptanceError: public std::runtime_error {
    public:
    AcceptanceError(const std::string & msg) : runtime_error(msg) {}
  };
#include "autogen/auto_ShowerRunner_ShowerRunner.hh"

  ///@}
}; // end class ShowerRunner

} // namespace panscales

#include "autogen/auto_ShowerRunner_global-hh.hh"
#endif // __SHOWERHELPERS_HH__
