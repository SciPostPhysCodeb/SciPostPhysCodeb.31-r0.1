//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "HoppetRunner.hh"
#include <cmath>
#include <iostream>
#include <cassert>
#include <exception>
#include <sstream>
#include "panhoppet_v1.h"
#ifdef WITH_LHAPDF
#include <LHAPDF/LHAPDF.h>
#endif


using namespace panscales;
using namespace std;

void hoppet_heralhc_init_(const double & x, const double & Q,
                          double * pdf){
  double N_g=1.7, N_ls=0.387975;
  double N_uv=5.107200, N_dv=3.064320;
  double N_db=N_ls/2;
  
  double uv   = N_uv * pow(x,0.8)  * pow(1-x,3);
  double dv   = N_dv * pow(x,0.8)  * pow(1-x,4);
  double dbar = N_db * pow(x,-0.1) * pow(1-x,6);
  double ubar = dbar * (1-x);
  
  pdf[ 0+6] = N_g * pow(x,-0.1) * pow(1-x,5);
  pdf[-3+6] = 0.2*(dbar + ubar);
  pdf[ 3+6] = pdf[-3+6];
  pdf[ 2+6] = uv + ubar;
  pdf[-2+6] = ubar;
  pdf[ 1+6] = dv + dbar;
  pdf[-1+6] = dbar;
  
  pdf[ 4+6] = 0;
  pdf[ 5+6] = 0;
  pdf[ 6+6] = 0;
  pdf[-4+6] = 0;
  pdf[-5+6] = 0;
  pdf[-6+6] = 0;
}
  
void hoppet_toy_nf5_init_(const double & x, const double & Q,
                          double * pdf){

  // adapted from heralhc_init, so as to have 
  // non-zero charm and bottom but still have
  // keep the same valence and the same total momentum
  double N_g=1.7, N_ls=0.387975;
  double N_uv=5.107200, N_dv=3.064320;
  double N_db=N_ls/2;
  
  double uv   = N_uv * pow(x,0.8)  * pow(1-x,3);
  double dv   = N_dv * pow(x,0.8)  * pow(1-x,4);
  double dbar = N_db * pow(x,-0.1) * pow(1-x,6);
  double ubar = dbar * (1-x);
  
  pdf[ 0+6] = N_g * pow(x,-0.1) * pow(1-x,5);
  pdf[-3+6] = 0.2*(dbar + ubar);
  pdf[ 3+6] = pdf[-3+6];
  pdf[ 2+6] = uv + 0.8*ubar;
  pdf[-2+6] = 0.8*ubar;
  pdf[ 1+6] = dv + 0.8*dbar;
  pdf[-1+6] = 0.8*dbar;
  
  pdf[ 4+6] = 0.15*(ubar+dbar);
  pdf[-4+6] = 0.15*(ubar+dbar);
  pdf[ 5+6] = 0.05*(ubar+dbar);
  pdf[-5+6] = 0.05*(ubar+dbar);
  pdf[ 6+6] = 0;
  pdf[-6+6] = 0;
}

HoppetRunner::~HoppetRunner(){
#ifdef WITH_LHAPDF
    if (_lhapdf_pdfset) delete _lhapdf_pdfset;
#endif
}

LimitedWarning HoppetRunner::_warning_Qmin(20), HoppetRunner::_warning_Qmax(20);  
// set how the shower scales should be mapped onto the hoppet Q scales
//
// If use_physical_scales is true, the Hoppet Q is the same as the
// shower Q (the other parameters are ignored)
//
// If use_physical_scales is false, the reference shower scale lnQ
// is mapped onto Hoppet's internal Qmax scale. The shower coupling
// at the lnQ scale is given by alphasQ and the (fixed) beta0 given
// by shower_beta0 (The internal Qmax scale can be passed to the
// constructor)
void HoppetRunner::set_scales_mapping(bool use_physical_scales,
                                      double lnQ, double alphasQ, int nloops){
  _use_physical_scales = use_physical_scales;
  _shower_lnQ     = lnQ;
  _shower_alphasQ = alphasQ;
  _shower_nloops  = nloops;

}

// initialise the run (to be called before any call to get_pdfs)
void HoppetRunner::initialise(const QCDinstance & qcd){
  // avoid initialising twice
  if (_hoppet_initialised) return;

  std::cout << "# Initialising Hoppet... " << std::endl;
  
  // make sure that anything we do with PDFs has the same colour factors
  // and also register the number of loops in the shower coupling
  panhoppetSetGroup(qcd.CA(), qcd.CF(), qcd.TR(), qcd.nf());
  _shower_nloops = qcd.nloops();

  if (_pdf_choice==LHAPDFSet) {
#ifndef WITH_LHAPDF
    throw std::runtime_error("HoppetRunner::initialise: LHAPDF support not compiled");
#else // WITH_LHAPDF
    // #TRK_ISSUE-125  selection of a PDF set is quite delicate:
    // - we need a LO PDF set, to have a reasonable chance of keeping positive PDFs
    // - the PDF set must have properly implemented flavour thresholds (that
    //   excludes the CT14 set)
    // - we want a sensible 3-flavour set (to NNPDF40 default doesn't
    //   work because it has intrinsic charm)
    // - we're left with NNPDF40_lo_pch_as_01180, which goes down to 1 GeV
    if (_lhapdf_name == "") _lhapdf_name = "NNPDF40_lo_pch_as_01180";
    //string pdfname = "NNPDF40_lo_pch_as_01180";
    //string pdfname = "PDF4LHC21_mc";
    //string pdfname = "NNPDF40_lo_as_01180";
    //string pdfname = "MSHT20lo_as130";    
    //string pdfname = "CT14lo";    
    int    pdf_mem  = 0;
    if (_lhapdf_pdfset) delete _lhapdf_pdfset;
    _lhapdf_pdfset = LHAPDF::mkPDF(_lhapdf_name,pdf_mem);

    _pdf_set_info.Qmin = _lhapdf_pdfset->qMin();
    _pdf_set_info.Qmax = _lhapdf_pdfset->qMax();
    _pdf_set_info.lnQmax = log(_pdf_set_info.Qmax);
    _pdf_set_info.alphasQmin = _lhapdf_pdfset->alphasQ(_pdf_set_info.Qmin);
    // by default, we force LHAPDF PDFs to be positive
    _pdf_set_info.force_positive = true;
    // choose some large, but not crazy value for the overhead
    _pdf_set_info.max_ISR_overhead = 20.0;

    _pdf_set_info.init_fct_ptr = nullptr;
    _pdf_set_info.max_active_flavours = 3;
    if (_lhapdf_pdfset->hasFlavor(4)){
      _pdf_set_info.mc = _lhapdf_pdfset->quarkMass(4);
      _pdf_set_info.max_active_flavours = 4;
    } else {
      _pdf_set_info.mc = 1e100;
    }
    if (_lhapdf_pdfset->hasFlavor(5)){
      _pdf_set_info.mb = _lhapdf_pdfset->quarkMass(5);
      _pdf_set_info.max_active_flavours = 5;
    } else {
      _pdf_set_info.mb = 1e101;
    }
    if (_lhapdf_pdfset->hasFlavor(6)){
      _pdf_set_info.mt = _lhapdf_pdfset->quarkMass(6);
      _pdf_set_info.max_active_flavours = 6;
    } else {
      _pdf_set_info.mt = 1e102;
    }

    _pdf_set_info.max_reliable_x = 0.99;

    _hoppet_initialised = true;
    std::cout << description() << std::endl;
    return;  
#endif // WITH_LHAPDF
  }
  
 
  // we also have to make sure that b0 of the shower is the same as b0 of Hoppet
  if(panhoppetB0() != qcd.b0()){
    cout << "b0 of shower:  " << qcd.b0() << ", b0 of hoppet: " << panhoppetB0() << endl;
    assert(false && " b0 of the shower is not equal to b0 of Hoppet!");
  }
  // #TRK_ISSUE-127 2-loop shower time: // same for b1, although we only need it if the shower runs at 2 loops
  //2-loop shower time: if(_shower_nloops == 2){
  //2-loop shower time:   if(abs(panhoppetB1()- qcd.b1()) > std::numeric_limits<double>::epsilon()){ 
  //2-loop shower time:     cout << "b1 of shower:  " << qcd.b1() << ", b1 of hoppet: " << panhoppetB1() << endl;
  //2-loop shower time:     assert(false && " b1 of the shower is not equal to b1 of Hoppet!");
  //2-loop shower time:   }
  //2-loop shower time: }

  // start HOPPET: use dy=0.05 and nloops=1
  //
  // see logbook/2021-05-pdf-timings/notes.pdf
  unsigned int nloops = 1;
  double dlnlnQ = 0.01;
  panhoppetStartExtended(_grid_ymax, _grid_dy,
                         _pdf_set_info.Qmin, _pdf_set_info.Qmax,
                         dlnlnQ, nloops, _grid_order,
                         factscheme_MSbar, _grid_nested? 1 : 0);
  
  // evolve the initial condition
  switch(_pdf_choice){
    case ToyNf5:
    case ToyNf5BigAlpha:
    case ToyNf5BiggerAlpha:
    case ToyNf5Physical:
    case ToyNf5Frozen:
      panhoppetSetFFN(5); break;
    case ToyVFNPhysical:
    case HERALHC:
      panhoppetSetPoleMassVFN(_pdf_set_info.mc, _pdf_set_info.mb, _pdf_set_info.mt); 
      break;
    default: assert(false && "unrecognised _pdf_choice");
  }
  //if (_pdf_choice == ToyNf5 || _pdf_choice == ToyNf5BigAlpha || _pdf_choice == ToyNf5BiggerAlpha || ){
  //  panhoppetSetFFN(5);
  //} else if (_pdf_choice == HERALHC) {
  //  // do nothing
  //} else {
  //  assert(false && "unrecognised _pdf_choice");
  //}

  double muR_over_muF = 1.0;
  panhoppetEvolve(_pdf_set_info.alphasQmin, _pdf_set_info.Qmin,
                  nloops, muR_over_muF,
                  _pdf_set_info.init_fct_ptr, _pdf_set_info.Qmin);

  /// for potential scale matching:
  _hoppet_alphasQmax = panhoppetAlphaS(_pdf_set_info.Qmax);

  _hoppet_initialised = true;
  std::cout << description() << std::endl;

}

/// returns the Q value to use with the native PDF, including
/// various range protections and warnings
double HoppetRunner::protected_Q(double lnkt) const {
  // get the effective PDF evolution scale Q
  double lnQ = map_lnkt_shower_to_t_hoppet(lnkt);
  double Q = exp(lnQ);

  if ( Q > _pdf_set_info.Qmax*(1+Q_MARGIN*std::numeric_limits<double>::epsilon())) {
    // #TRK_ISSUE-129  MARS-note: This is triggering during the overestimate determination
    _warning_Qmax.warn("in HoppetRunner: PDF requested at " + to_string(Q)+
                       " which is above the PDFSet Qmax="+to_string(_pdf_set_info.Qmax)+" scale");
  }

  if (_pdf_choice==LHAPDFSet){
    // LHAPDF extrapolates PDFs to zero below Qmin -- we instead freeze them at Qmin
    if (Q < _pdf_set_info.Qmin) Q = _pdf_set_info.Qmin;
    if (Q > _pdf_set_info.Qmax) Q = _pdf_set_info.Qmax;
  } else {
    // ToyNf5Frozen takes the minimum scale (where we had the initial condition)
    if (_pdf_choice == ToyNf5Frozen) Q = _pdf_set_info.Qmin;

    if (Q<_pdf_set_info.Qmin*(1-Q_MARGIN*std::numeric_limits<double>::epsilon())){  
      _warning_Qmin.warn("in HoppetRunner: PDF requested below the PDFSet Qmin scale");
      if (! _use_physical_scales) {
        throw range_error("Q < _pdf_set_info.Qmin when not using physical_scales: dangerous for log accuracy tests; throwing");
      }
    }
  }
  return Q;
}

// evaluate the PDFs at x and scale kt
PDF HoppetRunner::operator()(double x, double lnkt) const{
  PDF pdf;

  double Q = protected_Q(lnkt);

  // // get the effective PDF evolution scale Q
  // double lnQ = map_lnkt_shower_to_t_hoppet(lnkt);
  // double Q = exp(lnQ);
  // 
  // if ( Q > _pdf_set_info.Qmax*(1+Q_MARGIN*std::numeric_limits<double>::epsilon())) {
  //   // #TRK_ISSUE-129  MARS-note: This is triggering during the overestimate determination
  //   _warning_Qmax.warn("in HoppetRunner: PDF requested at " + to_string(Q)+
  //                      " which is above the PDFSet Qmax="+to_string(_pdf_set_info.Qmax)+" scale");
  // }
  
  if (_pdf_choice==LHAPDFSet){
#ifndef WITH_LHAPDF
    throw runtime_error("HoppetRunner: LHAPDFSet chosen but LHAPDF not compiled in");
#else 
    vector<double> data;
    // LHAPDF extrapolates PDFs to zero below Qmin -- we instead freeze them at Qmin
    // if (Q < _pdf_set_info.Qmin) Q = _pdf_set_info.Qmin;
    // if (Q > _pdf_set_info.Qmax) Q = _pdf_set_info.Qmax;
    _lhapdf_pdfset->xfxQ2(x, Q*Q, data);
    for (unsigned int i=0; i<13; ++i)
      pdf.pdf[i] = data[i];
    if (force_positive()) pdf.force_positive();
    return pdf;
#endif
  }


  // // ToyNf5Frozen takes the minimum scale (where we had the initial condition)
  // if (_pdf_choice == ToyNf5Frozen) Q = _pdf_set_info.Qmin;
  // 
  // if (Q<_pdf_set_info.Qmin*(1-Q_MARGIN*std::numeric_limits<double>::epsilon())){  
  //   _warning_Qmin.warn("in HoppetRunner: PDF requested below the PDFSet Qmin scale");
  //   if (! _use_physical_scales) {
  //     throw range_error("Q < _pdf_set_info.Qmin when not using physical_scales: dangerous for log accuracy tests; throwing");
  //   }
  // }

  panhoppetEval(x, Q, pdf.pdf);
  if (force_positive()) pdf.force_positive();
  return pdf;
}


// evaluate the PDFs at x and scale kt
double HoppetRunner::operator()(double x, double lnkt, int pdgid) const{

  double Q = protected_Q(lnkt);
  double xf;

  if (_pdf_choice==LHAPDFSet){
#ifndef WITH_LHAPDF
    throw runtime_error("HoppetRunner: LHAPDFSet chosen but LHAPDF not compiled in");
#else 
    xf = _lhapdf_pdfset->xfxQ2(pdgid, x, Q*Q);
#endif
  } else {
    xf = panhoppetEvalFlav(x, Q, PDF::iflv(pdgid));
  }

  if (force_positive() && xf < 0.0) xf = 0.0;
  return xf;
}



// apply a potential scale conversion (at the moment, this assumes
// a fixed 5 flavours in the PDFs)
double HoppetRunner::map_lnkt_shower_to_t_hoppet(double lnkt) const{
  double lnQ = lnkt;
  if (! _use_physical_scales){
    //2-loop shower time: if (_shower_nloops == 2) {
    //2-loop shower time:   // implements the 2 loop running
    //2-loop shower time:   double b0 = panhoppetB0();
    //2-loop shower time:   double b1 = panhoppetB1();
    //2-loop shower time:   // build in a safe to avoid 1+lambda to become negative
    //2-loop shower time:   if(lnkt < -1/(2*b0*_shower_alphasQ)) assert(false && "The lnkt is lower then -1/(2*b0*alphas(Q)), aborting");      
    //2-loop shower time:   double lambda       = 2*b0*_shower_alphasQ*(lnkt-_shower_lnQ);
    //2-loop shower time:   double oplambda     = 1+lambda;
    //2-loop shower time:   double log_oplambda = log(oplambda);
    //2-loop shower time:   // this equation corresponds to eqn (11) in logbook/2021-05-pdf-timings/notes.tex 
    //2-loop shower time:   double t_shower     = (log_oplambda-_shower_alphasQ*b1/b0*(lambda-log_oplambda)/oplambda)/(-2*M_PI*b0);
    //2-loop shower time:   // this equation is eqn (13) in the notes
    //2-loop shower time:   lnQ = _pdf_set_info.lnQmax - (1-exp(-2*M_PI*b0*t_shower))/(2*b0*_hoppet_alphasQmax);
    //2-loop shower time: 
    //2-loop shower time: } else if (_shower_nloops == 1) {
    if (_shower_nloops >= 1) {
      // with running coupling, just multiply the log by the ratios of couplings
      // at the high scale
      // this equation corresponds to eqn (3) in logbook/2021-05-pdf-timings/notes.tex
      lnQ = _pdf_set_info.lnQmax + _shower_alphasQ/_hoppet_alphasQmax*(lnkt-_shower_lnQ);
    } else if (_shower_nloops == 0) {
      // the PDF quantity t = \int_lnQ^lnQmax dlnQ alphas(lnQ) = 
      // is equivalent to _shower_alphasQ * (_shower_lnQ - lnkt)
      double t = _shower_alphasQ * (_shower_lnQ - lnkt);
      double b0 = panhoppetB0();
      double ell = (1 - exp(-2*b0*t))/(2*b0 * _hoppet_alphasQmax);
      // corresponds to eqn (7) in logbook/2021-05-pdf-timings/notes.tex
      lnQ = _pdf_set_info.lnQmax - ell;
    } else {
      assert(false && "shower_nloops value could not be handled");
    }
  } else {
    // if using physical scales, since the PDF will always have a running
    // coupling, the shower must as well; at NLL accuracy the difference
    // between 1 and 2-loop coupling in the PDF evolution is irrelevant
    // #TRK_ISSUE-132  (but REVISIT this if/when we go to NNLL)
    assert(_shower_nloops >= 1);
  }
  return lnQ;
}

// invert the map above
double HoppetRunner::map_lnQeff_hoppet_to_lnkt_shower(double lnQeff) const{
  double lnkt = lnQeff;
  if (! _use_physical_scales){
    //2-loop shower time: if (_shower_nloops == 2) {
    //2-loop shower time:   // inverts the nloop = 2 case
    //2-loop shower time:   // note that at nloop = 2 we cannot invert the map analytically
    //2-loop shower time:   // so we do the inversion with Newton-Raphson
    //2-loop shower time:   double b0 = panhoppetB0();
    //2-loop shower time:   double b1 = panhoppetB1();
    //2-loop shower time: 
    //2-loop shower time:   // we know that we need to equate the shower time to the hoppet time
    //2-loop shower time:   // hoppet time given by the one loop solution
    //2-loop shower time:   double t_hoppet = -log(1+2*_hoppet_alphasQmax*b0*(lnQeff - _pdf_set_info.lnQmax))/(2*M_PI*b0);
    //2-loop shower time:   // from this we estimate lambda0 which is our starting assumption
    //2-loop shower time:   double lambda0 = exp(-(2*M_PI*b0)*t_hoppet)-1;
    //2-loop shower time:   // we are going to use these quantities more often so let us store them
    //2-loop shower time:   double op_lambda0     = 1 + lambda0;
    //2-loop shower time:   double log_op_lambda0 = log(op_lambda0);  
    //2-loop shower time:   // f0 is the function we want to find the root for (tshower - thoppet = 0)
    //2-loop shower time:   // g0 is its derivative
    //2-loop shower time:   double f0 = ((log_op_lambda0 - _shower_alphasQ*b1/b0*(lambda0 - log_op_lambda0)/(op_lambda0))/(-2*M_PI*b0)) - t_hoppet;
    //2-loop shower time:   double g0 = (b1*_shower_alphasQ*log_op_lambda0)/(2*M_PI*pow2(b0*op_lambda0))- 1/(2*M_PI*b0*op_lambda0);
    //2-loop shower time:   // now we solve it recursively, keeping track of the number of tries... 
    //2-loop shower time:   unsigned int count     = 0;
    //2-loop shower time:   unsigned int MAX_TRIES = 1000;
    //2-loop shower time:   unsigned int STOPPING_MARGIN = 10;
    //2-loop shower time:   do{
    //2-loop shower time:     if(g0 == 0.0) assert(false && "Division by 0 in determining the root - cannot happen!");
    //2-loop shower time:     lambda0 = lambda0 - f0/g0;
    //2-loop shower time:     
    //2-loop shower time:     op_lambda0     = 1 + lambda0;
    //2-loop shower time:     log_op_lambda0 = log(op_lambda0);
    //2-loop shower time:     // compute the new f0 and g0
    //2-loop shower time:     f0 = ((log_op_lambda0 - _shower_alphasQ*b1/b0*(lambda0 - log_op_lambda0)/(op_lambda0))/(-2*M_PI*b0)) - t_hoppet;
    //2-loop shower time:     g0 = (b1*_shower_alphasQ*log_op_lambda0)/(2*M_PI*pow2(b0*op_lambda0))- 1/(2*M_PI*b0*op_lambda0);
    //2-loop shower time: 
    //2-loop shower time:     // exit when we have too many tries
    //2-loop shower time:     count+= 1;
    //2-loop shower time:     if(count > MAX_TRIES)  assert(false && "Too many tries to find the root");
    //2-loop shower time:   } while(fabs(f0)>STOPPING_MARGIN*std::numeric_limits<double>::epsilon());
    //2-loop shower time:   // finally obtain lnkt from lambda = as*b0*asQ * (lnkt - lnQ)
    //2-loop shower time:   lnkt = _shower_lnQ + lambda0/(2*b0*_shower_alphasQ);
    //2-loop shower time:   
    //2-loop shower time: } else if (_shower_nloops == 1) {
    if (_shower_nloops >= 1) {
      // inverts the nloop = 1 case
      lnkt = _shower_lnQ + _hoppet_alphasQmax/_shower_alphasQ*(lnQeff-_pdf_set_info.lnQmax);
    } else if (_shower_nloops == 0) {
      // inverts the nloop = 0 case
      double b0 = panhoppetB0();
      lnkt = _shower_lnQ + log(1+2*_hoppet_alphasQmax * b0 * (lnQeff - _pdf_set_info.lnQmax )) / (2 * _shower_alphasQ * b0);
    } else {
      assert(false && "shower_nloops value could not be handled");
    }
  } else {
    // if using physical scales, since the PDF will always have a running
    // coupling, the shower must as well; at NLL accuracy the difference
    // between 1 and 2-loop coupling in the PDF evolution is irrelevant
    // #TRK_ISSUE-134  (but REVISIT this if/when we go to NNLL)
    assert(_shower_nloops >= 1);
  }
  return lnkt;
}

// returns the convolution of the PDF with the splitting function
PDF HoppetRunner::pdf_convolution(double x, double lnkt, unsigned int iloop, unsigned int nf) const{
  PDF pdf;
  // get the effective PDF evolution scale Q
  double lnQ = map_lnkt_shower_to_t_hoppet(lnkt);
  double Q = exp(lnQ);
  panhoppetEvalSplit(x, Q, iloop, nf, pdf.pdf);
  return pdf;
}

// returns
//
//  [P(iloop,nf)_{G,XX} \otimes pdf_{XX}] (x,Q) [including the standard leading factor of x]
// 
// where XX=0 means the gluon and XX=1 means the singlet.
double HoppetRunner::pdf_gx_convolution(double x, double lnkt, unsigned int iloop, unsigned int nf, int xx) const {
  // get the effective PDF evolution scale Q
  double lnQ = map_lnkt_shower_to_t_hoppet(lnkt);
  double Q = exp(lnQ);
  return panhoppetEvalGXSplit(x, Q, iloop, nf, xx);
}

void HoppetRunner::_set_defaults(double Qmax){
  // default to massless heavy quarks
  _pdf_set_info.max_active_flavours = 5;
  _pdf_set_info.mc = 0.0;
  _pdf_set_info.mb = 0.0;
  _pdf_set_info.mt = 0.0;

  _pdf_set_info.Qmax = Qmax;
  _pdf_set_info.lnQmax = log(Qmax);
  _pdf_set_info.max_reliable_x = 0.995;

  if (_pdf_choice == ToyNf5 || _pdf_choice == ToyNf5Frozen){
    _pdf_set_info.Qmin = 1.0;
    _pdf_set_info.alphasQmin = 0.5;
    _pdf_set_info.init_fct_ptr = &hoppet_toy_nf5_init_;

  } else if (_pdf_choice == ToyNf5BigAlpha){
    _pdf_set_info.Qmin = 0.5;
    _pdf_set_info.alphasQmin = 0.8;
    _pdf_set_info.init_fct_ptr = &hoppet_toy_nf5_init_;

  } else if (_pdf_choice == ToyNf5BiggerAlpha){
    _pdf_set_info.Qmin = 0.5;
    _pdf_set_info.alphasQmin = 1.2;
    _pdf_set_info.init_fct_ptr = &hoppet_toy_nf5_init_;

  } else if (_pdf_choice == ToyNf5Physical){
    _pdf_set_info.Qmin = 0.5;
    _pdf_set_info.alphasQmin = 0.594599;
    _pdf_set_info.init_fct_ptr = &hoppet_toy_nf5_init_;

  } else if (_pdf_choice == ToyVFNPhysical){
    _pdf_set_info.Qmin = 0.5;
    _pdf_set_info.alphasQmin = 0.594599;
    _pdf_set_info.init_fct_ptr = &hoppet_heralhc_init_; //< use the HERA-LHC initial condition
    _pdf_set_info.mc = 1.414213563; // (sqrt(2)+epsilon)
    _pdf_set_info.mb = 4.5;
    _pdf_set_info.mt = 1e30;

  } else if (_pdf_choice == HERALHC){
    _pdf_set_info.Qmin = sqrt(2.0);
    _pdf_set_info.alphasQmin = 0.35;
    _pdf_set_info.init_fct_ptr = &hoppet_heralhc_init_;
    _pdf_set_info.mc = 1.414213563; // (sqrt(2)+epsilon)
    _pdf_set_info.mb = 4.5;
    _pdf_set_info.mt = 175.0;
    _pdf_set_info.max_active_flavours = 6;

  } else if (_pdf_choice == LHAPDFSet){
#ifdef WITH_LHAPDF
    // will be handled later
#else
    throw runtime_error("HoppetRunner: LHAPDFSet chosen but LHAPDF not compiled in");
#endif
    
  } else {
    cerr << "Unknown PDF choice (" << _pdf_choice << ") in HoppetRunner::set_defaults_. Aborting " << endl;
    exit(1);
  }
}

string HoppetRunner::description() const {
  ostringstream ostr;
  ostr << "HoppetRunner uses PDF set = " << _pdf_choice;
  if (_pdf_choice == LHAPDFSet) ostr << " (" << _lhapdf_name << ")";
  ostr << ", use_physical_scales = " << _use_physical_scales;
  const auto & psi = _pdf_set_info;
  ostr << ", Qmin = " << psi.Qmin << ", alphasQmin = " << psi.alphasQmin;
  ostr << ", Qmax = " << psi.Qmax;
  if (!_use_physical_scales){
    ostr << ", hoppet alphasQmax = " << _hoppet_alphasQmax;
    ostr << ", mapping the shower lnkt = " << _shower_lnQ << " to Hoppet Qmax from shower_nloops = " << _shower_nloops;
  }
  ostr << ", grid_dy = " << _grid_dy;
  ostr << ", grid_order = " << _grid_order;
  ostr << ", grid_nested = " << _grid_nested;
  ostr << ", max reliable x = " << pdf_max_reliable_x();
  ostr << ", force_positive = " << force_positive();
  ostr << ", max_ISR_overhead = " << max_ISR_overhead();
  for (int iflv = 4; iflv <=6; ++iflv) {
    ostr << ", m[" << iflv << "] = " << m_iflv(iflv);
  }
  return ostr.str();
}

/// get the smallest-accessible lnkt (w optional scale remapping)
double HoppetRunner::lnkt_min() const{
  double lnQmin = log(_pdf_set_info.Qmin);
  //if (_use_physical_scales) return lnQmin;
  // from the PDF retrieval method , we have  
  //   lnQmin = _pdf_set_info.lnQmax + _shower_alphasQ/_hoppet_alphasQmax*(lnkt_min-_shower_lnQ);
  // hence
  // return _shower_lnQ + _hoppet_alphasQmax/_shower_alphasQ*(lnQmin-_pdf_set_info.lnQmax);
  return map_lnQeff_hoppet_to_lnkt_shower(lnQmin);
}

void HoppetRunner::output_pdf(ostream & ostr, double lnkt) const {
  ostr << "# Dump of PDF at scale lnkt = " << lnkt << endl;
  int prec = 5;
  int prec_store = ostr.precision(prec);
  vector<double> xvals;//= {1e-4, 0.001, 0.01, 0.05, 0.1, 0.2, 0.3, 0.5, 0.7, 0.9};
  vector<string> flavs = {"tbar", "bbar", "cbar", "sbar", "ubar", "dbar", "g", "d", "u", "s", "c", "b", "t"};

  // fill up the x values
  double xmin = 1e-5;
  double dlnx = 0.1;
  int nlnx = (log(0.5/xmin))/dlnx;
  for (int ilnx = 0; ilnx <=nlnx; ++ilnx) {
    double lnx = log(xmin) + ilnx*dlnx;
    xvals.push_back(exp(lnx));
  }
  double omxmax = 1.0 - pdf_max_reliable_x();
  nlnx = log(0.5/omxmax)/dlnx;
  for (int ilnx = 0; ilnx <=nlnx; ++ilnx) {
    double lnomx = log(0.5) - ilnx*dlnx;
    xvals.push_back(1.0 - exp(lnomx));
  }

  // first the header
  ostr << left << setw(prec+7) << "# x";
  for (const auto & flav: flavs) ostr << setw(prec+7) << flav;
  ostr << endl;

  // then the PDFs
  for (const auto & x: xvals) {
    ostr << setw(prec+7) << x;
    PDF pdf = (*this)(x, lnkt);
    for (int i=0; i<13; ++i) ostr << setw(prec+7) << pdf.pdf[i];
    ostr << endl;
  }
  ostr.precision(prec_store);
}


///// overloaded output for PDFChoice
//std::ostream & panscales::operator<<(std::ostream & ostr, HoppetRunner::PDFChoice val) {
//  switch(val) {
//  case HoppetRunner::ToyNf5: ostr << "ToyNf5"; return ostr;
//  case HoppetRunner::ToyNf5Frozen: ostr << "ToyNf5Frozen"; return ostr;
//  case HoppetRunner::ToyNf5BigAlpha: ostr << "ToyNf5BigAlpha"; return ostr;
//  case HoppetRunner::ToyNf5BiggerAlpha: ostr << "ToyNf5BiggerAlpha"; return ostr;
//  case HoppetRunner::ToyNf5Physical: ostr << "ToyNf5Physical"; return ostr;
//  case HoppetRunner::ToyVFNPhysical: ostr << "ToyVFNPhysical"; return ostr;
//  case HoppetRunner::HERALHC: ostr << "HERALHC"; return ostr;
//  case HoppetRunner::LHAPDFSet: ostr << "LHAPDFSet"; return ostr;
//  default: ostr << "[unrecognized PDFChoice]"; return ostr;}
//}
//
///// overloaded input for PDFChoice
//std::istream & panscales::operator>>(std::istream & istr, HoppetRunner::PDFChoice & val) {
//  std::string strval;
//  istr >> strval;
//  if (strval == "ToyNf5") {val = HoppetRunner::ToyNf5;}
//  else  if (strval == "HERALHC") {val = HoppetRunner::HERALHC;}
//  else  if (strval == "ToyNf5BigAlpha") {val = HoppetRunner::ToyNf5BigAlpha;}
//  else  if (strval == "ToyNf5BiggerAlpha") {val = HoppetRunner::ToyNf5BiggerAlpha;}
//  else  if (strval == "ToyNf5Physical") {val = HoppetRunner::ToyNf5Physical;}
//  else  if (strval == "ToyNf5Frozen") {val = HoppetRunner::ToyNf5Frozen;}
//  else  if (strval == "ToyVFNPhysical") {val = HoppetRunner::ToyVFNPhysical;}
//  else  if (strval == "LHAPDFSet") {val = HoppetRunner::LHAPDFSet;}
//  else  {std::cerr << "Could not interpret " << strval << " as a PDFChoice\n";
//    exit(-1);}
//  return istr;
//}
#include "autogen/auto_HoppetRunner_global-cc.hh"
