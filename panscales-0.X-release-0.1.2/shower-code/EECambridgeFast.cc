//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#include "EECambridgeFast.hh"
#include <sstream>
#include <limits>
#include <iomanip>

FJCORE_BEGIN_NAMESPACE      // defined in fastjet/internal/base.hh

using namespace std;
using namespace panscales;

//----------------------------------------------------------------------
// NOTE:
//----------------------------------------------------------------------
// the initial implementation used pointers to the 4-vect. In this
// implementation, the priority queue stores both a pointer and an
// iterator to the element (see queue_element for details)
//
// The main resaon behind this is that iterators may become
// invalidated in the queue. This happens when an object has already
// been recombined through an earlier object in the queue. The
// iterator is then not valid. We therefore need to find a way to tell
// the queue (or the alg at a higher level) than the object has
// recombined.
//----------------------------------------------------------------------


//----------------------------------------------------------------------
/// EECamOrderedBriefJet implementation
//----------------------------------------------------------------------
// arguments are the momentum and cluster history index
EECamOrderedBriefJet::EECamOrderedBriefJet(const PseudoJet & jet, OrderingAxis axis) :
  /// internally, we just map x, y, z onto i, j, k 
  _cluster_hist_index(jet.cluster_hist_index()){
  init(jet);
  switch (axis){
  case X: _nk = _jet_mom.px(); break;
  case Y: _nk = _jet_mom.py(); break;
  case Z: _nk = _jet_mom.pz(); break;
  };    
}


/// rapidity ordering of two PseudoJet pointers
class eecambj_ptr_k_ordering{
public:
  bool operator()(const EECamOrderedBriefJet *j1, const EECamOrderedBriefJet *j2) const{
    return j1->nk() < j2->nk();
  }
};


//----------------------------------------------------------------------
// k_ordering_structure implementation
//----------------------------------------------------------------------
// provides an insertion in the list
// starting from a given point
// This should bring insertion down to O(sqrt(n))
k_ordering_iterator EECambridgeFastPlugin::k_ordering_structure::insert_from_point(const EECamOrderedBriefJet *new_bj,
                                                                                   const k_ordering_iterator &it_initial){
  precision_type new_k = new_bj->nk();

  k_ordering_iterator new_position = it_initial;
  // first determine if the particle has to go on the left or on the right
  if (new_k < (*new_position)->nk()){
    // as long as we have sth smaller, continue to the left
    while ((new_k < (*new_position)->nk()) && (new_position!=begin())){
      new_position--;
    } 
    // if we still have sth smaller, this mean that we've reached the
    // beginning
    if (new_k < (*new_position)->nk())
      return insert(new_position, new_bj);
    else{ // normal insertion
      new_position++;
      return insert(new_position, new_bj);
    }
  } 

  // insertion to the right
  // as long as we have sth smaller, continue to the right
  while ((new_position!=end()) && (new_k > (*new_position)->nk())){
    new_position++;
  } 
  return insert(new_position, new_bj);
}

// compute the nearest neighbour of one given particle
//  - ordered_particles   list of y-ordered particles
//  - reference_it        particle for which we must compute the neighbour
//  - neighbour_it        the found neighbour pon return (if any)
// returns true if a neighbour is found, false otherwise
bool EECambridgeFastPlugin::k_ordering_structure::get_neighbour(k_ordering_iterator &reference_it, 
                                                                k_ordering_iterator &neighbour_it,
                                                                precision_type &min_distance2) const{
  const EECamOrderedBriefJet * local_reference = *reference_it;
  precision_type reference_k = local_reference->nk();

  // at each step, we browse one step to the left and one to the right
  k_ordering_iterator neighbour_left_it  = reference_it;
  k_ordering_iterator neighbour_right_it = reference_it;
  
  min_distance2 = std::numeric_limits<precision_type>::max();
  neighbour_it = end();
  
  precision_type deltak, distance2;
  bool do_right = true, do_left = true;
  
  do{
    // go one step further with the right iterator
    if (do_right){
      // update on the right
      neighbour_right_it++;
      
      if (neighbour_right_it == end()){
        do_right = false;
      } else {
        deltak = (*neighbour_right_it)->nk()-reference_k;
	// the 0.5 factor in the next line comes from the fact that
	// 1-cos(theta) = 1/2 |neighbour-ref|^2
        if (0.5*deltak*deltak > min_distance2){  
          do_right = false;
        } else {
          distance2 = local_reference->distance(*neighbour_right_it);
          if (distance2<min_distance2){
            min_distance2 = distance2;
            neighbour_it = neighbour_right_it;
          }
        }
      }
    }
    
    // go one step further with the left iterator
    if (do_left){
      // update on the right
      if (neighbour_left_it == begin()){
        do_left = false;
      } else {
        neighbour_left_it--;
        deltak = reference_k-(*neighbour_left_it)->nk();
	// the 0.5 factor in the next line comes from the fact that
	// 1-cos(theta) = 1/2 |neighbour-ref|^2
        if (0.5*deltak*deltak > min_distance2){
          do_left = false;
        } else {
          distance2 = local_reference->distance(*neighbour_left_it);
          if (distance2<min_distance2){
            min_distance2 = distance2;
            neighbour_it = neighbour_left_it;
          }
        }
      }
    }
  } while ((do_left) || (do_right));
  
  // return true if a neighbour has been found
  // If it has not, we should still have the initial value
  return !(neighbour_it == end());
}


//----------------------------------------------------------------------
// main clustering class implementation
//----------------------------------------------------------------------

// plugin description
string EECambridgeFastPlugin::description () const {
  ostringstream desc;
  desc << "EECambridge(Fast) plugin with ycut = " << ycut() ;
  return desc.str();
}

// run the clustering
void EECambridgeFastPlugin::run_clustering(ClusterSequence & cs) const {
  // At low multiplicity, use the good old NNH
  //
  // 60 is a rough estimate based on ../timings-clust/timings.pdf
  // Ideally we could improve on this potentially introducing a
  // dependence on ycut
  if (cs.jets().size() < 60) {
    run_clustering_nnh(cs);
    return;
  }

  // at larger multiplicities, use the faster NsqrtN strategy
  run_clustering_nsqrtn(cs);
}
  
// run the clustering with NNH
void EECambridgeFastPlugin::run_clustering_nnh(fjcore::ClusterSequence & cs) const {
  int njets = cs.jets().size();
  fjcore::NNH<EECamBriefJet> nnh(cs.jets());
  
  precision_type Q2 = cs.Q2(); 
  
  while (njets > 0) {
    int i, j, k;
    // here we get a minimum based on the purely angular variable from the NNH class
    // (called dij there, but vij in the Cambridge article (which uses dij for 
    // a kt distance...)
    precision_type vij = nnh.dij_min(i, j); // i,j are return values...
    
    // next we work out the dij (ee kt distance), and based on its
    // value decide whether we have soft-freezing (represented here by
    // a "Beam" clustering) or not
    precision_type dij;
    if (j >= 0) {
      precision_type scale = min(cs.jets()[i].E(), cs.jets()[j].E());
      dij = 2 * vij * scale * scale;

      if (dij > Q2 * ycut()) {
        // we'll call the softer partner a "beam" jet
        if (cs.jets()[i].E() > cs.jets()[j].E()) std::swap(i,j);
        j = -1;
      }
    } else {
      // for the last particle left, just use yij = 1
      dij = Q2;
    }
    
    if (j >= 0) {
      cs.plugin_record_ij_recombination(i, j, dij, k);
      nnh.merge_jets(i, j, cs.jets()[k], k);
    } else {
      cs.plugin_record_iB_recombination(i, dij);
      nnh.remove_jet(i);
    }
    njets--;
  }
}


// run the clustering with the NsqrtN strategy
void EECambridgeFastPlugin::run_clustering_nsqrtn(ClusterSequence & cs) const {
  int njets = cs.jets().size();
  precision_type Q2 = cs.Q2();

  //--------
  // Step 0
  //--------
  // decide the orientation of the ordering
  // We take it as the axis (x, y or z) which minimises \sum |p_{i,axis}|
  precision_type modpx=0.0, modpy=0.0, modpz=0.0;
  //
  for (const auto & p : cs.jets()){
    modpx += std::abs(p.px());
    modpy += std::abs(p.py());
    modpz += std::abs(p.pz());
  }

  EECamOrderedBriefJet::OrderingAxis axis;
  if (modpx < modpy){
    if (modpx < modpz) axis = EECamOrderedBriefJet::X;
    else               axis = EECamOrderedBriefJet::Z;
  } else {
    if (modpy < modpz) axis = EECamOrderedBriefJet::Y;
    else               axis = EECamOrderedBriefJet::Z;
  }

  //--------
  // Step 1
  //--------
  // order the particles by rapidity
  // Important Note:
  //   The final ordered structure will be based on a STL list in such
  //   a way that addition and removal of elements does only
  //   invalidate the iterators affected by the operation. But to sort
  //   the initial list of particle, we temporarily use a STL vector.
  //
  // See also
  //   https://baptiste-wicht.com/posts/2012/12/cpp-benchmark-vector-list-deque.html
  // for benchmarks
  vector<EECamOrderedBriefJet> bj_vector;
  bj_vector.reserve(2*njets);
  for (int i=0; i<njets; ++i)
    bj_vector.push_back(EECamOrderedBriefJet(cs.jets()[i], axis));
    
  vector<const EECamOrderedBriefJet*> bj_vector_ptr;
  bj_vector_ptr.reserve(njets);
  for (int i=0; i<njets; ++i){
    bj_vector_ptr.push_back(&(bj_vector[i]));
  }
  sort(bj_vector_ptr.begin(), bj_vector_ptr.end(), eecambj_ptr_k_ordering());

  // transfer to list
  _k_ordered_bjs.clear();
  copy(bj_vector_ptr.begin(), bj_vector_ptr.end(),
       back_inserter(_k_ordered_bjs));

  //--------
  // Step 2
  //--------
  // for each particle decide which is the closest
  // and set the initial priority queue
  _PQ = priority_queue<queue_element>();
  for (k_ordering_iterator reference_it = _k_ordered_bjs.begin(); reference_it != _k_ordered_bjs.end(); reference_it++)
    if (! _update_neighbour_recombine_if_none(reference_it, cs)) return;
  
  //--------
  // Step 3
  //--------
  // iterate the recombinations.
  int reference_hist_index, neighbour_hist_index;
  precision_type scale, dij;
  
  while (true){ //(njets>0) && (!_PQ.empty())){
    // get the top of the priority queue
    const queue_element &q_elm = _PQ.top();

    // check if the reference in the queue_element 
    // points to an unrecombined PseudoJet
    k_ordering_iterator reference_it = q_elm._reference_it;
    reference_hist_index = q_elm._reference->cluster_hist_index();
    const ClusterSequence::history_element & reference_hist = cs.history()[reference_hist_index];
    if (reference_hist.child!=ClusterSequence::Invalid){
      // the reference has already been recombined.
      // just remove the element from the queue list
      _PQ.pop();
      continue;
    }

    // the reference has not recombined (yet)
    // check it the neighbour is also valid!
    k_ordering_iterator neighbour_it = q_elm._neighbour_it;
    neighbour_hist_index = q_elm._neighbour->cluster_hist_index();
    const ClusterSequence::history_element & neighbour_hist = cs.history()[neighbour_hist_index];
    //print_history_elm("  neighb elm: ", neighbour_hist, cs);
    if (neighbour_hist.child!=ClusterSequence::Invalid){
      // here we need to remove the current element
      // and to recompute the new one!
      //k_ordering_iterator reference_it = q_elm._reference_it;
      _PQ.pop();
      if (!_update_neighbour_recombine_if_none(reference_it, cs)) return;
      continue;
    }
    
    // now both the reference and neighbour exist (i.e. have not been previously recombined)
    // we need to make a recombination, either pairwise, or beam
    precision_type reference_E = cs.jets()[reference_hist.jetp_index].E();
    precision_type neighbour_E = cs.jets()[neighbour_hist.jetp_index].E();
    scale = min(reference_E, neighbour_E);
    dij = 2 * q_elm._reference->distance(q_elm._neighbour) * scale * scale;
    if (dij > Q2 * ycut()) {
      // beam recombination
      //
      // the softest of "reference" and "neighbour should combine w the beam
      if (reference_E < neighbour_E){
        // softest is reference. Nothing more to do than a beam recombination and bj list update
        cs.plugin_record_iB_recombination(reference_hist.jetp_index, dij);
        _k_ordered_bjs.erase(reference_it);
      } else {
        // softest is neighbour. Recombine + update list + update the reference's NN
        cs.plugin_record_iB_recombination(neighbour_hist.jetp_index, dij);
        _k_ordered_bjs.erase(neighbour_it);
        if (!_update_neighbour_recombine_if_none(reference_it, cs)) return;
      }
      continue;
    }

    // pairwise recombination
    // 1. do the recomb and create the BJ for the new particle
    int new_jet_index;
    cs.plugin_record_ij_recombination(reference_hist.jetp_index, neighbour_hist.jetp_index,
                                      dij, new_jet_index);
    PseudoJet new_jet = cs.jets()[new_jet_index];
    bj_vector.push_back(EECamOrderedBriefJet(new_jet, axis));

    // 2. insert the new jet in the ordered list
    reference_it = _k_ordered_bjs.insert_from_point(&bj_vector[bj_vector.size()-1],
                                                 (reference_E < neighbour_E) ? neighbour_it : reference_it);
	  
    // 3. remove the reference and neighbour from the ordered list
    _k_ordered_bjs.erase(q_elm._reference_it);
    _k_ordered_bjs.erase(q_elm._neighbour_it);

    // 4. remove the element from the PQ
    _PQ.pop();

    // 5. compute the neighbour of the new particle 
    //    and add it to the PQ
    if (!_update_neighbour_recombine_if_none(reference_it, cs)) return;
  }

  // we should actually never get to here (process ends when a single
  // particle is reclustered w the beam which happened within the loop)
  assert(false && "one should never get out of the above loop alive");

}

bool EECambridgeFastPlugin::_update_neighbour_recombine_if_none(k_ordering_iterator &reference_it,
                                                                  ClusterSequence &cs) const{
  precision_type min_distance2;
  k_ordering_iterator neighbour_it;
  if (_k_ordered_bjs.get_neighbour(reference_it, neighbour_it, min_distance2)){
    _PQ.push(queue_element(reference_it, neighbour_it, min_distance2));
    return true;
  }
  
  // we should normally only have one particle
  assert(_k_ordered_bjs.size()==1);

  cs.plugin_record_iB_recombination(cs.history()[(*reference_it)->cluster_hist_index()].jetp_index, cs.Q2());
  // this should genuinely be the end of the whole process
  return false;
}


FJCORE_END_NAMESPACE      // defined in fastjet/internal/base.hh
