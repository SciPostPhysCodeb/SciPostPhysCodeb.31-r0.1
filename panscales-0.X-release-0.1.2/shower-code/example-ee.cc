//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "AnalysisFramework.hh"
#include "FastY3.hh"
#include <iomanip>

using namespace std;
using namespace panscales;

//----------------------------------------------------------------------
/// Code to examine a few event shapes in e+e- events.
///
/// example of command line is 
/**
    build-double/example-ee -shower panglobal -beta 0 -process ee2qq \
      -physical-coupling -rts 91.1876 -nev 100000 \ 
      -out example-results/example-ee.dat
*/
/// This should run in a few seconds.
///
class ExampleEE : public  AnalysisFramework {
public:
  /// ctor
  ExampleEE(CmdLine * cmdline_in)
    : AnalysisFramework(cmdline_in), _broadening(true) {
      // check that we are running with the correct process
      if (!dynamic_cast<ProcessZ2qq*>(f_process.get())){
        throw runtime_error("This analysis is only to be used with a e+e-->qqbar process");
      }
    }

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_startup() override {
    // Tools to cluster the event with Cambridge/Aachen, compute thrust, ...
    _thrust.set_version(2); //< use fast version

    // declare histograms and binning
    Binning shape_binning(0.0, 0.5, 0.001);
    cumul_hists_err["evshp:y3" ].declare(shape_binning);    
    cumul_hists_err["evshp:1-T"].declare(shape_binning);    
    cumul_hists_err["evshp:BT" ].declare(shape_binning);    
    cumul_hists_err["evshp:BW" ].declare(shape_binning);
    
    Binning cparam_binning(0.0, 1.0, 0.02);
    cumul_hists_err["evshp:CParam"].declare(cparam_binning);    
    
    Binning frag_binning(-10.0, 1e-8, 0.2);
    hists_err["lnx_gluon"].declare(frag_binning);
    hists_err["lnx_qqbar"].declare(frag_binning);
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() override {
    //----------------------------------------------------------------------
    // fragmentation function
    //----------------------------------------------------------------------
    double evwgt = event_weight();
    for (const auto & p: f_event.particles()) {
      double lnx = to_double(log(2*p.E()/f_event.Q().E()));
      if (p.pdgid() == 21) hists_err["lnx_gluon"].add_entry(lnx, evwgt);
      else                 hists_err["lnx_qqbar"].add_entry(lnx, evwgt);
    }
    
    //----------------------------------------------------------------------
    // event shapes
    //----------------------------------------------------------------------
    
    // y23 requires C/A clustering
    _fasty3.analyse(f_event);
    cumul_hists_err["evshp:y3"].add_entry(to_double(_fasty3()), evwgt);

    // thrust
    _thrust.analyse(f_event);
    cumul_hists_err["evshp:1-T"].add_entry(to_double(_thrust.one_minus_thrust()), evwgt);

    // broadening
    _broadening.analyse(f_event, _thrust.event_axis());
    cumul_hists_err["evshp:BT"].add_entry(to_double(_broadening.total()   ), evwgt);
    cumul_hists_err["evshp:BW"].add_entry(to_double(_broadening.wide_jet()), evwgt);

    // C-Parameter
    _cparam.analyse(f_event);
    cumul_hists_err["evshp:CParam"].add_entry(to_double(_cparam()), evwgt);

  }
protected:
  FastY3 _fasty3;         ///< fast C/A clustering
  Thrust _thrust;         ///< Thrust-axis calculation
  Broadening _broadening; ///< Broadening  calculation
  CParam _cparam;         ///< C-Parameter calculation 
};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv,true);
  ExampleEE driver(&cmdline);
  driver.run();
}
