//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __EECAMBRIDGE_FAST_PLUGIN_HH__
#define __EECAMBRIDGE_FAST_PLUGIN_HH__

#include "Type.hh"
#include "fjcore_local.hh"
#include "Momentum.hh"

#include <list>
#include <queue>


FJCORE_BEGIN_NAMESPACE      // defined in fastjet/internal/base.hh

// forward declaration to reduce includes
class ClusterSequence;
//----------------------------------------------------------------------
/// @ingroup jet_algorithms
/// \class EECamBriefJet
/// class to help run a fast version of the e+e- Cambridge algorithm
/// (normally this would be hidden elsewhere, but it will
/// be useful for our high precision Lund Plane implementation)
class EECamBriefJet {
public:
  EECamBriefJet(){}
  
  void init(const PseudoJet & jet) {
    // From the point of view of calculating the angular distances
    // we just need a massless unit-length vector.
    //  
    // We use a two-step procedure to get it so as to reduce the risk of
    // numerical over and underflow, basically first normalising by the
    // maximum component and only afterwards normalising to unit length.
    // This makes it possible to explore momenta almost across the full
    // range of numeric_limits<double>::min() and max() rather than only
    // their square-roots.
    precision_type pmax = std::max(std::abs(jet.px()), std::max(std::abs(jet.py()), std::abs(jet.pz())));
    if (pmax > 0) {
      precision_type norm = 1.0/pmax;
      precision_type nx = jet.px() * norm;
      precision_type ny = jet.py() * norm;
      precision_type nz = jet.pz() * norm;
      norm = 1.0/sqrt(nx*nx + ny*ny + nz*nz);
      _jet_mom = panscales::Momentum(nx*norm, ny*norm, nz*norm, 1, 0);      
    } else {
      // occasionally (e.g. at end of clustering), we get a momentum
      // that has zero modp2. In that case, choose an arbitrary
      // direction (here, z): in cases where this happens it should
      // either not make any difference (end of clustering), or the
      // bevaviour of the algorithm was anyway arbitrary.
      _jet_mom = panscales::Momentum(0.0, 0.0, 1.0, 1, 0);
    }
  }

  precision_type distance(const EECamBriefJet * jet) const {
    return one_minus_costheta(_jet_mom,jet->_jet_mom);
  }

  precision_type beam_distance() const {
    return numeric_limit_extras<precision_type>::max();
  }

protected:
  panscales::Momentum _jet_mom;
};

//----------------------------------------------------------------------
/// \class EECamOrderedBriefJet
/// an extended version which additionally holds the projection along
/// an ordering axis
class EECamOrderedBriefJet : public EECamBriefJet {
public:
  /// ordering orientation: X, Y or Z
  enum OrderingAxis{ X, Y, Z };
   
  /// arguments are the momentum and cluster history index
  EECamOrderedBriefJet(const PseudoJet & jet, OrderingAxis axis);

  // the additional info needed for the N^{3/2} clustering

  /// projection of the (normalised) 3-momentum along the ordering axis
  precision_type nk() const{ return _nk; }
  
  /// cluster history index associated with this BriefJet
  unsigned int cluster_hist_index() const{ return _cluster_hist_index; }
  
private:
  precision_type _nk;   //< projection of the normalised momentum along the ordering axis
  unsigned int _cluster_hist_index; //< associated cluster history index

};

// typedef to help iterating through the BJs
typedef std::list<const EECamOrderedBriefJet*>::const_iterator k_ordering_iterator;

//----------------------------------------------------------------------
//
/// @ingroup plugins
/// \class EECambridgeFastPlugin
/// a faster Implementation of the e+e- Cambridge algorithm
///
/// Re-implementation of the EECambridgePlugin in FJ using a faster
/// O(N^{32}) strategy. See
/// 
/// Better jet clustering algorithms
/// Yuri Dokshitzer, Garth Leder, Stefano Moretti,  Bryan Webber 
/// JHEP 9708 (1997) 001
/// http://www-spires.slac.stanford.edu/spires/find/hep/www?rawcmd=FIND+j+JHEPA%2C9708%2C001
///
/// for the definition of the algorithm
///
class EECambridgeFastPlugin : public JetDefinition::Plugin {
public:
  /// Main constructor
  /// It takes the dimensionless parameter ycut (the Q value for normalisation
  /// of the kt-distances is taken from the sum of all particle energies).
  EECambridgeFastPlugin (precision_type ycut_in) : _ycut(ycut_in) {}

  /// copy constructor
  EECambridgeFastPlugin (const EECambridgeFastPlugin & plugin) {
    *this = plugin;
  }

  // the things that are required by base class
  virtual std::string description () const override;
  virtual void run_clustering(ClusterSequence &) const override;
  void run_clustering_nnh(ClusterSequence &) const;
  void run_clustering_nsqrtn(ClusterSequence &) const;

  precision_type ycut() const {return _ycut;}

  /// the plugin mechanism's standard way of accessing the jet radius.
  /// This must be set to return something sensible, even if R
  /// does not make sense for this algorithm!
  virtual precision_type R() const override {return 1.0;}

  /// avoid the warning whenever the user requests "exclusive" jets
  /// from the cluster sequence
  virtual bool exclusive_sequence_meaningful() const override {return true;}

  /// returns true because this plugin is intended for spherical
  /// geometries (i.e. it's an e+e- algorithm).
  virtual bool is_spherical() const override {return true;}

private:

  //----------------------------------------------------------------------
  /// a list of pointers to PseudoJet*, ordered in rapidity, with helpers
  /// for insertions with hints
  ///
  /// This is a helper class for the fast strategy
  class k_ordering_structure : public std::list<const EECamOrderedBriefJet*>{
  public:
    /// provides an insertion in the list
    /// starting from a given point
    /// This should bring insertion down to O(sqrt(n))
    k_ordering_iterator insert_from_point(const EECamOrderedBriefJet *new_bj, const k_ordering_iterator &it_initial);

    /// compute the nearest neighbour of one given particle
    /// \param ordered_particles   list of y-ordered particles
    /// \param reference_it        particle for which we must compute the neighbour
    /// \param neighbour_it        the found neighbour pon return (if any)
    /// \return true if a neighbour is found, false otherwise
    bool get_neighbour(k_ordering_iterator &reference_it, 
                       k_ordering_iterator &neighbour_it,
                       precision_type &min_distance2) const;  
  };



  //-----------------------------------------------------------------------------
  /// \class queue element
  /// 
  /// This class provides the main element that we'll use in a prioroty queue
  /// in order to know the order of the recombinations to process.
  /// Note that we keep track both of the iterator in the z-ordered list
  /// and to the pointers to the PseudoJets. The reason for that is that 
  /// previous recombinations might invalidate the iterators. 
  ///
  /// This is a helper class for the fast strategy
  class queue_element{
  public:
    /// ctor with initialisation
    /// \param reference_it   pointer to the reference jet in the y-ordered list
    /// \param neighbour_it   pointer to the neighbour jet in the y-ordered list
    /// \param distance2      distance between both
    queue_element(k_ordering_iterator reference_it, 
                  k_ordering_iterator neighbour_it,
                  precision_type distance2){
      _reference_it = reference_it;
      _neighbour_it = neighbour_it;
      _reference = *reference_it;  // just in case the iterator is invalidated
      _neighbour = *neighbour_it;
      _distance2 = distance2;
    }

    k_ordering_iterator _reference_it;      ///< pointer to the reference jet in the y-ordered list
    k_ordering_iterator _neighbour_it;      ///< pointer to the neighbour jet in the y-ordered list
    const EECamOrderedBriefJet *_reference; ///< the reference jet
    const EECamOrderedBriefJet *_neighbour; ///< the neighbour jet
    precision_type _distance2;              ///< the distance between both (squared)

    /// distance allowing to specify the ordering in the queue
    /// \param qe2   the other queue element to measure distance to
    /// \return true if the local distance is larger than the one given as argument
    bool operator <(const queue_element &qe2) const{
      return _distance2 > qe2._distance2;
    }
  };


  //--------------------------------------------------------------------------------
  bool _update_neighbour_recombine_if_none(k_ordering_iterator &reference_it,
                                           ClusterSequence &cs) const;
  
  precision_type _ycut;

  // the following two members hold the info necessary for the fast
  // clustering. They are mutable as run_clustering is const.
  //
  // One way around would be to do all the work in a subclass.
  mutable k_ordering_structure _k_ordered_bjs;
  mutable std::priority_queue<queue_element> _PQ;
};



FJCORE_END_NAMESPACE        // defined in fastjet/internal/base.hh

#endif // __EECAMBRIDGE_FAST_PLUGIN_HH__

