//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "Approx.hh"
#include <limits>
#include <iostream>
#include <iomanip>

using namespace std;

double ApproxBase::tolerance_ = 100.0 * numeric_limits<double>::epsilon();
double ApproxBase::half_tolerance_ = 0.5 * tolerance_;
bool   ApproxBase::verbose_ = false;

void ApproxBase::write_diff_message(double a, double diff, bool swap) const {

  if (swap) {
    cerr << "values " << a << " and " << this->value()
         << " differ by " << diff << endl;
  } else {
    cerr << "values " << this->value() << " and " << a
         << " differ by " << diff << endl;
  }
}

void ApproxBase::write_ltgt_message(double a, double diff, bool swap, bool lt) const {

  cerr << setprecision(20);
  char symbol = lt ? '<' : '>';
  if (swap) {
    cerr << a << " " << symbol << "= " << this->value() << " violated by " << fabs(diff) << endl;
  } else {
    cerr << this->value() << " " << symbol << "= " << a << " violated by " << fabs(diff) << endl;
  }
}
