//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerBase.hh"
#include "ColourTransitionRunner.hh"

/// \mainpage PanScales code documentation
///
/// \section quick_start Quick start
/// 
/// \section code_structure Basic code structure
///
/// - Most of the code is in the <a href="namespacepanscales.html">panscales</a> namespace
/// - Events are held in the panscales::Event class, with particles as panscales::Particle
/// - Showers derive from the panscales::ShowerBase class
/// - Running a shower is orchestrated by the panscales::ShowerRunner class
/// - Analyses derive from the AnalysisFramework class


using namespace std;

namespace panscales{
  
  LimitedWarning ErrorZeroMassElement::warn_;

  /// @brief f function to partition dipoles
  /// @param lnb (typically lnb would be an eta kind of variable)
  /// @return f(eta)
  double f_fcn(double lnb){
    if (-2*lnb > numeric_limit_extras<double>::log_max()) {
      return 0;
    } else {
      /// Write the following in two equivalent forms
      /// according to whether lnb > or < 0, so that we don't run
      /// into overflow issues when lnb gets very large (whether positive
      /// or negative)
      if (lnb > 0) return 1.0/(exp(-2*lnb) + 1.0);
      else         return exp(2*lnb)/(1.0 + exp(2*lnb));
    }
  }
  /// @brief f function to partition dipoles: variant that returns both f and 1-f
  /// @param lnb (typically lnb would be an eta kind of variable)
  /// @param f   (filled to f(lnb))
  /// @param omf (filled to 1-f(lnb))
  void f_fcn(double lnb, double &f, double &omf){
    if (-2*lnb > numeric_limit_extras<double>::log_max()) {
      f   = 0.0;
      omf = 1.0;
    } else {
      /// Write the following in two equivalent forms
      /// according to whether lnb > or < 0, so that we don't run
      /// into overflow issues when lnb gets very large (whether positive
      /// or negative)
      if (lnb > 0){
        double e = exp(-2*lnb);
        f   = 1/(e + 1);
        omf = e/(e + 1);
      } else {
        double e = exp(2*lnb);
        f   = e/(e + 1);
        omf = 1/(e + 1);
      }
    }
  } 

  /// @brief g function to partition dipoles
  /// @param lnb eta
  /// @return g(eta)
  double g_fcn(double lnb){ 
    if (lnb < -1.0){
      return 0.;
    } else if (lnb >= -1.0 && lnb <= 1.0) {
      double gvalue = 15./16.*(pow(lnb,5)/5. - 2./3.*pow(lnb,3) + lnb + 8./15.);
      return gvalue;
    } else {
      return 1.;
    }
  }

  //------------------------------------------------------------------------
  // ShowerBase implementation
  //------------------------------------------------------------------------
  // p is the (pre-branching) splitting particle 
  void ShowerBase::fill_dglap_splitting_weights(const Particle &p,
                                                const precision_type &z,
                                                double & weight_rad_g,
                                                double & weight_rad_q) const{
    // first handle the initial-state cases
    precision_type pq, pg;
    if (p.is_initial_state()){
      // treat differently cases where p  is a quark or a gluon
      //
      // WATCH OUT that we're doing backwards evolution so the flavour
      // of p is the flavour of the IS particle produced after the
      // splitting (i.e. the 1-z) part
      if (p.pdgid() == 21){
        pg = qcd().splitting_isr_zomzhalfPg2gg_normalised(z);
        // note that we do not put a factor nf here. This means that
        // this needs to be multiplied by the PDF ratio summed over
        // all quark flavours [either quark or anti-quark depending on
        // whether we're at the 3 or 3bar end of the dipole]
        pq = qcd().splitting_isr_zomzPq2gq_normalised(z);
      } else {
        pg = qcd().splitting_isr_zomzPq2qg_normalised(z);
        pq = qcd().splitting_isr_zomzPg2qqbar_normalised(z);
      }      
    } else { // final-state case
      // treat differently cases where p  is a quark or a gluon
      if (p.pdgid() == 21){
        pg = qcd().splitting_zhalfPg2gg_normalised(z);
        pq = qcd().splitting_zhalfPg2qqbar_normalised(z);
      } else {
        pg = qcd().splitting_zPq2qg_normalised(z);
        pq = 0.0;
      }
    }

    weight_rad_g = to_double(pg);
    weight_rad_q = to_double(pq);
  }

  //------------------------------------------------------------------------
  // ShowerBase::Element implementation
  //------------------------------------------------------------------------

  // update the event record with the emission, as specified in
  // emission_info, including changes to spectator/emitter, and any
  // other potential changes (e.g. due to global recoil). 
  void ShowerBase::Element::update_event(const ColourTransitionRunnerBase *transition_runner,
                                         ShowerBase::EmissionInfo * emission_info) {
    // optionally handle the direction difference updates which do not
    // depend on the rest of the event being updated
    if (use_diffs()) dirdiffs_pre_update(emission_info);

    // initial state                    / g
    // cases                           /
    //        q -----------  -> q ------------ q
    //
    //                                  / g
    //                                 /
    //        g -----------  -> g ------------ g
    //
    // These should both be handled by radiation_abs_pdgid == 21 case
    // and it looks like the final-state code can stay unchanged
    // whichever of the emitter / spectator does the splitting,
    // all that changes is its momentum.
    //
    // =========================================================
    //
    //                                  / qbar
    //                                 /
    //        q -----------  -> g ------------ q
    //
    //                                  / q
    //                                 /
    //        q -----------  -> q ------------ g
    //
    // These would be handled by what was the g->qqbar splitting code

    // this is needed for the update of the Born indices and MUST be computed
    // before we update the event 
    int pdgid_splitter = emission_info->do_split_emitter ? emitter().pdgid()
                                                         : spectator().pdgid();

    // start with things that are common across the board
    (*_event)[emitter_index()  ].reset_momentum(emission_info->emitter_out);
    (*_event)[spectator_index()].reset_momentum(emission_info->spectator_out);

    // add particles and set flavours (record the pre-splitting splitter flavour for later)
    //
    // Note: the particle needs to be added before one takes the
    // reference to the splitter so as to avoid references potentially
    // being invalidated.
    int inew = _event->add_particle_return_index(Particle(emission_info->radiation,
                                                          emission_info->radiation_pdgid));

    // set the lnkt at which the emission has been radiated
    (*_event)[inew].set_radiated_lnkt(lnkt_approx(emission_info->lnv,
                                                  emission_info->lnb));
    
    Particle & splitter = (*_event)[emission_info->splitter_index()];
    int splitter_pre_splitting_pdgid = splitter.pdgid();
    splitter.set_pdgid(emission_info->splitter_out_pdgid);

    // the splitter (either emitter or spectator) gets a new barcode
    splitter.set_barcode(_event->next_barcode());
    
    // insert a new dipole in the chain and update all dipole
    // links and pointers to particles
    //
    // the return value will be -ve if a non-splitting dipole has been created
    int new_dipole_index
      =  _event->update_dipole_after_emission(inew, dipole_index(),
                                              splitter_pre_splitting_pdgid,
                                              emission_info->splitter_is_3_end(),
                                              emission_info->radiation_pdgid);

    // finally, handle colour
    transition_runner->update_transitions(_event, new_dipole_index, *emission_info);
    
    // optionally finalise the treatment of direction differences
    if (use_diffs()) dirdiffs_post_update(emission_info);

    // if needed, also update the DIS chain information
    int dis_chain = emitter().get_DIS_chain();
    if(dis_chain != 0){ // update the DIS chain if needed
      _event->particles()[_event->particles().size()- 1].set_DIS_chain(dis_chain);
    }

    // handle potential colour-flow swap associated w the double-soft correction
    if (emission_info->colour_flow_swap){
      // swap the momenta
      _event->swap_momenta(emission_info->splitter_index(), _event->size()-1);
        
      // swap the momenta
      // handle dirdiffs
      if (use_diffs()) dirdiffs_colour_flow_swap(emission_info);
    }
    
    // update the Born indices (used in double-soft for Born gluons)
    // if the splitter is a gluon, we keep track of the hardest child
    if (pdgid_splitter == 21) {
      _event->update_Born_index(emission_info->splitter_index());
    }
  }

  void ShowerBase::Element::dirdiffs_pre_update(ShowerBase::EmissionInfo * eminfo) {
    // make sure we never get here if we're not using direction differences
    assert(use_diffs());

    // first round of code for calculating differences of
    // the _other_ dipoles to which this is connected
    Dipole * this_dipole = &dipole();

    // the code below will automatically include non-splitting dipoles
    // Note: nextdip_3    == previous
    //       nextdip_3bar == next
    Dipole * nextdip_3    = this_dipole->dipole_at_3_end(true);
    Dipole * nextdip_3bar = this_dipole->dipole_at_3bar_end(true);

    // we have 2 cases (see the 2021-03-23-non-splitting-dipoles
    // logbook for details):
    //  1. we radiate a gluon. In this case, a new dipole is inserted
    //     between the splitting dipole and the "next" (=next_3bar)
    //     with the new dipole 3 end and the splitting dipole 3bar end
    //     pointing to the radiated particle.
    //  2. we radiate a quark. In this case, the dipole connections
    //     (and hence the dirdiffs) depend on whether we split at the 3
    //     or 3bar end of the dipole
    if (eminfo->radiation_pdgid == 21) {
        // the next dipole at the triplet end of this dipole has its 3bar
        // end shifted by eminfo->d_3_end(), so 3_minus_3bar by minus
        // that 
      if (nextdip_3)    nextdip_3   ->dirdiff_3_minus_3bar -= eminfo->d_3_end();
        // the next dipole at our 3bar end has its 3 end shifted by the
        // the same shift as our 3bar end
      if (nextdip_3bar) nextdip_3bar->dirdiff_3_minus_3bar += eminfo->d_3bar_end();
    } else {
      // #TRK_ISSUE-251 GS-2021-03-23: note about non-splitting dipoles
      //
      // My understanding is that the difference between pre- and
      // post-update is that the newly created dipole is not available
      // in the pre-update. If there's more to it, then the structure
      // (between pre- and post-update probably has to be revisited
      // for non-splitting dipoles
      //
      // There is nonetheless still one question: why not only post_update?

      // Note: the drawings here are fine for FSR. Corresponding ISR
      // cases can be found in the 2021-03-23-non-splitting-dipoles
      // logbook
      
      // if the gluon splits at the 3 end of the dipole, the radiation
      // belongs to the next dipole at the 3 end; similarly if things
      // happen at the 3bar end
      //
      //
      //        3 || 3b   3 || 3bar  
      //          ||        ||  
      //     ----/  \------/  \----
      //          this dipole
      //
      // If the 3 end splits to qqbar we have
      //
      //                 non-split
      //                   3b 3
      //                  \.  ./
      //          ||       \\//
      //        3 || 3b   3 \/ 3bar (radiation) 
      //          ||        ||  
      //     ----/  \------/  \----
      // 
      // whereas if the 3bar end splits to qqbar we have 
      //
      //       non-split         
      //         3b 3
      //        \.  ./            
      //         \\//       ||  
      //   3(rad) \/ 3b   3 || 3bar 
      //          ||        ||  
      //     ----/  \------/  \----
      // 

      // firstly, we know that this dipole will change only according to
      // the 3 and 3bar end shifts
      this_dipole->dirdiff_3_minus_3bar += eminfo->d_3_end() - eminfo->d_3bar_end();
      // then we need to see if the 3 end split, or the 3bar; we have a
      // convention that antenna showers always have 3 as the emitter,
      // so if emitter is not 3 then do_split_emitter had better be true
      bool gluon_splits_3end = eminfo->splitter_is_3_end();
      if (gluon_splits_3end) {
        // at this stage (i.e. in pre_update), the non-splitting
        // dipole has not been inserted yet so nextdip_3 points to the
        // dipole carrying the new emission
        //        
        // nextdip's 3bar end is our emission
        //   next_3 - next_3b = (next_3 - next_3b_orig) - (next_3b - next_3b_orig)
        //                    = (next_3 - next_3b_orig) - (radiation - this_3_orig)
        if (nextdip_3)    nextdip_3->dirdiff_3_minus_3bar -= eminfo->d_radiation_wrt_3();
        // while the dipole at the 3bar end may get recoil because the
        // 3bar parton itself got recoil
        //   next_3 - next_3b = (next_3_orig - next_3b) + (next_3 - next_3_orig)
        //                    = (next_3_orig - next_3b) + (this_3b - this_3b_orig)
        if (nextdip_3bar) nextdip_3bar->dirdiff_3_minus_3bar += eminfo->d_3bar_end();
      } else {
        if (nextdip_3bar) nextdip_3bar->dirdiff_3_minus_3bar += eminfo->d_radiation_wrt_3bar();
        if (nextdip_3)    nextdip_3->dirdiff_3_minus_3bar -= eminfo->d_3_end();
      }
    }
  }

  void ShowerBase::Element::dirdiffs_post_update(const ShowerBase::EmissionInfo * eminfo) {
    /// code is validated only in cases where d_radiation is wrt emitter
    // the type of update depends on whether we emit a gluon or a
    // quark (see dirdiffs_pre_update and the
    // 2021-03-23-non-splitting-dipoles logbook)

    if (eminfo->radiation_pdgid != 21){
      // for quark radiation, we can have either g->qqbar in FSR, a
      // gluon backwards evolving into a(n anti)quark in ISR or a(n
      // anti)quark backwards evolving into a gluon in ISR.
      // 
      // for the first 2 cases, we create a non-splitting dipole. For
      // the third we create a new splitting dipole.
      //
      // We tell the difference by using eminfo->splitter_out_pdgid (21
      // for the third case, quark-like otherwise)

      // this assumes that the new dipole has been created at the back
      // of the list
      Dipole * new_dipole = (eminfo->splitter_out_pdgid == 21)
        ? & _event->dipoles().back()
        : & _event->non_splitting_dipoles().back();

      // Then the expressions for dirdiff depends on whether we split
      // at the3 or 3bar end of the dipole:
      //
      // for a splitting at the 3 end of the initial dipole, we have
      //   nonsplit_3_minus_3bar = radiation - this_3(post-branching)
      //                         = d_radiation_wrt_3 - d_3_end
      //
      // for a splitting at the 3bar end of the initial dipole, we have
      //   nonsplit_3_minus_3bar = this_3bar(post-branching) - radiation
      //                         = - d_radiation_wrt_3bar + d_3bar_end
      //
      // bear in mind that, say for a gluon splitting at the 3bar end,
      // the 3bar end of the splitting dipole would correspond to the
      // 3 end of the (next) (potentially non-splitting) dipole, while
      // the 3bar end of the non-splitting dipole would correspond to
      // the new radiation

      // note: we're in post_update so the dipole particle indices may
      // have changed during the update. In all the cases where we
      // radiate a quark, the splitting dipole particles are
      // unaffected, so the call to splitter_is_3end() is fine.
      bool gluon_splits_3end = eminfo->splitter_is_3_end();
      if (gluon_splits_3end) {
        new_dipole->dirdiff_3_minus_3bar = eminfo->d_radiation_wrt_3() - eminfo->d_3_end();
      } else {
        new_dipole->dirdiff_3_minus_3bar = eminfo->d_3bar_end() - eminfo->d_radiation_wrt_3bar();
      }

      return;
    }

    // handle the final case, a gluon radiated particle
    // if we insert a gluon into a 3-3bar dipole, then the
    // original dipole index is assigned to the 3-g dipole and the
    // new dipole index is assigned to the g-3bar dipole
    //
    // #TRK_ISSUE-256  Note: the dipole_3g's update could probably go in in pre_dirdiff_update?
    Dipole * dipole_3g    = &(_event->dipoles()[dipole_index()]);;
    Dipole * dipole_g3bar = & _event->dipoles().back();
    dipole_3g   ->dirdiff_3_minus_3bar = eminfo->d_3_end() - eminfo->d_radiation_wrt_3();
    dipole_g3bar->dirdiff_3_minus_3bar = eminfo->d_radiation_wrt_3bar() - eminfo->d_3bar_end();
  }

  void ShowerBase::Element::dirdiffs_colour_flow_swap(const ShowerBase::EmissionInfo * emission_info) {
    // make sure we never get here if we're not using direction differences
    assert(use_diffs());

    //assert((emission_info->radiation_pdgid == 21) && "dirdiff double-soft support currently only implemened for gluon emissions");

    // Note: we're recomputing everything. Maybe there is a more
    // efficient way to directly do this in dirdiffs_{pre,post}_update
    // but for now it is cleaner to keep everything in a single place
    const auto * element = emission_info->element();
    auto &splitting_dipole = _event->dipoles()[element->dipole_index()];

    if (emission_info->radiation_pdgid == 21){
      // Notes on dirdiff update (for gluon emission)
      //
      //         before swap                   after swap
      //      rad     splitter             splitter   rad
      //       ||      ||                     ||      ||     
      //       ||      ||         -->         ||      ||     
      // spec  ||      ||   next        spec  ||      ||     next
      //  ----/  \----/  \----           ----/  \----/  \----
      // Note: "next" is either the "dipole_at_3end" or "dipole_at_3bar_end"
      //
      // we need to update
      //   1. the (spec-rad)      dipole (assume it is currently d1)
      //   2. the (rad-splitter)  dipole (assume it is currently d2)
      //   3. the (splitter-next) dipole (assume it is currently d3)
      //
      // Note: . dirdiffs are always 3-3b
      //       . below, ~ denotes pre-branching momenta
      auto &new_dipole       = _event->dipoles()[_event->dipoles().size()-1];
  
      if (emission_info->splitter_is_3_end()){
        // Case 1: splitter is 3-end of initial dipole
        // The new dipole is rad-spec -> splitter-spec (after swap)
        // In emission info: 3 is split, 3bar is spec
        //
        // new dipole: 3end is split, 3barend is spec  [after swap]
        //   d = split-spec = (split~-spec~) + (split-split~) - (spec-spec~)
        new_dipole.dirdiff_3_minus_3bar = emission_info->cached_dirdiff_3_minus_3bar
                                        + emission_info->d_3_end()
                                        - emission_info->d_3bar_end();
        // splitting dipole: 3end is rad, 3barend is split  [after swap]
        //   d = rad-split = (rad-split~) - (split-split~)
        splitting_dipole.dirdiff_3_minus_3bar = emission_info->d_radiation_wrt_3()
                                              - emission_info->d_3_end();
        // next dipole at 3 end: 3end is "next_3", 3barend is rad  [after swap]
        //   d = next_3-rad = (next_3-split) - (split-rad)
        //     = d(next dip before swap) - d(splitting dipole)
        auto *next_dipole = splitting_dipole.dipole_at_3_end();
        next_dipole->dirdiff_3_minus_3bar = next_dipole->dirdiff_3_minus_3bar
                                          - splitting_dipole.dirdiff_3_minus_3bar;
      } else {
        // Case 2: splitter is 3bar-end of initial dipole (new dipole is rad-split)
        // The new dipole is rad-split
        // In emission info: 3 is spec, 3bar is split
        //            3   3b     (3~ - 3b~)      (3-3~)    (3b-3b~)
        //   d1 -> spec-split = cached_dirdiff + d_3_end - d_3bar_end
        //   d2 -> split-rad  = d_3bar_end - d_rad_3bar
        //   d3 -> rad-next = (rad-split) + (split-next)  = d3_old - d2_new
        // splitting dipole: 
        //
        // splitting dipole: 3end is spec, 3barend is split  [after swap]
        //   d = spec-split = (spec~-split~) - (split-split~) + (spec-spec~)
        splitting_dipole.dirdiff_3_minus_3bar = emission_info->cached_dirdiff_3_minus_3bar
                                              + emission_info->d_3_end()
                                              - emission_info->d_3bar_end();
        // new dipole: 3end is split, 3barend is rad  [after swap]
        //   d = split-rad = (split-split~) - (rad-split~)
        new_dipole.dirdiff_3_minus_3bar = emission_info->d_3bar_end()
                                        - emission_info->d_radiation_wrt_3bar();
        // next dipole at 3bar end:
        //   d = rad-next_3bar = (split-next_3bar) + (rad-split)
        //     = d(next dip before swap) - d_(new dipole) 
        auto *next_dipole = new_dipole.dipole_at_3bar_end();
        next_dipole->dirdiff_3_minus_3bar = next_dipole->dirdiff_3_minus_3bar
                                          - new_dipole.dirdiff_3_minus_3bar;        
      }
    } else { // g->qqbar
      // this assumes that the non-splitting dipole has been created
      // at the back of the list
      Dipole * dipole_qbarq = & _event->non_splitting_dipoles().back();

      //          before swap                    after swap         
      //     spec     new                   spec     new
      //     ||      \.  ./ rad             ||      \.  ./ split  
      //     ||   splt\\//                  ||    rad\\//       
      //     ||        ||          --->     ||        ||        
      //     ||  dip   ||   next            ||  dip   ||   next
      // ---/  \------/  \---           ---/  \------/  \--- n
      //
      // Next is at 3 or 3bar end depending on the splitting end
      
      if (emission_info->splitter_is_3_end()){
        // Radiation is a quark
        // The new dipole (non-splitting is rad-split (3-3b before split, 3b-3 after)
        //
        // Note that the radiation in emission_info still carries the
        // pre-swap pdgid, so we need to check for a -ve value
        assert((emission_info->radiation_pdgid<0)
               && "dirdiff with colour flow swap in g->qqbar at 3 end: was expecting to emit a quark (got an antiquark)");
        
        // In emission info: 3 is split, 3bar is spec
        //
        // splitting dipole: 3end is rad, 3barend is spec  [after swap]
        //   d = rad - spec = (rad-spec~) - (spec-spec~)
        splitting_dipole.dirdiff_3_minus_3bar = emission_info->d_radiation_wrt_3bar()
                                              - emission_info->d_3bar_end();
        // qqbar non-splitting: 3end is split, 3bar end is rad [after swap]
        //   d = split - rad = (split-split~) - (rad-split~)
        dipole_qbarq->dirdiff_3_minus_3bar = emission_info->d_3_end()
                                           - emission_info->d_radiation_wrt_3();
        // next dipole at 3 end: 3 end is next_3, 3bar end is split,  [after swap]
        //   d = next3 - split = (next3-rad) + (rad-split)
        //     = d(next dip before swap) - d(qqbar non-splitting dipole) 
        auto *next_dipole = dipole_qbarq->dipole_at_3_end();
        assert(next_dipole);
        next_dipole->dirdiff_3_minus_3bar = next_dipole->dirdiff_3_minus_3bar
                                          - dipole_qbarq->dirdiff_3_minus_3bar;
      } else { // splitter is the 3bar end of the splitting dipole
        // Radiation is an antiquark
        // The new dipole (non-splitting is rad-split (3b-3 before split, 3-3b after)
        //
        // Note that the radiation in emission_info still carries the
        // pre-swap pdgid, so we need to check for a +ve value
        assert((emission_info->radiation_pdgid>0)
               && "dirdiff with colour flow swap in g->qqbar at 3bar end: was expecting to emit an antiquark (got a quark)");
        
        // In emission info: 3 is spec, 3bar is split
        //
        // splitting dipole: 3end is spec, 3barend is rad  [after swap]
        //   d = spec - rad = (spec-spec~) - (rad-spec~)
        splitting_dipole.dirdiff_3_minus_3bar = emission_info->d_3_end()
                                              - emission_info->d_radiation_wrt_3();
        // qqbar non-splitting: 3end is rad, 3bar end is split [after swap]
        //   d = rad - split = (rad-split~) - (split-split~)
        dipole_qbarq->dirdiff_3_minus_3bar = emission_info->d_radiation_wrt_3bar()
                                           - emission_info->d_3bar_end();
        // next dipole at 3bar end: 3end is split, 3bar end is next_3bar [after swap]
        //   d = split - next3bar = (rad-next_3bar) + (split-rad)
        //     = d(next dip before swap) - d(qqbar non-splitting dipole) 
        auto *next_dipole = dipole_qbarq->dipole_at_3bar_end();
        assert(next_dipole);
        next_dipole->dirdiff_3_minus_3bar = next_dipole->dirdiff_3_minus_3bar
                                          - dipole_qbarq->dirdiff_3_minus_3bar;
      }
    }
  }


  //------------------------------------------------------------------------
  // ShowerBase::Emission implementation
  //------------------------------------------------------------------------
  
  /// update everything
  void ShowerBase::Element::update_cache(double lnv){
    double norm = enhancement_factor()*lnv_lnb_max_density();
    _cached_norm_a = norm*lnb_extent_const();
    _cached_norm_b = norm*lnb_extent_lnv_coeff();
    _cached_norm_astar = std::max(_cached_norm_a, -_cached_norm_b*lnv);
  }
  
  /// update and retrieve everything
  void ShowerBase::Element::update_cache(double lnv,
                    double &norm_a, double &norm_b, double &norm_astar){
    double norm = enhancement_factor()*lnv_lnb_max_density();
    norm_a     = _cached_norm_a     = norm*lnb_extent_const();
    norm_b     = _cached_norm_b     = norm*lnb_extent_lnv_coeff();
    norm_astar = _cached_norm_astar = std::max(_cached_norm_a, -_cached_norm_b*lnv);
  }  


  
  const double ShowerBase::Element::lnobs_approx(double lnv, double lnb, double beta) const {
    double lnobs = lnkt_approx(lnv,lnb) - 0.5 * to_double(log(_event->Q2()))
      - beta * std::abs(eta_approx(lnv,lnb));
    return lnobs;
  }


  void ShowerBase::Element::set_rotated_pieces(
         const ShowerBase::EmissionInfo * emission_info,
         ShowerBase::Element::RotatedPieces & pieces, 
         bool emitter_is_always_ref) const {
    
    // the following piece of code is intended to handle case where
    // we work with direction differences
    double eta = eta_approx(emission_info->lnv, emission_info->lnb);
    pieces.emitter_is_ref = (eta > 0) || emitter_is_always_ref;
    const Momentum * ref   = &emitter();
    const Momentum * other = &spectator();
    if (!pieces.emitter_is_ref) std::swap(ref,other);

    pieces.rotn_from_z = Matrix3::from_direction_no_pre_rotn(*ref);
    Matrix3 rotn_to_z  = pieces.rotn_from_z.transpose();

    Momentum ref_rot = Momentum::fromP3M2(Momentum3<Float>(0,0,ref->modp()), ref->m2());
    Float sign = (pieces.emitter_is_ref ^ emitter_is_quark_end_of_dipole()) ? +1 : -1;
    Momentum3<Float> other_rot3 = other->modp() * ( Momentum3<Float>(0,0,1) 
                              + sign * (rotn_to_z * dipole().dirdiff_3_minus_3bar ));
    Momentum other_rot = Momentum::fromP3M2(other_rot3, other->m2());

    const Momentum * emitter_rot   =  pieces.emitter_is_ref ? &ref_rot : &other_rot;
    const Momentum * spectator_rot = !pieces.emitter_is_ref ? &ref_rot : &other_rot;

    pieces.emitter   = *emitter_rot;
    pieces.spectator = *spectator_rot;
  }

  /// assuming rotated pieces have already been set, with the emitter
  /// as reference, return a copy re-aligned with the spectator the
  /// reference. 
  ShowerBase::Element::RotatedPieces
    ShowerBase::Element::RotatedPieces::copy_aligned_to_spectator() const {
    assert(emitter_is_ref);
    RotatedPieces copy;
    // get the extra rotations needed to take the spectator to / from
    // the z direction
    Matrix3 extra_rotn_from_z = Matrix3::from_direction_no_pre_rotn(spectator);
    Matrix3 extra_rotn_to_z = extra_rotn_from_z.transpose();
    // apply those rotations to all members where it is acceptable for
    // the rounding errors to be comparable to the rotation itself
    copy.emitter = extra_rotn_to_z * emitter;
    copy.perp1   = extra_rotn_to_z * perp1;
    copy.perp2   = extra_rotn_to_z * perp2;
    // the spectator needs to be exactly aligned along the z direction,
    // which we do manually
    copy.spectator = Momentum(
          Momentum4<Float>(0,0,spectator.modp(), spectator.E()), spectator.m2());
    // finally set the other information
    copy.emitter_is_ref = false;
    copy.rotn_from_z = rotn_from_z * extra_rotn_from_z;
    return copy;
  }

  void ShowerBase::Element::get_emit_spec_rad_kinematics_for_matching(typename ShowerBase::EmissionInfo * emission_info, Momentum & emit, Momentum & spec, Momentum & rad, bool include_boost) const {
    // set the dot products (possibly using directional differences)
    set_dot_products(emission_info);
    // set the emitter, spectator and radiated momentum
    emit = emission_info->emitter_out;
    spec = emission_info->spectator_out;
    rad  = emission_info->radiation;
  }

  void ShowerBase::Element::set_dot_products(typename ShowerBase::EmissionInfo * emission_info) const {
    // compute the dot products needed for the matrix elements
    if(use_diffs()) {
      emission_info->emit_dot_rad  = dot_product_with_dirdiff(emission_info->radiation, emission_info->emitter_out,
                                                  emission_info->d_radiation_wrt_emitter()-emission_info->d_emitter_out);
      emission_info->spec_dot_rad  = dot_product_with_dirdiff(emission_info->radiation, emission_info->spectator_out,
                                                  emission_info->d_radiation_wrt_spectator()-emission_info->d_spectator_out);
      emission_info->emit_dot_spec = dot_product_with_dirdiff(emission_info->emitter_out, emission_info->spectator_out,
                                                  emission_info->cached_dirdiff_em_minus_sp() + emission_info->d_emitter_out - emission_info->d_spectator_out);
    } else {
      emission_info->emit_dot_rad  = dot_product(emission_info->emitter_out,   emission_info->radiation);
      emission_info->spec_dot_rad  = dot_product(emission_info->spectator_out, emission_info->radiation);
      emission_info->emit_dot_spec = dot_product(emission_info->emitter_out,   emission_info->spectator_out);
    }
  }

  //------------------------------------------------------------------------
  // Range implementation
  //------------------------------------------------------------------------
  
  std::vector<double> Range::points_hd_edge(double mid_spacing, double edge_spacing) const {
    std::vector<double> points;
    double v = min();
    points.push_back(v);
    double spacing = edge_spacing;
    while (v < max()) {
      if (max()-v < 2*mid_spacing) {
        spacing = std::max(edge_spacing, (max()-v)/2);
        v += spacing;
      } else if (v - min() < 2*mid_spacing) {
        v += spacing;
        spacing = std::min(2*spacing, mid_spacing);
      } else {
        v += mid_spacing;
      }
      if (v < max()) points.push_back(v);
    }
    points.push_back(max());
    return points;
  }

  double ShowerBase::Element::double_soft_acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    typename ShowerBase::EmissionInfo & emission_info = *(static_cast<typename ShowerBase::EmissionInfo*>(emission_info_base));
    // Only do something non-trivial if double soft correction are turned on
    if(!double_soft()){
      // do not forget to set the relevant bits of EmissionInfo
      emission_info.flavour_swap_probability = 0.0;
      emission_info.colour_flow_swap_probability_qq = 0.0;
      emission_info.colour_flow_swap_probability_gg = 0.0;
      return 1.0;
    }

    assert(false && "cannot use this shower with double soft.");
    
    return 0.0;
  }


  double ShowerBase::pp2Z_one_emission_effective_ME(const Event evet, HoppetRunner *hopppet_runner) const {

    assert(false && "The pp2Z ME is not implemented for this shower");

    return 0.0;
  }

   double ShowerBase::pp2H_one_emission_effective_ME(const Event evet, HoppetRunner *hopppet_runner) const {

    assert(false && "The pp2H ME is not implemented for this shower");

    return 0.0;
  }

} // namespace panscales
