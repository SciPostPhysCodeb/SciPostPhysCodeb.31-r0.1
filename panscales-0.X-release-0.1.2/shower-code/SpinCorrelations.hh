//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

/// \file SpinCorrelations.hh
/// This file contains everything relevant to the building and
/// evaluation of SpinCorTrees. It contains:
///
///  * class Parton: 
///     class for Partons that encodes the 'SpinCorTree' structure needed for 
///     the on-the-fly propagation of spin correlations, as well as the
///     Parton's decay matrix.
///
///  * class SpinCorTree:
///     struct that contains a sorted list of vertices. When constructing
///     it takes an unsorted list of vertices, where the first vertex in
///     the list however has to be the incoming "hard" leg.
///
/// This code is inspired by the Collin-Knowles algorithm. 

#ifndef __PANSCALES_SPINCORRELATIONS_HH__
#define __PANSCALES_SPINCORRELATIONS_HH__ 

#include <complex>
#include <vector>
#include <assert.h>
#include <iostream>
#include <tuple>

#include "Weight.hh"
#include "Event.hh"
#include "ShowerBase.hh"
#include "Tensor.hh"

using namespace std;

namespace panscales {

/// Collinear branching channel enum
enum Channel{qFtoqFgF, gFtogFgF, gFtoqFqF, qItoqIgF, qItogIqF, gItogIgF, gItoqIqF};

// -------------------------------------------------------------------------
// ---------------- Splitting Amplitude (Spinor Products) -----------------
// -------------------------------------------------------------------------
/// Compute spinor products from momenta
/// The spinor products computed here are normalized such that |S| = 1, 
/// i.e. they are only a phase
void get_Spls_Smin(const Momentum &pi, const Momentum &pj, int ref_direction,
  PSCOMPLEX &Spls, PSCOMPLEX &Smin);

/// Compute spinor products from momenta, this time including the full norm
void get_Spls_Smin_with_norm(const Momentum &pi, const Momentum &pj, int ref_direction,
  PSCOMPLEX &Spls, PSCOMPLEX &Smin);

/// Compute spinor products from momenta using directional differences
void get_Spls_Smin_dirdiff(const Momentum &pi, const Momentum &pj, 
                           Momentum3<precision_type> &dirdiff_i_minus_j, int ref_direction,
                           PSCOMPLEX &Spls, PSCOMPLEX &Smin);

#ifndef SWIG
/// Get splitting amplitude from element
Tensor<3> get_splitting_amplitude(ShowerBase::Element &element, 
  ShowerBase::EmissionInfo &emission_info, int spinor_product_reference_direction, bool do_soft_spin, bool collinear_projection=false);
#endif

//--------------------------------------------------------------
/// \class Parton
///
/// A member of the Parton class contains:
///
///   - PDG id
///
///   - pointers to other connected partons:
///     * the incoming parton 'p_in' to the production vertex
///     * the partner parton 'p_partner' in the production vertex
///     * the 'p_daughter' outgoing parton in the decay vertex
///     * the 'p_son' outgoing parton in the decay vertex
///   (Note that we don't have proper vertices anymore, since all the
///   information needed is held in the Parton class)
///
///     * a pointer to the outgoing parton 'p_out' in a branch (needed for the alg)
///
///   - a decay matrix ( = 2x2 array of complex numbers at the moment )
///
///   - a cached value for the (previously) evaluated splitting amplitudes V and V*
///     of its production vertex
class Parton {

public:

  Parton(int pdgid, int barcode, Parton* p_parent): pdg_id_(pdgid), barcode_(barcode), p_parent_(p_parent) {}

  Parton() { }

  ~Parton() { }

  void print() const {
    std::cout << "pdg_id: " << pdg_id_ << std::endl;
    decay_matrix_.print();
  }

  void set_p_partner_ptr(Parton* p) { p_partner_ = p; }
  void set_p_daughter_ptr(Parton* p) { p_daughter_ = p; }
  void set_p_son_ptr(Parton* p) { p_son_ = p; }
  void set_pdg_id(int pdg_id) { pdg_id_ = pdg_id;}

  // the energy, polar angle and azimuthal plane direction
  // info is only stored if _store_extra_info is set to true
  void set_energy(precision_type E) { E_ = E; }
  void set_theta(precision_type theta) { theta_ = theta; }
  void set_azimuthal_plane(Momentum3<precision_type> azimuthal_plane) {
    azimuthal_plane_ = azimuthal_plane;
    // Make sure it is normalized
    azimuthal_plane_ /= azimuthal_plane_.modp();
  }
  precision_type get_energy() { return E_; }
  precision_type get_theta() { return theta_; }
  Momentum3<precision_type> get_azimuthal_plane() {return azimuthal_plane_;}

  Parton* get_p_parent_ptr() const { return p_parent_; }
  Parton* get_p_partner_ptr() const { return p_partner_; }
  Parton* get_p_daughter_ptr() const { return p_daughter_; }
  Parton* get_p_son_ptr() const { return p_son_; }

  int get_pdg_id() const { return pdg_id_; }
  int get_barcode() const { return barcode_; }

  Tensor<2>& get_decay_matrix() { return decay_matrix_; }
  const Tensor<2>& get_decay_matrix() const { return decay_matrix_; }

  /// Initialise decay matrix to delta function
  void init_decay_matrix() {
    decay_matrix_(0,0) = 0.5; decay_matrix_(0,1) = 0.;
    decay_matrix_(1,0) = 0.; decay_matrix_(1,1) = 0.5;
    decay_matrix_(a1_,a2_);
  }

  /// Recomputes spin density matrix for a parton that has already been decayed
  Tensor<2> recompute_spindensity(Tensor<2> & density_matrix_a);

  /// Recompute the decay matrix
  void recompute_decay_matrix();

  /// Indices for tensor contractions
  Index a1_,a2_;

  /// Cached splitting amplitude for *this to daughter and son 
  /// We keep the complex-conjugate of the splitting amplitude tensor
  /// separately for simplicity
  Tensor<3> splitting_amplitude_cached_;
  Tensor<3> splitting_amplitude_cached_star_;

  Tensor<3> splitting_amplitude_collinear_projection_cached_;
  Tensor<3> splitting_amplitude_collinear_projection_cached_star_;

private:

  int pdg_id_;
  int barcode_;

  /// Components for analysis
  precision_type E_ = NAN; // The energy of the parton
  precision_type theta_ = NAN; // The angle of the branching of this parton
  Momentum3<precision_type> azimuthal_plane_; // The azimuthal angle of the branching of this parton

  /// Pointers to the production vertex's incoming parton, partner,
  /// and decay vertex's daughter and son partons
  Parton* p_parent_ = nullptr;
  Parton* p_partner_ = nullptr;
  Parton* p_daughter_ = nullptr;
  Parton* p_son_ = nullptr;

  // Decay matrix
  Tensor<2> decay_matrix_;
};

//--------------------------------------------------------------
/// \class HardMatrixElement
///  helper to deal with scattering amplitudes 

template<size_t n_ext>
class HardMatrixElement {
public:

  HardMatrixElement(): ME_(0), MEstar_(0) {}

  // Initialisation from list of parton pointers
  void initialise(std::vector<Parton*> &parton_pointers) {
    assert(parton_pointers.size() == n_ext);
    for (size_t i=0; i<n_ext; i++) {
      // Set internal indices
      connected_partons_[i] = parton_pointers[i];
      ME_._indices[i]     = &connected_partons_[i]->a1_;
      MEstar_._indices[i] = &connected_partons_[i]->a2_;
    }
  }

  // ME accessors by spin indices
  template<typename... indices>
  const PSCOMPLEX& ME(indices... inds) const {return ME_(inds...);}

  template<typename... indices>
  PSCOMPLEX& ME(indices... inds) {return ME_(inds...);}

  template<typename... indices>
  const PSCOMPLEX& MEstar(indices... inds) const {return MEstar_(inds...);}

  template<typename... indices>
  PSCOMPLEX& MEstar(indices... inds) {return MEstar_(inds...);}

  const Tensor<n_ext>& ME    () const {return ME_;}
  const Tensor<n_ext>& MEstar() const {return MEstar_;}
  Tensor<n_ext>& ME    () {return ME_;}
  Tensor<n_ext>& MEstar() {return MEstar_;}

  // Sets MEstar_ to be the cc of ME_
  void set_MEstar_values() {
    for (size_t i=0; i<ME_._data.size(); i++) {
      MEstar_._data[i] = conj(ME_._data[i]);
    }
  }

  // Contract hard ME with all decay matrices that are not part_now
  Tensor<2> recompute_spindensity(Parton* part_now) {
    Tensor<n_ext> ME_now = ME_;
    for (size_t i=0; i<connected_partons_.size(); i++) {
      if (connected_partons_[i] != part_now) {
        ME_now = product_contract_single_index(ME_now, connected_partons_[i]->get_decay_matrix());
      }
    }

    Tensor<2> density_out = product_contract_n_index<n_ext-1>(ME_now, MEstar_);
    density_out.normalize();

    return density_out;
  }
  
  static constexpr size_t size_ = 1 << n_ext;

  std::array<Parton*, n_ext> connected_partons_;

  Tensor<n_ext> ME_, MEstar_;
};

//--------------------------------------------------------------
/// \class SpinCorTree

class SpinCorTree {

public:
  SpinCorTree() {}

  ~SpinCorTree() { }

  /// Disable spin correlations
  void disable_spin_correlations() {do_spin_corr_ = false; do_soft_spin_ = false;}
  /// Disable soft spin corrections
  void disable_soft_spin_corrections() {do_soft_spin_ = false;}
  /// Disable collinear spin projection
  void disable_collinear_spin_projection() {do_collinear_spin_projection_ = false;}

  /// return true if spin correlations are enabled
  bool do_spin_correlations() const {return do_spin_corr_;}
  /// return true if soft spin corrections are enabled
  bool do_soft_spin_corrections() const {return do_soft_spin_;}
  // return for collinear spin projection 
  bool do_collinear_spin_projection() const {return do_collinear_spin_projection_;}

  /// call if one wants to choose a fixed reference direction
  /// (only use to test dependence !)
  void set_fixed_reference_direction(int ref) {reference_direction_ = ref;}

  /// enable storing of extra branching information
  void store_extra_branching_info(bool value) {_store_extra_branching_info = value;}

  Tensor<2> get_current_spindensity() {return spindensity_in_current_;}

  /// Construct one of the initial partons 
  // returns a pointer to the newly created parton
  Parton* construct_initial_parton(Particle &part) {
    int pdg_id = part.pdgid();
    int barcode = part.barcode();

    parton_map_[barcode] = Parton(pdg_id, barcode, nullptr);

    if (_store_extra_branching_info) {
      parton_map_[barcode].set_energy(part.E());
    }

    parton_map_[barcode].init_decay_matrix();

    return &parton_map_[barcode];
  } 

  /// Initialisation
  void initialise(Event &event){
    // Reset the tree such that all variables are set
    reset();

    // Just return if we're not running spin correlations
    if (!do_spin_corr_) return;
  
    // Find the reference direction for spinor products
    // This is written to account for any number of partons in the initial event
    // Even though the rest of the code only supports 2 currently
    // Find a suitable reference direction for spinor products
    // This is done to avoid picking a direction that aligns exactly with one of the momenta
    // We pick between x and y since the z-direction is typically the beam direction
    // If this is the second time initialise gets called, e.g. during matching,
    // we should not alter the reference direction
    // Check if there are any partons in the initial state of the event
    bool found_pos_z_initial_state = false, found_neg_z_initial_state = false;
    for (unsigned int i=0; i<event.size(); i++) {
      if (event[i].is_initial_state() && event[i].is_parton()) {
        if (event[i].pz() > 0)  found_pos_z_initial_state = true;
        else                    found_neg_z_initial_state = true;
      }
    }

    // Complain when the found initial state does not agree with the setup of the event
    if (found_pos_z_initial_state != found_neg_z_initial_state && event.is_pp()) {
      throw std::runtime_error("Spin Correlations: Only one initial-state found for a pp event.");
    } else if (found_pos_z_initial_state == found_neg_z_initial_state && event.is_DIS()){
      throw std::runtime_error("Spin Correlations: Two/none initial-states found for a DIS event.");
    }

    // this is the e+e- situation
    if (event.size() == 2 && (!found_pos_z_initial_state&& !found_neg_z_initial_state)) {
      precision_type dx_min = 1;
      precision_type dy_min = 1;
      for (unsigned int i=0; i<event.size(); i++) {
        if (fabs(event[i].px()) < dx_min) dx_min = fabs(event[i].px());
        if (fabs(event[i].py()) < dy_min) dy_min = fabs(event[i].py());
      }
      // If we have a fixed reference direction (e.g. for tests, we choose that one)
      if (reference_direction_ > 0) {
        spinor_product_reference_direction_ = reference_direction_;
      } else {
        spinor_product_reference_direction_ = dx_min < dy_min ? 2 : 1;
      }
    } else if (found_pos_z_initial_state || found_neg_z_initial_state) {
      // for DIS / pp we just use the x-direction, as the particles will be aligned with the z-axis
      spinor_product_reference_direction_ = 1;
    } 

    // Construct the initial partons
    std::vector<Parton*> parton_pointers;

    // -------------------------------- Initial state (pp) --------------------------------
    if (found_pos_z_initial_state && event.is_pp()) {
      if (event.size() == 3) {        
        parton_pointers.push_back(construct_initial_parton(event[0]));
        parton_pointers.push_back(construct_initial_parton(event[1]));

        hard_ME_size_ = 2;
        hard_ME_two_.initialise(parton_pointers);

        if (event[0].pdgid() == 21 && event[1].pdgid() == 21 ) {
          PSCOMPLEX Spls_gg, Smin_gg;
          get_Spls_Smin_with_norm(event[0], event[1], spinor_product_reference_direction_, Spls_gg, Smin_gg);

          hard_ME_two_.ME(0,0) = Smin_gg*Smin_gg;
          hard_ME_two_.ME(1,1) = Spls_gg*Spls_gg;
        }
        else if (abs(event[0].pdgid()) == abs(event[1].pdgid())) {
          // The order of the q and qbar do not matter

          hard_ME_two_.ME(0,1) = 1;
          hard_ME_two_.ME(1,0) = 1;
        }
      }
      else if(!do_spin_corr_) return; // just do nothing in this case
      else {
        throw std::runtime_error("Spin Correlations: Found initial-state event with more than 1 final state.");
      }

      hard_ME_two_.set_MEstar_values();
    } 
    // --------------------------------- DIS ---------------------------------
    else if(event.is_DIS()){
      // normally we will have the DIS initialised with two initial-state particles (q & photon)
      // and one final-state particle
      if (event.size() == 3) {    
        // select the initial-state parton index
        int initial_state_index = event[0].is_parton() ? 0 : 1;  
        int final_state_index   = event[2].is_parton() ? 2 : 1;  
        parton_pointers.push_back(construct_initial_parton(event[initial_state_index]));
        parton_pointers.push_back(construct_initial_parton(event[final_state_index]));

        // ignore the photon/lepton in this setup
        hard_ME_size_ = 2;
        hard_ME_two_.initialise(parton_pointers);

        if (event[initial_state_index].pdgid() == 21 && event[final_state_index].pdgid() == 21 ) {
          PSCOMPLEX Spls_gg, Smin_gg; 
          get_Spls_Smin_with_norm(event[0], event[1], spinor_product_reference_direction_, Spls_gg, Smin_gg);

          hard_ME_two_.ME(0,0) = Smin_gg*Smin_gg;
          hard_ME_two_.ME(1,1) = Spls_gg*Spls_gg;
        }
        else if (abs(event[initial_state_index].pdgid()) == abs(event[final_state_index].pdgid())) {
          // The order of the q and qbar do not matter
          PSCOMPLEX Spls_qq, Smin_qq; 
          get_Spls_Smin_with_norm(event[initial_state_index], event[final_state_index], 
                                      spinor_product_reference_direction_, Spls_qq, Smin_qq);
          // assume polarization vector for the photon with averaged +/-
          // however, note that the normalisation here does not really matter (only the form of the matrix)
          hard_ME_two_.ME(0,1) = Spls_qq;
          hard_ME_two_.ME(1,0) = Smin_qq;
        }
      }
      else if(!do_spin_corr_) return; // just do nothing in this case
      else {
        throw std::runtime_error("Spin Correlations: Found initial-state event with more than 1 final state.");
      }
      hard_ME_two_.set_MEstar_values();
    }
    // --------------------------------- Final state ---------------------------------
    else {
      // Add the initial state, and for now (2024-03-19), treat the electron as massless
      // (NB non-zero masses not supported yet)
      precision_type electron_msq = 0;
      Particle p_emin = Particle(Momentum(0, 0,  sqrt(event.Q2()/4-electron_msq), sqrt(event.Q2())/2, electron_msq), 11);
      Particle p_epls = Particle(Momentum(0, 0, -sqrt(event.Q2()/4-electron_msq), sqrt(event.Q2())/2, electron_msq), -11);
      p_emin.set_barcode(-1);
      p_epls.set_barcode(-2);
      parton_pointers.push_back(construct_initial_parton(p_emin));
      parton_pointers.push_back(construct_initial_parton(p_epls));

      // NOTE: 0 = +, 1 = -

      // Determine the matrix element
      // 2-body matrix element
      if (event.size() == 2) {
        hard_ME_size_ = 4;

        // h -> gg
        if (event[0].pdgid() == 21 && event[1].pdgid() == 21) {
          // Partons
          parton_pointers.push_back(construct_initial_parton(event[0]));
          parton_pointers.push_back(construct_initial_parton(event[1]));
          hard_ME_four_.initialise(parton_pointers);

          // Compute amplitudes
          PSCOMPLEX Spls_gg, Smin_gg;
          get_Spls_Smin_with_norm(event[0], event[1], spinor_product_reference_direction_, Spls_gg, Smin_gg);

          hard_ME_four_.ME(0,0,0,0) = -Spls_gg*Spls_gg;
          hard_ME_four_.ME(1,1,0,0) = -Spls_gg*Spls_gg;
          hard_ME_four_.ME(0,0,1,1) = -Smin_gg*Smin_gg;
          hard_ME_four_.ME(1,1,1,1) = -Smin_gg*Smin_gg;

        }
        // gam -> qqbar
        else if (abs(event[0].pdgid()) == abs(event[1].pdgid())) {
          // Find the q and the qbar
          int i_q    = event[0].pdgid() > 0 ? 0 : 1;
          int i_qbar = event[1].pdgid() < 0 ? 1 : 0;

          // Partons
          parton_pointers.push_back(construct_initial_parton(event[i_q]));
          parton_pointers.push_back(construct_initial_parton(event[i_qbar]));
          hard_ME_four_.initialise(parton_pointers);

          // Compute amplitudes
          Momentum p_q    = event[i_q   ].momentum();
          Momentum p_qbar = event[i_qbar].momentum();

          PSCOMPLEX Spls_em_q, Smin_em_q;
          PSCOMPLEX Spls_em_qbar, Smin_em_qbar;
          PSCOMPLEX Spls_ep_q, Smin_ep_q;
          PSCOMPLEX Spls_ep_qbar, Smin_ep_qbar;

          get_Spls_Smin_with_norm(p_emin, p_q, spinor_product_reference_direction_, Spls_em_q, Smin_em_q);
          get_Spls_Smin_with_norm(p_emin, p_qbar, spinor_product_reference_direction_, Spls_em_qbar, Smin_em_qbar);
          get_Spls_Smin_with_norm(p_epls, p_q, spinor_product_reference_direction_, Spls_ep_q, Smin_ep_q);
          get_Spls_Smin_with_norm(p_epls, p_qbar, spinor_product_reference_direction_, Spls_ep_qbar, Smin_ep_qbar);

          // gamma -> qqbar
          hard_ME_four_.ME(0,1,0,1) = -Spls_ep_q*Smin_em_qbar;
          hard_ME_four_.ME(0,1,1,0) = -Spls_ep_qbar*Smin_em_q;
          hard_ME_four_.ME(1,0,1,0) = conj(hard_ME_four_.ME(0,1,0,1));
          hard_ME_four_.ME(1,0,0,1) = conj(hard_ME_four_.ME(0,1,1,0));
        }
        hard_ME_four_.set_MEstar_values();
      }

      // 3-body matrix element
      else if (event.size() == 3) {
        hard_ME_size_ = 5;

        int n_q = 0;
        int n_qbar = 0;
        int n_g = 0;
        for (unsigned int i=0; i<event.size(); i++) {
          if (event[i].pdgid() == 21)    n_g++;
          else if (event[i].pdgid() < 0) n_qbar++;
          else                           n_q++;
        }

        // h -> ggg
        if (n_g == 3 && n_q == 0 && n_qbar == 0) {
          // Partons
          parton_pointers.push_back(construct_initial_parton(event[0]));
          parton_pointers.push_back(construct_initial_parton(event[1]));
          parton_pointers.push_back(construct_initial_parton(event[2]));
          hard_ME_five_.initialise(parton_pointers);

          // Compute amplitudes
          Momentum p_1 = event[0].momentum();
          Momentum p_2 = event[1].momentum();
          Momentum p_3 = event[2].momentum();

          PSCOMPLEX Spls_12, Smin_12;
          PSCOMPLEX Spls_23, Smin_23;
          PSCOMPLEX Spls_31, Smin_31;
          precision_type mH2 = (p_1 + p_2 + p_3).m2();

          get_Spls_Smin_with_norm(p_1, p_2, spinor_product_reference_direction_, Spls_12, Smin_12);
          get_Spls_Smin_with_norm(p_2, p_3, spinor_product_reference_direction_, Spls_23, Smin_23);
          get_Spls_Smin_with_norm(p_3, p_1, spinor_product_reference_direction_, Spls_31, Smin_31);

          // All the same
          hard_ME_five_.ME(0,0,0,0,0) = -0.25*mH2*mH2/Smin_12/Smin_23/Smin_31;
          hard_ME_five_.ME(1,1,0,0,0) = -0.25*mH2*mH2/Smin_12/Smin_23/Smin_31;

          // Single flip
          hard_ME_five_.ME(0,0,0,0,1) = -Spls_12*Spls_12*Spls_12/Spls_23/Spls_31;
          hard_ME_five_.ME(1,1,0,0,1) = -Spls_12*Spls_12*Spls_12/Spls_23/Spls_31;

          hard_ME_five_.ME(0,0,0,1,0) = -Spls_31*Spls_31*Spls_31/Spls_23/Spls_12;
          hard_ME_five_.ME(1,1,0,1,0) = -Spls_31*Spls_31*Spls_31/Spls_23/Spls_12;

          hard_ME_five_.ME(0,0,1,0,0) = -Spls_23*Spls_23*Spls_23/Spls_12/Spls_31;
          hard_ME_five_.ME(1,1,1,0,0) = -Spls_23*Spls_23*Spls_23/Spls_12/Spls_31;

          hard_ME_five_.ME(1,1,1,1,1) = -conj(hard_ME_five_.ME(0,0,0,0,0));
          hard_ME_five_.ME(0,0,1,1,1) = -conj(hard_ME_five_.ME(1,1,0,0,0));
          hard_ME_five_.ME(1,1,1,1,0) = -conj(hard_ME_five_.ME(0,0,0,0,1));
          hard_ME_five_.ME(0,0,1,1,0) = -conj(hard_ME_five_.ME(1,1,0,0,1));
          hard_ME_five_.ME(1,1,1,0,1) = -conj(hard_ME_five_.ME(0,0,0,1,0));
          hard_ME_five_.ME(0,0,1,0,1) = -conj(hard_ME_five_.ME(1,1,0,1,0));
          hard_ME_five_.ME(1,1,0,1,1) = -conj(hard_ME_five_.ME(0,0,1,0,0));
          hard_ME_five_.ME(0,0,0,1,1) = -conj(hard_ME_five_.ME(1,1,1,0,0));
        }

        else if (n_g == 1 && n_q == 1 && n_qbar == 1) {
          // Two possible cases: Z -> qq -> qqg or h -> gg -> gqq
          int i_q=0, i_qbar=0, i_g=0;
          Momentum p_q, p_qbar, p_g;
          for (int i=0; i<3; i++) {
            if (event[i].pdgid() == 21)    {
              i_g = i;
              p_g = event[i].momentum();
            }
            else if (event[i].pdgid() > 0) {
              i_q = i;
              p_q = event[i].momentum();
            }
            else if (event[i].pdgid() < 0) {
              i_qbar = i;
              p_qbar = event[i].momentum();
            }
            else {
              assert(false && "unrecognised PDGid");
            }
          }
          PSCOMPLEX Spls_q_g, Smin_q_g;
          PSCOMPLEX Spls_qbar_g, Smin_qbar_g;
          PSCOMPLEX Spls_q_qbar, Smin_q_qbar;

          get_Spls_Smin_with_norm(p_q, p_g,    spinor_product_reference_direction_, Spls_q_g,    Smin_q_g);
          get_Spls_Smin_with_norm(p_qbar, p_g, spinor_product_reference_direction_, Spls_qbar_g, Smin_qbar_g);
          get_Spls_Smin_with_norm(p_q, p_qbar, spinor_product_reference_direction_, Spls_q_qbar, Smin_q_qbar);

          // Partons - order is always q qbar g
          parton_pointers.push_back(construct_initial_parton(event[i_q]));
          parton_pointers.push_back(construct_initial_parton(event[i_qbar]));
          parton_pointers.push_back(construct_initial_parton(event[i_g]));
          hard_ME_five_.initialise(parton_pointers);

          // The Z case always has the gluon in the last position
          if (event[2].pdgid() == 21) {
            PSCOMPLEX Spls_em_q, Smin_em_q;
            PSCOMPLEX Spls_ep_q, Smin_ep_q;
            PSCOMPLEX Spls_em_qbar, Smin_em_qbar;
            PSCOMPLEX Spls_ep_qbar, Smin_ep_qbar;
            PSCOMPLEX Spls_em_ep, Smin_em_ep;

            get_Spls_Smin_with_norm(p_emin, p_q, spinor_product_reference_direction_, Spls_em_q, Smin_em_q);
            get_Spls_Smin_with_norm(p_epls, p_q, spinor_product_reference_direction_, Spls_ep_q, Smin_ep_q);

            get_Spls_Smin_with_norm(p_emin, p_qbar, spinor_product_reference_direction_, Spls_em_qbar, Smin_em_qbar);
            get_Spls_Smin_with_norm(p_epls, p_qbar, spinor_product_reference_direction_, Spls_ep_qbar, Smin_ep_qbar);

            get_Spls_Smin_with_norm(p_emin, p_epls, spinor_product_reference_direction_, Spls_em_ep, Smin_em_ep);

            hard_ME_five_.ME_(0,1,0,1,0) =  Smin_em_qbar*Smin_em_qbar*Spls_em_ep/Smin_q_g/Smin_qbar_g;
            hard_ME_five_.ME_(0,1,0,1,1) =  Spls_ep_q   *Spls_ep_q   *Smin_em_ep/Spls_q_g/Spls_qbar_g;
            hard_ME_five_.ME_(0,1,1,0,0) = -Smin_em_q   *Smin_em_q   *Spls_em_ep/Smin_q_g/Smin_qbar_g;
            hard_ME_five_.ME_(0,1,1,0,1) = -Spls_ep_qbar*Spls_ep_qbar*Smin_em_ep/Spls_q_g/Spls_qbar_g;

            hard_ME_five_.ME_(1,0,1,0,1) = -conj(hard_ME_five_.ME(0,1,0,1,0));
            hard_ME_five_.ME_(1,0,1,0,0) = -conj(hard_ME_five_.ME(0,1,0,1,1));
            hard_ME_five_.ME_(1,0,0,1,1) = -conj(hard_ME_five_.ME(0,1,1,0,0));
            hard_ME_five_.ME_(1,0,0,1,0) = -conj(hard_ME_five_.ME(0,1,1,0,1));
          }

          // Otherwise it's h
          else {
            hard_ME_five_.ME_(0,0,0,1,0) = -Spls_q_g*Spls_q_g/Spls_q_qbar;
            hard_ME_five_.ME_(1,1,0,1,0) = -Spls_q_g*Spls_q_g/Spls_q_qbar;
            hard_ME_five_.ME_(0,0,1,0,0) = Spls_qbar_g*Spls_qbar_g/Spls_q_qbar;
            hard_ME_five_.ME_(1,1,1,0,0) = Spls_qbar_g*Spls_qbar_g/Spls_q_qbar;

            hard_ME_five_.ME_(1,1,1,0,1) = -conj(hard_ME_five_.ME_(0,0,0,1,0));
            hard_ME_five_.ME_(0,0,1,0,1) = -conj(hard_ME_five_.ME_(1,1,0,1,0));
            hard_ME_five_.ME_(1,1,0,1,1) = -conj(hard_ME_five_.ME_(0,0,1,0,0));
            hard_ME_five_.ME_(0,0,0,1,1) = -conj(hard_ME_five_.ME_(1,1,1,0,0));
          }
        }
        else {
          throw std::runtime_error("Spin Correlations: Unknown 3-body hard scattering.");
        }
        hard_ME_five_.set_MEstar_values();
      }
      
      else if(!do_spin_corr_) return; // just do nothing in this case
      else {
        throw std::runtime_error("Spin Correlations: Incorrect number of partons in hard scattering.");
      }
    }
  }

  // Testing area for matching
  void test_matching(Event &event);

  /// Reset the tree
  void reset() {
    parton_map_.clear();
    cached_info_ = false;
    hard_ME_size_ = 0;

    barcode_original_parton_sameside_1_ = 1;
    barcode_original_parton_sameside_2_ = 2;
    barcode_original_parton_opposite_1_ = 1;
    barcode_original_parton_opposite_2_ = 2;
    barcode_first_passed_emission_sameside_1_ = 0;
    barcode_first_passed_emission_sameside_2_ = 0;

    dpsi_sameside_1_ = 0;
    dpsi_sameside_2_ = 0;
    dpsi_opposite_ = 0;

    z_primary_sameside_1_ = 0;
    z_primary_sameside_2_ = 0;
    z_primary_opposite_1_ = 0;
    z_primary_opposite_2_ = 0;
    z_secondary_sameside_1_ = 0;
    z_secondary_sameside_2_ = 0;

    found_primary_sameside_1_ = false;
    found_primary_sameside_2_ = false;
    found_secondary_sameside_1_ = false;
    found_secondary_sameside_2_ = false;
    found_primary_opposite_1_ = false;
    found_primary_opposite_2_ = false;

    is_gg_sameside_1_ = false; 
    is_gg_sameside_2_ = false;
    is_qqbar_sameside_1_ = false;
    is_qqbar_sameside_2_ = false;

    is_gg_opposite_1_ = false;
    is_gg_opposite_2_ = false;
    is_qqbar_opposite_1_ = false;
    is_qqbar_opposite_2_ = false;
  }

  void wipe_cache() {cached_info_ = false;}

  precision_type get_relative_weight() {
    if (!do_spin_corr_) {
      return 1.;
    }
    else {
      return weight_current_relative_;
    }
  }

  /// Prints out the parton list
  void print() {
    std::cout << "___________________\n" 
              << "    Parton list    \n"
              << "___________________\n";
    for(auto p: parton_map_) {
      std::cout << "barcode " << p.first << " "; 
      p.second.print();
    }
  }

  /// Compute accept probability for a branching
  /// This returns a pair of Weights:
  /// - the first  is the genuine spin weight
  /// - the second is the overhead
  /// such that "weight<=overhead" for all phis
  std::pair<Weight /*weight*/ ,Weight /*overhead*/>
  compute_phi_weight_and_overhead(ShowerBase::Element &element, ShowerBase::EmissionInfo &emission_info);

  /// Update the binary tree
  void update_tree(ShowerBase::EmissionInfo &emission_info, ShowerBase::Element &element, Event &event);

  /// enables/disables the tracking of parent/daughter spin tree information
  /// (used in analysis)
  void set_do_declustering_analysis(bool value) {do_declustering_analysis_ = value;}

  /// returns true if the spin tree information is recorded (used in analysis)
  bool do_declustering_analysis() const {return do_declustering_analysis_;}

  // Analysis routines
  void do_analysis_step(ShowerBase::EmissionInfo &emission_info, ShowerBase::Element &element, Event &event);

  //-------------------------------------------------------
  // Temporary components of the delta-psi-proxy analysis
  precision_type z_1_cut_sameside_ = 0.1;
  precision_type z_2_cut_sameside_ = 0.1;

  precision_type z_cut_opposite_ = 0.1;

  int barcode_original_parton_sameside_1_;
  int barcode_original_parton_sameside_2_;
  int barcode_first_passed_emission_sameside_1_;
  int barcode_first_passed_emission_sameside_2_;
  int barcode_original_parton_opposite_1_;
  int barcode_original_parton_opposite_2_;

  Momentum3<precision_type> primary_plane_sameside_1_;
  Momentum3<precision_type> primary_plane_sameside_2_;
  Momentum3<precision_type> primary_plane_opposite_1_;
  Momentum3<precision_type> primary_plane_opposite_2_;

  bool found_primary_sameside_1_, found_primary_sameside_2_;
  bool found_secondary_sameside_1_, found_secondary_sameside_2_;
  bool found_primary_opposite_1_, found_primary_opposite_2_;

  double dpsi_sameside_1_, dpsi_sameside_2_, dpsi_opposite_;
  double z_primary_sameside_1_, z_primary_sameside_2_;
  double z_secondary_sameside_1_, z_secondary_sameside_2_;
  double z_primary_opposite_1_, z_primary_opposite_2_;

  bool is_gg_sameside_1_, is_qqbar_sameside_1_;
  bool is_gg_sameside_2_, is_qqbar_sameside_2_; 
  bool is_gg_opposite_1_, is_qqbar_opposite_1_;
  bool is_gg_opposite_2_, is_qqbar_opposite_2_;
  //------------------------------------------------------- 

  // Map of all Partons
  std::map<int, Parton> parton_map_;

  // Hard matrix elements
  size_t hard_ME_size_ = 0;
  HardMatrixElement<2> hard_ME_two_;
  HardMatrixElement<3> hard_ME_three_;
  HardMatrixElement<4> hard_ME_four_;
  HardMatrixElement<5> hard_ME_five_;

private:

  // Switch to control if spin correlations are incorporated
  bool do_spin_corr_ = true;
  // Switch to control if soft spin corrections are applied
  bool do_soft_spin_ = true;
  // Switch to control if collinear spin projections are used
  bool do_collinear_spin_projection_ = true;

  // Reference direction (-1: let the algo choose some stable direction, 1: x-dir, 2: y-dir, 3: z-dir)
  int reference_direction_ = -1;

  // Set to true to store extra branching information
  // at each node (energies and angles of parents/daughters)
  // useful for the EEEC analysis
  bool _store_extra_branching_info = false;

  // Reference to the QCD object
  std::shared_ptr<QCDinstance> qcd_;

  // The current reference direction for spinor product calculations
  int spinor_product_reference_direction_;

  // true if the spin tree information is recorded (used in analysis)
  bool do_declustering_analysis_ = false;

  //-----------------------------------------------------------------------
  // Information that is cached internally to prevent inefficiency when 
  // multiple values of phi have to be tried

  // Bool that tracks if information has already been cached
  bool cached_info_;

  // Cached info of the branching currently under consideration
  int barcode_p_current_;
  // Vector of partons that traces the binary tree from the branching parton to the hard scattering
  std::vector<Parton*> p_branch_current_;
  // Spin density matrix of currently branching parton
  Tensor<2> spindensity_in_current_;
  // Weight of the current branching with spin correlations
  precision_type weight_current_;
  precision_type weight_current_relative_;
};
}
#endif
