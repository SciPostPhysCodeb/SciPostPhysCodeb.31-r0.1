//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER

#ifndef __DKCMW_INTERPOLATION_HH__
#define __DKCMW_INTERPOLATION_HH__

#include <string>
#include <vector>

namespace panscales{

//----------------------------------------------------------------------
/// \class DeltaKCMWInterpolationTable
/// class that created an interpolated table with values of deltaKCMW
/// needed when double double-soft corrections are on. See
/// Eq. 6 in 2307.11142 for a precise definition of deltaKCMW 
class DeltaKCMWInterpolationTable{
public:
  /// dummy ctor
  DeltaKCMWInterpolationTable(){};

  /// ctor with initilialisation:
  /// \param filename            file from which to read the full deltaKCMW table (summed over colours/flavours)
  /// \param error_offset_factor offset dKCMW by theis factor time the estimated numerical uncertainty
  /// \param subtract_fit        if true, subtract our hardcoded estimated fit and interpolate the remainder
  DeltaKCMWInterpolationTable(const std::string &filename, double error_offset_factor=0.0, bool subtract_fit=false);

  /// ctor with separate initilialisation for each colour channel
  /// \param filenameCF          file from which to read the deltaKCMW table for the CF colour channel (w CF=1.5)
  /// \param filenameCA          file from which to read the deltaKCMW table for the CA colour channel (w CA=3)
  /// \param filenameTR          file from which to read the deltaKCMW table for the nfTR colour channel (w nf=5)
  /// \param error_offset_factor offset dKCMW by theis factor time the estimated numerical uncertainty
  /// \param subtract_fit        if true, subtract our hardcoded estimated fit and interpolate the remainder
  DeltaKCMWInterpolationTable(const std::string &filenameCF,const std::string &filenameCA,const std::string &filenameTR,
			      double error_offset_factor=0.0, bool subtract_fit=false);

  /// returns DeltaKCMW for a given phase-space point
  /// \param eta_skew   skewedness (log(tan(theta_ij)/2) of the double-soft pair parent dipole
  /// \param eta1       lnb of the first of the two double-soft emissions
  /// \param phi1       phi of the first of the two double-soft emissions
  double operator()(double eta_skew, double eta1, double phi1) const;

protected:
  // shortcut to access the linearized array
  unsigned int _table_index(unsigned int iskew, unsigned int ieta1, unsigned int iphi1) const{
    return (iskew*_neta1s + ieta1) * _nphi1s + iphi1;
  }

  // helper for initialising tables with the right size
  void _init_table(){
    _dKCMWs = std::vector<double>(_nskews*_neta1s*_nphi1s, 0.0);
  }

  // read a file and add the values to a locally-stored array
  void _add_to_table(const std::string &filename, double norm, double error_offset_factor);

  // fitted function that one could subtract in order to be left with something hopefully smoother to interpolate
  double _rough_fit_function(double eta_skew, double eta1, double phi1) const;
  
  // returns i s.t.
  //
  //  - vec[i] < val < vec[i+1]   (=> i>=0 and i<size-1)
  //  - underflow goes w i=0
  //  - overflow goes w i=size-2
  //
  // frac is set such that the interpolation from a "value" table is
  //   frac * value[i] + (1-frac) value[i+1]
  unsigned int _vector_index(double val, const std::vector<double> &vec, double & frac) const;
  
  std::vector<double> _eta_skews;
  std::vector<double> _eta1s;
  std::vector<double> _phi1s;
  std::vector<double> _dKCMWs;
  unsigned int _nskews;
  unsigned int _neta1s;
  unsigned int _nphi1s;

  bool _subtract_rough_fit;
};

}


#endif // __DKCMW_INTERPOLATION_HH__
