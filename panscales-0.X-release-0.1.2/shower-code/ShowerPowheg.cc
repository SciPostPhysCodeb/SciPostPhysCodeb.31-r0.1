//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerPowheg.hh"

namespace panscales{

  //----------------------------------------------------------------------
  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerPowheg::elements(
    Event & event, int dipole_index) const {
    int iq    = event.dipoles()[dipole_index].index_q   ;
    int iqbar = event.dipoles()[dipole_index].index_qbar;
    std::unique_ptr<typename ShowerBase::Element> elms[] = {
      std::unique_ptr<typename ShowerBase::Element>(new ShowerPowheg::Element(iq, iqbar, dipole_index, &event, this)),
      std::unique_ptr<typename ShowerBase::Element>(new ShowerPowheg::Element(iqbar, iq, dipole_index, &event, this))
      };
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }
  ShowerBase::EmissionInfo* ShowerPowheg::create_emission_info() const{ return new ShowerPowheg::EmissionInfo(); }

  //----------------------------------------------------------------------
  Range ShowerPowheg::Element::lnb_generation_range(double lnv) const {
    return Range((lnv - to_double(log_T(_dipole_m2)))/(1 + _shower->beta()), (to_double(log_T(_dipole_m2)) - lnv)/(1 + _shower->beta()));
  }

  // The exact range can't be solved, see eq. 14 in 2022-03-04-powheg-map
  Range ShowerPowheg::Element::lnb_exact_range(double lnv) const {
    std::cerr << "Error: Exact range unknown " << std::endl;
    return Range(-1, 1);
  }

  double ShowerPowheg::Element::lnv_lnb_max_density() const {
    // For now we work in a large-NC limit, with CF defined to be equal
    // to CA/2; the density that is returned here corresponds to that
    // for a quark or for half a gluon
    return _shower->max_alphas() * _shower->qcd().CA() / M_PI;
  }

  /// jacobian of this element
  precision_type ShowerPowheg::Element::dPhi_radiation_dlnv_dlnb(double lnv, double lnb) const {
    // phase-space boundaries
    double lnQ = to_double(0.5*log_T(event().Q2()));
    if (lnv == lnQ) return 0;
    if (fabs(lnb) > (lnQ - lnv)/(1+_shower->beta())) return 0;
    
    // precision_type dipole_m2 = (element.emitter() + element.spectator()).m2();
    precision_type exp_rap = exp(-lnb);
    precision_type omy     = 2*pow2(sin(atan(exp_rap)));
    precision_type omy2    = pow2(sin(2*atan(exp_rap)));
    precision_type xsi     = exp( lnv - 0.5*log_T(_dipole_m2/4) - log_T(sin(2*atan(exp(-lnb)))) + _shower->beta()*fabs(lnb) );

    precision_type xi  = 2.*(1. - xsi)/(2. - xsi*omy);
    precision_type jac = pow2(xi*xsi)*omy2/(1-xsi)/2;

    // if xsi is about 1, we use an expansion in (1-xsi)
    if (fabs(xsi-1) < numeric_limit_extras<precision_type>::sqrt_epsilon()) {
      // if y is simultaneously close to 2
      if (fabs(omy-2) < numeric_limit_extras<precision_type>::sqrt_epsilon()) {
        return 0;
      }
      // xsi -> 1 expansion of the above Jacobian (for omy != 2)
      return 2.*(1-xsi)*omy2/pow2(omy-2);
    }
    return jac;
  }

  // partitioning factor
  precision_type ShowerPowheg::Element::get_partitioning_factor_matching_probability(typename ShowerBase::EmissionInfo * emission_info) const {
    precision_type d_em = emission_info->emit_dot_rad/event().Q2();
    precision_type d_sp = emission_info->spec_dot_rad/event().Q2();
    return 1./(1 + d_em/d_sp);
  }
  
  //----------------------------------------------------------------------
  bool ShowerPowheg::veto_emission_after_matching(const double lnv_first, const double lnkt, const double eta) const {

    // The emission is to be vetoed if the point (lnkt, eta) is above the lnv contour.
    // The Powheg contour is constructed to be just kt = v*exp(beta*|eta|).
    // So, the emission is to be vetoed if lnkt > lnv_first + beta*|eta|
    if (lnkt > lnv_first + _beta*fabs(eta)) return true;
    else return false;
  }

  //----------------------------------------------------------------------
  /// Acceptance probability is not supposed to be used
  /// Only do some kinematics here
  bool ShowerPowheg::Element::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPowheg
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPowheg::EmissionInfo & emission_info = *(static_cast<typename ShowerPowheg::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;

    /// Phase-space variables given by Eq. (C14) in arXiv:2301.09645
    precision_type exp_rap = exp(-lnb);
    precision_type y = cos(2*atan(exp_rap)); 
    precision_type omy = 2*pow2(sin(atan(exp_rap)));
    precision_type omy2 = pow2(sin(2*atan(exp_rap)));
    precision_type xsi = exp( lnv - 0.5*log(_dipole_m2/4) - log(sin(2*atan(exp(-lnb)))) + _shower->_beta*fabs(lnb) );

    emission_info.xsi = xsi;
    emission_info.y = y;
    emission_info.omy = omy;
    emission_info.omy2 = omy2;
    
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter   = xsi;
    emission_info.z_radiation_wrt_spectator = 0.0;

    // get the radiations weoghts/probabilities
    emission_info.emitter_weight_rad_gluon   = 1;
    emission_info.emitter_weight_rad_quark   = 0;
    emission_info.spectator_weight_rad_gluon = 0;
    emission_info.spectator_weight_rad_quark = 0;

    
    // Check phase space 
    if (xsi >= 1.) return false;

    return true;  
  }


  //----------------------------------------------------------------------
  bool ShowerPowheg::Element::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, 
        const RotatedPieces & rp) const {
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerPowheg
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerPowheg::EmissionInfo & emission_info = *(static_cast<typename ShowerPowheg::EmissionInfo*>(emission_info_base));

    // retrieve cached info
    const double & phi = emission_info.phi;
    const precision_type & xsi  = emission_info.xsi;
    const precision_type & y    = emission_info.y;
    const precision_type & omy  = emission_info.omy;
    const precision_type & omy2 = emission_info.omy2;

    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);

    // solutions of Eqs. (C12a-c) in arXiv:2301.09645
    precision_type denom = 2. - (2. - xsi)*xsi*omy;
    precision_type ai = (1. - xsi)*(2. - xsi*omy)/denom;
    precision_type bi = (1. - xsi)*xsi*xsi*omy2/(2. - xsi*omy)/denom;
    precision_type ak = xsi*(1. + y)/denom;
    precision_type bk = pow2(1-xsi)*xsi*omy/denom;
    precision_type bj = 1. - bi - bk;

    precision_type norm_perp = sqrt(_dipole_m2*omy2)*(1. - xsi)*xsi/denom;

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
    emission_info.k_perp = perp;

    // Note: we use p3 in the operations here to avoid going via special "mass preserving" operations
    // (and mass tests) which bring no benefit in a case where the final mass should be zero.
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + bi * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> spectator_out3 = bj * rp.spectator.p3();

    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    return true;
  }


  //----------------------------------------------------------------------
  // update the elememt kinematics (called after an emission)
  void ShowerPowheg::Element::update_kinematics() {
    if (use_diffs()) {
      _dipole_m2 = 2*dot_product_with_dirdiff(emitter(), spectator(), dipole().dirdiff_3_minus_3bar); 
    } else {
      _dipole_m2 = (emitter() + spectator()).m2();
    }
    if (_dipole_m2 <= 0) {
      std::cerr << "ShowerPowheg::Element::update_kinematics: zero mass element with dipole_m2 = " << _dipole_m2 << std::endl;
      std::cerr << "   emitter  : " << emitter()   << std::endl;
      std::cerr << "   spectator: " << spectator() << std::endl;
      std::cerr << "   dipole   : " << emitter()+spectator() << std::endl;
      std::cerr << "   1-cos(th): " << one_minus_costheta(emitter(),spectator()) << std::endl;
      if (use_diffs()) {
        std::cerr << "   dirdiff  : " << dipole().dirdiff_3_minus_3bar << std::endl;
      }
      throw ErrorZeroMassElement();
    }
  }

  //----------------------------------------------------------------------    
  // update the element indices and kinematics
  void ShowerPowheg::Element::update_indices(unsigned emitter_index, unsigned spectator_index) {
    _emitter_index   = emitter_index;
    _spectator_index = spectator_index;
    update_kinematics();
  }

  //----------------------------------------------------------------------
  double ShowerPowheg::Element::lnkt_approx(double lnv, double lnb) const {return lnv + _shower->beta()*fabs(lnb);}

  // lnb is just eta for the first emission
  double ShowerPowheg::Element::eta_approx(double lnv, double lnb) const {return lnb;}

  double ShowerPowheg::Element::lnb_for_min_abseta(double lnv) const {return 0;}

  //----------------------------------------------------------------------
  bool ShowerPowheg::find_lnv_lnb_from_kinematics(const Momentum &p_emit,
                                     const Momentum &p_spec,
                                     const Momentum &p_rad,
                                     const precision_type &p_emit_dot_p_spec,
                                     const precision_type &p_emit_dot_p_rad,
                                     const precision_type &p_spec_dot_p_rad,
                                     const Event &event,
                                     double &lnv, double &lnb) const {

    // Note: the code below is not fully tested
    precision_type norm = 0.5*sqrt(event.Q2());
    precision_type xsi = p_rad.E()/norm;
    precision_type omy = p_emit_dot_p_rad / p_emit.E() / p_rad.E();

    // when theta is small (omy < sqrt(eps)), switch to theta = sqrt(2*omy)
    // we expand -log(tan(theta/2)) = -log(tan(sqrt(omy/2)))
    if (fabs(omy) < numeric_limit_extras<precision_type>::sqrt_epsilon()) {
      lnb = -0.5*to_double(log_T(0.5*omy) + omy/3. + 7./180*omy*omy);
      lnv = to_double(log_T(sqrt(event.Q2())/2) + log_T(sin(2*atan(exp(-lnb)))) + log_T(xsi) - _beta*fabs(lnb));
      return true;
    }

    // when theta is close to pi (|omy-2| < sqrt(eps)), in the anti-collinear limit,
    // we use the following trick:
    // Let's assume that θ_ij ~ pi is the angle we're trying to compute precisely.
    // We figure out which of the two other angles, θ_ik and θ_jk, is the smallest.
    // Let's assume, without loss of generality, that it is θ_ik.
    // We decompose θ_ik into two parts, θ_itildei and θ_itildek. Then we have
    //
    //            kt_i = kt_k
    //  ↔ xi θ_itildei = xk θ_itildek (*)
    //  ↔         θ_ik = θ_itildei + θ_itildek = θ_itildei*(1 + xi/xk)
    //  ↔    θ_itildei = θ_ik / (1 + xi/xk)
    //
    // We then have
    //
    //        θ_ij = π - θ_itildei = π - θ_ik / (1 + xi/xk)
    // Finally, using tan(π/2 - a) = 1/tan(a),
    //
    //  lnb = η = -log(tan(θ_ij/2)) = -log(tan(π/2 - θ_ik / 2 / (1 + xi/xk)))
    //      = log(tan(θ_ik / 2 / (1 + xi/xk)))
    //
    //             θ_ik     i
    //                       \                         .
    //              θ_itildei \ θ_ij
    //       itilde  ``````````------------------ j
    //              θ_itildek / θ_jk
    //                       /
    //                      k
    //
    if (fabs(omy-2) < numeric_limit_extras<precision_type>::sqrt_epsilon()) {

      // first, try to compute 2π - θ_ik - θ_jk, if all angles are large, but the sum
      // is not close to pi.
      precision_type omc_theta_ij = p_emit_dot_p_spec/p_emit.E()/p_spec.E();
      precision_type omc_theta_jk = p_spec_dot_p_rad/p_spec.E()/p_rad.E();

      if (omc_theta_ij > numeric_limit_extras<precision_type>::sqrt_epsilon() &&
          omc_theta_jk > numeric_limit_extras<precision_type>::sqrt_epsilon() &&
          fabs(omc_theta_ij + omc_theta_jk - 2) > numeric_limit_extras<precision_type>::sqrt_epsilon()) {

        precision_type theta_ik = 2*M_PI - acos(1-omc_theta_ij) - acos(1-omc_theta_jk);
        precision_type tan_theta_ik_half = tan(0.5*theta_ik);
        if (tan_theta_ik_half < 0) tan_theta_ik_half = -tan_theta_ik_half;
        lnb = -to_double(log_T(tan_theta_ik_half));
      }
      // else, use the strategy above
      else {
        precision_type omc_theta_ik_1 = p_emit_dot_p_spec/p_emit.E()/p_spec.E();
        precision_type omc_theta_ik_2 = p_spec_dot_p_rad/p_spec.E()/p_rad.E();

        precision_type omc_theta_ik, xi, xk;
        // θ_ik < θ_jk
        if (omc_theta_ik_1 < omc_theta_ik_2) {
          omc_theta_ik = omc_theta_ik_1;
          xi = p_emit_dot_p_rad;
          xk = p_spec_dot_p_rad;
        }
        // θ_jk < θ_ik
        else {
          omc_theta_ik = omc_theta_ik_2;
          xi = p_emit_dot_p_rad;
          xk = p_emit_dot_p_spec;
        }
        //
        // if theta_ik is small, use small-angle formulae (while this sometimes differs from
        // the value computed exactly, the resulting acceptance probabilities are negligible)
        if (fabs(omc_theta_ik) < numeric_limit_extras<precision_type>::sqrt_epsilon()) {
          // small-angle formula (tan θ/2 = θ/2, 1-cosθ = θ^2/2)
          lnb = 0.5*to_double(log_T(0.5*omc_theta_ik/pow(1+xi/xk,2)));
        }
        //
        // if theta_ik is not small, then the radiation is soft and we just take
        // the actual rapidity (in the event com frame) as lnb = eta
        //
        // Note: while we could in principle apply the same strategy as above,
        // the starting formula, assuming neither θ_itildei or θ_itildei is
        // small, is:
        //            kt_i = kt_k
        //  ↔ xi sin(θ_itildei) = xk sin(θ_itildek)
        //
        // which is a transcendental equation.
        else {
          lnb = to_double(-fabs(p_emit.rap()));
        }
      }
      // lnv should be numerically stable
      lnv = to_double(log_T(sqrt(event.Q2())/2) + log_T(sin(2*atan(exp(-lnb)))) + log_T(xsi) - _beta*fabs(lnb));
    }
    else {
      lnb = -to_double(log_T(tan(0.5*acos(1-omy))));
      lnv = to_double(log_T(sqrt(event.Q2())/2) + log_T(sin(2*atan(exp(-lnb)))) + log_T(xsi) - _beta*fabs(lnb));
    }

    return true;
  }
 
} // namespace panscales
