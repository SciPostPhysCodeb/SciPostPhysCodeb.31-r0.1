//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ProcessHandler.hh"

namespace panscales{

/////////////////////////////////////////
/// e+e- -> Z -> q qbar 
/////////////////////////////////////////

// constructor for the Z->qqbar process
ProcessZ2qq::ProcessZ2qq(CmdLine * cmdline, double rts) : ProcessBase(rts) {
  // select the flavour of Born partons
  cmdline->start_subsection("e+e- -> qqbar");
  _born_flav    = cmdline->value<int>("-born-flav", 1).help("Determine the flavour of the qqbar pair");
  // skew angle of the q qbar pair. Either as an angle or as a rapidity
  _skew_angle   = cmdline->value<double>("-skew-qq", 0).help("Skew angle of the qqbar pair," 
                                                            " with the Z-boson momentum-component along the beam azis pZ_Z = E_Z (1-cos(skew_angle))");

  auto skew_eta = cmdline->value<double>("-skew-qq-photon-eta",-1e100).argname("eta")
                  .help("if present, generate skewed event q-qbar-γ events, "
                        "where the anti-quark has a rapidity eta relative to the quark");
  _skew_eta = skew_eta.value();
  _has_skew_eta = skew_eta.present();


  // generate the orientation of the initial event randomly
  _random_axis       = cmdline->present("-random-axis").help("Generate events according to the Born distribution");
  // ... or along a random, but fixed axis
  _fixed_random_axis = cmdline->present("-fixed-random-axis").help("generate an axis that is hard-coded, but not aligned with any x,y,z axis (for debugging");
  // or along x axis
  _x_axis            = cmdline->present("-x-axis").help("Align events with the x axis");
  cmdline->end_subsection("e+e- -> qqbar");
  _use_fixed_born_event = _random_axis ? false : true;
}

Event ProcessZ2qq::generate_event() {
  // check whether we need to initialise the event
  if(_event_is_initialised && _use_fixed_born_event) return _event;

  // otherwise we set up the momenta and create an event
  Particle beam1(Momentum(0, 0, -_sqrts/2, _sqrts/2, 0), -11);
  Particle beam2(Momentum(0, 0, +_sqrts/2, _sqrts/2, 0),  11);
  // set up the partonic system 
  if(_random_axis){
    // generate random direction

    precision_type cos_theta;// = gsl.uniform(-1,1);
    while (true) {
      cos_theta = gsl.uniform(-1,1);
      double ME2 = to_double(1 + pow2(cos_theta))/2;
      if (gsl.accept(ME2)) break;
    }

    precision_type sin_theta = sqrt(1-pow2(cos_theta));
    precision_type phi = M_PI*gsl.uniform();
    precision_type px  = _sqrts/2 * sin_theta * cos(phi);
    precision_type py  = _sqrts/2 * sin_theta * sin(phi);
    precision_type pz  = _sqrts/2 * cos_theta;
    // set the 4 momenta of the q and qbar
    Particle p1(Momentum( px, py, pz, _sqrts/2, 0), +_born_flav);
    Particle p2(Momentum(-px,-py,-pz, _sqrts/2, 0), -_born_flav);
    _event = create_ee2X_event(beam1, beam2, p1, p2);
  } else if (_fixed_random_axis){
    // choose a fixed random direction
    precision_type z   = 0.21;
    precision_type t   = sqrt((1-z)*(1+z));
    precision_type phi = -2.73;
    precision_type px  = _sqrts/2 * t * cos(phi);
    precision_type py  = _sqrts/2 * t * sin(phi);
    precision_type pz  = _sqrts/2 * z;
    // set the 4 momenta of the q and qbar
    Particle p1(Momentum( px, py, pz, _sqrts/2, 0), +_born_flav);
    Particle p2(Momentum(-px,-py,-pz, _sqrts/2, 0), -_born_flav);
    _event = create_ee2X_event(beam1, beam2, p1, p2);
    _event_is_initialised = true;
  } else if (_x_axis){
    // align along x axis
    Particle p1(Momentum( _sqrts/2, 0, 0, _sqrts/2, 0), +_born_flav);
    Particle p2(Momentum(-_sqrts/2, 0, 0, _sqrts/2, 0), -_born_flav);
    // set the 4 momenta of the q and qbar
    _event = create_ee2X_event(beam1, beam2, p1, p2);
    _event_is_initialised = true;
  } else if (_has_skew_eta){
    precision_type E = _sqrts/2*sqrt(1+exp(2*_skew_eta));
    precision_type e2, th, ch;
    if (_skew_eta>0){ e2 = exp(-2*_skew_eta); th = (1-e2)/(1+e2); ch=(1+e2)*exp( _skew_eta)/2;}
    else            { e2 = exp( 2*_skew_eta); th = (e2-1)/(1+e2); ch=(e2+1)*exp(-_skew_eta)/2;}
    
    Particle beam1_local(Momentum(0, 0, -_sqrts/2, _sqrts/2, 0), -11);
    Particle beam2_local(Momentum(0, 0, +_sqrts/2, _sqrts/2, 0),  11);
    Particle p1(E * Momentum(   0, 0, 1.0, 1.0, 0), +_born_flav);
    Particle p2(E * Momentum(1/ch, 0,  th, 1.0, 0), -_born_flav);
    Momentum3<precision_type> p3rec = p1.momentum()+p2.momentum();
    Particle p3(Momentum::fromP3M2(-p3rec,0.0), 22);
    _event = create_ee2X_event(beam1_local, beam2_local, p1, p2, {p3});
    _event_is_initialised = true;
  } else if (_skew_angle>0){
    precision_type ct = cos(_skew_angle);
    precision_type st = sin(_skew_angle);
    precision_type norm;
    if(ct >= -1.0 + sqrt(std::numeric_limits<double>::epsilon())) {
      norm = _sqrts * ((1.0 - sqrt(0.5*(1 - ct))) / (1.0 + ct));
    } else {
      norm = _sqrts * 0.25 * (1.0 + (ct + 1)/8.0);
    }
    Particle beam1_local(Momentum(0, 0, -_sqrts/2, _sqrts/2, 0), -11);
    Particle beam2_local(Momentum(0, 0, +_sqrts/2, _sqrts/2, 0), 11);
    Particle p1(norm * Momentum(st, 0,  ct, 1.0, 0), +_born_flav);
    Particle p2(norm * Momentum(0, 0, -1.0, 1.0, 0), -_born_flav);
    Particle p3(Momentum(-norm*st, 0.0, norm*(1-ct), _sqrts - 2.0*norm, 0.0), 22);
    _event = create_ee2X_event(beam1_local, beam2_local, p1, p2, {p3});
    _event_is_initialised = true;      
  } else {
    // set the 4 momenta of the q and qbar including a skew angle
    precision_type norm = _sqrts/(sqrt(2.0 * (1.0 + cos(_skew_angle))));
    Particle p1(norm * Momentum(sin(_skew_angle), 0, cos(_skew_angle), 1.0, 0), +_born_flav);
    Particle p2(norm * Momentum(0, 0, -1.0, 1.0, 0), -_born_flav);
    _event = create_ee2X_event(beam1, beam2, p1, p2);
    _event_is_initialised = true;
  }

  // Store the born matrix element for matching
  // CHECK THIS FOR THE RANDOM ORIENTATIONS!
  _event.set_born_me((1. + pow2(_event[0].pz()/_event[0].E()))/2);
    
  // finalise
  return _event;
}


/////////////////////////////////////////
// e+e- -> H -> gg 
/////////////////////////////////////////

// constructor for the H->gg process
ProcessH2gg::ProcessH2gg(CmdLine * cmdline, double rts) : ProcessBase(rts) {
  cmdline->start_subsection("e+e- -> gg");
  _use_fixed_born_event = true;
  _random_axis          = cmdline->present("-random-axis").help("Generate events with a random orientation (not according to the Born matrix element)");;
  _use_fixed_born_event = _random_axis ? false : true;
  cmdline->end_subsection("e+e- -> gg");
}

Event ProcessH2gg::generate_event(){
  // check whether we need to initialise the event
  if(_event_is_initialised && _use_fixed_born_event) return _event;

  // otherwise we set up the momenta and create an event
  Particle beam1(Momentum(0, 0, -_sqrts/2, _sqrts/2, 0), -11);
  Particle beam2(Momentum(0, 0, +_sqrts/2, _sqrts/2, 0),  11);

  // set up the partonic system 
  if(_random_axis){
    // generate random direction
    double z   = gsl.uniform(-1,1);
    double t   = sqrt((1-z)*(1+z));
    double phi = M_PI*gsl.uniform();
    double px  = _sqrts/2 * t * cos(phi);
    double py  = _sqrts/2 * t * sin(phi);
    double pz  = _sqrts/2 * z;
    // set the 4 momenta of the g g
    Particle p1(Momentum(+px, py, pz, _sqrts/2, 0), 21);
    Particle p2(Momentum(-px,-py,-pz, _sqrts/2, 0), 21);
    // create the event
    _event = create_ee2X_event(beam1, beam2, p1, p2);
  } else{
    // generate gluons aligned with the beam axes
    Particle p1(Momentum(0, 0, +_sqrts/2, _sqrts/2, 0), 21);
    Particle p2(Momentum(0, 0, -_sqrts/2, _sqrts/2, 0), 21);
    _event = create_ee2X_event(beam1, beam2, p1, p2);
  }
  // Store the born matrix element for matching
  _event.set_born_me(1);
  // Finalise
  _event_is_initialised = true;
  return _event;
}


/////////////////////////////////////////
// pp -> Z 
/////////////////////////////////////////

// constructor for the Z->qqbar process
Processpp2Z::Processpp2Z(CmdLine * cmdline, double rts) : ProcessBase(rts) {
  cmdline->start_subsection("pp -> Z");
  _use_fixed_born_event = true;
  _mZ                   = cmdline->value<double>("-mZ",  91.1876).help("Mass of the Z boson");
  if((_mZ > _sqrts)  && !cmdline->help_requested())  
    throw std::runtime_error("Mass of the Z boson bigger than collider CM energy");
  _born_flav    = cmdline->value<int>("-born-flav", 1).help("Flavour of the initial-state quarks");
  _yrap         = cmdline->any_value<double>({"-yZ","-yX","-yrap"},0.0).help("Rapidity of the Z boson");
  // hard scale is mZ
  _hard_scale = _mZ;
  cmdline->end_subsection("pp -> Z");
}

Event Processpp2Z::generate_event(){
  // check whether we need to initialise the event
  if(_event_is_initialised) return _event;

  // otherwise we set up the momenta and create an event
  // beams are protons
  Particle beam1(Momentum(0, 0, -_sqrts/2, _sqrts/2, 0), 2212);
  Particle beam2(Momentum(0, 0, +_sqrts/2, _sqrts/2, 0), 2212);
  // particles are incoming quarks
  Particle p1(Momentum(0, 0, -_mZ/2 * exp(-_yrap), _mZ/2 * exp(-_yrap), 0), -_born_flav);
  Particle p2(Momentum(0, 0, +_mZ/2 * exp(+_yrap), _mZ/2 * exp(+_yrap), 0), +_born_flav);
  _event = create_pp2X_event(beam1, beam2, p1, p2);
  _event_is_initialised = true;
  return _event;
}


/////////////////////////////////////////
// pp -> H 
/////////////////////////////////////////

// constructor for the pp2H process
Processpp2H::Processpp2H(CmdLine * cmdline, double rts) : ProcessBase(rts) {
  cmdline->start_subsection("pp -> H");
  _use_fixed_born_event = true;
  _mH           = cmdline->value<double>("-mH",  125.0).help("Mass of the H boson");
  if((_mH > rts) && !cmdline->help_requested()) 
    throw std::runtime_error("Mass of the H boson bigger than collider CM energy");
  _yrap         = cmdline->any_value<double>({"-yH","-yX","-yrap"},0.0);
  // hard scale is mH
  _hard_scale = _mH;
  cmdline->end_subsection("pp -> H");
}

Event Processpp2H::generate_event(){
  // check whether we need to initialise the event
  if(_event_is_initialised) return _event;

  // otherwise we set up the momenta and create an event
  // beams are protons
  Particle beam1(Momentum(0, 0, -_sqrts/2, _sqrts/2, 0), 2212);
  Particle beam2(Momentum(0, 0, +_sqrts/2, _sqrts/2, 0), 2212);
  // particles are incoming gluons
  Particle p1(Momentum(0, 0, -_mH/2 * exp(-_yrap), _mH/2 * exp(-_yrap), 0), 21);
  Particle p2(Momentum(0, 0, +_mH/2 * exp(+_yrap), _mH/2 * exp(+_yrap), 0), 21);
  _event = create_pp2X_event(beam1, beam2, p1, p2);
  _event_is_initialised = true;
  return _event;
}


/////////////////////////////////////////
// pp -> Z(H)j 
/////////////////////////////////////////

// constructor for the pp->Z(H)j process
Processpp2Xj::Processpp2Xj(CmdLine * cmdline, double rts) : ProcessBase(rts) {
  cmdline->start_subsection("pp -> Xj");
  _use_fixed_born_event = true;
  _process_type = cmdline->present("-pp2Hj").help("Select the pp->Hj process (default it is pp->Zj)") ? ProcessType::pp2Hj : ProcessType::pp2Zj;
  auto mX = cmdline->optional_value<double>("-mX").help("Mass of the colour-singlet boson");
  if(mX.present()) _mX = mX.value();
  else                  _mX = (_process_type == ProcessType::pp2Hj) ? 125.0 : 91.1876;
  if(_mX > _sqrts  && !cmdline->help_requested()) throw std::runtime_error("Mass of the X boson bigger than collider CM energy");
  _yX  = cmdline->value<double>("-yX",0.0).help("Rapidity of the colour-singlet boson"); // rapidity of X
  _yJ  = cmdline->value<double>("-yJ",0.0).help("Rapidity of the outgoing parton");  // rapidity of j
  _phi = cmdline->value<double>("-phi",0.0).help("Azimuthal angle of the outgoing parton"); // azimuthal angle of j
  _pTj = cmdline->value<double>("-ptJ",25.).help("Transverse momentum of the outgoing parton"); // transverse momentum of j
  if(_pTj > _sqrts  && !cmdline->help_requested()) throw std::runtime_error("Transverse momentum of the jet is higher than collider CM energy");
  // determine flavours of the jet and incoming partons
  _jet_flavour      = cmdline->value<int>("-out-flav", 21).help("Flavour of the outgoing parton");
  _incoming_flavour = cmdline->value<int>("-born-flav", -1).help("Flavour of the first incoming parton");
  // hard scale is mX
  _hard_scale = _mX;
  // the following line is only relevant for PanGlobal
  _jet_is_hard      = !cmdline->present("-soft-jet").help("Do not make the jet part of the hard system");
  cmdline->end_subsection("pp -> Xj");
}


Event Processpp2Xj::generate_event(){
  // check whether we need to initialise the event
  if(_event_is_initialised) return _event;

  // otherwise we set up the momenta and create an event
  // beams are protons
  Particle beam1(Momentum(0, 0, -_sqrts/2, _sqrts/2, 0), 2212);
  Particle beam2(Momentum(0, 0, +_sqrts/2, _sqrts/2, 0), 2212);
  // select the flavour of incoming and outoing legs
  int in_flav1 = (_jet_flavour == 21) ? ((_process_type == ProcessType::pp2Hj) ? 21 : _incoming_flavour) : 21;
  int in_flav2 = (_jet_flavour == 21) ? ((_process_type == ProcessType::pp2Hj) ? 21 : -in_flav1)         : _jet_flavour;
  // choose a Higgs or a Z
  int boson_pdgid = (_process_type == ProcessType::pp2Hj) ? 25 : 23;
  // mXT = transverse mass of X boson
  double mXT = sqrt(pow2(_mX) + pow2(_pTj));
  // set the 4 momenta of the incoming legs
  Particle p1(Momentum(0, 0, -mXT/2 * exp(-_yX) - _pTj/2*exp(-_yJ),  mXT/2 * exp(-_yX) + _pTj/2*exp(-_yJ), 0), in_flav1);
  Particle p2(Momentum(0, 0,  mXT/2 * exp( _yX) + _pTj/2*exp( _yJ),  mXT/2 * exp( _yX) + _pTj/2*exp( _yJ), 0), in_flav2);
  // outgoing particle (jet)
  Particle p_out(Momentum(_pTj*cos(_phi), _pTj*sin(_phi), _pTj*sinh(_yJ), _pTj*cosh(_yJ), 0), _jet_flavour);
  // we construct X(=Z or higgs) by momentum conservation
  Particle pX(p1 + p2 - p_out, boson_pdgid); 
  // create the event
  _event = create_pp2Xj_event(beam1, beam2, p1, p2, pX, p_out);
  if(_jet_is_hard) _event[3].set_hard_system(true);
    
  // finalise
  _event_is_initialised = true;
  return _event;
}


/////////////////////////////////////////
// pp -> jj.
/////////////////////////////////////////

// constructor for the pp->jj process
Processpp2jj::Processpp2jj(CmdLine * cmdline, double rts) : ProcessBase(rts) {
  _use_fixed_born_event = true;
  cmdline->start_subsection("pp -> jj");
  _yJ  = cmdline->value<double>("-yJ",0.0).help("Rapidity of the first outgoing hard parton"); // rapidity of outgoing leg j
  _yR  = cmdline->value<double>("-yR",0.0).help("Rapidity of the second outgoing hard parton"); ; // rapidity of outgoing, recoiling leg j 
  _phi = cmdline->value<double>("-phi",0.0).help("Azimuthal angle of the first outgoing hard parton"); ; // azimuthal angle of j
  _pTj = cmdline->value<double>("-ptJ",25.).help("Transverse momentum of the first outgoing hard parton"); ; // transverse momentum of j
  // hard scale is pTj
  _hard_scale = _pTj;
  if(_pTj > _sqrts  && !cmdline->help_requested())  throw std::runtime_error("Transverse momentum of the jet is higher than collider CM energy");
    
  // particles are incoming quarks for now
  _incoming_flavour_parton_2 = 2;
  // choose flavour based on colour flow. For a 2to2 process ab->cd,
  // we have so far implemented two dipole structures
  //      - IFIF = (ac) (bd)
  //      - IIFF = (ab) (cd)
  if(cmdline->present("-IFIF").help("Denoting the 2->2 process as ab->cd, assign colour connections between (ac) and (bd)")){ 
    _incoming_flavour_parton_2 = +2;
  } else if(cmdline->present("-IIFF").help("Denoting the 2->2 process as ab->cd, assign colour connections between (ab) and (cd)")){
    _incoming_flavour_parton_2 = -2;  
  } else{
    if(!cmdline->help_requested()) throw std::runtime_error("Cannot handle this dipole structure in dijets, specify -IFIF or -IIFF");
  }

  // determine flavours of the jet and incoming partons
  _incoming_flavour_parton_1 = 1;
  // decide whether jet is part of the hard system for PanGlobal
  _jet_is_hard      = !cmdline->present("-soft-jet").help("Do not make the jet part of the hard system");
  cmdline->end_subsection("pp -> jj");
}

Event Processpp2jj::generate_event(){
  // check whether we need to initialise the event
  if(_event_is_initialised) return _event;

  // otherwise we set up the momenta and create an event
  // beams are protons
  Particle beam1(Momentum(0, 0, -_sqrts/2, _sqrts/2, 0), 2212);
  Particle beam2(Momentum(0, 0, +_sqrts/2, _sqrts/2, 0), 2212);
    
  // set up momentum of incoming legs
  Particle p1(Momentum(0, 0, - _pTj/2 *(exp(-_yR) + exp(-_yJ)), _pTj/2 * (exp(-_yR) + exp(-_yJ)), 0), _incoming_flavour_parton_1);

  // outgoing jet
  Particle pj_out(Momentum( _pTj*cos(_phi), _pTj*sin(_phi),  _pTj*sinh(_yJ), _pTj*cosh(_yJ), 0), _incoming_flavour_parton_1);
  Particle pr_out(Momentum(-_pTj*cos(_phi), -_pTj*sin(_phi), _pTj*sinh(_yR), _pTj*cosh(_yR), 0), _incoming_flavour_parton_2);
  // momentum conservation to set up the other initial-state parton
  Particle p2(Momentum(pj_out + pr_out - p1,0), _incoming_flavour_parton_2);
  // create the event
  _event = create_pp2jj_event(beam1, beam2, p1, p2, pj_out, pr_out);
    
  // finalise
  _event_is_initialised = true;
  return _event;
}



/////////////////////////////////////////
// q gamma* -> q
/////////////////////////////////////////

// constructor for the q gamma* -> q process
ProcessDIS::ProcessDIS(CmdLine * cmdline, double rts) : ProcessBase(rts) {

  cmdline->start_subsection("DIS");
  _use_fixed_born_event = true;
  _Q2_dis = cmdline->value<double>("-Q2",   1).help("DIS first IF dipole invariant mass");
  _y      = cmdline->value<double>("-y",  0.2).help("DIS variable y: (1-cth)/2 between the outgoing and incoming quark in the event frame");
  _x_dis  = _Q2_dis/pow2(_sqrts)/_y;
  // hard scale is sqrt(Q2)
  _hard_scale = sqrt(_Q2_dis);
  cmdline->end_subsection("DIS");
}

Event ProcessDIS::generate_event(){
  // check whether we need to initialise the event
  if(_event_is_initialised) return _event;

  // otherwise we set up the momenta and create an event
  // we set up the event in the Breit frame
  // incoming parton moves along the -z direction
  // outgoing parton moves along the +z direction
  // 'current direction' = z axis
  // we have
  // p_in  = x P_proton, x == x_dis 
  // p_out = x P_proton + q
  // 2.p_in.p_out = Q2 == Q2_DIS 
  double pZ = 0.5*sqrt(_Q2_dis); 
  Particle q1(Momentum::fromP3M2(Momentum3<precision_type>(0, 0, -pZ) , 0), 1); // the incoming quark
  Particle q2(Momentum::fromP3M2(Momentum3<precision_type>(0, 0,  pZ) , 0), 1); // the outgoing quark
  // from q2 and q1 we construct the photon, which is the other incoming beam & particle
  // beam1 needs to have momentum q = q2 - q1
  Particle beam1(Momentum(q2 - q1), 22); // the photon
  // q^2 needs to be negative
  if(beam1.m2() >= 0) throw std::runtime_error("Photon beam has 0 or positive invariant mass");
  // now construct the proton beam
  double pZ_of_proton = pZ / _x_dis;
  Particle beam2(Momentum::fromP3M2(Momentum3<precision_type>(0., 0., -pZ_of_proton), 0), 2212); // the incoming proton (neglecting mass)
  // now we may create the DIS event
  _event = create_DIS_event(beam1, beam2, q1, q2);
  _event_is_initialised = true;
  return _event;
}



/////////////////////////////////////////
// q q' -> H q q'
/////////////////////////////////////////

// constructor for the  q q -> H q q (VBF) process
ProcessVBF::ProcessVBF(CmdLine * cmdline, double rts) : ProcessBase(rts) {
  _use_fixed_born_event = true;
  cmdline->start_subsection("VBF");
  // set up the incoming partons and the higgs boson
  _Q    = cmdline->value<double>("-Qinv-hjj", 1).help("invariant mass of the Higgs+dijet system");
  // Q = hard scale
  _hard_scale = _Q;
  _ytot = cmdline->value<double>("-ytot", 0).help("rapidity of the higgs+ditjet system");
  // select the flavours
  _in_flav1 = cmdline->value<int>("-flav1",1).help("flavour of parton 1");
  _in_flav2 = cmdline->value<int>("-flav2",2).help("flavour of parton 2");
  // get the higgs boson properties
  _mH   = cmdline->value<double>("-mH", 0.25).help("mass of the higgs boson");
  _yH   = cmdline->value<double>("-yH", 0).help("rapidity of the higgs boson");
  _pTH  = cmdline->value<double>("-pTH", _mH/2).help("transverse momentum of the higgs boson");
  cmdline->end_subsection("VBF");
}

Event ProcessVBF::generate_event(){
  // check whether we need to initialise the event
  if(_event_is_initialised) return _event;

  // set up an event in the CM frame of the protons
  // set up the beams
  Particle beam1(Momentum(0, 0, -_sqrts/2, _sqrts/2, 0), 2212);
  Particle beam2(Momentum(0, 0, +_sqrts/2, _sqrts/2, 0), 2212);
    
  // set up the incoming partons
  Particle p1(Momentum(0, 0, -_Q/2 * exp(-_ytot), _Q/2 * exp(-_ytot), 0), _in_flav1);
  Particle p2(Momentum(0, 0, +_Q/2 * exp(+_ytot), _Q/2 * exp(+_ytot), 0), _in_flav2);
    
  // set up the Higgs boson momentum
  // get the momentum in the z direction given the rapidity and the transverse momentum
  double pZH  = sqrt(pow2(_mH) + pow2(_pTH)) * sinh(_yH);
  double eH   = sqrt(pow2(_mH) + pow2(pZH) + pow2(_pTH));
  // get the momentum of the Higgs boson itself
  Particle pH(Momentum(_pTH, 0, pZH, eH), 25);

  // take ZZh fusion, same flavours
  Momentum pJtot = p1 + p2 - pH;
  double pZj1 = to_double((pZH*(pZH - pJtot.E())*(pZH + pJtot.E()) - sqrt(pow2(pJtot.E())*(pZH - pJtot.E())*(pZH + pJtot.E())*(-pow2(pJtot.E()) + pow2(_pTH) + pow2(pZH))))/(2*(- pZH - pJtot.E())*(pZH - pJtot.E())));
  Particle pj1(Momentum(-_pTH/2, 0, pZj1 , sqrt(pow2(pZj1) + pow2(_pTH/2))), _in_flav1);
  Particle pj2(Momentum::fromP3M2((p1 + p2 - pH - pj1).p3(), 0), _in_flav2);
    
  // create the VBF event
  _event = create_VBF_event(beam1, beam2, p1, p2, pH, pj1, pj2);
  // finalise
  _event_is_initialised = true;
  return _event;
}



}
#include "autogen/auto_ProcessHandler_global-cc.hh"
