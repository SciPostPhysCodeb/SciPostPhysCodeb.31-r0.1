//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
/// just write a series of events to disk

#include "AnalysisFramework.hh"
#include <fstream>

using namespace std;
using namespace panscales;

class ExampleFramework : public  AnalysisFramework {
public:
  ExampleFramework(CmdLine * cmdline_in) : AnalysisFramework(cmdline_in) {}

  //----------------------------------------------------------------------
  /// options needed by the "user" analysis can be set here
  void user_post_startup() {
    _tuple_file.reset(new ofstream(output_filename+".tuple"));
  }

  //----------------------------------------------------------------------
  /// this gets called once for every event and should carry
  /// out the analysis and output histograms
  void user_analyse_event() {
    // get event info
    double evwgt = event_weight();
    const auto & particles = f_event.particles();

    (*_tuple_file) << "# EVENT " << iev
                   << " weight "<< evwgt << endl;
    for (const auto &p : particles){
      (*_tuple_file) << setprecision(15) << p << endl;
    }
  }

protected:
  shared_ptr<ofstream> _tuple_file;
};
  
//----------------------------------------------------------------------
/// a minimal main program
int main(int argc, char** argv) {
  CmdLine cmdline(argc,argv,true);
  ExampleFramework driver(&cmdline);
  driver.run();
}
