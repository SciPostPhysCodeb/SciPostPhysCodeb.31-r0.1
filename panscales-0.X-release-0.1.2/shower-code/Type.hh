//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __TYPE_HH__
#define __TYPE_HH__

#include "config.hh"
#include <cmath>
#include <string>

// precision selection is based on 
// preprocessor variables

//#define PSDOUBLEEXP
#if defined PSQUAD
// float128 is about 45x slower than double
// precision: 4e-34
#include <boost/multiprecision/float128.hpp>
using namespace boost::multiprecision;
typedef float128 precision_type;

#elif defined PSDDREAL
// dd_real is about 10x slower than double
// precision: 1e-31
#include <qd/dd_real.h>
#include <qd/fpu.h>
typedef dd_real precision_type;
namespace std{
  using ::sqrt;
}

#elif defined PSQDREAL
// qd_real is about 141x slower than double
// precision: 2e-64
#include <qd/qd_real.h>
#include <qd/fpu.h>
typedef qd_real precision_type;
namespace std{
  using ::sqrt;
}
 
#elif defined PSFLOAT
typedef float precision_type;

#elif defined PSDOUBLEEXP
#include "double_exp.hh"
typedef double_exp precision_type;

#elif defined PSDDEXP
#include "double_exp.hh"
#include "../helpers/double_exp/twotypes/twotypes.hh"
typedef TwoTypes<double,double_exp> precision_type;
template<> inline precision_type::operator double() const {return a;}
//inline double to_double(double_exp b) {return double(b);}
template<> inline bool precision_type::check_OK() {
  return std::abs(a-b) <= threshold*std::max(abs(double(a)),abs(double(b)));
} 
namespace std {
  template<> constexpr precision_type numeric_limits<precision_type>::epsilon() noexcept {
    return precision_type(numeric_limits<double>::epsilon(),numeric_limits<double_exp>::epsilon());
  }
  template<> constexpr precision_type numeric_limits<precision_type>::infinity() noexcept {
    return precision_type(numeric_limits<double>::infinity(),numeric_limits<double_exp>::infinity());
  }
  template<> constexpr precision_type numeric_limits<precision_type>::max() noexcept {
    return precision_type(numeric_limits<double>::max(),numeric_limits<double_exp>::max());
  }
}

#elif defined PSMPFR4096
#define MPFR_REAL_ENABLE_CONV_OPS
#include "mpfr_real.hh"
typedef mpfr::real<4096> precision_type;

#else
// define this to help with instantiating functions
// that are needed in both double and precision_type
#define PSDOUBLE
typedef double precision_type;


#endif // end of precision selection 

#if !defined(PSDDREAL) && !defined(PSQDREAL)
inline double to_double(precision_type x) {return double(x);}
#endif


#if defined PSDDREAL || defined PSQDREAL
namespace std {
  using ::abs;
}
#endif

/// \class numeric_limit_extras
///
/// This plays the role of numeric_limits, but is made slightly
/// more robust across different numeric types and includes
/// extra features such as sqrt(epsilon). It only implements
/// some of the constructs.
/// 
/// Individual parameters are defined manually in the .cc file
/// 
template <class T> 
class numeric_limit_extras {
public:
  inline static T sqrt_epsilon() {return sqrt_epsilon_;}
  inline static T epsilon() {return epsilon_;}
  inline static double log_max() {return log_max_;}
  inline static T infinity() {return infinity_;}
  inline static T max() {return max_;}
private:
  static T sqrt_epsilon_;
  static T epsilon_;
  static double log_max_;
  static T infinity_;
  static T max_;
};

// class with initialization need for QD library
template <class T>
class init_type {
public:
  init_type(); 
  ~init_type();
private:
  unsigned int old_cw;
};

inline precision_type log_T(precision_type x) {
  if (x > 0.) {
    return log(x);
  } else {
    return log(0.0);
  }
}




template <> 
precision_type numeric_limit_extras<precision_type>::sqrt_epsilon_;
template <> 
precision_type numeric_limit_extras<precision_type>::epsilon_;
template <> 
double numeric_limit_extras<precision_type>::log_max_;

template <> 
precision_type numeric_limit_extras<precision_type>::infinity_;
template <> 
precision_type numeric_limit_extras<precision_type>::max_;

// for those that return double, ensure that they are
// available with the double argument, even if our main
// precision type is a different one
#ifndef PSDOUBLE
template <> 
double numeric_limit_extras<double>::sqrt_epsilon_;
template <> 
double numeric_limit_extras<double>::epsilon_;
template <> 
double numeric_limit_extras<double>::log_max_;
#endif 

/// an alternate (and less verbose) way of using our precision type
typedef precision_type Float;


/// returns a multi-line header with information about the precision
/// type being used.
///
/// Each line of the header has the specified prefix.
/// The header includes a final newline.
std::string precision_header(const std::string & prefix="# ");

//--------------------------------------------------------------------------
// the following macros are to do with debugging rather than numeric
// types, but are included here because Type.hh is included in almost
// all code.

/// returns the filename and line number
#define PANSCALES_LOCATION() std::string("at ") + __FILE__ + ":" + std::to_string(__LINE__) + ": "

/// defines an IFPSVERBOSE macro that only executes the code if PSVERBOSE is defined
#ifdef PSVERBOSE
#define IFPSVERBOSE(x) {x;}
#else // PSVERBOSE
#define IFPSVERBOSE(x) {}
#endif

/// defines a helper to print out a message for experimental things
#define PRINT_EXPERIMENTAL(x) {std::cout << "PanScales Warning: " << x << " is still experimental. Use at your own risk and not for phenomenological applications." << std::endl;}

#include "extra-math.hh"

#endif //__TYPE_HH__
