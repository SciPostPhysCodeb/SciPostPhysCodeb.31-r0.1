#!/usr/bin/env python
#
# Integrate the dKCMW table over eta1 and phi1 at fixed eta_skew

import sys
sys.path.append('../../../scripts/')
from hfile import *
from scipy.interpolate import interpn
from math import sqrt

nsk = 20
orig_neta1 = 30
orig_nphi1 = 17

nsub=1
nexcl=0

integrals={}
uncertainties={}
for beta in ['00', '05']:
    for colour in ['CF', 'CA', 'TR']:
        data=get_array(f'pg{beta}-{colour}.res')
        data = data.reshape((nsk,orig_neta1,orig_nphi1,6))
    
        orig_etas = data[0,0:orig_neta1,0,1]
        orig_phis = data[0,0,0:orig_nphi1,2]
    
        neta1 = nsub*(orig_neta1-1)+1
        eta1s = np.zeros(neta1)
        for i in range(orig_neta1-1):
            for j in range(nsub):
                eta1s[nsub*i+j] = (nsub-j)/(1.0*nsub) * orig_etas[i] + j/(1.0*nsub) * orig_etas[i+1]
        eta1s[-1] = orig_etas[-1]
    
        nphi1 = nsub*(orig_nphi1-1)+1
        phi1s = np.zeros(nphi1)
        for i in range(orig_nphi1-1):
            for j in range(nsub):
                phi1s[nsub*i+j] = (nsub-j)/(1.0*nsub) * orig_phis[i] + j/(1.0*nsub) * orig_phis[i+1]
        phi1s[-1] = orig_phis[-1]
    
        orig_points = (orig_etas, orig_phis)
        new_points = np.zeros((neta1*nphi1,2))
        for i in range(neta1):
            for j in range(nphi1):
                new_points[i*nphi1+j,0] = eta1s[i]
                new_points[i*nphi1+j,1] = phi1s[j]
    
        
        integrals[beta+colour] = np.zeros(nsk)
        uncertainties[beta+colour] = np.zeros(nsk)
    
        # to a 2D trapezoidal integration
        #
        # It is delicate to estimate the uncertainty based on the "point"
        # uncertainty (ideally we should write a separate code for teh
        # integration) but we can at least get an idea of the uncertainty
        # associated w the finite eta1 range
        for isk in range(nsk):
            s = 0.0
            u2 = 0.0
    
            # build interpolation
            if nsub==1:
                d = data[isk,:,:,3]
            else:
                orig_d = data[isk,:,:,3]
                d = interpn(orig_points, orig_d, new_points, method='cubic').reshape(neta1, nphi1)
            
            for ieta in range(neta1-1):
                for iphi in range(nphi1-1):
                    c =0.25 * (eta1s[ieta+1]-eta1s[ieta]) * (phi1s[iphi+1]-phi1s[iphi]) \
                        * (d[ieta,iphi]+d[ieta+1,iphi+1]+d[ieta+1,iphi]+d[ieta,iphi+1]) 
                    if ieta>=neta1-1-nexcl*nsub: u2+=c*c
                    else: s+=c
            print (beta, colour, data[isk,1,1,0], s, sqrt(u2))
            integrals[beta+colour][isk] = s
            uncertainties[beta+colour][isk] = sqrt(u2)
        print ()
        print ()

# plot
import matplotlib.pyplot as plt
from   matplotlib.backends.backend_pdf import PdfPages
import matplotlib.transforms as transforms
with PdfPages('integrate.pdf') as pdf:
    fig = plt.figure(figsize=(4.1,3.7))
    ax = plt.gca()
    
    ax.grid(True, which='both', lw=0.5, ls=':', zorder=0)
    plt.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
    plt.tick_params(axis='x', which='major', pad=5)
    
    plt.title(r'$\delta K_{{\rm CMW}}$ integral over $\eta_1$ and $\phi_1$')
    plt.xlabel(r'$\eta_{{\rm sk}}$')
    
    plt.ylabel(r'$\delta K_{{\rm CMW}}$ integral')
    
    # shower
    for betac,c in zip(['05CF', '05CA', '05TR'], ['blue','red','green']):
        ax.fill_between(data[:,0,0,0], integrals[betac]-uncertainties[betac], integrals[betac]+uncertainties[betac],
                        ls='-', lw=0, color=c, alpha=0.2)
        ax.plot(data[:,0,0,0], integrals[betac], ls='-', color=c, label=r'$\beta=$'+f'{betac}')
       
    # legend
    ax.legend(loc='lower right', fontsize=9)
    
    #ax.text(0.03, 0.93, r'PG(0), $\delta=12$', ha='left', transform=ax.transAxes, fontsize=9)
    
    pdf.savefig(bbox_inches='tight')
    plt.close()
   
