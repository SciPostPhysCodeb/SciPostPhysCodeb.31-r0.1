//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __EXTRA_MATH_HH__
#define __EXTRA_MATH_HH__
#include <complex>
#include "Complex.hh"

/// return the square
template <class T>
inline T pow2(T x) {return x*x;}

/// return the cube
template <class T>
inline T pow3(T x) {return x*x*x;}

/// dd_real and qd_real has the "strange" (*) behaviour that
/// pow({dq}d_real, double) converts the double to an int rather than a
/// {dq}d_real. We explicitly define pow({dq}d_real, double) to avoid
/// this issue
#if defined  PSDDREAL
#include <qd/dd_real.h>
#include <qd/fpu.h>
inline dd_real pow(dd_real x, double a) {return pow(x,dd_real(a));}
#endif
#if defined PSQDREAL
#include <qd/qd_real.h>
#include <qd/fpu.h>
inline qd_real pow(qd_real x, double a) {return pow(x,qd_real(a));}
#endif



// choice between c++'s std::complex<> (which on some systems is limited
// to using precision_type that is among float, double, long double)
// and our own panscales::complex<> type, which is more basic
// (e.g. no special functions), but seems to work more widely
//#define PSCOMPLEX std::complex<precision_type>
#define PSCOMPLEX panscales::Complex<precision_type>


#if defined PSDDREAL || defined PSQDREAL || defined PSQUAD || defined PSDOUBLEEXP || defined PSMPFR4096
inline PSCOMPLEX operator*(PSCOMPLEX x, double y) {return x * precision_type(y);}
inline PSCOMPLEX operator/(PSCOMPLEX x, double y) {return x / precision_type(y);}
inline PSCOMPLEX operator*(double y, PSCOMPLEX x) {return x * precision_type(y);}
inline precision_type max(precision_type x, double y) {return std::max(x, precision_type(y));}
inline precision_type max(double y, precision_type x) {return std::max(x, precision_type(y));}
inline precision_type min(precision_type x, double y) {return std::min(x, precision_type(y));}
inline precision_type min(double y, precision_type x) {return std::min(x, precision_type(y));}
#endif

// in what follows, we define functions that are needed for
// clang's complex to work with with dd_real and qd_real
// and that are not present by default in dd_real and qd_real
#if defined PSDDREAL || defined PSQDREAL
#include <complex>
inline precision_type fmax(precision_type x, precision_type y) {return std::max(x,y);}
inline precision_type fmin(precision_type x, precision_type y) {return std::min(x,y);}
//inline precision_type max(precision_type x, double y) {return std::max(x,precision_type(y));}
//inline precision_type max(double x, precision_type y) {return std::max(precision_type(x),y);}
//inline std::complex<precision_type> operator*(const std::complex<precision_type> &a, double b) {return a*precision_type(b);}
//inline std::complex<precision_type> operator*(double a, const std::complex<precision_type> &b) {return precision_type(a)*b;}
//inline std::complex<precision_type> operator/(const std::complex<precision_type> &a, double b) {return a/precision_type(b);}
inline precision_type scalbn(precision_type x, int n) {return x * exp2(n);}
inline precision_type hypot(precision_type x, precision_type y) {return sqrt(x*x+y*y);}
inline precision_type copysign(precision_type x, precision_type y) {
  return y.is_positive() ? std::abs(x) : -std::abs(x);
}
inline double logb(precision_type x) {
  // we need the exponent, which is contained in the first
  // of the doubles inside the dd and qd_real types 
  // (NB: checked that denormalised numbers don't go 
  // beyond standard double denormalised numbers, so
  // this should be safe even then)
  return std::logb(x.x[0]);
}

// taken from suggestion in https://git.musl-libc.org/cgit/musl/tree/src/math/log1p.c
inline precision_type log1p(precision_type x) {
  precision_type x2 = x*x;
  precision_type x3 = x2*x;
  precision_type x6 = x3*x3;

  if (x6 < std::numeric_limits<precision_type>::epsilon()) {
    precision_type x4 = x3*x;
    precision_type x5 = x4*x;
    // Taylor expansion
    return x - x2/2 + x3/3 - x4/4 + x5/5 - x6/6;
  } else {
    return log(1+x);
  }
}


//#if defined __clang__
//// reimplement division, because clang (11.0.3) complex division uses fancy
//// things (like scalbn, cast from type to int) some of which are not
//// easy to get in dd_real and qd_real
//#include <complex>
//inline std::complex<precision_type> 
//operator/(const std::complex<precision_type> & a, const std::complex<precision_type> & b) {
//  return (a * std::conj(b)) / std::norm(b);
//}
//#endif

namespace std {
  using ::fabs;
  using ::fmax;
  using ::fmin;
  using ::scalbn;
  using ::hypot;
  using ::logb;
  using ::log1p;
}
#endif

#endif  //__EXTRA_MATH_HH__
