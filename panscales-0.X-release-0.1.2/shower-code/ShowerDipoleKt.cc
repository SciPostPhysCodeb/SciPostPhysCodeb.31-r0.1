//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#include "ShowerDipoleKt.hh"

namespace panscales{

  //----------------------------------------------------------------------
  // set the elements for all dipole types
  std::vector<std::unique_ptr<typename ShowerBase::Element> > ShowerDipoleKt::elements(
    Event & event, int dipole_index) const {
    unsigned int i3    = event.dipoles()[dipole_index].index_3()   ;
    unsigned int i3bar = event.dipoles()[dipole_index].index_3bar();
    ShowerDipoleKt::Element *elm1, *elm2;
    if (event.particles()[i3].is_initial_state()){
      if (event.particles()[i3bar].is_initial_state()){
        // II dipole
        elm1 = new ShowerDipoleKt::ElementII(i3, i3bar, dipole_index, &event, this);
        elm2 = new ShowerDipoleKt::ElementII(i3bar, i3, dipole_index, &event, this);
      } else {
        // 3 is I, 3bar is F
        if (_if_is_global) {
          elm1 = new ShowerDipoleKt::ElementIFGlobal(i3, i3bar, dipole_index, &event, this);
        }
        else {
          elm1 = new ShowerDipoleKt::ElementIFLocal(i3, i3bar, dipole_index, &event, this);
        }
        elm2 = new ShowerDipoleKt::ElementFI(i3bar, i3, dipole_index, &event, this);
      }
    } else {
      if (event.particles()[i3bar].is_initial_state()){
        // 3 is F, 3bar is I
        elm1 = new ShowerDipoleKt::ElementFI(i3, i3bar, dipole_index, &event, this);
        if (_if_is_global) {
          elm2 = new ShowerDipoleKt::ElementIFGlobal(i3bar, i3, dipole_index, &event, this);  
        }
        else {
          elm2 = new ShowerDipoleKt::ElementIFLocal(i3bar, i3, dipole_index, &event, this);  
        }
      } else {
        // 3 and 3bar are F
        elm1 = new ShowerDipoleKt::ElementFF(i3, i3bar, dipole_index, &event, this);
        elm2 = new ShowerDipoleKt::ElementFF(i3bar, i3, dipole_index, &event, this);
      }
    }      
    std::unique_ptr<typename ShowerBase::Element> elms[] = {
      std::unique_ptr<typename ShowerBase::Element>(elm1),
      std::unique_ptr<typename ShowerBase::Element>(elm2)
    };
    return {std::make_move_iterator(std::begin(elms)), std::make_move_iterator(std::end(elms))};
  }

  //----------------------------------------------------------------------
  ShowerBase::EmissionInfo* ShowerDipoleKt::create_emission_info() const {return new ShowerDipoleKt::EmissionInfo(); }
  
  //----------------------------------------------------------------------
  double ShowerDipoleKt::Element::lnv_lnb_max_density() const {
    // For now we work in a large-NC limit, with CF defined to be equal
    // to CA/2; the density that is returned here corresponds to that
    // for a quark or for half a gluon
    return _shower->max_alphas() * _shower->qcd().CA() / M_PI;
  }


  //----------------------------------------------------------------------
  // update the element kinematics (called after an emission)
  void ShowerDipoleKt::Element::update_kinematics() {
    if (use_diffs()) {
      _dipole_m2 = 2*dot_product_with_dirdiff(emitter(), spectator(), dipole().dirdiff_3_minus_3bar); 
    } else {
      _dipole_m2 = (emitter() + spectator()).m2();
    }
    if (_dipole_m2 <= 0) {
      std::cerr << "ShowerDipoleKt::Element::update_kinematics: zero mass element with dipole_m2 = " << _dipole_m2 << std::endl;
      std::cerr << "   emitter  : " << emitter()   << std::endl;
      std::cerr << "   spectator: " << spectator() << std::endl;
      std::cerr << "   dipole   : " << emitter()+spectator() << std::endl;
      std::cerr << "   1-cos(th): " << one_minus_costheta(emitter(),spectator()) << std::endl;
      if (use_diffs()) {
        std::cerr << "   dirdiff  : " << dipole().dirdiff_3_minus_3bar << std::endl;
      }
      throw ErrorZeroMassElement();
    }
  }

  // update the element indices and kinematics
  void ShowerDipoleKt::Element::update_indices(unsigned emitter_index, unsigned spectator_index) {
    _emitter_index   = emitter_index;
    _spectator_index = spectator_index;
    update_kinematics();
  }

  //----------------------------------------------------------------------
  // for a given lnv,lnb, returns the pseudorapidity with respect to
  // the closer of the emitter/spectator, defined in the event
  // centre-of-mass frame. This eta will be correct in the
  // soft+collinear limit but may differ from the true eta in the
  // soft large-angle limit and the hard-collinear limit.
  //
  // See tests in 2019-07-approx-lnkt-eta/ and
  // logbook/2019-07-27--approx-lnkt-eta
  double ShowerDipoleKt::Element::eta_approx(double lnv, double lnb) const {
    // work out eta wrt the emitter, keeping in mind that lnb is 0 for a hard
    // collinear emission, so eta there should be given by log(2E/kt) where
    // E is the emitter energy in the event centre-of-mass frame.
    // Decreasing lnb (i.e. going to negative lnb) decreases the rapidity.
    double eta_emitter = -lnv + to_double(log_T(2.0*dot_product(emitter(), _event->Q())/_event->Q().m())) + lnb;
    // similar formula wrt spectator, keeping in mind a map lnb ->
    // log(kappa2) - lnb gives us the symmetric point in lnb (swapping
    // spectator/emitter)
    double eta_spectator = -lnv + to_double(log_T(2.0*dot_product(spectator(), _event->Q())/_event->Q().m()))
                                + (to_double(log_T(kappa2(precision_type(lnv))))-lnb);
    // now decide which of the rapidities is smaller and return that
    // one, signing things such that we're positive if closer to the
    // emitter negative otherwise.
    if (eta_emitter > eta_spectator) return  eta_emitter;
    else                             return -eta_spectator;
  }

  // compensation term related to renormalization scale variations, see Eq. (8.1) in arXiv:2207.09467
  double ShowerDipoleKt::Element::alphas2_coeff(const typename ShowerBase::EmissionInfo * emission_info) const {
    const auto & qcd = _shower->qcd();
    if (qcd.nloops() <= 1) return 0;

    else {
      // 
      return 2 * qcd.b0() * qcd.lnxmuR() * to_double(1.0 - emission_info->z_radiation_wrt_splitter());
      // 
      // // the following lines only works for PanGlobal, because for PanLocal z_radiation_wrt_spectator=0.
      // // They are intended to work around the fact that in the transition region between the two
      // // dipole halves, using the "splitter" can give a small z, when there would actually be a larger
      // // z from the other side.
      // double z = std::max(emission_info->z_radiation_wrt_emitter, emission_info->z_radiation_wrt_spectator);
      // return 2 * qcd.b0() * qcd.lnxmuR() * (1.0 - z);
    }
  }

  double ShowerDipoleKt::Element::lnb_for_min_abseta(double lnv) const {
    // we need to work out the lnb value for which eta_emitter and
    // eta_spectator are equal in the eta_approx function
    return 0.5*(to_double(log_T(2.0*dot_product(spectator(), _event->Q())/_event->Q().m()))
                + to_double(log_T(kappa2(lnv)))
                - to_double(log_T(2.0*dot_product(emitter(), _event->Q())/_event->Q().m())));
  }

  //----------------------------------------------------------------------
  //Eq. (11a) of arXiv:2002.11114
  double ShowerDipoleKt::Element::g(double eta) const {
    if (eta < -1.0){
      return 0.;
    } else if (eta >= -1.0 && eta <= 1.0) {
      return 15./16.*(pow(eta,5)/5. - 2./3.*pow(eta,3) + eta + 8./15.);
    } else {
      return 1.;
    }
  }


  //======================================================================
  // ShowerDipoleKt::ElementII implementation 
  //======================================================================
  //  - for FSR we need a factor z P(z) (w z the emitted
  //    particle long mom fraction)
  //  - for ISR we have 2 terms:
  //     1. a z(1-z) P(z)
  //     2. a [xf(x)][xtilde f(xtilde)]
  //    Note that IF dipoles (or sometimes even FF dipoles when dealing
  //    with boosts) can also have a PDF factor
  Range ShowerDipoleKt::ElementII::lnb_generation_range(double lnv) const{
    // lnb = log((1-z)/z)
    //
    // xtilde/z < 1  => (1-z)/z < (1-xtilde)/xtilde
    //               => lnb < log((1-xtilde)/xtilde) < log(1/xtilde)
    // we take the last inequality to be on the safe side.
    //
    // For the lower bound, we still have (1-z) > kappa^2 [imposing
    // v_j<1 in Eq (A.16) of arXiv:1506.05057]
    //  => (1-z)/z > kappa2/(1-kappa2) > kappa2
    //    
    double lnxtilde = to_double(log_T(event().pdf_x(emitter())));
    return Range(to_double(log_T(kappa2(precision_type(lnv)))), -lnxtilde);
  }
  
  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  // #TRK_ISSUE-289 Question of whether we keep this in separate
  // acceptance_probabilities/do_kinematics for each type of
  // element or factor-out the bits and pieces common to II/IF/FI/FF

  /// return the acceptance probability for the given kinematic point
  bool ShowerDipoleKt::ElementII::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerShowerDipoleKt!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerDipoleKt::EmissionInfo & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;
    // we have lnb = log((1-z)/z)
    // => (1-z)/z = exp(lnb)
    // => 1-z = exp(lnb)/(1+exp(lnb))
    //      z = 1/(1+exp(lnb))
    precision_type explnb = exp(precision_type(lnb));
    // z is zeta in the paper, we have zeta = 1/(1+kappa*exp(eta_dip))
    precision_type z = 1.0/(1+explnb);
    precision_type omz = explnb*z;
    // kappa2 = e^(2 lnv)/m^2
    precision_type k2 = kappa2(precision_type(lnv));

    // cache this info for future usage
    emission_info.omz = omz;
    emission_info.z   = z;
    emission_info.k2  = k2; 
    
    // this is a dipole shower: only the emitter can split
    emission_info.emitter_weight_rad_gluon   = 0.0;
    emission_info.emitter_weight_rad_quark   = 0.0;
    emission_info.spectator_weight_rad_gluon = 0.0;
    emission_info.spectator_weight_rad_quark = 0.0;

    // impose some constraints so that all the mapping coefficients
    // are well-behaved
    //  1. vj < 1   => k2 < omz
    //  2. xjab > 0 => k2 < z*omz  [supersedes #1]
    //  3. 0 < (1-xjab-v)/xjab  => z<1 which is always true
    if (k2 >= omz*z) return false;
    
    // here we should make a decision based on flavour.
    //
    // pg (pq) corresponds to the emission of a gluon (quark)
    _shower->fill_dglap_splitting_weights(emitter(), omz,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);

    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor

    // the spectator PDF fraction is unchanged
    //
    // Watch out: there can be cases where
    //   xtilde/z > 1
    // and the true post-branching x would have been <=1.  We'd
    // therefore be throwing out emissions which we shouldn't have.
    //
    // This is (potentially) problematic in a region
    //   x < 1 < xtilde/z
    //
    // One potential reason to provide the exact x would be to make
    // sure that do_kinematics always returns true.
    //
    // Another option is to do some intermediate step where we have
    //  1. an acceptance_probability
    //  2. a PDF probability with the exact x (if the 1st one passes)
    //  3. do_kinematics
    // This is what we opt for
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = 1.0/z;
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = 1.0/z;
    }
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = omz;
    emission_info.z_radiation_wrt_spectator = 0.;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();

    // we need a g(eta) in the dipole rest frame [in this case we use
    // the pre-branching frame even for ISR]
    //   eta = 1/2 log(omz^2/k2)   [Eq. (3.6) of arXiv:1805.09327]
    //       = log(omz/k) = lnb - (lnv-log(m)) 
    // we've neglected the log(z-kappa^2/(1-z)) in the last equality
    double eta = lnb - lnv + 0.5*to_double(log_T(_dipole_m2));
    normalisation_factor *= g(eta);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;
    return true;
  }
  // carry out the splitting and update the event
  bool ShowerDipoleKt::ElementII::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    typename ShowerDipoleKt::EmissionInfo & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));
    
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    const precision_type & omz = emission_info.omz;
    const precision_type & z   = emission_info.z;
    const precision_type & k2  = emission_info.k2;
    
    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);
    
    // the eqs. after (A.16) in the Direv1 paper (arXiv:1506.05057)
    // when z is close to 1 and vj<<1, xjab ~ 1 and 1-xjab is small in ak. 
    // To help reduce numerical errors we write
    //  ak = (1-xjab-vj)/xjab
    //     = (1-z)/xjab
    //     = omz/xjab
    // where z is zeta in arXiv:2205.02237
    precision_type vj = k2/omz;
    precision_type xjab = z-vj;
    precision_type ak   = omz/xjab;  //< our own notation
    
    // in principle, we have made sure in acceptance_probability that
    // all the coefficients are well-behaved
    assert((vj<=1) && (xjab>=0));

    // update the PDF x fractions
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = 1.0/xjab;
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = 1.0/xjab;
    }

    // the transverse normalisation is given by masslessness condition
    // for (A.16b) from which we get
    //   kt^2 = sajb  k^2(1-z)/(z(1-z)-k2)
    // in practice, we reuse the notations above
    precision_type norm_perp = sqrt(_dipole_m2 * ak * vj);

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);

    // A.16 itself (pi = emitter, pj = radiation, pk = spectator)
    // Note: we use p3 in the operations here to avoid going via special "mass preserving" operations
    // (and mass tests) which bring no benefit in a case where the final mass should be zero.
    Momentum3<precision_type> emitter_out3   = 1/xjab * rp.emitter.p3();
    Momentum3<precision_type> radiation3     = ak     * rp.emitter.p3() + vj * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> spectator_out3 = rp.spectator.p3();

    // building Qout with rp to be used for the boost later
    // Qout = emitter + spectator - radiated
    //      = (1/x - ak)*em + (1-vj)*spec + kT
    //      = z/xjab*em +(omz+xjab)*spec + kT 
    Momentum3<precision_type> Qout_3 = z/xjab * rp.emitter.p3() + (omz+xjab) * rp.spectator.p3() + perp.p3();
    // mass of Qout^2 = (emitter()+spectator()).m2() = _dipole_m2
    
    // building Qout - Qin, necessary for the boost
    // Qout   = z/xjab*em +(omz+xjab)*spec + kT 
    // Qin = em + spec
    // Qout - Qin = ((z)/xjab-1)*em +(omz+xjab-1)*spec + kT
    // Qout - Qin = ((z-xjab)/xjab)*em +(xjab-z)*spec + kT
    // Qout - Qin = ((z-(z-vj))/xjab)*em +(-vj)*spec + kT
    // Qout - Qin = (vj/xjab)*em +(-vj)*spec + kT
    Momentum Qout_minus_Qin = vj/xjab * rp.emitter - vj * rp.spectator + perp;
    emission_info.Qout_minus_Qin = rp.rotn_from_z*Qout_minus_Qin; 
    // We need the dot product (Qout - Qin).Qout for the boost as well
    // (Qout - Qin). Qout = (pa+pb-pk - patilde -pbtilde).(pa+pb-pk)
    //  ((vj/xjab)*em +(-vj)*spec + kT). (z/xjab*em +(omz+xjab)*spec + kT)
    //  (((z/xjab)*(-vj) + vj/xjab * (1 - z +xjab))/2 - omz/xjab*vj)*_dipole_m2
    //  ((vj/xjab * omz - vj/xjab * z + vj / xjab * (z - vj)))/2  - omz/xjab*vj)*_dipole_m2
    //  ((- vj/xjab * omz  - vj^2 / xjab))/2 )*_dipole_m2
    //  (- k2  - vj^2) *_dipole_m2/2/xjab
    emission_info.Qout_minus_Qin_dot_Qout = -(emission_info.k2 + pow2(vj)) *_dipole_m2/2/xjab; 

    // The sum Qsum = Qout + Qin is needed in the boost
    // Qsum = (1 + 1/x - ak)*em + (2-vj)*spec + kT
    //      = 1+z/xjab*em +(omz+xjab)*spec + kT 
    Momentum3<precision_type> Qsum_3 = ((2*z-vj)/xjab) * rp.emitter.p3() + (1+omz+xjab) * rp.spectator.p3() + perp.p3();
    // Qsum^2 = ((2 + vj/x)*(2-vj) - 2*ak*vj)*Q2
    //        = (4+vj/xjab-vj)*Q2
    emission_info.Qsum2              = (4*z - 3*vj - vj*xjab)/xjab * _dipole_m2;

    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
    // and the 4-momentum of Qout, rotated back to the z frame
    emission_info.Qout          = Momentum::fromP3M2(rp.rotn_from_z*Qout_3, _dipole_m2);
    emission_info.Qsum          = Momentum::fromP3M2(rp.rotn_from_z*Qsum_3, emission_info.Qsum2);
    
    return true;
  }

  // do the final boost
  void ShowerDipoleKt::ElementII::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {
    // now that the new emission has been inserted, boost
    // the event according to Eq.(A17) of arXiv:1506.05057

    // We need to compute this here because the next line updates the
    // particle momenta
    
    // Retrieve ShowerDipoleKt version of emission_info
    const typename ShowerDipoleKt::EmissionInfo & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));
    
    Momentum Qin  = emitter() + spectator();
    
    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
    

    LorentzBoostII<MomentumM2<precision_type>> lboost(emission_info, _dipole_m2, Qin);

    if(use_diffs()){
      std::vector<MomentumM2<precision_type>> pre_boost_momenta(_event->size());
      for(unsigned i = 0; i < _event->size()-1; ++i) {
        Particle & p = (*_event)[i];
        pre_boost_momenta[i] = (*_event)[i];
        if (p.is_initial_state()) continue;
        precision_type p_m2 = p.m2();
        Momentum p_new = lboost*p;
        p_new.register_true_m2(p_m2);
        p.reset_momentum(p_new);
      }

      // then handle the dipole differences
      for (auto & dipole: _event->dipoles()) {

        unsigned int i3bar = dipole.index_3bar();
        unsigned int i3    = dipole.index_3();

        // the boost acts on the final state particles except the one just created
        // so not on the initial state particles!
        // we check whether the 3 or 3bar end of the dipole needs boosting
        bool i3_unboosted    = (*_event)[i3]   .is_initial_state() || (i3    == _event->size()-1);
        bool i3bar_unboosted = (*_event)[i3bar].is_initial_state() || (i3bar == _event->size()-1);

        if (i3_unboosted && i3bar_unboosted) continue;
        else if (i3_unboosted){
          // here we are in a situation where the 3bar end of the dipole is boosted
          precision_type E3bar  = pre_boost_momenta[i3bar].E();
          precision_type EB3bar = (*_event)[i3bar].E();
          
          // The boost could be defined as 
          // delta_d3bar = lboost.lambda(pre_boost_momenta[i3bar])/E3bar + (1/E3bar - 1/EB3bar)*(*_event)[i3bar];
          // This is too unstable numerically. However we may realise that
          // (1/E3bar - 1/EB3bar) = (EB3bar-E3bar) /E3bar EB3bar = - lboost.lambda(pre_boost_momenta[i3bar]).E()/E3bar EB3bar
          // similar rewrite is performed below when i3bar is unboosted
          Momentum lambda_boost_i3bar = lboost.lambda(pre_boost_momenta[i3bar]);
          Momentum delta_d3bar = lambda_boost_i3bar/E3bar - (lambda_boost_i3bar.E())/(E3bar*EB3bar)*(*_event)[i3bar];
          
          dipole.dirdiff_3_minus_3bar = dipole.dirdiff_3_minus_3bar + delta_d3bar;
        } else if (i3bar_unboosted){
          // here we are in a situation where the 3 end of the dipole is boosted
          precision_type E3  = pre_boost_momenta[i3].E();
          precision_type EB3 = (*_event)[i3].E();
          Momentum lambda_boost_i3 = lboost.lambda(pre_boost_momenta[i3]);
          Momentum delta_d3 = lambda_boost_i3/E3 - (lambda_boost_i3.E())/(E3*EB3)*(*_event)[i3];
          
          dipole.dirdiff_3_minus_3bar = dipole.dirdiff_3_minus_3bar - delta_d3;
        } else{
          //both ends need to be boosted
          dipole.dirdiff_3_minus_3bar = apply_linear_transform_to_dirdiff(lboost, pre_boost_momenta[i3bar].E(), pre_boost_momenta[i3].E(), (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);      
        }
      }
      // then handle the dipole differences for non-splitting dipoles as well
      for (auto & dipole: _event->non_splitting_dipoles()) {
        unsigned int i3bar = dipole.index_3bar();
        unsigned int i3    = dipole.index_3();
        // the boost acts on the final state particles except the one just created
        // so not on the initial state particles!
        // we check whether the 3 or 3bar end of the dipole needs boosting
        bool i3_unboosted    = (*_event)[i3]   .is_initial_state() || (i3    == _event->size()-1);
        bool i3bar_unboosted = (*_event)[i3bar].is_initial_state() || (i3bar == _event->size()-1);
        if (i3_unboosted && i3bar_unboosted) continue; // if both are true we do not need to do anything
        else if (i3_unboosted){
          // here we are in a situation where the 3bar end of the dipole is boosted
          precision_type E3bar  = pre_boost_momenta[i3bar].E();
          precision_type EB3bar = (*_event)[i3bar].E();
          Momentum lambda_boost_i3bar = lboost.lambda(pre_boost_momenta[i3bar]);
          Momentum delta_d3bar = lambda_boost_i3bar/E3bar - (lambda_boost_i3bar.E())/(E3bar*EB3bar)*(*_event)[i3bar];
          dipole.dirdiff_3_minus_3bar = dipole.dirdiff_3_minus_3bar + delta_d3bar;
        } else if (i3bar_unboosted){
          // here we are in a situation where the 3 end of the dipole is boosted
          precision_type E3  = pre_boost_momenta[i3].E();
          precision_type EB3 = (*_event)[i3].E();
          Momentum lambda_boost_i3 = lboost.lambda(pre_boost_momenta[i3]);
          Momentum delta_d3 = lambda_boost_i3/E3 - (lambda_boost_i3.E())/(E3*EB3)*(*_event)[i3];
          dipole.dirdiff_3_minus_3bar = dipole.dirdiff_3_minus_3bar - delta_d3;
        } else{
          dipole.dirdiff_3_minus_3bar = apply_linear_transform_to_dirdiff(lboost, pre_boost_momenta[i3bar].E(), pre_boost_momenta[i3].E(), (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);      
        }
      }
    } else{
      // loop over all final-state particles, except the radiated one
      // (the radiated one is conventionally expected always to be the
      // last one in the event particle array)
      for(unsigned i = 0; i < _event->size()-1; ++i) {
        Particle & p = (*_event)[i];
        if (p.is_initial_state()) continue;
        precision_type p_m2 = p.m2();

        Momentum p_new = lboost*p;
        
        p_new.register_true_m2(p_m2);

        p.reset_momentum(p_new);
      }
    }

  }

  //======================================================================
  // ShowerDipoleKt::ElementIF
  //
  // Initial-state splitter, final-state spectator. We implement from 
  // arXiv:1506.05057 both the local (appendix A.3.) and the global
  // recoil strategies (appendix A.4.)
  //
  //======================================================================
  Range ShowerDipoleKt::ElementIF::lnb_generation_range(double lnv) const{
    // lnb = log((1-z)/z)
    //
    // xtilde/z < 1  => (1-z)/z < (1-xtilde)/xtilde
    //               => lnb < log((1-xtilde)/xtilde) < log(1/xtilde)
    // we take the last inequality to be on the safe side.
    //
    // For the lower bound, we still have (1-z) > kappa^2 [imposing
    // u_j<1 in Eq (A.12) of arXiv:1506.05057]
    //  => (1-z)/z > kappa2/(1-kappa2) > kappa2
    double lnxtilde = to_double(log_T(event().pdf_x(emitter())));
    return Range(to_double(log_T(kappa2(precision_type(lnv)))), -lnxtilde);
  }
  
  //--------------------------------------------------
  // important definitions
  //--------------------------------------------------
  /// return the acceptance probability for the given kinematic point
  bool ShowerDipoleKt::ElementIF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerShowerDipoleKt!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerDipoleKt::EmissionInfo & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;
  
    // we have lnb = log((1-z)/z)
    // => 1-z = exp(lnb)/(1+exp(lnb))
    //      z = 1/(1+exp(lnb))
    precision_type explnb = exp(precision_type(lnb));
    precision_type z = 1.0/(1+explnb);
    precision_type omz = explnb*z;
    // kappa2 = e^(2 lnv)/m^2
    precision_type k2 = kappa2(precision_type(lnv));
  
    // cache this info for future usage
    emission_info.omz = omz;
    emission_info.z   = z;
    emission_info.k2  = k2;
    
    // this is a dipole shower: only the emitter can split
    emission_info.emitter_weight_rad_gluon   = 0.0;
    emission_info.emitter_weight_rad_quark   = 0.0;
    emission_info.spectator_weight_rad_gluon = 0.0;
    emission_info.spectator_weight_rad_quark = 0.0;
  
    // impose some constraints so that all the mapping coefficients
    // are well-behaved
    //  1. uj < 1   => k2 < omz
    //  2. xjka - uj > 0 => k2 < z*omz  [stronger than #1]
    //  3. xjka < 1 => z < 1 (always true)
    if (k2 >= omz*z) return false;
    
    // here we should make a decision based on flavour
    // pg (pq) corresponds to the emission of a gluon (quark)
    _shower->fill_dglap_splitting_weights(emitter(), omz,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);
  
    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = 1.0/z; //< note xjka=z
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = 1.0/z; //< note xjka=z
    }
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = omz;
    emission_info.z_radiation_wrt_spectator = 0.;
  
    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
  
    // we need a g(eta) in the dipole rest frame [in this case we use
    // the pre-branching frame even for ISR]
    //   eta = 1/2 log(omz^2/k2)   [Eq. (3.6) of arXiv:1805.09327]
    //       = log(omz/k) = lnb - (lnv-log(m))
    // we've neglected the log(z) in the last equality
    double eta = lnb - lnv + 0.5*to_double(log_T(_dipole_m2));
    normalisation_factor *= g(eta);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;
    
    return true;
  }


  // implementation of do_kinematics for the local case
  // carry out the splitting and update the event
  bool ShowerDipoleKt::ElementIFLocal::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    typename ShowerDipoleKt::EmissionInfo & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));
    
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    const precision_type & omz = emission_info.omz;
    const precision_type & z   = emission_info.z;
    const precision_type & k2  = emission_info.k2;
    
    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);
    // the eqs. after (A.8) in the Direv1 paper (arXiv:1506.05057)
    precision_type uj   = k2/omz;
    // in principle, we have made sure in acceptance_probability that
    // all the coefficients are well-behaved

    // xjka = z
    precision_type ai = 1 / z;
    // ak = (omz-k2 ) /z
    precision_type ak = (omz - k2) / z;
    precision_type bk = uj;

    // aj = uj * omz / z = k2 / z
    precision_type aj = k2 / z;
    precision_type bj = 1 - bk;

    // the transverse normalisation is given by masslessness condition
    // for (A.8, 2nd line)
    precision_type norm_perp = sqrt(_dipole_m2 * ak * bk);
  
    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
  
    // A.8 itself (pi = emitter, pj = radiation, pk = spectator)
    // Note: we use p3 in the operations here to avoid going via special "mass preserving" operations
    // (and mass tests) which bring no benefit in a case where the final mass should be zero.
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + bk * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> spectator_out3 = aj * rp.emitter.p3() + bj * rp.spectator.p3() + perp.p3();
    
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    // update the PDF x fractions
    if (emitter().initial_state()==1){ // emitter is beam1
      emission_info.beam1_pdf_new_x_over_old = 1 / z;
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // emitter is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = 1 / z;
    }
    return true;
  }


  void ShowerDipoleKt::ElementIFLocal::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {

    // add all the new particles to the event
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
  }

  
  /// carry out the splitting and update the event
  bool ShowerDipoleKt::ElementIFGlobal::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    typename ShowerDipoleKt::EmissionInfo & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));
    
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    const precision_type & omz = emission_info.omz;
    const precision_type & z   = emission_info.z;
    const precision_type & k2  = emission_info.k2;
    
    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);
    // the eqs. after (A.12) in the Direv1 paper (arXiv:1506.05057)
    precision_type uj   = k2/omz;
    // in principle, we have made sure in acceptance_probability that
    // all the coefficients are well-behaved
    assert((uj<=1) && (z>=uj));

    precision_type ai = (1-uj)/(z-uj);
    precision_type ak = omz/(z-uj);
    precision_type ombj = uj/z;
  
    // the transverse normalisation is given by masslessness condition
    // for (A.12, 2nd line)
    precision_type norm_perp = sqrt(_dipole_m2 * ai * ak * ombj);
  
    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
  
    // A.16 itself (pi = emitter, pj = radiation, pk = spectator)
    // Note: we use p3 in the operations here to avoid going via special "mass preserving" operations
    // (and mass tests) which bring no benefit in a case where the final mass should be zero.
    Momentum3<precision_type> emitter_out3   = ai * rp.emitter.p3() + ombj * ak * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> radiation3     = ak * rp.emitter.p3() + ombj * ai * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> spectator_out3 = (1-ombj) * rp.spectator.p3();
    
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    // update the PDF x fractions
    //
    // If pB is the unaffected incoming hadron, this is given by
    //   emitter_out.pB/emitter_in.pB
    //
    // first figure out pB
    // then compute the fraction
    if (emitter().initial_state()==1){ // emitter is beam1
      const Momentum & pB = rp.rotn_from_z.transpose()*_event->beam2();
      emission_info.beam1_pdf_new_x_over_old = dot_product(pB, emission_info.emitter_out)
                                             / dot_product(pB, rp.emitter);
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // emitter is beam2
      const Momentum & pB = rp.rotn_from_z.transpose()*_event->beam1();
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = dot_product(pB, emission_info.emitter_out)
                                             / dot_product(pB, rp.emitter);
    }

    return true;
  }
  
  /// do the final boost
  void ShowerDipoleKt::ElementIFGlobal::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {

    // Retrieve ShowerDipoleKt version of emission_info
    const auto & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));
    
    // Recall that in an IF dipole, it is I that is the emitter
    // (elsewhere we will have an FI element, where F is the emitter)
    const Momentum pA = emitter(); // pre-branching emitter (make sure to take a copy)
    const Momentum pB = _event->opposite_beam(emitter()).momentum();

    // Call update event of base shower class
    ShowerBase::Element::update_event(transition_runner, emission_info_base);

    // now that the new emission has been inserted, boost the event
    // according to the description in point 3 above Eq.(A12) of
    // arXiv:1506.05057
    const Momentum pa = emitter(); // post-branching emitter
    
    LorentzBoostIF<MomentumM2<precision_type>> lboost(pa,pA,pB);

    if(use_diffs()){
      // we need to do the boost but also store the energies of the particles before they enter the boost
      // first set up the boost class
      // then handle the boost, making sure we store the energies
      std::vector<precision_type> Ei(_event->size());
      for(unsigned i = 0; i < _event->size(); ++i) {
        Particle & p = (*_event)[i];
        Ei[i] = p.E();

        // enforce the exact kinematics for incoming particles
        if (p.is_initial_state()){
          unsigned int beam_index = emitter().initial_state();
          if (p.initial_state()==beam_index){
            if (beam_index==1){
              (*_event)[i].reset_momentum(emission_info.beam1_pdf_new_x_over_old * pA);
            } else {
              (*_event)[i].reset_momentum(emission_info.beam2_pdf_new_x_over_old * pA);
            }
          } // the other incoming parton is left unchanged
        } else { // final-state particle
          precision_type p_m2 = p.m2();
          Momentum p_new = lboost*p;
          p_new.register_true_m2(p_m2);
          p.reset_momentum(p_new);
        }
      }

       // handle the dipole differences
      for (auto & dipole: _event->dipoles()) {
        unsigned int i3bar = dipole.index_3bar();
        unsigned int i3    = dipole.index_3();
        dipole.dirdiff_3_minus_3bar = apply_linear_transform_to_dirdiff(lboost, Ei[i3bar], Ei[i3], (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);      
        // handle II exactly 
        if((*_event)[dipole.index_3()].is_initial_state() && (*_event)[dipole.index_3bar()].is_initial_state())  {
          dipole.dirdiff_3_minus_3bar  = Momentum3<precision_type>(0,0,dipole.dirdiff_3_minus_3bar.pz());
        }
      }
      // handle the dipole differences for non-splitting dipoles
      for (auto & dipole: _event->non_splitting_dipoles()) {
        unsigned int i3bar = dipole.index_3bar();
        unsigned int i3    = dipole.index_3();
        dipole.dirdiff_3_minus_3bar = apply_linear_transform_to_dirdiff(lboost, Ei[i3bar], Ei[i3], (*_event)[i3bar], (*_event)[i3], dipole.dirdiff_3_minus_3bar);      
        // enforce exact kinematics for II dipoles
        if((*_event)[dipole.index_3()].is_initial_state() && (*_event)[dipole.index_3bar()].is_initial_state())  {
          dipole.dirdiff_3_minus_3bar  = Momentum3<precision_type>(0,0,dipole.dirdiff_3_minus_3bar.pz());
        }
      }
    }
    else{
      for(unsigned i = 0; i < _event->size(); ++i) {
        // enforce the exact kinematics for incoming particles
        Particle & p = (*_event)[i];
        if (p.is_initial_state()){
          unsigned int beam_index = emitter().initial_state();
          if (p.initial_state()==beam_index){
            if (beam_index==1){
              (*_event)[i].reset_momentum(emission_info.beam1_pdf_new_x_over_old * pA);
            } else {
              (*_event)[i].reset_momentum(emission_info.beam2_pdf_new_x_over_old * pA);
            }
          } // the other incoming parton is left unchanged
        } else { // final-state particle
          precision_type p_m2 = p.m2();

          Momentum p_new = lboost*p;

          p_new.register_true_m2(p_m2);
          p.reset_momentum(p_new);
        }
      }
    }
  }

  
  //======================================================================
  // ShowerDipoleKt::ElementFI implementation
  //
  // final-state splitter, initial-state spectator. We take the global
  // recoil strategy from arXiv:1506.05057, appendix A.2.
  //======================================================================
  Range ShowerDipoleKt::ElementFI::lnb_generation_range(double lnv) const{
    // lnb = log(1-z)
    //
    // z > 0 => an upper bound of 0
    //
    // For the lower bound, we still have (1-z) > kappa^2 [imposing
    // xija>0 in Eq (A.6)]
    return Range(to_double(log_T(kappa2(precision_type(lnv)))), 0.0);
  }
  
  /// return the acceptance probability for the given kinematic point
  bool ShowerDipoleKt::ElementFI::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerShowerDipoleKt!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerDipoleKt::EmissionInfo & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;
  
    // we have lnb = log(1-z)
    // omz = z in the pp paper
    precision_type omz = exp(precision_type(lnb));
    precision_type z = 1.0 - omz;
    // kappa2 = e^(2 lnv)/m^2
    precision_type k2 = kappa2(precision_type(lnv));
  
    // cache this info for future usage
    emission_info.omz = omz;
    emission_info.z   = z;
    emission_info.k2  = k2;
    
    // this is a dipole shower: only the emitter can split
    emission_info.emitter_weight_rad_gluon   = 0.0;
    emission_info.emitter_weight_rad_quark   = 0.0;
    emission_info.spectator_weight_rad_gluon = 0.0;
    emission_info.spectator_weight_rad_quark = 0.0;
  
    // impose some constraints so that all the mapping coefficients
    // are well-behaved
    //   0 < xija < 1
    // The <1 part is automatically satisfied
    // The >0 part means k2<=1-z
    if (k2 >= omz) return false;
    
    // here we should make a decision based on flavour.
    //
    // pg (pq) corresponds to the emission of a gluon (quark)
    _shower->fill_dglap_splitting_weights(emitter(), omz,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);
  
    // fill in the ratio of PDF x's that should go in the PDF part of
    // the acceptance factor
    emission_info.beam1_pdf_new_x_over_old = 1.0;
    emission_info.beam2_pdf_new_x_over_old = 1.0;
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = omz;
    emission_info.z_radiation_wrt_spectator = 0.;
  
    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();
  
    // we need a g(eta) in the dipole rest frame 
    //   eta = 1/2 log(omz^2/k2)   [Eq. (3.6) of arXiv:1805.09327]
    //       = log(omz/k) = lnb - (lnv-log(m))
    double eta = lnb - lnv + 0.5*to_double(log_T(_dipole_m2));
    normalisation_factor *= g(eta);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;

    return true;
  }
  
  // carry out the splitting and update the event
  bool ShowerDipoleKt::ElementFI::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
    typename ShowerDipoleKt::EmissionInfo & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));
    
    // retrieve cached info
    const double & phi = emission_info.phi;
    
    const precision_type & omz = emission_info.omz;
    const precision_type & z   = emission_info.z;
    const precision_type & k2  = emission_info.k2;
    
    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);
    
    // the eqs. after (A.6) in the Direv1 paper (arXiv:1506.05057)
    // omz = z in the pp paper
    precision_type omxija  = k2/omz;
    precision_type xija  = 1-omxija;
    precision_type y = omxija/xija;
  
    // the transverse normalisation is given by masslessness condition
    // for (A.162b)
    precision_type norm_perp = sqrt(_dipole_m2 * z * omz * y);
  
    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);
  
    // A.16 itself (pi = emitter, pj = radiation, pk = spectator)
    // Note: we use p3 in the operations here to avoid going via special "mass preserving" operations
    // (and mass tests) which bring no benefit in a case where the final mass should be zero.
    Momentum3<precision_type> emitter_out3   = z   * rp.emitter.p3() + omz * y * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> radiation3     = omz * rp.emitter.p3() +   z * y * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> spectator_out3 = 1.0/xija * rp.spectator.p3();
    
    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);
 
    // fill in the exact PDF rescaling
    // now the rescaling is simply given by bj as there is no boost to perform

    if (spectator().initial_state()==1){ // spectator is beam1
      emission_info.beam1_pdf_new_x_over_old = 1.0/xija;   
      emission_info.beam2_pdf_new_x_over_old = 1.0;
    } else {                           // spectator is beam2
      emission_info.beam1_pdf_new_x_over_old = 1.0;
      emission_info.beam2_pdf_new_x_over_old = 1.0/xija; 
    }
  
    return true;
  }


  void ShowerDipoleKt::ElementFI::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {

    // add all the new particles to the event
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
  }

  //======================================================================
  // ShowerDipoleKt::ElementFF implementation
  //======================================================================
  Range ShowerDipoleKt::ElementFF::lnb_generation_range(double lnv) const {
    // return something that corresponds to log(kappa^2) < ln(1-z) < 0
    // or equivalently 0 < z < 1-kappa^2
    return Range(to_double(log_T(kappa2(precision_type(lnv)))), 0.0);
  }

  Range ShowerDipoleKt::ElementFF::lnb_exact_range(double lnv) const {
    // #TRK_ISSUE-305a shouldn't this have an assert/exception thrown?
    std::cerr << "Error: We do not expect lnb_exact_range to be called " << std::endl;
    double k2 = to_double(kappa2(precision_type(lnv)));
    double rt = k2 < 0.25 ? sqrt(0.25 - k2) : 0;
    return Range(log(0.5-rt), log(0.5+rt));
  }

  // return the acceptance probability for the given kinematic point
  bool ShowerDipoleKt::ElementFF::acceptance_probability(typename ShowerBase::EmissionInfo * emission_info_base) const{
    // WATCH OUT: the following line has an undefined behaviour if the
    // Emission info is mistakenly not that of ShowerShowerDipoleKt!
    // (NB: we tried a dynamic cast, but this had a small speed penalty)
    typename ShowerDipoleKt::EmissionInfo & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));
    
    const double & lnv = emission_info.lnv;
    const double & lnb = emission_info.lnb;
 
    // We will often be working with 1-z close to 0, because (1-z) is
    // the gluon momentum fraction.  To retain precision, we explicitly
    // introduce a 1-z variable called omz, and use that wherever
    // we would otherwise call 1-z
    precision_type omz = exp(precision_type(lnb));
    precision_type z = 1.0 - omz;
    // kappa2 = e^(2 lnv)/m^2
    precision_type k2 = kappa2(precision_type(lnv));
  
    // cache this info for future usage
    emission_info.omz = omz;
    emission_info.z   = z;
    emission_info.k2  = k2;

    // this is a dipole shower: only the emitter can split
    emission_info.emitter_weight_rad_gluon   = 0.0;
    emission_info.emitter_weight_rad_quark   = 0.0;
    emission_info.spectator_weight_rad_gluon = 0.0;
    emission_info.spectator_weight_rad_quark = 0.0;
    // avoid nans later
    precision_type omz_minus_k2 = omz-k2;
    if (omz_minus_k2 <= 0) return false;

    // Make sure that the kt2 generated by the kinematic map is +ve
    precision_type kt2 = (omz / omz_minus_k2) * ((z*omz - k2)/omz_minus_k2) * exp(precision_type(2.0*lnv));
    if (kt2 < 0 || kt2 != kt2) {return false;}

    // here we should make a decision based on flavour.
    // pg (pq) corresponds to the emission of a gluon (quark)
    _shower->fill_dglap_splitting_weights(emitter(), omz,
                                          emission_info.emitter_weight_rad_gluon,
                                          emission_info.emitter_weight_rad_quark);
      
    // Store collinear momentum fractions in emission info for spin correlations
    emission_info.z_radiation_wrt_emitter = omz;
    emission_info.z_radiation_wrt_spectator = 0.;

    // Note about normalisation: colour factors are treated elsewhere
    // so that this code can assume the large-Nc limit and use CA as
    // an overall factor. This gives a factor alphas CA/pi
    // we also normalise by the max density to get the acceptance
    double normalisation_factor = _shower->qcd().CA() * _shower->max_alphas()/M_PI;
    normalisation_factor /= lnv_lnb_max_density();

    // we need a g(eta) in the dipole rest frame
    //   eta = 1/2 log(omz^2/k2)   [Eq. (3.6) of arXiv:1805.09327]
    //       = log(omz/k) = lnb - (lnv-log(m))
    double eta = lnb - lnv + 0.5*to_double(log_T(_dipole_m2));
    normalisation_factor *= g(eta);
    
    emission_info.emitter_weight_rad_gluon *= normalisation_factor;
    emission_info.emitter_weight_rad_quark *= normalisation_factor;

    // update the PDF x fractions
    emission_info.beam1_pdf_new_x_over_old = 1.0;
    emission_info.beam2_pdf_new_x_over_old = 1.0;
  
    return true;
  }
  
  /// carry out the splitting and update the event
  bool ShowerDipoleKt::ElementFF::do_kinematics(typename ShowerBase::EmissionInfo * emission_info_base, const RotatedPieces & rp) const{
        typename ShowerDipoleKt::EmissionInfo & emission_info = *(static_cast<typename ShowerDipoleKt::EmissionInfo*>(emission_info_base));

    // retrieve cached info
    const double & phi = emission_info.phi;
    const precision_type & omz = emission_info.omz;
    const precision_type & z   = emission_info.z;
    const precision_type & k2  = emission_info.k2;

    // this is a dipole shower: only the emitter can split
    assert(emission_info.do_split_emitter);

    // the eqs. after (A.4) in the DipoleKt paper
    precision_type yijk   = k2/omz;

    // this is the formula we are looking for: ztilde = (z - yijk)/(1 - yijk);
    // but we want something that will retain precision when 1-z is small
    precision_type ztilde = (k2 - z*omz)/(k2-omz);
    precision_type omztilde = pow2(omz)/(omz-k2);

    // the transverse normalisation is given by masslessness condition
    // for (A.4a)
    precision_type norm_perp = sqrt(_dipole_m2 * ztilde * omztilde * yijk);

    // for cases with a plane, phi=0 means in-plane
    Momentum perp = norm_perp*(sin(precision_type(phi))*rp.perp1 + cos(precision_type(phi))*rp.perp2);

    // A.4 itself
    // pi = emitter
    // pj = radiation
    // pk = spectator
    // Note: we use p3 in the operations here to avoid going via special "mass preserving" operations
    // (and mass tests) which bring no benefit in a case where the final mass should be zero.
    Momentum3<precision_type> emitter_out3   = ztilde     * rp.emitter.p3() + (yijk * omztilde) * rp.spectator.p3() + perp.p3();
    Momentum3<precision_type> radiation3     = omztilde   * rp.emitter.p3() + (yijk * ztilde  ) * rp.spectator.p3() - perp.p3();
    Momentum3<precision_type> spectator_out3 = (1 - yijk) * rp.spectator.p3();

    // then construct the final 4-momenta, making use of the known masslessness condition
    emission_info.emitter_out   = Momentum::fromP3M2(emitter_out3  , 0.0);
    emission_info.radiation     = Momentum::fromP3M2(radiation3    , 0.0);
    emission_info.spectator_out = Momentum::fromP3M2(spectator_out3, 0.0);

    // update the PDF x fractions
    emission_info.beam1_pdf_new_x_over_old = 1.0;
    emission_info.beam2_pdf_new_x_over_old = 1.0;
  
    return true;
  }

  // update the event with chain information

  void ShowerDipoleKt::ElementFF::update_event(const ColourTransitionRunnerBase *transition_runner,
                                               typename ShowerBase::EmissionInfo * emission_info_base) {

    // add all the new particles to the event
    ShowerBase::Element::update_event(transition_runner, emission_info_base);
  }

  //-------------------------------------------------------------------
  
} // namespace panscales
