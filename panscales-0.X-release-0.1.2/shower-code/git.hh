
// - adapted from https://github.com/andrew-hardin/cmake-git-version-tracking
// - released under the MIT License

// PanScalesNOHEADER

#pragma once
#include <string>
#include "config.hh"

#ifdef GIT_WATCHER

class GitMetadata {
public:
  // Is the metadata populated? We may not have metadata if
  // there wasn't a .git directory (e.g. downloaded source
  // code without revision history).
  static bool Populated();

  // Were there any uncommitted changes that won't be reflected
  // in the CommitID?
  static bool AnyUncommittedChanges();

  // the list of any modified files
  static std::string StatusUNO();

  // The commit author's name.
  static std::string AuthorName();
  // The commit author's email.
  static std::string AuthorEmail();
  // The commit SHA1.
  static std::string CommitSHA1();
  // The ISO8601 commit date.
  static std::string CommitDate();
  // The commit subject.
  static std::string CommitSubject();
  // The commit body.
  static std::string CommitBody();
  // The commit describe.
  static std::string Describe();
};
#else // GIT_WATCHER undefined
class GitMetadata {
public:
  // Is the metadata populated? We may not have metadata if
  // there wasn't a .git directory (e.g. downloaded source
  // code without revision history).
  static bool Populated(){ return false;}

  // Were there any uncommitted changes that won't be reflected
  // in the CommitID?
  static bool AnyUncommittedChanges(){ return false;}

  // the list of any modified files
  static std::string StatusUNO() { return "Unknown (no git watcher)";}

  // The commit author's name.
  static std::string AuthorName(){ return "Unknown (no git watcher)";}
  // The commit author's email.
  static std::string AuthorEmail(){ return "Unknown (no git watcher)";}
  // The commit SHA1.
  static std::string CommitSHA1(){ return "Unknown (no git watcher)";}
  // The ISO8601 commit date.
  static std::string CommitDate(){ return "Unknown (no git watcher)";}
  // The commit subject.
  static std::string CommitSubject(){ return "Unknown (no git watcher)";}
  // The commit body.
  static std::string CommitBody(){ return "Unknown (no git watcher)";}
  // The commit describe.
  static std::string Describe(){ return "Unknown (no git watcher)";}
};

#endif // GIT_WATCHER
