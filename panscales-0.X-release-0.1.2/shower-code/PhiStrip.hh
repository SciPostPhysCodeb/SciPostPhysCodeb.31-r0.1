//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __PHISTRIP_HH__
#define __PHISTRIP_HH__

#include "Matrix3FJ.hh"
#include "EECambridgeOneMinusCosTheta.hh"
#include "EECambridgeFast.hh"
#include "Observables.hh"
//----------------------------------------------------------------------
/// @ingroup observable_classes
/// \class PhiStrip 
/// class that calculates energy flow in a φ-slice

class PhiStrip : public Observable {
public:
  // Constructor.
  PhiStrip(double phi, int select_in = 1) : Observable(select_in), _phi(phi),
    _jet_def(fjcore::JetDefinition(new fjcore::EECambridgeFastPlugin(1.0))) {
    _jet_def.delete_plugin_when_unused();
  }

  // analyse event
  virtual bool analyse(const panscales::Event& event) {
    std::vector<fjcore::PseudoJet> particles;
    for(unsigned int i=0; i<event.size(); ++i) {
      particles.push_back(fjcore::PseudoJet(event[i].px(),event[i].py(),
					    event[i].pz(),event[i].E()));
    }
    // run exclusive clustering
    fjcore::ClusterSequence cs(particles, _jet_def);
    std::vector<fjcore::PseudoJet> exclusive_jets = cs.exclusive_jets(2);
    if (exclusive_jets.size() != 2) return false;

    // order the two jets according to momentum along z axis
    fjcore::PseudoJet jet1 = exclusive_jets[0];
    fjcore::PseudoJet jet2 = exclusive_jets[1];
    if (jet1.pz() < jet2.pz()) std::swap(jet1,jet2);

    // define the event axis as jet1-jet2
    fjcore::PseudoJet d_ev = jet1 - jet2;
    fjcore::Matrix3 rotmat;
    fjcore::PseudoJet nx(1,0,0,0), ny(0,1,0,0);
    rotmat = rotmat.from_direction(d_ev);
    fjcore::PseudoJet rx = rotmat * nx;
    fjcore::PseudoJet ry = rotmat * ny;

    _kt_in_strip = 0.;
    for(auto jet : exclusive_jets) {
      fjcore::PseudoJet subjet, j1, j2;
      subjet = jet;
      while (subjet.has_parents(j1, j2)) {
	if (j1.modp2() < j2.modp2()) std::swap(j1,j2);
	fjcore::PseudoJet u1 = j1/j1.modp(), u2 = j2/j2.modp();
	fjcore::PseudoJet du = u1 - u2;
	precision_type x = du.px() * rx.px() + du.py() * rx.py() + du.pz() * rx.pz();
	precision_type y = du.px() * ry.px() + du.py() * ry.py() + du.pz() * ry.pz();
	precision_type psi = atan2(y,x);
	
	if (fabs(psi) < _phi/2.) {
	  EECamBriefJet j1_brief, j2_brief;
	  j1_brief.init(j1); j2_brief.init(j2);
	  double sin_theta, one_minus_costheta = to_double(j1_brief.distance(&j2_brief));
	  if (one_minus_costheta > numeric_limit_extras<precision_type>::sqrt_epsilon()) {
	    double cos_theta = 1.0 - one_minus_costheta;
	    double theta     = acos(cos_theta);
	    sin_theta = sin(theta);
	  } else {
	    // we are at small angles, so use small-angle formulas
	    sin_theta = sqrt(2 * one_minus_costheta);
	  }
	  _kt_in_strip += j2.modp() * sin_theta;
	}
	subjet = j1;
      }
      
    }
    return true;
  }

  // Provide a listing of the info.
  virtual void list() const {
    std::cout << "\n ---- PhiStrip with phi= " << to_double(_phi) << " ------ \n";
  }
  // return info on analysis result
  virtual precision_type operator()() const {return  kt_in_strip();}
  precision_type kt_in_strip()        const {return _kt_in_strip;  }

protected:
  precision_type _phi;
  precision_type _kt_in_strip;
  fjcore::JetDefinition _jet_def;
};

#endif // __PHISTRIP_HH__
