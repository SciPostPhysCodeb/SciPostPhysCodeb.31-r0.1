//PanScalesSTARTHEADER
//
// Copyright (c) 2018-2023, PanScales Collaboration
//
//----------------------------------------------------------------------
// This file is part of PanScales (<https://panscales.org>).
//
//  PanScales is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  PanScales is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details <http://www.gnu.org/licenses/>.
//
//  Please respect the MCnet academic usage guidelines,
//  see <https://www.montecarlonet.org/publications_guidelines/> for details.
//----------------------------------------------------------------------
//PanScalesENDHEADER
#ifndef __MOMENTUMM2_HH__
#define __MOMENTUMM2_HH__

#include <cmath>
#include <iostream>
#include <iomanip>
#include "Type.hh"
#include "Momentum4.hh"

namespace panscales{

// forward declaration
template <class MomType> class LorentzBoostGen;

//----------------------------------------------------------------------
/// MomentumM2 class containing a four vector
/// Heavily drawn from FastJet's PseudoJet
template <class T>
class MomentumM2 : public Momentum4<T> {
public:

  // the following lines are needed so that we can use the corresponding
  // parent class's members (specific to templated derived classes)
  using Momentum4<T>::px_;    
  using Momentum4<T>::py_;    
  using Momentum4<T>::pz_;    
  using Momentum4<T>::E_;    
  using Momentum4<T>::px;    
  using Momentum4<T>::py;    
  using Momentum4<T>::pz;    
  using Momentum4<T>::E;    
  using Momentum4<T>::mom3;
  using Momentum4<T>::verbose;

  /// default constructor
  MomentumM2() : Momentum4<T>(), m2_(0.0) {}

  /// constructor with four components; squared mass is calculated
  MomentumM2(T px_in, T py_in, T pz_in, T E_in)
    : Momentum4<T>(px_in,py_in,pz_in,E_in), m2_(m2_internal_function()) {}


  /// constructor with Momentum3 and energy components; squared mass is calculated
  MomentumM2(const Momentum3<T> & p3_in, T E_in)
    : Momentum4<T>(p3_in,E_in), m2_(m2_internal_function()) {}

  /// constructor with four components and the squared mass. If
  /// PANSCALES_MOMENTUM_CHECKS is defined, the supplied squared
  /// mass is cross-checked against the true squared mass
  MomentumM2(T px_in, T py_in, T pz_in, T E_in, T m2_in)
    : Momentum4<T>(px_in,py_in,pz_in,E_in), m2_(m2_in) {
#ifdef PANSCALES_MOMENTUM_CHECKS
    //std::cout << "doing check in MomentumM2::constructor(5arg)" << std::endl;
    internal_m2_check();
#endif  
    }
  
  MomentumM2(const Momentum4<T> & p4_in) : Momentum4<T>(p4_in), m2_(m2_internal_function()) {}
  MomentumM2(const Momentum4<T> & p4_in, const T m2_in) : Momentum4<T>(p4_in), m2_(m2_in) {}

  /// returns a MomentumM2 based on a 3-momentum and squared mass
  static MomentumM2 fromP3M2(const Momentum3<T> & p3_in, T m2_in) {
    return MomentumM2(p3_in.px(), p3_in.py(), p3_in.pz(), sqrt(p3_in.modpsq()+m2_in), m2_in);
  }
  /// returns a MomentumM2 based on the direction of ref + the direction
  /// difference, with a modp, energy and squared mass taken from new_mom 
  static MomentumM2 fromRefDiffNew(const MomentumM2<T> & ref, 
                                   const Momentum3<T>  & dirdiff, 
                                   const MomentumM2<T> & new_mom) {
    Momentum3<T> new_modp = new_mom.modp()*(ref.direction()+dirdiff);                                   
    return MomentumM2(new_modp.px(), new_modp.py(), new_modp.pz(),
                      new_mom.E(), new_mom.m2());
  }


  /// Constructor that takes four components, assumed to come from the
  /// sum of pa and pb, and which determines a mass based on ma^2, mb^2
  /// and the dot product pa.pb
  ///
  /// If the resulting |mass^2| is < |ma^2| + |mb^2|, then it defaults
  /// back to the squared mass from the 4-vector.
  ///
  /// If PANSCALES_MOMENTUM_CHECKS is defined, the squared mass
  /// determined from ma2, mb2 and dotab (if used) is cross-checked
  /// against the squared mass from the four-components
  MomentumM2(T px_in, T py_in, T pz_in, T E_in, T ma2_in, T mb2_in, T dotab_in)
     : Momentum4<T>(px_in,py_in,pz_in,E_in) {
    internal_set_m2_from_pair(ma2_in, mb2_in, dotab_in);
  }

  /// mass squared
  T m2_internal_function() const {return Momentum4<T>::m2();}

  /// rapidity
  T rap() const;

  // Plus and minus components
  // Make sure to avoid issues with the limit E pm pz close to zero 
  T p_plus() const {return pz_ > 0 ? E_ + pz_ : (px_*px_ + py_*py_)/(E_ - pz_);}
  T p_min()  const {return pz_ < 0 ? E_ - pz_ : (px_*px_ + py_*py_)/(E_ + pz_);}

  /// Assuming that the 4-vector components are properly set up
  /// it tries to construct a mass^2 as ma^2+mb^2+dotab;.
  /// If that looks like being numerically unstable, then
  /// simply use the m2_internal_function()
  inline void internal_set_m2_from_pair(T ma2, T mb2, T dotab) {
    T newm2 = ma2 + mb2 + 2*dotab;
    if (verbose() && (ma2 != 0.0 || mb2 != 0.0)) 
      std::cout << "ma2 = "<< ma2 << ", mb2 = " << mb2 << ", dotab = "  << dotab << ", newm2 = "  << newm2 
                << ", E^2 = " << pow2(E_) << std::endl;
    // we need to decide which value we expect to be more accurate: that
    // from the sum of masses, or that from the direct 4-vector
    // calculation.
    //
    // The rounding error on the 4-vector calculation is dangerous in
    // cases where the energy and |3-vector| are similar. In that case
    // the rounding error should be epsilon * E^2. 
    //
    // The error on the sum should be ~ epsilon * max
    // (|ma2|,|mb2|,|dotab|), assuming that the individual inputs are
    // themselves accurate.
    //
    // To establish if there is cancellation happening at all, we
    // consider
    //
    //   fabs(newm2) < fabs(ma2) + fabs(mb2)
    //
    // This will be true if there is a large cancellation between the
    // two that is not compensated by the dotab or if there is a
    // cancellation between their same-sign sum and dotab.
    //
    // Then we compare E^2 to abs_ma2_mb2 to gauge possible rounding
    // errors. 
    T abs_ma2_mb2 = fabs(ma2) + fabs(mb2);
    if (fabs(newm2) < abs_ma2_mb2
        && pow2(E_) < abs_ma2_mb2
    ) {
      newm2 = m2_internal_function();
      if (verbose() && (ma2 != 0.0 || mb2 != 0.0)) std::cout << "newm2 updated to " << newm2 << std::endl;
    }
    m2_ = newm2;
#ifdef PANSCALES_MOMENTUM_CHECKS
    if (verbose() && (ma2 != 0.0 || mb2 != 0.0)) std::cout << "doing check in MomentumM2::constructor(5arg)" << std::endl;
    internal_m2_check();
#endif  
  }

  T m2() const {return m2_;}

  /// mass
  T m() const {
    T mm = m2();
    return sqrt(mm);
    //return mm < 0.0 ? -sqrt(-mm) : sqrt(mm);
  }

  /// returns modp, taking advantage of the knowledge of the mass
  /// to just the energy (and so avoid a square root) if needed
  T modp() const {
    if (m2() == 0) return E();
    else           return Momentum4<T>::modp();
  }

  /// returns a 3-momentum direction
  Momentum3<T> direction() const {
    return Momentum3<T>(*this)/modp();
  }

  /// returns a 3-momentum difference with respect to the 
  /// positive z direction
  Momentum3<T> direction_diff_wrt_z() const {
    Momentum3<T> dir = direction();
    // strictly it would make more sense to have the transition at the
    // cube root of epsilon (cbrt), because the relative error from the
    // second-order expansion, cbrt**2, will be the same as the relative
    // error from the normal procedure of z - 1 (diff is cbrt, so
    // relative error is epsilon/cbrt = cbrt**2)
    T dir_pz = dir.pz() - 1.0;
    if (std::abs(dir_pz) < numeric_limit_extras<T>::sqrt_epsilon()) {
      // sqrt(1 - pt2) - 1 = -pt2/2 - pt2^2/8 + ...
      T dir_pt2 = dir.pt2();
      dir_pz = -dir_pt2/2.0 - (dir_pt2*dir_pt2)/8.0;
    }
    return Momentum3<T>(dir.px(), dir.py(), dir_pz);
  }

  /// returns a 3-momentum difference with respect to the 
  /// other vector (i.e. this-other)
  Momentum3<T> direction_diff(const MomentumM2 &other) const;

  /// check that the internally stored squared mass is consistent, to
  /// within reasonable accuracy, with the one that one would deduce 
  /// #TRK_ISSUE-203  from the 4-vector. Beware what happens if E is zero?
  void internal_m2_check() const {
    T modp2E2 = pow2(px_) + pow2(py_) + pow2(pz_) + pow2(E_);
    T diff = m2_internal_function() - m2();
    bool internal_m2_check_result = fabs(diff) 
            <= numeric_limit_extras<T>::sqrt_epsilon() * modp2E2;
    if (!internal_m2_check_result){
      std::cout << "  m2 internal function (from 4-vector)= " << m2_internal_function() 
                << "  m2_ internal variable = " << m2() 
                << "  diff = " << diff 
                << "  threshold = " << numeric_limit_extras<T>::sqrt_epsilon() * modp2E2
                << "  4vector = " << *this
                << std::endl;
      std::cout << "About to abort" << std::endl;
    }
    assert(internal_m2_check_result);
  }

  /// multiplication by T
  MomentumM2<T> & operator*=(T);

  /// division by T
  MomentumM2<T> & operator/=(T);

  /// addition with MomentumM2
  MomentumM2<T> & operator+=(const MomentumM2<T> &);

  /// subtraction with MomentumM2
  MomentumM2<T> & operator-=(const MomentumM2<T> &);

  /// reset MomentumM2
  void reset_momentum(const MomentumM2<T> &);
  
  /// reset MomentumM2
  void reset_momentum(T px, T py, T pz, T E);
  
  /// reset MomentumM2
  void reset_momentum(T px, T py, T pz, T E, T m2);
  
  void register_true_m2(T m2) {
    m2_ = m2;
#ifdef PANSCALES_MOMENTUM_CHECKS
    //std::cout << "doing check in MomentumM2::register_true_m2" << std::endl;
    internal_m2_check();
#endif  
  }

  /// Registers the true value of the mass and fixes the energy
  /// accordingly to avoid accumulated rounding errors.
  /// Should only be used for m2 >= 0. 
  ///
  /// Note: this is just meant for numerical accuracy, i.e. one can
  /// only use this function if the initial 4-vector had a mass close
  /// (within numerical errors) to the one passed as an argument.
  void register_true_m2_fix_E(T m2) {
    assert (m2>= 0);
    m2_ = m2;
#ifdef PANSCALES_MOMENTUM_CHECKS
    //std::cout << "doing first check in MomentumM2::register_true_m2_fix_E" << std::endl;
    internal_m2_check();
#endif
    // question of whether to place this before / after test depends
    // on whether you want to test user input, or our algebra.
    E_ = sqrt(this->modpsq() + m2);
#ifdef PANSCALES_MOMENTUM_CHECKS
    //std::cout << "doing second check in MomentumM2::register_true_m2_fix_E" << std::endl;
    internal_m2_check();
#endif
  }

  /// transform this jet (given in the rest frame of prest) into a jet
  /// in the lab frame
  MomentumM2<T> & boost(const MomentumM2<T> & prest);

  /// transform this jet (given in lab) into a jet in the rest
  /// frame of prest
  MomentumM2<T> & unboost(const MomentumM2<T> & prest);

  /// transform this jet according the boost_obj
  MomentumM2<T> & boost(const LorentzBoostGen<MomentumM2<T>> & boost_obj);




  /// constant related to the accuracy of the calculation of the
  /// dot_product (and 1-cos): whenever the raw calculation is smaller
  /// than dot_product_cross_transition one uses a more precise
  /// calculation based on a cross-product
  ///
  /// Note: A numerical study indicates that the
  /// cross-product approach is more precise than the direct dot
  /// product for cos(theta)>0.7. This correspond to
  ///   1-cos(theta)<0.3
  /// or, for massless particles,
  ///   result < 0.3 * a.E() * b.E()
  ///
  /// Switching this to 0.3 would be more precise. It has no effect
  /// on generation time but increases clustering time by ~50% (~10%)
  /// at low (high) multiplicities.
  ///
  /// Note: ideally this should be a constexpr... but since
  /// numeric_limit_extras<T>::sqrt_epsilon() is not declared as such
  /// we cannot do this for the time being. Hopefully compiler
  /// optimisation handles this properly...
  inline static T dot_product_cross_transition(){
    return numeric_limit_extras<T>::sqrt_epsilon();
  }

  /// returns a copy of the momentum with reversed spatial components
  MomentumM2<T> reversed() const {
    return MomentumM2<T>(-px_, -py_, -pz_, E_, m2_);
  }

  friend LorentzBoostGen<MomentumM2<T>>;

protected:
  /// four MomentumM2 components
  T m2_;
};

template <class T>
MomentumM2<T> operator*(T, const MomentumM2<T> &);

template <class T>
MomentumM2<T> operator/(const MomentumM2<T> &, T);

template <class T>
MomentumM2<T> operator+(const MomentumM2<T> &, const MomentumM2<T> &);

template <class T>
MomentumM2<T> operator-(const MomentumM2<T> &, const MomentumM2<T> &);

/// unary minus
template <class T>
MomentumM2<T> operator-(const MomentumM2<T> &);

/// returns the 4-vector dot product of a and b
/// #TRK_ISSUE-206  FORLATER: think what can be done here
template <class T>
T dot_product(const MomentumM2<T> & a, const MomentumM2<T> & b);
  
/// returns the 4-vector dot product of a and b, using the difference in
/// directions (dirdiff) as an aid to handle cases where a and b are
/// close in angle. Currently valid only for massless momenta
template <class T>
T dot_product_with_dirdiff(const MomentumM2<T> & a, const MomentumM2<T> & b, const Momentum3<T> dirdiff) {
  assert(a.m2() == 0 && b.m2() == 0);
  // Use: 
  //   1 - cos theta = 1 - cos^2 theta/2 + sin^2 theta/2 = 2 sin^2 theta/2
  // 
  // Then observe that dirdiff.modp()/2 = sin (theta/2)
  // so 2 sin^2(theta/2) = 0.5 * dirdiff.modpsq()
  return 0.5 * a.E()*b.E() * dirdiff.modpsq();
}

/// return the square
// template <class T>
// inline T pow2(T x) {return x*x;}

//----------------------------------------------------------------------
/// output a MomentumM2
template <class T>
inline std::ostream & operator<<(std::ostream & ostr, const MomentumM2<T> & p) {
  ostr << std::setw(13) << p.px() << " "
       << std::setw(13) << p.py() << " "
       << std::setw(13) << p.pz() << " "
       << std::setw(13) << p.E()  << " "
       << " pt = " << std::setw(11) << p.pt()
       << " y  = " << std::setw(11) << p.rap()
       << " phi= " << std::setw(11) << p.phi()
       << " m2 = " << std::setw(11) << p.m2();
  return ostr;
}

//----------------------------------------------------------------------
/// Returns the 3-vector dot-product of p1 and p2.
template <class T>
T dot3(const MomentumM2<T> & p1, const MomentumM2<T> & p2);

//----------------------------------------------------------------------
/// Returns the 3-vector cross-product of p1 and p2. If lightlike is false
/// then the energy component is zero; if it's true the the energy component
/// is arranged so that the vector is lighlike
template <class T>
MomentumM2<T> cross(const MomentumM2<T> & p1, const MomentumM2<T> & p2, bool lightlike = false);

//----------------------------------------------------------------------
/// Returns the squared mass of the 3-vector cross-product of p1 and p2. 
/// The result is multiplied by norm before being returned 
/// (this can sometimes help with avoiding
/// over or underflows, notably in the calculation of the mass)
template <class T>
T cross_m2_norm(const MomentumM2<T> & p1, const MomentumM2<T> & p2, T norm);

//----------------------------------------------------------------------
/// Returns two 4-vectors, each with square = -1, that have a zero
/// (4-vector) dot-product with dir1 and dir2.
///
/// When dir1 and dir2 form a plane, then perp1 will be out of that
/// plane and perp2 will be in the plane.
template <class T>
[[deprecated]] 
void twoPerpOld(const MomentumM2<T> & dir1, const MomentumM2<T> & dir2,
             MomentumM2<T> & perp1, MomentumM2<T> & perp2);

/// Returns two 4-vectors, each with square = -1, that have a zero
/// (4-vector) dot-product with dir1 and dir2.
///
/// When dir1 and dir2 form a plane, then perp1 will be out of that
/// plane and perp2 will be in the plane.
///
/// If either dir1 or dir2 is along the x, y or z axis, the routine
/// should give reliable results for theta_12 angles down to about 
/// sqrt(numeric_limits<T>::min()), otherwise it breaks down
/// at numeric_limits<T>::epsilon().
template <class T>
void two_perp(const MomentumM2<T> & dir1, const MomentumM2<T> & dir2,
             MomentumM2<T> & perp1, MomentumM2<T> & perp2);

template <class T>
[[deprecated("Use two_perp instead")]]
void twoPerp(const MomentumM2<T> & dir1, const MomentumM2<T> & dir2,
             MomentumM2<T> & perp1, MomentumM2<T> & perp2) {return two_perp(dir1,dir2,perp1,perp2);}

/// Returns (1-cos theta) where theta is the angle between p1 and p2
template <class T>
T one_minus_costheta(const MomentumM2<T> & p1, const MomentumM2<T> & p2);


//----------------------------------------------------------------------
/// Class that contains a Lorentz-boost
template <class M>
class LorentzBoostGen {

public:

  typedef typename M::prec_type T;
  typedef typename M::prec_type prec_type;

  ///
  LorentzBoostGen(const M & pboost) 
      : pboost_(pboost) {
    T m = pboost.m();
    assert(m != 0); // TRK_ISSUE-207 GPS: at some point, replace with throw
    inv_m_ = 1.0/m;
    inv_E_plus_m_ = 1.0 / (m + pboost.E());
    no_action_ = (   pboost.px() == 0.0 
                  && pboost.py() == 0.0 
                  && pboost.pz() == 0.0);
  }

  /// returns true if this boost has no effect
  bool no_action() const {return no_action_;}

  const M & pboost() const {return pboost_;}

  /// returns the application of the boost to the momentum
  M operator*(const M & p) const {
    if (no_action_) return p;
    T Enew = new_energy(p);

    // the 3-vector modification has to be parallel to the boost vector
    // (transverse components don't get any modification). One
    // can verify that the following coefficient gives the correct final
    // answer, e.g. by checking invariant mass of the final vector         
    T fn = coeff_3mom(p, Enew);
    return M(
      p.px() + fn * pboost_.px(),
      p.py() + fn * pboost_.py(),
      p.pz() + fn * pboost_.pz(),
      Enew,
      p.m2()
    );
  }

  Momentum3<T> boost_dirdiff(const Momentum3<T> & dirdiff_2_minus_1, 
                             const M & p1, const M & p1new) {
    assert(p1.m2() == 0.0);
    //assert(p2.m2() == 0.0);
    T E1 = p1.E();
    return boost_dirdiff(dirdiff_2_minus_1, E1, p1new);
  }

  /// Returns the new direction difference after boosting.
  /// The arguments are the differences in directions between momentum 2
  /// and momentum 1, as well as momentum 1 itself.
  /// The algorithm assumes both momenta are massless.
  Momentum3<T> boost_dirdiff(const Momentum3<T> & dirdiff_2_minus_1, 
                             const T E1, const M & p1new) {

    // Take the two initial directions to be unit-normalised; 
    // After boosting the corresponding massless 4-vectors will not be
    // unit-normalised. The difference in normalisations is given by
    // the difference in "new" energies; since both particles are
    // massless, we get the result directly from 3-vector part of the 
    // normal new_energy operation
    T deltaEnew_norm = dot_product_3(dirdiff_2_minus_1, pboost()) * inv_m_;

    // get the energies of the two new momenta, normalised
    // to the original energies (i.e. these are the energies
    // of the boosted light-like direction vectors)
    T E1new_norm = p1new.E()/E1;
    T E2new_norm = E1new_norm + deltaEnew_norm;

    // We would add the pboost_ vector to each of the directions, but
    // with a difference coefficient; for the final direction difference
    // we will need the difference in the 3mom coefficients; inspecting
    // the coeff_3mom() function, since the original direction energies
    // are both 1 (cf. masslessness), the result just depends on the 
    // difference in new energies (normalised s.t. original energies
    // are 1)
    T delta_3mom_coeff = deltaEnew_norm * inv_E_plus_m_;

    // To get the direction difference, first we need to get the
    // difference between the two boosted direction vectors, BUT with
    // the condition that they should both have the same normalisation.
    //
    // We have three terms
    // - the original direction difference 
    // - the difference in the coefficients of the pboost_ vector
    // - the extra piece associated with the difference in energies of
    //   the boosted directions. For this we will add the extra piece
    //   needed to get p1new scaled to the same energy as the boosted
    //   direction2 (recall that deltaEnew_norm is 2 minus 1, and that
    //   the direction difference is also 2 minus 1)
    Momentum3<T> dirdiff_new = (dirdiff_2_minus_1 + delta_3mom_coeff * pboost_.p3() 
                               - (deltaEnew_norm/p1new.E()) * p1new.p3());
    // finally we scale back such that the difference corresponds to
    // directions with unit energy
    dirdiff_new *= 1.0/(E2new_norm);
    return dirdiff_new;
  }

  /// returns the application of the boost to the momentum
  LorentzBoostGen<M> operator*(const LorentzBoostGen<M> & l) const {
    return LorentzBoostGen( (*this) * l.pboost() );
  }

  /// returns the inverse boost
  LorentzBoostGen<M> inverse() const {
    LorentzBoostGen<M> inv(*this);
    inv.pboost_ = pboost_.reversed();
    return inv;
  }


  /// returns the new energy of p after boosting
  typename M::prec_type new_energy(const M & p) const {
    // Let pf   = momentum after boost
    //     prev = pboost_.reversed()
    //
    // We start in rest frame of pboost_ and go into the lab frame;
    // That is the same as starting in the lab frame and going into
    // the rest frame of prev. 
    //
    // Since the dot product is boost invariant, we can use the dot
    // product in the original frame to deduce pf.E()
    //
    //     pf.E() = dot_product(p, prev) / prev.m()
    //
    // The signs are already taken into account here
    return (  p.px() * pboost_.px()
            + p.py() * pboost_.py()
            + p.pz() * pboost_.pz()
            + p.E () * pboost_.E ()) * inv_m_;
  }

  friend M;

private:

  /// returns the coefficient in front of the boost 3-momentum, to be
  /// used when modifying p.p3() for the boost (i.e. coeff*3mom is
  /// what gets added to p.p3()). It takes the new_energy argument
  /// because in most cases we will also need new_energy separately
  T coeff_3mom(const M & p, const typename M::prec_type new_energy) const {
    // One can verify that the following coefficient gives the correct
    // final answer, e.g. by checking invariant mass of the final vector
    return (new_energy + p.E()) * inv_E_plus_m_;
  }

  M pboost_;
  T inv_m_, inv_E_plus_m_;
  bool no_action_;
};


/// Given 
///
/// - a linear-transform function linear_transform 
/// - the energies of two massless momenta mom1 and mom2
/// - the (full) momenta lt_mom1 and lt_mom2 after application of 
///   linear_transform (i.e. lt_mom1==linear_transform(mom1)), 
/// - and the direction difference between mom1 and mom2
///   (dirdiff_2_minus_1)
///
/// return the direction difference between lt_mom1 and lt_mom2.
template<class T, class LT>
Momentum3<T> apply_linear_transform_to_dirdiff(
  const LT linear_transform, 
  const T & mom1_E,
  const T & ,
  const MomentumM2<T> & lt_mom1,
  const MomentumM2<T> & lt_mom2,
  const Momentum3<T> & dirdiff_2_minus_1
) {
  assert(lt_mom1.m2() == 0);
  assert(lt_mom2.m2() == 0);

  // energy component of the 4-vector diff is zero, because this is the
  // difference between two light-like vectors each of energy 1
  const MomentumM2<T> dirdiff_2_minus_1_4vec(dirdiff_2_minus_1.px(), dirdiff_2_minus_1.py(), dirdiff_2_minus_1.pz(), 0.0);
  MomentumM2<T> lt_dirdiff_2_minus_1_4vec = linear_transform * dirdiff_2_minus_1_4vec;

  // Imagine that we had applied the linear transform to each of 1 & 2, 
  // normalised such that the original lengths are equal to one.
  //
  //     / 2
  //    /
  //    -------- 1
  //
  // after the boost, the energy components of the two would no longer
  // be equal to 1 and would also no longer be equal to each other.
  //
  // to get the direction different we need to rescale such that one of
  // the lengths is equal to 1, and then adjust for the remaining
  // difference in length
  //
  // For now, take dir1 as the reference (it shouldn't matter which one
  // we use, unless one happens to be unusually close to zero)
  
  T lt_E1_norm = lt_mom1.E()/mom1_E;
  // this operation takes us to a 4-mom difference in which 
  // "1" has unit length
  lt_dirdiff_2_minus_1_4vec /= lt_E1_norm;

  // after rescaling, we still have a difference in length between 2 &
  // 1, and that difference in length is equal to the energy component
  // of 2_minus_1.
  T E2_minus_E1 = lt_dirdiff_2_minus_1_4vec.E();

  // the next step is to add something to the 4-vector difference, in the
  // direction of 2 (post branching), such that energy difference goes
  // to zero; so we remove something of length E2_minus_E1, in the
  // direction of lt_mom2
  lt_dirdiff_2_minus_1_4vec -= (E2_minus_E1/lt_mom2.E()) * lt_mom2;

  return Momentum3<T>(lt_dirdiff_2_minus_1_4vec);
}

} // namespace panscales
  
#endif //  __MOMENTUMM2_HH__
