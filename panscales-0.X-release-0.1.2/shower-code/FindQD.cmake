#[=======================================================================[.rst:
FindQD
------

Find the QD includes and libraries.

Imported Targets
^^^^^^^^^^^^^^^^

If QD is found, this module defines the following :prop_tgt:`IMPORTED`
targets::

 QD::qd       - The QD library.

Result Variables
^^^^^^^^^^^^^^^^

This module will set the following variables in your project::

 QD_FOUND             - True if QD found on the local system
 QD_CONFIG_EXECUTABLE - qd-confgi file used to obtain information
 QD_INCLUDE_DIR       - Location of QD header files.
 QD_LIBRARIES         - The QD libraries.
 QD_VERSION           - The version of the discovered QD install.

Hints
^^^^^

Set ``QD_ROOT_DIR`` to a directory that contains a QD installation.
This script expects to find qd-config at ``$QD_ROOT_DIR/bin``

#]=======================================================================]


# find qd-config
find_program( QD_CONFIG_EXECUTABLE
  NAMES qd-config
  HINTS "${QD_ROOT_DIR}/bin"
  )

 message("-- QD source dir is ${QD_ROOT_DIR}")
 message("-- QD executable is ${QD_CONFIG_EXECUTABLE}")

if( EXISTS "${QD_CONFIG_EXECUTABLE}" )
  # find the prefix
  execute_process(
    COMMAND "${QD_CONFIG_EXECUTABLE}" --prefix
    OUTPUT_VARIABLE QD_PREFIX
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
  # find the include dirs
  find_path( QD_INCLUDE_DIR 
    NAMES qd/dd_real.h
    HINTS ${QD_ROOT_DIR}/include ${QD_PREFIX}/include
    )
  # find the library
  find_library( QD_LIBRARIES 
    NAMES qd
    HINTS ${QD_ROOT_DIR}/lib ${QD_PREFIX}/lib
    )
  # find the version
  execute_process(
    COMMAND "${QD_CONFIG_EXECUTABLE}" --version
    OUTPUT_VARIABLE QD_VERSION
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
endif()

#=============================================================================
# handle the QUIETLY and REQUIRED arguments and set GSL_FOUND to TRUE if all
# listed variables are TRUE
find_package_handle_standard_args( QD
  FOUND_VAR
    QD_FOUND
  REQUIRED_VARS
    QD_INCLUDE_DIR
    QD_LIBRARIES
  VERSION_VAR
    QD_VERSION
    )

mark_as_advanced( QD_PREFIX )

#=============================================================================
# Register imported libraries
if (QD_FOUND)
  # For all environments without dll libraries, create
  # the imported library targets
  #
  # This will break on environments using dll libraries
  add_library( QD::qd      UNKNOWN IMPORTED )
  set_target_properties( QD::qd PROPERTIES
    IMPORTED_LOCATION                 "${QD_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES     "${QD_INCLUDE_DIRS}"
    IMPORTED_LINK_INTERFACE_LANGUAGES "C++"
  )
endif()
