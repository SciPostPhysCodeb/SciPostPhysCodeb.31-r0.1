#include "QCD.hh"
#include <sstream>
#include "autogen/auto_QCD_global-cc.hh"

namespace panscales{
  
std::string QCDinstance::description() const {
  std::ostringstream ostr;
  ostr << "QCD with " << _alphas->description();
  ostr << ", xmuR = " << xmuR();
  ostr << ", xmuR compensation = " << xmuR_compensation() << " (if supported in shower)";
  ostr << ", xmuF = " << xmuF();
  if (_alphas->lnQ()>_alphas->lnkt_cutoff()) {
    ostr << ", set to 0 for lnmuR < " << _alphas->lnkt_cutoff();
    ostr << " where alpha_s^MSbar = " << _alphas->alphasMSbar_no_cutoff(_alphas->lnkt_cutoff());
  }
      
  ostr << ", CA = " << _CA
       << ", CF = " << _CF
       << ", nf = " << _nf
       << ", TR = " << _TR
       << ", pure_soft_splitting = " << _soft_limit
       << ", gluon_uses_q2qg = "  << _gluon_uses_q2qg
       << ", gluon_uses_qqbar = " << _gluon_uses_qqbar
       << ", splitting_wgg = " << _splitting_finite_weight_g2gg
       << ", splitting_wqq = " << _splitting_finite_weight_g2qqbar
       << ", mc = " << _mc
       << ", mb = " << _mb;

  // #TRK_ISSUE-233  need better treatment here, because of danger of mismatch with 
  // ColourTransitionRunnerBase
  ostr << ", colour handling = " << _colour_scheme;

  return ostr.str();
}

}
